# 57: BlueStore Config Reference

**クリップソース:** [57: BlueStore Config Reference — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/configuration/bluestore-config-ref/)

# BlueStore Config Reference[¶](https://docs.ceph.com/en/pacific/rados/configuration/bluestore-config-ref/ "Permalink to this headline")

## Devices[¶](https://docs.ceph.com/en/pacific/rados/configuration/bluestore-config-ref/ "Permalink to this headline")

BlueStoreは、1つ、2つ、または（特定のケースでは）3つのストレージデバイスを管理します。

最も単純なケースでは、BlueStoreは1つの（プライマリ）ストレージデバイスを消費します。このストレージデバイスは通常、BlueStoreが直接管理するデバイス全体を占有して使用されます。このプライマリデバイスは通常、データディレクトリ内のblockシンボリックリンクで識別されます。

データディレクトリはtmpfsマウントで、OSDの識別子、所属するクラスタ、秘密鍵など、OSDに関する情報を持つ一般的なOSDファイルが\(起動時またはceph\-volumeが起動したときに\)格納されます。

また、1～2台の追加デバイスにBlueStoreを展開することも可能です。

* WAL（write\-ahead log）デバイス（データ・ディレクトリ内のblock.walとして識別される）は、BlueStoreの内部ジャーナルまたはライト・アヘッド・ログに使用できます。WALデバイスを使用するのは、そのデバイスがプライマリ・デバイスよりも高速である場合（例えば、WALデバイスがSSD上にあり、プライマリ・デバイスがHDDである場合）にのみ有用です。
* DBデバイス（データディレクトリ内のblock.dbとして識別される）は、BlueStoreの内部メタデータを保存するために使用することができます。 BlueStore（というよりも、組み込まれたRocksDB）は、パフォーマンスを向上させるために、できる限り多くのメタデータをDBデバイスに格納します。 DBデバイスがいっぱいになると、メタデータは（そうでなければ置かれていたであろう）プライマリデバイスにこぼれ落ちます。 繰り返しになりますが、DBデバイスをプロビジョニングするのは、プライマリデバイスよりも高速な場合にのみ有効です。

利用可能な高速ストレージが少量（例えば1ギガバイト以下）しかない場合は、WALデバイスとして使用することをお勧めします。 それ以上の量がある場合は、DBデバイスをプロビジョニングする方が理にかなっています。 BlueStoreジャーナルは常に利用可能な最速のデバイスに配置されるため、DBデバイスを使用することで、WALデバイスと同様のメリットが得られると同時に、（収まるのであれば）追加のメタデータをそこに保存することができます。 つまり、DBデバイスが指定されていても、明示的なWALデバイスが指定されていなければ、WALは暗黙のうちに高速なデバイス上のDBとコロケーションされることになります。

シングルデバイス（コロケーション）のBlueStore OSDのプロビジョニング：

```
ceph-volume lvm prepare --bluestore --data <device>
```

WALデバイスやDBデバイスを指定するには：

```
ceph-volume lvm prepare --bluestore --data <device> --block.wal <wal-device> --block.db <db-device>
```

注：\-\-dataはvg/lv表記の論理ボリュームになります。その他のデバイスは、既存の論理ボリュームまたはGPTパーティションです。

### Provisioning strategies[¶](https://docs.ceph.com/en/pacific/rados/configuration/bluestore-config-ref/ "Permalink to this headline")

BlueStoreのOSDを展開する方法は複数ありますが（1つしかなかったFilesoreとは異なります）、展開戦略を明確にするための2つの共通の取り決めがあります。

#### **block \(data\) only**[¶](https://docs.ceph.com/en/pacific/rados/configuration/bluestore-config-ref/ "Permalink to this headline")

すべてのデバイスが同じタイプで、たとえばすべての回転ドライブで、メタデータに使用する高速デバイスがない場合は、blockデバイスのみを指定し、block.dbやblock.walを分離しないことに意味があります。単一の/dev/sdaデバイスに対するlvmコマンドは次のようになります。

```
ceph-volume lvm create --bluestore --data /dev/sda
```

各デバイスに論理ボリュームがすでに作成されている場合（単一のLVがデバイスの100%を使用している場合）、ceph\-vg/block\-lvという名前のLVのlvmコールは次のようになります。

```
ceph-volume lvm create --bluestore --data ceph-vg/block-lv
```

#### **block and block.db**[¶](https://docs.ceph.com/en/pacific/rados/configuration/bluestore-config-ref/ "Permalink to this headline")

高速デバイスと低速デバイス\(SSD / NVMeおよび回転ドライブ\)が混在している場合は、block.dbを高速デバイスに配置し、block\(データ\)を低速\(回転ドライブ\)に常駐させることをお勧めします。

これらのボリュームグループと論理ボリュームは、現在ceph\-volumeツールでは自動的に作成できないため、手動で作成する必要があります。

以下の例では、4つのローテーショナル（sda, sdb, sdc, sdd）と1つの（高速）ソリッドステートドライブ（sdx）を想定しています。まず、ボリュームグループを作成します。

```
$ vgcreate ceph-block-0 /dev/sda
$ vgcreate ceph-block-1 /dev/sdb
$ vgcreate ceph-block-2 /dev/sdc
$ vgcreate ceph-block-3 /dev/sdd
```

次に、block用の論理ボリュームを作成します。

```
$ lvcreate -l 100%FREE -n block-0 ceph-block-0
$ lvcreate -l 100%FREE -n block-1 ceph-block-1
$ lvcreate -l 100%FREE -n block-2 ceph-block-2
$ lvcreate -l 100%FREE -n block-3 ceph-block-3
```

4台の低速回転デバイス用に4つのOSDを作成するので、/dev/sdxに200GBのSSDがあると仮定して、それぞれ50GBの4つの論理ボリュームを作成します。

```
$ vgcreate ceph-db-0 /dev/sdx
$ lvcreate -L 50GB -n db-0 ceph-db-0
$ lvcreate -L 50GB -n db-1 ceph-db-0
$ lvcreate -L 50GB -n db-2 ceph-db-0
$ lvcreate -L 50GB -n db-3 ceph-db-0
```

最後に、ceph\-volumeで4つのOSDを作成します。

```
$ ceph-volume lvm create --bluestore --data ceph-block-0/block-0 --block.db ceph-db-0/db-0
$ ceph-volume lvm create --bluestore --data ceph-block-1/block-1 --block.db ceph-db-0/db-1
$ ceph-volume lvm create --bluestore --data ceph-block-2/block-2 --block.db ceph-db-0/db-2
$ ceph-volume lvm create --bluestore --data ceph-block-3/block-3 --block.db ceph-db-0/db-3
```

これらの操作により、4つのOSDが作成され、blockは低速の回転ドライブに、それぞれ50GBの論理ボリューム（DB）はソリッドステートドライブに配置されることになります。

## Sizing[¶](https://docs.ceph.com/en/pacific/rados/configuration/bluestore-config-ref/ "Permalink to this headline")

スピニングドライブとソリッドドライブの混在したセットアップを使用する場合、BlueStore用に十分な大きさのblock.db論理ボリュームを作成することが重要です。一般的に、block.dbはできるだけ大きな論理ボリュームを持つべきです。

一般的には、block.dbのサイズをblockサイズの1%から4%の間にすることが推奨される。RGWのワークロードでは、block.dbのサイズをblockの4%以下にすることが推奨されます。これは、RGWがメタデータ（omapキー）の保存にblock.dbを多用するためです。例えば、blockサイズが1TBであれば、block.dbは40GB以下にすべきではないと考えられます。RBDのワークロードでは、通常、blockサイズの1～2%で十分です。

古いリリースでは、内部レベルのサイズとは、L0、L0\+L1、L1\+L2などのサイズの合計に対応する特定のパーティション／LVサイズのみをDBが完全に利用できることを意味し、デフォルト設定では約3GB、30GB、300GBなどとなっています。 ほとんどのデプロイメントでは、L3以上のサイズに対応しても実質的なメリットはありませんが、この数字を6GB、60GB、600GBと倍にすることで、DBの圧縮が容易になります。

Nautilus 14.2.12およびOctopus 15.2.6以降のリリースでは、任意のDBデバイスサイズをより有効に活用できるようになり、Pacificリリースでは実験的にダイナミックレベルをサポートしています。 古いリリースのユーザは、将来のアップグレードでその利点を実現するために、今日、より大きなDBデバイスをプロビジョニングすることで、先を見越して計画することができます。

高速デバイスと低速デバイスが混在していない場合は、block.db（またはblock.wal）用に個別の論理ボリュームを作成する必要はありません。BlueStoreはこれらをblock.db（またはblock.wal）のスペース内に自動的にコロケーションします。

## Automatic Cache Sizing[¶](https://docs.ceph.com/en/pacific/rados/configuration/bluestore-config-ref/ "Permalink to this headline")

BlueStoreは、TCMallocがメモリアロケータとして設定され、bluestore\_cache\_autotune設定が有効になっている場合、キャッシュのサイズを自動的に変更するように設定できます。 このオプションは現在、デフォルトで有効になっています。 BlueStoreは、OSD\_memory\_target設定オプションにより、OSDのヒープメモリ使用量を指定されたターゲットサイズ以下に維持しようとします。 これはベストエフォートアルゴリズムであり、キャッシュはosd\_memory\_cache\_minで指定された量よりも小さく縮小することはありません。 キャッシュの比率は、優先順位の階層に基づいて選択されます。 優先順位の情報が得られない場合は、bluestore\_cache\_meta\_ratio および bluestore\_cache\_kv\_ratio オプションがフォールバックとして使用されます。

**bluestore\_cache\_autotune**

Description

BlueStoreの各種キャッシュに割り当てられているスペース比率を、最小値を尊重しながら自動的に調整。

Type

_Boolean_

Required

_Yes_

Default

_True_

**osd\_memory\_target**

Description

TCMalloc が利用可能で、キャッシュの自動調整が有効な場合は、このバイト数をメモリにマッピングしておくようにする。注意：これは、プロセスのRSSメモリ使用量と正確に一致しない場合がある。 プロセスによってマップされたヒープメモリの総量は通常、この目標に近いはずだが、カーネルが実際にマップされていないメモリを再利用するという保証はない。 初期の開発段階では、カーネルによってはOSDのRSSメモリがマップされたメモリを最大20%も上回ることが判明した。 しかし、一般的には、メモリ圧が高い場合、カーネルはマップされていないメモリをより積極的に回収するのではないかという仮説がある

Type

_Unsigned Integer_

Required

_Yes_

Default

_4294967296_

**bluestore\_cache\_autotune\_chunk\_size**

Description

キャッシュオートチューンが有効な場合に、キャッシュに割り当てるチャンクサイズ（バイト）。 オートチューナが様々なキャッシュにメモリを割り当てる際、チャンクでメモリを割り当てる。 これは、ヒープサイズやオートチューンされたキャッシュ比率にわずかな変動があった場合に、evictionを避けるために行われる

Type

_Unsigned Integer_

Required

_No_

Default

_33554432_

**bluestore\_cache\_autotune\_interval**

Description

キャッシュの自動調整を有効にした場合、リバランスの間に待つ秒数。 この設定は、さまざまなキャッシュの割り当て比率を再計算する速度を変更する。 注意：この間隔を小さく設定しすぎると、CPU使用率が高くなり、パフォーマンスが低下する

Type

_Float_

Required

_No_

Default

_5_

**osd\_memory\_base**

Description

TCMallocおよびキャッシュ自動調整機能が有効な場合、OSDが必要とする最小のメモリ量をバイト単位で推定する。 これは、オートチューナがキャッシュの予想総メモリ消費量を見積もる際に使用される

Type

_Unsigned Integer_

Required

_No_

Default

_805306368_

**osd\_memory\_expected\_fragmentation**

Description

TCMallocおよびキャッシュのオートチューニングが有効な場合、メモリの断片化の割合を推定する。 これは、オートチューナがキャッシュの予想される総メモリ消費量を推定するために使用される

Type

_Float_

Required

_No_

Default

_0.15_

**osd\_memory\_cache\_min**

Description

TCMallocとキャッシュオートチューニングが有効な場合、キャッシュに使用するメモリの最小量を設定する。注意：この値を低く設定しすぎると、キャッシュのスラッシングが著しくなることがある

Type

_Unsigned Integer_

Required

_No_

Default

_134217728_

**osd\_memory\_cache\_resize\_interval**

Description

TCMallocとキャッシュの自動調整が有効な場合、キャッシュのサイズ変更の間にこの秒数を待つ。 この設定は、BlueStoreがキャッシュに使用できるメモリの合計量を変更する。 この間隔を小さく設定しすぎると、メモリ・アロケータのスラッシングが発生し、パフォーマンスが低下することがあるので注意

Type

_Float_

Required

_No_

Default

_1_

## Manual Cache Sizing[¶](https://docs.ceph.com/en/pacific/rados/configuration/bluestore-config-ref/ "Permalink to this headline")

各OSDがBlueStoreのキャッシュのために消費するメモリ量は、設定オプションの_bluestore\_cache\_size_によって決定されます。 この設定オプションが設定されていない（つまり0のまま）場合、プライマリデバイスにHDDとSSDのどちらが使用されているかによって使用される異なるデフォルト値があります（_bluestore\_cache\_size\_ssd_および_bluestore\_cache\_size\_hdd_設定オプションによって設定されます）。

BlueStoreとCeph OSDデーモンの残りの部分は、このメモリバジェット内で動作するように最善を尽くします。 設定されたキャッシュサイズの他に、OSD自体が消費するメモリと、メモリの断片化やその他のアロケータのオーバーヘッドによる追加利用があることに注意してください。

設定されたキャッシュメモリーバジェットは、いくつかの異なる方法で使用することができます。

* キー/バリューのメタデータ\(RocksDBの内部キャッシュなど\)
* BlueStoreのメタデータ
* BlueStoreデータ（最近読み書きされたオブジェクトデータなど）

キャッシュのメモリ使用量は、次のオプションによって管理されます: _bluestore\_cache\_meta\_ratio_ および _bluestore\_cache\_kv\_ratio_。データに割り当てられるキャッシュの割合は、meta と kv の比率だけでなく、有効な bluestore キャッシュサイズ \(_bluestore\_cache\_size\[\_ssd|\_hdd\]_ の設定とプライマリデバイスのデバイスクラスに依存\) にも左右されます。データの割合は、_\<effective\_cache\_size\>\*\(1\-bluestore\_cache\_meta\_ratio\-bluestore\_cache\_kv\_ratio\)_で計算できます。

**bluestore\_cache\_size**

Description

BlueStoreがキャッシュに使用するメモリの量。 ゼロの場合は、代わりにbluestore\_cache\_size\_hddまたはbluestore\_cache\_size\_ssdが使用される

Type

_Unsigned Integer_

Required

_Yes_

Default

_0_

**bluestore\_cache\_size\_hdd**

Description

HDDでバックアップされている場合、BlueStoreがキャッシュに使用するデフォルトのメモリ量

Type

_Unsigned Integer_

Required

_Yes_

Default

_1\*1024\*1024\*1024 \(1 GB\)_

**bluestore\_cache\_size\_ssd**

Description

SSDでバックアップされている場合、BlueStoreがキャッシュに使用するデフォルトのメモリ量

Type

_Unsigned Integer_

Required

_Yes_

Default

_3\*1024\*1024\*1024 \(3 GB\)_

**bluestore\_cache\_meta\_ratio**

Description

メタデータに充てられるキャッシュの割合

Type

_Floating point_

Required

_Yes_

Default

_.4_

**bluestore\_cache\_kv\_ratio**

Description

キー／バリューデータ（RocksDB）に充てられるキャッシュの割合

Type

_Floating point_

Required

_Yes_

Default

_.4_

**bluestore\_cache\_kv\_max**

Description

キー／バリューデータ（RocksDB）に割り当てられるキャッシュの最大量

Type

_Unsigned Integer_

Required

_Yes_

Default

_512\*1024\*1024 \(512 MB\)_

## Checksums[¶](https://docs.ceph.com/en/pacific/rados/configuration/bluestore-config-ref/ "Permalink to this headline")

BlueStoreは、ディスクに書き込まれるすべてのメタデータとデータをチェックサムします。 メタデータのチェックサムはRocksDBが行い、crc32cを使用します。データのチェックサムはBlueStoreによって行われ、crc32c、xxhash32、またはxxhash64を使用します。 デフォルトはcrc32cで、ほとんどの目的に適しています。

データのフルチェックサムを行うと、BlueStoreが保存・管理しなければならないメタデータの量が増えます。 クライアントがデータの書き込みと読み込みが順次行われることを示唆している場合など、可能な場合には、BlueStoreはより大きなブロックをチェックサムしますが、多くの場合、4キロバイトのデータブロックごとにチェックサム値（通常4バイト）を保存する必要があります。

チェックサムを2バイトまたは1バイトに切り詰めることで、より小さなチェックサム値を使用し、メタデータのオーバーヘッドを減らすことができます。 この場合、32ビット（4バイト）のチェックサムでは約40億分の1、16ビット（2バイト）のチェックサムでは65,536分の1、8ビット（1バイト）のチェックサムでは256分の1となり、ランダムエラーが検出されない確率が高くなります。チェックサムのアルゴリズムとしてcrc32c\_16またはcrc32c\_8を選択すると、より小さなチェックサム値を使用できます。

チェックサムのアルゴリズムは、プールごとの csum\_type プロパティまたはグローバルなコンフィグオプションで設定できます。 例えば、以下のようになります。

```
ceph osd pool set <pool-name> csum_type <algorithm>
```

**bluestore\_csum\_type**

Description

使用するデフォルトのチェックサムアルゴリズム

Type

_String_

Required

_Yes_

Valid Settings

_none, crc32c, crc32c\_16, crc32c\_8, xxhash32, xxhash64_

Default

_crc32c_

## Inline Compression[¶](https://docs.ceph.com/en/pacific/rados/configuration/bluestore-config-ref/ "Permalink to this headline")

BlueStoreは、snappy、zlib、またはlz4を使用したインライン圧縮をサポートしています。lz4圧縮プラグインは公式リリースでは配布されていませんのでご注意ください。

BlueStoreのデータが圧縮されているかどうかは、圧縮モードと書き込み操作に関連するヒントの組み合わせによって決定されます。 モードは：

* **none**
* **passive**
* **aggressive**
* **force**

圧縮性と非圧縮性のIOヒントについては、[rados\_set\_alloc\_hint\(\)](https://docs.ceph.com/en/pacific/rados/configuration/bluestore-config-ref/ "rados_set_alloc_hint")を参照してください。 

なお、モードにかかわらず、データチャンクのサイズが十分に縮小されない場合は使用されず、元の（圧縮されていない）データが保存されます。 例えば、_bluestore compression required ratio_が.7に設定されている場合、圧縮されたデータは元のデータの70％のサイズ（またはそれ以下）でなければなりません。

圧縮モード、圧縮アルゴリズム、圧縮必要率、最小ブロブサイズ、最大ブロブサイズは、プールごとのプロパティまたはグローバル設定オプションで設定できます。 プールのプロパティは、次の方法で設定できます：

```
ceph osd pool set <pool-name> compression_algorithm <algorithm>
ceph osd pool set <pool-name> compression_mode <mode>
ceph osd pool set <pool-name> compression_required_ratio <ratio>
ceph osd pool set <pool-name> compression_min_blob_size <size>
ceph osd pool set <pool-name> compression_max_blob_size <size>
```

**bluestore\_compression\_algorithm**

Description

プールごとのプロパティcompression\_algorithmが設定されていない場合に使用するデフォルトのコンプレッサ（もしあれば）。zstdは、少量のデータを圧縮する際のCPUオーバーヘッドが大きいため、BlueStoreでは推奨されていないことに注意

Type

_String_

Required

_No_

Valid Settings

_lz4, snappy, zlib, zstd_

Default

_snappy_

**bluestore\_compression\_mode**

Description

プールごとのプロパティ compression\_mode が設定されていない場合、圧縮を使用するためのデフォルトのポリシ。none は、決して圧縮を使用しないことを意味する。 passive は、クライアントがデータが圧縮可能であることを示唆した場合に圧縮を使用することを意味する

Type

_String_

Required

_No_

Valid Settings

_none, passive, aggressive, force_

Default

_none_

**bluestore\_compression\_required\_ratio**

Description

圧縮版を保存するためには、元のサイズに対する圧縮後のデータチャンクのサイズの比率が、少なくともこの程度小さくなければならない

Type

_Floating point_

Required

_No_

Default

_.875_

**bluestore\_compression\_min\_blob\_size**

Description

これより小さいチャンクは圧縮されない。プールごとのプロパティ compression\_min\_blob\_size がこの設定を上書きする

Type

_Unsigned Integer_

Required

_No_

Default

_0_

**bluestore\_compression\_min\_blob\_size\_hdd**

Description

回転するメディアに対する_bluestore compression min blob size_のデフォルト値

Type

_Unsigned Integer_

Required

_No_

Default

_128K_

**bluestore\_compression\_min\_blob\_size\_ssd**

Description

回転しない（ソリッドステート）メディアに対する_bluestore compression min blob size_のデフォルト値

Type

_Unsigned Integer_

Required

_No_

Default

_8K_

**bluestore\_compression\_max\_blob\_size**

Description

この値より大きいチャンクは、圧縮される前に、最大でも bluestore\_compression\_max\_blob\_size バイトの小さな blob に分割される。プールごとのプロパティcompression\_max\_blob\_sizeは、この設定よりも優先される

Type

_Unsigned Integer_

Required

_No_

Default

_0_

**bluestore\_compression\_max\_blob\_size\_hdd**

Description

回転するメディアに対するbluestore compression max blob sizeのデフォルト値

Type

_Unsigned Integer_

Required

_No_

Default

_512K_

**bluestore\_compression\_max\_blob\_size\_ssd**

Description

回転しない（SSD、NVMe）メディアに対するbluestore compression max blob sizeのデフォルト値

Type

_Unsigned Integer_

Required

_No_

Default

_64K_

## SPDK Usage[¶](https://docs.ceph.com/en/pacific/rados/configuration/bluestore-config-ref/ "Permalink to this headline")

NVMeデバイス用のSPDKドライバーを使用する場合は、システムを準備する必要があります。詳細については、SPDKドキュメントを参照してください。

SPDK は、デバイスを自動的に構成するためのスクリプトを提供しています。ユーザーはこのスクリプトを root で実行できます。

```
$ sudo src/spdk/scripts/setup.sh
```

bluestore\_block\_pathには、対象となるNVMeデバイスのデバイスセレクターを「spdk:」というプレフィックスで指定する必要があります。

例えば、インテル製PCIe SSDのデバイスセレクタは次のようになります。

```
$ lspci -mm -n -D -d 8086:0953
```

デバイスセレクターは、常にDDDD:BB:DD.FFまたはDDDD.BB.DD.FFの形をしています。

以下のように設定します：

```
bluestore_block_path = spdk:0000:01:00.0
```

0000:01:00.0は、上記のlspciコマンドの出力にあるデバイスセレクタです。

ノードごとに複数のSPDKインスタンスを実行するには、各インスタンスが使用するdpdkメモリの量をMB単位で指定する必要があります。

ほとんどの場合、1つのデバイスをデータ、DB、WALに使用することができます。 この戦略を、これらのコンポーネントをコロケーションすると表現します。すべてのIOがSPDKを通じて発行されるように、以下の設定を必ず入力してください。

```
bluestore_block_db_path = ""
bluestore_block_db_size = 0
bluestore_block_wal_path = ""
bluestore_block_wal_size = 0
```

そうでなければ、現在の実装では、SPDKのマップファイルにカーネルのファイルシステムのシンボルを入力し、カーネルドライバーを使ってDB/WALのIOを発行することになります。
