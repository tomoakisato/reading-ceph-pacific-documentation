# 412: Snaps — Ceph Documentation

 # Snaps[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/snaps/ "Permalink to this headline")

## Overview[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/snaps/ "Permalink to this headline")

Rados supports two related snapshotting mechanisms:

> 1. _pool snaps_: snapshots are implicitly applied to all objects in a pool
> 2. _self managed snaps_: the user must provide the current _SnapContext_on each write.

These two are mutually exclusive, only one or the other can be used on a particular pool.

The _SnapContext_ is the set of snapshots currently defined for an object as well as the most recent snapshot \(the _seq_\) requested from the mon for sequencing purposes \(a _SnapContext_ with a newer _seq_ is considered to be more recent\).

The difference between _pool snaps_ and _self managed snaps_ from the OSD’s point of view lies in whether the _SnapContext_ comes to the OSD via the client’s MOSDOp or via the most recent OSDMap.

See OSD::make\_writeable

## Ondisk Structures[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/snaps/ "Permalink to this headline")

Each object has in the PG collection a _head_ object \(or _snapdir_, which we will come to shortly\) and possibly a set of _clone_ objects. Each hobject\_t has a snap field. For the _head_ \(the only writeable version of an object\), the snap field is set to CEPH\_NOSNAP. For the _clones_, the snap field is set to the _seq_ of the _SnapContext_ at their creation. When the OSD services a write, it first checks whether the most recent_clone_ is tagged with a snapid prior to the most recent snap represented in the _SnapContext_. If so, at least one snapshot has occurred between the time of the write and the time of the last clone. Therefore, prior to performing the mutation, the OSD creates a new clone for servicing reads on snaps between the snapid of the last clone and the most recent snapid.

The _head_ object contains a _SnapSet_ encoded in an attribute, which tracks

> 1. The full set of snaps defined for the object
> 2. The full set of clones which currently exist
> 3. Overlapping intervals between clones for tracking space usage
> 4. Clone size

If the _head_ is deleted while there are still clones, a _snapdir_ object is created instead to house the _SnapSet_.

Additionally, the _object\_info\_t_ on each clone includes a vector of snaps for which clone is defined.

## Snap Removal[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/snaps/ "Permalink to this headline")

To remove a snapshot, a request is made to the _Monitor_ cluster to add the snapshot id to the list of purged snaps \(or to remove it from the set of pool snaps in the case of _pool snaps_\). In either case, the _PG_ adds the snap to its _snap\_trimq_ for trimming.

A clone can be removed when all of its snaps have been removed. In order to determine which clones might need to be removed upon snap removal, we maintain a mapping from snap to _hobject\_t_ using the_SnapMapper_.

See PrimaryLogPG::SnapTrimmer, SnapMapper

This trimming is performed asynchronously by the snap\_trim\_wq while the PG is clean and not scrubbing.

> 1. The next snap in PG::snap\_trimq is selected for trimming
> 2. We determine the next object for trimming out of PG::snap\_mapper. For each object, we create a log entry and repop updating the object info and the snap set \(including adjusting the overlaps\). If the object is a clone which no longer belongs to any live snapshots, it is removed here. \(See PrimaryLogPG::trim\_object\(\) when new\_snaps is empty.\)
> 3. We also locally update our _SnapMapper_ instance with the object’s new snaps.
> 4. The log entry containing the modification of the object also contains the new set of snaps, which the replica uses to update its own _SnapMapper_ instance.
> 5. The primary shares the info with the replica, which persists the new set of purged\_snaps along with the rest of the info.

## Recovery[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/snaps/ "Permalink to this headline")

Because the trim operations are implemented using repops and log entries, normal PG peering and recovery maintain the snap trimmer operations with the caveat that push and removal operations need to update the local_SnapMapper_ instance. If the purged\_snaps update is lost, we merely retrim a now empty snap.

## SnapMapper[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/snaps/ "Permalink to this headline")

_SnapMapper_ is implemented on top of map\_cacher\<string, bufferlist\>, which provides an interface over a backing store such as the file system with async transactions. While transactions are incomplete, the map\_cacher instance buffers unstable keys allowing consistent access without having to flush the filestore. _SnapMapper_ provides two mappings:

> 1. hobject\_t \-\> set\<snapid\_t\>: stores the set of snaps for each clone object
> 2. snapid\_t \-\> hobject\_t: stores the set of hobjects with the snapshot as one of its snaps

Assumption: there are lots of hobjects and relatively few snaps. The first encoding has a stringification of the object as the key and an encoding of the set of snaps as a value. The second mapping, because there might be many hobjects for a single snap, is stored as a collection of keys of the form stringify\(snap\)\_stringify\(object\) such that stringify\(snap\) is constant length. These keys have a bufferlist encoding pair\<snapid, hobject\_t\> as a value. Thus, creating or trimming a single object does not involve reading all objects for any snap. Additionally, upon construction, the _SnapMapper_ is provided with a mask for filtering the objects in the single SnapMapper keyspace belonging to that PG.

## Split[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/snaps/ "Permalink to this headline")

The snapid\_t \-\> hobject\_t key entries are arranged such that for any PG, up to 8 prefixes need to be checked to determine all hobjects in a particular snap for a particular PG. Upon split, the prefixes to check on the parent are adjusted such that only the objects remaining in the PG will be visible. The children will immediately have the correct mapping.
