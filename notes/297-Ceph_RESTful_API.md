# 297: Ceph RESTful API

 # Ceph RESTful API[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

## Introduction[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

The **Ceph RESTful API** \(henceforth **Ceph API**\) is provided by the[Ceph Dashboard](https://docs.ceph.com/en/pacific/mgr/ceph_api/) module. The Ceph API service is available at the same URL as the regular Ceph Dashboard, under the`/api` base path \(please refer to [Host Name and Port](https://docs.ceph.com/en/pacific/mgr/ceph_api/)\):

```
http://<server_addr>:<server_port>/api
```

or, if HTTPS is enabled \(please refer to [SSL/TLS Support](https://docs.ceph.com/en/pacific/mgr/ceph_api/)\):

```
https://<server_addr>:<ssl_server_port>/api
```

The Ceph API leverages the following standards:

* [HTTP 1.1](https://docs.ceph.com/en/pacific/mgr/ceph_api/) for API syntax and semantics,
* [JSON](https://docs.ceph.com/en/pacific/mgr/ceph_api/) for content encoding,
* [HTTP Content Negotiation](https://docs.ceph.com/en/pacific/mgr/ceph_api/) and [MIME](https://docs.ceph.com/en/pacific/mgr/ceph_api/) for versioning,
* [OAuth 2.0](https://docs.ceph.com/en/pacific/mgr/ceph_api/) and [JWT](https://docs.ceph.com/en/pacific/mgr/ceph_api/) for authentication and authorization.

Warning:

Some endpoints are still under active development, and should be carefully used since new Ceph releases could bring backward incompatible changes.

## Authentication and Authorization[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

Requests to the Ceph API pass through two access control checkpoints:

* **Authentication**: ensures that the request is performed on behalf of an existing and valid user account.
* **Authorization**: ensures that the previously authenticated user can in fact perform a specific action \(create, read, update or delete\) on the target endpoint.

So, prior to start consuming the Ceph API, a valid JSON Web Token \(JWT\) has to be obtained, and it may then be reused for subsequent requests. The`/api/auth` endpoint will provide the valid token:

```
$ curl -X POST "https://example.com:8443/api/auth" 
  -H  "Accept: application/vnd.ceph.api.v1.0+json" 
  -H  "Content-Type: application/json" 
  -d '{"username": <username>, "password": <password>}'

{ "token": "<redacted_token>", ...}
```

The token obtained must be passed together with every API request in the`Authorization` HTTP header:

```
curl -H "Authorization: Bearer <token>" ...
```

Authentication and authorization can be further configured from the Ceph CLI, the Ceph\-Dashboard UI and the Ceph API itself \(please refer to[User and Role Management](https://docs.ceph.com/en/pacific/mgr/ceph_api/)\).

## Versioning[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

One of the main goals of the Ceph API is to keep a stable interface. For this purpose, Ceph API is built upon the following principles:

* **Mandatory**: in order to avoid implicit defaults, all endpoints require an explicit default version \(starting with `1.0`\).
* **Per\-endpoint**: as this API wraps many different Ceph components, this allows for a finer\-grained change control.
    * **Content/MIME Type**: the version expected from a specific endpoint is stated by the `Accept: application/vnd.ceph.api.v<major>.<minor>+json` HTTP header. If the current Ceph API server is not able to address that specific major version, a [415 \- Unsupported Media Type](https://docs.ceph.com/en/pacific/mgr/ceph_api/) response will be returned.
* **Semantic Versioning**: with a `major.minor` version:
    * Major changes are backward incompatible: they might result in non\-additive changes to the request and/or response formats of a specific endpoint.
    * Minor changes are backward/forward compatible: they basically consists of additive changes to the request or response formats of a specific endpoint.

An example:

```
$ curl -X GET "https://example.com:8443/api/osd" 
  -H  "Accept: application/vnd.ceph.api.v1.0+json" 
  -H  "Authorization: Bearer <token>"
```

## Specification[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

### Auth[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`POST ``/api/auth`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Example request:**

```
POST /api/auth HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "password": "string",
    "username": "string"
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/auth/check`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Check token Authentication**

Query Parameters
* **token** \(_string_\) – Authentication Token \(Required\)

**Example request:**

```
POST /api/auth/check?token=string HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "token": "string"
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/auth/logout`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### Cephfs[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/cephfs`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Example request:**

```
GET /api/cephfs HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/cephfs/{fs_id}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **fs\_id** \(_string_\) – 

**Example request:**

```
GET /api/cephfs/{fs_id} HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`DELETE ``/api/cephfs/{fs_id}/client/{client_id}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **fs\_id** \(_string_\) – 
* **client\_id** \(_string_\) – 

Status Codes
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [204 No Content](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource deleted.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/cephfs/{fs_id}/clients`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **fs\_id** \(_string_\) – 

**Example request:**

```
GET /api/cephfs/{fs_id}/clients HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/cephfs/{fs_id}/get_root_directory`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")> The root directory that can’t be fetched using ls\_dir \(api\). :param fs\_id: The filesystem identifier. :return: The root directory :rtype: dict

Parameters
* **fs\_id** \(_string_\) – 

**Example request:**

```
GET /api/cephfs/{fs_id}/get_root_directory HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/cephfs/{fs_id}/ls_dir`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")> List directories of specified path. :param fs\_id: The filesystem identifier. :param path: The path where to start listing the directory content. Defaults to ‘/’ if not set. :type path: str | bytes :param depth: The number of steps to go down the directory tree. :type depth: int | str :return: The names of the directories below the specified path. :rtype: list

Parameters
* **fs\_id** \(_string_\) – 

Query Parameters
* **path** \(_string_\) – 
* **depth** \(_integer_\) – 

**Example request:**

```
GET /api/cephfs/{fs_id}/ls_dir HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/cephfs/{fs_id}/mds_counters`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **fs\_id** \(_string_\) – 

Query Parameters
* **counters** \(_integer_\) – 

**Example request:**

```
GET /api/cephfs/{fs_id}/mds_counters HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/cephfs/{fs_id}/quota`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Get Cephfs Quotas of the specified path**

> Get the quotas of the specified path. :param fs\_id: The filesystem identifier. :param path: The path of the directory/file. :return: Returns a dictionary containing ‘max\_bytes’ and ‘max\_files’. :rtype: dict

Parameters
* **fs\_id** \(_string_\) – File System Identifier

Query Parameters
* **path** \(_string_\) – File System Path \(Required\)

**Example request:**

```
GET /api/cephfs/{fs_id}/quota?path=string HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`PUT ``/api/cephfs/{fs_id}/quota`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")> Set the quotas of the specified path. :param fs\_id: The filesystem identifier. :param path: The path of the directory/file. :param max\_bytes: The byte limit. :param max\_files: The file limit.

Parameters
* **fs\_id** \(_string_\) – 

**Example request:**

```
PUT /api/cephfs/{fs_id}/quota HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "max_bytes": "string",
    "max_files": "string",
    "path": "string"
}
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource updated.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`DELETE ``/api/cephfs/{fs_id}/snapshot`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")> Remove a snapshot. :param fs\_id: The filesystem identifier. :param path: The path of the directory. :param name: The name of the snapshot.

Parameters
* **fs\_id** \(_string_\) – 

Query Parameters
* **path** \(_string_\) – \(Required\)
* **name** \(_string_\) – \(Required\)

Status Codes
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [204 No Content](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource deleted.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/cephfs/{fs_id}/snapshot`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")> Create a snapshot. :param fs\_id: The filesystem identifier. :param path: The path of the directory. :param name: The name of the snapshot. If not specified, a name using the current time in RFC3339 UTC format will be generated. :return: The name of the snapshot. :rtype: str

Parameters
* **fs\_id** \(_string_\) – 

**Example request:**

```
POST /api/cephfs/{fs_id}/snapshot HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "name": "string",
    "path": "string"
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`DELETE ``/api/cephfs/{fs_id}/tree`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")> Remove a directory. :param fs\_id: The filesystem identifier. :param path: The path of the directory.

Parameters
* **fs\_id** \(_string_\) – 

Query Parameters
* **path** \(_string_\) – \(Required\)

Status Codes
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [204 No Content](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource deleted.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/cephfs/{fs_id}/tree`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")> Create a directory. :param fs\_id: The filesystem identifier. :param path: The path of the directory.

Parameters
* **fs\_id** \(_string_\) – 

**Example request:**

```
POST /api/cephfs/{fs_id}/tree HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "path": "string"
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### Cluster[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/cluster`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Get the cluster status**

**Example request:**

```
GET /api/cluster HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`PUT ``/api/cluster`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Update the cluster status**

**Example request:**

```
PUT /api/cluster HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "status": "string"
}
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource updated.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### ClusterConfiguration[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/cluster_conf`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Example request:**

```
GET /api/cluster_conf HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/cluster_conf`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Example request:**

```
POST /api/cluster_conf HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "name": "string",
    "value": "string"
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`PUT ``/api/cluster_conf`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Example request:**

```
PUT /api/cluster_conf HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "options": "string"
}
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource updated.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/cluster_conf/filter`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Get Cluster Configuration by name**

Query Parameters
* **names** \(_string_\) – Config option names

**Example request:**

```
GET /api/cluster_conf/filter HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`DELETE ``/api/cluster_conf/{name}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **name** \(_string_\) – 

Query Parameters
* **section** \(_string_\) – \(Required\)

Status Codes
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [204 No Content](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource deleted.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/cluster_conf/{name}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **name** \(_string_\) – 

**Example request:**

```
GET /api/cluster_conf/{name} HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### CrushRule[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/crush_rule`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**List Crush Rule Configuration**

**Example request:**

```
GET /api/crush_rule HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/crush_rule`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Example request:**

```
POST /api/crush_rule HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "device_class": "string",
    "failure_domain": "string",
    "name": "string",
    "root": "string"
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`DELETE ``/api/crush_rule/{name}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **name** \(_string_\) – 

Status Codes
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [204 No Content](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource deleted.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/crush_rule/{name}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **name** \(_string_\) – 

**Example request:**

```
GET /api/crush_rule/{name} HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### ErasureCodeProfile[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/erasure_code_profile`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**List Erasure Code Profile Information**

**Example request:**

```
GET /api/erasure_code_profile HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/erasure_code_profile`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Example request:**

```
POST /api/erasure_code_profile HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "name": "string"
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`DELETE ``/api/erasure_code_profile/{name}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **name** \(_string_\) – 

Status Codes
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [204 No Content](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource deleted.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/erasure_code_profile/{name}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **name** \(_string_\) – 

**Example request:**

```
GET /api/erasure_code_profile/{name} HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### FeatureTogglesEndpoint[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/feature_toggles`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Get List Of Features**

**Example request:**

```
GET /api/feature_toggles HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### Grafana[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`POST ``/api/grafana/dashboards`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/grafana/url`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**List Grafana URL Instance**

**Example request:**

```
GET /api/grafana/url HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/grafana/validation/{params}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **params** \(_string_\) – 

**Example request:**

```
GET /api/grafana/validation/{params} HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### Health[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/health/full`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Example request:**

```
GET /api/health/full HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/health/minimal`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Get Cluster’s minimal health report**

**Example request:**

```
GET /api/health/minimal HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### Host[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/host`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**List Host Specifications**

Query Parameters
* **sources** \(_string_\) – Host Sources
* **facts** \(_boolean_\) – Host Facts

**Example request:**

```
GET /api/host HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/host`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Example request:**

```
POST /api/host HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "addr": "string",
    "hostname": "string",
    "labels": [
        "string"
    ],
    "status": "string"
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`DELETE ``/api/host/{hostname}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **hostname** \(_string_\) – 

Status Codes
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [204 No Content](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource deleted.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/host/{hostname}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")> Get the specified host. :raises: cherrypy.HTTPError: If host not found.

Parameters
* **hostname** \(_string_\) – 

**Example request:**

```
GET /api/host/{hostname} HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`PUT ``/api/host/{hostname}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")> Update the specified host. Note, this is only supported when Ceph Orchestrator is enabled. :param hostname: The name of the host to be processed. :param update\_labels: To update the labels. :param labels: List of labels. :param maintenance: Enter/Exit maintenance mode. :param force: Force enter maintenance mode.

Parameters
* **hostname** \(_string_\) – Hostname

**Example request:**

```
PUT /api/host/{hostname} HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "force": true,
    "labels": [
        "string"
    ],
    "maintenance": true,
    "update_labels": true
}
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource updated.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/host/{hostname}/daemons`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **hostname** \(_string_\) – 

**Example request:**

```
GET /api/host/{hostname}/daemons HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/host/{hostname}/devices`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **hostname** \(_string_\) – 

**Example request:**

```
GET /api/host/{hostname}/devices HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/host/{hostname}/identify_device`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")> Identify a device by switching on the device light for N seconds. :param hostname: The hostname of the device to process. :param device: The device identifier to process, e.g. `/dev/dm-0` or`ABC1234DEF567-1R1234_ABC8DE0Q`. :param duration: The duration in seconds how long the LED should flash.

Parameters
* **hostname** \(_string_\) – 

**Example request:**

```
POST /api/host/{hostname}/identify_device HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "device": "string",
    "duration": "string"
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/host/{hostname}/inventory`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Get inventory of a host**

Parameters
* **hostname** \(_string_\) – Hostname

Query Parameters
* **refresh** \(_string_\) – Trigger asynchronous refresh

**Example request:**

```
GET /api/host/{hostname}/inventory HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/host/{hostname}/smart`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **hostname** \(_string_\) – 

**Example request:**

```
GET /api/host/{hostname}/smart HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### Iscsi[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/iscsi/discoveryauth`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Get Iscsi discoveryauth Details**

**Example request:**

```
GET /api/iscsi/discoveryauth HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`PUT ``/api/iscsi/discoveryauth`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Set Iscsi discoveryauth**

Query Parameters
* **user** \(_string_\) – Username \(Required\)
* **password** \(_string_\) – Password \(Required\)
* **mutual\_user** \(_string_\) – Mutual UserName \(Required\)
* **mutual\_password** \(_string_\) – Mutual Password \(Required\)

**Example request:**

```
PUT /api/iscsi/discoveryauth?user=string&password=string&mutual_user=string&mutual_password=string HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "mutual_password": "string",
    "mutual_user": "string",
    "password": "string",
    "user": "string"
}
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource updated.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### IscsiTarget[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/iscsi/target`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Example request:**

```
GET /api/iscsi/target HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/iscsi/target`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Example request:**

```
POST /api/iscsi/target HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "acl_enabled": "string",
    "auth": "string",
    "clients": "string",
    "disks": "string",
    "groups": "string",
    "portals": "string",
    "target_controls": "string",
    "target_iqn": "string"
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`DELETE ``/api/iscsi/target/{target_iqn}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **target\_iqn** \(_string_\) – 

Status Codes
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [204 No Content](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource deleted.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/iscsi/target/{target_iqn}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **target\_iqn** \(_string_\) – 

**Example request:**

```
GET /api/iscsi/target/{target_iqn} HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`PUT ``/api/iscsi/target/{target_iqn}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **target\_iqn** \(_string_\) – 

**Example request:**

```
PUT /api/iscsi/target/{target_iqn} HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "acl_enabled": "string",
    "auth": "string",
    "clients": "string",
    "disks": "string",
    "groups": "string",
    "new_target_iqn": "string",
    "portals": "string",
    "target_controls": "string"
}
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource updated.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### Logs[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/logs/all`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Display Logs Configuration**

**Example request:**

```
GET /api/logs/all HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### MdsPerfCounter[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/perf_counters/mds/{service_id}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **service\_id** \(_string_\) – 

**Example request:**

```
GET /api/perf_counters/mds/{service_id} HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### MgrModule[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/mgr/module`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**List Mgr modules**

> Get the list of managed modules. :return: A list of objects with the fields ‘enabled’, ‘name’ and ‘options’. :rtype: list

**Example request:**

```
GET /api/mgr/module HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/mgr/module/{module_name}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")> Retrieve the values of the persistent configuration settings. :param module\_name: The name of the Ceph Mgr module. :type module\_name: str :return: The values of the module options. :rtype: dict

Parameters
* **module\_name** \(_string_\) – 

**Example request:**

```
GET /api/mgr/module/{module_name} HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`PUT ``/api/mgr/module/{module_name}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")> Set the values of the persistent configuration settings. :param module\_name: The name of the Ceph Mgr module. :type module\_name: str :param config: The values of the module options to be stored. :type config: dict

Parameters
* **module\_name** \(_string_\) – 

**Example request:**

```
PUT /api/mgr/module/{module_name} HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "config": "string"
}
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource updated.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/mgr/module/{module_name}/disable`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")> Disable the specified Ceph Mgr module. :param module\_name: The name of the Ceph Mgr module. :type module\_name: str

Parameters
* **module\_name** \(_string_\) – 

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/mgr/module/{module_name}/enable`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")> Enable the specified Ceph Mgr module. :param module\_name: The name of the Ceph Mgr module. :type module\_name: str

Parameters
* **module\_name** \(_string_\) – 

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/mgr/module/{module_name}/options`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")> Get the module options of the specified Ceph Mgr module. :param module\_name: The name of the Ceph Mgr module. :type module\_name: str :return: The module options as list of dicts. :rtype: list

Parameters
* **module\_name** \(_string_\) – 

**Example request:**

```
GET /api/mgr/module/{module_name}/options HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### MgrPerfCounter[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/perf_counters/mgr/{service_id}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **service\_id** \(_string_\) – 

**Example request:**

```
GET /api/perf_counters/mgr/{service_id} HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### MonPerfCounter[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/perf_counters/mon/{service_id}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **service\_id** \(_string_\) – 

**Example request:**

```
GET /api/perf_counters/mon/{service_id} HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### Monitor[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/monitor`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Get Monitor Details**

**Example request:**

```
GET /api/monitor HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### NFS\-Ganesha[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/nfs-ganesha/cluster`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Example request:**

```
GET /api/nfs-ganesha/cluster HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/nfs-ganesha/export`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**List all NFS\-Ganesha exports**

**Example request:**

```
GET /api/nfs-ganesha/export HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/nfs-ganesha/export`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Creates a new NFS\-Ganesha export**

**Example request:**

```
POST /api/nfs-ganesha/export HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "access_type": "string",
    "clients": [
        {
            "access_type": "string",
            "addresses": [
                "string"
            ],
            "squash": "string"
        }
    ],
    "cluster_id": "string",
    "fsal": {
        "fs_name": "string",
        "name": "string",
        "sec_label_xattr": "string"
    },
    "path": "string",
    "protocols": [
        1
    ],
    "pseudo": "string",
    "security_label": "string",
    "squash": "string",
    "transports": [
        "string"
    ]
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`DELETE ``/api/nfs-ganesha/export/{cluster_id}/{export_id}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Deletes an NFS\-Ganesha export**

Parameters
* **cluster\_id** \(_string_\) – Cluster identifier
* **export\_id** \(_integer_\) – Export ID

Status Codes
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [204 No Content](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource deleted.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/nfs-ganesha/export/{cluster_id}/{export_id}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Get an NFS\-Ganesha export**

Parameters
* **cluster\_id** \(_string_\) – Cluster identifier
* **export\_id** \(_string_\) – Export ID

**Example request:**

```
GET /api/nfs-ganesha/export/{cluster_id}/{export_id} HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`PUT ``/api/nfs-ganesha/export/{cluster_id}/{export_id}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Updates an NFS\-Ganesha export**

Parameters
* **cluster\_id** \(_string_\) – Cluster identifier
* **export\_id** \(_integer_\) – Export ID

**Example request:**

```
PUT /api/nfs-ganesha/export/{cluster_id}/{export_id} HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "access_type": "string",
    "clients": [
        {
            "access_type": "string",
            "addresses": [
                "string"
            ],
            "squash": "string"
        }
    ],
    "fsal": {
        "fs_name": "string",
        "name": "string",
        "sec_label_xattr": "string"
    },
    "path": "string",
    "protocols": [
        1
    ],
    "pseudo": "string",
    "security_label": "string",
    "squash": "string",
    "transports": [
        "string"
    ]
}
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource updated.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/nfs-ganesha/status`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Status of NFS\-Ganesha management feature**

**Example request:**

```
GET /api/nfs-ganesha/status HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### OSD[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/osd`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Example request:**

```
GET /api/osd HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/osd`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Example request:**

```
POST /api/osd HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "data": "string",
    "method": "string",
    "tracking_id": "string"
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/osd/flags`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Display OSD Flags**

**Example request:**

```
GET /api/osd/flags HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`PUT ``/api/osd/flags`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Sets OSD flags for the entire cluster.**

> The recovery\_deletes, sortbitwise and pglog\_hardlimit flags cannot be unset.purged\_snapshots cannot even be set. It is therefore required to at least include those four flags for a successful operation.

**Example request:**

```
PUT /api/osd/flags HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "flags": [
        "string"
    ]
}
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource updated.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/osd/flags/individual`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Displays individual OSD flags**

**Example request:**

```
GET /api/osd/flags/individual HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`PUT ``/api/osd/flags/individual`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Sets OSD flags for a subset of individual OSDs.**

> Updates flags \(noout, noin, nodown, noup\) for an individual subset of OSDs.

**Example request:**

```
PUT /api/osd/flags/individual HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "flags": {
        "nodown": true,
        "noin": true,
        "noout": true,
        "noup": true
    },
    "ids": [
        1
    ]
}
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource updated.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/osd/safe_to_delete`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")> type idsint|\[int\]

Query Parameters
* **svc\_ids** \(_string_\) – \(Required\)

**Example request:**

```
GET /api/osd/safe_to_delete?svc_ids=string HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/osd/safe_to_destroy`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Check If OSD is Safe to Destroy**

> type idsint|\[int\]

Query Parameters
* **ids** \(_string_\) – OSD Service Identifier \(Required\)

**Example request:**

```
GET /api/osd/safe_to_destroy?ids=string HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`DELETE ``/api/osd/{svc_id}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **svc\_id** \(_string_\) – 

Query Parameters
* **preserve\_id** \(_string_\) – 
* **force** \(_string_\) – 

Status Codes
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [204 No Content](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource deleted.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/osd/{svc_id}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")> Returns collected data about an OSD.
> 
> returnReturns the requested data.

Parameters
* **svc\_id** \(_string_\) – 

**Example request:**

```
GET /api/osd/{svc_id} HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`PUT ``/api/osd/{svc_id}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **svc\_id** \(_string_\) – 

**Example request:**

```
PUT /api/osd/{svc_id} HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "device_class": "string"
}
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource updated.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/osd/{svc_id}/destroy`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")> Mark osd as being destroyed. Keeps the ID intact \(allowing reuse\), but removes cephx keys, config\-key data and lockbox keys, rendering data permanently unreadable.
> 
> The osd must be marked down before being destroyed.

Parameters
* **svc\_id** \(_string_\) – 

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/osd/{svc_id}/devices`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **svc\_id** \(_string_\) – 

**Example request:**

```
GET /api/osd/{svc_id}/devices HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/osd/{svc_id}/histogram`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")> returnReturns the histogram data.

Parameters
* **svc\_id** \(_string_\) – 

**Example request:**

```
GET /api/osd/{svc_id}/histogram HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`PUT ``/api/osd/{svc_id}/mark`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Mark OSD flags \(out, in, down, lost, …\)**

> Note: osd must be marked down before marking lost.

Parameters
* **svc\_id** \(_string_\) – SVC ID

**Example request:**

```
PUT /api/osd/{svc_id}/mark HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "action": "string"
}
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource updated.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/osd/{svc_id}/purge`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")> Note: osd must be marked down before removal.

Parameters
* **svc\_id** \(_string_\) – 

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/osd/{svc_id}/reweight`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")> Reweights the OSD temporarily.
> 
> Note that ‘ceph osd reweight’ is not a persistent setting. When an OSD gets marked out, the osd weight will be set to 0. When it gets marked in again, the weight will be changed to 1.
> 
> Because of this ‘ceph osd reweight’ is a temporary solution. You should only use it to keep your cluster running while you’re ordering more hardware.
> 
> 
> * Craig Lewis \([http://lists.ceph.com/pipermail/ceph\-users\-ceph.com/2014\-June/040967.html](https://docs.ceph.com/en/pacific/mgr/ceph_api/)\)

Parameters
* **svc\_id** \(_string_\) – 

**Example request:**

```
POST /api/osd/{svc_id}/reweight HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "weight": "string"
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/osd/{svc_id}/scrub`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **svc\_id** \(_string_\) – 

Query Parameters
* **deep** \(_boolean_\) – 

**Example request:**

```
POST /api/osd/{svc_id}/scrub HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "deep": true
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/osd/{svc_id}/smart`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **svc\_id** \(_string_\) – 

**Example request:**

```
GET /api/osd/{svc_id}/smart HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### Orchestrator[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/orchestrator/status`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Display Orchestrator Status**

**Example request:**

```
GET /api/orchestrator/status HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### OsdPerfCounter[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/perf_counters/osd/{service_id}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **service\_id** \(_string_\) – 

**Example request:**

```
GET /api/perf_counters/osd/{service_id} HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### PerfCounters[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/perf_counters`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Display Perf Counters**

**Example request:**

```
GET /api/perf_counters HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### Pool[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/pool`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Display Pool List**

Query Parameters
* **attrs** \(_string_\) – Pool Attributes
* **stats** \(_boolean_\) – Pool Stats

**Example request:**

```
GET /api/pool HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/pool`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Example request:**

```
POST /api/pool HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "application_metadata": "string",
    "configuration": "string",
    "erasure_code_profile": "string",
    "flags": "string",
    "pg_num": 1,
    "pool": "string",
    "pool_type": "string",
    "rule_name": "string"
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`DELETE ``/api/pool/{pool_name}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **pool\_name** \(_string_\) – 

Status Codes
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [204 No Content](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource deleted.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/pool/{pool_name}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **pool\_name** \(_string_\) – 

Query Parameters
* **attrs** \(_string_\) – 
* **stats** \(_boolean_\) – 

**Example request:**

```
GET /api/pool/{pool_name} HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`PUT ``/api/pool/{pool_name}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **pool\_name** \(_string_\) – 

**Example request:**

```
PUT /api/pool/{pool_name} HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "application_metadata": "string",
    "configuration": "string",
    "flags": "string"
}
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource updated.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/pool/{pool_name}/configuration`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **pool\_name** \(_string_\) – 

**Example request:**

```
GET /api/pool/{pool_name}/configuration HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### Prometheus[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/prometheus`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Example request:**

```
GET /api/prometheus HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/prometheus/rules`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Example request:**

```
GET /api/prometheus/rules HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/prometheus/silence`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`DELETE ``/api/prometheus/silence/{s_id}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **s\_id** \(_string_\) – 

Status Codes
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [204 No Content](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource deleted.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/prometheus/silences`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Example request:**

```
GET /api/prometheus/silences HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### PrometheusNotifications[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/prometheus/notifications`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Example request:**

```
GET /api/prometheus/notifications HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### Rbd[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/block/image`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Display Rbd Images**

Query Parameters
* **pool\_name** \(_string_\) – Pool Name

**Example request:**

```
GET /api/block/image HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/block/image`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Example request:**

```
POST /api/block/image HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "configuration": "string",
    "data_pool": "string",
    "features": "string",
    "name": "string",
    "namespace": "string",
    "obj_size": 1,
    "pool_name": "string",
    "size": 1,
    "stripe_count": 1,
    "stripe_unit": "string"
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/block/image/clone_format_version`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Return the RBD clone format version.

**Example request:**

```
GET /api/block/image/clone_format_version HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/block/image/default_features`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Example request:**

```
GET /api/block/image/default_features HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`DELETE ``/api/block/image/{image_spec}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **image\_spec** \(_string_\) – 

Status Codes
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [204 No Content](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource deleted.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/block/image/{image_spec}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **image\_spec** \(_string_\) – 

**Example request:**

```
GET /api/block/image/{image_spec} HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`PUT ``/api/block/image/{image_spec}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **image\_spec** \(_string_\) – 

**Example request:**

```
PUT /api/block/image/{image_spec} HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "configuration": "string",
    "features": "string",
    "name": "string",
    "size": 1
}
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource updated.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/block/image/{image_spec}/copy`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **image\_spec** \(_string_\) – 

**Example request:**

```
POST /api/block/image/{image_spec}/copy HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "configuration": "string",
    "data_pool": "string",
    "dest_image_name": "string",
    "dest_namespace": "string",
    "dest_pool_name": "string",
    "features": "string",
    "obj_size": 1,
    "snapshot_name": "string",
    "stripe_count": 1,
    "stripe_unit": "string"
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/block/image/{image_spec}/flatten`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **image\_spec** \(_string_\) – 

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/block/image/{image_spec}/move_trash`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Move an image to the trash.Images, even ones actively in\-use by clones, can be moved to the trash and deleted at a later time.

Parameters
* **image\_spec** \(_string_\) – 

**Example request:**

```
POST /api/block/image/{image_spec}/move_trash HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "delay": 1
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### RbdMirroring[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/block/mirroring/site_name`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Display Rbd Mirroring sitename**

**Example request:**

```
GET /api/block/mirroring/site_name HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`PUT ``/api/block/mirroring/site_name`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Example request:**

```
PUT /api/block/mirroring/site_name HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "site_name": "string"
}
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource updated.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### RbdMirroringPoolBootstrap[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`POST ``/api/block/mirroring/pool/{pool_name}/bootstrap/peer`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **pool\_name** \(_string_\) – 

**Example request:**

```
POST /api/block/mirroring/pool/{pool_name}/bootstrap/peer HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "direction": "string",
    "token": "string"
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/block/mirroring/pool/{pool_name}/bootstrap/token`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **pool\_name** \(_string_\) – 

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### RbdMirroringPoolMode[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/block/mirroring/pool/{pool_name}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Display Rbd Mirroring Summary**

Parameters
* **pool\_name** \(_string_\) – Pool Name

**Example request:**

```
GET /api/block/mirroring/pool/{pool_name} HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`PUT ``/api/block/mirroring/pool/{pool_name}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **pool\_name** \(_string_\) – 

**Example request:**

```
PUT /api/block/mirroring/pool/{pool_name} HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "mirror_mode": "string"
}
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource updated.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### RbdMirroringPoolPeer[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/block/mirroring/pool/{pool_name}/peer`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **pool\_name** \(_string_\) – 

**Example request:**

```
GET /api/block/mirroring/pool/{pool_name}/peer HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/block/mirroring/pool/{pool_name}/peer`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **pool\_name** \(_string_\) – 

**Example request:**

```
POST /api/block/mirroring/pool/{pool_name}/peer HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "client_id": "string",
    "cluster_name": "string",
    "key": "string",
    "mon_host": "string"
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`DELETE ``/api/block/mirroring/pool/{pool_name}/peer/{peer_uuid}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **pool\_name** \(_string_\) – 
* **peer\_uuid** \(_string_\) – 

Status Codes
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [204 No Content](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource deleted.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/block/mirroring/pool/{pool_name}/peer/{peer_uuid}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **pool\_name** \(_string_\) – 
* **peer\_uuid** \(_string_\) – 

**Example request:**

```
GET /api/block/mirroring/pool/{pool_name}/peer/{peer_uuid} HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`PUT ``/api/block/mirroring/pool/{pool_name}/peer/{peer_uuid}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **pool\_name** \(_string_\) – 
* **peer\_uuid** \(_string_\) – 

**Example request:**

```
PUT /api/block/mirroring/pool/{pool_name}/peer/{peer_uuid} HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "client_id": "string",
    "cluster_name": "string",
    "key": "string",
    "mon_host": "string"
}
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource updated.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### RbdMirroringSummary[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/block/mirroring/summary`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Display Rbd Mirroring Summary**

**Example request:**

```
GET /api/block/mirroring/summary HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### RbdNamespace[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/block/pool/{pool_name}/namespace`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **pool\_name** \(_string_\) – 

**Example request:**

```
GET /api/block/pool/{pool_name}/namespace HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/block/pool/{pool_name}/namespace`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **pool\_name** \(_string_\) – 

**Example request:**

```
POST /api/block/pool/{pool_name}/namespace HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "namespace": "string"
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`DELETE ``/api/block/pool/{pool_name}/namespace/{namespace}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **pool\_name** \(_string_\) – 
* **namespace** \(_string_\) – 

Status Codes
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [204 No Content](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource deleted.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### RbdSnapshot[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`POST ``/api/block/image/{image_spec}/snap`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **image\_spec** \(_string_\) – 

**Example request:**

```
POST /api/block/image/{image_spec}/snap HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "snapshot_name": "string"
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`DELETE ``/api/block/image/{image_spec}/snap/{snapshot_name}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **image\_spec** \(_string_\) – 
* **snapshot\_name** \(_string_\) – 

Status Codes
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [204 No Content](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource deleted.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`PUT ``/api/block/image/{image_spec}/snap/{snapshot_name}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **image\_spec** \(_string_\) – 
* **snapshot\_name** \(_string_\) – 

**Example request:**

```
PUT /api/block/image/{image_spec}/snap/{snapshot_name} HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "is_protected": true,
    "new_snap_name": "string"
}
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource updated.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/block/image/{image_spec}/snap/{snapshot_name}/clone`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")> Clones a snapshot to an image

Parameters
* **image\_spec** \(_string_\) – 
* **snapshot\_name** \(_string_\) – 

**Example request:**

```
POST /api/block/image/{image_spec}/snap/{snapshot_name}/clone HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "child_image_name": "string",
    "child_namespace": "string",
    "child_pool_name": "string",
    "configuration": "string",
    "data_pool": "string",
    "features": "string",
    "obj_size": 1,
    "stripe_count": 1,
    "stripe_unit": "string"
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/block/image/{image_spec}/snap/{snapshot_name}/rollback`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **image\_spec** \(_string_\) – 
* **snapshot\_name** \(_string_\) – 

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### RbdTrash[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/block/image/trash`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Get RBD Trash Details by pool name**

List all entries from trash.

Query Parameters
* **pool\_name** \(_string_\) – Name of the pool

**Example request:**

```
GET /api/block/image/trash HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/block/image/trash/purge`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Remove all expired images from trash.

Query Parameters
* **pool\_name** \(_string_\) – 

**Example request:**

```
POST /api/block/image/trash/purge HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "pool_name": "string"
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`DELETE ``/api/block/image/trash/{image_id_spec}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Delete an image from trash.If image deferment time has not expired you can not removed it unless use force. But an actively in\-use by clones or has snapshots can not be removed.

Parameters
* **image\_id\_spec** \(_string_\) – 

Query Parameters
* **force** \(_boolean_\) – 

Status Codes
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [204 No Content](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource deleted.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/block/image/trash/{image_id_spec}/restore`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Restore an image from trash.

Parameters
* **image\_id\_spec** \(_string_\) – 

**Example request:**

```
POST /api/block/image/trash/{image_id_spec}/restore HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "new_image_name": "string"
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### Rgw[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/rgw/status`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Display RGW Status**

**Example request:**

```
GET /api/rgw/status HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### RgwBucket[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/rgw/bucket`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Query Parameters
* **stats** \(_boolean_\) – 
* **daemon\_name** \(_string_\) – 
* **uid** \(_string_\) – 

**Example request:**

```
GET /api/rgw/bucket HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/rgw/bucket`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Example request:**

```
POST /api/rgw/bucket HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "bucket": "string",
    "daemon_name": "string",
    "lock_enabled": "string",
    "lock_mode": "string",
    "lock_retention_period_days": "string",
    "lock_retention_period_years": "string",
    "placement_target": "string",
    "uid": "string",
    "zonegroup": "string"
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`DELETE ``/api/rgw/bucket/{bucket}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **bucket** \(_string_\) – 

Query Parameters
* **purge\_objects** \(_string_\) – 
* **daemon\_name** \(_string_\) – 

Status Codes
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [204 No Content](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource deleted.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/rgw/bucket/{bucket}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **bucket** \(_string_\) – 

Query Parameters
* **daemon\_name** \(_string_\) – 

**Example request:**

```
GET /api/rgw/bucket/{bucket} HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`PUT ``/api/rgw/bucket/{bucket}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **bucket** \(_string_\) – 

**Example request:**

```
PUT /api/rgw/bucket/{bucket} HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "bucket_id": "string",
    "daemon_name": "string",
    "lock_mode": "string",
    "lock_retention_period_days": "string",
    "lock_retention_period_years": "string",
    "mfa_delete": "string",
    "mfa_token_pin": "string",
    "mfa_token_serial": "string",
    "uid": "string",
    "versioning_state": "string"
}
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource updated.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### RgwDaemon[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/rgw/daemon`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Display RGW Daemons**

**Example request:**

```
GET /api/rgw/daemon HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/rgw/daemon/{svc_id}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **svc\_id** \(_string_\) – 

**Example request:**

```
GET /api/rgw/daemon/{svc_id} HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### RgwMirrorPerfCounter[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/perf_counters/rbd-mirror/{service_id}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **service\_id** \(_string_\) – 

**Example request:**

```
GET /api/perf_counters/rbd-mirror/{service_id} HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### RgwPerfCounter[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/perf_counters/rgw/{service_id}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **service\_id** \(_string_\) – 

**Example request:**

```
GET /api/perf_counters/rgw/{service_id} HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### RgwSite[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/rgw/site`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Query Parameters
* **query** \(_string_\) – 
* **daemon\_name** \(_string_\) – 

**Example request:**

```
GET /api/rgw/site HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### RgwUser[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/rgw/user`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Display RGW Users**

Query Parameters
* **daemon\_name** \(_string_\) – 

**Example request:**

```
GET /api/rgw/user HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/rgw/user`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Example request:**

```
POST /api/rgw/user HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "access_key": "string",
    "daemon_name": "string",
    "display_name": "string",
    "email": "string",
    "generate_key": "string",
    "max_buckets": "string",
    "secret_key": "string",
    "suspended": "string",
    "uid": "string"
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/rgw/user/get_emails`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Query Parameters
* **daemon\_name** \(_string_\) – 

**Example request:**

```
GET /api/rgw/user/get_emails HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`DELETE ``/api/rgw/user/{uid}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **uid** \(_string_\) – 

Query Parameters
* **daemon\_name** \(_string_\) – 

Status Codes
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [204 No Content](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource deleted.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/rgw/user/{uid}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **uid** \(_string_\) – 

Query Parameters
* **daemon\_name** \(_string_\) – 
* **stats** \(_boolean_\) – 

**Example request:**

```
GET /api/rgw/user/{uid} HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`PUT ``/api/rgw/user/{uid}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **uid** \(_string_\) – 

**Example request:**

```
PUT /api/rgw/user/{uid} HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "daemon_name": "string",
    "display_name": "string",
    "email": "string",
    "max_buckets": "string",
    "suspended": "string"
}
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource updated.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`DELETE ``/api/rgw/user/{uid}/capability`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **uid** \(_string_\) – 

Query Parameters
* **type** \(_string_\) – \(Required\)
* **perm** \(_string_\) – \(Required\)
* **daemon\_name** \(_string_\) – 

Status Codes
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [204 No Content](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource deleted.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/rgw/user/{uid}/capability`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **uid** \(_string_\) – 

**Example request:**

```
POST /api/rgw/user/{uid}/capability HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "daemon_name": "string",
    "perm": "string",
    "type": "string"
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`DELETE ``/api/rgw/user/{uid}/key`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **uid** \(_string_\) – 

Query Parameters
* **key\_type** \(_string_\) – 
* **subuser** \(_string_\) – 
* **access\_key** \(_string_\) – 
* **daemon\_name** \(_string_\) – 

Status Codes
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [204 No Content](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource deleted.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/rgw/user/{uid}/key`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **uid** \(_string_\) – 

**Example request:**

```
POST /api/rgw/user/{uid}/key HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "access_key": "string",
    "daemon_name": "string",
    "generate_key": "string",
    "key_type": "string",
    "secret_key": "string",
    "subuser": "string"
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/rgw/user/{uid}/quota`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **uid** \(_string_\) – 

Query Parameters
* **daemon\_name** \(_string_\) – 

**Example request:**

```
GET /api/rgw/user/{uid}/quota HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`PUT ``/api/rgw/user/{uid}/quota`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **uid** \(_string_\) – 

**Example request:**

```
PUT /api/rgw/user/{uid}/quota HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "daemon_name": "string",
    "enabled": "string",
    "max_objects": "string",
    "max_size_kb": 1,
    "quota_type": "string"
}
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource updated.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/rgw/user/{uid}/subuser`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **uid** \(_string_\) – 

**Example request:**

```
POST /api/rgw/user/{uid}/subuser HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "access": "string",
    "access_key": "string",
    "daemon_name": "string",
    "generate_secret": "string",
    "key_type": "string",
    "secret_key": "string",
    "subuser": "string"
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`DELETE ``/api/rgw/user/{uid}/subuser/{subuser}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")> param purge\_keysSet to False to do not purge the keys. Note, this only works for s3 subusers.

Parameters
* **uid** \(_string_\) – 
* **subuser** \(_string_\) – 

Query Parameters
* **purge\_keys** \(_string_\) – 
* **daemon\_name** \(_string_\) – 

Status Codes
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [204 No Content](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource deleted.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### Role[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/role`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Display Role list**

**Example request:**

```
GET /api/role HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/role`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Example request:**

```
POST /api/role HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "description": "string",
    "name": "string",
    "scopes_permissions": "string"
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`DELETE ``/api/role/{name}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **name** \(_string_\) – 

Status Codes
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [204 No Content](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource deleted.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/role/{name}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **name** \(_string_\) – 

**Example request:**

```
GET /api/role/{name} HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`PUT ``/api/role/{name}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **name** \(_string_\) – 

**Example request:**

```
PUT /api/role/{name} HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "description": "string",
    "scopes_permissions": "string"
}
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource updated.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/role/{name}/clone`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **name** \(_string_\) – 

**Example request:**

```
POST /api/role/{name}/clone HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "new_name": "string"
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### Service[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/service`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Query Parameters
* **service\_name** \(_string_\) – 

**Example request:**

```
GET /api/service HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/service`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")> param service\_specThe service specification as JSON.
> 
> param service\_nameThe service name, e.g. ‘alertmanager’.
> 
> returnNone

**Example request:**

```
POST /api/service HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "service_name": "string",
    "service_spec": "string"
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/service/known_types`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")> Get a list of known service types, e.g. ‘alertmanager’, ‘node\-exporter’, ‘osd’ or ‘rgw’.

**Example request:**

```
GET /api/service/known_types HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`DELETE ``/api/service/{service_name}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")> param service\_nameThe service name, e.g. ‘mds’ or ‘crash.foo’.
> 
> returnNone

Parameters
* **service\_name** \(_string_\) – 

Status Codes
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [204 No Content](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource deleted.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/service/{service_name}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **service\_name** \(_string_\) – 

**Example request:**

```
GET /api/service/{service_name} HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/service/{service_name}/daemons`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **service\_name** \(_string_\) – 

**Example request:**

```
GET /api/service/{service_name}/daemons HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### Settings[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/settings`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Display Settings Information**

> Get the list of available options. :param names: A comma separated list of option names that should be processed. Defaults to `None`. :type names: None|str :return: A list of available options. :rtype: list\[dict\]

Query Parameters
* **names** \(_string_\) – Name of Settings

**Example request:**

```
GET /api/settings HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`PUT ``/api/settings`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource updated.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`DELETE ``/api/settings/{name}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **name** \(_string_\) – 

Status Codes
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [204 No Content](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource deleted.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/settings/{name}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")> Get the given option. :param name: The name of the option. :return: Returns a dict containing the name, type, default value and current value of the given option. :rtype: dict

Parameters
* **name** \(_string_\) – 

**Example request:**

```
GET /api/settings/{name} HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`PUT ``/api/settings/{name}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **name** \(_string_\) – 

**Example request:**

```
PUT /api/settings/{name} HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "value": "string"
}
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource updated.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### Summary[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/summary`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Display Summary**

**Example request:**

```
GET /api/summary HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### Task[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/task`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Display Tasks**

Query Parameters
* **name** \(_string_\) – Task Name

**Example request:**

```
GET /api/task HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### TcmuRunnerPerfCounter[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/perf_counters/tcmu-runner/{service_id}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **service\_id** \(_string_\) – 

**Example request:**

```
GET /api/perf_counters/tcmu-runner/{service_id} HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### Telemetry[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`PUT ``/api/telemetry`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")> Enables or disables sending data collected by the Telemetry module. :param enable: Enable or disable sending data :type enable: bool :param license\_name: License string e.g. ‘sharing\-1\-0’ to make sure the user is aware of and accepts the license for sharing Telemetry data. :type license\_name: string

**Example request:**

```
PUT /api/telemetry HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "enable": true,
    "license_name": "string"
}
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource updated.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/telemetry/report`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Get Detailed Telemetry report**

> Get Ceph and device report data :return: Ceph and device report data :rtype: dict

**Example request:**

```
GET /api/telemetry/report HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### User[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`GET ``/api/user`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Get List Of Users**

**Example request:**

```
GET /api/user HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`POST ``/api/user`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")**Example request:**

```
POST /api/user HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "email": "string",
    "enabled": true,
    "name": "string",
    "password": "string",
    "pwdExpirationDate": "string",
    "pwdUpdateRequired": true,
    "roles": "string",
    "username": "string"
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`DELETE ``/api/user/{username}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **username** \(_string_\) – 

Status Codes
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [204 No Content](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource deleted.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`GET ``/api/user/{username}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **username** \(_string_\) – 

**Example request:**

```
GET /api/user/{username} HTTP/1.1
Host: example.com
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – OK
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

`PUT ``/api/user/{username}`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **username** \(_string_\) – 

**Example request:**

```
PUT /api/user/{username} HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "email": "string",
    "enabled": "string",
    "name": "string",
    "password": "string",
    "pwdExpirationDate": "string",
    "pwdUpdateRequired": true,
    "roles": "string"
}
```

Status Codes
* [200 OK](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource updated.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### UserChangePassword[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`POST ``/api/user/{username}/change_password`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")Parameters
* **username** \(_string_\) – 

**Example request:**

```
POST /api/user/{username}/change_password HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "new_password": "string",
    "old_password": "string"
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.

### UserPasswordPolicy[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this headline")

`POST ``/api/user/validate_password`[¶](https://docs.ceph.com/en/pacific/mgr/ceph_api/ "Permalink to this definition")> Check if the password meets the password policy. :param password: The password to validate. :param username: The name of the user \(optional\). :param old\_password: The old password \(optional\). :return: An object with properties valid, credits and valuation. ‘credits’ contains the password complexity credits and ‘valuation’ the textual summary of the validation.

**Example request:**

```
POST /api/user/validate_password HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "old_password": "string",
    "password": "string",
    "username": "string"
}
```

Status Codes
* [201 Created](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Resource created.
* [202 Accepted](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation is still executing. Please check the task queue.
* [400 Bad Request](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Operation exception. Please check the response body for details.
* [401 Unauthorized](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthenticated access. Please login first.
* [403 Forbidden](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unauthorized access. Please check your permissions.
* [500 Internal Server Error](https://docs.ceph.com/en/pacific/mgr/ceph_api/) – Unexpected error. Please check the response body for the stack trace.
