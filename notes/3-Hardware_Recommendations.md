# 3: Hardware Recommendations

**クリップソース:** [3: Hardware Recommendations — Ceph Documentation](https://docs.ceph.com/en/pacific/start/hardware-recommendations/)

# Hardware Recommendations¶

Cephはコモディティハードウェアで動作するように設計されているため、ペタバイトスケールのデータクラスタの構築と維持が経済的に実現可能です。クラスタのハードウェアを計画する際には、障害ドメインや潜在的なパフォーマンスの問題など、さまざまな考慮事項のバランスをとる必要があります。ハードウェアの計画には、Cephを使用するCephデーモンやその他のプロセスを多くのホストに分散することが含まれます。一般に、特定の種類のCephデーモンは、その種類のデーモン用に構成されたホストで実行することをお勧めします。データクラスタを利用するプロセス\(OpenStack、CloudStackなど\)には、他のホストを使用することをお勧めします。

Tip

Cephの[ブログ](https://ceph.com/community/blog/)もチェックしてみてください。

## **CPU[¶](https://docs.ceph.com/en/pacific/start/hardware-recommendations/#cpu "Permalink to this headline")**

CephFSメタデータサーバはCPUを多用するため、かなりの処理能力\(クアッドコア以上のCPUなど\)を持ち、高いクロックレート\(周波数はGHz\)の恩恵を受ける必要があります。Ceph OSDは、[RADOS](https://docs.ceph.com/en/pacific/glossary/#term-RADOS)サービスを実行し、[CRUSH](https://docs.ceph.com/en/pacific/glossary/#term-CRUSH)でデータ配置を計算し、データを複製し、クラスタマップの独自のコピーを維持します。したがって、OSDノードには相応の処理能力が必要です。要件はユースケースによって異なりますが、ライト／アーカイブ用途ではOSDあたり1コア、VMに接続されたRBDボリュームのような重いワークロードではOSDあたり2コアが目安となります。 モニタ/マネージャノードでは、CPUへの要求は高くないため、控えめなプロセッサを選択することができます。 また、ホストマシンがCephデーモンに加えてCPU負荷の高いプロセスを実行するかどうかも考慮してください。たとえば、ホストがコンピューティングVM\(OpenStack Novaなど\)を実行する場合は、これらの他のプロセスがCephデーモンに十分な処理能力を残すようにする必要があります。リソースの競合を避けるために、追加のCPU負荷の高いプロセスを別のホストで実行することをお勧めします。

## **RAM[¶](https://docs.ceph.com/en/pacific/start/hardware-recommendations/#ram "Permalink to this headline")**

一般的に、RAMは多い方が良いとされています。 大規模なクラスタのモニタ/マネージャノードでは64GBで十分ですが、数百のOSDを備えた大規模なクラスタでは128GBが妥当な目標となります。 BlueStore OSDのメモリターゲットは、デフォルトで4GBです。 オペレーティングシステムや管理タスク\(監視や測定など\)、およびリカバリ時の消費量の増加を考慮して、BlueStore OSDあたり8GB程度のプロビジョニングを推奨します。

### Monitors and managers \(ceph\-mon and ceph\-mgr\)[¶](https://docs.ceph.com/en/pacific/start/hardware-recommendations/#monitors-and-managers-ceph-mon-and-ceph-mgr "Permalink to this headline")

モニタとマネージャのデーモンのメモリ使用量は、一般的にクラスタのサイズに比例します。 起動時、トポロジー変更時、リカバリー時には、これらのデーモンは定常動作時よりも多くのRAMを必要としますので、ピーク時の使用量を考慮して計画してください。 非常に小さなクラスターでは、32GBで十分です。 300台以上のOSDを搭載したクラスターでは、64GBを使用してください。 さらに多くのOSDを搭載した（または搭載する予定の）クラスターには、129GBを用意してください。 また、mon\_osd\_cache\_sizeや rocksdb\_cache\_sizeなどの設定を調整することも慎重に検討する必要があります。

### Metadata servers \(ceph\-mds\)[¶](https://docs.ceph.com/en/pacific/start/hardware-recommendations/#metadata-servers-ceph-mds "Permalink to this headline")

メタデータ・デーモンのメモリ使用量は、キャッシュが消費するメモリ量が設定されているかどうかで決まります。 ほとんどのシステムでは、最低でも 1 GB を推奨しています。 mds\_cache\_memory を参照してください。

### OSDs \(ceph\-osd\)[¶](https://docs.ceph.com/en/pacific/start/hardware-recommendations/#osds-ceph-osd "Permalink to this headline")

## **Memory[¶](https://docs.ceph.com/en/pacific/start/hardware-recommendations/#memory "Permalink to this headline")**

Bluestoreでは、OSDがデータをキャッシュする際に、オペレーティングシステムのページキャッシュに頼らず、独自のメモリを使用します。 bluestoreでは、OSDが消費しようとするメモリの量をosd\_memory\_target構成オプションで調整することができます。

* osd\_memory\_targetを2GB以下に設定することは、一般的に推奨されません（メモリをそこまで低く保つことができず、またパフォーマンスが極端に低下する可能性があります）。
* メモリターゲットを2GBから4GBの間に設定すると通常は動作しますが、アクティブなデータセットが比較的小さい場合を除き、IO時にメタデータがディスクから読み込まれることがあるため、パフォーマンスが低下する可能性があります。
* 4GBは現在のデフォルトのosd\_memory\_targetサイズで、一般的な使用例でのメモリ要件とOSDのパフォーマンスのバランスをとるために設定されました。
* osd\_memory\_targetを4GBよりも高く設定すると、処理するオブジェクトの数が多い（小さい）場合や、データセットのサイズが大きい（256GB/OSD以上）場合に、パフォーマンスが向上することがあります。

Important

OSDのメモリ自動調整は「最善の努力」です。 OSDはカーネルがメモリを再利用できるようにメモリをアンマップすることがありますが、特定の時間内にカーネルが解放されたメモリを実際に再利用するという保証はありません。 これは、Cephの古いバージョンでは特に顕著で、透明な巨大ページによって、断片化された巨大ページから解放されたメモリをカーネルが再利用できないことがあります。Cephの最新バージョンでは、アプリケーションレベルで透明な巨大ページを無効にしてこの問題を回避していますが、それでもカーネルがマップされていないメモリを直ちに解放することは保証されません。 OSDがメモリターゲットを超えることもあります。 一時的な急上昇時やカーネルによる解放ページの再利用の遅れによるOSDの暴走を防ぐために、システムに20％程度の追加メモリを確保することをお勧めします。 この値は、システムの正確な構成に応じて、必要以上または以下の場合があります。

レガシーのFileStoreバックエンドを使用する場合、ページキャッシュはデータのキャッシュに使用されるため、通常はチューニングの必要はなく、OSDのメモリ消費量は一般的にシステム内のデーモンごとのPGの数に関連します。

## **Data Storage[¶](https://docs.ceph.com/en/pacific/start/hardware-recommendations/#data-storage "Permalink to this headline")**

データストレージの構成は慎重に計画してください。データストレージの計画を立てる際には、コストとパフォーマンスのトレードオフを考慮する必要があります。OSの同時操作や、1台のドライブに対して複数のデーモンが同時に読み取りと書き込みの操作を要求すると、パフォーマンスが大幅に低下します。

Important

Cephは書き込みをACKする前にすべてのデータをジャーナル\(またはWAL\+DB\)に書き込まなければならないため、このメタデータとOSDのパフォーマンスのバランスを取ることはとても重要です。

### Hard Disk Drives[¶](https://docs.ceph.com/en/pacific/start/hardware-recommendations/#hard-disk-drives "Permalink to this headline")

 

OSDは、オブジェクトデータ用に十分なハードディスクドライブの容量を確保する必要があります。我々は、最低でも1テラバイトのハードディスクドライブを推奨します。ディスクサイズが大きいほど、ギガバイトあたりのコストが高くなります。ギガバイトあたりのコストに大きな影響を与える可能性があるため、ハードディスク・ドライブの価格をギガバイト数で割って、ギガバイトあたりのコストを算出することをお勧めします。例えば、75ドルで販売されている1テラバイトのハードディスクの場合、1ギガバイトあたりのコストは0.07ドルとなります（つまり、75ドル÷1024＝0.0732となります）。一方、150ドルの3テラバイトのハードディスクの場合、1ギガバイトあたり0.05ドル（150ドル÷3072＝0.0488）となります。前述の例では、1テラバイトのディスクを使用すると、ギガバイトあたりのコストが40％増加し、クラスタのコスト効率が大幅に低下します。

Tip

1台のSAS/SATAドライブで複数のOSDを動作させることは、決して良いことではありません。 

しかし、NVMeドライブは、2つ以上のOSDに分割することで、パフォーマンスの向上を実現できます。

Tip

また、OSDとモニターやメタデータサーバーを1台のドライブで動かすのもよくありません。

ストレージ・ドライブには、シークタイム、アクセスタイム、リードタイム、ライトタイム、およびトータル・スループットの制限があります。

これらの物理的な制限は、システム全体のパフォーマンス、特にリカバリ時のパフォーマンスに影響します。

オペレーティングシステムとソフトウェアには専用の\(理想的にはミラーリングされた\)ドライブを使用し、ホスト上で実行するCeph OSDデーモンにはそれぞれ1台のドライブを使用することをお勧めします\(上記のNVMeの項を参照\)。

ハードウェアの故障に起因しない「遅いOSD」の問題の多くは、オペレーティングシステム、複数のOSD、および複数のジャーナルを同じドライブで実行することによって発生します。

小規模なクラスターでパフォーマンス問題をトラブルシューティングするコストは、追加のディスクドライブのコストを上回る可能性が高いため、OSDストレージドライブに過剰な負荷をかけないようにすることで、クラスターの設計計画を最適化することができます。

SAS/SATAドライブごとに複数のCeph OSDデーモンを実行することもできますが、リソースの競合が発生し、全体のスループットが低下する可能性があります。

ジャーナルとオブジェクトデータを同じドライブに保存することもできますが、これにより、書き込みをジャーナルしてクライアントにACKするまでの時間が長くなる可能性があります。

Cephは、書き込みをACKする前にジャーナルに書き込む必要があります。

Cephのベストプラクティスでは、オペレーティングシステム、OSDデータ、OSDジャーナルを別々のドライブで実行することが定められています。

### Solid State Drives[¶](https://docs.ceph.com/en/pacific/start/hardware-recommendations/#solid-state-drives "Permalink to this headline")

パフォーマンスを向上させる1つの方法として、ソリッド・ステート・ドライブ（SSD）を使用して、ランダム・アクセス・タイムとリード・レイテンシーを短縮するとともに、スループットを高速化することが挙げられます。SSDはハードディスク・ドライブと比較してギガバイトあたりのコストが10倍以上かかることが多いですが、SSDはハードディスク・ドライブと比較して少なくとも100倍のアクセス時間を実現しています。

SSDには動く機械部品がないため、ハードディスクドライブのような制限を受けることはありません。しかし、SSDには大きな制限があります。SSDを評価する際には、シーケンシャルリードとライトのパフォーマンスを考慮することが重要です。複数のOSD用に複数のジャーナルを保存する場合、シーケンシャルライトのスループットが400MB/sのSSDの方が、シーケンシャルライトのスループットが120MB/sのSSDよりもはるかに優れたパフォーマンスを発揮する可能性があります。

Important

私たちは、パフォーマンスを向上させるためにSSDの使用を検討することをお勧めします。ただし、SSDに多額の投資を行う前に、SSDのパフォーマンス指標を確認し、テスト構成でSSDをテストしてパフォーマンスを評価することを強くお勧めします。

SSDには動く機械部品がないため、Cephのストレージ容量をあまり使わない部分\(ジャーナルなど\)にSSDを使用することは理にかなっています。比較的安価なSSDは、経済的な感覚に訴えることができるかもしれません。注意を払ってください。Cephで使用するSSDを選択する場合、許容できるIOPSだけでは十分ではありません。ジャーナルとSSDには、いくつかの重要なパフォーマンス上の考慮点があります。

* **Write\-intensive semantics:**
* **Sequential Writes:**
* **Partition Alignment:**

SSDはこれまで、オブジェクト・ストレージとしてはコストが高いとされてきましたが、最近ではQLCドライブが登場し、その差は縮まってきています。 HDDのOSDでは、WAL\+DBをSSDにオフロードすることで、大幅なパフォーマンスの向上が期待できます。

CephがCephFSファイルシステムのパフォーマンスを高速化する方法の1つは、CephFSメタデータのストレージとCephFSファイルコンテンツのストレージを分離することです。Cephは、CephFSメタデータ用にデフォルトのメタデータプールを提供します。CephFSメタデータ用のプールを作成する必要はありませんが、ホストのSSDストレージメディアのみを指すCephFSメタデータプール用のCRUSHマップ階層を作成することができます。詳細については、「[CRUSHデバイスクラス](https://docs.ceph.com/en/pacific/rados/operations/crush-map-edits/#crush-map-device-class)」を参照してください。

### Controllers[¶](https://docs.ceph.com/en/pacific/start/hardware-recommendations/#controllers "Permalink to this headline")

ディスクコントローラ（HBA）は、書き込みスループットに大きな影響を与えます。パフォーマンスのボトルネックにならないように、慎重に選択してください。 特に、RAIDモード（IR）のHBAは、シンプルな「JBOD」（IT）モードのHBAよりも高いレイテンシーを示す場合があり、また、RAID SoC、ライトキャッシュ、バッテリーバックアップは、ハードウェアとメンテナンスのコストを大幅に増加させます。 RAID HBAの中には、ITモードの「パーソナリティ」を設定できるものもある。

Tip

[Cephブログ](https://ceph.com/community/blog/)は、Cephのパフォーマンス問題に関する優れた情報源であることがよくあります。その他の詳細については、「[Ceph Write Throughput 1](http://ceph.com/community/ceph-performance-part-1-disk-controller-write-throughput/)」および「[Ceph Write Throughput 2](http://ceph.com/community/ceph-performance-part-2-write-throughput-without-ssd-journals/)」を参照してください。

### Additional Considerations[¶](https://docs.ceph.com/en/pacific/start/hardware-recommendations/#additional-considerations "Permalink to this headline")

通常、ホストごとに複数のOSDを稼働させますが、OSDドライブのスループットの合計が、クライアントのデータの読み書きに必要なネットワーク帯域幅を超えないようにする必要があります。また、クラスタが各ホストに保存するデータ全体の割合を考慮する必要があります。特定のホストでの割合が大きく、そのホストに障害が発生すると、完全な比率を超えてしまうなどの問題が発生し、データ損失を防ぐ安全対策としてCephが操作を停止してしまいます。

ホストごとに複数のOSDを実行する場合、カーネルが最新の状態であることも確認する必要があります。ホストごとに複数のOSDを実行する際に、ハードウェアが期待通りの性能を発揮できるよう、glibcおよびsyncfs\(2\)に関する注意事項については、「[OSの推奨事�](https://docs.ceph.com/en/pacific/start/os-recommendations)��」をご覧ください。

## **Networks[¶](https://docs.ceph.com/en/pacific/start/hardware-recommendations/#networks "Permalink to this headline")**

ラックには最低でも10Gbps\+のネットワークを用意してください。1Gbpsのネットワークで1TBのデータを複製すると3時間、10TBだと30時間かかります 一方、10Gbpsのネットワークを使用した場合、レプリケーションにかかる時間はそれぞれ20分、1時間となります。ペタバイト規模のクラスターでは、OSDドライブの故障は例外ではなく想定内のことです。システム管理者は、価格と性能のトレードオフを考慮した上で、PGが劣化した状態からアクティブでクリーンな状態に可能な限り迅速に回復することを評価するでしょう。また、一部の導入ツールでは、ハードウェアやネットワークケーブルをより管理しやすくするためにVLANを採用しています。802.1qプロトコルを使用したVLANには、VLAN対応のNICとSwitchが必要です。ハードウェアの追加費用は、ネットワークの設定やメンテナンスのための運用コストの削減によって相殺される場合があります。クラスタとコンピュートスタック（OpenStack、CloudStackなど）間のVMトラフィックをVLANで処理する場合は、10Gイーサネット以上を使用することに価値があります。

また、各ネットワークのトップオブラック・ルーターは、40Gbp/s以上のより高速なスループットを持つスパイン・ルーターと通信できる必要があります。

サーバーハードウェアには、ベースボードマネジメントコントローラー（BMC）が搭載されているはずです。管理ツールやデプロイメントツールも、特にIPMIやRedfishを介してBMCを多用する場合がありますので、管理用のアウトオブバンドネットワークのコストとベネフィットのトレードオフを考慮してください。ハイパーバイザーのSSHアクセス、VMイメージのアップロード、OSイメージのインストール、管理用ソケットなどは、ネットワークに大きな負荷をかけます。 3つのネットワークを運用するのはやり過ぎのように思えるかもしれませんが、各トラフィックパスは容量、スループット、パフォーマンスのボトルネックとなる可能性があり、大規模なデータクラスタを導入する前に慎重に検討する必要があります。

## **Failure Domains[¶](https://docs.ceph.com/en/pacific/start/hardware-recommendations/#failure-domains "Permalink to this headline")**

障害ドメインとは、1つまたは複数のOSDへのアクセスを妨げるあらゆる障害のことである。ホスト上のデーモンの停止、ハードディスクの故障、OS のクラッシュ、NIC の誤動作、電源の故障、ネットワークの停止、停電などが考えられます。ハードウェアの必要性を計画する際には、少なすぎる障害領域に多くの責任を負わせることでコストを削減しようとする誘惑と、すべての潜在的な障害領域を隔離するための追加コストとのバランスを取る必要があります。

## **Minimum Hardware Recommendations[¶](https://docs.ceph.com/en/pacific/start/hardware-recommendations/#minimum-hardware-recommendations "Permalink to this headline")**

Cephは安価なコモディティハードウェアで動作します。小規模な生産クラスターや開発クラスターであれば、安価なハードウェアでも問題なく動作します。

|Process       |Criteria                                                                                                    |Minimum Recommended                                                                                                                                                                                                                                                                                                                                                                                             |
|--------------|------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|ceph\-osd     |Processor                                                                                                   |* 1 core minimum
| | |* 1 core per 200\-500 MB/s
| | |* 1 core per 1000\-3000 IOPS
| | |* Results are before replication.
| | |* Results may vary with different CPU models and Ceph features. \(erasure coding, compression, etc\)
| | |* ARM processors specifically may require additional cores.
| | |* Actual performance depends on many factors including drives, net, and client throughput and latency. Benchmarking is highly recommended.
| |RAM           |* 4GB\+ per daemon \(more is better\)
| | |* 2\-4GB often functions \(may be slow\)
| | |* Less than 2GB not recommended
|                                                                                                                                                                                                                                                                                                                                                                                                                |
| |Volume Storage|1x storage drive per daemon                                                                                 |                                                                                                                                                                                                                                                                                                                                                                                                                |
| |DB/WAL        |1x SSD partition per daemon \(optional\)                                                                    |                                                                                                                                                                                                                                                                                                                                                                                                                |
| |Network       |1x 1GbE\+ NICs \(10GbE\+ recommended\)                                                                      |                                                                                                                                                                                                                                                                                                                                                                                                                |
|ceph\-mon     |Processor                                                                                                   |* 2 cores minimum |
| |RAM           |24GB\+ per daemon                                                                                           |                                                                                                                                                                                                                                                                                                                                                                                                                |
| |Disk Space    |60 GB per daemon                                                                                            |                                                                                                                                                                                                                                                                                                                                                                                                                |
| |Network       |1x 1GbE\+ NICs                                                                                              |                                                                                                                                                                                                                                                                                                                                                                                                                |
|ceph\-mds     |Processor                                                                                                   |* 2 cores minimum |
| |RAM           |2GB\+ per daemon                                                                                            |                                                                                                                                                                                                                                                                                                                                                                                                                |
| |Disk Space    |1 MB per daemon                                                                                             |                                                                                                                                                                                                                                                                                                                                                                                                                |
| |Network       |1x 1GbE\+ NICs                                                                                              |                                                                                                                                                                                                                                                                                                                                                                                                                |

Tip

1枚のディスクでOSDを動作させる場合は、ボリュームストレージ用のパーティションを、OSの入ったパーティションとは別に作成してください。一般的には、OS用とボリュームストレージ用のディスクは別々にすることをお勧めします。
