# 202: iSCSI Targets

**クリップソース:** [202: iSCSI Targets — Ceph Documentation](https://docs.ceph.com/en/pacific/rbd/iscsi-targets/)

# iSCSI Targets[¶](https://docs.ceph.com/en/pacific/rbd/iscsi-targets/ "Permalink to this headline")

従来、Cephストレージクラスタへのブロックレベルのアクセスは、QEMUとlibbdに限られていましたが、これはOpenStack環境での採用を可能にする重要な要素となっています。Ceph Luminousリリースから、ブロックレベルのアクセスは標準のiSCSIサポートに拡張され、プラットフォームの利用範囲が広がり、新しいユースケースが生まれる可能性があります。

* Red Hat Enterprise Linux/CentOS 7.5（以降）、Linux kernel v4.16（以降）
* ceph\-ansibleまたはコマンドラインインターフェイスを使用してデプロイされた、動作中のCeph Storageクラスタ
* iSCSIゲートウェイノードは、OSDノードとコロケーションするか、専用ノードに設置することが可能
* iSCSIフロントエンドのトラフィックとCephバックエンドのトラフィック用に別々のネットワークサブネットを用意

Ceph iSCSIゲートウェイのインストールと設定には、Ansibleまたはコマンドラインインターフェイスを使用するデプロイ方法を選択できます：

* [Using Ansible](https://docs.ceph.com/en/pacific/rbd/iscsi-targets/)
* [Using the Command Line Interface](https://docs.ceph.com/en/pacific/rbd/iscsi-targets/)
