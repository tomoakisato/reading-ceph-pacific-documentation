# 27: OSD Service

**クリップソース:** [26: OSD Service — Ceph Documentation](https://docs.ceph.com/en/pacific/cephadm/services/osd/)

# OSD Service[¶](https://docs.ceph.com/en/pacific/cephadm/services/osd/ "Permalink to this headline")

## List Devices[¶](https://docs.ceph.com/en/pacific/cephadm/services/osd/ "Permalink to this headline")

ceph\-volumeは、クラスタ内の各ホストを随時スキャンして、どのデバイスが存在するか、OSDとして使用する資格があるかどうかを判断します。

cephadmによって検出されたデバイスのリストを表示するには、次のコマンドを実行します。

```
# ceph orch device ls [--hostname=...] [--wide] [--refresh]
```

Example

```
Hostname  Path      Type  Serial              Size   Health   Ident  Fault  Available
srv-01    /dev/sdb  hdd   15P0A0YFFRD6         300G  Unknown  N/A    N/A    No
srv-01    /dev/sdc  hdd   15R0A08WFRD6         300G  Unknown  N/A    N/A    No
srv-01    /dev/sdd  hdd   15R0A07DFRD6         300G  Unknown  N/A    N/A    No
srv-01    /dev/sde  hdd   15P0A0QDFRD6         300G  Unknown  N/A    N/A    No
srv-02    /dev/sdb  hdd   15R0A033FRD6         300G  Unknown  N/A    N/A    No
srv-02    /dev/sdc  hdd   15R0A05XFRD6         300G  Unknown  N/A    N/A    No
srv-02    /dev/sde  hdd   15R0A0ANFRD6         300G  Unknown  N/A    N/A    No
srv-02    /dev/sdf  hdd   15R0A06EFRD6         300G  Unknown  N/A    N/A    No
srv-03    /dev/sdb  hdd   15R0A0OGFRD6         300G  Unknown  N/A    N/A    No
srv-03    /dev/sdc  hdd   15R0A0P7FRD6         300G  Unknown  N/A    N/A    No
srv-03    /dev/sdd  hdd   15R0A0O7FRD6         300G  Unknown  N/A    N/A    No
```

\-\-wideオプションを使用すると、OSDとして使用できない理由を含め、デバイスに関するすべての詳細が表示されます。

上の例では、「Health」、「Ident」、「Fault」というフィールドが表示されています。この情報は、[libstoragemgmt](https://docs.ceph.com/en/pacific/cephadm/services/osd/)との統合により提供されます。デフォルトでは、この統合は無効になっています\([libstoragemgmt](https://docs.ceph.com/en/pacific/cephadm/services/osd/)がハードウェアと100%互換性がない場合があるため\)。 cephadmにこれらのフィールドを含めるには、以下のようにcephadmの「enhanced device scan」オプションを有効にします。

```
# ceph config set mgr mgr/cephadm/device_enhanced_scan true
```

警告：libstoragemgmtライブラリは、標準的なSCSI問い合わせコールを実行しますが、ファームウェアがこれらの標準を完全に実装しているという保証はありません。このため、古いハードウェアでは、動作が不安定になったり、バスがリセットされたりすることがあります。したがって、この機能を有効にする前に、サービスの予期せぬ中断を避けるため、まずハードウェアの libstoragemgmt との互換性をテストすることをお勧めします。

互換性をテストする方法はいくつかありますが、最も簡単な方法は、cephadmシェルを使用してlibstoragemgmtを直接呼び出すことです \(cephadm shell lsmcli ldl\)。ハードウェアがサポートされていれば、次のように表示されます。

```
Path     | SCSI VPD 0x83    | Link Type | Serial Number      | Health Status
----------------------------------------------------------------------------
/dev/sda | 50000396082ba631 | SAS       | 15P0A0R0FRD6       | Good
/dev/sdb | 50000396082bbbf9 | SAS       | 15P0A0YFFRD6       | Good
```

libstoragemgmtのサポートを有効にすると、以下のような出力が得られます。

```
# ceph orch device ls
Hostname   Path      Type  Serial              Size   Health   Ident  Fault  Available
srv-01     /dev/sdb  hdd   15P0A0YFFRD6         300G  Good     Off    Off    No
srv-01     /dev/sdc  hdd   15R0A08WFRD6         300G  Good     Off    Off    No
:
```

この例では、libstoragemgmtがドライブの健全性を確認し、ドライブ筐体のIdentificationおよびFault LEDと対話できることを確認しています。これらのLEDとの対話についての詳細は、「デバイス管理」を参照してください。

注：libstoragemgmtの現在のリリース（1.8.8）は、SCSI、SAS、SATAベースのローカルディスクのみをサポートしています。NVMeデバイス\(PCIe\)の公式サポートはありません。

## Deploy OSDs[¶](https://docs.ceph.com/en/pacific/cephadm/services/osd/ "Permalink to this headline")

### Listing Storage Devices[¶](https://docs.ceph.com/en/pacific/cephadm/services/osd/ "Permalink to this headline")

OSDを展開するためには、OSDが展開されるストレージデバイスが利用可能である必要があります。

このコマンドを実行して、すべてのクラスタホスト上のストレージデバイスのインベントリを表示します。

```
# ceph orch device ls
```

以下の条件がすべて満たされている場合、ストレージデバイスは使用可能とみなされます。

* デバイスにパーティションがないこと
* デバイスにLVMの状態がないこと
* デバイスがマウントされていないこと
* デバイスにファイルシステムが含まれていないこと
* デバイスにCeph BlueStore OSDが含まれていないこと
* 5GB以上のデバイスであること

Cephは、利用できないデバイスにOSDをプロビジョニングしません。

### Creating New OSDs[¶](https://docs.ceph.com/en/pacific/cephadm/services/osd/ "Permalink to this headline")

新しいOSDを作るにはいくつかの方法があります。

* Cephに、利用可能なストレージデバイスと未使用のストレージデバイスを消費するように指示する

```
# ceph orch apply osd --all-available-devices
```

* 特定のホスト上の特定のデバイスからOSDを作成する

```
# ceph orch daemon add osd *<host>*:*<device-path>*
```

For example:

```
# ceph orch daemon add osd host1:/dev/sdb
```

* Advanced OSD Service Specifications\(後述\)を使用すると、デバイスのプロパティに基づいてデバイスを分類することができます。これは、どのデバイスが消費可能かを明確に把握するのに役立つ場合があります。プロパティには、デバイスの種類（SSDまたはHDD）、デバイスのモデル名、サイズ、およびデバイスが存在するホストが含まれます。

### Dry Run[¶](https://docs.ceph.com/en/pacific/cephadm/services/osd/ "Permalink to this headline")

\-\-dry\-runフラグは、オーケストレーターが実際にOSDを作成せずに、何が起こるかのプレビューを提示します。

例えば、以下のようになります。

```
# ceph orch apply osd --all-available-devices --dry-run
```

```
NAME                  HOST  DATA      DB  WAL
all-available-devices node1 /dev/vdb  -   -
all-available-devices node2 /dev/vdc  -   -
all-available-devices node3 /dev/vdd  -   -
```

### Declarative State[¶](https://docs.ceph.com/en/pacific/cephadm/services/osd/ "Permalink to this headline")

_ceph orch apply_の効果は持続します。つまり、_ceph orch apply_コマンドが完了した後にシステムに追加されたドライブは、自動的に検出され、クラスタに追加されます。 また、_ceph orch apply_コマンドが完了した後に\(ザッピングなどで\)使用可能になったドライブは、自動的に検出されてクラスタに追加されることになります。

ここでは、以下のコマンドの効果を検証します。

```
# ceph orch apply osd --all-available-devices
```

上記のコマンドを実行すると

* クラスタに新しいディスクを追加すると、自動的に新しいOSDの作成に使用されます。
* OSDを削除してLVM物理ボリュームをクリーニングすると、新しいOSDが自動的に作成されます。

利用可能なデバイスでOSDの自動作成を無効にするには、unmanagedパラメータを使用します。

この動作を回避したい（利用可能なデバイスでのOSDの自動作成を無効にしたい）場合は、unmanagedパラメータを使用してください。

```
# ceph orch apply osd --all-available-devices --unmanaged=true
```

Note:この3つの事実を覚えておいてください。

* _ceph orch apply_
* unmanaged:Trueを設定すると、OSDの作成が無効になります。unmanaged:Trueを設定すると、新しいOSDサービスを適用しても何も起こりません。
* _ceph orch daemon add_
* cephadmについては、「 

## Remove an OSD[¶](https://docs.ceph.com/en/pacific/cephadm/services/osd/ "Permalink to this headline")

クラスタからのOSDの削除には2つのステップがあります。

1. すべてのプレースメントグループ\(PG\)をクラスタから退避させる
2. PGフリーOSDをクラスタから外す

次のコマンドは、この2つのステップを実行します。

```
# ceph orch osd rm <osd_id(s)> [--replace] [--force]
```

Example:

```
# ceph orch osd rm 0
```

Expected output:

```
Scheduled OSD(s) for removal
```

破壊しても安全ではないOSDは拒否されます。

### Monitoring OSD State[¶](https://docs.ceph.com/en/pacific/cephadm/services/osd/ "Permalink to this headline")

以下のコマンドで、OSDの動作状態を問い合わせることができます。

```
# ceph orch osd rm status
```

Expected output:

```
OSD_ID  HOST         STATE                    PG_COUNT  REPLACE  FORCE  STARTED_AT
2       cephadm-dev  done, waiting for purge  0         True     False  2020-07-17 13:01:43.147684
3       cephadm-dev  draining                 17        False    True   2020-07-17 13:01:45.162158
4       cephadm-dev  started                  42        False    True   2020-07-17 13:01:45.162158
```

OSD上にPGが残らなくなると、OSDはデコミッションされ、クラスタから削除されます。

注記：OSDを削除した後、削除したOSDが使用していたデバイスのLVM物理ボリュームをワイプすると、新しいOSDが作成されます。これについては、「 [Declarative State](https://docs.ceph.com/en/pacific/cephadm/services/osd/)」のunmanagedパラメータについてをお読みください。

### Stopping OSD Removal[¶](https://docs.ceph.com/en/pacific/cephadm/services/osd/ "Permalink to this headline")

以下のコマンドを使用して、キューに入っているOSD削除を停止することができます。

```
# ceph orch osd rm stop <osd_id(s)>
```

Example:

```
# ceph orch osd rm stop 4
```

Expected output:

```
Stopped OSD(s) removal
```

これにより、OSDの初期状態がリセットされ、削除キューから外されます。

### Replacing an OSD[¶](https://docs.ceph.com/en/pacific/cephadm/services/osd/ "Permalink to this headline")

```
# orch osd rm <osd_id(s)> --replace [--force]
```

Example:

```
# ceph orch osd rm 4 --replace
```

Expected output:

```
Scheduled OSD(s) for replacement
```

これは、「OSDの削除」の手順と同じ手順で行われますが、1つの例外があります。OSDはCRUSH階層から永久に削除されず、代わりに「destroyed」フラグが割り当てられます。

Note:削除されたOSDの代わりとなる新しいOSDは、削除されたOSDと同じホスト上に作成する必要があります。

**Preserving the OSD ID**

destroyedフラグは、どのOSD IDが次回のOSDデプロイで再利用されるかを決定するために使用されます。

OSDのデプロイにOSDSpecを使用している場合、新しく追加されたディスクには、置き換えられたディスクのOSD IDが割り当てられます。これは、新しいディスクがOSDSpecsと一致していることを前提としています。

_ceph orch apply osd_コマンドが希望通りの動作をするかどうかを確認するには、\-\-dry\-runフラグを使用します。\-\-dry\-run フラグは、指定した変更を行わない場合のコマンドの結果を示します。コマンドが希望通りの動作をすることが確認できたら、\-\-dry\-run フラグを付けずにコマンドを実行します。

Tip:OSDSpecの名前は、ceph orch lsコマンドで取得できます。

また、OSDSpecファイルを使用することもできます。

```
# ceph orch apply osd -i <osd_spec_file> --dry-run
```

Expected output:

```
NAME                  HOST  DATA     DB WAL
<name_of_osd_spec>    node1 /dev/vdb -  -
```

この出力に意図が反映されていれば、\-\-dry\-run フラグを省略してデプロイを実行します。

### Erasing Devices \(Zapping Devices\)[¶](https://docs.ceph.com/en/pacific/cephadm/services/osd/ "Permalink to this headline")

デバイスを消去\(zap\)して再利用できるようにします。zapはリモートホストで_ceph\-volume zap_を呼び出します。

```
# ceph orch device zap <hostname> <path>
```

Example command:

```
# ceph orch device zap my_hostname /dev/sdx
```

Note:unmanagedフラグが設定されていない場合、cephadmはOSDSpecに一致するドライブを自動的に展開します。 たとえば、OSDの作成時にall\-available\-devicesオプションを使用した場合、デバイスをザッピングすると、cephadmオーケストレータは自動的にそのデバイスに新しいOSDを作成します。 この動作を無効にするには、「 [Declarative State](https://docs.ceph.com/en/pacific/cephadm/services/osd/)」を参照してください。

## Automatically tuning OSD memory[¶](https://docs.ceph.com/en/pacific/cephadm/services/osd/ "Permalink to this headline")

OSDデーモンは、osd\_memory\_target設定オプション\(デフォルトでは数ギガバイト\)に基づいて、メモリ消費量を調整します。 Cephが他のサービスとメモリを共有していない専用ノードに展開されている場合、cephadmは、RAMの総量と展開されたOSDの数に基づいてOSDごとのメモリ消費量を自動的に調整できます。

このオプションは、でグローバルに有効になります。

```
ceph config set osd osd_memory_target_autotune true
```

Cephadmは、システム全体のRAMのフラクション\(mgr/cephadm/autotune\_memory\_target\_ratio、デフォルトは0.7\)から開始し、自動調整されていないデーモン\(OSD以外とosd\_memory\_target\_autotuneがfalseのOSD\)が消費するメモリを差し引いて、残りのOSDで割ります。

最終的なターゲットは、次のようなオプションで設定データベースに反映されます。

```
WHO   MASK      LEVEL   OPTION              VALUE
osd   host:foo  basic   osd_memory_target   126092301926
osd   host:bar  basic   osd_memory_target   6442450944
```

各デーモンの制限値と現在消費しているメモリは、_ceph orch ps_の出力のMEM LIMIT列で確認できます。

```
NAME        HOST  PORTS  STATUS         REFRESHED  AGE  MEM USED  MEM LIMIT  VERSION                IMAGE ID      CONTAINER ID
osd.1       dael         running (3h)     10s ago   3h    72857k     117.4G  17.0.0-3781-gafaed750  7015fda3cd67  9e183363d39c
osd.2       dael         running (81m)    10s ago  81m    63989k     117.4G  17.0.0-3781-gafaed750  7015fda3cd67  1f0cc479b051
osd.3       dael         running (62m)    10s ago  62m    64071k     117.4G  17.0.0-3781-gafaed750  7015fda3cd67  ac5537492f27

```

メモリの自動調整からOSDを除外するには、そのOSDの自動調整オプションを無効にし、さらに特定のメモリターゲットを設定します。 例えば、以下のようになります。

```
# ceph config set osd.123 osd_memory_target_autotune false
# ceph config set osd.123 osd_memory_target 16G
```

## Advanced OSD Service Specifications[¶](https://docs.ceph.com/en/pacific/cephadm/services/osd/ "Permalink to this headline")

osdタイプのサービス仕様は、ディスクのプロパティを使用してクラスタのレイアウトを記述する方法です。サービス仕様は、デバイス名やパスの詳細を知らなくても、どのディスクをどの構成でOSDにするかをCephに伝える抽象的な方法を提供します。

サービス仕様では、OSDの作成に伴う手動作業を減らすために使用できるyamlまたはjsonファイルを定義することが可能です。

例えば、次のようなコマンドを実行する代わりに各デバイスと各ホストのために、レイアウトを記述できるyamlまたはjsonファイルを定義することができます：

```
[monitor.1]# ceph orch daemon add osd *<host>*:*<path-to-device>*
```

最も基本的な例を示します。

osd\_spec.ymlというファイルを作成します。

```
service_type: osd
service_id: default_drive_group  # custom name of the osd spec
placement:
  host_pattern: '*'              # which hosts to target
spec:
  data_devices:                  # the type of devices you are applying specs to
    all: true                    # a filter, check below for a full list
```

これはつまり:

globパターン'\*'に一致するすべてのホスト上で、任意の利用可能なデバイス\(ceph\-volumeが'利用可能'を決定します\)をOSDに変換します。\(グロブパターンはhost lsで登録されたホストにマッチします\)host\_patternの詳細については、以下のセクションを参照してください。

そして、それを次のように_osd create_に渡します。

この命令は、一致するすべてのホストに発行され、これらのOSDを展開します。

allフィルタで指定されたものよりも複雑な設定が可能です。詳細は、「フィルタ」を参照してください。

_apply osd_コマンドに\-\-dry\-runフラグを渡すと、提案されたレイアウトの概要を表示することができます。

Example

```
[monitor.1]# ceph orch apply osd -i /path/to/osd_spec.yml --dry-run
```

### Filters[¶](https://docs.ceph.com/en/pacific/cephadm/services/osd/ "Permalink to this headline")

Note:フィルタは、デフォルトではANDゲートを使用して適用されます。つまり、ドライブが選択されるためには、すべてのフィルタ基準を満たす必要があります。

この動作は、OSD仕様でfilter\_logic:ORを設定することで調整できます。

フィルタは、ディスクの属性を使用してグループに割り当てるために使用されます。

属性は、ceph\-volumeのディスククエリに基づいています。このコマンドで、属性に関する情報を取得できます。

```
ceph-volume inventory </path/to/disk>
```

#### Vendor or Model[¶](https://docs.ceph.com/en/pacific/cephadm/services/osd/ "Permalink to this headline")

ベンダーやモデルごとに特定のディスクをターゲットとすることができます。

```
model: disk_model_name
```

or

```
vendor: disk_vendor_name
```

#### Size[¶](https://docs.ceph.com/en/pacific/cephadm/services/osd/ "Permalink to this headline")

特定のディスクを「サイズ」で指定することができます。

```
size: size_spec
```

##### Size specs[¶](https://docs.ceph.com/en/pacific/cephadm/services/osd/ "Permalink to this headline")

サイズ指定には以下の形式があります。

* LOW:HIGH
* :HIGH
* LOW:
* EXACT

具体例：

正確なサイズのディスクを含めるために

```
size: '10G'
```

与えられたサイズの範囲内のディスクを含むように

```
size: '10G:40G'
```

サイズが10G以下のディスクを含めるため

```
size: ':10G'
```

40G以上のディスクを含むように

```
size: '40G:'
```

サイズはギガバイト\(G\)のみで指定する必要はありません。

他のサイズの単位もサポートしています。メガバイト\(M\)、ギガバイト\(G\)、テラバイト\(T\)などです。また、バイトの\(B\)を付けることも可能です。MB、GB、TB。

#### Rotational[¶](https://docs.ceph.com/en/pacific/cephadm/services/osd/ "Permalink to this headline")

これは、ディスクの「回転」属性に作用します。

```
rotational: 0 | 1
```

1は、回転するすべてのディスクにマッチします。

0は、回転しないすべてのディスク（SSD、NVMEなど）とマッチします。

#### All[¶](https://docs.ceph.com/en/pacific/cephadm/services/osd/ "Permalink to this headline")

これは、「利用可能」なすべてのディスクを取得します。

注：これはdata\_devicesセクションのための排他的なものです。

```
all: true
```

#### Limiter[¶](https://docs.ceph.com/en/pacific/cephadm/services/osd/ "Permalink to this headline")

いくつかの有効なフィルターを指定したが、そのフィルターがマッチするディスクの数を制限したい場合は、limitディレクティブを使用します。

```
limit: 2
```

例えば、VendorAのディスクをすべて照合するためにvendorを使用したが、最初の2つだけを使用したい場合は、limitを使用することができます。

```
data_devices:
  vendor: VendorA
  limit: 2
```

Note: limit は最終手段であり、避けられるならば使うべきではありません。

### Additional Options[¶](https://docs.ceph.com/en/pacific/cephadm/services/osd/ "Permalink to this headline")

OSDの展開方法を変更するために使用できる複数のオプション設定があります。これらのオプションをOSD仕様の基本レベルに追加することで、有効になります。

この例では、暗号化を有効にしてすべてのOSDを展開します。

```
service_type: osd
service_id: example_osd_spec
placement:
  host_pattern: '*'
spec:
  data_devices:
    all: true
  encrypted: true
```

完全なリストはDriveGroupSpecsでご覧ください。

```
classceph.deployment.drive_group.DriveGroupSpec(*args:Any, **kwargs:Any)¶
```

ceph\-volumeが理解できるのと同じ形式で、ドライブグループを記述します。

```
block_db_size:Optional[Union[int,str]]¶
```

bluestore\_block\_db\_sizeの値をバイト単位で設定（または上書き）する

```
block_wal_size:Optional[Union[int,str]]¶
```

bluestore\_block\_wal\_sizeの値をバイト単位で設定（またはオーバーライド）する

```
data_devices¶
```

A ceph.deployment.drive\_group.DeviceSelection

```
data_directories¶
```

A list of strings, containing paths which should back OSDs

```
db_devices¶
```

A ceph.deployment.drive\_group.DeviceSelection

```
db_slots¶
```

How many OSDs per DB device

```
encrypted¶
```

true or false

```
filter_logic¶
```

The logic gate we use to match disks with filters. defaults to ‘AND’

```
journal_devices¶
```

A ceph.deployment.drive\_group.DeviceSelection

```
journal_size:Optional[Union[int,str]]¶
```

set journal\_size in bytes

```
objectstore¶
```

filestore or bluestore

```
osd_id_claims¶
```

Optional: mapping of host \-\> List of osd\_ids that should be replaced See [OSD Replacement](https://docs.ceph.com/en/pacific/cephadm/services/osd/)

```
osds_per_device¶
```

Number of osd daemons per “DATA” device. To fully utilize nvme devices multiple osds are required.

```
preview_only¶
```

If this should be treated as a ‘preview’ spec

```
wal_devices¶
```

A ceph.deployment.drive\_group.DeviceSelection

```
wal_slots¶
```

How many OSDs per WAL device

## Examples[¶](https://docs.ceph.com/en/pacific/cephadm/services/osd/ "Permalink to this headline")

### The simple case[¶](https://docs.ceph.com/en/pacific/cephadm/services/osd/ "Permalink to this headline")

すべてのノードは同じ設定

```
20 HDDs
Vendor: VendorA
Model: HDD-123-foo
Size: 4TB

2 SSDs
Vendor: VendorB
Model: MC-55-44-ZX
Size: 512GB
```

これは一般的な設定で、非常に簡単に説明できます。

```
service_type: osd
service_id: osd_spec_default
placement:
  host_pattern: '*'
spec:
  data_devices:
    model: HDD-123-foo # Note, HDD-123 would also be valid
  db_devices:
    model: MC-55-44-XZ # Same here, MC-55-44 is valid
```

しかし、ドライブのコア特性に対するフィルタを減らすことで、改善することができます。

```
service_type: osd
service_id: osd_spec_default
placement:
  host_pattern: '*'
spec:
  data_devices:
    rotational: 1
  db_devices:
    rotational: 0
```

ここでは、すべての回転デバイスを「データデバイス」として宣言し、すべての非回転デバイスをshared\_devices \(wal, db\)として使用します。

2TB以上のドライブが常に低速のデータデバイスになることがわかっている場合は、サイズでフィルタリングすることもできます。

```
service_type: osd
service_id: osd_spec_default
placement:
  host_pattern: '*'
spec:
  data_devices:
    size: '2TB:'
  db_devices:
    size: ':2TB'
```

Note:上記のOSDの仕様はどれも同じように有効です。どれを使うかは、好みや、ノードレイアウトの変更をどの程度想定しているかによります。

### Multiple OSD specs for a single host[¶](https://docs.ceph.com/en/pacific/cephadm/services/osd/ "Permalink to this headline")

ここでは、2つの異なるセットアップがあります。

```
20 HDDs
Vendor: VendorA
Model: HDD-123-foo
Size: 4TB

12 SSDs
Vendor: VendorB
Model: MC-55-44-ZX
Size: 512GB

2 NVMEs
Vendor: VendorC
Model: NVME-QQQQ-987
Size: 256GB
```

* 20台のHDDが2台のSSDを共有する
* 10台のSSDが2つのNVMを共有する

これは2つのレイアウトで表現できます。

```
service_type: osd
service_id: osd_spec_hdd
placement:
  host_pattern: '*'
spec:
  data_devices:
    rotational: 0
  db_devices:
    model: MC-55-44-XZ
    limit: 2 # db_slots is actually to be favoured here, but it's not implemented yet
---
service_type: osd
service_id: osd_spec_ssd
placement:
  host_pattern: '*'
spec:
  data_devices:
    model: MC-55-44-XZ
  db_devices:
    vendor: VendorC
```

これにより、すべてのHDDをデータデバイスとして使用し、2台のSSDを専用のdb/walデバイスとして割り当てることで、希望のレイアウトを実現することができます。残りのSSD\(8台\)は、「VendorC」のNVMEを専用のdb/walデバイスとして割り当てられたデータデバイスとなります。

### Multiple hosts with the same disk layout[¶](https://docs.ceph.com/en/pacific/cephadm/services/osd/ "Permalink to this headline")

クラスタには、同じようなディスクレイアウトを持つさまざまな種類のホストが存在すると仮定すると、1つのホストセットにのみ異なるOSD仕様を適用することをお勧めします。通常は、同じレイアウトの複数のホストに対して1つの仕様を適用します。

サービスIDを一意のキーとする: 既に適用されているサービスIDを持つ新しいOSD仕様を適用すると、既存のOSD仕様がスーパーシードされます。既存のOSDデーモンは影響を受けません。[Declarative State](https://docs.ceph.com/en/pacific/cephadm/services/osd/)を参照してください。 .

Node1\-5

```
20 HDDs
Vendor: Intel
Model: SSD-123-foo
Size: 4TB
2 SSDs
Vendor: VendorA
Model: MC-55-44-ZX
Size: 512GB
```

Node6\-10

```
5 NVMEs
Vendor: Intel
Model: SSD-123-foo
Size: 4TB
20 SSDs
Vendor: VendorA
Model: MC-55-44-ZX
Size: 512GB
```

レイアウトの「placement」キーを使って、特定のノードを狙うことができます。

```
service_type: osd
service_id: disk_layout_a
placement:
  label: disk_layout_a
spec:
  data_devices:
    rotational: 1
  db_devices:
    rotational: 0
---
service_type: osd
service_id: disk_layout_b
placement:
  label: disk_layout_b
spec:
  data_devices:
    model: MC-55-44-XZ
  db_devices:
    model: SSD-123-foo
```

これにより、配置キーに応じて、異なるホストに異なるOSD仕様が適用されます。[Daemon Placement](https://docs.ceph.com/en/pacific/cephadm/services/osd/)を参照

注：各ホストが固有のディスクレイアウトを持っていると仮定すると、各OSD仕様は異なるサービスIDを持つ必要があります。 

### Dedicated wal \+ db[¶](https://docs.ceph.com/en/pacific/cephadm/services/osd/ "Permalink to this headline")

以前のすべてのケースでは、WALをDBと同居させていました。しかし、意味があるならば、WALを専用のデバイスに配置することも可能です。

```
20 HDDs
Vendor: VendorA
Model: SSD-123-foo
Size: 4TB

2 SSDs
Vendor: VendorB
Model: MC-55-44-ZX
Size: 512GB

2 NVMEs
Vendor: VendorC
Model: NVME-QQQQ-987
Size: 256GB
```

この場合のOSD仕様は以下のようになります（modelフィルタ使用）。

```
service_type: osd
service_id: osd_spec_default
placement:
  host_pattern: '*'
spec:
  data_devices:
    model: MC-55-44-XZ
  db_devices:
    model: SSD-123-foo
  wal_devices:
    model: NVME-QQQQ-987
```

また、以下のように特定のホストのデバイスパスを直接指定することも可能です。

```
service_type: osd
service_id: osd_using_paths
placement:
  hosts:
    - Node01
    - Node02
spec:
  data_devices:
    paths:
    - /dev/sdb
  db_devices:
    paths:
    - /dev/sdc
  wal_devices:
    paths:
    - /dev/sdd
```

これは、サイズやベンダーなどの他のフィルターでも同様に簡単に行うことができます。

## Activate existing OSDs[¶](https://docs.ceph.com/en/pacific/cephadm/services/osd/ "Permalink to this headline")

ホストのOSが再インストールされた場合、既存のOSDを再度アクティブにする必要があります。このような場合のために、cephadmはホスト上のすべての既存のOSDをアクティブにするactivateのラッパーを提供します。

```
# ceph cephadm osd activate <host>...
```

これにより、既存のすべてのディスクをスキャンしてOSDを探し、対応するデーモンを配置します。

## Futher Reading[¶](https://docs.ceph.com/en/pacific/cephadm/services/osd/ "Permalink to this headline")

* [ceph\-volume](https://docs.ceph.com/en/pacific/cephadm/services/osd/)
* [Ceph Storage Cluster](https://docs.ceph.com/en/pacific/cephadm/services/osd/)
