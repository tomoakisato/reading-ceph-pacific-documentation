# 441: batch

**クリップソース:** [441: batch — Ceph Documentation](https://docs.ceph.com/en/pacific/ceph-volume/lvm/batch/)

**[Report a Documentation Bug](https://docs.ceph.com/en/pacific/ceph-volume/lvm/batch/)**

# batch[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/batch/ "Permalink to this headline")

このサブコマンドは、デバイスの入力があれば、同時に複数のOSDを作成することができます。batchサブコマンドは、drive\-groupと密接に関連しています。1つのdrive\-group指定は、1つのbatch呼び出しに変換されます。

batchサブコマンドは[create](https://docs.ceph.com/en/pacific/ceph-volume/lvm/create/#ceph-volume-lvm-create)をベースにしており、全く同じコードパスを使用します。batchが行うのは、すべてのボリュームの適切なサイズを計算し、すでに作成されたボリュームをスキップすることだけです。

ceph\-volume lvm createがサポートする、dmcrypt、systemdユニットの起動回避、bluestoreやfilestoreの定義などの機能はすべてサポートされています。

## Automatic sorting of disks[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/batch/ "Permalink to this headline")

batchが単一のデータ・デバイス・リストのみを受け取り、他のオプションが渡された場合、ceph\-volumeはディスクを回転プロパティで自動ソートし、使用するオブジェクトストアに応じて非回転ディスクをblock.dbまたはjournalに使用します。回転かソリッドステートかに関係なく、すべてのデバイスをスタンドアロンOSDに使用する場合、\-\-no\-autoを渡します。例えば、[bluestore](https://docs.ceph.com/en/pacific/glossary/#term-bluestore)を使用し、\-\-no\-autoを指定しない場合、非推奨（deprecated）の動作は、渡されたデバイスに応じて、次のようにデプロイします：

1. デバイスはすべてHDD：1デバイスにつき1つのOSDが作成される
2. デバイスはすべてSSD：1デバイスにつき2つのOSDが作成される
3. デバイスはHDDとSSDの混在：HDDにデータを置き、SSDにできるだけ大きなblock.dbを作成する

注意: ceph\-volume lvm createの操作ではblock.walを使用できますが、autoの動作ではサポートされていません。

このデフォルトの自動ソート動作は現在では廃止されており、将来のリリースで変更される予定です。代わりに、\-\-auto オプションが渡されない限り、デバイスは自動的にソートされません。

block.db、block.wal、journalについては、明示的なデバイスリストを利用することを推奨します。

# Reporting[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/batch/ "Permalink to this headline")

デフォルトでは、batchは計算されたOSDレイアウトのレポートを印刷し、ユーザーに確認するよう求めます。これは、\-\-yesを渡すことでオーバーライドすることができます。

デプロイを要求された状態で何回か実行を試したい場合、\-\-reportを渡すことができます。ceph\-volumeはレポートを印刷した後に終了します。

次のような起動を考えてみましょう：

```
$ ceph-volume lvm batch --report /dev/sdb /dev/sdc /dev/sdd --db-devices /dev/nvme0n1
```

これにより、NVMEデバイスに外部DBとWALlボリュームを持つ3つのOSDがデプロイされます。

**pretty reporting** 

レポート形式（デフォルト）は、次のようになります：

```
$ ceph-volume lvm batch --report /dev/sdb /dev/sdc /dev/sdd --db-devices /dev/nvme0n1
--> passed data devices: 3 physical, 0 LVM
--> relative data size: 1.0
--> passed block_db devices: 1 physical, 0 LVM

Total OSDs: 3

  Type            Path                                                    LV Size         % of device
----------------------------------------------------------------------------------------------------
  data            /dev/sdb                                              300.00 GB         100.00%
  block_db        /dev/nvme0n1                                           66.67 GB         33.33%
----------------------------------------------------------------------------------------------------
  data            /dev/sdc                                              300.00 GB         100.00%
  block_db        /dev/nvme0n1                                           66.67 GB         33.33%
----------------------------------------------------------------------------------------------------
  data            /dev/sdd                                              300.00 GB         100.00%
  block_db        /dev/nvme0n1                                           66.67 GB         33.33%

```

**JSON reporting** 

\-\-formatjsonまたは\-\-formatjson\-prettyを使用すると、構造化された出力を生成できます：

```
$ ceph-volume lvm batch --report --format json-pretty /dev/sdb /dev/sdc /dev/sdd --db-devices /dev/nvme0n1
--> passed data devices: 3 physical, 0 LVM
--> relative data size: 1.0
--> passed block_db devices: 1 physical, 0 LVM
[
    {
        "block_db": "/dev/nvme0n1",
        "block_db_size": "66.67 GB",
        "data": "/dev/sdb",
        "data_size": "300.00 GB",
        "encryption": "None"
    },
    {
        "block_db": "/dev/nvme0n1",
        "block_db_size": "66.67 GB",
        "data": "/dev/sdc",
        "data_size": "300.00 GB",
        "encryption": "None"
    },
    {
        "block_db": "/dev/nvme0n1",
        "block_db_size": "66.67 GB",
        "data": "/dev/sdd",
        "data_size": "300.00 GB",
        "encryption": "None"
    }
]

```

# Sizing[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/batch/ "Permalink to this headline")

サイズ指定引数が渡されない場合、ceph\-volumeは渡されたデバイス・リスト\(または自動ソート使用時のソート済みリスト\)からサイズ指定を導出します。ceph\-volume batchは、デバイスの利用可能容量を完全に利用しようとします。自動サイズ設定に依存することをお勧めします。

wal、db、ジャーナルデバイスに異なるサイジングポリシーが必要な場合、ceph\-volumeは暗黙的および明示的なサイジングルールを提供します。

## Implicit sizing[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/batch/ "Permalink to this headline")

デバイス数が不足していたり、一部のデータデバイスが現在使用可能でない場合（たとえばディスクが壊れているなど）でも、ceph\-volumeによる自動サイジングに頼ることができます。ユーザは、高速デバイスに外部ボリュームを持つべきデータデバイスの数について、ceph\-volumeにヒントを提供することができます。これらのオプションは以下のとおりです：

* \-\-block\-db\-slots
* \-\-block\-wal\-slots
* \-\-journal\-slots

例えば、5つのデータデバイスとwal/dbボリューム用の1つのデバイスを含むことになっているOSDホストを考えてみましょう。しかし、1つのデータ・デバイスは現在故障しており、交換中です。wal/dbボリュームの明示的なサイズを計算する代わりに、単純に以下を呼び出すことができます：

```
$ ceph-volume lvm batch --report /dev/sdb /dev/sdc /dev/sdd /dev/sde --db-devices /dev/nvme0n1 --block-db-slots 5
```

## Explicit sizing[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/batch/ "Permalink to this headline")

引数を使用して、ceph\-volumeに明示的なサイズを指定することもできます。

* \-\-block\-db\-size
* \-\-block\-wal\-size
* \-\-journal\-size

ceph\-volumeは、渡されたディスクから要求されたサイズを満たそうとします。これができない場合、OSDはデプロイされません。

# Idempotency and disk replacements[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/batch/ "Permalink to this headline")

ceph\-volume lvm batchはべき等です。つまり、同じコマンドを繰り返し呼び出すと、同じ結果になるはずです。例えば、次のように呼び出した場合：

```
$ ceph-volume lvm batch --report /dev/sdb /dev/sdc /dev/sdd --db-devices /dev/nvme0n1
```

すべてのディスクが利用可能であった場合、3 つの OSDがデプロイされます。このコマンドを再度呼び出すと、やはり3つのOSDができ、ceph\-volumeはリターンコード0で終了します。

例えば、/dev/sdcの調子が悪くなり、交換が必要になったとします。OSDを破壊してハードウェアを交換した後、再び同じコマンドを呼び出すと、ceph\-volumeは3つのOSDのうち2つだけが設定されていることを検出し、足りないOSDを再作成します。

このべき等の概念は、[Advanced OSD サービス仕様](https://docs.ceph.com/en/pacific/cephadm/services/osd/#drivegroups)と密接に結び付き、広範囲に使用されています。
