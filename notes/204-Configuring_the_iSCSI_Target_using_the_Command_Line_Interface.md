# 204: Configuring the iSCSI Target using the Command Line Interface

**クリップソース:** [204: Configuring the iSCSI Target using the Command Line Interface — Ceph Documentation](https://docs.ceph.com/en/pacific/rbd/iscsi-target-cli/)

# Configuring the iSCSI Target using the Command Line Interface[¶](https://docs.ceph.com/en/pacific/rbd/iscsi-target-cli/ "Permalink to this headline")

Ceph iSCSIゲートウェイは、iSCSIターゲットとCephクライアントの両方です。CephのRBDインターフェースとiSCSIスタンダードの間の「トランスレータ」とお考えください。Ceph iSCSIゲートウェイは、スタンドアロンノードで実行するか、Ceph Object Store Disk（OSD）ノードなど、他のデーモンとコロケーションすることが可能です。 コロケーションする場合は、共有するために十分なCPUとメモリが利用可能であることを確認します。次の手順では、Ceph iSCSIゲートウェイのインストールと基本的な動作の設定を行います。

**Requirements:**

* Ceph Luminousまたはそれ以降のストレージクラスタが動作していること
* Red Hat Enterprise Linux/CentOS 7.5（以降）、Linux kernel v4.16（以降）
* targetcli\-2.1.fb47 or newer package
    python\-rtslib\-2.1.fb68 or newer package
    tcmu\-runner\-1.4.0 or newer package
    ceph\-iscsi\-3.2 or newer package
    以下のパッケージは、お使いのLinuxディストリビューションのソフトウェアリポジトリからインストールする必要があります：

重要：これらのパッケージの旧バージョンが存在する場合、新しいバージョンをインストールする前に、それらを削除する必要があります。

インストールセクションに進む前に、Ceph iSCSIゲートウェイノードで以下の手順を実行します：

1. Ceph iSCSIゲートウェイがOSDノードにコロケートされていない場合、/etc/ceph/にあるCeph構成ファイルをストレージクラスタの実行中のCephノードからiSCSIゲートウェイノードへコピーします。Ceph構成ファイルは、iSCSIゲートウェイノードの/etc/ceph/に存在する必要があります。
2. [Cephコマンドラインインターフェイス](https://docs.ceph.com/en/pacific/start/quick-rbd/#install-ceph)
3. 注意: ポート5000へのアクセスは、信頼できる内部ネットワーク、またはgwcliを使用するホストやceph\-mgrデーモンが動作する個々のホストのみに制限する必要があります。
    必要に応じて、ファイアウォールでTCPポート3260と5000を開放してください。
4. RADOS Block Device（RBD）を新規に作成するか、既存のRBDを使用します。

**Installing:**

アップストリームのceph\-iscsiパッケージを使用する場合は、[マニュアルインストールの手順](https://docs.ceph.com/en/pacific/rbd/iscsi-target-cli-manual-install)に従ってください。

rpmベースの手順では、以下のコマンドを実行します：

1. rootとして、すべてのiSCSIゲートウェイノードで、ceph\-iscsiパッケージをインストールします：

```
# yum install ceph-iscsi
```

1. rootとして、すべてのiSCSIゲートウェイノードに、tcmu\-runnerパッケージをインストールします：

```
# yum install tcmu-runner
```

**Setup:**

1. gwcliは、iSCSI設定のようなメタデータを保存できるように、rbdという名前のプールを必要とします。このプールが作成されているかどうかを確認するために、以下を実行します：

```
# ceph osd lspools
```

存在しない場合は、[RADOSのプール操作のページ](http://docs.ceph.com/en/latest/rados/operations/pools/)でプールの作成方法を確認することができます。

1. ルートとして、iSCSIゲートウェイノードで、/etc/ceph/ディレクトリに iscsi\-gateway.cfg という名前のファイルを作成します：

```
# touch /etc/ceph/iscsi-gateway.cfg
```

    1. iscsi\-gateway.cfg ファイルを編集し、以下の行を追加します：

```
[config]
# Name of the Ceph storage cluster. A suitable Ceph configuration file allowing
# access to the Ceph storage cluster from the gateway node is required, if not
# colocated on an OSD node.
cluster_name = ceph

# Place a copy of the ceph cluster's admin keyring in the gateway's /etc/ceph
# drectory and reference the filename here
gateway_keyring = ceph.client.admin.keyring

# API settings.
# The API supports a number of options that allow you to tailor it to your
# local environment. If you want to run the API under https, you will need to
# create cert/key files that are compatible for each iSCSI gateway node, that is
# not locked to a specific node. SSL cert and key files *must* be called
# 'iscsi-gateway.crt' and 'iscsi-gateway.key' and placed in the '/etc/ceph/' directory
# on *each* gateway node. With the SSL files in place, you can use 'api_secure = true'
# to switch to https mode.

# To support the API, the bare minimum settings are:
api_secure = false

# Additional API configuration options are as follows, defaults shown.
# api_user = admin
# api_password = admin
# api_port = 5001
# trusted_ip_list = 192.168.0.10,192.168.0.11
```

注：trusted\_ip\_listは、ターゲット作成、LUNエクスポートなどの管理操作に使用される、各iSCSIゲートウェイのIPアドレスのリストです。このIPは、RBDイメージとの間のREAD/WRITEコマンドのようなiSCSIデータに使用されるものと同じにすることができますが、別々のIPを使用することが推奨されます。

重要： iscsi\-gateway.cfg ファイルは、すべての iSCSI ゲートウェイノードで同一である必要があります。

2.  rootとして、iscsi\-gateway.cfg ファイルをすべての iSCSI ゲートウェイノードにコピーします。

1. rootとして、すべての iSCSI ゲートウェイノードで、API サービスを有効化し、開始します：

```
# systemctl daemon-reload

# systemctl enable rbd-target-gw
# systemctl start rbd-target-gw

# systemctl enable rbd-target-api
# systemctl start rbd-target-api
```

**Configuring:**

gwcli は iSCSI ターゲットと RBD イメージの作成と設定を行い、最後のセクションで設定したゲートウェイ間で設定をコピーします。targetcli と rbd を含む下位のツールは、ローカルの設定を照会するために使用できますが、それを変更するために使用すべきではありません。この次のセクションでは、iSCSI ターゲットを作成し、RBD イメージを LUN 0 としてエクスポートする方法を説明します。

1. iSCSIゲートウェイノードで、rootとして、iSCSIゲートウェイのコマンドラインインターフェイスを起動します：

```
# gwcli
```

1. iscsi\-targetsに移動し、iqn.2003\-01.com.redhat.iscsi\-gw:iscsi\-igwという名前でターゲットを作成します：

```
> /> cd /iscsi-target
> /iscsi-target>  create iqn.2003-01.com.redhat.iscsi-gw:iscsi-igw
```

1. iSCSIゲートウェイを作成します。以下で使用するIPは、READやWRITEコマンドのようなiSCSIデータに使用されるものです。これらは、trusted\_ip\_listに記載されている管理操作に使用されるIPと同じであることができますが、異なるIPを使用することが推奨されます。

```
> /iscsi-target> cd iqn.2003-01.com.redhat.iscsi-gw:iscsi-igw/gateways
> /iscsi-target...-igw/gateways>  create ceph-gw-1 10.172.19.21
> /iscsi-target...-igw/gateways>  create ceph-gw-2 10.172.19.22
```

RHEL/CentOS を使用していない場合、またはアップストリームもしくは ceph\-iscsi\-test カーネルを使用している場合、skipchecks=true 引数を使用する必要があります。これにより、Red Hat カーネルと rpm のチェックを回避することができます：

```
> /iscsi-target> cd iqn.2003-01.com.redhat.iscsi-gw:iscsi-igw/gateways
> /iscsi-target...-igw/gateways>  create ceph-gw-1 10.172.19.21 skipchecks=true
> /iscsi-target...-igw/gateways>  create ceph-gw-2 10.172.19.22 skipchecks=true
```

1. プールrbdにdisk\_1という名前のRBDイメージを追加します：

```
> /iscsi-target...-igw/gateways> cd /disks
> /disks> create pool=rbd image=disk_1 size=90G
```

1. イニシエータ名 iqn.1994\-05.com.redhat:rh7\-client でクライアントを作成します：

```
> /disks> cd /iscsi-target/iqn.2003-01.com.redhat.iscsi-gw:iscsi-igw/hosts
> /iscsi-target...eph-igw/hosts>  create iqn.1994-05.com.redhat:rh7-client
```

1. クライアントのCHAPユーザー名をmyiscsiusernameに、パスワードをmyiscsipasswordに設定します：

```
> /iscsi-target...at:rh7-client>  auth username=myiscsiusername password=myiscsipassword
```

警告： CHAPは常に設定する必要があります。CHAPを使用しない場合、ターゲットはすべてのログイン要求を拒否します。

1. ディスクをクライアントに追加します：

```
> /iscsi-target...at:rh7-client> disk add rbd/disk_1
```

次のステップでは、iSCSIイニシエータを設定します。
