# 170: Advanced: Metadata repair tools

**クリップソース:** [170: Advanced: Metadata repair tools — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/disaster-recovery-experts/)

# Advanced: Metadata repair tools[¶](https://docs.ceph.com/en/pacific/cephfs/disaster-recovery-experts/ "Permalink to this headline")

警告：CephFS内部に関する専門知識がない場合、これらのツールを使用する前に支援を求める必要があります。

ここで紹介するツールは、修理するだけでなく、簡単に破損の原因にもなります。

ファイルシステムの修復を試みる前に、何が問題になっているのかを正確に理解することが重要です。

クラスタの専門サポートを受けられない場合は、ceph\-usersメーリングリストや\#ceph IRCチャンネルを参照してください。

## Journal export[¶](https://docs.ceph.com/en/pacific/cephfs/disaster-recovery-experts/ "Permalink to this headline")

危険な操作をする前に、以下のようにジャーナルをコピーしてください：

```
cephfs-journal-tool journal export backup.bin
```

なお、ジャーナルがひどく破損している場合、このコマンドは常に動作するとは限りません。この場合、RADOSレベルのコピーを作成する必要があります\([http://tracker.ceph.com/issues/9902](http://tracker.ceph.com/issues/9902)\)。

## Dentry recovery from journal[¶](https://docs.ceph.com/en/pacific/cephfs/disaster-recovery-experts/ "Permalink to this headline")

ジャーナルが破損したり、何らかの理由でMDSが再生できない場合、可能な限りファイルのメタデータを復元するよう試みます：

```
cephfs-journal-tool event recover_dentries summary
```

このコマンドはデフォルトでMDSのランク0に作用し、他のランクに作用させるには\-\-rank=\<n\>を渡します。

このコマンドは、ジャーナルからバッキング・ストアに復元可能なすべてのinode/dentry（バッキング・ストアのコンテンツよりもバージョンが大きいinode/dentry）を書き込みます。 ジャーナルに欠落や破損がある場合、その領域はスキップされます。

このコマンドは、dentryとinodeの書き出しに加えて、書き出されたinodeの番号が現在使用されていることを示すために、「in」状態の各MDSランクのInoTableを更新することに注意してください。 単純なケースでは、これで完全に有効なバッキングストア状態になります。

警告: 結果としてのバッキングストアの状態は自己一貫性が保証されておらず、その後オンラインMDSスクラブが必要になります。 ジャーナルの内容はこのコマンドによって変更されないため、可能な限り復旧した後、ジャーナルを別途切り詰める必要があります。

## Journal truncation[¶](https://docs.ceph.com/en/pacific/cephfs/disaster-recovery-experts/ "Permalink to this headline")

ジャーナルが破損していたり、MDSが何らかの理由で再生できない場合は、このようにトランケートすることができます：

```
cephfs-journal-tool [--rank=N] journal reset
```

ファイルシステムに複数のアクティブなMDSがある/あった場合、\-\-rankオプションでMDSのランクを指定します。

警告：ジャーナルをリセットすると、recover\_dentriesなどの他の手段で抽出した場合を除き、メタデータが失われます。 また、データ・プールに孤立したオブジェクトが残る可能性があります。 既に書き込まれている inode を再割り当てすることになり、パーミッションのルールに違反する可能性があります。

## MDS table wipes[¶](https://docs.ceph.com/en/pacific/cephfs/disaster-recovery-experts/ "Permalink to this headline")

ジャーナルをリセットすると、MDSのテーブル（InoTable、SessionMap、SnapServer）の内容との整合性が取れなくなる場合があります。

SessionMapをリセットする（すべてのセッションを消去する）には、以下を使用します：

```
cephfs-table-tool all reset session
```

このコマンドは、すべての「in」状態のMDSランクのテーブルに対して作用します。 「all」をMDSランクに置き換えると、そのランクに対してのみ動作します。

セッション・テーブルが最もリセットが必要なテーブルですが、他のテーブルもリセットする必要があることが分かっている場合は、「session」を「snap」または「inode」に置き換えてください。

## MDS map reset[¶](https://docs.ceph.com/en/pacific/cephfs/disaster-recovery-experts/ "Permalink to this headline")

ファイルシステムのIn\-RADOS状態（つまりメタデータプールの内容）がある程度回復したら、メタデータプールの内容を反映するためにMDSマップを更新することが必要になる場合があります。 以下のコマンドを使用して、MDSマップを単一のMDSにリセットします：

```
ceph fs reset <fs name> --yes-i-really-mean-it
```

これを実行すると、MDSのランクが0以外のRADOS中の状態は無視されます。

「fs reset」と「fs remove」の主な違いは、remove / newを実行すると、ランク0が「creating」の状態のままになり、ディスク上の既存のルートiノードが上書きされ、既存のファイルが孤立することです。 対照的に、「reset」コマンドはランク0を「active」状態のままにして、ランクを要求する次のMDSデーモンが先に進み、既存のRADOS内メタデータを使用するようにします。

## Recovery from missing metadata objects[¶](https://docs.ceph.com/en/pacific/cephfs/disaster-recovery-experts/ "Permalink to this headline")

どのオブジェクトが欠けたり、破損しているかによって、異なるコマンドを実行してオブジェクトのデフォルトバージョンを再生成する必要があります。

```
# Session table
cephfs-table-tool 0 reset session
# SnapServer
cephfs-table-tool 0 reset snap
# InoTable
cephfs-table-tool 0 reset inode
# Journal
cephfs-journal-tool --rank=0 journal reset
# Root inodes ("/" and MDS directory)
cephfs-data-scan init
```

最後に、データプールの内容に基づいて、見つからないファイルやディレクトリのメタデータオブジェクトを再生成することができます。 これは、3段階のプロセスです。 まず、すべてのオブジェクトをスキャンして、inodeのサイズとmtimeのメタデータを計算します。 第2に、すべてのファイルから最初のオブジェクトをスキャンして、このメタデータを収集し、メタデータ・プールに注入します。第3に、inodeのリンクをチェックし、見つかったエラーを修正します。

```
cephfs-data-scan scan_extents <data pool>
cephfs-data-scan scan_inodes <data pool>
cephfs-data-scan scan_links
```

scan\_extents' と 'scan\_inodes' コマンドは、データプールに多くのファイルや大きなファイルがある場合、長い時間がかかることがあります。

処理を高速化するために、ツールを複数インスタンス実行します。

ワーカーの数を決め、各ワーカーに0〜\(worker\_m \- 1\)の範囲内の数字を渡します。

以下の例では、4つのワーカーを同時に動作させる方法を示しています：

```
# Worker 0
cephfs-data-scan scan_extents --worker_n 0 --worker_m 4 <data pool>
# Worker 1
cephfs-data-scan scan_extents --worker_n 1 --worker_m 4 <data pool>
# Worker 2
cephfs-data-scan scan_extents --worker_n 2 --worker_m 4 <data pool>
# Worker 3
cephfs-data-scan scan_extents --worker_n 3 --worker_m 4 <data pool>

# Worker 0
cephfs-data-scan scan_inodes --worker_n 0 --worker_m 4 <data pool>
# Worker 1
cephfs-data-scan scan_inodes --worker_n 1 --worker_m 4 <data pool>
# Worker 2
cephfs-data-scan scan_inodes --worker_n 2 --worker_m 4 <data pool>
# Worker 3
cephfs-data-scan scan_inodes --worker_n 3 --worker_m 4 <data pool>
```

ワーカーがscan\_inodesフェーズに入る前に、すべてのワーカーがscan\_extentsフェーズを終了していることを確認することが重要です。

メタデータのリカバリが完了したら、クリーンアップ操作を実行して、リカバリ中に生成された補助データを削除することをお勧めします。

```
cephfs-data-scan cleanup <data pool>
```

## Using an alternate metadata pool for recovery[¶](https://docs.ceph.com/en/pacific/cephfs/disaster-recovery-experts/ "Permalink to this headline")

注意：この手順については、広範なテストが行われていません。十分な注意を払いながら行う必要があります。

既存のファイルシステムが損傷して動作しなくなった場合、新しいメタデータプールを作成し、古いメタデータを残して、この新しいプールにファイルシステムのメタデータを再構築することが可能です。この場合、既存のメタデータプールは変更されないので、より安全に復旧を試みることができます。

注意：このプロセスでは、複数のメタデータプールに同じデータプールを参照するデータが含まれます。この間、データプールの内容を変更しないよう、十分な注意が必要です。復元が完了したら、破損したメタデータプールをアーカイブまたは削除する必要があります。

まず、既存のファイルシステムを削除していない場合は、データプールがさらに変更されないようにする必要があります。すべてのクライアントをアンマウントし、ファイルシステムをfailedとマークします：

```
ceph fs fail <fs_name>
```

次に、元のデータプールに裏打ちされた新しいメタデータプールを設定するリカバリファイルシステムを作成します。

```
ceph fs flag set enable_multiple true --yes-i-really-mean-it
ceph osd pool create cephfs_recovery_meta
ceph fs new cephfs_recovery recovery <data_pool> --allow-dangerous-metadata-overlay
```

リカバリファイルシステムは、新しいメタデータプールをメタデータで初期化するMDSランクで開始します。 これは、リカバリをブートストラップするために必要です。 ただし、メタデータプールとこれ以上相互作用させたくないので、ここでMDSを停止させます。

```
ceph fs fail cephfs_recovery
```

次に、MDSが作成した初期メタデータをリセットします：

```
cephfs-table-tool cephfs_recovery:all reset session
cephfs-table-tool cephfs_recovery:all reset snap
cephfs-table-tool cephfs_recovery:all reset inode
```

ここで、データプールからメタデータプールのリカバリーを実行します：

```
cephfs-data-scan init --force-init --filesystem cephfs_recovery --alternate-pool cephfs_recovery_meta
cephfs-data-scan scan_extents --alternate-pool cephfs_recovery_meta --filesystem <fs_name> <data_pool>
cephfs-data-scan scan_inodes --alternate-pool cephfs_recovery_meta --filesystem <fs_name> --force-corrupt <data_pool>
cephfs-data-scan scan_links --filesystem cephfs_recovery
```

注：上記の各スキャン手順は、データプール全体を通過します。これには時間がかかる可能性があります。このタスクを複数ワーカーに分散させる方法については、前のセクションを参照してください。

破損したファイルシステムにダーティなジャーナルデータが含まれている場合、次の方法で回復できる可能性があります：

```
cephfs-journal-tool --rank=<fs_name>:0 event recover_dentries list --alternate-pool cephfs_recovery_meta
cephfs-journal-tool --rank cephfs_recovery:0 journal reset --force
```

リカバリ後、リカバリされたディレクトリの統計情報が不正確になります。MDSが統計情報をチェックしないように、パラメータ mds\_verify\_scatter と mds\_debug\_scatterstat が false（デフォルト）に設定されていることを確認してください：

```
ceph config rm mds mds_verify_scatter
ceph config rm mds mds_debug_scatterstat
```

\(注意: グローバルまたはceph.confファイルで設定されている場合もあります。\) ここで、MDSがリカバリファイルシステムに参加することを許可します：

```
ceph fs set cephfs_recovery joinable true
```

最後に、フォワード[スクラブ](https://docs.ceph.com/en/pacific/cephfs/scrub/)を実行して、統計情報を修復してください。MDSが稼働していることを確認し、実行します：

```
ceph fs status # get active MDS
ceph tell mds.<id> scrub start / recursive repair
```

注：シンボリックリンクは空の通常ファイルとして復元されます。[シンボリックリンクの復元](https://tracker.ceph.com/issues/46166)は、Pacificでサポートされる予定です。

リカバリーファイルシステムからデータを移行する場合は、できるだけ早く行うことをお勧めします。リカバリーファイルシステムが稼働している間は、旧ファイルシステムをリストアしないでください。

注：データプールも破損している場合、バックトレース情報が失われるため、一部のファイルが復元されないことがあります。データプールでプレースメントグループが失われるなどして、データオブジェクトが欠落している場合、復元されるファイルには欠落したデータの代わりホールが含まれます。
