# 220: rbd-replay-prep – prepare captured rados block device (RBD) workloads for replay

 # rbd\-replay\-prep – prepare captured rados block device \(RBD\) workloads for replay[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay-prep/ "Permalink to this headline")

## Synopsis[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay-prep/ "Permalink to this headline")

**rbd\-replay\-prep** \[ –window _seconds_ \] \[ –anonymize \] _trace\_dir_ _replay\_file_

## Description[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay-prep/ "Permalink to this headline")

**rbd\-replay\-prep** processes raw rados block device \(RBD\) traces to prepare them for **rbd\-replay**.

## Options[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay-prep/ "Permalink to this headline")

`--window`` seconds`[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay-prep/ "Permalink to this definition")Requests further apart than ‘seconds’ seconds are assumed to be independent.

`--anonymize```[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay-prep/ "Permalink to this definition")Anonymizes image and snap names.

`--verbose```[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay-prep/ "Permalink to this definition")Print all processed events to console

## Examples[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay-prep/ "Permalink to this headline")

To prepare workload1\-trace for replay:

```
rbd-replay-prep workload1-trace/ust/uid/1000/64-bit workload1
```

## Availability[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay-prep/ "Permalink to this headline")

**rbd\-replay\-prep** is part of Ceph, a massively scalable, open\-source, distributed storage system. Please refer to the Ceph documentation at [http://ceph.com/docs](https://docs.ceph.com/en/pacific/man/8/rbd-replay-prep/) for more information.

## See also[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay-prep/ "Permalink to this headline")

[rbd\-replay](https://docs.ceph.com/en/pacific/man/8/rbd-replay-prep/)\(8\),[rbd](https://docs.ceph.com/en/pacific/man/8/rbd-replay-prep/)\(8\)
