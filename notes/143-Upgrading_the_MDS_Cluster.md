# 143: Upgrading the MDS Cluster

**クリップソース:** [143: Upgrading the MDS Cluster — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/upgrading/)

**[Report a Documentation Bug](https://docs.ceph.com/en/pacific/cephfs/upgrading/)**

# Upgrading the MDS Cluster[¶](https://docs.ceph.com/en/pacific/cephfs/upgrading/ "Permalink to this headline")

現在、MDSクラスタには、互換性のないメッセージやその他の機能の違いによるアサーションやその他の障害を引き起こさずに、MDSのシームレスなアップグレードをサポートするためのバージョン管理やファイルシステムフラグがありません。 このため、クラスタのアップグレード中は、ファイルシステムのアクティブなMDSの数を最初に1つに減らして、2つのアクティブなMDSが異なるバージョン間で通信しないようにする必要があります。

MDSクラスタの適切なアップグレード順序は次のとおりです：

1. 各ファイルシステムで、standby\-replayデーモンを無効化し、停止します。

```
ceph fs set <fs_name> allow_standby_replay false
```

Pacificでは、このコマンドを実行すると、standby\-replayデーモンが停止されます。Cephの古いバージョンでは、これらのdaemonを手動で停止する必要があります。

```
ceph fs dump # find standby-replay daemons
ceph mds fail mds.<X>
```

1. 各ファイルシステムで、ランク数を1に減らします：

```
ceph fs set <fs_name> max_mds 1
```

1. クラスタがランク0以外を停止するのを待ちます。ランク0のみがアクティブで、残りはスタンバイです。

```
ceph status # wait for MDS to finish stopping
```

1. 各MDSについて、パッケージをアップグレードし、再起動します。注意：フェイルオーバーを減らすために、スタンバイデーモンを最初にアップグレードすることをお勧めしますが、必須ではありません。

```
# use package manager to update cluster
systemctl restart ceph-mds.target
```

1. 各ファイルシステムで、クラスタの以前のmax\_mdsとallow\_standby\_replayの設定を復元します：

```
ceph fs set <fs_name> max_mds <old_max_mds>
ceph fs set <fs_name> allow_standby_replay <old_allow_standby_replay>
```

# Upgrading pre\-Firefly file systems past Jewel[¶](https://docs.ceph.com/en/pacific/cephfs/upgrading/ "Permalink to this headline")

ヒント: このアドバイスは、Firefly \(0.80\)よりも古いバージョンのCephを使用して作成されたファイルシステムを使用しているユーザにのみ適用されます。新しいファイルシステムを作成するユーザは、このアドバイスを無視してもかまいません。

CephのFirefly以前のバージョンでは、CephFSのディレクトリオブジェクトを保存するために、TMAPと呼ばれる現在では非推奨の形式が使用されています。 RADOSでこれらを読み取るためのサポートは、CephのJewelリリース後に削除されるため、CephFSユーザのアップグレードでは、古いディレクトリオブジェクトが変換されていることを確認することが重要です。

すべてのデータシートおよびOSDサーバーにJewelをインストールし、サービスを再起動した後、以下のコマンドを実行してください：

```
cephfs-data-scan tmap_upgrade <metadata pool name>
```

これは一度だけ実行する必要があり、実行中に他のサービスを停止する必要はありません。 このコマンドは、メタデータ・プールのオブジェクト全体を繰り返し処理するため、実行に時間がかかる場合があります。 実行中は、通常通りファイル・システムを使用しても問題ありません。 何らかの理由でコマンドが中断された場合は、単に再実行するのが安全です。

Firefly以前のCephFSファイルシステムをJewelより新しいCephバージョンにアップグレードする場合、最新バージョンへのアップグレードを完了する前に、まずJewelにアップグレードし、tmap\_upgradeコマンドを実行する必要があります。
