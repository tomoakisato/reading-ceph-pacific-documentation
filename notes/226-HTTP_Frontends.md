# 226: HTTP Frontends

**クリップソース:** [226: HTTP Frontends — Ceph Documentation](https://docs.ceph.com/en/pacific/radosgw/frontends/)

# [HTTP Frontends](https://docs.ceph.com/en/pacific/radosgw/frontends/)[¶](https://docs.ceph.com/en/pacific/radosgw/frontends/ "Permalink to this headline")

Contents

* [HTTP Frontends](https://docs.ceph.com/en/pacific/radosgw/frontends/)
    * [Beast](https://docs.ceph.com/en/pacific/radosgw/frontends/)
        * [Options](https://docs.ceph.com/en/pacific/radosgw/frontends/)
    * [Civetweb](https://docs.ceph.com/en/pacific/radosgw/frontends/)
        * [Options](https://docs.ceph.com/en/pacific/radosgw/frontends/)
    * [Generic Options](https://docs.ceph.com/en/pacific/radosgw/frontends/)

Ceph Object Gatewayは、rgw\_frontendsで設定可能な2つの組み込みHTTPフロントエンドライブラリをサポートしています。構文の詳細については、「[Config Reference](https://docs.ceph.com/en/pacific/radosgw/config-ref)」を参照してください。

## [Beast](https://docs.ceph.com/en/pacific/radosgw/frontends/)[¶](https://docs.ceph.com/en/pacific/radosgw/frontends/ "Permalink to this headline")

Mimicバージョンの新機能です。

beastフロントエンドは、HTTPパースにBoost.Beastライブラリ、非同期ネットワークI/OにBoost.Asioライブラリを使用しています。

### [Options](https://docs.ceph.com/en/pacific/radosgw/frontends/)[¶](https://docs.ceph.com/en/pacific/radosgw/frontends/ "Permalink to this headline")

**port and ssl\_port**

Description

ipv4 & ipv6 のリスニングポート番号。port=80 port=8000のように複数回指定できる

Type

Integer

Default

80

**endpoint and ssl\_endpoint**

Description

addressはドット付き10進数で表したIPv4アドレス文字列、または角括弧で囲んだ16進数で表したIPv6アドレスを指定する。IPv6エンドポイントを指定すると、v6のみをリッスンすることになる。オプションのポートのデフォルトは、endpointの場合は80、ssl\_endpointの場合は443。endpoint=\[::1\] endpoint=192.168.0.100:8000 のように複数回指定することができる

Type

Integer

Default

None

**ssl\_certificate**

Description

SSL対応エンドポイントに使用されるSSL証明書ファイルへのpath。pathの前にconfig://がある場合、証明書はceph monitorのconfig\-keyデータベースから取得される

Type

String

Default

None

**ssl\_private\_key**

Description

SSL対応エンドポイントに使用する秘密鍵ファイルへのpath（オプション）。省略した場合は、ssl\_certificate ファイルが秘密鍵として使用される。pathの前にconfig://がある場合、証明書はceph monitorのconfig\-keyデータベースから取得される

Type

String

Default

None

**ssl\_options**

Description

ssl コンテキストオプションのコロン区切りリスト（オプション）：

default\_workarounds 様々なバグ回避策を実装する。

no\_compression 圧縮を無効にする

no\_sslv2 SSL v2を無効にする

no\_sslv3 SSL v3を無効にする

no\_tlsv1 TLS v1 を無効にする

no\_tlsv1\_1 TLS v1.1を無効にする

no\_tlsv1\_2 TLS v1.2を無効にする

single\_dh\_use tmp\_dhパラメータを使用する場合は、常に新しいキーを作成する

Type

String

Default

no\_sslv2:no\_sslv3:no\_tlsv1:no\_tlsv1\_1

**ssl\_ciphers**

Description

コロンで区切られた 1 つまたは複数の暗号化文字列のオプションのリスト。文字列の形式は openssl の ciphers\(1\) マニュアルに記載

Type

String

Default

None

**tcp\_nodelay**

Description

socketオプションを設定すると、接続時にNagleのアルゴリズムを無効にし、バッファがいっぱいになったりタイムアウトが発生するのを待たずに、できるだけ早くパケットを送信するようになる

1 すべてのソケットでNagelアルゴリズムを無効にする

0 デフォルトのままにする。Nagelアルゴリズムを有効にする

Type

Integer \(0 or 1\)

Default

0

**max\_connection\_backlog**

Description

接続受付待ちのキューの最大サイズを定義するオプショナルな値。設定されていない場合、boost::asio::socket\_base::max\_connectionsの値が使用される

Type

Integer

Default

None

**request\_timeout\_ms**

Description

Beastがあきらめるまで、受信データまたは送信データを待つ時間（ミリ秒）。この値を0にすると、タイムアウトは無効になる

Type

Integer

Default

65000

## [Civetweb](https://docs.ceph.com/en/pacific/radosgw/frontends/)[¶](https://docs.ceph.com/en/pacific/radosgw/frontends/ "Permalink to this headline")

Fireflyバージョンの新機能。

Pacific バージョン以降、非推奨。

civetwebフロントエンドはMongooseのフォークであるCivetweb HTTPライブラリを使用します．

### [Options](https://docs.ceph.com/en/pacific/radosgw/frontends/)[¶](https://docs.ceph.com/en/pacific/radosgw/frontends/ "Permalink to this headline")

**port**

Description

リスニングポート番号。SSL対応ポートの場合は、443sのようにサフィックスをつける。特定のIPv4またはIPv6アドレスをバインドする場合は、address:portという形式を使う。127.0.0.1:8000\+443sのように複数のエンドポイントを\+で区切るか、port=8000 port=443sのように複数のオプションを指定することも可能

Type

String

Default

7480

**num\_threads**

Description

CivetwebがHTTP接続を処理するために生成するスレッド数。これは、フロントエンドが同時に処理できるコネクションの数を制限する

Type

Integer

Default

rgw\_thread\_pool\_size

**request\_timeout\_ms**

Description

Civetwebがあきらめるまで、受信データを待つ時間（ミリ秒）

Type

Integer

Default

30000

**ssl\_certificate**

Description

SSL対応ポートに使用されるSSL証明書ファイルへのpath

Type

String

Default

None

**access\_log\_file**

Description

アクセスログを記録するファイルへのpath。フルパス、または現在の作業ディレクトリからの相対パス。省略した場合（デフォルト）、アクセスはログに記録されない

Type

String

Default

EMPTY

**error\_log\_file**

Description

エラーログを記録するファイルへのpath。フルパス、または現在の作業ディレクトリからの相対パス。存在しない場合（デフォルト）、エラーは記録されない

Type

String

Default

EMPTY

以下は、これらのオプションのいくつかを設定した /etc/ceph/ceph.conf ファイルの例です：

```
[client.rgw.gateway-node1]
rgw_frontends = civetweb request_timeout_ms=30000 error_log_file=/var/log/radosgw/civetweb.error.log access_log_file=/var/log/radosgw/civetweb.access.log
```

対応するオプションの一覧は、[Civetwebのユーザーマニュアル](https://civetweb.github.io/civetweb/UserManual.html)に記載されています。

## [Generic Options](https://docs.ceph.com/en/pacific/radosgw/frontends/)[¶](https://docs.ceph.com/en/pacific/radosgw/frontends/ "Permalink to this headline")

いくつかのフロントエンドオプションは、すべてのフロントエンドでサポートされている汎用的なものです：

**prefix**

Description

すべてのリクエストの URI に挿入されるプレフィックス文字列。たとえば、Swift専用のフロントエンドは /swift という URI プレフィックスを指定できる

Type

String

Default

None
