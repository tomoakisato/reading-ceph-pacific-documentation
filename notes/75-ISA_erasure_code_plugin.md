# 75: ISA erasure code plugin

**クリップソース:** [75: ISA erasure code plugin — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-isa/)

# ISA erasure code plugin[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-isa/ "Permalink to this headline")

isaプラグインは、ISAライブラリをカプセル化します。

## Create an isa profile[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-isa/ "Permalink to this headline")

新しいisa ECプロファイルを作成するには：

```
ceph osd erasure-code-profile set {name} 
     plugin=isa 
     technique={reed_sol_van|cauchy} 
     [k={data-chunks}] 
     [m={coding-chunks}] 
     [crush-root={root}] 
     [crush-failure-domain={bucket-type}] 
     [crush-device-class={device-class}] 
     [directory={directory}] 
     [--force]
```

ここで：

**k={datachunks}**

Description

各オブジェクトはデータチャンク部分に分割され、それぞれが異なるOSDに格納される。

Type

Integer

Required

No.

Default

7

**m={coding\-chunks}**

Description

各オブジェクトのコーディングチャンクを計算し、異なるOSDに保存する。コーディングチャンクの数は、データを失うことなくダウンすることができるOSDの数でもある。

Type

Integer

Required

No.

Default

3

**technique={reed\_sol\_van|cauchy}**

Description

ISAプラグインには2種類のReed Solomon形式がある。reed\_sol\_vanがセットされていればVandermonde、cauchyがセットされていればCauchy

Type

String

Required

No.

Default

reed\_sol\_van

**crush\-root={root}**

Description

CRUSHルールの最初のステップに使用されるCRUSHバケットの名前

Type

String

Required

No.

Default

default

**crush\-failure\-domain={bucket\-type}**

Description

同じ障害ドメインを持つバケットに、2つのチャンクが存在しないことを確認する。例えば、障害ドメインがhostの場合、2つのチャンクが同じhostに保存されることはない。step chooseleaf hostのようなCRUSHルールのステップを作成するために使用される

Type

String

Required

No.

Default

host

**crush\-device\-class={device\-class}**

Description

CRUSHマップのcrushデバイスクラス名を使って、特定のクラスのデバイス（例：ssdやhdd）に配置を制限する。

Type

String

Required

No.

Default

**directory={directory}**

Description

ECプラグインの読み込み元となるディレクトリ名

Type

String

Required

No.

Default

/usr/lib/ceph/erasure\-code

**\-\-force**

Description

同名の既存プロファイルを上書きする

Type

String

Required

No.
