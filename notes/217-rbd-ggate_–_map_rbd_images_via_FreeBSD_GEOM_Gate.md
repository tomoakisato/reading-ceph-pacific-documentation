# 217: rbd-ggate – map rbd images via FreeBSD GEOM Gate

 # rbd\-ggate – map rbd images via FreeBSD GEOM Gate[¶](https://docs.ceph.com/en/pacific/man/8/rbd-ggate/ "Permalink to this headline")

## Synopsis[¶](https://docs.ceph.com/en/pacific/man/8/rbd-ggate/ "Permalink to this headline")

**rbd\-ggate** \[–read\-only\] \[–exclusive\] \[–device _ggate device_\] map _image\-spec_ | _snap\-spec_
**rbd\-ggate** unmap _ggate device_
**rbd\-ggate** list

## Description[¶](https://docs.ceph.com/en/pacific/man/8/rbd-ggate/ "Permalink to this headline")

**rbd\-ggate** is a client for RADOS block device \(rbd\) images. It will map a rbd image to a ggate \(FreeBSD GEOM Gate class\) device, allowing access it as regular local block device.

## Commands[¶](https://docs.ceph.com/en/pacific/man/8/rbd-ggate/ "Permalink to this headline")

### map[¶](https://docs.ceph.com/en/pacific/man/8/rbd-ggate/ "Permalink to this headline")

Spawn a process responsible for the creation of ggate device and forwarding I/O requests between the GEOM Gate kernel subsystem and RADOS.

### unmap[¶](https://docs.ceph.com/en/pacific/man/8/rbd-ggate/ "Permalink to this headline")

Destroy ggate device and terminate the process responsible for it.

### list[¶](https://docs.ceph.com/en/pacific/man/8/rbd-ggate/ "Permalink to this headline")

List mapped ggate devices.

## Options[¶](https://docs.ceph.com/en/pacific/man/8/rbd-ggate/ "Permalink to this headline")

`--device`` *ggate device*`[¶](https://docs.ceph.com/en/pacific/man/8/rbd-ggate/ "Permalink to this definition")Specify ggate device path.

`--read-only```[¶](https://docs.ceph.com/en/pacific/man/8/rbd-ggate/ "Permalink to this definition")Map read\-only.

`--exclusive```[¶](https://docs.ceph.com/en/pacific/man/8/rbd-ggate/ "Permalink to this definition")Forbid writes by other clients.

## Image and snap specs[¶](https://docs.ceph.com/en/pacific/man/8/rbd-ggate/ "Permalink to this headline")

_image\-spec_ is \[_pool\-name_\]/_image\-name_
_snap\-spec_ is \[_pool\-name_\]/_image\-name_@_snap\-name_

The default for _pool\-name_ is “rbd”. If an image name contains a slash character \(‘/’\), _pool\-name_ is required.

## Availability[¶](https://docs.ceph.com/en/pacific/man/8/rbd-ggate/ "Permalink to this headline")

**rbd\-ggate** is part of Ceph, a massively scalable, open\-source, distributed storage system. Please refer to the Ceph documentation at[http://ceph.com/docs](https://docs.ceph.com/en/pacific/man/8/rbd-ggate/) for more information.

## See also[¶](https://docs.ceph.com/en/pacific/man/8/rbd-ggate/ "Permalink to this headline")

[rbd](https://docs.ceph.com/en/pacific/man/8/rbd-ggate/)\(8\)[ceph](https://docs.ceph.com/en/pacific/man/8/rbd-ggate/)\(8\)
