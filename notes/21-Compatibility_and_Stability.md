# 21: Compatibility and Stability

**クリップソース:** [21: Compatibility and Stability — Ceph Documentation](https://docs.ceph.com/en/pacific/cephadm/compatibility/)

# Compatibility and Stability[¶](https://docs.ceph.com/en/pacific/cephadm/compatibility/#compatibility-and-stability "Permalink to this headline")

## Compatibility with Podman Versions[¶](https://docs.ceph.com/en/pacific/cephadm/compatibility/#compatibility-with-podman-versions "Permalink to this headline")

PodmanとCephはそれぞれ異なるエンドオブライフ戦略をとっているため、互換性のあるPodmanとCephのバージョンを見つけるのは難しいかもしれません。

それらのバージョンが動作することが期待されます。

|Ceph      |Podman|     |     |     |     |
|----------|------|-----|-----|-----|-----|
|          |1.9   |2.0  |2.1  |2.2  |3.0  |
|\<= 15.2.5|True  |False|False|False|False|
|\>= 15.2.6|True  |True |True |False|False|
|\>= 16.2.1|False |True |True |False|True |

Warning: Ceph Pacificで動作するのは、2.0.0以降のpodmanバージョンのみで、Ceph Pacificでは動作しないpodmanバージョン2.2.1は例外です。

## Stability[¶](https://docs.ceph.com/en/pacific/cephadm/compatibility/#stability "Permalink to this headline")

Cephadmは現在開発中です。一部の機能はまだ粗削りであることをご了承ください。特に以下のコンポーネントはcephadmで動作していますが、ドキュメントの完成度が低く、近い将来に変更があるかもしれません。

* RGW

以下の機能に対するCephadmのサポートは現在開発中であり、将来のリリースでは変更される可能性があります。

* Ingress
* Cephadm exporter daemon
* cephfs\-mirror

問題が発生した場合は、「 [Pausing or disabling cephadm](https://docs.ceph.com/en/pacific/cephadm/troubleshooting/#cephadm-pause).」も参照してください。[37: Troubleshooting](37-Troubleshooting.md)
