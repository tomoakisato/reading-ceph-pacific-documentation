# 24: Service Management

**クリップソース:** [24: Service Management — Ceph Documentation](https://docs.ceph.com/en/pacific/cephadm/services/)

# Service Management[¶](https://docs.ceph.com/en/pacific/cephadm/services/#service-management "Permalink to this headline")

サービスとは、一緒に設定されたデーモンのグループのことです。個々のサービスの詳細については、これらの章を参照してください。

* [MON Service](https://docs.ceph.com/en/pacific/cephadm/services/mon/)
* [MGR Service](https://docs.ceph.com/en/pacific/cephadm/services/mgr/)
* [OSD Service](https://docs.ceph.com/en/pacific/cephadm/services/osd/)
* [RGW Service](https://docs.ceph.com/en/pacific/cephadm/services/rgw/)
* [MDS Service](https://docs.ceph.com/en/pacific/cephadm/services/mds/)
* [NFS Service](https://docs.ceph.com/en/pacific/cephadm/services/nfs/)
* [iSCSI Service](https://docs.ceph.com/en/pacific/cephadm/services/iscsi/)
* [Custom Container Service](https://docs.ceph.com/en/pacific/cephadm/services/custom-container/)
* [Monitoring Services](https://docs.ceph.com/en/pacific/cephadm/services/monitoring/)

## Service Status[¶](https://docs.ceph.com/en/pacific/cephadm/services/#service-status "Permalink to this headline")

Cephクラスタで実行されているサービスの1つのステータスを確認するには、次のようにします。

1. コマンドラインを使用して、サービスのリストを印刷します。

2. ステータスを確認したいサービスを探します。

3. サービスの状態を印刷します。

次のコマンドは、オーケストレータに知られているサービスのリストを表示します。指定したホスト上のサービスのみに出力を限定するには、オプションの\-\-hostパラメータを使用します。特定のタイプのサービスのみに出力を限定するには、オプションの \-\-type パラメーター \(mon, osd, mgr, mds, rgw\) を使用します。

```
# ceph orch ls [--service_type type] [--service_name name] [--export] [--format f] [--refresh]
```

特定のサービスやデーモンのステータスを確認することができます。

```
# ceph orch ls --service_type type --service_name <name> [--refresh]
```

オーケストレーターに知っているサービス仕様をエクスポートするには、次のコマンドを実行します。

```
# ceph orch ls --export
```

このコマンドでエクスポートされたサービス仕様はyamlとしてエクスポートされ、そのyamlはceph orch apply \-iコマンドで使用できます。

単一のサービスの仕様を検索する方法（コマンドの例を含む）については、「[Retrieving the running Service Specification](https://docs.ceph.com/en/pacific/cephadm/services/#orchestrator-cli-service-spec-retrieve).」を参照してください。

## Daemon Status[¶](https://docs.ceph.com/en/pacific/cephadm/services/#daemon-status "Permalink to this headline")

デーモンとは、実行中のサービスの一部であるsystemdユニットのことです。

デーモンの状態を見るには、次のようにします。

1.オーケストレーターに知られているすべてのデーモンのリストを印刷します。

2. 対象となるデーモンの状態を問い合わせます。

まず、オーケストレーターに知られているすべてのデーモンのリストを表示します。

```
# ceph orch ps [--hostname host] [--daemon_type type] [--service_name name] [--daemon_id id] [--format f] [--refresh]
```

 次に、特定のサービスインスタンス（mon、osd、mds、rgw）の状態を問い合わせます。OSDの場合、idは数値化されたOSD IDです。MDSサービスの場合、idはファイルシステム名です。

```
# ceph orch ps --daemon_type osd --daemon_id 0
```

## Service Specification[¶](https://docs.ceph.com/en/pacific/cephadm/services/#service-specification "Permalink to this headline")

サービス仕様書は、サービスの展開を指定するために使用されるデータ構造です。 ここでは、YAMLによるサービス仕様書の例を示します。

```
service_type: rgw
service_id: realm.zone
placement:
  hosts:
    - host1
    - host2
    - host3
unmanaged: false
networks:
- 192.169.142.0/24
spec:
  # Additional service specific attributes.

```

この例でのサービス仕様のプロパティ：

```
class ceph.deployment.service_spec.ServiceSpec(service_type, service_id=None, placement=None, count=None, config=None, unmanaged=False, preview_only=False, networks=None)¶
```

サービス作成の詳細です。

MDS、RGW、iscsiゲートウェイ、MON、MGR、Prometheusなどのデーモンのクラスタに対するオーケストレーターへのリクエスト

この構造は、サービスを開始するための十分な情報となるはずです。

```
networks:List[str]¶
```

ネットワークIDのリストで、このリストに含まれる特定のネットワークにのみデーモンをバインドするように指示します。クラスタが複数のネットワークに分散している場合には、複数のネットワークを追加することができます。[Networks and Ports](https://docs.ceph.com/en/pacific/cephadm/services/monitoring/#cephadm-monitoring-networks-ports),、[Specifying Networks](https://docs.ceph.com/en/pacific/cephadm/services/rgw/#cephadm-rgw-networks)、[Specifying Networks](https://docs.ceph.com/en/pacific/cephadm/services/mgr/#cephadm-mgr-networks)を参照してください。[33: Monitoring Services](33-Monitoring_Services.md)、[28: RGW Service](28-RGW_Service.md)、[26: MGR Service](26-MGR_Service.md)

```
placement:ceph.deployment.service_spec.PlacementSpec¶
```

[Daemon Placement](https://docs.ceph.com/en/pacific/cephadm/services/#orchestrator-cli-placement-spec)を参照してください。

```
service_id¶
```

サービスの名前です。iscsi、mds、nfs、osd、rgw、container、ingressでは必須です。

```
service_type¶
```

サービスの種類。Cephサービス\(mon、crash、mds、mgr、osdまたはrbd\-mirror\)、ゲートウェイ\(nfsまたはrgw\)、監視スタックの一部\(amertmanager、grafana、node\-exporterまたはprometheus\)、またはカスタムコンテナの\(container\)のいずれかである必要があります。

```
unmanaged¶
```

trueに設定すると、オーケストレーターは、このサービスに関連するデーモンのデプロイも削除も行いません。配置や他のすべてのプロパティは無視されます。これは、このサービスを一時的に管理したくない場合に便利です。cephadmについては、「 [Disabling automatic deployment of daemons](https://docs.ceph.com/en/pacific/cephadm/services/#cephadm-spec-unmanaged)」を参照してください。

各サービスタイプは、さらにサービス固有のプロパティを持つことができます。

mon、mgr、およびモニタリングタイプのサービス仕様には、service\_idは必要ありません。

osdタイプのサービスは、「 [Advanced OSD Service Specifications](https://docs.ceph.com/en/pacific/cephadm/services/osd/#drivegroups)」に記載されています。

_ceph orch apply \-i_ では、複数文書のYAMLファイルを送信することで、多くのサービス仕様を一度に適用することができます。

```
cat <<EOF | ceph orch apply -i -
service_type: mon
placement:
  host_pattern: "mon*"
---
service_type: mgr
placement:
  host_pattern: "mgr*"
---
service_type: osd
service_id: default_drive_group
placement:
  host_pattern: "osd*"
data_devices:
  all: true
EOF

```

### Retrieving the running Service Specification[¶](https://docs.ceph.com/en/pacific/cephadm/services/#retrieving-the-running-service-specification "Permalink to this headline")

_ceph orch apply..._でサービスを開始している場合は、Service Specificationを直接変更するのは複雑です。

サービス仕様を直接変更するのではなく、以下の手順で実行中のサービス仕様をエクスポートすることをお勧めします。

```
# ceph orch ls --service-name rgw.<realm>.<zone> --export > rgw.<realm>.<zone>.yaml
# ceph orch ls --service-type mgr --export > mgr.yaml
# ceph orch ls --export > cluster.yaml

```

その後、仕様書を変更し、上記のように再度適用することができます。

### Updating Service Specifications[¶](https://docs.ceph.com/en/pacific/cephadm/services/#updating-service-specifications "Permalink to this headline")

Ceph Orchestratorは、ServiceSpecで各サービスの宣言的な状態を維持します。

RGW HTTPポートの更新など、特定の操作については、既存の仕様を更新する必要があります。

現在のServiceSpecをリストします：

```
ceph orch ls --service_name=<service-name> --export > myservice.yaml
```

yamlファイルを更新します：

```
vi myservice.yaml
```

新しいServiceSpecを適用します：

```
ceph orch apply -i myservice.yaml [--dry-run]
```

## Daemon Placement[¶](https://docs.ceph.com/en/pacific/cephadm/services/#daemon-placement "Permalink to this headline")

オーケストレーターがサービスをデプロイするためには、どこにデーモンをデプロイするのか、どれくらいの数をデプロイするのかを知る必要があります。 これが配置指定の役割です。 配置指定は、コマンドライン引数またはYAMLファイルで渡すことができます。

注意:cephadmは\_no\_scheduleラベルのついたホストにはデーモンを配備しません;  [Special host labels](https://docs.ceph.com/en/pacific/cephadm/host-management/#cephadm-special-host-labels)を参照してください。

注意：applyコマンドは分かりにくい場合があります。そのため、YAML仕様を使用することをお勧めします。

各 _ceph orch apply \<サービス名\>_ コマンドは、前のコマンドよりも優先されます。適切な構文を使用しないと、作業を進めていくうちに作業が崩れてしまいます。

For example:

```
# ceph orch apply mon host1
# ceph orch apply mon host2
# ceph orch apply mon host3

```

この結果、モニターが適用されるホストはホスト3のみとなります。

\(最初のコマンドは、host1にモニタを作成します。次に、2番目のコマンドは、host1のモニタを破壊して、host2にモニタを作成します。そして、3つ目のコマンドは、host2のモニターを破壊し、host3にモニターを作成します。このシナリオでは、この時点で、ホスト3にのみモニターが存在します\)。

これら3つのホストそれぞれにモニターが適用されていることを確認するには、次のようなコマンドを実行します。

```
# ceph orch apply mon "host1,host2,host3"

```

複数のホストにモニタを適用するには、yamlファイルを使用する方法もあります。"ceph orch apply mon"コマンドを使用する代わりに、次の形式のコマンドを実行します。

```
# ceph orch apply -i file.yaml

```

以下は、サンプルのfile.yamlファイルです。

```
service_type: mon
placement:
  hosts:
   - host1
   - host2
   - host3

```

### Explicit placements[¶](https://docs.ceph.com/en/pacific/cephadm/services/#explicit-placements "Permalink to this headline")

デーモンは、ホストを指定するだけで、明示的に配置することができます。

```
# orch apply prometheus --placement="host1 host2 host3"

```

Or in YAML:

```
service_type: prometheus
placement:
  hosts:
    - host1
    - host2
    - host3

```

MONなどのサービスでは、ネットワークの仕様を一部強化する必要があります。

```
# orch daemon add mon --placement="myhost:[v2:1.2.3.4:3300,v1:1.2.3.4:6789]=name"

```

ここで、\[v2:1.2.3.4:3300,v1:1.2.3.4:6789\]はモニターのネットワークアドレス、=nameは新しいモニターの名前を指定します。

### Placement by labels[¶](https://docs.ceph.com/en/pacific/cephadm/services/#placement-by-labels "Permalink to this headline")

デーモンの配置を、特定のラベルに合致するホストに限定することができます。ラベル「mylabel」を適切なホストに設定するには、次のコマンドを実行します。

```
# ceph orch host label add *<hostname>* mylabel

```

現在のホストとラベルを表示するには、次のコマンドを実行します。

```
# ceph orch host ls

```

For example:

```
# ceph orch host label add host1 mylabel
# ceph orch host label add host2 mylabel
# ceph orch host label add host3 mylabel
# ceph orch host ls

```

```
HOST   ADDR   LABELS  STATUS
host1         mylabel
host2         mylabel
host3         mylabel
host4
host5

```

次に、以下のコマンドを実行して、cephadmにラベルに基づいてデーモンを展開するように指示します。

```
# orch apply prometheus --placement="label:mylabel"

```

Or in YAML:

```
service_type: prometheus
placement:
  label: "mylabel"

```

* See 

### Placement by pattern matching[¶](https://docs.ceph.com/en/pacific/cephadm/services/#placement-by-pattern-matching "Permalink to this headline")

デーモンはホスト上にも置くことができます。

```
# orch apply prometheus --placement='myhost[1-3]'

```

Or in YAML:

```
service_type: prometheus
placement:
  host_pattern: "myhost[1-3]"

```

すべてのホストにサービスを配置するには、「\*」を使用します。

```
# orch apply node-exporter --placement='*'

```

Or in YAML:

```
service_type: node-exporter
placement:
  host_pattern: "*"

```

### Changing the number of daemons[¶](https://docs.ceph.com/en/pacific/cephadm/services/#changing-the-number-of-daemons "Permalink to this headline")

countを指定すると、指定した数のデーモンのみが作成されます。

```
# orch apply prometheus --placement=3

```

ホストのサブセットにデーモンを配備するには、カウントを指定します。

```
# orch apply prometheus --placement="2 host1 host2 host3"

```

数がホストの数より多い場合、cephadmはホストごとに1つずつ展開します。

```
# orch apply prometheus --placement="3 host1 host2"

```

すぐ上のコマンドでは、Prometheusデーモンが2つになります。

また、YAMLを使って制限値を指定することもでき、以下のようになります。

```
service_type: prometheus
placement:
  count: 3

```

YAMLは、ホストの制限を指定するためにも使用できます。

```
service_type: prometheus
placement:
  count: 2
  hosts:
    - host1
    - host2
    - host3

```

### Algorithm description[¶](https://docs.ceph.com/en/pacific/cephadm/services/#algorithm-description "Permalink to this headline")

Cephadmの宣言的な状態は、配置仕様を含むサービス仕様のリストで構成されています。

Cephadmは、クラスタ内で実際に稼働しているデーモンのリストとサービス仕様のリストを継続的に比較します。Cephadmは、サービス仕様に準拠するために、必要に応じて新しいデーモンを追加し、古いデーモンを削除します。

Cephadmは、サービス仕様への準拠を維持するために次のことを行います。

Cephadmはまず、候補となるホストのリストを選択します。Cephadmは明示的なホスト名を探し、それらを選択します。cephadmは明示的なホスト名が見つからない場合、ラベルの仕様を探します。仕様にラベルが定義されていない場合、cephadmはホストパターンに基づいてホストを選択します。ホストパターンが定義されていない場合、最後の手段として、cephadmはすべての既知のホストを候補として選択します。

Cephadmは、サービスを実行している既存のデーモンを認識し、それらを移動しないようにします。

Cephadmは一定量のサービスの展開をサポートします。以下のサービス仕様を考えてみましょう。

```
service_type: mds
service_name: myfs
placement:
  count: 3
  label: myfs

```

このサービス仕様は、cephadmに、クラスタ全体でmyfsというラベルのついたホストに3つのデーモンを展開するよう指示します。

候補ホストに展開されたデーモンが3つ未満の場合、cephadmは新しいデーモンを展開するホストをランダムに選択します。

候補ホスト上に3つ以上のデーモンが展開されている場合、cephadmは既存のデーモンを削除します。

最後に、cephadmは候補ホストのリストの外にあるホスト上のデーモンを削除します。

注意:cephadmが考慮しなければならない特別なケースがあります。

配置指定で選択されたホストの数が要求された数よりも少ない場合、cephadmは選択されたホストにのみデプロイします。

## Removing a Service[¶](https://docs.ceph.com/en/pacific/cephadm/services/#removing-a-service "Permalink to this headline")

サービスを削除するには、そのサービスのすべてのデーモンの削除を含めて、次のように実行します。

```
$ ceph orch rm <service-name>

```

For example:

```
$ ceph orch rm rgw.myrgw

```

## Disabling automatic deployment of daemons[¶](https://docs.ceph.com/en/pacific/cephadm/services/#disabling-automatic-deployment-of-daemons "Permalink to this headline")

Cephadmは、サービスごとにデーモンの自動展開と削除を無効にすることをサポートしています。CLIでは、このための2つのコマンドをサポートしています。

サービスを完全に削除するには、「 [Removing a Service](https://docs.ceph.com/en/pacific/cephadm/services/#orch-rm).」を参照してください。

### Disabling automatic management of daemons[¶](https://docs.ceph.com/en/pacific/cephadm/services/#disabling-automatic-management-of-daemons "Permalink to this headline")

デーモンの自動管理を無効にするには、サービス仕様書（mgr.yaml）でunmanaged=Trueを設定します。

mgr.yaml:

```
service_type: mgr
unmanaged: true
placement:
  label: mgr

```

```
# ceph orch apply -i mgr.yaml

```

Note:サービス仕様にこの変更を適用すると、cephadmは新しいデーモンを展開しなくなります\(配置仕様が追加のホストと一致していても\)。

### Deploying a daemon on a host manually[¶](https://docs.ceph.com/en/pacific/cephadm/services/#deploying-a-daemon-on-a-host-manually "Permalink to this headline")

Note:このワークフローは非常に限定された使用例であり、稀な状況下でのみ使用されるべきものです。

デーモンを手動でホストに配置するには、以下の手順に従います。

サービスのサービス仕様を変更します。既存の仕様を取得し、unmanaged:trueを追加して、変更した仕様を適用します。

その後、以下の手順でデーモンを手動でデプロイします。

```
# ceph orch daemon add <daemon-type>  --placement=<placement spec>

```

For example :

```
# ceph orch daemon add mgr --placement=my_host

```

Note:サービス仕様からunmanaged:trueを削除すると、このサービスのreconciliationループが有効になり、配置仕様によってはデーモンの削除につながる可能性があります。

### Removing a daemon from a host manually[¶](https://docs.ceph.com/en/pacific/cephadm/services/#removing-a-daemon-from-a-host-manually "Permalink to this headline")

デーモンを手動で削除するには、次のような形式のコマンドを実行します。

```
# ceph orch daemon rm <daemon name>... [--force]

```

For example:

```
# ceph orch daemon rm mgr.my_host.xyzxyz

```

Note:マネージドサービス\(unmanaged=False\)の場合、cephadmは数秒後に新しいデーモンを自動的にデプロイします。

### See also[¶](https://docs.ceph.com/en/pacific/cephadm/services/#see-also "Permalink to this headline")

* 管理されていないOSDの特別な取り扱いについては、「
* See also 
