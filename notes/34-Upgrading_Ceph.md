# 34: Upgrading Ceph

**クリップソース:** [34: Upgrading Ceph — Ceph Documentation](https://docs.ceph.com/en/pacific/cephadm/upgrade/)

# Upgrading Ceph[¶](https://docs.ceph.com/en/pacific/cephadm/upgrade/ "Permalink to this headline")

Cephadmは、Cephをあるバグフィックスリリースから次のバグフィックスリリースに安全にアップグレードすることができます。 たとえば、v15.2.0\(最初のOctopusリリース\)から次のポイントリリースであるv15.2.1にアップグレードすることができます。

自動アップグレードプロセスは、Cephのベストプラクティスに従っています。 例えば、以下のようになります。

* アップグレードの順番は、マネージャ、モニタ、その他のデーモンの順になります。
* 各デーモンは、Cephがクラスタの利用可能性を維持することを示した後にのみ再起動されます。

注:アップグレード中は、CephクラスタのヘルスステータスがHEALTH\_WARNINGに切り替わる可能性があります。

注:クラスタのホストがオフラインの場合、アップグレードは一時停止します。

## Starting the upgrade[¶](https://docs.ceph.com/en/pacific/cephadm/upgrade/ "Permalink to this headline")

cephadmを使用してCephをアップグレードする前に、次のコマンドを実行して、すべてのホストが現在オンラインであり、クラスタが正常であることを確認します。

```
# ceph -s
```

特定のリリースにアップグレード（またはダウングレード）するには、以下のコマンドを実行します。

```
# ceph orch upgrade start --ceph-version <version>
```

例えば、v16.2.6にアップグレードする場合は、以下のコマンドを実行します。

```
# ceph orch upgrade start --ceph-version 15.2.1
```

Note:バージョン16.2.6からDocker Hubのレジストリが使われなくなったので、Dockerを使う場合はquay.ioのレジストリにあるイメージを指定する必要があります。

```
# ceph orch upgrade start --image quay.io/ceph/ceph:v16.2.6
```

## Monitoring the upgrade[¶](https://docs.ceph.com/en/pacific/cephadm/upgrade/ "Permalink to this headline")

以下のコマンドを実行して、\(1\)アップグレードが進行中かどうか、\(2\)クラスタがどのバージョンにアップグレードしているかを判断します。

```
# ceph orch upgrade status
```

### Watching the progress bar during a Ceph upgrade[¶](https://docs.ceph.com/en/pacific/cephadm/upgrade/ "Permalink to this headline")

アップグレード中は、cephのステータス出力にプログレスバーが表示されます。これは次のようなものです。

```
# ceph -s

[...]
  progress:
    Upgrade to docker.io/ceph/ceph:v15.2.1 (00h 20m 12s)
      [=======.....................] (time remaining: 01h 43m 31s)
```

### Watching the cephadm log during an upgrade[¶](https://docs.ceph.com/en/pacific/cephadm/upgrade/ "Permalink to this headline")

以下のコマンドを実行して、cephadmのログを見ます。

```
# ceph -W cephadm
```

## Canceling an upgrade[¶](https://docs.ceph.com/en/pacific/cephadm/upgrade/ "Permalink to this headline")

以下のコマンドを実行すれば、いつでもアップグレードプロセスを停止することができます。

```
# ceph orch upgrade stop
```

## Potential problems[¶](https://docs.ceph.com/en/pacific/cephadm/upgrade/ "Permalink to this headline")

アップグレードの際には、いくつかのヘルスアラートが発生することがあります。

### UPGRADE\_NO\_STANDBY\_MGR[¶](https://docs.ceph.com/en/pacific/cephadm/upgrade/ "Permalink to this headline")

このアラート\(UPGRADE\_NO\_STANDBY\_MGR\)は、Cephがアクティブなスタンバイマネージャデーモンを検出していないことを意味します。アップグレードを続行するには、Cephはアクティブなスタンバイマネージャデーモンを必要とします\(この文脈では、「第2のマネージャ」と考えることができます\)。

以下のコマンドを実行することで、Cephadmが2つ\(またはそれ以上\)のマネージャを実行するように設定されていることを確認できます。

```
# ceph orch apply mgr 2  # or more
```

既存のmgrデーモンの状態を確認するには、以下のコマンドを実行します。

```
# ceph orch ps --daemon-type mgr
```

既存のmgrデーモンが停止している場合は、以下のコマンドを実行して再起動を試みることができます。

```
# ceph orch daemon restart <name>
```

### UPGRADE\_FAILED\_PULL[¶](https://docs.ceph.com/en/pacific/cephadm/upgrade/ "Permalink to this headline")

このアラート\(UPGRADE\_FAILED\_PULL\)は、Cephが対象バージョンのコンテナイメージを引き出せなかったことを意味します。これは、存在しないバージョンまたはコンテナイメージを指定した場合\(たとえば「1.2.3」\)、またはクラスタ内の1つまたは複数のホストがコンテナレジストリに到達できない場合に発生します。

既存のアップグレードをキャンセルし、別のターゲットバージョンを指定するには、以下のコマンドを実行します。

```
# ceph orch upgrade stop
# ceph orch upgrade start --ceph-version <version>
```

## Using customized container images[¶](https://docs.ceph.com/en/pacific/cephadm/upgrade/ "Permalink to this headline")

ほとんどのユーザにとって、アップグレードには、アップグレード先のCephのバージョン番号を指定する以上の複雑なことは必要ありません。 このような場合、cephadmは、container\_image\_base設定オプション\(デフォルト: docker.io/ceph/ceph\)とvX.Y.Zのタグを組み合わせることで、使用する特定のCephコンテナイメージを探します。

しかし、必要であれば、任意のコンテナイメージにアップグレードすることも可能です。たとえば、次のコマンドは開発用のビルドにアップグレードします。

```
# ceph orch upgrade start --image quay.io/ceph-ci/ceph:recent-git-branch-name
```

利用可能なコンテナイメージの詳細については、「 [Ceph Container Images](https://docs.ceph.com/en/pacific/cephadm/upgrade/)」を参照してください。[14: Ceph Container Images](14-Ceph_Container_Images.md)
