# 421: Admin Operations — Ceph Documentation

 # Admin Operations[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

An admin API request will be done on a URI that starts with the configurable ‘admin’ resource entry point. Authorization for the admin API duplicates the S3 authorization mechanism. Some operations require that the user holds special administrative capabilities. The response entity type \(XML or JSON\) may be specified as the ‘format’ option in the request and defaults to JSON if not specified.

## Get Object[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

Get an existing object. NOTE: Does not require owner to be non\-suspended.

### Syntax[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

```
GET /{admin}/bucket?object&format=json HTTP/1.1
Host {fqdn}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

`bucket`

DescriptionThe bucket containing the object to be retrieved.

TypeString

Example`foo_bucket`

RequiredYes

`object`

DescriptionThe object to be retrieved.

TypeString

Example`foo.txt`

RequiredYes

### Response Entities[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

If successful, returns the desired object.

`object`

DescriptionThe desired object.

TypeObject

### Special Error Responses[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

`NoSuchObject`

DescriptionSpecified object does not exist.

Code404 Not Found

## Head Object[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

Verify the existence of an object. If the object exists, metadata headers for the object will be returned.

### Syntax[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

```
HEAD /{admin}/bucket?object HTTP/1.1
Host {fqdn}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

`bucket`

DescriptionThe bucket containing the object to be retrieved.

TypeString

Example`foo_bucket`

RequiredYes

`object`

DescriptionThe object to be retrieved.

TypeString

Example`foo.txt`

RequiredYes

### Response Entities[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

None.

### Special Error Responses[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

`NoSuchObject`

DescriptionSpecified object does not exist.

Code404 Not Found

## Get Zone Info[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

Get cluster information.

### Syntax[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

```
GET /{admin}/zone&format=json HTTP/1.1
Host {fqdn}
```

### Response Entities[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

If successful, returns cluster pool configuration.

`zone`

DescriptionContains current cluster pool configuration.

TypeContainer

`domain_root`

Descriptionroot of all buckets.

TypeString

Parent`cluster`

`control_pool`

Description

TypeString

Parent`cluster`

`gc_pool`

DescriptionGarbage collection pool.

TypeString

Parent`cluster`

`log_pool`

DescriptionLog pool.

TypeString

Parent`cluster`

`intent_log_pool`

DescriptionIntent log pool.

TypeString

Parent`cluster`

`usage_log_pool`

DescriptionUsage log pool.

TypeString

Parent`cluster`

`user_keys_pool`

DescriptionUser key pool.

TypeString

Parent`cluster`

`user_email_pool`

DescriptionUser email pool.

TypeString

Parent`cluster`

`user_swift_pool`

DescriptionPool of swift users.

TypeString

Parent`cluster`

### Special Error Responses[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

None.

### Example Response[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

```
HTTP/1.1 200
Content-Type: application/json

{
  "domain_root": ".rgw",
  "control_pool": ".rgw.control",
  "gc_pool": ".rgw.gc",
  "log_pool": ".log",
  "intent_log_pool": ".intent-log",
  "usage_log_pool": ".usage",
  "user_keys_pool": ".users",
  "user_email_pool": ".users.email",
  "user_swift_pool": ".users.swift",
  "user_uid_pool ": ".users.uid"
}
```

## Add Placement Pool[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

Make a pool available for data placement.

### Syntax[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

```
PUT /{admin}/pool?format=json HTTP/1.1
Host {fqdn}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

`pool`

DescriptionThe pool to be made available for data placement.

TypeString

Example`foo_pool`

RequiredYes

`create`

DescriptionCreates the data pool if it does not exist.

TypeBoolean

ExampleFalse \[False\]

RequiredNo

### Response Entities[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

TBD.

### Special Error Responses[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

TBD.

## Remove Placement Pool[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

Make a pool unavailable for data placement.

### Syntax[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

```
DELETE /{admin}/pool?format=json HTTP/1.1
Host {fqdn}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

`pool`

DescriptionThe existing pool to be made available for data placement.

TypeString

Example`foo_pool`

RequiredYes

`destroy`

DescriptionDestroys the pool after removing it from the active set.

TypeBoolean

ExampleFalse \[False\]

RequiredNo

### Response Entities[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

TBD.

### Special Error Responses[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

TBD.

## List Available Data Placement Pools[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

List current pools available for data placement.

### Syntax[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

```
GET /{admin}/pool?format=json HTTP/1.1
Host {fqdn}
```

### Response Entities[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

If successful, returns a list of pools available for data placement.

`pools`

DescriptionContains currently available pools for data placement.

TypeContainer

## List Expired Garbage Collection Items[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

List objects scheduled for garbage collection.

### Syntax[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

```
GET /{admin}/garbage?format=json HTTP/1.1
Host {fqdn}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

None.

### Response Entities[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

If expired garbage collection items exist, a list of such objects will be returned.

`garbage`

DescriptionExpired garbage collection items.

TypeContainer

`object`

DescriptionA container garbage collection object information.

TypeContainer

Parent`garbage`

`name`

DescriptionThe name of the object.

TypeString

Parent`object`

`expired`

DescriptionThe date at which the object expired.

TypeString

Parent`object`

### Special Error Responses[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

TBD.

## Manually Processes Garbage Collection Items[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

List objects scheduled for garbage collection.

### Syntax[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

```
DELETE /{admin}/garbage?format=json HTTP/1.1
Host {fqdn}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

None.

### Response Entities[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

If expired garbage collection items exist, a list of removed objects will be returned.

`garbage`

DescriptionExpired garbage collection items.

TypeContainer

`object`

DescriptionA container garbage collection object information.

TypeContainer

Parent`garbage`

`name`

DescriptionThe name of the object.

TypeString

Parent`object`

`expired`

DescriptionThe date at which the object expired.

TypeString

Parent`object`

### Special Error Responses[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

TBD.

## Show Log Objects[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

Show log objects

### Syntax[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

```
GET /{admin}/log?format=json HTTP/1.1
Host {fqdn}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

`object`

DescriptionThe log object to return.

TypeString:

Example`2012-10-11-09-4165.2-foo_bucket`

RequiredNo

### Response Entities[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

If no object is specified, returns the full list of log objects.

`log-objects`

DescriptionA list of log objects.

TypeContainer

`object`

DescriptionThe name of the log object.

TypeString

`log`

DescriptionThe contents of the log object.

TypeContainer

### Special Error Responses[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

None.

## Standard Error Responses[¶](https://docs.ceph.com/en/pacific/dev/radosgw/admin/adminops_nonimplemented/ "Permalink to this headline")

`AccessDenied`

DescriptionAccess denied.

Code403 Forbidden

`InternalError`

DescriptionInternal server error.

Code500 Internal Server Error

`NoSuchUser`

DescriptionUser does not exist.

Code404 Not Found

`NoSuchBucket`

DescriptionBucket does not exist.

Code404 Not Found

`NoSuchKey`

DescriptionNo such access key.

Code404 Not Found
