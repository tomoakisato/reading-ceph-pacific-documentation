# 447: list

**クリップソース:** [447: list — Ceph Documentation](https://docs.ceph.com/en/pacific/ceph-volume/lvm/list/)

# list[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/list/ "Permalink to this headline")

このサブコマンドは、Cephクラスタに関連するあらゆるデバイス\(論理デバイスと物理デバイス\)をリストアップします。

出力はデバイスに関連付けられたOSD IDでグループ化され、ceph\-diskとは異なり、Cephに関連付けされていないデバイスの情報は提供されません。

コマンドラインオプション：

* \-\-format jsonまたはpretty値を指定します。デフォルトはprettyで、デバイス情報を人間が読みやすい形式にまとめます。

## Full Reporting[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/list/ "Permalink to this headline")

位置引数を使用しない場合、完全なレポートが表示されます。これは、システムで見つかったすべてのデバイスと論理ボリュームが表示されることを意味します。

2つのOSD、ジャーナルとしてLVを持つものと、物理デバイスを持つもののFull prettyレポートは、以下のように見えるます：

```
# ceph-volume lvm list

====== osd.1 =======

  [journal]    /dev/journals/journal1

      journal uuid              C65n7d-B1gy-cqX3-vZKY-ZoE0-IEYM-HnIJzs
      osd id                    1
      cluster fsid              ce454d91-d748-4751-a318-ff7f7aa18ffd
      type                      journal
      osd fsid                  661b24f8-e062-482b-8110-826ffe7f13fa
      data uuid                 SlEgHe-jX1H-QBQk-Sce0-RUls-8KlY-g8HgcZ
      journal device            /dev/journals/journal1
      data device               /dev/test_group/data-lv2
      devices                   /dev/sda

  [data]    /dev/test_group/data-lv2

      journal uuid              C65n7d-B1gy-cqX3-vZKY-ZoE0-IEYM-HnIJzs
      osd id                    1
      cluster fsid              ce454d91-d748-4751-a318-ff7f7aa18ffd
      type                      data
      osd fsid                  661b24f8-e062-482b-8110-826ffe7f13fa
      data uuid                 SlEgHe-jX1H-QBQk-Sce0-RUls-8KlY-g8HgcZ
      journal device            /dev/journals/journal1
      data device               /dev/test_group/data-lv2
      devices                   /dev/sdb

====== osd.0 =======

  [data]    /dev/test_group/data-lv1

      journal uuid              cd72bd28-002a-48da-bdf6-d5b993e84f3f
      osd id                    0
      cluster fsid              ce454d91-d748-4751-a318-ff7f7aa18ffd
      type                      data
      osd fsid                  943949f0-ce37-47ca-a33c-3413d46ee9ec
      data uuid                 TUpfel-Q5ZT-eFph-bdGW-SiNW-l0ag-f5kh00
      journal device            /dev/sdd1
      data device               /dev/test_group/data-lv1
      devices                   /dev/sdc

  [journal]    /dev/sdd1

      PARTUUID                  cd72bd28-002a-48da-bdf6-d5b993e84f3f
```

論理ボリュームの場合、devicesキーには、論理ボリュームに関連する物理デバイスが入力されます。LVMでは、複数の物理デバイスを論理ボリュームの一部にすることができるため、prettyを使用する場合はカンマ区切り、jsonを使用する場合は配列で指定します。

注：タグは読みやすい形式で表示されます。osdidキーはceph.osd\_idタグとして格納されます。lvmタグの規約の詳細については、[タグAPI](https://docs.ceph.com/en/pacific/dev/ceph-volume/lvm/#ceph-volume-lvm-tag-api)を参照してください。

## Single Reporting[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/list/ "Permalink to this headline")

単一レポートは、デバイスと論理ボリュームの両方を入力（位置パラメーター）として消費することができます。論理ボリュームの場合、論理ボリューム名だけでなく、グループ名も使用する必要があります。

例えば、test\_groupボリュームグループ内のdata\-lv2論理ボリュームは、次のように一覧表示されます：

```
# ceph-volume lvm list test_group/data-lv2

====== osd.1 =======

  [data]    /dev/test_group/data-lv2

      journal uuid              C65n7d-B1gy-cqX3-vZKY-ZoE0-IEYM-HnIJzs
      osd id                    1
      cluster fsid              ce454d91-d748-4751-a318-ff7f7aa18ffd
      type                      data
      osd fsid                  661b24f8-e062-482b-8110-826ffe7f13fa
      data uuid                 SlEgHe-jX1H-QBQk-Sce0-RUls-8KlY-g8HgcZ
      journal device            /dev/journals/journal1
      data device               /dev/test_group/data-lv2
      devices                   /dev/sdc
```

注：タグは読みやすい形式で表示されます。osdidキーはceph.osd\_idタグとして格納されます。lvmタグの規約の詳細については、[タグAPI](https://docs.ceph.com/en/pacific/dev/ceph-volume/lvm/#ceph-volume-lvm-tag-api)を参照してください。

プレーンディスクの場合、デバイスのフルパスが必要です。例えば、/dev/sdd1 のようなデバイスの場合、以下のようになります：

```
# ceph-volume lvm list /dev/sdd1

====== osd.0 =======

  [journal]    /dev/sdd1

      PARTUUID                  cd72bd28-002a-48da-bdf6-d5b993e84f3f
```

## json output[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/list/ "Permalink to this headline")

\-\-format=jsonを使用したすべての出力は、タグを含め、システムがデバイスのメタデータとして保存しているすべてのものを表示します。

jsonレポートでは読みやすさのための変更は行わず、すべての情報がそのまま表示されます。フル出力だけでなく、単一デバイスの一覧表示も可能です。

簡潔にするために、1つの論理ボリュームをjsonで出力するとこのようになります（タグが変更されていないことに注意してください）：

```
# ceph-volume lvm list --format=json test_group/data-lv1
{
    "0": [
        {
            "devices": ["/dev/sda"],
            "lv_name": "data-lv1",
            "lv_path": "/dev/test_group/data-lv1",
            "lv_tags": "ceph.cluster_fsid=ce454d91-d748-4751-a318-ff7f7aa18ffd,ceph.data_device=/dev/test_group/data-lv1,ceph.data_uuid=TUpfel-Q5ZT-eFph-bdGW-SiNW-l0ag-f5kh00,ceph.journal_device=/dev/sdd1,ceph.journal_uuid=cd72bd28-002a-48da-bdf6-d5b993e84f3f,ceph.osd_fsid=943949f0-ce37-47ca-a33c-3413d46ee9ec,ceph.osd_id=0,ceph.type=data",
            "lv_uuid": "TUpfel-Q5ZT-eFph-bdGW-SiNW-l0ag-f5kh00",
            "name": "data-lv1",
            "path": "/dev/test_group/data-lv1",
            "tags": {
                "ceph.cluster_fsid": "ce454d91-d748-4751-a318-ff7f7aa18ffd",
                "ceph.data_device": "/dev/test_group/data-lv1",
                "ceph.data_uuid": "TUpfel-Q5ZT-eFph-bdGW-SiNW-l0ag-f5kh00",
                "ceph.journal_device": "/dev/sdd1",
                "ceph.journal_uuid": "cd72bd28-002a-48da-bdf6-d5b993e84f3f",
                "ceph.osd_fsid": "943949f0-ce37-47ca-a33c-3413d46ee9ec",
                "ceph.osd_id": "0",
                "ceph.type": "data"
            },
            "type": "data",
            "vg_name": "test_group"
        }
    ]
}
```

## Synchronized information[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/list/ "Permalink to this headline")

リストアップの前に、使用中の物理デバイスの名前が変更されていないことを確認するために、lvm APIが照会されます。/dev/sda1 のような非永続デバイスが /dev/sdb1 に変更される可能性があります。

PARTUUIDは、データlvの論理ボリュームのメタデータの一部として格納されているため、検出が可能であす。物理デバイスのジャーナルの場合も、この情報はジャーナルに関連するデータ論理ボリュームに保存されます。

名前が同じでなくなった（と、PARTUUID使用時にblkidが報告する）場合、タグは更新され、レポートは新しく更新された情報を使用します。
