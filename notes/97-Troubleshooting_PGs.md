# 97: Troubleshooting PGs

**クリップソース:** [97: Troubleshooting PGs — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-pg/)

# Troubleshooting PGs[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-pg/ "Permalink to this headline")

## Placement Groups Never Get Clean[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-pg/ "Permalink to this headline")

クラスタを作成しても、クラスタがactive、active\+remapped、またはactive\+degragedの状態のままで、active\+cleanの状態にならない場合、設定に問題がある可能性があります。

[Pool, PG and CRUSH Config Reference](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-pg/) を確認し、適切な調整を行う必要がある場合があります。

一般的なルールとして、複数のOSDと1以上のオブジェクトレプリカのプールサイズでクラスタを実行する必要があります。

### One Node Cluster[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-pg/ "Permalink to this headline")

分散コンピューティング用に設計されたシステムをシングルノードに展開することはないため、Cephはシングルノードでの運用に関するドキュメントを提供しなくなりました。また、Cephデーモンを含む単一ノードにクライアントカーネルモジュールをマウントすると、Linuxカーネル自体の問題によりデッドロックが発生する可能性があります\(クライアントにVMを使用する場合を除く\)。本書で説明するような制限はありますが、1ノード構成でCephを実験することは可能です。

単一ノードでクラスタを作成しようとする場合、モニタとOSDを作成する前に、Ceph構成ファイルでosd crush choose leaf type設定のデフォルトを1 \(ホストまたはノードの意\)から0 \(osdの意\)に変更する必要があります。これにより、OSDが同じホスト上の別のOSDとピアリングできることをCephに伝えます。1ノードクラスタをセットアップしようとしていて、osd crush choose leaf typeが0より大きい場合、CephはあるOSDのPGを別のノード、シャーシ、ラック、行、データセンター上の別のOSDのPGとピアリングしようとします。

ヒント:カーネルの競合が発生する可能性があるため、Ceph Storage Clusterと同じノードにカーネルクライアントを直接マウントしないでください。ただし、単一ノード上の仮想マシン\(VM\)内にカーネルクライアントをマウントすることは可能です。

単一ディスクを使用してOSDを作成する場合、まずデータ用のディレクトリを手動で作成する必要があります。

### Fewer OSDs than Replicas[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-pg/ "Permalink to this headline")

2つのOSDをup状態にしても、active\+clean PGが表示されない場合は、osd pool default sizeが2より大きく設定されている可能性があります。

この状況に対応するには、いくつかの方法があります。もし、クラスタを2つのレプリカを持つactive\+degraded状態で運用したい場合は、osd pool default min sizeを2に設定し、active\+degraded状態でオブジェクトを書き込むことができるようにすることができます。また、osd pool default size設定を2に設定して、2つの保存済みレプリカ（オリジナルと1つのレプリカ）のみを持つようにすることもでき、この場合、クラスタはactive\+clean状態を達成する必要があります。

注：実行時に変更することができます。Ceph設定ファイルで変更した場合、クラスタの再起動が必要になることがあります。

### Pool Size = 1[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-pg/ "Permalink to this headline")

osd pool default sizeを1に設定した場合、オブジェクトのコピーは1つしかありません。OSDは、どのオブジェクトを持つべきかを他のOSDに教えてもらうことに依存しています。もし、最初のOSDがオブジェクトのコピーを持っていて、2番目のコピーがない場合、2番目のOSDは最初のOSDにそのコピーを持っているべきであると伝えることができません。最初の OSD にマップされた各PG \(ceph pg dump を参照\) について、以下を実行することによって、最初の OSD に必要なPGを通知するように強制できます。

```
ceph osd force-create-pg <pgid>
```

### CRUSH Map Errors[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-pg/ "Permalink to this headline")

また、CRUSHマップに誤りがある場合にも、PGがuncleanになる可能性があります。

## Stuck Placement Groups[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-pg/ "Permalink to this headline")

障害発生後、PGが「degraded」や「peering」といった状態になるのは正常なことです。 通常、これらの状態は、障害回復プロセスにおける正常な進行を示します。しかし、PGがこれらの状態のいずれかに長時間とどまる場合、より大きな問題の兆候である可能性があります。このため、モニターは、PGが最適でない状態で「立ち往生」した場合に警告を発します。 具体的には、次のようなことをチェックします。

* inactive \- PGがあまりに長い間アクティブになっていない（読み取り/書き込み要求を処理できていない）
* unclean \- PGがあまりにも長い間クリーンな状態でない（障害から完全に立ち直ることができていない）
* stale \-ceph\-osdによってPGの状態が更新されていないため、このPGを格納しているすべてのノードがダウンしている可能性がある

以下のいずれかで、スタックしたPGを明示的にリストアップすることができます：

```
ceph pg dump_stuck stale
ceph pg dump_stuck inactive
ceph pg dump_stuck unclean
```

動かなくなったstale PGの場合、通常は正しいceph\-osdデーモンが再び動作するようにすることが問題です。 

動かないinactive PGの場合、通常はピアリングの問題です\([Placement Group Down \- Peering Failure](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-pg/)を参照\)。 

動かなくなったunclean PGの場合、通常、未発見のオブジェクトなど、復旧の完了を妨げる何かがあります\([Unfound Objects](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-pg/)を参照\)。

## Placement Group Down \- Peering Failure[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-pg/ "Permalink to this headline")

場合によっては、ceph\-osd Peeringプロセスに問題が発生し、PGがアクティブにならず、使用できなくなることがあります。  たとえば、ceph healthが以下を報告する場合があります：

```
ceph health detail
HEALTH_ERR 7 pgs degraded; 12 pgs down; 12 pgs peering; 1 pgs recovering; 6 pgs stuck unclean; 114/3300 degraded (3.455%); 1/3 in osds are down
...
pg 0.5 is down+peering
pg 1.4 is down+peering
...
osd.1 is down since epoch 69, last address 192.168.106.220:6801/8651
```

クラスタに問い合わせることで、PGがマークdownされた理由を正確に把握することができます：

```
ceph pg 0.5 query
```

```
{ "state": "down+peering",
  ...
  "recovery_state": [
       { "name": "Started/Primary/Peering/GetInfo",
         "enter_time": "2012-03-06 14:40:16.169679",
         "requested_info_from": []},
       { "name": "Started/Primary/Peering",
         "enter_time": "2012-03-06 14:40:16.169659",
         "probing_osds": [
               0,
               1],
         "blocked": "peering is blocked due to down osds",
         "down_osds_we_would_probe": [
               1],
         "peering_blocked_by": [
               { "osd": 1,
                 "current_lost_at": 0,
                 "comment": "starting or marking this osd lost may let us proceed"}]},
       { "name": "Started",
         "enter_time": "2012-03-06 14:40:16.169513"}
   ]
}

```

recovery\_stateセクションは、ceph\-osdデーモン、特にosd.1がダウンしているため、ピアリングがブロックされていることを示しています。 この場合、そのceph\-osdを起動すれば、事態は回復します。

あるいは、osd.1に壊滅的な障害（ディスク障害など）が発生した場合、クラスタが失われたことを伝え、できる限りの対処をするように指示することも可能です。

重要：これは、クラスタがデータの他のコピーの一貫性と最新性を保証できないという点で危険です。

Cephにとにかく続けるように指示するには：

```
ceph osd lost 1
```

リカバリが進みます。

## Unfound Objects[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-pg/ "Permalink to this headline")

特定の障害の組み合わせでは、Cephはオブジェクトが見つからないと文句を言うことがあります。

```
ceph health detail
HEALTH_WARN 1 pgs degraded; 78/3778 unfound (2.065%)
pg 2.4 is active+degraded, 78 unfound
```

これは、ストレージクラスタがあるオブジェクト（または既存オブジェクトの新しいコピー）が存在することは知っているが、そのコピーが見つかっていないことを意味します。ceph\-osds 1と2にデータがあるPGの場合、 以下のような現象が起こる可能性があります。

* 1がdown
* 2が、一人でいくつかのwriteを処理する
* 1がup
* 1と2が再ピアし、復旧のために1に欠落しているオブジェクトがキューに入れられる
* 新しいオブジェクトがコピーされる前に、2がdown

今、1はこれらのオブジェクトが存在することを知っていますが、コピーを持っている生きているceph\-osdは存在しません。 この場合、これらのオブジェクトへのI/Oはブロックされ、クラスタは故障したノードがすぐに戻ってくることを望みます。これは、ユーザにI/Oエラーを返すよりも望ましいと想定されています。

まず、どのオブジェクトが未発見のものかを特定します：

```
ceph pg 2.4 list_unfound [starting offset, in json]
```

```
{
  "num_missing": 1,
  "num_unfound": 1,
  "objects": [
      {
          "oid": {
              "oid": "object",
              "key": "",
              "snapid": -2,
              "hash": 2249616407,
              "max": 0,
              "pool": 2,
              "namespace": ""
          },
          "need": "43'251",
          "have": "0'0",
          "flags": "none",
          "clean_regions": "clean_offsets: [], clean_omap: 0, new_object: 1",
          "locations": [
              "0(3)",
              "4(2)"
          ]
      }
  ],
  "state": "NotRecovering",
  "available_might_have_unfound": true,
  "might_have_unfound": [
      {
          "osd": "2(4)",
          "status": "osd is down"
      }
  ],
  "more": false
}

```

オブジェクトの数が多すぎて1つの結果に表示できない場合は、moreフィールドがtrueになり、さらに問い合わせができるようになります。 \(最終的にはコマンドラインツールがこれを隠してくれますが、今はまだです\)。

第二に、どのOSDがプローブされているか、またはデータを含んでいる可能性があるかを特定することができます。

リストの最後（moreがfalseになる前）に、available\_might\_have\_unfoundがtrueの場合、might\_have\_unfoundが提供されます。 これは、ceph pg\#.\# queryの出力と同じです。 これにより、queryを直接使用する必要がなくなります。与えられたmight\_have\_unfoundの情報は、以下のqueryの説明と同じように動作します。唯一の違いは、すでにprobedステータスを持つOSDは無視されることです。

queryの使用：

```
ceph pg 2.4 query
```

```
"recovery_state": [
     { "name": "Started/Primary/Active",
       "enter_time": "2012-03-06 15:15:46.713212",
       "might_have_unfound": [
             { "osd": 1,
               "status": "osd is down"}]},

```

この場合、例えば、クラスタはosd.1がデータを持っているかもしれないが、ダウンしていることを知っています。 可能な状態は以下のいずれかです：

* 既にプローブされている
* query中
* OSDがdownしている
* （まだ）queryされていない

クラスタが可能な場所を問い合わせるのに、単に時間がかかることもあります。

リストアップされていない他の場所にオブジェクトが存在する可能性があります。 例えば、ceph\-osdが停止してクラスタからoutされ、クラスタが完全に回復し、将来の何らかの障害によって未発見のオブジェクトが存在することになった場合、長期間離れたceph\-osdは潜在的な場所として考慮されません。 \(ただし、このシナリオは考えにくい\)。

もし、すべての可能な場所を照会しても、オブジェクトが失われる場合は、失われたオブジェクトをあきらめなければならないかもしれません。これは、writeそのものが回復する前に実行されたwriteについてクラスタが知ることができるような、異常な故障の組み合わせの場合に起こり得ます。unfoundオブジェクトをlostとしてマークするには：

```
ceph pg 2.5 mark_unfound_lost revert|delete
```

この最後の引数は、クラスタが失われたオブジェクトをどのように処理すべきかを指定します。

deleteオプションは、それらを完全に忘れます。

revert オプション（ECプールでは使用不可）は、オブジェクトの以前のバージョンにロールバックするか、（新しいオブジェクトの場合）それを完全に忘れるかのいずれかです。 オブジェクトが存在することを期待していたアプリケーションを混乱させる可能性があるため、このオプションは慎重に使用してください。

## Homeless Placement Groups[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-pg/ "Permalink to this headline")

あるPGのコピーを持っていたすべてのOSDが故障する可能性があります。その場合、オブジェクト ストアのそのサブセットは使用できなくなり、モニタはそれらのPGのステータス更新を受け取ることができなくなります。 この状況を検出するために、モニタは、プライマリOSDが故障した任意のPGをstaleとしてマークします。 たとえば、次のようになります：

```
ceph health
HEALTH_WARN 24 pgs stale; 3/300 in osds are down
```

どのPGがstaleになっているのか、最後に格納したOSDは何だったのか、以下で識別することができます：

```
ceph health detail
HEALTH_WARN 24 pgs stale; 3/300 in osds are down
...
pg 2.5 is stuck stale+active+remapped, last acting [2,0]
...
osd.10 is down since epoch 23, last address 192.168.106.220:6800/11080
osd.11 is down since epoch 13, last address 192.168.106.220:6803/11539
osd.12 is down since epoch 24, last address 192.168.106.220:6806/11861
```

これらのceph\-osdデーモンを再起動することで、クラスタはそのPG\(およびおそらく他の多くのPG\)を回復できます。

## Only a Few OSDs Receive Data[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-pg/ "Permalink to this headline")

クラスタに多数のノードがあり、そのうちの数ノードしかデータを受信しない場合、プールのPGの数を確認してください。PGはOSDにマッピングされるため、PGの数が少ないと、クラスタ全体に分散されません。OSDの数の倍数であるPG数でプールを作成してみてください。詳細については、「[Placement Groups](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-pg/)」を参照してください。プールのデフォルトのPG数は有用ではありませんが、ここで変更することができます。 

## Can’t Write Data[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-pg/ "Permalink to this headline")

クラスタは稼働しているが、一部のOSDがダウンしており、データの書き込みができない場合、PGに対して最小数のOSDが稼働していることを確認してください。最小数のOSDが動作していない場合、Cephがデータをレプリケートできる保証はないため、Cephはデータの書き込みを許可しません。詳細については、「[Pool, PG and CRUSH Config Reference](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-pg/)」の「osd pool default min size」を参照してください。 

## PGs Inconsistent[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-pg/ "Permalink to this headline")

active\+clean\+inconsistentの状態を受け取った場合、スクラビング中のエラーにより発生する可能性があります。いつものように、inconsistent PGを特定することができます。

```
$ ceph health detail
HEALTH_ERR 1 pgs inconsistent; 2 scrub errors
pg 0.6 is active+clean+inconsistent, acting [0,1,2]
2 scrub errors
```

プログラム的な方法で出力を検査したい場合は：

```
$ rados list-inconsistent-pg rbd
["0.6"]
```

一貫性のある状態は1つだけですが、最悪の場合、複数のオブジェクトに複数の観点で異なる矛盾が発生する可能性があります。PG 0.6 の foo というオブジェクトがトランケートされた場合、次のようになります：

```
$ rados list-inconsistent-obj 0.6 --format=json-pretty
```

```
{
    "epoch": 14,
    "inconsistents": [
        {
            "object": {
                "name": "foo",
                "nspace": "",
                "locator": "",
                "snap": "head",
                "version": 1
            },
            "errors": [
                "data_digest_mismatch",
                "size_mismatch"
            ],
            "union_shard_errors": [
                "data_digest_mismatch_info",
                "size_mismatch_info"
            ],
            "selected_object_info": "0:602f83fe:::foo:head(16'1 client.4110.0:1 dirty|data_digest|omap_digest s 968 uv 1 dd e978e67f od ffffffff alloc_hint [0 0 0])",
            "shards": [
                {
                    "osd": 0,
                    "errors": [],
                    "size": 968,
                    "omap_digest": "0xffffffff",
                    "data_digest": "0xe978e67f"
                },
                {
                    "osd": 1,
                    "errors": [],
                    "size": 968,
                    "omap_digest": "0xffffffff",
                    "data_digest": "0xe978e67f"
                },
                {
                    "osd": 2,
                    "errors": [
                        "data_digest_mismatch_info",
                        "size_mismatch_info"
                    ],
                    "size": 0,
                    "omap_digest": "0xffffffff",
                    "data_digest": "0xffffffff"
                }
            ]
        }
    ]
}

```

この場合、出力から以下が分かります：

* 不整合オブジェクトはfooだけで、不整合を持つのはその頭です。
* その矛盾は2つに分けられる。
    * errors: これらのエラーは、シャード間の不整合を示していますが、どのシャードが悪いかは判断できません。もしあれば、シャード配列のエラーをチェックし、問題を特定します。
        * data\_digest\_mismatch: OSD.2から読み取ったレプリカのダイジェストがOSD.0とOSD.1のものと異な
        * size\_mismatch: OSD.2から読み込まれるレプリカのサイズは0であり、OSD.0とOSD.1から報告されるサイズは968である。
    * union\_shard\_errors: Shards 配列に含まれる、シャード固有のすべてのエラーの和を返します。このエラーは、問題が発生したシャードに対して設定されます。read\_error のようなエラーも含まれます。oi で終わるエラーは selected\_object\_info と比較されたことを示します。どのシャードがどのエラーを持っているかは、shards 配列を見ればわかります。
        * data\_digest\_mismatch\_info: Object\-infoに格納されているダイジェストが、OSD.2から読み込んだシャードから算出される0xffffffでないこと。
        * size\_mismatch\_info: object\-infoに格納されているサイズとOSD.2から読み出したサイズが異なる。後者は0である。

不整合PGを修復するには：

```
ceph pg repair {placement-group-ID}
```

これは、権威あるコピーで不良コピーを上書きするものです。ほとんどの場合、Cephは、事前に定義された基準を使用して、利用可能なすべてのレプリカから権威あるコピーを選択することができます。しかし、これは常に機能するわけではありません。たとえば、保存されたデータダイジェストが欠落している可能性があり、権威あるコピーを選択する際に、計算されたダイジェストは無視されます。そのため、上記のコマンドは注意して使用してください。

シャードのerrors属性にread\_errorが記載されている場合、不整合はディスクのエラーによるものと思われます。そのOSDが使用しているディスクを確認するとよいでしょう。

クロックスキューのためにactive\+clean\+inconsistentの状態を定期的に受け取る場合、モニターホスト上のNTPデーモンがピアとして動作するように設定することを検討してもよいでしょう。詳細については、[The Network Time Protocol](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-pg/)と[Clock Settings](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-pg/)を参照してください。

## Erasure Coded PGs are not active\+clean[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-pg/ "Permalink to this headline")

CRUSHがPGにマッピングするのに十分なOSDを見つけられなかった場合、ITEM\_NONEまたはnoOSDfoundである2147483647と表示されます。例えば：

```
[2,1,6,0,5,8,2147483647,7,4]
```

### Not enough OSDs[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-pg/ "Permalink to this headline")

Cephクラスタに8個のOSDしかなく、ECプールに9個が必要な場合、そのように表示されます。より少ないOSDを必要とする別のECプールを作成することができます。

```
ceph osd erasure-code-profile set myprofile k=5 m=3
ceph osd pool create erasurepool erasure myprofile
```

または新しいOSDを追加すると、PGは自動的にそれらを使用します。

### CRUSH constraints cannot be satisfied[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-pg/ "Permalink to this headline")

クラスタに十分なOSDがある場合、CRUSHルールが満たすことのできない制約を課す可能性があります。2つのホスト上に10個のOSDがあり、CRUSHルールが同じホストからの2つのOSDが同じPGで使用されないことを要求する場合、2つのOSDだけが見つかるため、マッピングは失敗する可能性があります。ルールを表示（ダンプ）することで、制約を確認することができます：

```
$ ceph osd crush rule ls
[
    "replicated_rule",
    "erasurepool"]
$ ceph osd crush rule dump erasurepool
{ "rule_id": 1,
  "rule_name": "erasurepool",
  "ruleset": 1,
  "type": 3,
  "min_size": 3,
  "max_size": 20,
  "steps": [
        { "op": "take",
          "item": -1,
          "item_name": "default"},
        { "op": "chooseleaf_indep",
          "num": 0,
          "type": "host"},
        { "op": "emit"}]}
```

PGが同じホストに存在するOSDを許可する新しいプールを作成することで問題を解決することができます：

```
ceph osd erasure-code-profile set myprofile crush-failure-domain=osd
ceph osd pool create erasurepool erasure myprofile
```

### CRUSH gives up too soon[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-pg/ "Permalink to this headline")

CephクラスタにPGをマッピングするのにちょうど十分な数のOSDがある場合\(たとえば、合計9個のOSDを持つクラスタで、PGごとに9個のOSDを必要とするECプールの場合\)、CRUSHがマッピングを見つける前にあきらめる可能性があります。それは、以下の方法で解決することができます：

* ECプールの要件を下げ、PGあたりのOSD使用量を減らす（ECプロファイルは動的に変更できないため、別のプールを作成する必要がある）
* クラスタにOSDを追加する（この場合、ECプールを変更する必要はなく、自動的にクリーンになる）
* ハンドメイドのCRUSHルールを使用し、良いマッピングをより多くの回数で見つけようとします。これは、set\_choose\_tries をデフォルトより大きな値に設定することで可能です。

Cephクラスタに変更を加えず、ローカルファイルのみで実験できるように、クラスタからcrushmapを抽出した後、まずcrushtoolで問題を確認する必要があります：

```
$ ceph osd crush rule dump erasurepool
{ "rule_name": "erasurepool",
  "ruleset": 1,
  "type": 3,
  "min_size": 3,
  "max_size": 20,
  "steps": [
        { "op": "take",
          "item": -1,
          "item_name": "default"},
        { "op": "chooseleaf_indep",
          "num": 0,
          "type": "host"},
        { "op": "emit"}]}
$ ceph osd getcrushmap > crush.map
got crush map from osdmap epoch 13
$ crushtool -i crush.map --test --show-bad-mappings 
   --rule 1 
   --num-rep 9 
   --min-x 1 --max-x $((1024 * 1024))
bad mapping rule 8 x 43 num_rep 9 result [3,2,7,1,2147483647,8,5,6,0]
bad mapping rule 8 x 79 num_rep 9 result [6,0,2,1,4,7,2147483647,5,8]
bad mapping rule 8 x 173 num_rep 9 result [0,4,6,8,2,1,3,7,2147483647]
```

ここで、\-\-num\-repはEC CRUSHルールが必要とするOSDの数、\-\-ruleはceph osd crush rule dumpが表示するルールセット・フィールドの値です。 このテストでは100万個の値 \(すなわち \[\-\-min\-x,\-\-max\-x\] で定義される範囲\) のマッピングを試み、少なくとも1つの不良マッピングを表示する必要があります。もし何も出力されなければ、すべてのマッピングが成功したことを意味し、そこでやめることができます: 問題は別のところにあります。

CRUSHルールは、CRUSHマップをデコンパイルすることで編集することができます：

```
$ crushtool --decompile crush.map > crush.txt
```

ルールに以下の行を追加します：

```
step set_choose_tries 100
```

crush.txtの該当箇所は、以下のような感じです：

```
rule erasurepool {
        ruleset 1
        type erasure
        min_size 3
        max_size 20
        step set_chooseleaf_tries 5
        step set_choose_tries 100
        step take default
        step chooseleaf indep 0 type host
        step emit
}
```

再びコンパイルしてテストすることができます：

```
$ crushtool --compile crush.txt -o better-crush.map
```

すべてのマッピングが成功した場合、crushtoolの\-\-show\-choose\-triesオプションで、すべてのマッピングを見つけるために必要だった試行回数のヒストグラムを表示することがでます：

```
$ crushtool -i better-crush.map --test --show-bad-mappings 
   --show-choose-tries 
   --rule 1 
   --num-rep 9 
   --min-x 1 --max-x $((1024 * 1024))
...
11:        42
12:        44
13:        54
14:        45
15:        35
16:        34
17:        30
18:        25
19:        19
20:        22
21:        20
22:        17
23:        13
24:        16
25:        13
26:        11
27:        11
28:        13
29:        11
30:        10
31:         6
32:         5
33:        10
34:         3
35:         7
36:         5
37:         2
38:         5
39:         5
40:         2
41:         5
42:         4
43:         1
44:         2
45:         2
46:         3
47:         1
48:         0
...
102:         0
103:         1
104:         0
...

```

42個のPGをマッピングするのに11回、44個のPGをマッピングするのに12回などの試行が必要でした。試行回数の最高値は、誤ったマッピングを防ぐための set\_choose\_tries の最小値です \(つまり、上記の出力では、どの PG もマッピングされるまでに 103 回以上かからなかったので 103 となります\)。
