# 111: ceph-osd – ceph object storage daemon

 # ceph\-osd – ceph object storage daemon[¶](https://docs.ceph.com/en/pacific/man/8/ceph-osd/ "Permalink to this headline")

## Synopsis[¶](https://docs.ceph.com/en/pacific/man/8/ceph-osd/ "Permalink to this headline")

**ceph\-osd** \-i _osdnum_ \[ –osd\-data _datapath_ \] \[ –osd\-journal_journal_ \] \[ –mkfs \] \[ –mkjournal \] \[–flush\-journal\] \[–check\-allows\-journal\] \[–check\-wants\-journal\] \[–check\-needs\-journal\] \[ –mkkey \] \[ –osdspec\-affinity \]

## Description[¶](https://docs.ceph.com/en/pacific/man/8/ceph-osd/ "Permalink to this headline")

**ceph\-osd** is the object storage daemon for the Ceph distributed file system. It is responsible for storing objects on a local file system and providing access to them over the network.

The datapath argument should be a directory on a xfs file system where the object data resides. The journal is optional, and is only useful performance\-wise when it resides on a different disk than datapath with low latency \(ideally, an NVRAM device\).

## Options[¶](https://docs.ceph.com/en/pacific/man/8/ceph-osd/ "Permalink to this headline")

`-f````, ``--foreground```[¶](https://docs.ceph.com/en/pacific/man/8/ceph-osd/ "Permalink to this definition")Foreground: do not daemonize after startup \(run in foreground\). Do not generate a pid file. Useful when run via [ceph\-run](https://docs.ceph.com/en/pacific/man/8/ceph-osd/)\(8\).

`-d```[¶](https://docs.ceph.com/en/pacific/man/8/ceph-osd/ "Permalink to this definition")Debug mode: like `-f`, but also send all log output to stderr.

`--setuser`` userorgid`[¶](https://docs.ceph.com/en/pacific/man/8/ceph-osd/ "Permalink to this definition")Set uid after starting. If a username is specified, the user record is looked up to get a uid and a gid, and the gid is also set as well, unless –setgroup is also specified.

`--setgroup`` grouporgid`[¶](https://docs.ceph.com/en/pacific/man/8/ceph-osd/ "Permalink to this definition")Set gid after starting. If a group name is specified the group record is looked up to get a gid.

`--osd-data`` osddata`[¶](https://docs.ceph.com/en/pacific/man/8/ceph-osd/ "Permalink to this definition")Use object store at _osddata_.

`--osd-journal`` journal`[¶](https://docs.ceph.com/en/pacific/man/8/ceph-osd/ "Permalink to this definition")Journal updates to _journal_.

`--check-wants-journal```[¶](https://docs.ceph.com/en/pacific/man/8/ceph-osd/ "Permalink to this definition")Check whether a journal is desired.

`--check-allows-journal```[¶](https://docs.ceph.com/en/pacific/man/8/ceph-osd/ "Permalink to this definition")Check whether a journal is allowed.

`--check-needs-journal```[¶](https://docs.ceph.com/en/pacific/man/8/ceph-osd/ "Permalink to this definition")Check whether a journal is required.

`--mkfs```[¶](https://docs.ceph.com/en/pacific/man/8/ceph-osd/ "Permalink to this definition")Create an empty object repository. This also initializes the journal \(if one is defined\).

`--mkkey```[¶](https://docs.ceph.com/en/pacific/man/8/ceph-osd/ "Permalink to this definition")Generate a new secret key. This is normally used in combination with `--mkfs` as it is more convenient than generating a key by hand with [ceph\-authtool](https://docs.ceph.com/en/pacific/man/8/ceph-osd/)\(8\).

`--mkjournal```[¶](https://docs.ceph.com/en/pacific/man/8/ceph-osd/ "Permalink to this definition")Create a new journal file to match an existing object repository. This is useful if the journal device or file is wiped out due to a disk or file system failure.

`--flush-journal```[¶](https://docs.ceph.com/en/pacific/man/8/ceph-osd/ "Permalink to this definition")Flush the journal to permanent store. This runs in the foreground so you know when it’s completed. This can be useful if you want to resize the journal or need to otherwise destroy it: this guarantees you won’t lose data.

`--get-cluster-fsid```[¶](https://docs.ceph.com/en/pacific/man/8/ceph-osd/ "Permalink to this definition")Print the cluster fsid \(uuid\) and exit.

`--get-osd-fsid```[¶](https://docs.ceph.com/en/pacific/man/8/ceph-osd/ "Permalink to this definition")Print the OSD’s fsid and exit. The OSD’s uuid is generated at –mkfs time and is thus unique to a particular instantiation of this OSD.

`--get-journal-fsid```[¶](https://docs.ceph.com/en/pacific/man/8/ceph-osd/ "Permalink to this definition")Print the journal’s uuid. The journal fsid is set to match the OSD fsid at –mkfs time.

`-c`` ceph.conf``, ``--conf``=ceph.conf`[¶](https://docs.ceph.com/en/pacific/man/8/ceph-osd/ "Permalink to this definition")Use _ceph.conf_ configuration file instead of the default`/etc/ceph/ceph.conf` for runtime configuration options.

`-m`` monaddress[:port]`[¶](https://docs.ceph.com/en/pacific/man/8/ceph-osd/ "Permalink to this definition")Connect to specified monitor \(instead of looking through`ceph.conf`\).

`--osdspec-affinity```[¶](https://docs.ceph.com/en/pacific/man/8/ceph-osd/ "Permalink to this definition")Set an affinity to a certain OSDSpec. This option can only be used in conjunction with –mkfs.

## Availability[¶](https://docs.ceph.com/en/pacific/man/8/ceph-osd/ "Permalink to this headline")

**ceph\-osd** is part of Ceph, a massively scalable, open\-source, distributed storage system. Please refer to the Ceph documentation at [http://ceph.com/docs](https://docs.ceph.com/en/pacific/man/8/ceph-osd/) for more information.

## See also[¶](https://docs.ceph.com/en/pacific/man/8/ceph-osd/ "Permalink to this headline")

[ceph](https://docs.ceph.com/en/pacific/man/8/ceph-osd/)\(8\),[ceph\-mds](https://docs.ceph.com/en/pacific/man/8/ceph-osd/)\(8\),[ceph\-mon](https://docs.ceph.com/en/pacific/man/8/ceph-osd/)\(8\),[ceph\-authtool](https://docs.ceph.com/en/pacific/man/8/ceph-osd/)\(8\)
