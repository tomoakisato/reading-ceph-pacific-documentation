# 85: CRUSH Maps

**クリップソース:** [85: CRUSH Maps — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/operations/crush-map/)

# CRUSH Maps[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

CRUSHアルゴリズムは、ストレージの位置を計算することで、データの保存と取得の方法を決定します。CRUSHは、Cephクライアントが集中型サーバやブローカーを経由せず、直接OSDと通信することを可能にします。Cephは、アルゴリズムで決定されたデータの保存および取得方法により、単一障害点、パフォーマンスのボトルネック、スケーラビリティの物理的な制限を回避することができます。

CRUSHは、クラスタのマップ（CRUSHマップ）を使ってデータをOSDに擬似的にランダムにマッピングし、設定されたレプリケーションポリシーと障害ドメインに従ってクラスタ全体にデータを分散させます。 CRUSHの詳細については、[CRUSH \- Controlled, Scalable, Decentralized Placement of Replicated Data](https://docs.ceph.com/en/pacific/rados/operations/crush-map/)を参照してください。

CRUSHマップには、OSDのリスト、デバイスとバケットを集約するための「バケット」階層、CRUSHがクラスタのプール内でデータを複製する方法を管理するルールが含まれています。CRUSHは、インストレーションの基本的な物理組織を反映することで、相関するデバイス障害の可能性をモデル化し、それに対処することができます。代表的な要因としては、シャーシ、ラック、物理的な近接性、共有電源、共有ネットワーキングなどがあります。この情報をクラスターマップにエンコードすることで、CRUSHの配置ポリシーは、望ましい分布を維持しながら、障害ドメインにオブジェクトレプリカを分散させます。例えば、同時障害の可能性に対処するために、データレプリカが異なる棚、ラック、電源、コントローラ、物理的な場所を使用するデバイス上にあることを保証したい場合があります。

OSDをデプロイすると、それらが実行されるノードの名前が付けられたホストバケットの下のCRUSHマップに自動的に追加されます。 これは、設定されたCRUSH障害ドメインと組み合わせて、レプリカや消去コードシャードがホスト間で分散され、単一のホストや他の障害が可用性に影響を与えないことを保証します。 大規模なクラスタでは、管理者は障害ドメインの選択を慎重に検討する必要があります。 たとえば、ラック間でレプリカを分離することは、中規模から大規模のクラスタでは一般的な方法です。

## CRUSH Location[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

CRUSH マップの階層内での OSD の位置を CRUSHロケーションと呼びます。 CRUSHロケーションは、キーバリューペアのリスト形式です。 たとえば、OSD が特定の行、ラック、シャーシ、ホストにあり、「default」CRUSH ルートの一部である場合（ほとんどのクラスタがそうです）、その CRUSH ロケーションは次のように記述されます。

```
root=default row=a rack=a2 chassis=a2a host=a2a1
```

Note:

1. キーの順番は関係ないことに注意
2. キー名\(=の左側\)は有効なCRUSHタイプである必要があります。 デフォルトでは、root、datacenter、room、row、pod、pdu、rack、chassis、hostが含まれます。これらの定義されたタイプは、ほとんどすべてのクラスタで十分ですが、CRUSHマップを変更することでカスタマイズすることができます。
3. すべてのキーを指定する必要はありません。 たとえば、デフォルトでは、Cephは（hostname \-s出力に基づいて）OSDの場所をroot=default host=HOSTNAMEに自動的に設定します。

OSDのCRUSHの位置は、ceph.confにcrush locationオプションを追加することで定義できます。 OSDは起動するたびに、CRUSHマップの正しい位置にいるかどうかを確認し、間違っている場合は自ら移動します。 この自動CRUSHマップ管理を無効にするには、設定ファイルの\[osd\]セクションに以下を追加してください。

```
osd crush update on start = false
```

ほとんどの場合、手動で設定する必要はないことに注意してください。

### Custom location hooks[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

カスタマイズされたロケーションフックを使用することで、より完全なCRUSHロケーションを起動時に生成することができます。 CRUSHロケーションは、優先順位の高いものから順に：

1. ceph.confのcrush locationオプション
2. デフォルトは root=default host=HOSTNAME で、ホスト名は hostname \-s コマンドから取得

スクリプトを書くことで、追加のロケーションフィールド（例えば、rackやdatacenter）と、configオプションで有効になるフックを提供することができます。

```
crush location hook = /path/to/customized-ceph-crush-location
```

このフックにはいくつかの引数（下記）が渡され、CRUSHロケーションの記述をstdoutに一行出力する必要があります：

```
--cluster CLUSTER --id ID --type TYPE
```

ここで、クラスタ名は通常ceph、idはデーモン識別子（OSD番号やデーモン識別子など）、デーモンタイプはosdやmdsなどです。

例えば、/etc/rackファイルの値を元にラックの位置を追加で指定するシンプルなフックは以下のようになります：

```
#!/bin/sh
echo "host=$(hostname -s) rack=$(cat /etc/rack) root=default"
```

## CRUSH structure[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

CRUSHマップは、クラスタの物理的なトポロジーを記述する階層と、データの配置ポリシーを定義するルール群から構成されています。 階層はデバイス（OSD）を葉に持ち、内部ノードはホスト、ラック、列、データセンターなど、他の物理的な特徴やグループ分けに対応しています。 ルールは、その階層の観点からレプリカをどのように配置するかを記述します（例えば、「3つのレプリカを異なるラックに配置する」）。

### Devices[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

デバイスはデータを保存する個々のOSDで、通常は各ストレージドライブに1つずつあります。デバイスはID（非負の整数）と名前で識別され、通常はosd.N（NはデバイスID）です。

Luminous リリース以降、デバイスにはデバイスクラス（例：hdd、ssd、nvme）が割り当てられることがあり、CRUSH ルールの対象にするのに便利です。 これは、ホスト内でデバイスの種類が混在している場合に特に便利です。

### Types and Buckets[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

バケットとは、ホスト、ラック、ロウなど、階層内の内部ノードのCRUSH用語です。 CRUSHマップは、これらのノードを記述するために使用される一連の型を定義しています。 デフォルトのタイプは以下の通りです。

* osd \(or device\)
* host
* chassis
* rack
* row
* pdu
* pod
* room
* datacenter
* zone
* region
* root

ほとんどのクラスタでは、これらのタイプのうちほんの一握りしか使用しませんが、必要に応じて他のタイプを定義することができます。

階層は、デバイス（通常はosdタイプ）を葉に、デバイス以外のタイプを持つ内部ノード、rootタイプのルートノードで構築されています。 例えば：
![333c87ab047190de18451153664e59ed0c8847cb8f5ea9b19128affb3ef47101.png](image/333c87ab047190de18451153664e59ed0c8847cb8f5ea9b19128affb3ef47101.png)

階層の各ノード（デバイスまたはバケット）には、そのデバイスまたは階層のサブツリーが保存すべき全データの相対的な割合を示すウェイトが設定されています。 重みはデバイスのサイズを示す葉に設定され、ルートノードの重みがその下に含まれるすべてのデバイスの合計となるように、ツリーを自動的に合計します。 通常、重みはテラバイト（TB）単位で設定されます。

クラスタの CRUSH 階層を、重みを含めて簡単に表示するには：

```
ceph osd tree
```

### Rules[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

CRUSHルールは、階層内のデバイスにデータをどのように分散させるかについてのポリシーを定義します。ルールは、CRUSHがどのようにデータレプリカを配置するかを正確に指定するための配置・複製ストラテジーや分散ポリシーを定義します。例えば、2つのターゲットを選択して双方向ミラーリングを行うルール、2つの異なるデータセンターにある3つのターゲットを選択して3方向ミラーリングを行うルール、6台のストレージデバイスを使用してECを行うルールなどを作成することができます。CRUSHルールの詳細については、[CRUSH \- Controlled, Scalable, Decentralized Placement of Replicated Data](https://docs.ceph.com/en/pacific/rados/operations/crush-map/),、特にセクション3.2 を参照してください。

CRUSHルールは、CLIを介して、使用するプールタイプ（replicatedまたはEC）、障害ドメイン、オプションとしてデバイスクラスを指定して作成することができます。まれに、CRUSHマップを手動で編集してルールを作成する必要がある場合があります。

クラスタにどのようなルールが定義されているかを確認するには：

```
ceph osd crush rule ls
```

ルールの内容を確認するには：

```
ceph osd crush rule dump
```

### Device classes[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

各デバイスには、オプションでクラスを割り当てることができます。 デフォルトでは、OSDは起動時に、バックアップされているデバイスの種類に基づいて、自動的にクラスをhdd、ssd、またはnvmeに設定します。

OSDのデバイスクラスは、次のように明示的に設定することができます。

```
ceph osd crush set-device-class <class> <osd-name> [...]
```

一度設定されたデバイスクラスは、古いクラスが解除されるまで他のクラスに変更することはできません。

```
ceph osd crush rm-device-class <osd-name> [...]
```

これにより、管理者はOSD再起動時や他のスクリプトによってクラスが変更されることなく、デバイスクラスを設定することができます。

特定のデバイスクラスを対象とした配置ルールを作成するには：

```
ceph osd crush rule create-replicated <rule-name> <root> <failure-domain> <class>
```

新しいルールを使用するようにプールを変更するには：

```
ceph osd pool set <pool-name> crush_rule <rule-name>
```

デバイスクラスは、使用するデバイスクラスごとに、そのクラスのデバイスのみを含む「shadow」CRUSH階層を作成することで実装されます。CRUSHルールは、このshadow階層上にデータを分散させることができます。この方法は、古いCephクライアントと完全に後方互換性があります。 shadowアイテムを含むCRUSH階層を表示するには：

```
ceph osd crush tree --show-shadow
```

デバイスタイプごとの階層を維持するために手動で作成した CRUSH マップに依存していた Luminous 以前に作成された古いクラスタについては、データ移動をトリガーせずにデバイスクラスに移行するのに役立つ reclassify ツールが利用できます \(「[Migrating from a legacy SSD rule to device classes](https://docs.ceph.com/en/pacific/rados/operations/crush-map/)」を参照してください\)。

### Weights sets[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

ウェイトセットとは、データ配置を計算する際に使用する代替ウェイトのセットです。 CRUSHマップの各デバイスに関連する通常の重みは、デバイスサイズに基づいて設定されており、どこにどれだけのデータを格納すべきかを示しています。 しかし、CRUSHは「確率的」な疑似ランダム配置プロセスであるため、サイコロを60回振っても1と6が10個ずつ出るとは限らないのと同じように、常にこの理想的な分布から多少のばらつきがあります。 ウェイトセットにより、クラスタの仕様（階層、プールなど）に基づいた数値的な最適化を行い、バランスの取れた分布を実現することができます。

サポートするウェイトセットは2種類：

1. **compat**
2. **per\-pool**

ウェイトセットが使用されている場合、階層内の各ノードに関連するウェイトは、以下のコマンドから別の列（（compat）またはプール名のいずれかとラベル付けされている）として表示されます：

```
ceph osd tree
```

compatとper\-poolのウエイトセットの両方が使用されている場合、特定のプールのデータ配置では、per\-poolウエイトセットが存在すれば、それを使用します。 そうでない場合は、compatウェイトセットがあればそれを使用します。 どちらも存在しない場合、通常のCRUSHウェイトが使用されます。

ウェイトセットは手動で設定・操作することもできますが、Luminous以降のリリースを実行する場合は、ceph\-mgr balancerモジュールを有効にして自動的に操作することが推奨されます。

## Modifying the CRUSH map[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

### Add/Move an OSD[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

実行中のクラスタのCRUSHマップにOSDを追加または移動するには：

```
ceph osd crush set {name} {weight} root={root} [{bucket-type}={bucket-name} ...]
```

ここで：

**name**

Description

OSDのフルネーム

Type

String

Required

Yes

Example

osd.0

**weight**

Description

OSDのCRUSHウエイトで、通常はテラバイト\(TB\)単位でサイズを指定

Type

Double

Required

Yes

Example

2.0

**root**

Description

OSDが存在するツリーのルートノード\(通常は"default"\)

Type

Key/value pair.

Required

Yes

Example

root=default

**bucket\-type**

Description

CRUSH階層内のOSDの位置

Type

Key/value pairs.

Required

No

Example

datacenter=dc1 room=room1 row=foo rack=bar host=foo\-bar\-1

次の例では、osd.0を階層に追加したり、以前の場所からOSDを移動させたりしています。

```
ceph osd crush set osd.0 1.0 root=default datacenter=dc1 room=room1 row=foo rack=bar host=foo-bar-1
```

### Adjust OSD weight[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

実行中のクラスタのCRUSHマップでOSDのCRUSHウェイトを調整するには：

```
ceph osd crush reweight {name} {weight}
```

ここで：

**name**

Description

OSDのフルネーム

Type

String

Required

Yes

Example

osd.0

**weight**

Description

OSDのCRUSHウエイト

Type

Double

Required

Yes

Example

2.0

### Remove an OSD[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

実行中のクラスタのCRUSHマップからOSDを削除するには：

```
ceph osd crush remove {name}
```

ここで：

**name**

Description

OSDのフルネーム

Type

String

Required

Yes

Example

osd.0

### Add a Bucket[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

稼働中のクラスタのCRUSHマップにバケットを追加するには、ceph osd crush add\-bucketコマンドを実行します。

```
ceph osd crush add-bucket {bucket-name} {bucket-type}
```

ここで：

**bucket\-name**

Description

バケツのフルネーム

Type

String

Required

Yes

Example

rack12

**bucket\-type**

Description

バケットタイプを指定します。このタイプはすでに階層に存在する必要があります。Type

String

Required

Yes

Example

rack

次の例では、rack12バケットを階層に追加しています。

```
ceph osd crush add-bucket rack12 rack
```

### Move a Bucket[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

バケットをCRUSHマップの階層内の別の場所や位置に移動させるには：

```
ceph osd crush move {bucket-name} {bucket-type}={bucket-name}, [...]
```

ここで：

**bucket\-name**

Description

移動／再配置するバケットの名前

Type

String

Required

Yes

Example

foo\-bar\-1

**bucket\-type**

Description

CRUSH 階層におけるバケットの位置

Type

Key/value pairs.

Required

No

Example

datacenter=dc1 room=room1 row=foo rack=bar host=foo\-bar\-1

### Remove a Bucket[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

CRUSH階層からバケットを削除するには：

```
ceph osd crush remove {bucket-name}
```

注：バケットを空にすると、CRUSH階層から削除されます。

ここで：

**bucket\-name**

Description

削除したいバケットの名前

Type

String

Required

Yes

Example

rack12

次の例では、rack12バケットを階層から削除しています。

```
ceph osd crush remove rack12
```

### Creating a compat weight set[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

compatウェイトセットを作成するには：

```
ceph osd crush weight-set create-compat
```

compatウェイトセットのウェイトを調整するには：

```
ceph osd crush weight-set reweight-compat {name} {weight}
```

compatウェイトセットを削除するには：

```
ceph osd crush weight-set rm-compat
```

### Creating per\-pool weight sets[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

per\-poolウェイトセットを作成するには：

```
ceph osd crush weight-set create {pool-name} {mode}
```

注意：per\-poolウェイト設定には、すべてのサーバーとデーモンでLuminous v12.2.z以降が必要です。

ここで：

**pool\-name**

Description

RADOSプールの名前

Type

String

Required

Yes

Example

rbd

**mode**

Description

flatとpositionalのどちらかです。 flatウェイトセットは、各デバイスまたはバケットに対して単一のウェイトを持ちます。 positionalウエイトセットは、結果として得られる配置マッピングの各位置に対して、異なる重みを持つ可能性があります。 例えば、プールのレプリカ数が3である場合、positionalウエイトセットは、各デバイスとバケットに対して3つのウエイトを持つことになります。

Type

String

Required

Yes

Example

flat

ウェイトセット内のアイテムのウェイトを調整するには：

```
ceph osd crush weight-set reweight {pool-name} {item-name} {weight [...]}
```

既存のウェイトセットを一覧表示するには：

```
ceph osd crush weight-set ls
```

ウェイトセットを削除するには：

```
ceph osd crush weight-set rm {pool-name}
```

### Creating a rule for a replicated pool[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

replicatedプールの場合、CRUSHルールを作成する際の主要な決定は、障害ドメインを何にするかということです。 例えば、障害ドメインとしてホストを選択した場合、CRUSHはデータの各レプリカが一意のホスト上に保存されることを保証します。 もしrackが選択された場合、各レプリカは異なるラックに保存されます。どの障害ドメインを選択するかは、主にクラスタのサイズとトポロジーに依存します。

ほとんどの場合、クラスタ階層全体はdefaultという名前のルート・ノードの下にネストされています。 階層をカスタマイズしている場合は、階層の他のノードにネストされたルールを作成することができます。 そのノードに関連するタイプは問題ではありません（ルート・ノードである必要はありません）。

また、データの配置を特定のクラスのデバイスに制限するルールを作成することも可能です。 デフォルトでは、Ceph OSDは、使用されているデバイスの基本タイプに応じて、自動的にhddまたはssdのいずれかに分類されます。 これらのクラスはカスタマイズすることも可能です。

replicatedルールを作成するには：

```
ceph osd crush rule create-replicated {name} {root} {failure-domain-type} [{class}]
```

ここで：

**name**

Description

ルールの名前

Type

String

Required

Yes

Example

rbd\-rule

**root**

Description

データを配置するノードの名前

Type

String

Required

Yes

Example

default

**failure\-domain\-type**

Description

レプリカを分離すべきCRUSHノードの種類を指定します

Type

String

Required

Yes

Example

rack

**class**

Description

データを載せるべきデバイスクラス

Type

String

Required

No

Example

ssd

### 

### Creating a rule for an erasure coded pool[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

EC（Erasure\-Coded）プールの場合、障害ドメインは何か、データは階層のどのノードに配置されるか（通常はデフォルト）、配置は特定のデバイス・クラスに制限されるかなど、同じ基本的な決定を下す必要があります。 しかし、ECプールは、使用するECに基づいて慎重に構築する必要があるため、作成方法が少し異なります。 このため、ECプロファイルにこの情報を含める必要があります。 CRUSH ルールは、プロファイルを使用してプールを作成する際に、明示的または自動的 に作成されます。

ECプロファイルをリストアップするには：

```
ceph osd erasure-code-profile ls
```

既存のプロフィールを確認するには：

```
ceph osd erasure-code-profile get {profile-name}
```

通常、プロファイルは決して変更しないでください。その代わり、新しいプールを作成するとき、または既存のプールに新しいルールを作成するときに、新しいプロファイルを作成して使用する必要があります。

ECプロファイルは、キーバリューペアのセットで構成されます。 これらのほとんどは、プール内のデータをエンコードしているECの動作を制御します。 ただし、crush\-で始まるものは、作成されるCRUSHルールに影響します。

ECプロファイルの特性は以下の通り：

* **crush\-root**
* **crush\-failure\-domain**
* **crush\-device\-class**
* kとm（lrcプラグインの場合はl）：これらはECのシャードの数を決定し、結果のCRUSHルールに影響を与えます。

プロファイルが定義されると、CRUSHルールを作成することができます：

```
ceph osd crush rule create-erasure {name} {profile-name}
```

### Deleting rules[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

プールで使用されていないルールは削除することができます：

```
ceph osd crush rule rm {rule-name}
```

## Tunables[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

これまで、データの配置を計算するために使用するCRUSHアルゴリズムに改良を加えてきました（現在も改良中）。 この動作の変化をサポートするために、従来のアルゴリズムと改良されたアルゴリズムのどちらを使用するかを制御する一連の調整可能なオプションを導入しました。

新しいチューナブルを使用するためには、クライアントとサーバーの両方が新しいバージョンのCRUSHをサポートする必要があります。 このため、導入されたCephのバージョンにちなんだ名前のプロファイルを作成しました。 たとえば、fireflyチューナブルはFireflyリリースで初めてサポートされ、古い\(Dumplingなど\)クライアントでは動作しません。 特定のチューナブルのセットがレガシーのデフォルト動作から変更されると、ceph\-monとceph\-osdは、新しいCRUSH機能をサポートしない古いクライアントがクラスタに接続できないようにします。

### argonaut \(legacy\)[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

Argonautとそれ以前のリリースで使用されていたレガシーCRUSHの動作は、マークoutされているOSDが多くなければ、ほとんどのクラスタに問題なく機能します。

### bobtail \(CRUSH\_TUNABLES2\)[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

bobtail tunable profileは、いくつかの重要な誤動作を修正するものです。

* リーフバケットのデバイス数が少ない階層では、一部のPGが希望するレプリカ数よりも少ない数にマッピングされることがあります。 これは、「host」ノードの下に少数の（1～3）OSDがネストされている階層でよく起こります。
* 大規模クラスタでは、PGのわずかな割合が、望ましいOSDの数より少ない数にマッピングされることがあります。 これは、複数の階層レイヤー（例：行、ラック、ホスト、OSD）が使用されている場合に、より顕著になります。
* 一部のOSDがマークoutされると、データが階層全体ではなく、近くのOSDに再分配される傾向があります。

新しい調整項目：

* choose\_local\_tries: ローカルリトライの回数。 レガシー値は2、最適値は0
* choose\_local\_fallback\_tries: レガシー値：5、最適値：0
* choose\_total\_tries: 項目を選択するための試行回数の合計。レガシー値は19でしたが、その後のテストにより、典型的なクラスタでは50の値がより適切であることが示されました。 非常に大きなクラスタでは、より大きな値が必要な場合があります。
* chooseleaf\_descend\_once: 再帰的な選択リーフの試行が再試行されるか、あるいは一度だけ試行して元の配置の再試行を許可するかを設定します。 レガシーのデフォルトは0、最適値は1です。

マイグレーションの影響：

* argonautからbobtail tunablesへの移行は、適度な量のデータ移動を誘発します。 すでにデータが投入されているクラスタでは注意が必要です。

### firefly \(CRUSH\_TUNABLES3\)[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

fireflyチューナブル・プロファイルは、あまりにも多くのOSDがマークoutされている場合に、PGマッピングの結果が少なすぎる傾向にある選択リーフCRUSHルール動作の問題を修正します。

新しいチューナブル：

* chooseleaf\_vary\_r: 再帰的なchooseleafの試行を、親が既に行った試行回数に基づき、0以外の値rから開始するかどうかを指定します。 レガシーのデフォルトは0ですが、この値ではCRUSHはマッピングを見つけることができないことがあります。 最適な値は\(計算コストと正しさの観点から\)1です。

マイグレーションの影響：

* 多くのデータが格納されている既存のクラスタでは、0から1に変更すると多くのデータが移動します。4または5の値では、CRUSHは有効なマッピングを見つけることができますが、移動するデータの量は少なくなります。

### straw\_calc\_version tunable \(introduced with Firefly too\)[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

strawアルゴリズム用バケットのCRUSHマップに計算・保存される内部ウエイトに問題がありました。 具体的には、CRUSHの重みが0のアイテムがある場合、あるいは異なるユニークな重みが混在している場合、CRUSHがデータを正しく分配しない（つまり、重みに比例しない）ことがありました。

新しいチューナブル：

* straw\_calc\_version: 値0は、古い、壊れた内部重量計算を維持し、値1は、動作を修正します。

マイグレーションの影響：

* straw\_calc\_version 1に移行してからstrawバケットを調整（アイテムの追加、削除、再重み付け、またはreweight\-allコマンドを使用）すると、クラスタが問題のある条件の1つに当たった場合、小規模から中規模のデータ移動が発生する場合があります。

この調整オプションは、クライアント側で必要とされるカーネルバージョンに全く影響を与えないという点で特別なものです。

### hammer \(CRUSH\_V4\)[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

hammerチューナブルプロファイルは、プロファイルを変更するだけで、既存のCRUSHマップのマッピングに影響を与えることはありません。 しかし：

* 新しいバケットアルゴリズム\(straw2\)がサポートされています。 新しいstraw2バケットアルゴリズムは、オリジナルのstrawにあったいくつかの制限を修正したものです。 具体的には、古いstrawバケットでは、重みが調整されたときに変更されるべきマッピングがいくつか変更されていましたが、straw2では、重みが変更されたバケットアイテムとのマッピングのみを変更するという本来の目的を達成しています。
* straw2 は、新しく作成されたバケットのデフォルトです。

マイグレーションの影響：

* バケツの種類を straw から straw2 に変更すると、バケツのアイテムの重さが互いにどの程度異なるかに応じて、それなりに小さなデータ移動が発生します。 重さがすべて同じであればデータは移動せず、重さが大きく異なる場合は移動が多くなります。

### jewel \(CRUSH\_TUNABLES5\)[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

jewelチューナブルプロファイルはCRUSHの全体的な動作を改善し、OSDがクラスタからマークoutされたときに変更されるマッピングの数を大幅に減らします。 その結果、データの移動が大幅に少なくなります。

新しいチューナブル：

* chooseleaf\_stable: 再帰的なchooseleafの試行が、OSDがマークoutされたときにマッピングの変更回数を大幅に減らす内部ループのために、より良い値を使用するかどうかを指定します。 レガシー値は0であり、新しい値である1は新しいアプローチを使用します。

マイグレーションの影響：

* 既存のクラスタでこの値を変更すると、ほぼすべてのPGマッピングが変更される可能性があるため、非常に大量のデータ移動が発生します。

### Which client versions support CRUSH\_TUNABLES[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

* argonaut series, v0.48.1 or later
* v0.49 or later
* Linux kernel version v3.6 or later \(for the file system and RBD kernel clients\)

### Which client versions support CRUSH\_TUNABLES2[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

* v0.55 or later, including bobtail series \(v0.56.x\)
* Linux kernel version v3.9 or later \(for the file system and RBD kernel clients\)

### Which client versions support CRUSH\_TUNABLES3[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

* v0.78 \(firefly\) or later
* Linux kernel version v3.15 or later \(for the file system and RBD kernel clients\)

### Which client versions support CRUSH\_V4[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

* v0.94 \(hammer\) or later
* Linux kernel version v4.1 or later \(for the file system and RBD kernel clients\)

### Which client versions support CRUSH\_TUNABLES5[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

* v10.0.2 \(jewel\) or later
* Linux kernel version v4.5 or later \(for the file system and RBD kernel clients\)

### Warning when tunables are non\-optimal[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

バージョン0.74以降、現在のCRUSHチューナブルにデフォルトプロファイルの最適値がすべて含まれていない場合、Cephはヘルス警告を発行します\(デフォルトプロファイルの意味については以下を参照してください\)。この警告を表示しないようにするには、2つのオプションがあります。

1. 既存のクラスタのチューナブルを調整します。 この場合、多少のデータ移動が発生することに注意してください（おそらく10%程度）。 これは望ましい方法ですが、データの移動がパフォーマンスに影響する可能性があるため、実稼働クラスタでは注意が必要です。 最適なチューナブルを有効にするには、以下を使用します。

```
ceph osd crush tunables optimal
```

物事がうまくいかず \(負荷が高すぎるなど\)、あまり進展がない場合、またはクライアントの互換性に問題がある場合 \(古いカーネルの CephFS または RBD クライアント、Bobtail 以前の librados クライアント\)、以下でスイッチバックすることが可能です。

```
ceph osd crush tunables legacy
```

1. ceph.conf \[mon\] セクションに以下のオプションを追加することで、CRUSHに変更を加えることなく警告を消すことができます。

```
mon warn on legacy crush tunables = false
```

変更を有効にするには、モニターを再起動するか、モニターを起動している状態でオプションを適用する必要があります。

```
ceph tell mon.\* config set mon_warn_on_legacy_crush_tunables false
```

### A few important points[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

* これらの値を調整すると、ストレージノード間で一部のPGが移動することになります。 Cephクラスタにすでに多くのデータが保存されている場合は、データの一部が移動することを覚悟してください。
* ceph\-osdおよびceph\-monデーモンは、更新されたマップを取得するとすぐに、新しい接続にこの機能ビットを要求するようになります。 しかし、すでに接続されているクライアントは事実上保護されており、新しい機能をサポートしていない場合は誤動作します。
* CRUSHチューナブルを非レガシー値に設定し、その後デフォルト値に戻した場合、ceph\-osdデーモンでこの機能をサポートする必要はありません。 ただし、OSDピアリングプロセスでは、古いマップを調査して理解する必要があります。 したがって、クラスタが以前に非レガシーCRUSH値を使用していた場合は、マップの最新バージョンがレガシーのデフォルトの使用に切り替わったとしても、古いバージョンのceph\-osdデーモンを実行しないでください。

### Tuning CRUSH[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

CRUSHのチューニングを行う最もシンプルな方法は、プロファイルと呼ばれるマッチングセットで適用することです。 Octopus のリリースでは、以下のようなものがあります。

* legacy: argonaut以前からのレガシーな動作
* argonaut: オリジナルのArgonautリリースでサポートされているレガシー値
* bobtail: bobtailリリースでサポートされる値
* firefly: fireflyリリースでサポートされる値
* hammer: hammerリリースでサポートされる値
* jewel: jewelリリースでサポートされる値
* optimal: 現在のバージョンのCephのベスト\(つまり最適\)値
* default:ゼロからインストールされた新しいクラスタのデフォルト値。これらの値は、現在のCephのバージョンに依存し、ハードコードされており、通常、最適な値とレガシーな値が混在しています。これらの値は通常、以前のLTSリリースの最適なプロファイル、またはほとんどのユーザが最新のクライアントを持っていると思われる最新リリースに一致します。

実行中のクラスターにプロファイルを適用するには：

```
ceph osd crush tunables {PROFILE}
```

この場合、データの移動が発生する可能性があり、かなりの量になる可能性があることに注意してください。 実行中のクラスタでプロファイルを変更する前に、リリースノートとドキュメントをよく読んでください。また、バックフィルのbolusの影響を抑えるために、リカバリ/バックフィルのパラメータを調整することも検討してください。

## Primary Affinity[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

Ceph Clientがデータを読み書きする場合、まず、影響を受ける各PGのActing SetのプライマリOSDに連絡します。デフォルトでは、Acting Set内の最初のOSDがプライマリになります。 たとえば、Acting Setセット\[2,3,4\]では、osd.2が最初にリストされているため、プライマリ（別名、リード）OSDとなります。時には、あるOSDが他のOSDよりもleadとして働くのに適していないことがわかっています（たとえば、遅いドライブや遅いコントローラを持っているなど）。ハードウェアの利用率を最大化しながらパフォーマンスのボトルネック（特に読み取り操作）を防ぐために、primary affinity値を調整したり、優先するOSDを最初に選択するCRUSHルールを作ったりして、プライマリOSDの選択に影響を与えることができます。

デフォルトでは、読み取り操作は各PGのプライマリOSDから提供されるため、プライマリOSDの選択を調整することは、主にreplicatedプールに対して有用です。ECプールの場合、読み取り操作を高速化する方法は、プールの設定で説明したようにfast readを有効にすることです。

primary affinityの一般的なシナリオは、クラスタにドライブサイズが混在している場合です。たとえば、1.9 TB SATA SSDを搭載した古いラックと3.84 TB SATA SSDを搭載した新しいラックがあります。 平均して、後者には2倍のPGが割り当てられるため、2倍の書き込みおよび読み取り操作が行われ、前者よりも負荷が高くなります。 OSDサイズに反比例するprimary affinityの大まかな割り当てが100％最適とは言えませんが、SATAインタフェースの帯域とCPUサイクルをより均等に利用することで、全体の読み取りスループットを15％向上させることは容易に達成できるでしょう。

デフォルトでは、すべてのCeph OSDのprimary affinityは1であり、これはどのOSDも等しい確率でプライマリとして機能する可能性があることを示します。

Ceph OSDのprimary affinityを下げ、PGのActing SetでCRUSHがOSDをプライマリとして選択しにくくすることができます。

```
ceph osd primary-affinity <osd-id> <weight>
```

OSDのprimary affinityを\[0\-1\]の範囲の実数で設定することができます。ここで、0はOSDがプライマリとして使用されないことを示し、1はOSDがプライマリとして使用される可能性があることを示しています。 重みがこの両極の間にある場合、CRUSH がその OSD をプライマリとして選択する可能性は低くなる。 リード OSD を選択するプロセスは、相対的なaffinity値に基づく単純な確率よりも微妙なものですが、望ましい値の一次近似値であっても測定可能な結果を得ることは可能です。

### Custom CRUSH Rules[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map/ "Permalink to this headline")

たまに、同じreplicatedプールにSSDとHDDを混在させて、コストとパフォーマンスのバランスを取っているクラスタがあります。HDD OSDのprimary affinityを0に設定することで、各演算セットでSSDに処理を振り分けることができます。また、最初のOSDは常にSSD OSDを選択し、残りのOSDはHDDを選択するCRUSHルールを定義することも可能です。これにより、各 PG のActing Setには、プライマリとして SSD OSD が 1 台、残りは HDD となります。

例えば、以下のCRUSHルールは、：

```
rule mixed_replicated_rule {
        id 11
        type replicated
        min_size 1
        max_size 10
        step take default class ssd
        step chooseleaf firstn 1 type host
        step emit
        step take default class hdd
        step chooseleaf firstn 0 type host
        step emit
}
```

最初の OSD として SSD を選択します。 N 回複製されたプールでは、最初の SSD OSD が N 個の HDD OSD のいずれかと同じ場所にある可能性があるため、N 個の複製が異なるホスト上にあることを保証するために、このルールは N\+1 個の OSD を選択することに留意してください。

この余分なストレージ要件は、SSDとHDDを異なるホストに配置することで回避できます。ただし、SSDを搭載したホストがすべてのクライアント要求を受け取るというトレードオフがあります。 そのため、SSD のホストにはより高速な CPU を、HDD のノードにはより控えめな CPU を検討することができます。 ここで、CRUSH のルート ssd\_hosts と hdd\_hosts には、厳密に同じサーバを含んではいけません。

```
rule mixed_replicated_rule_two {
       id 1
       type replicated
       min_size 1
       max_size 10
       step take ssd_hosts class ssd
       step chooseleaf firstn 1 type host
       step emit
       step take hdd_hosts class hdd
       step chooseleaf firstn -1 type host
       step emit
}
```

また、SSDに障害が発生した場合、PGのデータが代替のプライマリSSD OSDにレプリケートされるまで、PGへのリクエストは一時的に（低速の）HDD OSDから提供されることに注意してください。
