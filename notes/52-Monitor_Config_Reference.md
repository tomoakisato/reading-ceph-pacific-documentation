# 52: Monitor Config Reference

**クリップソース:** [52: Monitor Config Reference — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/configuration/mon-config-ref/)

# Monitor Config Reference[¶](https://docs.ceph.com/en/pacific/rados/configuration/mon-config-ref/ "Permalink to this headline")

Ceph Monitorの設定方法を理解することは、信頼性の高いCeph Storage Clusterを構築するための重要な要素です。すべてのCeph Storage Clustersには、少なくとも1つのモニタがあります。モニタの補完は通常、ほぼ一貫していますが、クラスタ内のモニタを追加、削除、または交換することができます。詳細については、「[Adding/Removing a Monitor](https://docs.ceph.com/en/pacific/rados/configuration/mon-config-ref/)」を参照してください。

## Background[¶](https://docs.ceph.com/en/pacific/rados/configuration/mon-config-ref/ "Permalink to this headline")

Cephモニターはクラスタマップの「マスターコピー」を保持しているため、Cephクライアントは、1つのCephモニターに接続して現在のクラスタマップを取得するだけで、すべてのCephモニター、Ceph OSDデーモン、およびCeph Metadata Serverの場所を特定できます。Cephクライアントは、Ceph OSDデーモンやCeph Metadata Serverの読み取りや書き込みを行う前に、まずCeph Monitorに接続する必要があります。Cephクライアントは、クラスタマップの最新のコピーとCRUSHアルゴリズムを使用して、任意のオブジェクトの位置を計算できます。オブジェクトの位置を計算できるため、Ceph ClientはCeph OSDデーモンと直接対話することができ、これはCephの高いスケーラビリティとパフォーマンスにとって非常に重要な要素です。詳細は「[Scalability and High Availability](https://docs.ceph.com/en/pacific/rados/configuration/mon-config-ref/)」を参照してください。

Ceph Monitorの主な役割は、クラスタマップのマスターコピーを維持することです。Ceph Monitorは、認証サービスとログサービスも提供します。Ceph Monitorは、モニタサービスのすべての変更を1つのPaxosインスタンスに書き込み、Paxosはその変更をキー/バリューストアに書き込んで強力な一貫性を保ちます。Ceph Monitorsは、同期操作中にクラスタマップの最新バージョンを照会できます。Ceph Monitorsは、キー/バリューストアのスナップショットとイテレータ\(leveldbを使用\)を活用してストア全体の同期を実行します。

バージョン0.58以降は非推奨

Cephバージョン0.58以前では、Ceph Monitorsは各サービスにPaxosインスタンスを使用し、マップをファイルとして保存します。

### Cluster Maps[¶](https://docs.ceph.com/en/pacific/rados/configuration/mon-config-ref/ "Permalink to this headline")

クラスタマップは、モニタマップ、OSDマップ、配置グループマップ、メタデータサーバマップなどのマップを合成したものです。クラスタマップは、Ceph Storage Clusterにどのプロセスがあるか、Ceph Storage Clusterにあるプロセスが稼働しているか、停止しているか、配置グループがアクティブか非アクティブか、クリーンかその他の状態か、さらに、ストレージスペースの総量や使用されているストレージの量など、クラスタの現在の状態を反映するその他の詳細など、さまざまな重要事項を追跡します。

たとえば、Ceph OSDデーモンがダウンした、配置グループがデグレード状態になったなど、クラスタの状態に大きな変化があった場合、クラスタマップが更新され、クラスタの現在の状態が反映されます。さらに、Ceph Monitorはクラスタの過去の状態の履歴も保持しています。モニタマップ、OSDマップ、配置グループマップ、メタデータサーバマップは、それぞれのマップバージョンの履歴を保持します。各バージョンを "エポック "と呼びます。

Ceph Storage Clusterを運用する場合、これらの状態を追跡することは、システム管理業務の重要な部分です。詳細については、「[Monitoring a Cluster](https://docs.ceph.com/en/pacific/rados/configuration/mon-config-ref/)」および「[Monitoring OSDs and PGs](https://docs.ceph.com/en/pacific/rados/configuration/mon-config-ref/)」を参照してください。

### Monitor Quorum[¶](https://docs.ceph.com/en/pacific/rados/configuration/mon-config-ref/ "Permalink to this headline")

Configuring cephのセクションでは、テストクラスタで1つのモニタを提供する些細なCeph設定ファイルを提供しています。クラスタは1つのモニタで問題なく動作しますが、1つのモニタは単一の障害点です。本番のCeph Storage Clusterで高可用性を確保するには、複数のモニタでCephを実行して、1つのモニタの障害でクラスタ全体が停止しないようにします。

Ceph Storage Clusterが高可用性のために複数のCeph Monitorを実行している場合、Ceph MonitorはPaxosを使用してマスタークラスターマップに関するコンセンサスを確立します。コンセンサスを得るには、実行中のモニタの過半数がクラスタマップに関するコンセンサスの定足数を確立する必要があります\(例: 1、2/3、3/5、4/ 6、など\)。

**mon force quorum join**

Description

過去にマップから削除されたモニターでも、強制的にクォーラムに参加させる

Type

_Boolean_

Default

_False_

### Consistency[¶](https://docs.ceph.com/en/pacific/rados/configuration/mon-config-ref/ "Permalink to this headline")

Cephの構成ファイルにモニタ設定を追加する際には、Cephモニタのアーキテクチャ上のいくつかの点に注意する必要があります。Cephは、クラスタ内で別のCephモニタを検出する際に、Cephモニタに**厳格な一貫性要件を課します。**Cephクライアントやその他のCephデーモンはCeph設定ファイルを使用してモニタを検出しますが、モニタはCeph設定ファイルではなくモニタマップ\(monmap\)を使用して相互に検出します。

Ceph Monitorは、Ceph Storage Cluster内の他のCeph Monitorを検出するときに、常にモニタマップのローカルコピーを参照します。Ceph構成ファイルではなくモニタマップを使用することで、クラスタを壊す可能性のあるエラーを回避できます\(たとえば、モニタのアドレスまたはポートを指定する際のceph.confのタイプミス\)。モニタは発見のためにモニタマップを使用し、クライアントや他のCephデーモンとモニタマップを共有するので、**モニタマップはモニタに、そのコンセンサスが有効であることを厳密に保証します。**

厳密な一貫性は、モニタマップの更新にも適用されます。Ceph Monitorの他の更新と同様に、モニタマップの変更は常にPaxosという分散型コンセンサスアルゴリズムを介して行われます。Ceph Monitorの追加や削除など、モニタマップの各更新については、定足数の各モニタが同じバージョンのモニタマップを持っているように、Ceph Monitorが合意する必要があります。モニタマップの更新はインクリメンタルに行われるため、Ceph Monitorは合意された最新のバージョンと、以前のバージョンのセットを持つことになります。履歴を維持することで、古いバージョンのモニタを持っているCeph Monitorは、Ceph Storage Clusterの現在の状態に追いつくことができます。

Ceph MonitorがmonmapではなくCeph構成ファイルを介してお互いを発見する場合、Ceph構成ファイルが自動的に更新・配布されないため、新たなリスクが発生します。Ceph Monitorが誤って古いCeph構成ファイルを使用したり、Ceph Monitorを認識できなかったり、クォーラムから外れたり、Paxosがシステムの現在の状態を正確に判断できない状況が発生する可能性があります。

### Bootstrapping Monitors[¶](https://docs.ceph.com/en/pacific/rados/configuration/mon-config-ref/ "Permalink to this headline")

ほとんどの構成と展開の場合、Cephを展開するツールは、モニターマップを生成することでCeph Monitorのブートストラップを支援します\(cephadmなど\)。Ceph Monitorにはいくつかの明示的な設定が必要です。

* **Filesystem ID**
* **Monitor ID**
* **Keys**

ブートストラップの詳細については、「モニターのブートストラップ」を参照してください。

## Configuring Monitors[¶](https://docs.ceph.com/en/pacific/rados/configuration/mon-config-ref/ "Permalink to this headline")

クラスタ全体にコンフィグレーション設定を適用するには、\[global\]にコンフィグレーション設定を入力します。クラスタ内のすべてのモニターにコンフィグレーション設定を適用するには、\[mon\]にコンフィグレーション設定を入力します。特定のモニターにコンフィギュレーション設定を適用するには、モニターインスタンスを指定します（例：\[mon.a\]）。慣例として、モニターインスタンス名にはアルファ表記を使用します。

```
[global]

[mon]

[mon.a]

[mon.b]

[mon.c]

```

### Minimum Configuration[¶](https://docs.ceph.com/en/pacific/rados/configuration/mon-config-ref/ "Permalink to this headline")

Ceph設定ファイルによるCephモニタの最低限のモニタ設定には、各モニタのホスト名とネットワークアドレスが含まれます。これらは\[mon\]の下、または特定のモニタのエントリの下で設定できます。

```
[global]
        mon host = 10.0.0.2,10.0.0.3,10.0.0.4

```

```
[mon.a]
        host = hostname1
        mon addr = 10.0.0.10:6789

```

詳細は「[Network Configuration Reference](https://docs.ceph.com/en/pacific/rados/configuration/mon-config-ref/) 」を参照してください。

注:このモニターの最小構成は、デプロイメントツールがfsidとmon.keyを生成してくれることを前提としています。

Cephクラスタを展開した後は、モニタのIPアドレスを変更すべきではありません。ただし、モニタのIPアドレスを変更する場合は、特定の手順に従う必要があります。詳細については、「 [Changing a Monitor’s IP Address](https://docs.ceph.com/en/pacific/rados/configuration/mon-config-ref/) 」を参照してください。

クライアントは、DNS SRVレコードを使用してモニターを検索することもできます。詳細は、「[Monitor lookup through DNS](https://docs.ceph.com/en/pacific/rados/configuration/mon-config-ref/) 」を参照してください。

### Cluster ID[¶](https://docs.ceph.com/en/pacific/rados/configuration/mon-config-ref/ "Permalink to this headline")

各Ceph Storage Clusterには、一意の識別子\(fsid\)があります。指定されている場合は、通常、設定ファイルの\[global\]セクションの下に表示されます。展開ツールは通常、fsidを生成してモニタマップに保存するため、値が設定ファイルに表示されない場合があります。fsidにより、同じハードウェア上で複数のクラスターのデーモンを実行することが可能になります。

**fsid**

Description

クラスタID。クラスタごとに1つ。

Type

_UUID_

Required

_Yes._

Default

該当なし。指定されていない場合は、デプロイメントツールによって生成されます。

注意：デプロイメントツールを使用している場合は、この値を設定しないでください。

### Initial Members[¶](https://docs.ceph.com/en/pacific/rados/configuration/mon-config-ref/ "Permalink to this headline")

高可用性を確保するために、本番用Ceph Storage Clusterを少なくとも3つのCeph Monitorで実行することをお勧めします。複数のモニタを実行する場合は、クォーラムを確立するためにクラスタのメンバーになる必要のある初期モニタを指定することができます。これにより、クラスタがオンラインになるまでの時間が短縮される場合があります。

```
[mon]
        mon_initial_members = a,b,c
```

**mon\_initial\_members**

Description

起動時のクラスタ内の初期モニターのID。指定すると、Cephは初期クォーラムを形成するために奇数のモニターを必要とします\(例: 3\)。

Type

_String_

Default

_None_

注：クォーラムを確立するためには、クラスタ内の過半数のモニターが相互に通信可能である必要があります。この設定では、クォーラムを確立するためのモニターの初期数を減らすことができます。

### Data[¶](https://docs.ceph.com/en/pacific/rados/configuration/mon-config-ref/ "Permalink to this headline")

Cephでは、Ceph Monitorsがデータを保存するデフォルトパスが用意されています。本番のCeph Storage Clusterで最適なパフォーマンスを得るには、Ceph MonitorsをCeph OSDデーモンとは別のホストとドライブで実行することをお勧めします。leveldbはデータの書き込みにmmap\(\)を使用するため、Ceph Monitorsはデータをメモリからディスクに非常に頻繁にフラッシュします。データストアがCeph OSDデーモンと同居している場合、Ceph OSDデーモンのワークロードに干渉する可能性があります。

Cephバージョン0.58以前では、Cephモニターはデータをプレーンファイルに保存します。このアプローチにより、ユーザはlsやcatなどの一般的なツールでモニタデータを検査することができます。ただし、このアプローチでは強力な一貫性が得られませんでした。

Cephバージョン0.59以降では、Ceph Monitorsはデータをキー/値ペアとして格納します。Ceph MonitorにはACIDトランザクションが必要です。データストアを使用すると、復旧したCeph Monitorsが破損したバージョンをPaxosで実行するのを防ぐことができ、1つのアトミックバッチで複数の修正操作が可能になるなどの利点があります。

一般に、デフォルトのデータの場所を変更することはお勧めしません。デフォルトの場所を変更する場合は、設定ファイルの\[mon\]セクションで設定して、Ceph Monitor全体で統一することをお勧めします。

**mon\_data**

Description

モニターのデータの場所です。

Type

_String_

Default

_/var/lib/ceph/mon/$cluster\-$id_

**mon\_data\_size\_warn**

Description

モニターのデータストアがこのサイズ（デフォルトでは15GB）よりも大きくなった場合、HEALTH\_WARNステータスを発生させます。

Type

_Integer_

Default

_15\*1024\*1024\*1024_

**mon\_data\_avail\_warn**

Description

モニターのデータストアを格納しているファイルシステムの利用可能な容量が、この割合以下になった場合、HEALTH\_WARNステータスを発生させます。

Type

_Integer_

Default

_30_

**mon\_data\_avail\_crit**

Description

モニターのデータストアを格納しているファイルシステムの利用可能な容量が、この割合以下であると報告された場合、HEALTH\_ERRステータスを発生させます。

Type

_Integer_

Default

_5_

**mon\_warn\_on\_cache\_pools\_without\_hit\_sets**

Description

キャッシュプールにhit\_set\_typeの値が設定されていない場合、HEALTH\_WARNを発生させます。詳細はhit\_set\_typeを参照してください。

Type

_Boolean_

Default

_True_

**mon\_warn\_on\_crush\_straw\_calc\_version\_zero**

Description

CRUSH straw\_calc\_versionが0の場合にHEALTH\_WARNを発生させます。詳細はCRUSH map tunablesを参照してください。

Type

_Boolean_

Default

_True_

**mon\_warn\_on\_legacy\_crush\_tunables**

Description

CRUSHチューナブルが古すぎる（mon\_min\_crush\_required\_versionより古い）場合にHEALTH\_WARNを発生させる。

Type

_Boolean_

Default

_True_

**mon\_crush\_min\_required\_version**

Description

クラスタが必要とする最小のチューナブル・プロファイル。詳細は「[CRUSH map tunables](https://docs.ceph.com/en/pacific/rados/configuration/mon-config-ref/)」を参照してください。

Type

_String_

Default

_hammer_

**mon\_warn\_on\_osd\_down\_out\_interval\_zero**

Description

mon\_osd\_down\_out\_intervalが0のとき、HEALTH\_WARNを発生させる。リーダーにこのオプションを0に設定すると、nooutフラグと同じように動作します。nooutフラグが設定されていないクラスタで何が問題なのかを理解するのは難しいですが、同じような動作をするので、この場合は警告を報告します。

Type

_Boolean_

Default

_True_

**mon\_warn\_on\_slow\_ping\_ratio**

Description

OSD間のハートビートがosd\_heartbeat\_graceのmon\_warn\_on\_slow\_ping\_ratioを超えた場合、HEALTH\_WARNを発生させる。 デフォルトは5%です。

Type

_Float_

Default

_0.05_

**mon\_warn\_on\_slow\_ping\_time**

Description

mon\_warn\_on\_slow\_ping\_ratio を特定の値で上書きする。OSD間のハートビートがmon\_warn\_on\_slow\_ping\_timeミリ秒を超えた場合、HEALTH\_WARNを発生させます。 デフォルトは0（無効）です。

Type

_Integer_

Default

_0_

**mon\_warn\_on\_pool\_no\_redundancy**

Description

レプリカを持たないプールが構成されている場合、HEALTH\_WARNを発生させます。

Type

_Boolean_

Default

_True_

**mon\_cache\_target\_full\_warn\_ratio**

Description

プールのcache\_target\_fullとtarget\_max\_objectの間の位置で警告を開始する。

Type

_Float_

Default

_0.66_

**mon\_health\_to\_clog**

Description

健全性の概要をクラスタログに定期的に送信することを有効にします。

Type

_Boolean_

Default

_True_

**mon\_health\_to\_clog\_tick\_interval**

Description

モニターがクラスタログにヘルスサマリーを送信する頻度\(秒単位\)を指定します\(正の数でない場合は無効\)。現在のヘルスサマリーが空であったり、前回と同じであったりする場合、モニタはクラスタログに送信しません。

Type

_Float_

Default

_60.0_

**mon\_health\_to\_clog\_interval**

Description

モニターがクラスタ・ログにヘルス・サマリーを送信する頻度（秒単位）を指定します（正の数でない場合は無効）。モニターは、前回のサマリーと異なるかどうかに関わらず、常にサマリーをクラスター・ログに送信します。

Type

_Integer_

Default

_3600_

### Storage Capacity[¶](https://docs.ceph.com/en/pacific/rados/configuration/mon-config-ref/ "Permalink to this headline")

Ceph Storage Clusterが最大容量に近づくと\(\`mon\_osd\_full ratio\`を参照\)、Cephはデータ損失を防ぐための安全策として、OSDへの書き込みやOSDからの読み込みを阻止します。そのため、本番のCeph Storage Clusterをフルレシオに近づけてしまうと、高可用性が犠牲になってしまうため、良い方法ではありません。デフォルトのフルレシオは0.95、つまり容量の95%です。これは、少数のOSDを持つテストクラスタにとっては非常に積極的な設定です。

ヒント:クラスタを監視する際には、ニアフルレシオに関連する警告に注意してください。これは、いくつかのOSDに障害が発生した場合、一時的にサービスが停止する可能性があることを意味します。ストレージ容量を増やすために、OSDの増設を検討してください。

テストクラスタの一般的なシナリオは、システム管理者がCeph Storage ClusterからOSDを削除し、クラスタがリバランスするのを見て、次に別のOSDを削除し、さらに別のOSDを削除し、少なくとも1つのOSDが最終的にフルレシオに達してクラスタがロックするというものです。テストクラスタであっても、少しは容量計画を立てることをお勧めします。計画を立てることで、高可用性を維持するためにどの程度の予備容量が必要かを判断することができます。理想的には、一連のCeph OSDデーモンの障害が発生した場合に、それらのOSDをすぐに交換せずにクラスタがactive\+clean状態に回復できるように計画することをお勧めします。クラスタの動作はactive\+degraded状態で継続しますが、これは通常の動作としては理想的ではないため、速やかに対処する必要があります。

次の図は、33台のCeph Nodesを含む単純なCeph Storage Clusterで、ホストごとに1つのOSDがあり、各OSDが3TBのドライブを読み書きしている様子を示しています。したがって、この例示的なCeph Storage Clusterの実際の最大容量は99TBとなります。mon osd fullr atioが0.95の場合、Ceph Storage Clusterの残りの容量が5TBになると、クラスタはCephクライアントによるデータの読み書きを許可しません。つまり、Ceph Storage Clusterの動作容量は99TBではなく95TBです。

このようなクラスタでは、1つまたは2つのOSDが故障するのが普通です。頻度は低いものの、合理的なシナリオとしては、ラックのルーターまたは電源が故障し、複数のOSDが同時にダウンすることが挙げられます（例：OSD 7～12）。このようなシナリオでは、すぐにOSDを追加して数台のホストを追加することになったとしても、稼働状態を維持し、アクティブ\+クリーンな状態を実現できるクラスタを目指すべきである。容量使用率が高すぎる場合、データを失うことはないかもしれませんが、クラスタの容量使用率が完全な比率を超えてしまうと、障害ドメイン内の障害を解決する際にデータの可用性が犠牲になる可能性があります。このような理由から、少なくとも大まかな容量計画を立てることをお勧めします。

クラスターの2つの値を特定する。

1. OSDの数
2. クラスターの総容量

クラスタの総容量をクラスタ内のOSDの数で割ると、クラスタ内のOSDの平均平均容量がわかります。この数字に、通常の運用時に同時に故障すると予想されるOSDの数（比較的少ない数）を乗じることを検討します。最後に、クラスタの容量にフルレシオを掛けて最大動作容量を算出し、次に、故障すると予想されるOSDのデータ量の数を差し引いて、妥当なフルレシオを算出します。前述のプロセスを、より多くのOSD故障数（例えば、OSDのラック）で繰り返し、フルレシオに近い合理的な数に到達する。

以下の設定は、クラスタ作成時にのみ適用され、その後OSDMapに保存されます。誤解のないように申し上げますが、通常の運用でOSDが使用する値は、設定ファイルや集中設定ストアにあるものではなく、OSDMapにあるものです。

```
[global]
        mon_osd_full_ratio = .80
        mon_osd_backfillfull_ratio = .75
        mon_osd_nearfull_ratio = .70
```

**mon\_osd\_full\_ratio**

Description

OSDが満杯と判断するまでに使用されるデバイス容量の閾値。

Type

_Float_

Default

_0.95_

**mon\_osd\_backfillfull\_ratio**

Description

OSDが backfill ができないほど満杯になるまでに使用されるデバイススペースの閾値。

Type

_Float_

Default

_0.90_

**mon\_osd\_nearfull\_ratio**

Description

OSDが満杯に近いと判断されるまでに使用されているデバイス領域の割合を示す閾値。

Type

_Float_

Default

_0.85_

ヒント：一部のOSDが満杯に近い状態で、他のOSDが十分な容量を持っている場合、満杯に近い状態のOSDに設定されたCRUSH重量が不正確である可能性があります。

ヒント:これらの設定はクラスタ作成時にのみ適用されます。その後、OSDMapでcephosdset\-nearfull\-ratioおよびcephosdset\-full\-ratioを使用して変更する必要があります。

### Heartbeat[¶](https://docs.ceph.com/en/pacific/rados/configuration/mon-config-ref/ "Permalink to this headline")

Cephのモニタは、各OSDからのレポートを要求したり、隣接するOSDの状態に関するレポートをOSDから受け取ったりすることで、クラスタについて知っています。Cephでは、モニタとOSDの相互作用について妥当なデフォルト設定が用意されていますが、必要に応じて変更することができます。詳細については、「[Monitor/OSD Interaction](https://docs.ceph.com/en/pacific/rados/configuration/mon-config-ref/)」を参照してください。

[54: Configuring Monitor/OSD Interaction](54-Configuring_Monitor-OSD_Interaction.md)

### Monitor Store Synchronization[¶](https://docs.ceph.com/en/pacific/rados/configuration/mon-config-ref/ "Permalink to this headline")

本番環境のクラスタを複数のモニタで運用する場合（推奨）、各モニタは隣接するモニタのクラスタマップがより新しいバージョンであるかどうかを確認します（例えば、隣接するモニタのマップのエポック番号がインスタントモニタのマップの最新のエポック番号よりも1つ以上大きい）。定期的に、クラスター内の1つのモニターが他のモニターから遅れをとることがあります。その場合、そのモニターはクォーラムを離れ、同期をとってクラスターに関する最新の情報を取得し、その後、再びクォーラムに参加しなければなりません。同期化のために、モニターは3つの役割のうちの1つを担うことができます。

1. **Leader**
2. **Provider**
3. **Requester:**

これらの役割により、リーダーは同期作業をプロバイダーに委任することができ、同期要求がリーダーの負担になるのを防ぎ、パフォーマンスを向上させることができます。次の図では、リクエスタが他のモニタに遅れをとっていることを知りました。リクエスタはリーダーに同期を要求し、リーダーはリクエスタにプロバイダとの同期を指示します。

同期は、新しいモニターがクラスターに参加するときに常に発生します。実行時の操作では、モニターは異なるタイミングでクラスタマップの更新を受け取ることがあります。これは、リーダーとプロバイダーの役割があるモニターから別のモニターに移行する可能性があることを意味します。同期中にこのような事態が発生した場合 \(プロバイダがリーダーに遅れをとった場合など\)、プロバイダはリクエスタとの同期を終了させることができます。

同期が完了すると、Cephはクラスタ全体でトリミングを実行します。トリミングには、配置グループがactive\+cleanであることが必要です。

**mon\_sync\_timeout**

Description

モニターが、同期プロバイダからの次の更新メッセージを待ってから、あきらめて再び起動するまでの秒数

Type

_Double_

Default

_60.0_

**mon\_sync\_max\_payload\_size**

Description

同期ペイロードの最大サイズ（単位：バイト）

Type

_32\-bit Integer_

Default

_1048576_

**paxos\_max\_join\_drift**

Description

モニターのデータストアを最初に同期しなければならないまでのPaxosの最大イテレーション数。モニターは、そのピアがあまりにも先に進んでいることを発見すると、先に進む前にまずデータストアを同期します。

Type

_Integer_

Default

_10_

**paxos\_stash\_full\_interval**

Description

PaxosServiceの状態の完全なコピーを保存する頻度\(コミット数\)を指定します。現在、この設定は、mds、mon、auth、mgrのPaxosServicesにのみ影響します。

Type

_Integer_

Default

_25_

**paxos\_propose\_interval**

Description

この時間間隔で更新情報を集めてから、マップの更新を提案します。

Type

_Double_

Default

_1.0_

**paxos\_min**

Description

Paxos状態を維持するための最小数

Type

_Integer_

Default

_500_

**paxos\_min\_wait**

Description

活動していない期間の後にアップデートを収集するための最小時間。

Type

_Double_

Default

_0.05_

**paxos\_trim\_min**

Description

トリミング前に許容される追加提案の数

Type

_Integer_

Default

_250_

**paxos\_trim\_max**

Description

一度にトリミングする追加提案の最大数

Type

_Integer_

Default

_500_

**paxos\_service\_trim\_min**

Description

トリムのトリガーとなる最小のバージョン数（0であれば無効）

Type

_Integer_

Default

_250_

**paxos\_service\_trim\_max**

Description

1回のプロポーザル中にトリミングするバージョン数の最大値（0であれば無効）

Type

_Integer_

Default

_500_

**paxos service trim max multiplier**

Description

トリムサイズが大きい場合に、新しい上限値を得るために、paxosサービスのtrim maxに乗じる係数（0は無効）

Type

_Integer_

Default

_20_

**mon mds force trim to**

Description

モニターがmdsmapsをこのポイントまで強制的にトリムする（0は無効）

Type

_Integer_

Default

_0_

**mon\_osd\_force\_trim\_to**

Description

指定されたエポックでクリーンでないPGがあっても、モニタはこのポイントまでosdmapsを強制的にトリムする（0は無効）

Type

_Integer_

Default

_0_

**mon\_osd\_cache\_size**

Description

基礎となるストアのキャッシュに依存しないための、osdmapsのキャッシュサイズ

Type

_Integer_

Default

_500_

**mon\_election\_timeout**

Description

選挙の提案者での全ACKの最大待ち時間（秒）

Type

_Float_

Default

_5.00_

**mon\_lease**

Description

モニタのバージョンのリース期間（秒）

Type

_Float_

Default

_5.00_

**mon\_lease\_renew\_interval\_factor**

Description

mon\_lease \* mon\_lease\_renew\_interval\_factor は、リーダが他のモニタのリースを更新する間隔。ファクターは1.0以下でなければならない。

Type

_Float_

Default

_0.60_

**mon\_lease\_ack\_timeout\_factor**

Description

リーダは、プロバイダがリース延長を承認するまで、mon\_lease \* mon\_lease\_ack\_timeout\_factor を待つ

Type

_Float_

Default

_2.00_

**mon\_accept\_timeout\_factor**

Description

リーダは、リクエスターがPaxosアップデートを受け入れるまでmon\_lease \* mon\_accept\_timeout\_factor待つ。同様の目的で、Paxosの回復フェーズでも使用される。

Type

_Float_

Default

_2.00_

**mon\_min\_osdmap\_epochs**

Description

常時保持するOSDマップエポックの最小数

Type

_32\-bit Integer_

Default

_500_

**mon\_max\_log\_epochs**

Description

モニタが保持するログエポックの最大数

Type

_32\-bit Integer_

Default

_500_

### Clock[¶](https://docs.ceph.com/en/pacific/rados/configuration/mon-config-ref/ "Permalink to this headline")

Cephのデーモンは重要なメッセージを相互に渡し、デーモンがタイムアウトのしきい値に達する前に処理しなければなりません。Cephモニタのクロックが同期していないと、さまざまな異常が発生する可能性があります。たとえば、以下のようなことです。

* デーモンが受信したメッセージを無視する（例：タイムスタンプが古い）
* メッセージの受信が間に合わなかった場合、タイムアウトが早すぎたり遅すぎたりする。

詳しくは「[Monitor Store Synchronization](https://docs.ceph.com/en/pacific/rados/configuration/mon-config-ref/)」をご覧ください。

ヒント:モニタクラスタが同期したクロックで動作するように、CephモニタホストにNTPまたはPTPデーモンを設定する必要があります。モニターホストが互いに同期するだけでなく、複数の高品質なアップストリームの時間ソースと同期することが有利になる場合があります。

NTPを使用すると、不一致がまだ害になっていなくても、クロックドリフトが目立ってしまうことがあります。NTPが妥当なレベルの同期を維持していても、Cephのクロックドリフト/クロックスキューの警告が発生することがあります。このような状況では、クロックドリフトの増加は許容できるかもしれません。ただし、作業負荷、ネットワーク遅延、デフォルトのタイムアウトに対するオーバーライドの設定、モニタストアの同期設定などの多くの要因が、Paxosの保証を損なうことなく許容できるクロックドリフトのレベルに影響を与える可能性があります。

Cephには以下の調整可能なオプションがあり、許容できる値を見つけることができます。

**mon\_tick\_interval**

Description

モニタの目盛り間隔（秒）

Type

_32\-bit Integer_

Default

_5_

**mon\_clock\_drift\_allowed**

Description

モニタ間で許容されるクロックドリフト（秒）

Type

_Float_

Default

_0.05_

**mon\_clock\_drift\_warn\_backoff**

Description

クロックドリフト警告のための指数関数バックオフ

Type

_Float_

Default

_5.00_

**mon\_timecheck\_interval**

Description

リーダのタイムチェック間隔（クロックドリフトチェック）を秒単位で指定

Type

_Float_

Default

_300.00_

**mon\_timecheck\_skew\_interval**

Description

リーダに秒単位のスキューがある場合のタイムチェック間隔（クロックドリフトチェック）を秒単位で指定

Type

_Float_

Default

_30.00_

### Client[¶](https://docs.ceph.com/en/pacific/rados/configuration/mon-config-ref/ "Permalink to this headline")

**mon\_client\_hunt\_interval**

Description

クライアントは、接続を確立するまで、N秒ごとに新しいモニターを試みる

Type

_Double_

Default

_3.00_

**mon\_client\_ping\_interval**

Description

クライアントは、N秒ごとにモニタにpingを行う

Type

_Double_

Default

_10.00_

**mon\_client\_max\_log\_entries\_per\_message**

Description

クライアントメッセージごとにモニターが生成するログエントリの最大数

Type

_Integer_

Default

_1000_

**mon\_client\_bytes**

Description

メモリ上で許可されるクライアントメッセージデータの量（バイト）

Type

_64\-bit Integer Unsigned_

Default

_100ul\<\<20_

## Pool settings[¶](https://docs.ceph.com/en/pacific/rados/configuration/mon-config-ref/ "Permalink to this headline")

バージョンv0.94以降では、プールへの変更を許可または禁止するプールフラグがサポートされています。適切に設定されていれば、モニターはプールの削除を禁止することもできます。このガードレールの不便さは、誤ってプール（つまりデータ）を削除してしまうことの多さに勝るとも劣りません。

**mon\_allow\_pool\_delete**

Description

モニタは、プールのフラグがどうなっていても、プールの削除を認めるべきかどうか

Type

_Boolean_

Default

_false_

**osd\_pool\_default\_ec\_fast\_read**

Description

プールの高速読み取りをオンにするかどうか。作成時にfast\_readが指定されていない場合、新規に作成されたEC付きプールのデフォルト設定として使用される

Type

_Boolean_

Default

_false_

**osd\_pool\_default\_flag\_hashpspool**

Description

新規プールへのhashpspoolフラグの設定

Type

_Boolean_

Default

_true_

**osd\_pool\_default\_flag\_nodelete**

Description

新規プールに nodelete フラグを設定し、プールの削除を防止する

Type

_Boolean_

Default

_false_

**osd\_pool\_default\_flag\_nopgchange**

Description

新規プールに nopgchange フラグを設定する。PGの数の変更を許可しない。

Type

_Boolean_

Default

_false_

**osd\_pool\_default\_flag\_nosizechange**

Description

新しいプールに nosizechange フラグを設定する。サイズの変更を許可しない。

Type

_Boolean_

Default

_false_

プールフラグの詳細については、「[Pool values](https://docs.ceph.com/en/pacific/rados/configuration/mon-config-ref/)」を参照 

## Miscellaneous[¶](https://docs.ceph.com/en/pacific/rados/configuration/mon-config-ref/ "Permalink to this headline")

**mon\_max\_osd**

Description

クラスタで許容されるOSDの最大数

Type

_32\-bit Integer_

Default

_10000_

**mon\_globalid\_prealloc**

Description

クラスタ内のクライアントおよびデーモンに事前に割り当てるグローバルIDの数

Type

_32\-bit Integer_

Default

_10000_

**mon\_subscribe\_interval**

Description

サブスクリプションの更新間隔（秒）。サブスクリプションの仕組みにより、クラスタマップやログ情報を取得することができる

Type

_Double_

Default

_86400.00_

**mon\_stat\_smooth\_intervals**

Description

直近のN個のPGマップの統計情報を平滑化する

Type

_Integer_

Default

_6_

**mon\_probe\_timeout**

Description

モニターがブートストラップの前にピアを見つけるのを待つ秒数

Type

_Double_

Default

_2.00_

**mon\_daemon\_bytes**

Description

メタデータサーバおよびOSDメッセージのメッセージメモリキャップ（単位：バイト）。

Type

_64\-bit Integer Unsigned_

Default

_400ul\<\<20_

**mon\_max\_log\_entries\_per\_event**

Description

1イベントあたりのログエントリの最大数

Type

_Integer_

Default

_4096_

**mon\_osd\_prime\_pg\_temp**

Description

外れたOSDがクラスタに戻ってきたときに、以前のOSDでPGマップのプライミングを行うことを有効または無効にする。trueに設定すると、PGに新しく入ってきたOSDがプライミングするまで、クライアントは以前のOSDを使い続ける

Type

_Boolean_

Default

_true_

**mon\_osd\_primepgtempmaxtime**

Description

外出中のOSDがクラスタに戻ってきたときに、モニタがPGMAPの起動を試みる時間を秒単位で指定する

Type

_Float_

Default

_0.50_

**mon\_osd\_prime\_pg\_temp\_max\_time\_estimate**

Description

すべてのPGを並行してプライミングする前の、各PGにかかる時間の最大推定値

Type

_Float_

Default

_0.25_

**mon\_mds\_skip\_sanity**

Description

FSMapの安全性アサーションをスキップする（バグの場合、とにかく続行したい場合）。FSMapのサニティチェックに失敗した場合、モニタは終了するが、このオプションを有効にすることで、これを無効にすることができる

Type

_Boolean_

Default

_False_

**mon\_max\_mdsmap\_epochs**

Description

1つのプロポーザル中にトリミングするmdsmapエポックの最大数

Type

_Integer_

Default

_500_

**mon\_config\_key\_max\_entry\_size**

Description

config\-keyエントリの最大サイズ（単位：バイト）

Type

_Integer_

Default

_65536_

**mon\_scrub\_interval**

Description

保存されているチェックサムと、保存されているすべてのキーについて計算されたチェックサムを比較することにより、モニターがストアをスクラブする頻度。\(0は無効。危険なので使用には注意\)

Type

_Seconds_

Default

_1day_

**mon\_scrub\_max\_keys**

Description

毎回スクラブするキーの最大数

Type

_Integer_

Default

_100_

**mon\_compact\_on\_start**

Description

ceph\-monの起動時に、Ceph Monitorストアとして使用するデータベースをコンパクトにする。手動コンパクションは、通常のコンパクションが機能しない場合に、モニタデータベースを縮小してパフォーマンスを向上させるのに役立つ

Type

_Boolean_

Default

_False_

**mon\_compact\_on\_bootstrap**

Description

ブートストラップ時にCeph Monitorストアとして使用するデータベースをコンパクトにする。モニターは、ブートストラップ後にクォーラムを確立するためにお互いにプローブする。クォーラムに参加する前にモニターがタイムアウトした場合は、最初からやり直して再度ブートストラップを行う

Type

_Boolean_

Default

_False_

**mon\_compact\_on\_trim**

Description

あるプレフィックス（paxosを含む）の古い状態を整理してコンパクトにする

Type

_Boolean_

Default

_True_

**mon\_cpu\_threads**

Description

モニタ上でCPU負荷の高い作業を行うためのスレッドの数

Type

_Integer_

Default

_4_

**mon\_osd\_mapping\_pgs\_per\_chunk**

Description

PGからOSDへのマッピングをチャンク単位で計算する。このオプションでは、チャンクごとのPGの数を指定する

Type

_Integer_

Default

_4096_

**mon\_session\_timeout**

Description

モニタは、この制限時間を超えてアイドル状態にある非アクティブなセッションを終了する

Type

_Integer_

Default

_300_

**mon\_osd\_cache\_size\_min**

Description

osdモニタのキャッシュのために、メモリにマップされた状態を維持する最小のバイト数

Type

_64\-bit Integer_

Default

_134217728_

**mon\_memory\_target**

Description

キャッシュオートチューニングを有効にした場合に、OSDモニターキャッシュおよびKVキャッシュに係るバイト数をメモリにマッピングしておく

Type

_64\-bit Integer_

Default

_2147483648_

**mon\_memory\_autotune**

Description

OSDモニタやKVデータベースに使用するキャッシュメモリを自動調整する

Type

_Boolean_

Default

_True_
