# 372: Peering — Ceph Documentation

 # Peering[¶](https://docs.ceph.com/en/pacific/dev/peering/ "Permalink to this headline")

## Concepts[¶](https://docs.ceph.com/en/pacific/dev/peering/ "Permalink to this headline")

_Peering_the process of bringing all of the OSDs that store a Placement Group \(PG\) into agreement about the state of all of the objects \(and their metadata\) in that PG. Note that agreeing on the state does not mean that they all have the latest contents.

_Acting set_the ordered list of OSDs who are \(or were as of some epoch\) responsible for a particular PG.

_Up set_the ordered list of OSDs responsible for a particular PG for a particular epoch according to CRUSH. Normally this is the same as the _acting set_, except when the _acting set_ has been explicitly overridden via _PG temp_ in the OSDMap.

_PG temp_a temporary placement group acting set used while backfilling the primary osd. Let say acting is \[0,1,2\] and we are active\+clean. Something happens and acting is now \[3,1,2\]. osd 3 is empty and can’t serve reads although it is the primary. osd.3 will see that and request a _PG temp_ of \[1,2,3\] to the monitors using a MOSDPGTemp message so that osd.1 temporarily becomes the primary. It will select osd.3 as a backfill peer and continue to serve reads and writes while osd.3 is backfilled. When backfilling is complete, _PG temp_ is discarded and the acting set changes back to \[3,1,2\] and osd.3 becomes the primary.

_current interval_ or _past interval_a sequence of OSD map epochs during which the _acting set_ and _up set_ for particular PG do not change

_primary_the \(by convention first\) member of the _acting set_, who is responsible for coordination peering, and is the only OSD that will accept client initiated writes to objects in a placement group.

_replica_a non\-primary OSD in the _acting set_ for a placement group \(and who has been recognized as such and _activated_ by the primary\).

_stray_an OSD who is not a member of the current _acting set_, but has not yet been told that it can delete its copies of a particular placement group.

_recovery_ensuring that copies of all of the objects in a PG are on all of the OSDs in the _acting set_. Once_peering_ has been performed, the primary can start accepting write operations, and _recovery_ can proceed in the background.

_PG info_ basic metadata about the PG’s creation epoch, the versionfor the most recent write to the PG, _last epoch started_, _last epoch clean_, and the beginning of the _current interval_. Any inter\-OSD communication about PGs includes the _PG info_, such that any OSD that knows a PG exists \(or once existed\) also has a lower bound on _last epoch clean_ or _last epoch started_.

_PG log_a list of recent updates made to objects in a PG. Note that these logs can be truncated after all OSDs in the _acting set_ have acknowledged up to a certain point.

_missing set_Each OSD notes update log entries and if they imply updates to the contents of an object, adds that object to a list of needed updates. This list is called the _missing set_ for that \<OSD,PG\>.

_Authoritative History_a complete, and fully ordered set of operations that, if performed, would bring an OSD’s copy of a Placement Group up to date.

_epoch_a \(monotonically increasing\) OSD map version number

_last epoch start_the last epoch at which all nodes in the _acting set_for a particular placement group agreed on an_authoritative history_. At this point, _peering_ is deemed to have been successful.

_up\_thru_before a primary can successfully complete the _peering_ process, it must inform a monitor that is alive through the current OSD map epoch by having the monitor set its _up\_thru_ in the osd map. This helps peering ignore previous _acting sets_ for which peering never completed after certain sequences of failures, such as the second interval below:

* _acting set_ = \[A,B\]
* _acting set_ = \[A\]
* _acting set_ = \[\] very shortly after \(e.g., simultaneous failure, but staggered detection\)
* _acting set_ = \[B\] \(B restarts, A does not\)

_last epoch clean_the last epoch at which all nodes in the _acting set_for a particular placement group were completely up to date \(both PG logs and object contents\). At this point, _recovery_ is deemed to have been completed.

## Description of the Peering Process[¶](https://docs.ceph.com/en/pacific/dev/peering/ "Permalink to this headline")

The _Golden Rule_ is that no write operation to any PG is acknowledged to a client until it has been persisted by all members of the _acting set_ for that PG. This means that if we can communicate with at least one member of each _acting set_ since the last successful _peering_, someone will have a record of every \(acknowledged\) operation since the last successful _peering_. This means that it should be possible for the current primary to construct and disseminate a new _authoritative history_.

It is also important to appreciate the role of the OSD map \(list of all known OSDs and their states, as well as some information about the placement groups\) in the _peering_process:

> When OSDs go up or down \(or get added or removed\) this has the potential to affect the _active sets_of many placement groups.
> 
> Before a primary successfully completes the _peering_process, the OSD map must reflect that the OSD was alive and well as of the first epoch in the _current interval_.
> 
> Changes can only be made after successful _peering_.

Thus, a new primary can use the latest OSD map along with a recent history of past maps to generate a set of _past intervals_ to determine which OSDs must be consulted before we can successfully_peer_. The set of past intervals is bounded by _last epoch started_, the most recent _past interval_ for which we know _peering_ completed. The process by which an OSD discovers a PG exists in the first place is by exchanging _PG info_ messages, so the OSD always has some lower bound on _last epoch started_.

The high level process is for the current PG primary to:

> 1. get a recent OSD map \(to identify the members of the all interesting _acting sets_, and confirm that we are still the primary\).
> 2. generate a list of _past intervals_ since _last epoch started_. Consider the subset of those for which _up\_thru_ was greater than the first interval epoch by the last interval epoch’s OSD map; that is, the subset for which _peering_ could have completed before the _acting set_ changed to another set of OSDs.
> Successful _peering_ will require that we be able to contact at least one OSD from each of _past interval_’s _acting set_.
> 3. ask every node in that list for its _PG info_, which includes the most recent write made to the PG, and a value for _last epoch started_. If we learn about a _last epoch started_ that is newer than our own, we can prune older _past intervals_ and reduce the peer OSDs we need to contact.
> 4. if anyone else has \(in its PG log\) operations that I do not have, instruct them to send me the missing log entries so that the primary’s_PG log_ is up to date \(includes the newest write\)..
> 5. for each member of the current _acting set_:
> 1. ask it for copies of all PG log entries since _last epoch start_so that I can verify that they agree with mine \(or know what objects I will be telling it to delete\).
> If the cluster failed before an operation was persisted by all members of the _acting set_, and the subsequent _peering_ did not remember that operation, and a node that did remember that operation later rejoined, its logs would record a different \(divergent\) history than the _authoritative history_ that was reconstructed in the _peering_ after the failure.
> Since the _divergent_ events were not recorded in other logs from that _acting set_, they were not acknowledged to the client, and there is no harm in discarding them \(so that all OSDs agree on the _authoritative history_\). But, we will have to instruct any OSD that stores data from a divergent update to delete the affected \(and now deemed to be apocryphal\) objects.
> 2. ask it for its _missing set_ \(object updates recorded in its PG log, but for which it does not have the new data\). This is the list of objects that must be fully replicated before we can accept writes.
> 6. at this point, the primary’s PG log contains an _authoritative history_ of the placement group, and the OSD now has sufficient information to bring any other OSD in the _acting set_ up to date.
> 7. if the primary’s _up\_thru_ value in the current OSD map is not greater than or equal to the first epoch in the _current interval_, send a request to the monitor to update it, and wait until receive an updated OSD map that reflects the change.
> 8. for each member of the current _acting set_:
> 1. send them log updates to bring their PG logs into agreement with my own \(_authoritative history_\) … which may involve deciding to delete divergent objects.
> 2. await acknowledgment that they have persisted the PG log entries.
> 9. at this point all OSDs in the _acting set_ agree on all of the meta\-data, and would \(in any future _peering_\) return identical accounts of all updates.
> 1. start accepting client write operations \(because we have unanimous agreement on the state of the objects into which those updates are being accepted\). Note, however, that if a client tries to write to an object it will be promoted to the front of the recovery queue, and the write willy be applied after it is fully replicated to the current _acting set_.
> 2. update the _last epoch started_ value in our local _PG info_, and instruct other _active set_ OSDs to do the same.
> 3. start pulling object data updates that other OSDs have, but I do not. We may need to query OSDs from additional _past intervals_ prior to _last epoch started_\(the last time _peering_ completed\) and following _last epoch clean_ \(the last epoch that recovery completed\) in order to find copies of all objects.
> 4. start pushing object data updates to other OSDs that do not yet have them.
> We push these updates from the primary \(rather than having the replicas pull them\) because this allows the primary to ensure that a replica has the current contents before sending it an update write. It also makes it possible for a single read \(from the primary\) to be used to write the data to multiple replicas. If each replica did its own pulls, the data might have to be read multiple times.
> 10. once all replicas store the all copies of all objects \(that existed prior to the start of this epoch\) we can update _last epoch clean_ in the _PG info_, and we can dismiss all of the_stray_ replicas, allowing them to delete their copies of objects for which they are no longer in the _acting set_.
> We could not dismiss the _strays_ prior to this because it was possible that one of those _strays_ might hold the sole surviving copy of an old object \(all of whose copies disappeared before they could be replicated on members of the current _acting set_\).

## State Model[¶](https://docs.ceph.com/en/pacific/dev/peering/ "Permalink to this headline")
