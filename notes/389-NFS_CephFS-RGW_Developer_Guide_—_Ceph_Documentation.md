# 389: NFS CephFS-RGW Developer Guide — Ceph Documentation

 # NFS CephFS\-RGW Developer Guide[¶](https://docs.ceph.com/en/pacific/dev/vstart-ganesha/ "Permalink to this headline")

CephFS exports are supported since Octopus and RGW exports are supported since Quincy.

## Configuring NFS Ganesha to export CephFS with vstart[¶](https://docs.ceph.com/en/pacific/dev/vstart-ganesha/ "Permalink to this headline")

1. Using `cephadm`
    > ```
    > $ MDS=1 MON=1 OSD=3 NFS=1 ../src/vstart.sh -n -d --cephadm
    > ```
    > 
    > 
    > 
    > 
    > This will deploy a single NFS Ganesha daemon using `vstart.sh`, where the daemon will listen on the default NFS Ganesha port. Also cephfs export is created.
2. Using test orchestrator
    > ```
    > $ MDS=1 MON=1 OSD=3 NFS=1 ../src/vstart.sh -n -d
    > ```
    > 
    > 
    > 
    > 
    > Environment variable `NFS` is the number of NFS Ganesha daemons to be deployed, each listening on a random port.
    > 
    > 
    > 
    > 
    > Note:
    > 
    > 
    > 
    > NFS Ganesha packages must be pre\-installed for this to work.

## Configuring NFS Ganesha to export RGW with vstart[¶](https://docs.ceph.com/en/pacific/dev/vstart-ganesha/ "Permalink to this headline")

1. Using `cephadm`
    > ```
    > $ MON=1 OSD=3 RGW=1 NFS=1 ../src/vstart.sh -n -d --cephadm
    > ```
    > 
    > 
    > 
    > 
    > This will deploy a single NFS Ganesha daemon using `vstart.sh`, where the daemon will listen on the default NFS Ganesha port. Also rgw export is created.
    > 
    > 
    > 
    > 
    > Note:
    > 
    > 
    > 
    > boto python module must be pre\-installed for this to work.
