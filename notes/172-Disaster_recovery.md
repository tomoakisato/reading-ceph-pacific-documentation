# 172: Disaster recovery

**クリップソース:** [172: Disaster recovery — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/disaster-recovery/)

# Disaster recovery[¶](https://docs.ceph.com/en/pacific/cephfs/disaster-recovery/ "Permalink to this headline")

## Metadata damage and repair[¶](https://docs.ceph.com/en/pacific/cephfs/disaster-recovery/ "Permalink to this headline")

ファイルシステムに不整合または欠落したメタデータがある場合、それは破損しているとみなされます。 ダメージについては、ヘルスメッセージや、実行中のMDSデーモンのアサーションから知ることができるかもしれません。

メタデータの損傷は、基盤となるRADOSレイヤーのデータ損失（例えば、PGのすべてのコピーを失う複数ディスク障害）、またはソフトウェアのバグのいずれかによって生じます。

CephFSには、破損したファイルシステムを復旧できる可能性のあるツールがいくつか含まれていますが、それらを安全に使用するには、CephFSの内部をしっかりと理解する必要があります。これらの危険性のある操作のドキュメントは別ページにあります：[高度な メタデータ修復ツール](https://docs.ceph.com/en/pacific/cephfs/disaster-recovery-experts/#disaster-recovery-experts)

## Data pool damage \(files affected by lost data PGs\)[¶](https://docs.ceph.com/en/pacific/cephfs/disaster-recovery/ "Permalink to this headline")

データプールでPGが失われた場合、ファイルシステムは正常に動作し続けますが、いくつかのファイルの一部が単に欠落します（読み取りはゼロを返します）。

データPGを失うと、多くのファイルに影響を与える可能性があります。 ファイルは多くのオブジェクトに分割されているため、特定の PG の損失によってどのファイルが影響を受けるかを特定するには、ファイルのサイズ内に存在する可能性のあるすべてのオブジェクト ID をフル スキャンする必要があります。この種のスキャンは、バックアップからの復元が必要なファイルを特定するのに有効な場合があります。

危険：このコマンドはメタデータを修復しないので、ファイルを復元するときは、破損したファイルを削除し、新しいinodeを持つために置き換える必要があります。 破損したファイルをそのまま上書きしないでください。

PGからオブジェクトが失われたことが分かっている場合、pg\_filesサブコマンドを使用して、その結果破損した可能性のあるファイルをスキャンします：

```
cephfs-data-scan pg_files <path> <pg id> [<pg id>...]
```

例えば、PG1.4と4.5のデータを失ってしまい、/home/bob以下のどのファイルが破損した可能性があるかを知りたい場合：

```
cephfs-data-scan pg_files /home/bob 1.4 4.5
```

出力は、破損した可能性のあるファイルのパスが1行に1つずつリストアップされます。

このコマンドは通常のCephFSクライアントとして動作し、ファイルシステム内のすべてのファイルを検索し、そのレイアウトを読み取るため、MDSが起動している必要があることに注意してください。
