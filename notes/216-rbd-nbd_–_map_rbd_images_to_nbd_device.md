# 216: rbd-nbd – map rbd images to nbd device

 # rbd\-nbd – map rbd images to nbd device[¶](https://docs.ceph.com/en/pacific/man/8/rbd-nbd/ "Permalink to this headline")

## Synopsis[¶](https://docs.ceph.com/en/pacific/man/8/rbd-nbd/ "Permalink to this headline")

**rbd\-nbd** \[\-c conf\] \[–read\-only\] \[–device _nbd device_\] \[–nbds\_max _limit_\] \[–max\_part _limit_\] \[–exclusive\] \[–encryption\-format _format_\] \[–encryption\-passphrase\-file _passphrase\-file_\] \[–io\-timeout _seconds_\] \[–reattach\-timeout _seconds_\] map _image\-spec_ | _snap\-spec_
**rbd\-nbd** unmap _nbd device_ | _image\-spec_ | _snap\-spec_
**rbd\-nbd** list\-mapped
**rbd\-nbd** attach –device _nbd device_ _image\-spec_ | _snap\-spec_
**rbd\-nbd** detach _nbd device_ | _image\-spec_ | _snap\-spec_

## Description[¶](https://docs.ceph.com/en/pacific/man/8/rbd-nbd/ "Permalink to this headline")

**rbd\-nbd** is a client for RADOS block device \(rbd\) images like rbd kernel module. It will map a rbd image to a nbd \(Network Block Device\) device, allowing access it as regular local block device.

## Options[¶](https://docs.ceph.com/en/pacific/man/8/rbd-nbd/ "Permalink to this headline")

`-c`` ceph.conf`[¶](https://docs.ceph.com/en/pacific/man/8/rbd-nbd/ "Permalink to this definition")Use _ceph.conf_ configuration file instead of the default`/etc/ceph/ceph.conf` to determine monitor addresses during startup.

`--read-only```[¶](https://docs.ceph.com/en/pacific/man/8/rbd-nbd/ "Permalink to this definition")Map read\-only.

`--nbds_max`` *limit*`[¶](https://docs.ceph.com/en/pacific/man/8/rbd-nbd/ "Permalink to this definition")Override the parameter nbds\_max of NBD kernel module when modprobe, used to limit the count of nbd device.

`--max_part`` *limit*`[¶](https://docs.ceph.com/en/pacific/man/8/rbd-nbd/ "Permalink to this definition")Override for module param max\_part.

`--exclusive```[¶](https://docs.ceph.com/en/pacific/man/8/rbd-nbd/ "Permalink to this definition")Forbid writes by other clients.

`--encryption-format```[¶](https://docs.ceph.com/en/pacific/man/8/rbd-nbd/ "Permalink to this definition")Image encryption format. Possible values: _luks1_, _luks2_

`--encryption-passphrase-file```[¶](https://docs.ceph.com/en/pacific/man/8/rbd-nbd/ "Permalink to this definition")Path of file containing a passphrase for unlocking image encryption.

`--io-timeout`` *seconds*`[¶](https://docs.ceph.com/en/pacific/man/8/rbd-nbd/ "Permalink to this definition")Override device timeout. Linux kernel will default to a 30 second request timeout. Allow the user to optionally specify an alternate timeout.

`--reattach-timeout`` *seconds*`[¶](https://docs.ceph.com/en/pacific/man/8/rbd-nbd/ "Permalink to this definition")Specify timeout for the kernel to wait for a new rbd\-nbd process is attached after the old process is detached. The default is 30 second.

## Image and snap specs[¶](https://docs.ceph.com/en/pacific/man/8/rbd-nbd/ "Permalink to this headline")

_image\-spec_ is \[_pool\-name_\]/_image\-name_
_snap\-spec_ is \[_pool\-name_\]/_image\-name_@_snap\-name_

The default for _pool\-name_ is “rbd”. If an image name contains a slash character \(‘/’\), _pool\-name_ is required.

## Availability[¶](https://docs.ceph.com/en/pacific/man/8/rbd-nbd/ "Permalink to this headline")

**rbd\-nbd** is part of Ceph, a massively scalable, open\-source, distributed storage system. Please refer to the Ceph documentation at [http://ceph.com/docs](https://docs.ceph.com/en/pacific/man/8/rbd-nbd/) for more information.

## See also[¶](https://docs.ceph.com/en/pacific/man/8/rbd-nbd/ "Permalink to this headline")

[rbd](https://docs.ceph.com/en/pacific/man/8/rbd-nbd/)\(8\)
