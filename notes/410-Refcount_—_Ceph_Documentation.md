# 410: Refcount — Ceph Documentation

 # Refcount[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/refcount/ "Permalink to this headline")

## Introduction[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/refcount/ "Permalink to this headline")

Dedupliation, as described in ../deduplication.rst, needs a way to maintain a target pool of deduplicated chunks with atomic ref refcounting. To that end, there exists an osd object class refcount responsible for using the object class machinery to maintain refcounts on deduped chunks and ultimately remove them as the refcount hits 0.

## Class Interface[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/refcount/ "Permalink to this headline")

See cls/refcount/cls\_refcount\_client\*

* cls\_refcount\_get
    Atomically increments the refcount with specified tag
    ```
    void cls_refcount_get(librados::ObjectWriteOperation& op, const string& tag, bool implicit_ref = false);
    ```
* cls\_refcount\_put
    Atomically decrements the refcount specified by passed tag
    ```
    void cls_refcount_put(librados::ObjectWriteOperation& op, const string& tag, bool implicit_ref = false);
    ```
* cls\_refcount\_Set
    Atomically sets the set of refcounts with passed list of tags
    ```
    void cls_refcount_set(librados::ObjectWriteOperation& op, list<string>& refs);
    ```
* cls\_refcount\_read
    Dumps the current set of ref tags for the object
    ```
    int cls_refcount_read(librados::IoCtx& io_ctx, string& oid, list<string> *refs, bool implicit_ref = false);
    ```
