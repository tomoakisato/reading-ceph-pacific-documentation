# 89: Adding/Removing OSDs

**クリップソース:** [89: Adding/Removing OSDs — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-osds/)

# Adding/Removing OSDs[¶](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-osds/ "Permalink to this headline")

クラスタが稼働している場合、ランタイムにOSDを追加したり、クラスタからOSDを削除することができます。

## Adding OSDs[¶](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-osds/ "Permalink to this headline")

クラスタを拡張したい場合、実行時にOSDを追加することがあります。Cephでは、OSDは通常、ホストマシン内の1つのストレージドライブに対する1つのCeph ceph\-osdデーモンです。ホストに複数のストレージドライブがある場合、各ドライブに対して1つのceph\-osdデーモンをマッピングすることができます。

一般的に、クラスタの容量が上限に達しているかどうかを確認するのがよいでしょう。クラスタが満杯に近い比率に達したら、1つまたは複数のOSDを追加してクラスタの容量を拡張する必要があります。

警告：OSDを追加する前に、クラスタをフルレシオにしないでください。クラスタがニアフルレシオに達した後に発生したOSD障害は、クラスタがフルレシオを超える原因となる場合があります。

### Deploy your Hardware[¶](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-osds/ "Permalink to this headline")

  新しいOSDを追加する際に新しいホストを追加する場合、OSDハードウェアの最小推奨事項の詳細については、「[Hardware Recommendations](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-osds/)」を参照してください。OSD ホストをクラスタに追加するには、まず、最新バージョンの Linux がインストールされていることと、ストレージドライブの初期準備がされていることを確認してください。 詳しくは[Filesystem Recommendations](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-osds/)をご覧ください。 

OSDホストをクラスタ内のラックに追加し、ネットワークに接続し、ネットワーク接続が可能であることを確認します。詳細については、「[Network Configuration Reference](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-osds/)」を参照してください。

### Install the Required Software[¶](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-osds/ "Permalink to this headline")

手動で展開されたクラスタの場合、Cephパッケージを手動でインストールする必要があります。詳細については、「[Installing Ceph \(Manual\)](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-osds/)」を参照してください。SSHは、パスワードなしの認証とroot権限を持つユーザに設定する必要があります。

### Adding an OSD \(Manual\)[¶](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-osds/ "Permalink to this headline")

この手順では、ceph\-osdデーモンを設定し、1つのドライブを使用するように設定し、OSDにデータを配布するようにクラスタを設定します。ホストに複数のドライブがある場合、この手順を繰り返して各ドライブ用のOSDを追加することができます。

OSDを追加するには、そのデータディレクトリを作成し、そのディレクトリにドライブをマウントし、OSDをクラスタに追加し、CRUSHマップに追加します。

OSDをCRUSHマップに追加する際、新しいOSDに与える重みを考慮してください。ハードディスクの容量は年間40%成長するので、新しいOSDホストはクラスタ内の古いホストより大きなハードディスクを持っているかもしれません（つまり、より大きな重みを持つかもしれません）。

ヒント:Cephはプール間で均一なハードウェアを好みます。異なるサイズのドライブを追加する場合、その重みを調整できます。ただし、最高のパフォーマンスを得るには、同じタイプ/サイズのドライブでCRUSH階層を検討します。

1. OSDを作成します。UUIDが与えられていない場合は、OSDの起動時に自動的に設定されます。次のコマンドはOSD番号を出力しますが、これは以降のステップで必要になります。

```
ceph osd create [{uuid} [{id}]]
```

オプションのパラメータ{id}が与えられると、それがOSD IDとして使用されます。この場合、その番号がすでに使用されていると、コマンドは失敗する可能性があることに注意してください。

警告：一般に、{id}を明示的に指定することは推奨されません。IDは配列として割り当てられ、エントリーをスキップすることで余分なメモリを消費します。これは、大きなギャップがあったり、クラスタが大きい場合に顕著になります。{id}が指定されない場合、利用可能な最小のものが使用されます。

1. 新しいOSDにデフォルトディレクトリを作成します。

```
ssh {new-osd-host}
sudo mkdir /var/lib/ceph/osd/ceph-{osd-number}
```

1. OSDがOSドライブ以外のドライブの場合、Cephで使用するために準備し、先ほど作成したディレクトリにマウントします。

```
ssh {new-osd-host}
sudo mkfs -t {fstype} /dev/{drive}
sudo mount -o user_xattr /dev/{hdd} /var/lib/ceph/osd/ceph-{osd-number}
```

1. OSDデータディレクトリを初期化します。

```
ssh {new-osd-host}
ceph-osd -i {osd-num} --mkfs --mkkey
```

ceph\-osdを実行する前に、このディレクトリを空にする必要があります。

1. OSD認証キーを登録します。パスのceph\-{osd\-num}のcephの値が$cluster\-$idになります。 クラスタ名がcephと異なる場合は、クラスタ名を代用すること：

```
ceph auth add osd.{osd-num} osd 'allow *' mon 'allow rwx' -i /var/lib/ceph/osd/ceph-{osd-num}/keyring
```

1. OSDをCRUSHマップに追加し、OSDがデータの受信を開始できるようにします。ceph osd crush addコマンドを使用すると、CRUSH階層に好きな場所にOSDを追加することができます。少なくとも1つのバケットを指定すると、コマンドはOSDを指定した最も具体的なバケットに配置し、そのバケットを指定した他のバケットの下に移動させます。

以下を実行します：

```
ceph osd crush add {id-or-name} {weight}  [{bucket-type}={bucket-name} ...]
```

また、CRUSHマップのデコンパイル、デバイスリストへのOSDの追加、バケットとしてのホストの追加（まだCRUSHマップにない場合）、ホストのアイテムとしてのデバイスの追加、重みの割り当て、リコンパイルしてセットも可能です。詳細は、「[Add/Move an OSD](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-osds/)」を参照してください。

### Replacing an OSD[¶](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-osds/ "Permalink to this headline")

ディスクに障害が発生した場合、または管理者が新しいバックエンドでOSDを再プロビジョンしたい場合、例えばFileStoreからBlueStoreに変更する場合、OSDを交換する必要があります。OSDの削除とは異なり、交換されたOSDのIDとCRUSHマップ・エントリーは、交換のためにOSDが破壊された後でもそのままにしておく必要があります。

1. OSDを破壊しても安全であることを確認する

```
while ! ceph osd safe-to-destroy osd.{id} ; do sleep 10 ; done
```

1. まずOSDを破壊する：

```
ceph osd destroy {id} --yes-i-really-mean-it
```

1. ディスクが以前他の目的で使用されていた場合、新しいOSDのためにディスクをザッピングする。新しいディスクの場合は必要ありません：

```
ceph-volume lvm zap /dev/sdX
```

1. 破壊されたOSD idを使用して、交換用のディスクを準備する：

```
ceph-volume lvm prepare --osd-id {id} --data /dev/sdX
```

1. そしてOSDを起動させる：

```
ceph-volume lvm activate {id} {fsid}
```

あるいは、準備と起動の代わりに、一回の呼び出しでデバイスを再作成する方法もあります：

```
ceph-volume lvm create --osd-id {id} --data /dev/sdX
```

### Starting the OSD[¶](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-osds/ "Permalink to this headline")

CephにOSDを追加すると、OSDは設定に含まれます。ただし、まだ実行されていません。OSDはdownでinです。データの受信を開始する前に、新しいOSDを起動する必要があります。管理ホストからservice cephを使用するか、そのホストマシンからOSDを起動することができます。

Ubuntu Trusty の場合は、Upstart を使用します：

```
sudo start ceph-osd id={osd-num}
```

他のすべてのディストロでは、systemd を使ってください。

```
sudo systemctl start ceph-osd@{osd-num}
```

OSDを起動したら、upでinです。

### Observe the Data Migration[¶](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-osds/ "Permalink to this headline")

新しいOSDをCRUSHマップに追加すると、CephはPGを新しいOSDに移行することでサーバーのリバランスを開始します。cephツールでこのプロセスを観察することができます。

```
ceph -w
```

移行が完了すると、PGの状態が、active\+cleanから、activeでいくつかのdegradedオブジェクト、最後にactive\+cleanに変化するのが確認できるはずです\(control\-cで終了\)。

## Removing OSDs \(Manual\)[¶](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-osds/ "Permalink to this headline")

クラスタのサイズを縮小したり、ハードウェアを交換したりする場合、実行中にOSDを削除することがあります。Cephでは、OSDは通常、ホストマシン内の1つのストレージドライブに対する1つのCeph ceph\-osdデーモンです。ホストに複数のストレージドライブがある場合、各ドライブに対して1つのceph\-osdデーモンを削除する必要がある場合があります。一般に、クラスタの容量が上限に達しているかどうかを確認することをお勧めします。OSDを削除するときに、クラスタがニアフルレシオになっていないことを確認します。

警告：OSDを取り外す際には、クラスタがフルレシオにならないように注意してください。OSDを取り外すと、クラスタがフルレシオに達するか、それを超える可能性があります。

### Take the OSD out of the Cluster[¶](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-osds/ "Permalink to this headline")

OSDを削除する前に、通常、OSDはupでinです。 Cephがリバランスを開始し、そのデータを他のOSDにコピーできるように、クラスタからOSDをoutする必要があります。

```
ceph osd out {osd-num}
```

### Observe the Data Migration[¶](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-osds/ "Permalink to this headline")

OSDをクラスタからoutすると、Cephは、取り外したOSDからPGを移行することによってクラスタのリバランスを開始します。このプロセスは、cephツールで観察できます。

```
ceph -w
```

PGの状態が、active\+cleanから、activeでいくつかのdegradedオブジェクト、最後にactive\+cleanに変化するのがわかると思います\(control\-cで終了\)。

注意:ホスト数が少ない「小さな」クラスタ\(例えばテストクラスタ\)では、OSDを取り外すと、いくつかのPGがactive\+remapped状態で立ち往生するCRUSHコーナーケースが発生することがあります。このような場合は、OSDをマークinする必要があります；

```
ceph osd in {osd-num}
```

初期状態に戻し、OSDをマークoutする代わりに、その重みを0に設定します：

```
ceph osd crush reweight osd.{osd-num} 0
```

その後、データ移行が終了するのを観察することができます。OSD をマークoutする場合と 0 に再ウエイトする場合の違いは、最初のケースでは OSD を含むバケットのウエイトは変更されないのに対し、2番目のケースではバケットのウエイトが更新される（そして OSD のウエイトが減少する）点です。reweightコマンドは、「小さな」クラスタの場合に有効な場合があります。

### Stopping the OSD[¶](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-osds/ "Permalink to this headline")

OSDをクラスタからoutした後、まだ動作している場合があります。つまり、OSDがupでoutしている可能性があります。OSDを構成から削除する前に、OSDを停止する必要があります。

```
ssh {osd-host}
sudo systemctl stop ceph-osd@{osd-num}
```

OSDを停止したら、downです。

### Removing the OSD[¶](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-osds/ "Permalink to this headline")

この手順では、クラスタマップからOSDを削除し、その認証キーを削除し、OSDマップからOSDを削除し、ceph.confファイルからOSDを削除します。ホストに複数のドライブがある場合、この手順を繰り返して各ドライブのOSDを削除する必要がある場合があります。

1. まずクラスタにOSDを忘れさせます。このステップでは、CRUSHマップからOSDを削除し、その認証キーを削除します。そして、OSDマップからも削除されます。purge サブコマンドは Luminous で導入されたことに注意してください、古いバージョンでは以下を参照してください。

```
ceph osd purge {id} --yes-i-really-mean-it
```

1. クラスタのceph.confファイルのマスターコピーを保存しているホストに移動します。

```
ssh {admin-host}
cd /etc/ceph
vim ceph.conf
```

1. ceph.confファイルからOSDエントリを\(存在する場合\)削除します。

```
[osd.1]
        host = {hostname}
```

1. クラスタのceph.confファイルのマスターコピーを保管しているホストから、更新されたceph.confファイルをクラスタ内の他のホストの/etc/cephディレクトリにコピーしてください。

CephクラスタがLuminousより古い場合、ceph osd purgeを使用する代わりに、このステップを手動で実行する必要があります。

1. OSDをCRUSHマップから削除し、データを受信しないようにします。また、CRUSHマップをデコンパイルし、デバイスリストからOSDを削除し、ホストバケットのアイテムとしてデバイスを削除するか、ホストバケットを削除し（CRUSHマップにあり、ホストを削除する予定の場合）、マップを再コンパイルして設定することも可能です。詳しくは、

```
ceph osd crush remove {name}
```

1. OSD認証キーを削除します。

```
ceph auth del osd.{osd-num}
```

パスのceph\-{osd\-num}に対するcephの値は、$cluster\-$idです。クラスタ名がcephと異なる場合は、代わりに自分のクラスタ名を使用します。

1. OSDを削除します。

```
ceph osd rm {osd-num}
#for example
ceph osd rm 1
```
