# 2: Intro to Ceph

**クリップソース:** [Intro to Ceph — Ceph Documentation](https://docs.ceph.com/en/pacific/start/intro/)

# Intro to Ceph¶

[Ceph Object Storage](https://docs.ceph.com/en/pacific/glossary/#term-Ceph-Object-Storage)や[Ceph Block Device](https://docs.ceph.com/en/pacific/glossary/#term-Ceph-Block-Device)のサービスを[クラウドプラット](https://docs.ceph.com/en/pacific/glossary/#term-Cloud-Platforms)フォームに提供する場合も、[Ceph File System](https://docs.ceph.com/en/pacific/glossary/#term-Ceph-File-System)を展開する場合も、Cephを別の目的で使用する場合も、すべての[Ceph Storage Cluster](https://docs.ceph.com/en/pacific/glossary/#term-Ceph-Storage-Cluster)の展開は、各[Cephノード](https://docs.ceph.com/en/pacific/glossary/#term-Ceph-Node)、ネットワーク、Ceph Storage Clusterの設定から始まります。

Ceph Storage Clusterには、少なくとも1つのCeph Monitor、Ceph Manager、およびCeph OSD \(Object Storage Daemon\)が必要です。

また、Ceph File Systemクライアントを実行する場合は、Ceph Metadata Serverが必要です。


![37f38700cd784da451becd6718695f086edd0fd2ab5f8e8daf686249096ce7ab.png](image/37f38700cd784da451becd6718695f086edd0fd2ab5f8e8daf686249096ce7ab.png)

* **Monitors**  
  Ceph Monitor (ceph-mon)は、モニタマップ、マネージャマップ、OSDマップ、MDSマップ、CRUSHマップなど、クラスタ状態のマップを維持します。 これらのマップは、Cephデーモンが相互に調整するために必要な重要なクラスタ状態です。モニターは、デーモンとクライアント間の認証を管理する役割も果たします。 冗長性と高可用性のために、通常は少なくとも3つのモニタが必要です。
* **Managers**  
  Ceph Managerデーモン(ceph-mgr)は、ランタイムメトリクスと、ストレージ使用率、現在のパフォーマンスメトリクス、システム負荷など、Cephクラスタの現在の状態を追跡する役割を果たします。また、Ceph Managerデーモンは、WebベースのCeph DashboardやREST APIなど、Cephクラスタの情報を管理・公開するPythonベースのモジュールをホストします。 高可用性のためには、通常、少なくとも2つのマネージャが必要です。
* **Ceph OSDs**  
  Ceph OSD (オブジェクトストレージデーモン、ceph-osd)は、データを保存し、データのレプリケーション、リカバリ、リバランシングを処理し、他のCeph OSDデーモンにハートビートを確認することで、Ceph MonitorやManagerにいくつかの監視情報を提供します。冗長性と高可用性のためには、通常、少なくとも3つのCeph OSDが必要です。
* **MDSs**  
  Ceph Metadata Server (MDS, ceph-mds)は、Ceph File Systemに代わってメタデータを保存します(つまり、Ceph Block DevicesとCeph Object StorageはMDSを使用しません)。Ceph Metadata Serverにより、POSIXファイルシステムのユーザは、Ceph Storage Clusterに大きな負担をかけることなく、基本的なコマンド(ls、findなど)を実行できます。

Cephは、データを論理ストレージプール内のオブジェクトとして保存します。Cephは[CRUSH](https://docs.ceph.com/en/pacific/glossary/#term-CRUSH)アルゴリズムを使用して、どの配置グループにオブジェクトを格納するかを計算し、さらにどのCeph OSDデーモンに配置グループを格納するかを計算します。 CRUSHアルゴリズムにより、Ceph Storage Clusterは動的にスケーリング、リバランス、およびリカバリを行うことができます。

### Recommendations

Cephを本番環境で使い始めるには、推奨ハードウェアと推奨オペレーティングシステムを確認する必要があります。
### Get Involved

Cephのコミュニティに参加することで、ヘルプを利用したり、ドキュメントやソースコード、バグを提供することができます。
