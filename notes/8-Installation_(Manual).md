# 8: Installation (Manual)

**クリップソース:** [8: Installation \(Manual\) — Ceph Documentation](https://docs.ceph.com/en/pacific/install/index_manual/)

# Installation \(Manual\)¶

## **Get Software[¶](https://docs.ceph.com/en/pacific/install/index_manual/#get-software "Permalink to this headline")**

Cephのソフトウェアを入手する方法はいくつかあります。

最も簡単で一般的な方法は、Advanced Package Tool \(APT\)やYellowdog Updater, Modified \(YUM\)などのパッケージ管理ツールで使用するリポジトリを追加して[パッケージを入手する](https://docs.ceph.com/en/pacific/install/get-packages)方法です。

また、Cephのリポジトリからコンパイル済みのパッケージを取得することもできます。

最後に、tarボールを取得したり、Cephのソースコードリポジトリをクローンして自分でCephをビルドすることもできます。

[9: Get Packages](9-Get_Packages.md) 

[10: Downloading a Ceph Release Tarball](10-Downloading_a_Ceph_Release_Tarball.md)

[11: Cloning the Ceph Source Code Repository](11-Cloning_the_Ceph_Source_Code_Repository.md) 

[12: Build Ceph](12-Build_Ceph_—_Ceph_Documentation.md)

## **Install Software[¶](https://docs.ceph.com/en/pacific/install/index_manual/#install-software "Permalink to this headline")**

Cephのソフトウェア\(または追加されたリポジトリ\)を入手したら、ソフトウェアのインストールは簡単です。

クラスタの各[Cephノード](https://docs.ceph.com/en/pacific/glossary/#term-Ceph-Node)にパッケージをインストールします。cephadmを使用してストレージクラスタ用のCephをインストールするか、パッケージ管理ツールを使用します。Ceph Object GatewayまたはQEMUをインストールする場合は、RHEL/CentOSおよびYumを使用するその他のディストリビューション用のYum Prioritiesをインストールする必要があります。

## **Upgrade Software[¶](https://docs.ceph.com/en/pacific/install/index_manual/#upgrade-software "Permalink to this headline")**

Cephの新しいバージョンが利用可能になると、新しい機能を利用するためにクラスタをアップグレードすることができます。クラスタをアップグレードする前に、アップグレード文書を読んでください。Cephのアップグレードでは、アップグレードの順序に従わなければならない場合があります。
