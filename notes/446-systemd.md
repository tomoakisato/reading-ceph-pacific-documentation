# 446: systemd

**クリップソース:** [446: systemd — Ceph Documentation](https://docs.ceph.com/en/pacific/ceph-volume/lvm/systemd/)

# systemd[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/systemd/ "Permalink to this headline")

起動時に、[LVMタグ](https://docs.ceph.com/en/pacific/glossary/#term-LVM-tags)を使用して論理ボリュームを識別し、一致するIDを見つけ、後で[OSD uuid](https://docs.ceph.com/en/pacific/glossary/#term-OSD-uuid)でそれが正しいものであることを確認します。

正しいボリュームを特定した後、OSDの宛先規則（下記）を使用してマウントを行います：

```
/var/lib/ceph/osd/<cluster name>-<osd id>
```

IDが0のOSDの例では、識別されたデバイスが下記にマウントされることを意味します：

```
/var/lib/ceph/osd/ceph-0
```

その処理が完了すると、OSDを起動するため下記の呼び出しが行われます：

```
systemctl start ceph-osd@0
```

このプロセスのsystemdの部分は、ceph\-volume lvm triggerサブコマンドによって処理され、systemdとstartupから来るメタデータを解析し、activationを進めるceph\-volume lvm activateにディスパッチすることだけを担当します。
