# 77: SHEC erasure code plugin

**クリップソース:** [77: SHEC erasure code plugin — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-shec/)

# SHEC erasure code plugin[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-shec/ "Permalink to this headline")

shecプラグインは、複数のSHECライブラリをカプセル化しています。これにより、cephはReed Solomonコードよりも効率的にデータを復元することができます。

## Create an SHEC profile[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-shec/ "Permalink to this headline")

新しいshec ECプロファイルを作成するには：

```
ceph osd erasure-code-profile set {name} 
     plugin=shec 
     [k={data-chunks}] 
     [m={coding-chunks}] 
     [c={durability-estimator}] 
     [crush-root={root}] 
     [crush-failure-domain={bucket-type}] 
     [crush-device-class={device-class}] 
     [directory={directory}] 
     [--force]
```

ここで：

**k={data\-chunks}**

Description

各オブジェクトはデータチャンク部分に分割され、それぞれが異なるOSDに格納される

Type

Integer

Required

No.

Default

4

**m={coding\-chunks}**

Description

各オブジェクトのcoding\-chunkを計算し、異なるOSDに保存する。コーディングチャンクの数は、必ずしもデータを失うことなくダウンできるOSDの数と同じではない

Type

Integer

Required

No.

Default

3

**c={durability\-estimator}**

Description

計算範囲内の各データチャンクを含む、各パリティチャンクの数。この数値は耐久性の推定値として使用される。例えば、c=2 の場合、2 つの OSD がデータを失うことなくダウンすることができる

Type

Integer

Required

No.

Default

2

**crush\-root={root}**

Description

CRUSHルールの最初のステップに使用されるCRUSHバケットの名前

Type

String

Required

No.

Default

default

**crush\-failure\-domain={bucket\-type}**

Description

同じ障害ドメインを持つバケットに、2つのチャンクが存在しないことを確認する。例えば、障害ドメインがhostの場合、2つのチャンクが同じhostに保存されることはない。step chooseleaf hostのようなCRUSHルールのステップを作成するために使用される

Type

String

Required

No.

Default

host

**crush\-device\-class={device\-class}**

Description

CRUSHマップのcrushデバイスクラス名を使って、特定のクラスのデバイス（例：ssdやhdd）に配置を制限する

Type

String

Required

No.

Default

**directory={directory}**

Description

ECプラグインの読み込み元となるディレクトリ名

Type

String

Required

No.

Default

/usr/lib/ceph/erasure\-code

**\-\-force**

Description

同名の既存プロファイルを上書きする

Type

String

Required

No.

## Brief description of SHEC’s layouts[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-shec/ "Permalink to this headline")

### Space Efficiency[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-shec/ "Permalink to this headline")

スペース効率とは、オブジェクト内のすべてのデータチャンクに対する割合で、k/\(k\+m\)で表されます。スペース効率を上げるには、kを大きくするか、mを小さくする必要があります。

SHEC\(4,3,2\)のスペース効率 =  4/\(4\+3\)  = 0.57 

SHEC\(5,3,2\) や SHEC\(4,2,2\) は SHEC\(4,3,2\)のスペース効率を向上させます。

### Durability[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-shec/ "Permalink to this headline")

SHECの第3パラメータ（=c）は耐久性推定量であり、データを失うことなくダウンできるOSDの数を近似するものです。

SHEC\(4,3,2\)の耐久性推定量 = 2 

### Recovery Efficiency[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-shec/ "Permalink to this headline")

回復効率の計算方法については本書で説明することはできませんが、少なくともcを増やさずにmを増やすことで回復効率の向上は実現できます。\(ただし、この場合、スペース効率が犠牲になることに注意が必要です\)。

SHEC\(4,2,2\)\-\>SHEC\(4,3,2\)：回復効率を向上させる

## Erasure code profile examples[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-shec/ "Permalink to this headline")

```
$ ceph osd erasure-code-profile set SHECprofile 
     plugin=shec 
     k=8 m=4 c=3 
     crush-failure-domain=host
$ ceph osd pool create shecpool erasure SHECprofile
```
