# 30: NFS Service

**クリップソース:** [30: NFS Service — Ceph Documentation](https://docs.ceph.com/en/pacific/cephadm/services/nfs/)

# NFS Service[¶](https://docs.ceph.com/en/pacific/cephadm/services/nfs/ "Permalink to this headline")

NFSv4プロトコルのみ対応しています。

NFSを管理する最も簡単な方法は、_ceph nfs cluster..._コマンドを使用することで、「 [CephFS & RGW Exports over NFS](https://docs.ceph.com/en/pacific/cephadm/services/nfs/)」を参照してください。 [313: CephFS & RGW Exports over NFS](313-CephFS_&_RGW_Exports_over_NFS.md)

この文書では、cephadmサービスを直接管理する方法を説明します。

これは、通常とは異なるNFS構成の場合にのみ必要です。 

## Deploying NFS ganesha[¶](https://docs.ceph.com/en/pacific/cephadm/services/nfs/ "Permalink to this headline")

CephadmはNFS Ganeshaデーモン\(またはデーモンのセット\)を展開します。 

NFSの設定はnfs\-ganeshaプールに保存され、エクスポートは_ceph nfs export..._コマンドとダッシュボードで管理されます。

NFS Ganeshaゲートウェイを展開するには、次のコマンドを実行します。

```
# ceph orch apply nfs *<svc_id>* [--port *<port>*] [--placement ...]
```

例えば、サービスIDがfooのNFSを、デフォルトのポート2049に、デフォルトの配置で1つのデーモンを配置する場合。

```
# ceph orch apply nfs foo
```

配置指定の詳細については、「[Daemon Placement](https://docs.ceph.com/en/pacific/cephadm/services/nfs/) 」を参照してください。

## Service Specification[¶](https://docs.ceph.com/en/pacific/cephadm/services/nfs/ "Permalink to this headline")

また、YAML仕様を使ってNFSサービスを適用することもできます。

```
service_type: nfs
service_id: mynfs
placement:
  hosts:
    - host1
    - host2
spec:
  port: 12345
```

この例では、ホスト1とホスト2において、（デフォルトの2049ではなく）12345という非デフォルトのポートでサーバーを稼働させます。

そして、以下のコマンドを実行することで仕様を適用することができます。

```
# ceph orch apply -i nfs.yaml
```

## High\-availability NFS[¶](https://docs.ceph.com/en/pacific/cephadm/services/nfs/ "Permalink to this headline")

既存のnfsサービスにingressサービスを展開すると、以下のようになります。

* NFSサーバーへのアクセスに使用できる、安定した仮想IP
* ホスト障害時のホスト間フェイルオーバー
* 複数のNFSゲートウェイへの負荷分散（ただし、これが必要になることはほとんどない）。

Ingress for NFSは、既存のNFSサービス（この例ではnfs.mynfs）に対して、以下のような仕様で導入することができます。

```
service_type: ingress
service_id: nfs.mynfs
placement:
  count: 2
spec:
  backend_service: nfs.mynfs
  frontend_port: 2049
  monitor_port: 9000
  virtual_ip: 10.0.0.123/24
```

A few notes:

* virtual\_ipには、上の例のようにCIDRのプレフィックス長を含める必要があります。 通常、仮想IPは、同じサブネット内に既存のIPを持つ、最初に確認されたネットワークインターフェースに設定されます。 virtual\_interface\_networksプロパティを指定して、他のネットワーク上のIPと照合することもできます。詳しくは、「 
* monitor\_portは、haproxyのロードステータスページにアクセスするために使用します。 ユーザーは、デフォルトではadminですが、仕様書のadminプロパティで変更することができます。 パスワードが指定されていない場合は、自動生成されたパスワードを使用して見つけることができます。

```
# ceph config-key get mgr/cephadm/ingress.*{svc_id}*/monitor_password
```

For example:

```
# ceph config-key get mgr/cephadm/ingress.nfs.myfoo/monitor_password
```

* バックエンドサービス（この例ではnfs.mynfs）には、同じホストに置かれている可能性のあるイングレスサービスとの衝突を避けるために、2049以外のポートプロパティを含める必要があります。

## Further Reading[¶](https://docs.ceph.com/en/pacific/cephadm/services/nfs/ "Permalink to this headline")

* CephFS: 
* MGR: 
