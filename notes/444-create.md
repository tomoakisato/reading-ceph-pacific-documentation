# 444: create

**クリップソース:** [444: create — Ceph Documentation](https://docs.ceph.com/en/pacific/ceph-volume/lvm/create/)

# create[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/create/ "Permalink to this headline")

このサブコマンドは、新しいosdをプロビジョニングする2段階のプロセス（最初にprepareを呼び出し、次にactivateを呼び出す）を1つにまとめたものです。prepareとactivateを好む理由は、新しいOSDをクラスタに徐々に導入し、大量のデータがリバランスされるのを避けるためです。

シングルコールのプロセスは、[prepare](https://docs.ceph.com/en/pacific/ceph-volume/lvm/prepare/#ceph-volume-lvm-prepare)と[activate](https://docs.ceph.com/en/pacific/ceph-volume/lvm/activate/#ceph-volume-lvm-activate)の内容を統一し、一度にすべてを行うことができる利便性を備えています。

完了後すぐにOSDが立ち上がること以外は、何も変わりません。

バッキングオブジェクトストアを指定することができます：

* [–filestore](https://docs.ceph.com/en/pacific/ceph-volume/lvm/prepare/#ceph-volume-lvm-prepare-filestore)
* [–bluestore](https://docs.ceph.com/en/pacific/ceph-volume/lvm/prepare/#ceph-volume-lvm-prepare-bluestore)

すべてのコマンドラインフラグおよびオプションは、ceph\-volume lvm prepareと同じです。詳細については[prepare](https://docs.ceph.com/en/pacific/ceph-volume/lvm/prepare/#ceph-volume-lvm-prepare)を参照してください。
