# 100: Memory Profiling

**クリップソース:** [100: Memory Profiling — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/troubleshooting/memory-profiling/)

# Memory Profiling[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/memory-profiling/ "Permalink to this headline")

Ceph MON、OSD、MDSは、tcmallocを使用してヒーププロファイルを生成できます。ヒーププロファイルを生成するには、google\-perftoolsがインストールされていることを確認します：

```
sudo apt-get install google-perftools
```

プロファイラーは、ログファイルディレクトリ（/var/log/ceph）に出力をダンプします。詳細は、「[Logging and Debugging](https://docs.ceph.com/en/pacific/rados/troubleshooting/memory-profiling/)」を参照してください。Google のパフォーマンスツールでプロファイラのログを表示するには、以下を実行します： 

```
google-pprof --text {path-to-daemon}  {log-path/filename}
```

For example:

```
$ ceph tell osd.0 heap start_profiler
$ ceph tell osd.0 heap dump
osd.0 tcmalloc heap stats:------------------------------------------------
MALLOC:        2632288 (    2.5 MiB) Bytes in use by application
MALLOC: +       499712 (    0.5 MiB) Bytes in page heap freelist
MALLOC: +       543800 (    0.5 MiB) Bytes in central cache freelist
MALLOC: +       327680 (    0.3 MiB) Bytes in transfer cache freelist
MALLOC: +      1239400 (    1.2 MiB) Bytes in thread cache freelists
MALLOC: +      1142936 (    1.1 MiB) Bytes in malloc metadata
MALLOC:   ------------
MALLOC: =      6385816 (    6.1 MiB) Actual memory used (physical + swap)
MALLOC: +            0 (    0.0 MiB) Bytes released to OS (aka unmapped)
MALLOC:   ------------
MALLOC: =      6385816 (    6.1 MiB) Virtual address space used
MALLOC:
MALLOC:            231              Spans in use
MALLOC:             56              Thread heaps in use
MALLOC:           8192              Tcmalloc page size
------------------------------------------------
Call ReleaseFreeMemory() to release freelist memory to the OS (via madvise()).
Bytes released to the OS take up virtual address space but no physical memory.
$ google-pprof --text 
               /usr/bin/ceph-osd  
               /var/log/ceph/ceph-osd.0.profile.0001.heap
 Total: 3.7 MB
 1.9  51.1%  51.1%      1.9  51.1% ceph::log::Log::create_entry
 1.8  47.3%  98.4%      1.8  47.3% std::string::_Rep::_S_create
 0.0   0.4%  98.9%      0.0   0.6% SimpleMessenger::add_accept_pipe
 0.0   0.4%  99.2%      0.0   0.6% decode_message
 ...
```

同じデーモンで別のヒープダンプを行うと、別のファイルが追加されます。以前のヒープダンプと比較することで、その間に何が成長したかを示すことができ、便利です。例えば：

```
$ google-pprof --text --base out/osd.0.profile.0001.heap 
      ceph-osd out/osd.0.profile.0003.heap
 Total: 0.2 MB
 0.1  50.3%  50.3%      0.1  50.3% ceph::log::Log::create_entry
 0.1  46.6%  96.8%      0.1  46.6% std::string::_Rep::_S_create
 0.0   0.9%  97.7%      0.0  26.1% ReplicatedPG::do_op
 0.0   0.8%  98.5%      0.0   0.8% __gnu_cxx::new_allocator::allocate
```

詳細は、[Google Heap Profiler](https://docs.ceph.com/en/pacific/rados/troubleshooting/memory-profiling/)を参照してください。

ヒーププロファイラをインストールしたら、クラスタを起動し、ヒーププロファイラの使用を開始します。ヒープ・プロファイラを実行時に有効または無効にしたり、継続的に実行するようにしたりすることができます。以下のコマンドライン使用法では、{daemon\-type}をmon、osd、mdsに置き換え、{daemon\-id}をOSD番号、MON ID、MDS IDに置き換えてください。

## Starting the Profiler[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/memory-profiling/ "Permalink to this headline")

ヒーププロファイラーを起動するには、次のように実行します：

```
ceph tell {daemon-type}.{daemon-id} heap start_profiler
```

For example:

```
ceph tell osd.1 heap start_profiler
```

または、CEPH\_HEAP\_PROFILER\_INIT=true変数が環境にある場合、デーモンの実行開始時にプロファイルを開始することができます。

## Printing Stats[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/memory-profiling/ "Permalink to this headline")

統計情報をプリントアウトするには、次のように実行します：

```
ceph  tell {daemon-type}.{daemon-id} heap stats
```

For example:

```
ceph tell osd.0 heap stats
```

注：統計情報を印刷する場合、プロファイラが起動している必要はなく、ヒープ割り当て情報をファイルにダンプすることもありません。

## Dumping Heap Information[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/memory-profiling/ "Permalink to this headline")

ヒープ情報をダンプするには、以下を実行します：

```
ceph tell {daemon-type}.{daemon-id} heap dump
```

For example:

```
ceph tell mds.a heap dump
```

注意：ヒープ情報のダンプは、プロファイラが動作しているときのみ有効です。

## Releasing Memory[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/memory-profiling/ "Permalink to this headline")

tcmallocが割り当てたが、Cephデーモン自身が使用していないメモリを解放するには、以下を実行します：

```
ceph tell {daemon-type}{daemon-id} heap release
```

For example:

```
ceph tell osd.2 heap release
```

## Stopping the Profiler[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/memory-profiling/ "Permalink to this headline")

ヒーププロファイラーを停止するには、次のように実行します：

```
ceph tell {daemon-type}.{daemon-id} heap stop_profiler
```

For example:

```
ceph tell osd.0 heap stop_profiler
```
