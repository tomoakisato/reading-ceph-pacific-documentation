# 145: Snapshot Scheduling Module

**クリップソース:** [145: Snapshot Scheduling Module — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/snap-schedule/)

# Snapshot Scheduling Module[¶](https://docs.ceph.com/en/pacific/cephfs/snap-schedule/ "Permalink to this headline")

このモジュールは、CephFSのスケジュールされたスナップショットを実装しています。スナップショットのスケジュールとリテンション・ポリシを追加、照会、削除するためのユーザインタフェースと、スナップショットを取得し、それに応じて既存のスナップショットを削除するスケジューラが用意されています。

## How to enable[¶](https://docs.ceph.com/en/pacific/cephfs/snap-schedule/ "Permalink to this headline")

snap\_scheduleモジュールを有効にするには：

```
ceph mgr module enable snap_schedule
```

## Usage[¶](https://docs.ceph.com/en/pacific/cephfs/snap-schedule/ "Permalink to this headline")

このモジュールは[CephFS Snapshots](https://docs.ceph.com/en/pacific/dev/cephfs-snapshots/)を使用するので、このドキュメントも考慮してください。

このモジュールのサブコマンドは、ceph fs snap\-schedule名前空間の下に存在します。引数は、位置指定引数またはキーワード引数として指定できます。キーワード引数に遭遇すると、それに続くすべての引数がキーワード引数であると見なされます。

スナップショットのスケジュールは、path、繰り返し間隔、開始時刻で識別されます。繰り返し間隔は、2つのスナップショット間の時間を定義します。これは、数値と、h\(our\)、d\(ay\)、w\(eek\)のうちの1つで指定します。例えば、繰り返し間隔12hは、12時間ごとに1つのスナップショットを指定します。開始時刻は時間文字列で指定します（時間経過の詳細は後述）。デフォルトでは、開始時刻は昨夜深夜です。したがって、デフォルトの開始時刻13:50に繰り返し間隔1hのスナップショットスケジュールを追加した場合、最初のスナップショットは14:00に取得されます。

リテンションSPECは、pathとリテンションSPEC自体によって識別されます。 リテンションSPECは、スペースで区切られた\<number\> と\<time period\>、または\<number\> \<time period\>の連結ペアで構成されます。 セマンティクスは、SPECにより、少なくとも\<time period\>離れた\<number\>個のスナップショットが保持されることを保証するというものです。 たとえば、7dは、少なくとも1日（ただし潜在的にはそれより長い）離れた7つのスナップショットを保持したいことを意味します。 次の\<time period\>が認識されます：h\(our\)、d\(ay\)、w\(eek\)、m\(onth\)、y\(ear\)、n。 nは特別な修飾子です。 10nは、タイミングに関係なく、最後の10個のスナップショットを保持することを意味します。

すべてのサブコマンドは、複数FSセットアップと、[FSボリューム&サブボリュー�](https://docs.ceph.com/en/pacific/cephfs/fs-volumes/)�を管理するセットアップでpathを指定するために、オプションでfs引数とsubvol引数を取ります。指定しない場合、fs のデフォルトは fs\_map にリストされている最初のファイル・システムで、subvol のデフォルトは何もありません。[FSボリューム&サブボリュー�](https://docs.ceph.com/en/pacific/cephfs/fs-volumes/)�を使用する場合、fs引数はボリュームと同じです。

タイムスタンプを渡す場合（add、remove、activate、deactivate サブコマンドの start 引数）、常に ISO 形式 %Y\-%m\-%dT%H:%M:%S が使用され ます。python3.7 以降が使われているか、[https://github.com/movermeyer/backports.datetime\_fromisoformat](https://github.com/movermeyer/backports.datetime_fromisoformat) がインストールされている場合、python の datetime.fromisoformat でパースされる ISO タイムスタンプはすべて有効です。

サブコマンドが指定されていない場合、概要が出力されます：

```
#> ceph fs snap-schedule
no valid command found; 8 closest matches:
fs snap-schedule status [<path>] [<subvol>] [<fs>] [<format>]
fs snap-schedule list <path> [<subvol>] [--recursive] [<fs>] [<format>]
fs snap-schedule add <path> <snap_schedule> [<start>] [<fs>] [<subvol>]
fs snap-schedule remove <path> [<repeat>] [<start>] [<subvol>] [<fs>]
fs snap-schedule retention add <path> <retention_spec_or_period> [<retention_count>] [<fs>] [<subvol>]
fs snap-schedule retention remove <path> <retention_spec_or_period> [<retention_count>] [<fs>] [<subvol>]
fs snap-schedule activate <path> [<repeat>] [<start>] [<subvol>] [<fs>]
fs snap-schedule deactivate <path> [<repeat>] [<start>] [<subvol>] [<fs>]
Error EINVAL: invalid command
```

### Inspect snapshot schedules[¶](https://docs.ceph.com/en/pacific/cephfs/snap-schedule/ "Permalink to this headline")

このモジュールには、既存のスケジュールを検査するための2つのサブコマンド（listとstatus）があります。オプションのformat引数を介してplain出力とjson出力を提供します。 デフォルトはplainです。 listサブコマンドは、パス上のすべてのスケジュールを1行形式で一覧表示します。 指定されたディレクトリと含まれているすべてのディレクトリ内のすべてのスケジュールを一覧表示するための再帰的な引数を提供します。 statusサブコマンドは、パスで使用可能なすべてのスケジュールとリテンションSPECを出力します。

例：

```
ceph fs snap-schedule status /
ceph fs snap-schedule status /foo/bar --format=json
ceph fs snap-schedule list /
ceph fs snap-schedule list / --recursive=true # list all schedules in the tree
```

### Add and remove schedules[¶](https://docs.ceph.com/en/pacific/cephfs/snap-schedule/ "Permalink to this headline")

addおよびremoveサブコマンドは、それぞれスナップショットのスケジュールを追加および削除するコマンドです。どちらも少なくともpath引数を必要としますが、addはさらにUSAGEセクションで説明されているように、schedule引数を必要とします。

1つのpathに複数のスケジュールを追加することができます。2つのスケジュールは、繰り返しの間隔と開始時刻が異なる場合、異なるものとみなされます。

pathに複数スケジュールが設定されている場合、removeは正確な繰り返し間隔と開始時刻を指定することで個々のスケジュールを削除することができ、pathだけを指定した場合はすべてのスケジュールを削除することができます。

例：

```
ceph fs snap-schedule add / 1h
ceph fs snap-schedule add / 1h 11:55
ceph fs snap-schedule add / 2h 11:55
ceph fs snap-schedule remove / 1h 11:55 # removes one single schedule
ceph fs snap-schedule remove / 1h # removes all schedules with --repeat=1h
ceph fs snap-schedule remove / # removes all schedules on path /
```

### Add and remove retention policies[¶](https://docs.ceph.com/en/pacific/cephfs/snap-schedule/ "Permalink to this headline")

retention add および retention remove サブコマンドを使用すると、リテンションポリシを管理できます。1つのpathは厳密に1つのリテンションポリシを持ちます。ただし、複雑なリテンションポリシを指定するために、1つのポリシに複数のcount\-time period のペアを含めることができます。リテンションポリシは、ceph fs snap\-schedule retention add \<path\> \<time period\> \<count\>およびceph fs snap\-schedule retention add \<path\> \<count\-time period\>\[count\-time period\] 形式で個別または一括で追加/削除できます。

例：

```
ceph fs snap-schedule retention add / h 24 # keep 24 snapshots at least an hour apart
ceph fs snap-schedule retention add / d 7 # and 7 snapshots at least a day apart
ceph fs snap-schedule retention remove / h 24 # remove retention for 24 hourlies
ceph fs snap-schedule retention add / 24h4w # add 24 hourly and 4 weekly to retention
ceph fs snap-schedule retention remove / 7d4w # remove 7 daily and 4 weekly, leaves 24 hourly
```

### Active and inactive schedules[¶](https://docs.ceph.com/en/pacific/cephfs/snap-schedule/ "Permalink to this headline")

スナップショットスケジュールは、ディレクトリツリーにまだ存在しないpathに追加することができます。同様に、pathは、スナップショットスケジュールに影響を与えることなく削除することができます。スナップショットがスケジュールされるときにディレクトリが存在しない場合、スケジュールは非アクティブに設定され、再びアクティブになるまでスケジュールから除外されます。スケジュールを手動で非アクティブに設定することで、スケジュールされたスナップショットの作成を一時停止することができます。このモジュールには、この目的のためにactivateとdeactivateのサブコマンドが用意されています。

例：

```
ceph fs snap-schedule activate / # activate all schedules on the root directory
ceph fs snap-schedule deactivate / 1d # deactivates daily snapshots on the root directory
```

### Limitations[¶](https://docs.ceph.com/en/pacific/cephfs/snap-schedule/ "Permalink to this headline")

スナップショットはpythonのタイマーを使ってスケジュールされます。通常の場合、スケジュールとして1時間を指定すると、1時間間隔のスナップショットがかなり正確に行われます。しかし、mgr デーモンの負荷が高い場合、タイマーのスレッドがすぐにスケジュールされず、スナップショットが少し遅れてしまうことがあります。この場合、次のスナップショットは、前のスナップショット遅延がなかったかのようにスケジュールされます。

ファイルシステム内のスナップショットの全体数をある程度制限するために、このモジュールはディレクトリごとに最大50のスナップショットのみを保持します。リテンションポリシが50以上のスナップショットを保持する結果になった場合、リテンションリストは最新の50スナップショットに短縮されます。

### Data storage[¶](https://docs.ceph.com/en/pacific/cephfs/snap-schedule/ "Permalink to this headline")

スナップショットのスケジュールデータは、cephfsメタデータプールのradosオブジェクトに格納されます。実行時には、すべてのデータは、シリアライズされてradosオブジェクトとして格納されるsqliteデータベースに格納されます。
