# 73: Erasure code profiles

**クリップソース:** [73: Erasure code profiles — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-profile/)

# Erasure code profiles[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-profile/ "Permalink to this headline")

ECはプロファイルで定義され、ECプールと関連するCRUSHルールを作成する際に使用されます。

デフォルトのECプロファイル\(Cephクラスタの初期化時に作成される\)は、データを2つの同じサイズのチャンクに分割し、同じサイズのパリティチャンクを2つ持ちます。これは、k=2およびm=2のプロファイルとして記述され、情報が4つのOSD \(k\+m == 4\)に分散され、そのうちの2つが失われる可能性があることを意味します。

rawストレージの必要量を増やさずに冗長性を向上させるには、新しいプロファイルを作成することができます。例えば、k=10、m=4のプロファイルは、14（k\+m=14）台のOSDにオブジェクトを分散させることで、4（m=4）台のOSDの損失を維持することができます。オブジェクトはまず10個のチャンクに分割され（オブジェクトが10MBの場合、各チャンクは1MB）、回復のために4個のコーディングチャンクが計算されます（各コーディングチャンクはデータチャンクと同じサイズ、すなわち1MBです）。rawスペースのオーバーヘッドはわずか40％で、4つのOSDが同時に壊れてもオブジェクトは失われません。

* [Jerasure erasure code plugin](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-profile/)
* [ISA erasure code plugin](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-profile/)
* [Locally repairable erasure code plugin](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-profile/)
* [SHEC erasure code plugin](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-profile/)
* [CLAY code plugin](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-profile/)

## osd erasure\-code\-profile set[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-profile/ "Permalink to this headline")

ECプロファイルを新規に作成するには：

```
ceph osd erasure-code-profile set {name} 
     [{directory=directory}] 
     [{plugin=plugin}] 
     [{stripe_unit=stripe_unit}] 
     [{key=value} ...] 
     [--force]
```

ここで：

**{directory=directory}**

Description

ECプラグインの読み込み元となるディレクトリ名

Type

String

Required

No.

Default

/usr/lib/ceph/erasure\-code

**{plugin=plugin}**

Description

コーディングチャンクを計算し、欠落したチャンクを回復するのに使用するECプラグイン。詳しくは利用可能なプラグインのリストを参照

Type

String

Required

No.

Default

jerasure

**{stripe\_unit=stripe\_unit}**

Description

データチャンクに含まれるデータ量を、ストライプごとに指定する。例えば、2つのデータチャンクを持つプロファイルでstripe\_unit=4Kの場合、チャンク0に0\-4K、チャンク1に4K\-8K、そして再びチャンク0に8K\-12Kの範囲を置くことになる。最高のパフォーマンスを得るには、これは4Kの倍数であるべき。デフォルト値は、プール作成時のモニタ設定オプションosd\_pool\_erasure\_code\_stripe\_unitから取得される。 このプロファイルを使用するプールのstripe\_widthは、データチャンクの数にこのstripe\_unitを乗じたものになる。

Type

String

Required

No.

**{key=value}**

Description

キーバリューのペアのセマンティックは、ECプラグインによって定義される

Type

String

Required

No.

**\-\-force**

Description

同名の既存のプロファイルを上書きし、4Kアライメントでないstripe\_unitを設定できるようにする

Type

String

Required

No.

## osd erasure\-code\-profile rm[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-profile/ "Permalink to this headline")

ECプロファイルを削除するには：

```
ceph osd erasure-code-profile rm {name}
```

プロファイルがプールから参照されている場合、削除は失敗します。

## osd erasure\-code\-profile get[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-profile/ "Permalink to this headline")

ECプロファイルを表示するには：

```
ceph osd erasure-code-profile get {name}
```

## osd erasure\-code\-profile ls[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-profile/ "Permalink to this headline")

すべてのECプロファイルの名前をリスト表示するには：

```
ceph osd erasure-code-profile ls
```
