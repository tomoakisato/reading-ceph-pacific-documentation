# 93: Control Commands

**クリップソース:** [93: Control Commands — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/operations/control/)

# Control Commands[¶](https://docs.ceph.com/en/pacific/rados/operations/control/ "Permalink to this headline")

## Monitor Commands[¶](https://docs.ceph.com/en/pacific/rados/operations/control/ "Permalink to this headline")

モニタコマンドは、cephユーティリティを使用して発行します。

```
ceph [-m monhost] {command}
```

コマンドは通常（常にではありませんが）、次のような形式です。

```
ceph {subsystem} {command}
```

## System Commands[¶](https://docs.ceph.com/en/pacific/rados/operations/control/ "Permalink to this headline")

現在のクラスタの状態を表示します。

```
ceph -s
ceph status
```

クラスタの状態および主要なイベントの実行中のサマリーが表示されます。

```
ceph -w
```

どのモニタが参加しているか、どのモニタがリーダーであるかなど、モニタクォーラムが表示されます。

```
ceph mon stat
ceph quorum_status
```

クォーラムに参加しているかどうかなど、1つのモニタの状態を問い合わせるには、以下を実行します。

```
ceph tell mon.[id] mon_status
```

ここで、\[id\]の値は、例えばceph \-sから決定することができます。

## Authentication Subsystem[¶](https://docs.ceph.com/en/pacific/rados/operations/control/ "Permalink to this headline")

OSDのキーリングを追加するには、以下を実行します。

```
ceph auth add {osd} {--in-file|-i} {path-to-osd-keyring}
```

クラスタのキーとそのCAPSを一覧表示するには、以下を実行します。

```
ceph auth ls
```

## Placement Group Subsystem[¶](https://docs.ceph.com/en/pacific/rados/operations/control/ "Permalink to this headline")

すべての配置グループ（PG）の統計情報を表示するには、次のように実行します。

```
ceph pg dump [--format {format}]
```

有効なフォーマットは、plain（デフォルト）、jsonjson\-pretty、xml、xml\-prettyです。監視ツールなどを実装する場合は、json形式を使用するのがベストです。JSONのパースは人間向けのplainよりも決定論的であり、レイアウトもリリースごとに大きく変わることはありません。 jqユーティリティは、JSONの出力からデータを抽出する際に非常に有効です。

指定した状態で止まっているすべての配置グループの統計情報を表示するには、次のように実行します。

```
ceph pg dump_stuck inactive|unclean|stale|undersized|degraded [--format {format}] [-t|--threshold {seconds}]
```

\-\-format は plain \(デフォルト\)、json、json\-pretty、xml、xml\-pretty のいずれかです。

\-\-threshold は "stuck" が何秒かを定義します \(デフォルト: 300\)

**Inactive** PGは、最新のデータを持つOSDが戻ってくるのを待っているため、読み取りや書き込みの処理を行うことができません。

**Unclean** PGには、必要な回数だけレプリケートされていないオブジェクトが含まれています。リカバリーする必要があります。

**Stale** PGは未知の状態です \- それらをホストするOSDは、しばらくモニタクラスタに報告していません（mon\_osd\_report\_timeoutによって構成されます）。

「lost」オブジェクトを削除したり、以前のバージョンに戻したり、作成されたばかりの場合は削除したりすることができます。

```
ceph pg {pgid} mark_unfound_lost revert|delete
```

## OSD Subsystem[¶](https://docs.ceph.com/en/pacific/rados/operations/control/ "Permalink to this headline")

OSDサブシステムのステータスを問い合わせます。

```
ceph osd stat
```

最新のOSDマップのコピーをファイルに書き込みます。[osdmaptool](https://docs.ceph.com/en/pacific/rados/operations/control/) を参照のこと。

```
ceph osd getmap -o file
```

直近のOSDマップからCRUSHマップのコピーをファイルに書き込みます。

```
ceph osd getcrushmap -o file
```

上記は、機能的には以下と同等です。

```
ceph osd getmap -o /tmp/osdmap
osdmaptool /tmp/osdmap --export-crush file
```

OSDマップをダンプします。fで有効なフォーマットは、plain、json、json\-pretty、xml、xml\-prettyです。もし\-\-formatオプションが与えられない場合、OSDマップはプレーンテキストとしてダンプされます。 上記のように、JSON形式はツールやスクリプトなどの自動化に最適です。

```
ceph osd dump [--format {format}]
```

OSD マップを、重みと状態を含む OSD 毎に 1 行のツリーとしてダンプします。

```
ceph osd tree [--format {format}]
```

特定のオブジェクトがシステムのどこに保存されているか、または保存されるかを調べます。

```
ceph osd map <pool-name> <object-name>
```

指定された場所に、指定されたid/name/weightを持つ新しいアイテム（OSD）を追加または移動します。

```
ceph osd crush set {id} {weight} [{loc1} [{loc2} ...]]
```

CRUSH マップから既存のアイテム（OSD）を削除します。

```
ceph osd crush remove {name}
```

CRUSH マップから既存のバケットを削除します。

```
ceph osd crush remove {bucket-name}
```

既存のバケットを階層内のある位置から別の位置に移動させます。

```
ceph osd crush move {id} {loc1} [{loc2} ...]
```

{name}で指定された項目の重さを{weight}に設定します。

```
ceph osd crush reweight {name} {weight}
```

OSDをlostとマークします。これは、永久的なデータの損失につながる可能性があります。注意して使用してください。

```
ceph osd lost {id} [--yes-i-really-mean-it]
```

新しいOSDを作成します。UUIDが与えられていない場合、OSDの起動時に自動的に設定されます。

```
ceph osd create [{uuid}]
```

指定されたOSD（複数可）を削除します。

```
ceph osd rm [{id}...]
```

OSDマップの現在のmax\_osdパラメータを問い合わせます。

```
ceph osd getmaxosd
```

与えられたCRUSHマップをインポートします。

```
ceph osd setcrushmap -i file
```

OSDマップのmax\_osdパラメータを設定します。これは現在デフォルトで10000に設定されているので、ほとんどの管理者はこれを調整する必要はないでしょう。

```
ceph osd setmaxosd
```

OSD {osd\-num}をマークdownします。

```
ceph osd down {osd-num}
```

OSD {osd\-num}をディストリビューションからoutする（データを割り当てない）とマークします。

```
ceph osd out {osd-num}
```

OSD {osd\-num}をディストリビューションにinする（データを割り当てる）とマークします。

```
ceph osd in {osd-num}
```

OSDマップの一時停止フラグを設定または解除します。設定した場合、IOリクエストはどのOSDにも送られません。一時停止解除によってフラグをクリアすると、保留中のリクエストが再送信されます。

```
ceph osd pause
ceph osd unpause
```

{osd\-num}のオーバーライドウェイト\(reweight\)を{weight}に設定します。同じウェイトを持つ2つのOSDは、ほぼ同じ数のI/O要求を受け取り、ほぼ同じ量のデータを保存します。ceph osd reweightはOSDにオーバーライドウェイトを設定します。この値は0から1の範囲で、CRUSHにこのドライブ上に存在するはずのデータの再配置（1\-weight）を強制します。これは、CRUSHマップのOSDより上のバケットに割り当てられたウェイトを変更するものではなく、通常のCRUSH分布が全く正しく機能していない場合の修正措置となるものです。例えば、OSDの1つが90%で他が50%の場合、このウェイトを減らして補正することができます。

```
ceph osd reweight {osd-num} {weight}
```

過度に使用されているOSDのオーバーライドウェイトを減らすことによって、OSDの使用量をバランスさせる。 これらのオーバーライドウェイトのデフォルト値は1.00000で、お互いに相対的なものであり、絶対的なものではないことに注意してください。バケットの絶対的な容量を TiB で反映する CRUSH ウエイトと区別することが重要です。 デフォルトでは、このコマンドは、平均使用率の \+ または \- 20% を持つ OSD のオーバーライドの重みを調整しますが、閾値を含めると、代わりにそのパーセンテージが使用されます。

```
ceph osd reweight-by-utilization [threshold [max_change [max_osds]]] [--no-increasing]
```

OSDの再ウエイトが変更されるステップを制限するには、max\_changeを指定します（デフォルトは0.05）。 これらのパラメータを増やすと、一度に移動するデータが増えるため、クライアント操作に大きな影響を与える可能性がありますが、OSDの使用率の平準化を速めることができます。

特定の呼び出しによって影響を受けるPGとOSDの数を決定するには、実行する前にテストできます。

```
ceph osd test-reweight-by-utilization [threshold [max_change max_osds]] [--no-increasing]
```

いずれかのコマンドに \-\-no\-increasing を追加すると、現在 \< 1.00000 であるオーバーライドウェイトを増加させないようにします。 これは、fullまたはnearfull OSD の救済を急いでいるときや、いくつかの OSD を退避させたり、ゆっくりと稼働させたりするときに便利です。

Luminous以前のクライアントがないNautilus（またはLuminousとMimicの新しいリビジョン）を利用するデプロイメントでは、代わりにceph\-mgrのバランサーモジュールを有効にすることをお勧めします。

ブロックリストへのIPアドレスの追加/削除を行います。アドレスを追加するときに、ブロックリストに登録する時間を秒単位で指定できます。指定しない場合は、デフォルトで1時間になります。ブロックリストに登録されたアドレスは、どのOSDにも接続できないようになります。ブロックリストは、最も頻繁に、遅れているメタデータサーバーがOSD上のデータに悪い変更を加えることを防ぐために使用されます。

ブロックリストは通常自動的にメンテナンスされ、手動での介入を必要としないはずなので、これらのコマンドはほとんど障害テストにのみ有用です。

```
ceph osd blocklist add ADDRESS[:source_port] [TIME]
ceph osd blocklist rm ADDRESS[:source_port]
```

プールのスナップショットを作成／削除します。

```
ceph osd pool mksnap {pool-name} {snap-name}
ceph osd pool rmsnap {pool-name} {snap-name}
```

ストレージプールを作成／削除／名称変更します。

```
ceph osd pool create {pool-name} [pg_num [pgp_num]]
ceph osd pool delete {pool-name} [{pool-name} --yes-i-really-really-mean-it]
ceph osd pool rename {old-name} {new-name}
```

プールの設定を変更します。

```
ceph osd pool set {pool-name} {field} {value}
```

有効なフィールドは以下の通りです。

* size: プール内のデータのコピー数を設定します
* pg\_num: PG数
* pgp\_num: 配置計算時の有効PG数
* crush\_rule: マッピング配置のルール番号

プール設定の値を取得します。

```
ceph osd pool get {pool-name} {field}
```

有効なフィールドは以下の通りです。

* pg\_num: PG数
* pgp\_num: 配置計算時の有効PG数

OSD {osd\-num}にスクラブコマンドを送信する。すべてのOSDにコマンドを送信するには、\*を使用する。

```
ceph osd scrub {osd-num}
```

OSD.Nに修復コマンドを送信します。すべてのOSDにコマンドを送信するには、\*を使用します。

```
ceph osd repair N
```

OSD.Nに対して、BYTES\_PER\_WRITEの書き込み要求でTOTAL\_DATA\_BYTESを書き込み、単純なスループットベンチマークを実行します。デフォルトでは、このテストは4MB刻みで合計1GBを書き込みます。このベンチマークは非破壊で、既存のライブOSDデータを上書きすることはありませんが、OSDに同時にアクセスしているクライアントのパフォーマンスに一時的に影響を与える可能性があります。

```
ceph tell osd.N bench [TOTAL_DATA_BYTES] [BYTES_PER_WRITE]
```

ベンチマーク実行の間にOSDのキャッシュをクリアするには、「cache drop」コマンドを使用します。

```
ceph tell osd.N cache drop
```

OSDのキャッシュの統計情報を取得するには、「cache status」コマンドを使用します。

```
ceph tell osd.N cache status
```

## MDS Subsystem[¶](https://docs.ceph.com/en/pacific/rados/operations/control/ "Permalink to this headline")

実行中のmdsの設定パラメータを変更します。

```
ceph tell mds.{mds-id} config set {setting} {value}
```

Example:

```
ceph tell mds.0 config set debug_ms 1
```

デバッグメッセージを有効にします。

```
ceph mds stat
```

すべてのメタデータサーバーのステータスを表示します。

```
ceph mds fail 
```

アクティブなMDSをfailedとしてマークし、スタンバイが存在する場合はフェイルオーバーをトリガーします。

Todo：cephmdsサブコマンドにドキュメントがありません：set、dump、getmap、stop、setmap

## Mon Subsystem[¶](https://docs.ceph.com/en/pacific/rados/operations/control/ "Permalink to this headline")

モニタの統計情報を表示します。

```
ceph mon stat

e2: 3 mons at {a=127.0.0.1:40000/0,b=127.0.0.1:40001/0,c=127.0.0.1:40002/0}, election epoch 6, quorum 0,1,2 a,b,c
```

末尾のクォーラムリストには、現在のクォーラムの一部であるモニタノードがリストアップされます。

こちらもよりダイレクトにご利用いただけます。

```
ceph quorum_status -f json-pretty
```

```
{
    "election_epoch": 6,
    "quorum": [
        0,
        1,
        2
    ],
    "quorum_names": [
        "a",
        "b",
        "c"
    ],
    "quorum_leader_name": "a",
    "monmap": {
        "epoch": 2,
        "fsid": "ba807e74-b64f-4b72-b43f-597dfe60ddbc",
        "modified": "2016-12-26 14:42:09.288066",
        "created": "2016-12-26 14:42:03.573585",
        "features": {
            "persistent": [
                "kraken"
            ],
            "optional": []
        },
        "mons": [
            {
                "rank": 0,
                "name": "a",
                "addr": "127.0.0.1:40000/0",
                "public_addr": "127.0.0.1:40000/0"
            },
            {
                "rank": 1,
                "name": "b",
                "addr": "127.0.0.1:40001/0",
                "public_addr": "127.0.0.1:40001/0"
            },
            {
                "rank": 2,
                "name": "c",
                "addr": "127.0.0.1:40002/0",
                "public_addr": "127.0.0.1:40002/0"
            }
        ]
    }
}

```

上記は定足数に達するまでブロックされます。

モニタ1台だけの状態の場合：

```
ceph tell mon.[name] mon_status
```

ここで、\[name\]の値はceph quorum\_statusから取得することができます。サンプル出力：

```
{
    "name": "b",
    "rank": 1,
    "state": "peon",
    "election_epoch": 6,
    "quorum": [
        0,
        1,
        2
    ],
    "features": {
        "required_con": "9025616074522624",
        "required_mon": [
            "kraken"
        ],
        "quorum_con": "1152921504336314367",
        "quorum_mon": [
            "kraken"
        ]
    },
    "outside_quorum": [],
    "extra_probe_peers": [],
    "sync_provider": [],
    "monmap": {
        "epoch": 2,
        "fsid": "ba807e74-b64f-4b72-b43f-597dfe60ddbc",
        "modified": "2016-12-26 14:42:09.288066",
        "created": "2016-12-26 14:42:03.573585",
        "features": {
            "persistent": [
                "kraken"
            ],
            "optional": []
        },
        "mons": [
            {
                "rank": 0,
                "name": "a",
                "addr": "127.0.0.1:40000/0",
                "public_addr": "127.0.0.1:40000/0"
            },
            {
                "rank": 1,
                "name": "b",
                "addr": "127.0.0.1:40001/0",
                "public_addr": "127.0.0.1:40001/0"
            },
            {
                "rank": 2,
                "name": "c",
                "addr": "127.0.0.1:40002/0",
                "public_addr": "127.0.0.1:40002/0"
            }
        ]
    }
}

```

以下のモニタ状態をダンプしたもの：

```
ceph mon dump

dumped monmap epoch 2
epoch 2
fsid ba807e74-b64f-4b72-b43f-597dfe60ddbc
last_changed 2016-12-26 14:42:09.288066
created 2016-12-26 14:42:03.573585
0: 127.0.0.1:40000/0 mon.a
1: 127.0.0.1:40001/0 mon.b
2: 127.0.0.1:40002/0 mon.c

```
