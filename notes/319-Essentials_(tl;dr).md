# 319: Essentials (tl;dr)

 # Essentials \(tl;dr\)[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/ "Permalink to this headline")

This chapter presents essential information that every Ceph developer needs to know.

## Leads[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/ "Permalink to this headline")

The Ceph project is led by Sage Weil. In addition, each major project component has its own lead. The following table shows all the leads and their nicks on [GitHub](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/):

|Scope |Lead |GitHub nick |
|---------|----------------|------------|
|Ceph |Sage Weil |liewegas |
|RADOS |Neha Ojha |neha\-ojha |
|RGW |Yehuda Sadeh |yehudasa |
|RGW |Matt Benjamin |mattbenjamin |
|RBD |Jason Dillaman |dillaman |
|CephFS |Patrick Donnelly |batrick |
|Dashboard |Lenz Grimmer |LenzGr |
|MON |Joao Luis |jecluis |
|Build/Ops |Ken Dreyer |ktdreyer |
|Docs |Zac Dover |zdover23 |

The Ceph\-specific acronyms in the table are explained in[Architecture](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/).

## History[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/ "Permalink to this headline")

See the [History chapter of the Wikipedia article](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/).

## Licensing[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/ "Permalink to this headline")

Ceph is free software.

Unless stated otherwise, the Ceph source code is distributed under the terms of the LGPL2.1 or LGPL3.0. For full details, see the file[COPYING](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/) in the top\-level directory of the source\-code tree.

## Source code repositories[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/ "Permalink to this headline")

The source code of Ceph lives on [GitHub](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/) in a number of repositories below the [Ceph “organization”](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/).

A working knowledge of [git](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/) is essential to make a meaningful contribution to the project as a developer.

Although the [Ceph “organization”](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/) includes several software repositories, this document covers only one: [https://github.com/ceph/ceph](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/).

## Redmine issue tracker[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/ "Permalink to this headline")

Although [GitHub](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/) is used for code, Ceph\-related issues \(Bugs, Features, Backports, Documentation, etc.\) are tracked at [http://tracker.ceph.com](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/), which is powered by [Redmine](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/).

The tracker has a Ceph project with a number of subprojects loosely corresponding to the various architectural components \(see[Architecture](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/)\).

Mere [registration](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/) in the tracker automatically grants permissions sufficient to open new issues and comment on existing ones.

To report a bug or propose a new feature, [jump to the Ceph project](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/) and click on [New issue](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/).

## Mailing lists[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/ "Permalink to this headline")

### Ceph Development Mailing List[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/ "Permalink to this headline")

The `dev@ceph.io` list is for discussion about the development of Ceph, its interoperability with other technology, and the operations of the project itself.

The email discussion list for Ceph development is open to all. Subscribe by sending a message to `dev-request@ceph.io` with the following line in the body of the message:

```
subscribe ceph-devel
```

### Ceph Client Patch Review Mailing List[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/ "Permalink to this headline")

The `ceph-devel@vger.kernel.org` list is for discussion and patch review for the Linux kernel Ceph client component. Note that this list used to be an all\-encompassing list for developers. When searching the archives, remember that this list contains the generic devel\-ceph archives before mid\-2018.

Subscribe to the list covering the Linux kernel Ceph client component by sending a message to `majordomo@vger.kernel.org` with the following line in the body of the message:

```
subscribe ceph-devel
```

### Other Ceph Mailing Lists[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/ "Permalink to this headline")

There are also [other Ceph\-related mailing lists](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/).

## IRC[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/ "Permalink to this headline")

In addition to mailing lists, the Ceph community also communicates in real time using [Internet Relay Chat](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/).

See `https://ceph.com/irc/` for how to set up your IRC client and a list of channels.

## Submitting patches[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/ "Permalink to this headline")

The canonical instructions for submitting patches are contained in the file [CONTRIBUTING.rst](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/) in the top\-level directory of the source\-code tree. There may be some overlap between this guide and that file.

All newcomers are encouraged to read that file carefully.

## Building from source[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/ "Permalink to this headline")

See instructions at [Build Ceph](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/).

## Using ccache to speed up local builds[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/ "Permalink to this headline")

[ccache](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/) can make the process of rebuilding the ceph source tree faster.

Before you use [ccache](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/) to speed up your rebuilds of the ceph source tree, make sure that your source tree is clean and will produce no build failures. When you have a clean source tree, you can confidently use [ccache](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/), secure in the knowledge that you’re not using a dirty tree.

Old build artifacts can cause build failures. You might introduce these artifacts unknowingly when switching from one branch to another. If you see build errors when you attempt a local build, follow the procedure below to clean your source tree.

### Cleaning the Source Tree[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/ "Permalink to this headline")

```
$ make clean
```

Note:

The following commands will remove everything in the source tree that isn’t tracked by git. Make sure to back up your log files and configuration options before running these commands.

```
$ git clean -fdx; git submodule foreach git clean -fdx
```

### Building Ceph with ccache[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/ "Permalink to this headline")

`ccache` is available as a package in most distros. To build ceph with ccache, run the following command.

```
$ cmake -DWITH_CCACHE=ON ..
```

### Using ccache to Speed Up Build Times[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/ "Permalink to this headline")

`ccache` can be used for speeding up all builds of the system. For more details, refer to the [run modes](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/) section of the ccache manual. The default settings of `ccache` can be displayed with the `ccache -s` command.

Note:

We recommend overriding the `max_size`. The default is 10G. Use a larger value, like 25G. Refer to the [configuration](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/) section of the ccache manual for more information.

To further increase the cache hit rate and reduce compile times in a development environment, set the version information and build timestamps to fixed values. This makes it unnecessary to rebuild the binaries that contain this information.

This can be achieved by adding the following settings to the `ccache`configuration file `ccache.conf`:

```
sloppiness = time_macros
run_second_cpp = true
```

Now, set the environment variable `SOURCE_DATE_EPOCH` to a fixed value \(a UNIX timestamp\) and set `ENABLE_GIT_VERSION` to `OFF` when running`cmake`:

```
$ export SOURCE_DATE_EPOCH=946684800
$ cmake -DWITH_CCACHE=ON -DENABLE_GIT_VERSION=OFF ..
```

Note:

Binaries produced with these build options are not suitable for production or debugging purposes, as they do not contain the correct build time and git version information.

## Development\-mode cluster[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/ "Permalink to this headline")

See [Developer Guide \(Quick\)](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/).

## Kubernetes/Rook development cluster[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/ "Permalink to this headline")

See [Hacking on Ceph in Kubernetes with Rook](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/)

## Backporting[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/ "Permalink to this headline")

All bugfixes should be merged to the `master` branch before being backported. To flag a bugfix for backporting, make sure it has a[tracker issue](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/) associated with it and set the `Backport` field to a comma\-separated list of previous releases \(e.g. “hammer,jewel”\) that you think need the backport. The rest \(including the actual backporting\) will be taken care of by the[Stable Releases and Backports](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/) team.

## Guidance for use of cluster log[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/ "Permalink to this headline")

If your patches emit messages to the Ceph cluster log, please consult this: [Use of the cluster log](https://docs.ceph.com/en/pacific/dev/developer_guide/essentials/).
