# 95: Troubleshooting Monitors

**クリップソース:** [95: Troubleshooting Monitors — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-mon/)

# Troubleshooting Monitors[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-mon/ "Permalink to this headline")

クラスタがモニタ関連のトラブルに見舞われると、パニックになりがちですが、それには正当な理由があります。1台以上のモニタを失ったとしても、大多数が稼働し、クォーラムを形成している限り、必ずしもクラスタがダウンしたことを意味するわけではありません。状況がどの程度悪いかにかかわらず、まず最初にすべきことは、落ち着いて、一息ついて、以下のトラブルシューティングのステップを踏むことです。

## Initial Troubleshooting[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-mon/ "Permalink to this headline")

**Are the monitors running?**

まず、モニタ（mon）デーモンプロセス（ceph\-mon）が稼働していることを確認する必要があります。 Cephの管理者がmonの起動を忘れたり、アップグレード後に再起動し忘れたりすることがよくあることに驚くでしょう。恥ずかしいことではありませんが、問題を探すために数時間を無駄にしないようにしましょう。Kraken以降のリリースを実行する場合、マネージャデーモン\(ceph\-mgr\)が実行されていることも確認します。

**Are you able to reach to the mon nodes?**

あまりないことですが、monノードやTCPポートへのアクセスをブロックするiptablesルールが存在することがあります。これらは、以前のストレステストやルールの開発で残ったものかもしれません。サーバに SSH してみて、成功したら telnet や nc などでモニタのポート \(tcp/3300 と tcp/6789\) に接続してみてください。

**Does ceph \-s run and obtain a reply from the cluster?**

答えが「Yes」であれば、クラスタは稼働しています。 モニタがステータス要求に応答するのは、クォーラムが形成されている場合のみであることは、当然のこととして理解しておいてください。 また、少なくとも1つのmgrデーモンが実行中であることが報告されていることを確認してください。

クラスタからの応答がなく、障害メッセージも表示されずにceph \-sがハングする場合、モニタが完全にダウンしているか、ほんの一部しか起動していない可能性があります \- 過半数のクォーラムを形成するには不十分な割合です。 このチェックでは任意のモニタに接続します。まれに、コマンドに \-m mymon1 などと追加して、特定のモニタに順番にバインドすることが有効な場合があります。

**What if ceph \-s doesn’t come back?**

これまでの手順をすべて行っていない場合は、戻って行ってください。

クォーラムが形成されているかどうかにかかわらず、各モニタに個別に連絡してステータスを尋ねることができます。これは、ceph tell mon.ID mon\_status\(IDはモニタの識別子\)を使用して実現することができます。これはクラスタの各モニタに対して実行する必要があります。[Understanding mon\_status](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-mon/) セクションでは、このコマンドの出力を解釈する方法について説明します。

## Using the monitor’s admin socket[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-mon/ "Permalink to this headline")

adminソケットは、Unixのソケットファイルを使用して、指定されたデーモンと直接対話することができます。このファイルは、モニタのrunディレクトリにあります。デフォルトでは、adminソケットは/var/run/ceph/ceph\-mon.ID.asokに保存されますが、デフォルトディレクトリをオーバーライドした場合は、他の場所にある可能性があります。このディレクトリにない場合、別のパスや実行ディレクトリがないか、ceph.confを確認してください。

```
ceph-conf --name mon.ID --show-config-value admin_socket
```

adminソケットは、モニタデーモンが動作している間のみ利用可能であることに留意してください。モニタが正しくシャットダウンされると、adminソケットは削除されます。しかし、モニタが動作していないのにadminソケットが残っている場合、モニタが不適切にシャットダウンされた可能性があります。いずれにせよ、モニタが動作していない場合、adminソケットを使用することはできず、cephはError111:Connection Refusedを返す可能性があります。

adminソケットへのアクセスは、対象となるデーモン上でceph tellを実行することで簡単に行うことができます。例えば：

```
ceph tell mon.<id> mon_status
```

これは、/var/run/cephの下のどこかにある.asokで終わるファイルである「adminソケット」を介して、実行中のMONデーモン\<id\>にコマンドのヘルプを渡します。そのファイルへのフルパスがわかれば、自分でこれを実行することもできます。

```
ceph --admin-daemon <full_path_to_asok_file> <command>
```

cephツールのコマンドとしてhelpを使用すると、adminソケットから使用できるサポートされているコマンドが表示されます。config get、config show、mon stat、quorum\_statusはモニタのトラブルシューティングの際に役立つので、ぜひ見てみてください。

## Understanding mon\_status[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-mon/ "Permalink to this headline")

mon\_status は、常にadminソケット経由で取得できます。このコマンドは、quorum\_status で得られるのと同じ出力を含む、モニタに関する多数の情報を出力します。

ceph tell mon.c mon\_statusの出力例として、次のようなものがあります。

```
{ "name": "c",
  "rank": 2,
  "state": "peon",
  "election_epoch": 38,
  "quorum": [
        1,
        2],
  "outside_quorum": [],
  "extra_probe_peers": [],
  "sync_provider": [],
  "monmap": { "epoch": 3,
      "fsid": "5c4e9d53-e2e1-478a-8061-f543f8be4cf8",
      "modified": "2013-10-30 04:12:01.945629",
      "created": "2013-10-29 14:14:41.914786",
      "mons": [
            { "rank": 0,
              "name": "a",
              "addr": "127.0.0.1:6789/0"},
            { "rank": 1,
              "name": "b",
              "addr": "127.0.0.1:6790/0"},
            { "rank": 2,
              "name": "c",
              "addr": "127.0.0.1:6795/0"}]}}
```

いくつかのことは明らかです。monmapには3つのモニタ（a、b、c）がいて、クォーラムは2つのモニタだけで形成されており、cはpeonとしてクォーラムに入っています。

**クォーラムから外れているモニターはどれですか？**

答えは、aでしょう。

**どうしてですか？**

クォーラムセットを見てみましょう。このセットには2つのモニタがあります。1と2です。これらはモニタ名ではありません。現在のmonmapで設定されているモニタのランクです。ランク0のモニタがありません。monmapによると、それはmon.aになります。

**ところで、ランクはどのように設定されるのでしょうか？**

ランクはモニタを追加または削除するたびに（再）計算され、IP:PORTの組み合わせが多いほどランクが低くなるという単純なルールに従います。この場合、127.0.0.1:6789が残りのすべてのIP:PORTの組み合わせより低いことを考えると、mon.aはランク0になります。

## Most Common Monitor Issues[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-mon/ "Permalink to this headline")

### Have Quorum but at least one Monitor is down[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-mon/ "Permalink to this headline")

このような場合、実行しているCephのバージョンによっては、次のような表示が出るはずです。

```
$ ceph health detail
[snip]
mon.a (rank 0) addr 127.0.0.1:6789/0 is down (out of quorum)
```

**トラブルシューティングの方法は？**

まず、mon.aが起動していることを確認します。

次に、他のmonノードからmon.aのノードに接続できることを確認します。TCPポートも確認します。すべてのノードで iptables と nf\_conntrack を確認し、接続をドロップ/拒否していないことを確認します。

この最初のトラブルシューティングで問題が解決されない場合は、さらに深く掘り下げる必要があります。

まず、「[Using the monitor’s admin socket](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-mon/)」および「[Understanding mon\_status](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-mon/)」で説明するように、問題のあるモニタのmon\_statusをadminソケットで確認します。

モニタがクォーラムから外れている場合、その状態は、プローブ、選出、同期のいずれかである必要があります。 それがリーダーまたはpeonのいずれかである場合、モニタはクォーラムにあると信じますが、残りのクラスタはそうではないと確信しています。 または、モニタのトラブルシューティング中にクォーラムに入った可能性があるため、確認のためにもう一度ceph \-sを確認してください。 モニタがまだクォーラムに入っていない場合は続行します。

**状態がプローブの場合どうする？**

これは、モニタがまだ他のモニタを捜していることを意味します。モニタを起動するたびに、モニタはこの状態にしばらくとどまり、monmapで指定された残りのモニタとの接続を試みます。モニタがこの状態で過ごす時間は様々です。例えば、シングルモニタクラスタ（実稼働環境では決して行わない）の場合、モニタはほぼ瞬時にプロービング状態を通過します。マルチモニタクラスタでは、モニタはクォーラムを形成するのに十分なモニタを見つけるまでこの状態に留まります。これは、3台のモニタのうち2台がダウンした場合、残りの1台のモニタは他のモニタを起動するまで無期限にこの状態に留まることを意味します。

クォーラムがある場合、起動デーモンは他のモニタに到達できる限り、他のモニタを素早く見つけることができるはずです。mon\_status はモニタが知っているmonmapを出力します：他のモニタの位置が現実と一致しているかどうかをチェックします。もし一致しない場合は、「[Recovering a Monitor’s Broken monmap](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-mon/)」にジャンプします。もし一致する場合は、モニタノード間のクロックスキューに関連している可能性があるので、「[Clock Skews](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-mon/)」を最初に参照してください。

**状態が選出の場合どうする？**

これは、モニタが選挙の最中であることを意味します。最近のCephのリリースでは、通常、選挙はすぐに完了しますが、時には、いわゆる選挙嵐でモニタが立ち往生することがあります。この場合、モニタノード間でクロックスキューが発生している可能性があります。すべてのクロックが正しく同期している場合は、メーリングリストやトラッカーで検索してください。これは持続する状態ではなく、\(本当に\)古いバグを除けば、クロックスキュー以外になぜこのようなことが起こるのか、明白な理由はありません。 最悪の場合、もし十分な数の残存monがあるならば、調査する間、問題のあるmonを停止してください。

**状態が同期の場合どうする？**

これは、クォーラムに参加するために、モニタがクラスタの残りの部分に追いついていることを意味します。同期にかかる時間は、モニタストアのサイズ、つまりクラスタのサイズと状態の関数であり、大規模なクラスタやデグレードしたクラスタでは、時間がかかることがあります。

同期から選出、そしてまた同期へとモニタがジャンプするようであれば、問題があります。クラスタの状態が進みすぎて（つまり、新しいマップが生成されて）、同期プロセスが追いつかなくなっている可能性があります。これは初期（Cuttlefish）にはよくあることでしたが、それ以降、同期処理はリファクタリングされ、この動きを回避するように強化されています。もし、それ以降のバージョンでこのような現象が発生した場合は、バグトラッカーでお知らせください。また、ログをいくつか持参してください（ [Preparing your logs](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-mon/)参照）。

**状態がリーダーかpeonの場合どうする？**

このようなことは起こってはなりません。 しかし、もしそうなら、それはおそらくクロックスキューと大いに関係があります \- [Clock Skews](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-mon/)を参照してください。もしあなたがクロックスキューに悩まされていないなら、ログを準備し\([Preparing your logs](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-mon/)を参照\)、コミュニティに連絡を取ってみてください。

### Recovering a Monitor’s Broken monmap[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-mon/ "Permalink to this headline")

モニタの数にもよりますが、通常、monmapはこのように表示されます。

```
epoch 3
fsid 5c4e9d53-e2e1-478a-8061-f543f8be4cf8
last_changed 2013-10-30 04:12:01.945629
created 2013-10-29 14:14:41.914786
0: 127.0.0.1:6789/0 mon.a
1: 127.0.0.1:6790/0 mon.b
2: 127.0.0.1:6795/0 mon.c
```

しかし、これはあなたが持っているものとは違うかもしれません。例えば、初期のCuttlefishのいくつかのバージョンでは、monmapが無効化されるバグがありました。 完全にゼロで埋め尽くされてしまうのです。つまり、monmaptool でさえ理解することができないのです。また、ベンダーの TAC と戦っている間に、ノードが何ヶ月もダウンしていた場合など、ひどく古い monmap を持つモニタになってしまう可能性もあります。 ceph\-monデーモンは生存するモニタを見つけられないかもしれません\(例えば、mon.cがダウンしたとします。新しいモニタmon.dを追加し、mon.aを削除し、新しいモニタmon.eを追加してmon.bを削除すると、mon.cが知っているものと全く異なるmonmapになってしまうでしょう\)。

このような場合、2つの解決策が考えられます。

**モニタを破棄して再展開する**

この方法は、そのモニタが保持している情報を失わないこと、他のモニタがあり、それらが正常に動作しており、新しいモニタが残りのモニタから同期できることを確認した場合のみ取る必要があります。モニタを破壊すると、そのコンテンツのコピーが他にない場合、データの損失につながる可能性があることに留意してください。

**モニタにmonmapを注入する**

通常、最も安全な道です。残りのモニタからmonmapを取得し、破損/紛失したmonmapのあるモニタに注入する必要があります。

以上が基本的な手順です。

1. クォーラムが形成されているか？もしそうなら、クォーラムからmonmapを取り出す

```
$ ceph mon getmap -o /tmp/monmap
```

1. クォーラムがない？他のモニタから直接monmapを取得する（monmapを取得するモニタのIDがID\-FOOで、停止していることが前提です）

```
$ ceph-mon -i ID-FOO --extract-monmap /tmp/monmap
```

1. monmapを注入するモニタを停止する
2. monmapを注入する

```
$ ceph-mon -i ID --inject-monmap /tmp/monmap
```

1. モニタを起動する

monmapを注入する機能は強力ですが、使い方を誤ると、モニタが保持している最新の既存のmonmapを上書きしてしまうので、モニタに大混乱をもたらす可能性があることに留意してください。

### Clock Skews[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-mon/ "Permalink to this headline")

PAXOSコンセンサス アルゴリズムは厳密な時間調整を必要とするため、クォーラムの各mon間のクロック スキューによってモニタ動作に深刻な影響が及ぶ可能性があります。スキューは、明らかな原因のない奇妙な動作になることがあります。このような問題を回避するためには、モニタノード上でクロック同期ツールを実行する必要があります。 Chronyまたはレガシーのntpdです。 iburstオプションと複数のピアでモニタノードを構成することを確認してください。

* Each other
* Internal NTP servers
* Multiple external, public pool servers

また、クラスタ内のすべてのノードが内部および外部のサーバーと同期する必要があり、さらにmonとも同期する必要があります。 NTPサーバーはベアメタルで動作させる必要があります。VM仮想化クロックは安定した時間管理には適していません。 詳しくは https://www.ntp.org をご覧ください。 あなたの組織には、すでに高品質の内部NTPサーバーがあるかもしれません。NTPサーバーアプライアンスのソースは以下の通りです。

* Microsemi \(formerly Symmetricom\) 
* EndRun 
* Netburner 

クロックスキューの最大許容値はどのくらいですか？

デフォルトでは、モニタは0.05秒（50ms）までのクロックのドリフトを許容します。

**クロックスキューの最大許容値を増やすことはできますか？**

許容される最大のクロックスキューは mon\-clock\-drift\-allowed オプションで設定可能であり、可能ですが、確実に避けるべきです。クロックスキューのあるモニタは誤動作を起こしやすいためクロックスキューメカニズムがあります。私たち開発者は、モニタが手に負えなくなる前にユーザーに警告するのは現在のデフォルト値で十分だと考えています。この値を変更すると、モニタの安定性とクラスタ全体の健全性に予期せぬ影響を与える可能性があります。

**クロックスキューはどうすれば分かりますか？**

モニタは、クラスタステータスHEALTH\_WARNで警告します。

```
mon.c addr 10.10.0.1:6789/0 clock skew 0.08235s > max 0.05s (latency 0.0045s)
```

mon.cにはクロックスキューが発生しているフラグが立っているのです。

Luminous 以降のリリースでは cephtime\-sync\-status コマンドを発行して状態を確認することができます。 lead monは通常、数値的に最も低い IP アドレスを持つものであることに注意してください。 他のmonが報告するオフセットは、外部参照元ではなく、先頭のmonを基準としています。

**クロックスキューがある場合、どうすればよいですか？**

時計を同期させます。NTPクライアントを実行するとよいでしょう。すでにNTPクライアントを使用していて、このような問題が発生した場合は、ネットワークから離れた場所にあるNTPサーバーを使用していないか確認し、ネットワーク上に独自のNTPサーバーをホストすることを検討してください。 この最後のオプションは、モニタクロックのスキューの問題を減らす傾向があります。

### Client Can’t Connect or Mount[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-mon/ "Permalink to this headline")

IPテーブルを確認してください。OSのインストールユーティリティの中には、iptablesにREJECTルールを追加するものがあります。このルールは、sshを除くすべてのクライアントがホストに接続しようとするのを拒否します。モニタホストのIPテーブルにそのようなREJECTルールがある場合、別ノードから接続するクライアントはタイムアウトエラーでマウントに失敗します。Cephデーモンに接続しようとするクライアントを拒否するiptablesルールに対処する必要があります。 例えば、以下のようなルールに適切に対処する必要があります。

```
REJECT all -- anywhere anywhere reject-with icmp-host-prohibited
```

また、クライアントがCephモニタ\(デフォルトではポート6789\)およびCeph OSD\(デフォルトでは6800～7300\)に関連するポートにアクセスできるように、CephホストのIPテーブルにルールを追加する必要がある場合もあります。たとえば、以下のようになります。

```
iptables -A INPUT -m multiport -p tcp -s {ip-address}/{netmask} --dports 6789,6800:7300 -j ACCEPT
```

## Monitor Store Failures[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-mon/ "Permalink to this headline")

### Symptoms of store corruption[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-mon/ "Permalink to this headline")

Cephモニタは、クラスタマップをLevelDBなどのキー/バリューストアに格納します。キー/バリューストアの破損によりモニタが失敗した場合、モニタログに以下のエラーメッセージが表示されることがあります。

```
Corruption: error in middle of record
```

or:

```
Corruption: 1 missing files; e.g.: /var/lib/ceph/mon/mon.foo/store.db/1234567.ldb
```

### Recovery using healthy monitor\(s\)[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-mon/ "Permalink to this headline")

もし生存者がいれば、いつでも破損したものを新しいものと交換することができます。起動後、新しい参加者は健全なピアと同期し、完全に同期されると、クライアントにサービスを提供できるようになります。

### Recovery using OSDs[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-mon/ "Permalink to this headline")

しかし、すべてのモニタが同時に故障したらどうでしょうか？Cephクラスタには少なくとも3台（できれば5台）のモニタを導入することが推奨されているため、同時に障害が発生する可能性は稀です。しかし、ディスク/fsの設定が不適切なデータセンターで計画外の停電が発生すると、基盤となるファイルシステムに障害が発生し、その結果、すべてのモニタが停止する可能性があります。この場合、OSDに格納されている情報を使ってモニタストアを復旧させることができます。

```
ms=/root/mon-store
mkdir $ms

# collect the cluster map from stopped OSDs
for host in $hosts; do
  rsync -avz $ms/. user@$host:$ms.remote
  rm -rf $ms
  ssh user@$host <<EOF
    for osd in /var/lib/ceph/osd/ceph-*; do
      ceph-objectstore-tool --data-path $osd --no-mon-config --op update-mon-db --mon-store-path $ms.remote
    done
EOF
  rsync -avz user@$host:$ms.remote/. $ms
done

# rebuild the monitor store from the collected map, if the cluster does not
# use cephx authentication, we can skip the following steps to update the
# keyring with the caps, and there is no need to pass the "--keyring" option.
# i.e. just use "ceph-monstore-tool $ms rebuild" instead
ceph-authtool /path/to/admin.keyring -n mon. 
  --cap mon 'allow *'
ceph-authtool /path/to/admin.keyring -n client.admin 
  --cap mon 'allow *' --cap osd 'allow *' --cap mds 'allow *'
# add one or more ceph-mgr's key to the keyring. in this case, an encoded key
# for mgr.x is added, you can find the encoded key in
# /etc/ceph/${cluster}.${mgr_name}.keyring on the machine where ceph-mgr is
# deployed
ceph-authtool /path/to/admin.keyring --add-key 'AQDN8kBe9PLWARAAZwxXMr+n85SBYbSlLcZnMA==' -n mgr.x 
  --cap mon 'allow profile mgr' --cap osd 'allow *' --cap mds 'allow *'
# if your monitors' ids are not single characters like 'a', 'b', 'c', please
# specify them in the command line by passing them as arguments of the "--mon-ids"
# option. if you are not sure, please check your ceph.conf to see if there is any
# sections named like '[mon.foo]'. don't pass the "--mon-ids" option, if you are
# using DNS SRV for looking up monitors.
ceph-monstore-tool $ms rebuild -- --keyring /path/to/admin.keyring --mon-ids alpha beta gamma

# make a backup of the corrupted store.db just in case!  repeat for
# all monitors.
mv /var/lib/ceph/mon/mon.foo/store.db /var/lib/ceph/mon/mon.foo/store.db.corrupted

# move rebuild store.db into place.  repeat for all monitors.
mv $ms/store.db /var/lib/ceph/mon/mon.foo/store.db
chown -R ceph:ceph /var/lib/ceph/mon/mon.foo/store.db

```

上記のステップは：

1. すべてのOSDホストからマップを収集し
2. ストアを再構築する
3. キーリング・ファイルのエンティティを適切なCAPSで埋める
4. mon.foo上の破損したストアを復旧したコピーに置き換える

#### Known limitations[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-mon/ "Permalink to this headline")

以下の情報は、上記の手順では復旧できません。

* **some added keyrings**
* **creating pools**
* **MDS Maps**

## Everything Failed\! Now What?[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-mon/ "Permalink to this headline")

### Reaching out for help[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-mon/ "Permalink to this headline")

IRC では OFTC の \#ceph と \#ceph\-devel \(サーバーは irc.oftc.net\) で、ceph\-devel@vger.kernel.org と ceph\-users@lists.ceph.com で私たちを見つけることができます。ログを取り、誰かに聞かれたらすぐに答えられるようにしておいてください：対話が速く、応答における遅延が少ないほど、全員の時間が最適化される可能性が高くなります。

### Preparing your logs[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-mon/ "Permalink to this headline")

モニタログは、デフォルトでは、/var/log/ceph/ceph\-mon.FOO.log\*に保存されます。私たちはそれらを望むかもしれません。しかし、ログに必要な情報が含まれていない場合があります。モニタログがデフォルトの場所にない場合、以下を実行することでどこにあるかを確認することができます。

```
ceph-conf --name mon.FOO --show-config-value log_file
```

ログに含まれる情報量は、設定ファイルによって強制されるデバッグレベルに依存します。特定のデバッグレベルを強制していない場合、Cephはデフォルトのレベルを使用し、ログには問題を突き止めるための重要な情報が含まれない可能性があります。ログに関連情報を取得するための最初のステップは、デバッグレベルを上げることです。この場合、モニタからの情報に関心があります。他のコンポーネントで起こることと同様に、モニタの異なる部分は、異なるサブシステムでデバッグ情報を出力します。

問題に密接に関連するサブシステムのデバッグレベルを上げる必要があります。これは、Cephのトラブルシューティングに慣れていない人にとっては簡単な作業ではないかもしれません。ほとんどの場合、以下のオプションをモニタに設定するだけで、問題の潜在的な原因を突き止めることができます。

```
debug mon = 10
debug ms = 1
```

これらのデバッグレベルが十分でないことがわかった場合は、それらを上げるか、情報を取得するために他のデバッグサブシステムを定義するように依頼する可能性があります。 

### Do I need to restart a monitor to adjust debug levels?[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-mon/ "Permalink to this headline")

いいえ、2つの方法のどちらかで行います。

**クォーラムを満たしている場合**

デバッグしたいモニタにdebugオプションを注入するか：

```
ceph tell mon.FOO config set debug_mon 10/10
```

またはすべてのモニターに一度に注入する

```
ceph tell mon.* config set debug_mon 10/10
```

**クォーラムを満たしていない場合**

モニタのadminソケットを使用し、設定オプションを直接調整することができます。

```
ceph daemon mon.FOO config set debug_mon 10/10
```

デフォルト値に戻すには、上記のコマンドをデバッグレベル1/10で再実行すればよいので簡単です。 現在の値は、adminソケットと以下のコマンドで確認することができます。

```
ceph daemon mon.FOO config show
```

or:

```
ceph daemon mon.FOO config get 'OPTION_NAME'
```

### Reproduced the problem with appropriate debug levels. Now what?[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-mon/ "Permalink to this headline")

ログの該当部分のみをお送りいただくのが理想的です。該当箇所を特定するのは、簡単な作業ではないかもしれません。そのため、ログを全部送っていただいても構いませんが、常識的な範囲でお願いします。ログが数十万行に及ぶ場合、どの時点でどのような問題が発生したのかが分からないと、全部を見るのは難しいかもしれません。例えば、再現する際には、現在の日時を書き、それを元にログの該当箇所を抽出するように心がけましょう。

最後に、メーリングリストやIRCで私たちに連絡を取るか、トラッカーに新しい課題を申請してください。
