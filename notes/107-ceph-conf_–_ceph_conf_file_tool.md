# 107: ceph-conf – ceph conf file tool

 # ceph\-conf – ceph conf file tool[¶](https://docs.ceph.com/en/pacific/man/8/ceph-conf/ "Permalink to this headline")

## Synopsis[¶](https://docs.ceph.com/en/pacific/man/8/ceph-conf/ "Permalink to this headline")

**ceph\-conf** \-c _conffile_ –list\-all\-sections
**ceph\-conf** \-c _conffile_ \-L
**ceph\-conf** \-c _conffile_ \-l _prefix_
**ceph\-conf** _key_ \-s _section1_ …
**ceph\-conf** \[\-s _section_ \] \[\-r\] –lookup _key_
**ceph\-conf** \[\-s _section_ \] _key_

## Description[¶](https://docs.ceph.com/en/pacific/man/8/ceph-conf/ "Permalink to this headline")

**ceph\-conf** is a utility for getting information from a ceph configuration file. As with most Ceph programs, you can specify which Ceph configuration file to use with the `-c` flag.

Note that unlike other ceph tools, **ceph\-conf** will _only_ read from config files \(or return compiled\-in default values\)–it will _not_fetch config values from the monitor cluster. For this reason it is recommended that **ceph\-conf** only be used in legacy environments that are strictly config\-file based. New deployments and tools should instead rely on either querying the monitor explicitly for configuration \(e.g., `ceph config get <daemon> <option>`\) or use daemons themselves to fetch effective config options \(e.g.,`ceph-osd -i 123 --show-config-value osd_data`\). The latter option has the advantages of drawing from compiled\-in defaults \(which occasionally vary between daemons\), config files, and the monitor’s config database, providing the exact value that that daemon would be using if it were started.

## Actions[¶](https://docs.ceph.com/en/pacific/man/8/ceph-conf/ "Permalink to this headline")

**ceph\-conf** performs one of the following actions:

`-L````, ``--list-all-sections```[¶](https://docs.ceph.com/en/pacific/man/8/ceph-conf/ "Permalink to this definition")list all sections in the configuration file.

`-l````, ``--list-sections`` *prefix*`[¶](https://docs.ceph.com/en/pacific/man/8/ceph-conf/ "Permalink to this definition")list the sections with the given _prefix_. For example, `--list-sections mon`would list all sections beginning with `mon`.

`--lookup`` *key*`[¶](https://docs.ceph.com/en/pacific/man/8/ceph-conf/ "Permalink to this definition")search and print the specified configuration setting. Note: `--lookup` is the default action. If no other actions are given on the command line, we will default to doing a lookup.

`-h````, ``--help```[¶](https://docs.ceph.com/en/pacific/man/8/ceph-conf/ "Permalink to this definition")print a summary of usage.

## Options[¶](https://docs.ceph.com/en/pacific/man/8/ceph-conf/ "Permalink to this headline")

`-c`` *conffile*`[¶](https://docs.ceph.com/en/pacific/man/8/ceph-conf/ "Permalink to this definition")the Ceph configuration file.

`--filter-key`` *key*`[¶](https://docs.ceph.com/en/pacific/man/8/ceph-conf/ "Permalink to this definition")filter section list to only include sections with given _key_ defined.

`--filter-key-value`` *key* ``=`` *value*`[¶](https://docs.ceph.com/en/pacific/man/8/ceph-conf/ "Permalink to this definition")filter section list to only include sections with given _key_/_value_ pair.

`--name`` *type.id*`[¶](https://docs.ceph.com/en/pacific/man/8/ceph-conf/ "Permalink to this definition")the Ceph name in which the sections are searched \(default ‘client.admin’\). For example, if we specify `--name osd.0`, the following sections will be searched: \[osd.0\], \[osd\], \[global\]

`--pid`` *pid*`[¶](https://docs.ceph.com/en/pacific/man/8/ceph-conf/ "Permalink to this definition")override the `$pid` when expanding options. For example, if an option is configured like `/var/log/$name.$pid.log`, the `$pid` portion in its value will be substituded using the PID of **ceph\-conf** instead of the PID of the process specfied using the `--name` option.

`-r````, ``--resolve-search```[¶](https://docs.ceph.com/en/pacific/man/8/ceph-conf/ "Permalink to this definition")search for the first file that exists and can be opened in the resulted comma delimited search list.

`-s````, ``--section```[¶](https://docs.ceph.com/en/pacific/man/8/ceph-conf/ "Permalink to this definition")additional sections to search. These additional sections will be searched before the sections that would normally be searched. As always, the first matching entry we find will be returned.

## Examples[¶](https://docs.ceph.com/en/pacific/man/8/ceph-conf/ "Permalink to this headline")

To find out what value osd 0 will use for the “osd data” option:

```
ceph-conf -c foo.conf  --name osd.0 --lookup "osd data"
```

To find out what value will mds a use for the “log file” option:

```
ceph-conf -c foo.conf  --name mds.a "log file"
```

To list all sections that begin with “osd”:

```
ceph-conf -c foo.conf -l osd
```

To list all sections:

```
ceph-conf -c foo.conf -L
```

To print the path of the “keyring” used by “client.0”:

```
ceph-conf --name client.0 -r -l keyring
```

## Files[¶](https://docs.ceph.com/en/pacific/man/8/ceph-conf/ "Permalink to this headline")

`/etc/ceph/$cluster.conf`, `~/.ceph/$cluster.conf`, `$cluster.conf`

the Ceph configuration files to use if not specified.

## Availability[¶](https://docs.ceph.com/en/pacific/man/8/ceph-conf/ "Permalink to this headline")

**ceph\-conf** is part of Ceph, a massively scalable, open\-source, distributed storage system. Please refer to the Ceph documentation at [http://ceph.com/docs](https://docs.ceph.com/en/pacific/man/8/ceph-conf/) for more information.

## See also[¶](https://docs.ceph.com/en/pacific/man/8/ceph-conf/ "Permalink to this headline")

[ceph](https://docs.ceph.com/en/pacific/man/8/ceph-conf/)\(8\),
