# 155: ceph-fuse – FUSE-based client for ceph — Ceph Documentation

 # ceph\-fuse – FUSE\-based client for ceph[¶](https://docs.ceph.com/en/pacific/man/8/ceph-fuse/ "Permalink to this headline")

## Synopsis[¶](https://docs.ceph.com/en/pacific/man/8/ceph-fuse/ "Permalink to this headline")

**ceph\-fuse** \[\-n _client.username_\] \[ \-m _monaddr_:_port_ \] _mountpoint_ \[ _fuse options_ \]

## Description[¶](https://docs.ceph.com/en/pacific/man/8/ceph-fuse/ "Permalink to this headline")

**ceph\-fuse** is a FUSE \(“Filesystem in USErspace”\) client for Ceph distributed file system. It will mount a ceph file system specified via the \-m option or described by ceph.conf \(see below\) at the specific mount point. See[Mount CephFS using FUSE](https://docs.ceph.com/en/pacific/man/8/ceph-fuse/) for detailed information.

The file system can be unmounted with:

```
fusermount -u mountpoint
```

or by sending `SIGINT` to the `ceph-fuse` process.

## Options[¶](https://docs.ceph.com/en/pacific/man/8/ceph-fuse/ "Permalink to this headline")

Any options not recognized by ceph\-fuse will be passed on to libfuse.

`-o`` opt,[opt...]`[¶](https://docs.ceph.com/en/pacific/man/8/ceph-fuse/ "Permalink to this definition")Mount options.

`-d```[¶](https://docs.ceph.com/en/pacific/man/8/ceph-fuse/ "Permalink to this definition")Run in foreground, send all log output to stderr and enable FUSE debugging \(\-o debug\).

`-c`` ceph.conf``, ``--conf``=ceph.conf`[¶](https://docs.ceph.com/en/pacific/man/8/ceph-fuse/ "Permalink to this definition")Use _ceph.conf_ configuration file instead of the default`/etc/ceph/ceph.conf` to determine monitor addresses during startup.

`-m`` monaddress[:port]`[¶](https://docs.ceph.com/en/pacific/man/8/ceph-fuse/ "Permalink to this definition")Connect to specified monitor \(instead of looking through ceph.conf\).

`-n`` client.{cephx-username}`[¶](https://docs.ceph.com/en/pacific/man/8/ceph-fuse/ "Permalink to this definition")Pass the name of CephX user whose secret key is be to used for mounting.

`-k`` <path-to-keyring>`[¶](https://docs.ceph.com/en/pacific/man/8/ceph-fuse/ "Permalink to this definition")Provide path to keyring; useful when it’s absent in standard locations.

`--client_mountpoint/-r`` root_directory`[¶](https://docs.ceph.com/en/pacific/man/8/ceph-fuse/ "Permalink to this definition")Use root\_directory as the mounted root, rather than the full Ceph tree.

`-f```[¶](https://docs.ceph.com/en/pacific/man/8/ceph-fuse/ "Permalink to this definition")Foreground: do not daemonize after startup \(run in foreground\). Do not generate a pid file.

`-s```[¶](https://docs.ceph.com/en/pacific/man/8/ceph-fuse/ "Permalink to this definition")Disable multi\-threaded operation.

## Availability[¶](https://docs.ceph.com/en/pacific/man/8/ceph-fuse/ "Permalink to this headline")

**ceph\-fuse** is part of Ceph, a massively scalable, open\-source, distributed storage system. Please refer to the Ceph documentation at [http://ceph.com/docs](https://docs.ceph.com/en/pacific/man/8/ceph-fuse/) for more information.

## See also[¶](https://docs.ceph.com/en/pacific/man/8/ceph-fuse/ "Permalink to this headline")

fusermount\(8\),[ceph](https://docs.ceph.com/en/pacific/man/8/ceph-fuse/)\(8\)
