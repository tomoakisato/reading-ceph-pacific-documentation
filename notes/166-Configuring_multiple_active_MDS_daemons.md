# 166: Configuring multiple active MDS daemons

**クリップソース:** [166: Configuring multiple active MDS daemons — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/multimds/)

# Configuring multiple active MDS daemons[¶](https://docs.ceph.com/en/pacific/cephfs/multimds/ "Permalink to this headline")

_別名：マルチMDS，アクティブ\-アクティブMDS_

各CephFSファイルシステムは、デフォルトで1つのアクティブなMDSデーモンに設定されています。 大規模システムのメタデータパフォーマンスを拡張するには、複数のアクティブなMDSデーモンを有効にして、メタデータのワークロードを互いに共有することができます。

## When should I use multiple active MDS daemons?[¶](https://docs.ceph.com/en/pacific/cephfs/multimds/ "Permalink to this headline")

デフォルトで動作する単一のMDSでメタデータのパフォーマンスがボトルネックになっている場合、複数のアクティブなMDSデーモンを構成する必要があります。

デーモンを追加しても、すべてのワークロードでパフォーマンスが向上するとは限りません。 一般的に、1つのクライアントで動作する1つのアプリケーションは、アプリケーションが多くのメタデータ操作を並行して行っていない限り、MDSデーモン数の増加による恩恵を受けることはありません。

より多くのアクティブなMDSデーモンが一般的に恩恵をもたらすワークロードは、多くのクライアントが存在し、多くの個別のディレクトリで作業しているものです。

## Increasing the MDS active cluster size[¶](https://docs.ceph.com/en/pacific/cephfs/multimds/ "Permalink to this headline")

各CephFSファイルシステムにはmax\_mds設定があり、ランクがいくつ作成されるかを制御します。 ファイルシステムの実際のランク数は、新しいランクを引き受ける予備のデーモンが利用可能な場合にのみ増加します。例えば、MDSデーモンが1つしか動作しておらず、max\_mdsが2つに設定されている場合、2つ目のランクは作成されません。\(このような構成は、故障したランクを引き継ぐスタンバイが利用できないため、HA（Highly Available）ではないことに注意してください。このように構成された場合、クラスタは健全性警告で文句を言います\)。

max\_mds に希望のランク数を設定します。 以下の例では、コマンドの期待される結果を説明するために、「ceph status」の「fsmap」行を表示しています。

```
# fsmap e5: 1/1/1 up {0=a=up:active}, 2 up:standby

ceph fs set <fs_name> max_mds 2

# fsmap e8: 2/2/2 up {0=a=up:active,1=c=up:creating}, 1 up:standby
# fsmap e9: 2/2/2 up {0=a=up:active,1=c=up:active}, 1 up:standby
```

新しく作られたランク（1）は、「creating」の状態を経て、「active」の状態に入ります。

## Standby daemons[¶](https://docs.ceph.com/en/pacific/cephfs/multimds/ "Permalink to this headline")

複数のアクティブなMDSデーモンがある場合でも、高可用性システムには、アクティブデーモンを実行しているサーバーのいずれかに障害が発生した場合に引き継ぐためのstandbyデーモンが必要です。

その結果、高可用性システムのmax\_mdsの実用的な最大値は、システム内のMDSサーバーの総数より最大で1つ少なくなります。

複数のサーバーが故障した場合でも利用できるように、耐えられるサーバーの数に合わせてシステムのstandbyデーモンの数を増やしてください。

## Decreasing the number of ranks[¶](https://docs.ceph.com/en/pacific/cephfs/multimds/ "Permalink to this headline")

ランク数を減らすのは、max\_mdsを減らすのと同じくらい簡単です：

```
# fsmap e9: 2/2/2 up {0=a=up:active,1=c=up:active}, 1 up:standby
ceph fs set <fs_name> max_mds 1
# fsmap e10: 2/2/1 up {0=a=up:active,1=c=up:stopping}, 1 up:standby
# fsmap e10: 2/2/1 up {0=a=up:active,1=c=up:stopping}, 1 up:standby
...
# fsmap e10: 1/1/1 up {0=a=up:active}, 2 up:standby
```

クラスタは、max\_mdsに達するまで、自動的に余分なランクを段階的に停止します。

\<role\>が取ることのできるフォームの詳細については、[CephFS管理コマンド](https://docs.ceph.com/en/pacific/cephfs/administration/)を参照してください。

注意：停止したランクは、最初に一定期間stopping状態に入り、残りのアクティブなデーモンにメタデータのシェアを渡す間、stopping状態になります。 このフェーズは数秒から数分かかることがあります。 MDSがstopping状態で止まっているように見える場合は、バグの可能性があるため調査する必要があります。

MDSデーモンがup:stoppingの状態でクラッシュまたはkillされた場合、スタンバイが引き継ぎ、クラスタモニターはデーモンの停止を試みないようにします。

デーモンはstoppingが終了すると、自ら再起動してstandby状態に戻ります。

## Manually pinning directory trees to a particular rank[¶](https://docs.ceph.com/en/pacific/cephfs/multimds/ "Permalink to this headline")

複数のアクティブなメタデータサーバーの構成では、メタデータの負荷をクラスタ全体に均等に分散するように働くバランサーが実行されます。これは通常、ほとんどのユーザーにとって十分に機能しますが、メタデータを特定のランクに明示的にマッピングして動的バランサーをオーバーライドすることが望ましい場合もあります。これにより、管理者やユーザーはアプリケーションの負荷を均等に分散させたり、ユーザーのメタデータ要求がクラスタ全体に与える影響を制限したりすることができます。

このために用意された仕組みが、ディレクトリの拡張属性であるエクスポートピンと呼ばれるものです。この拡張属性の名前は、ceph.dir.pinです。 ユーザは標準的なコマンドを使用してこの属性を設定することができます：

```
setfattr -n ceph.dir.pin -v 2 path/to/dir
```

拡張属性の値は、ディレクトリサブツリーに割り当てるランクです。デフォルトの値 \-1 は、ディレクトリがピン留めされていないことを示します。

ディレクトリのエクスポートピンは、エクスポートピンが設定されている最も近い親から継承されます。 このように、ディレクトリのエクスポートピンを設定すると、そのディレクトリのすべての子に影響します。しかし、子ディレクトリのエクスポートピンを設定することで、親のピンを上書きすることができます。たとえば、以下のようになります：

```
mkdir -p a/b
# "a" and "a/b" both start without an export pin set
setfattr -n ceph.dir.pin -v 1 a/
# a and b are now pinned to rank 1
setfattr -n ceph.dir.pin -v 0 a/b
# a/b is now pinned to rank 0 and a/ and the rest of its children are still pinned to rank 1
```

## Setting subtree partitioning policies[¶](https://docs.ceph.com/en/pacific/cephfs/multimds/ "Permalink to this headline")

また、一連のポリシーによって、サブツリーの自動的な静的パーティショニングを設定することも可能です。CephFSでは、この自動的な静的パーティショニングをエフェメラルピニングと呼びます。エフェメラルピンされたディレクトリ\(inode\)は、そのinode番号の一貫したハッシュに従って、特定のランクに自動的に割り当てられます。エフェメラルピンされたディレクトリの集合は、すべてのランクに一様に分布している必要があります。

エフェメラルピンされているディレクトリは、ディレクトリinodeがキャッシュからドロップされるとピン留めが持続しない可能性があるため、このような名前が付けられました。しかし、MDSのフェイルオーバーは、ピン留めされたディレクトリのエフェメラルな性質に影響を及ぼしません。MDSはどのサブツリーがエフェメラルピンされているかをジャーナルに記録しており、MDSのフェイルオーバーでこの情報が削除されることはありません。

ディレクトリは、エフェメラルピンされるか、されないかのどちらかです。どのランクに固定されているかは、その inode 番号と一貫性のあるハッシュから判断されます。これは、エフェメラルピンされたディレクトリは、MDSクラスタ内である程度均等に分散されることを意味します。また、一貫性のあるハッシュは、MDSクラスターが成長または縮小する際の再分配を最小限に抑えます。そのため、MDSクラスタを成長させることで、他の管理者の介入なしに自動的にメタデータのスループットを向上させることができます。

現在、エフェメラルピンの種類は2種類あります：

**Distributed Ephemeral Pins**: このポリシーは、ディレクトリを（通常の断片化のしきい値よりも低い値でも）断片化し、その断片をエフェメラルにピン留めしたサブツリーとして配布します。これは、MDSのランクの範囲に直下の子を分散させる効果があります。 典型的な使用例としては、/homeディレクトリがあります。各ユーザーのホームディレクトリをMDSクラスター全体に分散させたい場合です。これは、以下の方法で設定できます：

```
setfattr -n ceph.dir.pin.distributed -v 1 /cephfs/home
```

**Random Ephemeral Pins**: このポリシーは、子孫のサブディレクトリがエフェメラルにピン留めされる可能性があることを示します。これは、拡張属性ceph.dir.pin.randomによって設定し、ピン留めされるディレクトリのパーセンテージを値に設定します。たとえば、以下のようになります：

```
setfattr -n ceph.dir.pin.random -v 0.5 /cephfs/tmp
```

キャッシュに読み込まれるディレクトリや/tmp下に作成されるディレクトリが、50%の確率でエフェメラルにピン留めされるようになります。

.001や0.1%のような小さな値を設定することを推奨します。サブツリーが多すぎると、パフォーマンスが低下することがあります。このため、mds\_export\_ephemeral\_random\_maxという設定により、パーセンテージの最大上限（デフォルト：0.01）が設けられています。この設定を超える値を設定しようとすると、MDSはEINVALを返します。

Octopusでは、ランダムと分散エフェメラルの両方のピンポリシーがデフォルトでオフになっています。これらの機能は、mds\_export\_ephemeral\_randomおよびmds\_export\_ephemeral\_distributed設定オプションで有効にすることができます。

エフェメラルピンは親のエクスポートピンを上書きすることができ、逆もまた然りです。どちらのポリシーに従うかを決定するのは、最も近い親のルールです: より近い親ディレクトリに矛盾するポリシーがある場合、代わりにそちらを使用します。たとえば：

```
mkdir -p foo/bar1/baz foo/bar2
setfattr -n ceph.dir.pin -v 0 foo
setfattr -n ceph.dir.pin.distributed -v 1 foo/bar1
```

foo/bar1/baz ディレクトリは、foo/bar1 のポリシーが foo のエクスポートピンより優先されるため、エフェメラルにピン留めされます。foo/bar2 ディレクトリは、通常、foo の pin に従います。

逆の状況の場合：

```
mkdir -p home/{patrick,john}
setfattr -n ceph.dir.pin.distributed -v 1 home
setfattr -n ceph.dir.pin -v 2 home/patrick
```

home/patrick ディレクトリとその子ディレクトリは、そのエクスポートピンが home のポリシーより優先されるため、ランク 2 に固定されます。
