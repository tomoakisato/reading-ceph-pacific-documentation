# 300: Local Pool Module

**クリップソース:** [300: Local Pool Module — Ceph Documentation](https://docs.ceph.com/en/pacific/mgr/localpool/)

# Local Pool Module[¶](https://docs.ceph.com/en/pacific/mgr/localpool/ "Permalink to this headline")

localpoolモジュールは、クラスタ全体のサブセットにローカライズされたRADOSプールを自動的に作成することができます。 例えば、デフォルトでは、クラスタ内の各個別のラックに対してプールを作成します。 これは、一部のデータをローカルに、他のデータをクラスタ全体にグローバルに配布することが望ましいデプロイメントに便利です。 使用例としては、特定のドライブ、NIC、シャーシ・モデルを分離して、パフォーマンスを測定したり、動作をテストしたりすることが挙げられます。

## Enabling[¶](https://docs.ceph.com/en/pacific/mgr/localpool/ "Permalink to this headline")

localpoolモジュールを有効にするには：

```
ceph mgr module enable localpool
```

## Configuring[¶](https://docs.ceph.com/en/pacific/mgr/localpool/ "Permalink to this headline")

localpoolモジュールは、以下のオプションを理解します：

* **subtree**
* **failure\_domain**
* **pg\_num**
* **num\_rep**
* **min\_size**
* **prefix**

これらのオプションは、config\-key インターフェースで設定します。 例えば、64個のPGのみを使用してレプリケーション・レベルを2に変更する場合：

```
ceph config set mgr mgr/localpool/num_rep 2
ceph config set mgr mgr/localpool/pg_num 64
```
