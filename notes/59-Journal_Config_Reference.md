# 59: Journal Config Reference

**クリップソース:** [59: Journal Config Reference — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/configuration/journal-ref/)

# Journal Config Reference[¶](https://docs.ceph.com/en/pacific/rados/configuration/journal-ref/ "Permalink to this headline")

ファイルストアOSDがジャーナルを使用するのは、スピードと一貫性という2つの理由からです。 Luminous 以降、BlueStore OSD のバックエンドが優先され、デフォルトになっていることに注意してください。この情報は、既存の OSD と、新規導入の際に Filestore が優先されるまれな状況のために提供されています。

* **Speed:**
* **Consistency:**

Ceph OSD Daemonsは、以下のジャーナル設定を認識します。

**journal\_dio**

Description

ジャーナルへのDirect I/Oを有効にする。_journal block align_ が true に設定されている必要がある

Type

Boolean

Required

Yes when using aio.

Default

true

**journal\_aio**

Changed in version 0.61: Cuttlefish

Description

ジャーナルへの非同期書き込みに libaio を使用することを有効にする。_journal dio_ が true に設定されている必要がある

Type

Boolean

Required

No.

Default

Version 0.61 and later, true. Version 0.60 and earlier, false.

**journal\_block\_align**

Description

書き込み操作をブロック整列させる。dioとaioに必要

Type

Boolean

Required

Yes when using dio and aio.

Default

true

**journal\_max\_write\_bytes**

Description

ジャーナルが一度に書き込む最大バイト数

Type

Integer

Required

No

Default

10\<\<20

**journal\_max\_write\_entries**

Description

ジャーナルが一度に書き込む最大エントリ数

Type

Integer

Required

No

Default

100

**journal\_queue\_max\_ops**

Description

キューで一度に許可される操作の最大数

Type

Integer

Required

No

Default

500

**journal\_queue\_max\_bytes**

Description

キューで一度に許可される最大バイト数

Type

Integer

Required

No

Default

10\<\<20

**journal\_align\_min\_size**

Description

指定された最小値より大きいデータペイロードを揃える

Type

Integer

Required

No

Default

64\<\<10

**journal\_zero\_on\_create**

Description

mkfs時にジャーナル全体を0に上書きする

Type

Boolean

Required

No

Default

false
