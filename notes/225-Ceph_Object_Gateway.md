# 225: Ceph Object Gateway

**クリップソース:** [225: Ceph Object Gateway — Ceph Documentation](https://docs.ceph.com/en/pacific/radosgw/)

# Ceph Object Gateway[¶](https://docs.ceph.com/en/pacific/radosgw/ "Permalink to this headline")

[Ceph Object Gateway](https://docs.ceph.com/en/pacific/glossary/#term-Ceph-Object-Gateway)は、Ceph Storage ClustersへのRESTfulなゲートウェイをアプリケーションに提供するために、 libradosの上に構築されたオブジェクトストレージインターフェースです。[Ceph Object Storage](https://docs.ceph.com/en/pacific/glossary/#term-Ceph-Object-Storage)は2つのインタフェースをサポートしています。

1. **S3\-compatible:**
2. **Swift\-compatible:**

Ceph Object Storageは、Ceph Storage Clusterと対話するためのHTTPサーバであるCeph Object Gatewayデーモン\(radosgw\)を使用します。OpenStack SwiftやAmazon S3と互換性のあるインタフェースを提供しているため、Ceph Object Gatewayには独自のユーザ管理があります。Ceph Object Gatewayは、Ceph File SystemクライアントやCeph Block Deviceクライアントからのデータの格納に使用されるのと同じCeph Storage Clusterにデータを格納できます。S3とSwiftのAPIは共通の名前空間を共有しているため、一方のAPIでデータを書き込み、もう一方のAPIでデータを取り出すことができます。
![1ae399f8fa9af1042d3e1cbf31828f14eb3fe01a6eb3352f88c3d2a04ac4dc50.png](image/1ae399f8fa9af1042d3e1cbf31828f14eb3fe01a6eb3352f88c3d2a04ac4dc50.png)

Ceph Object Storageは、Ceph Metadata Serverを使用しません。

* [HTTP Frontends](https://docs.ceph.com/en/pacific/radosgw/)
* [Pool Placement and Storage Classes](https://docs.ceph.com/en/pacific/radosgw/)
* [Multisite Configuration](https://docs.ceph.com/en/pacific/radosgw/)
* [Multisite Sync Policy Configuration](https://docs.ceph.com/en/pacific/radosgw/)
* [Configuring Pools](https://docs.ceph.com/en/pacific/radosgw/)
* [Config Reference](https://docs.ceph.com/en/pacific/radosgw/)
* [Admin Guide](https://docs.ceph.com/en/pacific/radosgw/)
* [S3 API](https://docs.ceph.com/en/pacific/radosgw/)
* [Data caching and CDN](https://docs.ceph.com/en/pacific/radosgw/)
* [Swift API](https://docs.ceph.com/en/pacific/radosgw/)
* [Admin Ops API](https://docs.ceph.com/en/pacific/radosgw/)
* [Python binding](https://docs.ceph.com/en/pacific/radosgw/)
* [Export over NFS](https://docs.ceph.com/en/pacific/radosgw/)
* [OpenStack Keystone Integration](https://docs.ceph.com/en/pacific/radosgw/)
* [OpenStack Barbican Integration](https://docs.ceph.com/en/pacific/radosgw/)
* [HashiCorp Vault Integration](https://docs.ceph.com/en/pacific/radosgw/)
* [KMIP Integration](https://docs.ceph.com/en/pacific/radosgw/)
* [Open Policy Agent Integration](https://docs.ceph.com/en/pacific/radosgw/)
* [Multi\-tenancy](https://docs.ceph.com/en/pacific/radosgw/)
* [Compression](https://docs.ceph.com/en/pacific/radosgw/)
* [LDAP Authentication](https://docs.ceph.com/en/pacific/radosgw/)
* [Server\-Side Encryption](https://docs.ceph.com/en/pacific/radosgw/)
* [Bucket Policy](https://docs.ceph.com/en/pacific/radosgw/)
* [Dynamic bucket index resharding](https://docs.ceph.com/en/pacific/radosgw/)
* [Multi factor authentication](https://docs.ceph.com/en/pacific/radosgw/)
* [Sync Modules](https://docs.ceph.com/en/pacific/radosgw/)
* [Bucket Notifications](https://docs.ceph.com/en/pacific/radosgw/)
* [Data Layout in RADOS](https://docs.ceph.com/en/pacific/radosgw/)
* [STS](https://docs.ceph.com/en/pacific/radosgw/)
* [STS Lite](https://docs.ceph.com/en/pacific/radosgw/)
* [Keycloak](https://docs.ceph.com/en/pacific/radosgw/)
* [Role](https://docs.ceph.com/en/pacific/radosgw/)
* [Orphan List and Associated Tooling](https://docs.ceph.com/en/pacific/radosgw/)
* [OpenID Connect Provider](https://docs.ceph.com/en/pacific/radosgw/)
* [Troubleshooting](https://docs.ceph.com/en/pacific/radosgw/)
* [Manpage radosgw](https://docs.ceph.com/en/pacific/radosgw/)
* [Manpage radosgw\-admin](https://docs.ceph.com/en/pacific/radosgw/)
* [QAT Acceleration for Encryption and Compression](https://docs.ceph.com/en/pacific/radosgw/)
* [S3\-select](https://docs.ceph.com/en/pacific/radosgw/)
* [Lua Scripting](https://docs.ceph.com/en/pacific/radosgw/)
