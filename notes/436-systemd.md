# 436: systemd

**クリップソース:** [436: systemd — Ceph Documentation](https://docs.ceph.com/en/pacific/ceph-volume/systemd/)

# systemd[¶](https://docs.ceph.com/en/pacific/ceph-volume/systemd/ "Permalink to this headline")

（[activate](https://docs.ceph.com/en/pacific/ceph-volume/lvm/activate/#ceph-volume-lvm-activate) [440: activate](440-activate.md)または [activate](https://docs.ceph.com/en/pacific/ceph-volume/simple/activate/#ceph-volume-simple-activate) [453: activate](453-activate.md)による）アクティベーションプロセスの一部として、OSD ID と uuid を名前の一部として使用する systemd ユニットが有効化されます。これらのユニットはシステムの起動時に実行され、サブコマンドの実装を通して対応するボリュームを起動するようになります。

起動のAPIは、使用するサブコマンドと、ダッシュで区切られたメタ情報の2つの部分だけで済みます。この規約でユニットは次のようになります：

```
ceph-volume@{command}-{extra metadata}
```

メタデータは、処理を実行するサブコマンドが必要とするものであれば何でも構いません。[lvm](https://docs.ceph.com/en/pacific/ceph-volume/lvm/#ceph-volume-lvm) と [simple](https://docs.ceph.com/en/pacific/ceph-volume/simple/#ceph-volume-simple) の場合、どちらも [OSD id](https://docs.ceph.com/en/pacific/glossary/#term-OSD-id) と [OSD uuid](https://docs.ceph.com/en/pacific/glossary/#term-OSD-uuid) を指定するように見えますが、これは難しい要件ではなく、サブコマンドがどのように実装されているかに過ぎません。

コマンドとメタデータの両方が、ユニットの "instance name" の一部として systemd によって保持されます。 例えば、ID が 0 の lvm サブコマンド用の OSD は以下のようになります：

```
systemctl enable ceph-volume@lvm-0-0A3E1ED2-DA8A-4F0E-AA95-61DEC71768D6
```

有効なユニットは [systemd oneshot](https://docs.ceph.com/en/pacific/glossary/#term-systemd-oneshot) サービスで、ローカルファイルシステムが使用可能な状態になった後にブート時に起動することを意図しています。

## Failure and Retries[¶](https://docs.ceph.com/en/pacific/ceph-volume/systemd/ "Permalink to this headline")

システムがオンラインになるときに障害が発生するのはよくあることです。デバイスが完全に使用できないこともあり、この予測できない動作により、OSDが使用できる状態にならないことがあります。

リトライの動作を設定するために使用される設定可能な環境変数が2つあります：

* CEPH\_VOLUME\_SYSTEMD\_TRIES: デフォルトは30
* CEPH\_VOLUME\_SYSTEMD\_INTERVAL: デフォルトは5

”_tries_"は、ユニットがOSDの起動をあきらめるまでの最大試行回数を設定する数値です。

“_interval_”は秒単位の値で、OSDを再度起動させるまでの待ち時間を決定します。
