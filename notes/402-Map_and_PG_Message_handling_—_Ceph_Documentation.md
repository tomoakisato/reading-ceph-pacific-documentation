# 402: Map and PG Message handling — Ceph Documentation

 # Map and PG Message handling[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/map_message_handling/ "Permalink to this headline")

## Overview[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/map_message_handling/ "Permalink to this headline")

The OSD handles routing incoming messages to PGs, creating the PG if necessary in some cases.

PG messages generally come in two varieties:

> 1. Peering Messages
> 2. Ops/SubOps

There are several ways in which a message might be dropped or delayed. It is important that the message delaying does not result in a violation of certain message ordering requirements on the way to the relevant PG handling logic:

> 1. Ops referring to the same object must not be reordered.
> 2. Peering messages must not be reordered.
> 3. Subops must not be reordered.

## MOSDMap[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/map_message_handling/ "Permalink to this headline")

MOSDMap messages may come from either monitors or other OSDs. Upon receipt, the OSD must perform several tasks:

> 1. Persist the new maps to the filestore. Several PG operations rely on having access to maps dating back to the last time the PG was clean.
> 2. Update and persist the superblock.
> 3. Update OSD state related to the current map.
> 4. Expose new maps to PG processes via _OSDService_.
> 5. Remove PGs due to pool removal.
> 6. Queue dummy events to trigger PG map catchup.

Each PG asynchronously catches up to the currently published map during process\_peering\_events before processing the event. As a result, different PGs may have different views as to the “current” map.

One consequence of this design is that messages containing submessages from multiple PGs \(MOSDPGInfo, MOSDPGQuery, MOSDPGNotify\) must tag each submessage with the PG’s epoch as well as tagging the message as a whole with the OSD’s current published epoch.

## MOSDPGOp/MOSDPGSubOp[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/map_message_handling/ "Permalink to this headline")

See OSD::dispatch\_op, OSD::handle\_op, OSD::handle\_sub\_op

MOSDPGOps are used by clients to initiate rados operations. MOSDSubOps are used between OSDs to coordinate most non peering activities including replicating MOSDPGOp operations.

OSD::require\_same\_or\_newer map checks that the current OSDMap is at least as new as the map epoch indicated on the message. If not, the message is queued in OSD::waiting\_for\_osdmap via OSD::wait\_for\_new\_map. Note, this cannot violate the above conditions since any two messages will be queued in order of receipt and if a message is received with epoch e0, a later message from the same source must be at epoch at least e0. Note that two PGs from the same OSD count for these purposes as different sources for single PG messages. That is, messages from different PGs may be reordered.

MOSDPGOps follow the following process:

> 1. OSD::handle\_op: validates permissions and crush mapping. discard the request if they are not connected and the client cannot get the reply \( See OSD::op\_is\_discardable \) See OSDService::handle\_misdirected\_op See PG::op\_has\_sufficient\_caps See OSD::require\_same\_or\_newer\_map
> 2. OSD::enqueue\_op

MOSDSubOps follow the following process:

> 1. OSD::handle\_sub\_op checks that sender is an OSD
> 2. OSD::enqueue\_op

OSD::enqueue\_op calls PG::queue\_op which checks waiting\_for\_map before calling OpWQ::queue which adds the op to the queue of the PG responsible for handling it.

OSD::dequeue\_op is then eventually called, with a lock on the PG. At this time, the op is passed to PG::do\_request, which checks that:

> 1. the PG map is new enough \(PG::must\_delay\_op\)
> 2. the client requesting the op has enough permissions \(PG::op\_has\_sufficient\_caps\)
> 3. the op is not to be discarded \(PG::can\_discard\_{request,op,subop,scan,backfill}\)
> 4. the PG is active \(PG::flushed boolean\)
> 5. the op is a CEPH\_MSG\_OSD\_OP and the PG is in PG\_STATE\_ACTIVE state and not in PG\_STATE\_REPLAY

If these conditions are not met, the op is either discarded or queued for later processing. If all conditions are met, the op is processed according to its type:

> 1. CEPH\_MSG\_OSD\_OP is handled by PG::do\_op
> 2. MSG\_OSD\_SUBOP is handled by PG::do\_sub\_op
> 3. MSG\_OSD\_SUBOPREPLY is handled by PG::do\_sub\_op\_reply
> 4. MSG\_OSD\_PG\_SCAN is handled by PG::do\_scan
> 5. MSG\_OSD\_PG\_BACKFILL is handled by PG::do\_backfill

## CEPH\_MSG\_OSD\_OP processing[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/map_message_handling/ "Permalink to this headline")

PrimaryLogPG::do\_op handles CEPH\_MSG\_OSD\_OP op and will queue it

> 1. in wait\_for\_all\_missing if it is a CEPH\_OSD\_OP\_PGLS for a designated snapid and some object updates are still missing
> 2. in waiting\_for\_active if the op may write but the scrubber is working
> 3. in waiting\_for\_missing\_object if the op requires an object or a snapdir or a specific snap that is still missing
> 4. in waiting\_for\_degraded\_object if the op may write an object or a snapdir that is degraded, or if another object blocks it \(“blocked\_by”\)
> 5. in waiting\_for\_backfill\_pos if the op requires an object that will be available after the backfill is complete
> 6. in waiting\_for\_ack if an ack from another OSD is expected
> 7. in waiting\_for\_ondisk if the op is waiting for a write to complete

## Peering Messages[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/map_message_handling/ "Permalink to this headline")

See OSD::handle\_pg\_\(notify|info|log|query\)

Peering messages are tagged with two epochs:

> 1. epoch\_sent: map epoch at which the message was sent
> 2. query\_epoch: map epoch at which the message triggering the message was sent

These are the same in cases where there was no triggering message. We discard a peering message if the message’s query\_epoch if the PG in question has entered a new epoch \(See PG::old\_peering\_evt, PG::queue\_peering\_event\). Notifies, infos, notifies, and logs are all handled as PG::PeeringMachine events and are wrapped by PG::queue\_\* by PG::CephPeeringEvts, which include the created state machine event along with epoch\_sent and query\_epoch in order to generically check PG::old\_peering\_message upon insertion and removal from the queue.

Note, notifies, logs, and infos can trigger the creation of a PG. See OSD::get\_or\_create\_pg.
