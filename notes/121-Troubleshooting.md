# 121: Troubleshooting

**クリップソース:** [121: Troubleshooting — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/troubleshooting/)

# Troubleshooting[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/ "Permalink to this headline")

Cephはまだ最先端なので、設定の検証、ログ出力の修正、モニタやOSDのトラブルシューティング、メモリやCPUの使用状況のプロファイル、Cephコミュニティへの問い合わせなどが必要な状況に遭遇する可能性があります。

* [The Ceph Community](https://docs.ceph.com/en/pacific/rados/troubleshooting/)
* [Logging and Debugging](https://docs.ceph.com/en/pacific/rados/troubleshooting/)
* [Troubleshooting Monitors](https://docs.ceph.com/en/pacific/rados/troubleshooting/)
* [Troubleshooting OSDs](https://docs.ceph.com/en/pacific/rados/troubleshooting/)
* [Troubleshooting PGs](https://docs.ceph.com/en/pacific/rados/troubleshooting/)
* [Memory Profiling](https://docs.ceph.com/en/pacific/rados/troubleshooting/)
* [CPU Profiling](https://docs.ceph.com/en/pacific/rados/troubleshooting/)
