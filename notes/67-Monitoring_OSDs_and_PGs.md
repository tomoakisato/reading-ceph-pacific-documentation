# 67: Monitoring OSDs and PGs

**クリップソース:** [67: Monitoring OSDs and PGs — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/operations/monitoring-osd-pg/)

# Monitoring OSDs and PGs[¶](https://docs.ceph.com/en/pacific/rados/operations/monitoring-osd-pg/ "Permalink to this headline")

高可用性と高信頼性を実現するには、ハードウェアとソフトウェアの問題を管理するフォールトトレラントなアプローチが必要です。

Cephには単一障害点がなく、「デグレード」モードでもデータに対する要求を処理することができます。

Cephのデータ配置は、データが特定のOSDアドレスに直接バインドされないようにするために、間接的なレイヤーを導入しています。

つまり、システム障害を追跡するには、問題の根源にあるPGと基礎となるOSDを見つける必要があります。

ヒント：クラスタの一部で障害が発生した場合、特定のオブジェクトにアクセスできなくなることがありますが、他のオブジェクトにアクセスできなくなるわけではありません。

フォルトに遭遇しても、慌てないでください。

OSDとPGを監視する手順に従えばよいのです。

その後、トラブルシューティングを開始します。

Cephは通常、自己修復が可能です。

しかし、問題が続く場合は、OSDとPGを監視することで問題を特定することができます。

## Monitoring OSDs[¶](https://docs.ceph.com/en/pacific/rados/operations/monitoring-osd-pg/ "Permalink to this headline")

OSDの状態は、クラスタ内（in）またはクラスタ外（out）であり、また、稼働中（up）または稼働していない（down）である。

OSDがアップしている場合、クラスタ内（データの読み取りと書き込みが可能）か、クラスタ外かのどちらかです。 

クラスタ内（in）にあり、最近クラスタ外（out）に移動した場合、CephはPGを他のOSDにマイグレートします。

OSDがクラスタから外れている（out）場合、CRUSHはOSDにPGを割り当てません。

OSDがダウン（down）している場合、それもアウト（out）にする必要があります。

注意：OSDがダウン（down）して入っている（in）場合は、問題があり、クラスタは健全な状態にはなりません。

![02f607b45842e47da91dce4eb18e312c2560d41d5bad3ca34ff62779ac5e00a0.png](image/02f607b45842e47da91dce4eb18e312c2560d41d5bad3ca34ff62779ac5e00a0.png)

ceph health、ceph\-s、ceph\-wなどのコマンドを実行すると、クラスタが常にHEALTH OKをエコーバックしないことに気がつくかもしれません。

OSDに関しては、いくつかの想定される状況でクラスタがHEALTH OKをエコーしないことを予期しておく必要があります。

1. まだクラスタが起動していない（反応しない）
2. クラスタを起動または再起動したばかりで、PGが作成され、OSDがピアリングされている最中なので、まだ準備ができていない
3. OSDを追加・削除したところ
4. クラスターマップを変更したところ

OSDのモニタリングで重要なことは、クラスタが稼働しているときに、クラスタにあるすべてのOSDも稼働していることを確認することです。

すべてのOSDが稼働しているかどうかを確認するには、以下を実行します。

```
ceph osd stat
```

その結果、OSDの総数（x）、アップしている数（y）、インしている数（z）、マップエポック（eNNNN）がわかるはずです。

```
x osds: y up, z in; epoch: eNNNN
```

クラスタにあるOSDの数が稼働しているOSDの数よりも多い場合は、次のコマンドを実行して、稼働していないceph\-osdデーモンを特定します。

```
ceph osd tree
```

```
#ID CLASS WEIGHT  TYPE NAME             STATUS REWEIGHT PRI-AFF
 -1       2.00000 pool openstack
 -3       2.00000 rack dell-2950-rack-A
 -2       2.00000 host dell-2950-A1
  0   ssd 1.00000      osd.0                up  1.00000 1.00000
  1   ssd 1.00000      osd.1              down  1.00000 1.00000
```

ヒント：よく設計されたCRUSH階層を検索する機能は、物理的な場所をより速く特定することで、クラスタのトラブルシューティングに役立つ場合があります。

OSDがダウンしている場合は、起動します。

```
sudo systemctl start ceph-osd@1
```

停止した、または再起動しないOSDに関連する問題については、「[OSD Not Running](https://docs.ceph.com/en/pacific/rados/operations/monitoring-osd-pg/) 」を参照してください。

## PG Sets[¶](https://docs.ceph.com/en/pacific/rados/operations/monitoring-osd-pg/ "Permalink to this headline")

CRUSHがPGをOSDに割り当てる際、プールのレプリカの数を調べ、PGの各レプリカが異なるOSDに割り当てられるように、PGをOSDに割り当てます。

例えば、プールで3つのPGのレプリカが必要な場合、CRUSHはそれらをそれぞれosd.1, osd.2, osd.3に割り当てることができます。

CRUSHは実際には、CRUSHマップで設定した障害ドメインを考慮した擬似ランダム配置を求めるため、大規模クラスタで最近接のOSDにPGが割り当てられることはほとんどありません。

Cephは、アクティング・セットを使用してクライアント要求を処理します。

アクティング・セットとは、要求を実際に処理するOSDのセットのことで、PGシャードのfull and working バージョン\(単\)を持ちます。

アップ・セットとして特定のPGのシャードを含むべきOSDのセット、つまり、データが移動/コピーされる\(またはされる予定の\)場所です。

場合によっては、アクティング・セット内のOSDがダウンしていたり、PG内のオブジェクトに対する要求を処理できないことがあります。

よくある例としては、以下のようなものがあります。

* OSDを追加または削除しました。そして、CRUSHはPGを他のOSDに再割り当てし、アクティング・セットの構成を変更し、「バックフィル」プロセスでデータの移行を開始しました。
* あるOSDがダウンし、再起動され、現在リカバリしています。
* アクティング・セット内のOSDがダウンしているか、リクエストに対応できないため、別のOSDが一時的にその任務を引き継いでいます。

ほとんどの場合、アップ・セットとアクティング・セットは同一です。

そうでない場合、CephがPGをマイグレーションしている \(リマップされている\)、OSDが回復している、または問題があることを示す場合があります \(つまり、Cephは通常このようなシナリオで「HEALTH WARN」状態を「stuck stale」メッセージとともにエコーします\)。

PGの一覧を取得する場合は、実行します。

```
ceph pg dump
```

指定されたPGのアクティング・セットまたはアップ・セット内にどのOSDがあるかを表示するには、以下を実行します。

```
ceph pg map {pg-num}
```

その結果、osdmapエポック（eNNN）、PG番号（{pg\-num}）、アップセット内のOSD（up\[\]）、アクティングセット内のOSD（acting\[\]）がわかるはずです。

```
osdmap eNNN pg {raw-pg-num} ({pg-num}) -> up [0,1,2] acting [0,1,2]
```

注意：アップ・セットとアクティング・セットが一致しない場合、クラスタ自体のリバランスやクラスタの潜在的な問題の可能性があります。

## Peering[¶](https://docs.ceph.com/en/pacific/rados/operations/monitoring-osd-pg/ "Permalink to this headline")

PGにデータを書き込む前に、アクティブな状態である必要があり、クリーンな状態である必要があります。

CephがPGの現在の状態を判断するには、PGのプライマリOSD\(つまり、アクティングセットの最初のOSD\)が、セカンダリおよびターシャリOSD\(PGのレプリカを3個持つプールと仮定\)とピアリングしてPGの現在の状態に関する合意を確立します。
![174325a044ac2a293e36baa0b41fcd253528f7e6f542c0e68a31da37f968cdc2.png](image/174325a044ac2a293e36baa0b41fcd253528f7e6f542c0e68a31da37f968cdc2.png)

また、OSDはその状態をモニタに報告します。

詳しくは、「 [Configuring Monitor/OSD Interaction](https://docs.ceph.com/en/pacific/rados/operations/monitoring-osd-pg/)」を参照してください。

ピアリングの問題のトラブルシューティングは、[Peering Failure](https://docs.ceph.com/en/pacific/rados/operations/monitoring-osd-pg/)を参照してください。 

## Monitoring Placement Group States[¶](https://docs.ceph.com/en/pacific/rados/operations/monitoring-osd-pg/ "Permalink to this headline")

ceph health、ceph\-s、ceph\-wなどのコマンドを実行すると、クラスタが常にHEALTH OKをエコーバックするとは限らないことに気付くかもしれません。

OSDが実行されているかどうかを確認した後、PGの状態も確認する必要があります。

PGのピアリングに関連する多くの状況で、クラスタがHEALTH OKをエコーしないことを予期しておく必要があります。

1. プールを作ったばかりで、PGはまだピアリングされていない
2. PGはリカバリ中
3. クラスタにOSDを追加、またはクラスタからOSDを削除したところ
4. CRUSHマップを修正したところで、PGがマイグレーションされた
5. PGの異なるレプリカに不整合なデータが存在する
6. CephはPGのレプリカをスクラビング中
7. バックフィル作業を完了するのに十分なストレージ容量がない

上記のいずれかの状況でCephがHEALTH WARNをエコーした場合、クラスタは自力で回復します。

場合によっては、対処が必要なこともあります。

PGの監視で重要なのは、クラスタが稼働しているときに、すべてのPGがアクティブであり、できればクリーンな状態であることを確認することです。

すべてのPGの状態を確認するには、以下を実行します。

```
ceph pg stat
```

その結果、PGの総数（x）、active\+cleanなど特定の状態にあるPGの数（y）、保存されているデータ量（z）がわかるはずです。

```
x pgs: y active+clean; z bytes data, aa MB used, bb GB / cc GB avail
```

注記: Cephでは、PGに対して複数の状態を報告することが一般的です。

Cephは、PGの状態に加えて、使用されたストレージ容量の量\(aa\)、残りのストレージ容量の量\(bb\)、およびPGの合計ストレージ容量もエコーバックします。これらの数値は、いくつかのケースで重要になることがあります。

* Near full ratioまたはFull ratioに到達している
* CRUSHの設定にエラーがあるため、データがクラスタに分散されない

Placement Group IDs

PG IDは、プール番号（プール名ではない）の後にピリオド（.）とPG ID（16進数）で構成されています。

ceph osd lspoolsの出力から、プール番号とその名前を確認できます。

たとえば、最初に作成されたプールは、プール番号1に対応します。

完全修飾されたPG IDは次の形式をとります。

```
{pool-num}.{pg-id}
```

そして、一般的にはこのような形になります。

```
1.1f
```

PGの一覧を取得する場合は、以下を実行します。

```
ceph pg dump
```

また、JSON形式で出力し、ファイルに保存することも可能です。

```
ceph pg dump -o {filename} --format=json
```

特定のPGを問い合わせるには、次のように実行します。

```
ceph pg {poolnum}.{pg-id} query
```

Cephは、クエリをJSON形式で出力します。

以下のサブセクションでは、一般的なpgの状態について詳しく説明します。

### Creating[¶](https://docs.ceph.com/en/pacific/rados/operations/monitoring-osd-pg/ "Permalink to this headline")

プールを作成すると、指定した数のPGが作成されます。 

Cephは、1つ以上のPGを作成する際にcreatingをエコー表示します。

それらが作成されると、PGのアクティングセットの一部であるOSDがピアリングされます。

ピアリングが完了すると、PGのステータスはactive\+cleanになり、CephクライアントがPG

への書き込みを開始できるようになります。
![f5792aa71980fbe30166d3a5cd03af0c205e3befdae1f19636c3b848da2e5418.png](image/f5792aa71980fbe30166d3a5cd03af0c205e3befdae1f19636c3b848da2e5418.png)

### Peering[¶](https://docs.ceph.com/en/pacific/rados/operations/monitoring-osd-pg/ "Permalink to this headline")

CephがPGをピアリングしているとき、CephはPGのレプリカを格納するOSDを、PG内のオブジェクトとメタデータの状態について一致させることになります。

Cephがピアリングを完了すると、これは、PGを格納するOSDがPGの現在の状態について合意することを意味します。

ただし、ピアリングプロセスの完了は、各レプリカに最新のコンテンツがあることを意味するものではありません。

Authoritative History

Cephは、アクティング・セットのすべてのOSDが書き込み操作を永続化するまで、クライアントへの書き込み操作を承認\(ACK\)しません。

この方法によって、アクティング・セットの少なくとも1つのメンバーが、最後に成功したピアリング操作以降、承認\(ACK\)されたすべての書き込み操作のレコードを持つことが保証されます。

認識\(ACK\)された各書き込み操作の正確なレコードにより、CephはPGの新しい信頼できる履歴を構築し、配布することができます。これは、完全に順序付けられた操作のセットで、実行するとPGのOSDのコピーが最新になります。

### Active[¶](https://docs.ceph.com/en/pacific/rados/operations/monitoring-osd-pg/ "Permalink to this headline")

Cephがピアリングプロセスを完了すると、PGがアクティブになる場合があります。

アクティブな状態とは、PG内のデータがプライマリPGとレプリカで一般的に読み取りおよび書き込み操作に利用可能であることを意味します。

### Clean[¶](https://docs.ceph.com/en/pacific/rados/operations/monitoring-osd-pg/ "Permalink to this headline")

PGがクリーンな状態にあるとき、プライマリOSDとレプリカOSDは正常にピアリングし、PGにはストレイ・レプリカがありません。

Cephは、PG内のすべてのオブジェクトを正しい回数だけレプリケートしました。

### Degraded[¶](https://docs.ceph.com/en/pacific/rados/operations/monitoring-osd-pg/ "Permalink to this headline")

クライアントがプライマリOSDにオブジェクトを書き込むと、プライマリOSDはレプリカOSDにレプリカを書き込む責任を負う。プライマリOSDがストレージにオブジェクトを書き込んだ後、プライマリOSDがレプリカOSDからレプリカオブジェクトを正常に作成したという通知を受け取るまで、PGはデグレード状態のままになります。

PGがアクティブ\+デグレードになる理由は、OSDがまだすべてのオブジェクトを保持していなくても、アクティブになる可能性があるからです。

OSDがダウンすると、CephはそのOSDに割り当てられた各PGをデグレードとしてマークします。

OSDがオンラインに戻ると、OSDは再びピアリングする必要があります。

ただし、クライアントがアクティブであれば、デグレードされたPGに新しいオブジェクトを書き込むことは可能です。

OSDがダウンしてデグレード状態が続く場合、CephはダウンしたOSDをクラスタ外としてマークし、ダウンしたOSDから別のOSDにデータを再マップする場合があります。

マークダウンされてからマークアウトされるまでの時間は、mon osd down out intervalによって制御され、デフォルトでは600秒に設定されています。

CephがPGにあるべきと考える1つまたは複数のオブジェクトが見つからないため、PGがデグレードされることもあります。

見つからないオブジェクトに対する読み取りまたは書き込みはできませんが、デグレードされたPG内の他のすべてのオブジェクトにアクセスすることは可能です。

### Recovering[¶](https://docs.ceph.com/en/pacific/rados/operations/monitoring-osd-pg/ "Permalink to this headline")

Cephは、ハードウェアとソフトウェアの問題が継続する規模でのフォールトトレランスを想定して設計されています。

OSDがダウンすると、そのコンテンツはPG内の他のレプリカの現在の状態より遅れることがあります。

OSDがバックアップされると、PGのコンテンツは現在の状態を反映するように更新される必要があります。

その期間中、OSDはRECOVERINGの状態を反映することがあります。

ハードウェアの故障が複数のOSDのカスケード障害を引き起こす可能性があるため、リカバリは必ずしも容易ではありません。

たとえば、ラックやキャビネットのネットワークスイッチが故障し、多数のホストマシンのOSDがクラスタの現在の状態から遅れることがあります。

障害が解決すると、それぞれのOSDがリカバリする必要があります。

Cephには、新しいサービス要求と、データオブジェクトをリカバリしてPGを現在の状態に復元する必要性との間のリソース競合のバランスをとるための設定が多数用意されています。

osd recovery delay start設定により、OSDはリカバリプロセスを開始する前に再起動、再ピア、さらに一部のリプレイ要求を処理することができます。 

osd recovery thread timeoutはスレッドタイムアウトを設定し、複数のOSDが障害、再起動、再ピアを時差速度で行うことができるからです。

osd recovery max active設定は、OSDがサービスを提供できなくなるのを防ぐために、OSDが同時に受け入れるリカバリ要求の数を制限します。

osd recovery max chunk設定は、ネットワークの輻輳を防ぐために、リカバリされたデータチャンクのサイズを制限します。

### Back Filling[¶](https://docs.ceph.com/en/pacific/rados/operations/monitoring-osd-pg/ "Permalink to this headline")

新しいOSDがクラスタに参加すると、CRUSHはクラスタ内のOSDから新しく追加されたOSDにPGを再割り当てします。

新しいOSDに再割り当てされたPGをすぐに受け入れるように強制すると、新しいOSDに過剰な負荷がかかる可能性があります。

OSDをPGでバックフィルすることで、このプロセスをバックグラウンドで開始することができます。 

バックフィルが完了すると、新しいOSDは準備ができ次第、要求の処理を開始します。

backfill\_waitはバックフィル処理が保留中であることを示しますが、まだ進行中ではありません。

backfillingはバックフィル処理が進行中であることを示し、backfill\_toofullはバックフィル処理が要求されたが、ストレージ容量不足により完了できなかったことを示します。

PGを埋め戻すことができない場合、それは不完全と見なされることがあります。

backfill\_toofullの状態は一時的である可能性があります。 

PGの移動に伴い、スペースに空きができる可能性があります。 

backfill\_toofullはbackfill\_waitと同様で、条件が変わればすぐにバックフィルを実行することができます。

Cephは、PGをOSD\(特に新しいOSD\)に再割り当てする際に発生する負荷スパイクを管理するための設定を多数提供しています。

デフォルトでは、osd\_max\_backfillsはOSDとの同時バックフィル最大数を1に設定します。

backfill full ratioは、OSDがその完全比率 \(デフォルトでは90%\)に近づいている場合にバックフィル要求を拒否することができ、ceph osd set\-backfillfull\-ratioコマンドで変更することが可能です。

OSDがバックフィル要求を拒否した場合、osd backfill retry intervalは、OSDが要求を再試行することを可能にします（デフォルトで30秒後）。

OSDはまた、スキャン間隔（デフォルトでは64と512）を管理するためにosd backfill scan minとosd backfill scan maxを設定することができます。

### Remapped[¶](https://docs.ceph.com/en/pacific/rados/operations/monitoring-osd-pg/ "Permalink to this headline")

PGにサービスを提供するアクティング・セットが変更されると、データは古いアクティング・セットから新しいアクティング・セットに移行されます。

新しいプライマリOSDがリクエストを処理するのに時間がかかることがあります。

そのため、PGの移行が完了するまで、古いプライマリにリクエストのサービスを継続するよう依頼することがあります。

データの移行が完了すると、マッピングは新しいアクティング・セットのプライマリOSDを使用します。

### Stale[¶](https://docs.ceph.com/en/pacific/rados/operations/monitoring-osd-pg/ "Permalink to this headline")

Cephはハートビートを使用してホストとデーモンの実行を確認しますが、ceph\-osdデーモンが統計情報をタイムリーに報告しないスタック状態になることもあります\(一時的なネットワーク障害など\)。

デフォルトでは、OSDデーモンはPG、アップスルー、ブート、障害の統計情報を半秒ごと（つまり0.5）に報告しますが、これはハートビート閾値よりも頻度が高くなります。

PGのアクティング・セットのプライマリOSDがモニタへの報告に失敗した場合、または他のOSDがプライマリOSDのダウンを報告した場合、モニタはPGをstaleとマークします。

クラスタを開始すると、ピアリング・プロセスが完了するまで、staleの状態が表示されるのが一般的です。

クラスタがしばらく実行された後、stale状態の配置グループを見ると、それらのPGのプライマリOSDがダウンしているか、モニタにPG統計情報を報告していないことを示します。

## Identifying Troubled PGs[¶](https://docs.ceph.com/en/pacific/rados/operations/monitoring-osd-pg/ "Permalink to this headline")

前述の通り、PGの状態がactive\+cleanでないからといって、必ずしも問題があるわけではありません。

一般に、PGがスタックした場合、Cephの自己修復機能が働いていない可能性があります。

スタックした状態には以下のようなものがあります。

* **Unclean**
* **Inactive**
* **Stale**

スタックしたPGを特定するには、次のように実行します。

```
ceph pg dump_stuck [unclean|inactive|stale|undersized|degraded]
```

詳しくは、「[Placement Group Subsystem](https://docs.ceph.com/en/pacific/rados/operations/monitoring-osd-pg/)」を参照してください。固まったPGのトラブルシューティングは、「[Troubleshooting PG Errors](https://docs.ceph.com/en/pacific/rados/operations/monitoring-osd-pg/)」を参照してください。 

## Finding an Object Location[¶](https://docs.ceph.com/en/pacific/rados/operations/monitoring-osd-pg/ "Permalink to this headline")

Ceph Object Storeにオブジェクトデータを格納するには、Cephクライアントが必要です。

1. オブジェクト名を設定する
2. プールを指定する

Cephクライアントが最新のクラスタマップを取得し、CRUSHアルゴリズムがオブジェクトをPGにマッピングする方法を計算し、そのPGをOSDに動的に割り当てる方法を計算します。

オブジェクトの場所を見つけるために必要なのは、オブジェクト名とプール名だけです。

例えば

```
ceph osd map {poolname} {object-name} [namespace]
```

練習問題： オブジェクトの位置を確認する

練習として、オブジェクトを作成してみましょう。

コマンドラインで radosput コマンドを使用して、オブジェクト名、いくつかのオブジェクトデータを含むテストファイルへのパス、およびプール名を指定します。

例えば、以下のようになります。

```
rados put {object-name} {file-path} --pool=data
rados put test-object-1 testfile.txt --pool=data
```

Ceph Object Storeがオブジェクトを保存したことを確認するには、以下を実行します。

```
rados -p data ls
```

ここで、オブジェクトの位置を特定します。

```
ceph osd map {pool-name} {object-name}
ceph osd map data test-object-1
```

Cephはオブジェクトの位置を出力する必要があります。例えば

```
osdmap e537 pool 'data' (1) object 'test-object-1' -> pg 1.d1743484 (1.4) -> up ([0,1], p0) acting ([0,1], p0)
```

テストオブジェクトを削除するには、radosrmコマンドを使用して単純に削除します。例えば

```
rados rm test-object-1 --pool=data
```

クラスタの進化に伴い、オブジェクトの位置は動的に変化する可能性があります。Cephの動的リバランスの利点の1つは、Cephが手動で移行を実行する必要から解放されることです。詳細については、「 [Architecture](https://docs.ceph.com/en/pacific/rados/operations/monitoring-osd-pg/)」のセクションを参照してください。
