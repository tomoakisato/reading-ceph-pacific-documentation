# 293: ceph-mgr administrator’s guide

**クリップソース:** [293: ceph\-mgr administrator’s guide — Ceph Documentation](https://docs.ceph.com/en/pacific/mgr/administrator/)

# ceph\-mgr administrator’s guide[¶](https://docs.ceph.com/en/pacific/mgr/administrator/ "Permalink to this headline")

## Manual setup[¶](https://docs.ceph.com/en/pacific/mgr/administrator/ "Permalink to this headline")

通常、ceph\-ansibleなどのツールを使用してceph\-mgrデーモンをセットアップすることになります。 この手順では、ceph\-mgrデーモンを手動でセットアップする方法を説明します。

まず、デーモン用の認証キーを作成します：

```
ceph auth get-or-create mgr.$name mon 'allow profile mgr' osd 'allow *' mds 'allow *'
```

そのキーをmgr dataパスに置きます。クラスタ「ceph」およびmgr $name「foo」の場合、/var/lib/ceph/mgr/ceph\-fooになります。

ceph\-mgrデーモンを起動します：

```
ceph-mgr -i $name
```

ceph statusの出力にmgrの状態行が含まれるようになり、mgrが起動したことを確認します：

```
mgr active: $name
```

## Client authentication[¶](https://docs.ceph.com/en/pacific/mgr/administrator/ "Permalink to this headline")

マネージャーは新しいデーモンで、新しいCephX CAPSを必要とします。古いバージョンのCephからクラスタをアップグレードした場合、またはデフォルトのインストール/デプロイツールを使用した場合、管理クライアントがこのCAPSを自動的に取得するはずです。他の場所からツールを使用すると、特定のcephクラスタコマンドを呼び出すときにEACCESエラーが発生する場合があります。これを修正するには、[Modifying User Capabilities](https://docs.ceph.com/en/pacific/mgr/administrator/)を使用して、クライアントのcephx CAPSに「mgr allow \*」スタンザを追加します。[68: User Management](68-User_Management.md)

## High availability[¶](https://docs.ceph.com/en/pacific/mgr/administrator/ "Permalink to this headline")

一般に、同じレベルの可用性を得るには、ceph\-monデーモンを実行している各ホストにceph\-mgrをセットアップする必要があります。

デフォルトでは、最初に起動したceph\-mgrインスタンスがモニタによってアクティブにされ、他のインスタンスはスタンバイになります。 ceph\-mgrデーモン間のクォーラムは必要ありません。

アクティブデーモンがモニタへのビーコン送信に mon mgr beacon grace \(デフォルト 30s\) 以上失敗した場合、スタンバイデーモンに置き換わります。

フェイルオーバーをプリエンプトしたい場合は、ceph mgr fai l\<mgr name\>を使用してceph\-mgrデーモンを明示的にfailedとマークすることができます。

## Using modules[¶](https://docs.ceph.com/en/pacific/mgr/administrator/ "Permalink to this headline")

ceph mgr module lsコマンドを使用して、利用可能なモジュールと現在有効になっているモジュールを確認します。 モジュールを有効化または無効化するには、それぞれceph mgr module enable\<module\>およびceph mgr module disable\<module\>というコマンドを使用します。

モジュールが有効な場合、アクティブなceph\-mgrデーモンはそのモジュールをロードして実行します。 HTTPサーバのようなサービスを提供するモジュールの場合、モジュールはロード時にそのアドレスを公開することがあります。 このようなモジュールのアドレスを確認するには、ceph mgr servicesコマンドを使用します。

モジュールによっては、アクティブデーモンと同様にスタンバイceph\-mgrデーモン上で動作する特別なスタンバイモードを実装することもできます。 これにより、サービスを提供するモジュールは、クライアントがスタンバイに接続しようとした場合、そのクライアントをアクティブデーモンにリダイレクトすることができます。

各モジュールが提供する機能の詳細については、各マネージャーモジュールのドキュメントページを参照してください。

以下は、Dashboardモジュールを有効にする例です：

```
$ ceph mgr module ls
{
        "enabled_modules": [
                "restful",
                "status"
        ],
        "disabled_modules": [
                "dashboard"
        ]
}

$ ceph mgr module enable dashboard
$ ceph mgr module ls
{
        "enabled_modules": [
                "restful",
                "status",
                "dashboard"
        ],
        "disabled_modules": [
        ]
}

$ ceph mgr services
{
        "dashboard": "http://myserver.com:7789/",
        "restful": "https://myserver.com:8789/"
}
```

クラスタの初回起動時には mgr\_initial\_modules 設定を使用して、有効にするモジュールを上書きします。 しかし、この設定はクラスタの残りの寿命の間、無視されます: ブートストラップ時のみ使用してください。 たとえば、初めてモニタデーモンを起動する前に、ceph.confに次のようなセクションを追加します：

```
[mon]
    mgr_initial_modules = dashboard balancer
```

## Calling module commands[¶](https://docs.ceph.com/en/pacific/mgr/administrator/ "Permalink to this headline")

モジュールがコマンドラインフックを実装している場合、そのコマンドは通常のCephコマンドとしてアクセスできます。 Cephは自動的にモジュールコマンドを標準CLIインタフェースに組み込み、それらをモジュールに適切にルーティングします：

```
ceph <command | help>
```

## Configuration[¶](https://docs.ceph.com/en/pacific/mgr/administrator/ "Permalink to this headline")

**mgr\_module\_path**

Description

モジュールを読み込むパス

Type

String

Default

"\<librarydir\>/mgr"

**mgr\_data**

Description

デーモンのデータ（キーリングなど）を読み込むパス

Type

String

Default

"/var/lib/ceph/mgr/$cluster\-$id"

**mgr\_tick\_period**

Description

mgrからモニタへのビーコン、その他の定期的なチェックの間隔（秒）

Type

Integer

Default

5

**mon\_mgr\_beacon\_grace**

Description

最後のビーコンからどのくらいでmgrがfailedと判断されるか（秒）

Type

Integer

Default

30
