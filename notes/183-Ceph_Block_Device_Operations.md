# 183: Ceph Block Device Operations

**クリップソース:** [183: Ceph Block Device Operations — Ceph Documentation](https://docs.ceph.com/en/pacific/rbd/rbd-operations/)

# Ceph Block Device Operations[¶](https://docs.ceph.com/en/pacific/rbd/rbd-operations/undefined "Permalink to this headline")

* [Snapshots](https://docs.ceph.com/en/pacific/rbd/rbd-operations/undefined)
* [Exclusive Locking](https://docs.ceph.com/en/pacific/rbd/rbd-operations/undefined)
* [Mirroring](https://docs.ceph.com/en/pacific/rbd/rbd-operations/undefined)
* [Live\-Migration](https://docs.ceph.com/en/pacific/rbd/rbd-operations/undefined)
* [Persistent Read\-only Cache](https://docs.ceph.com/en/pacific/rbd/rbd-operations/undefined)
* [Persistent Write\-back Cache](https://docs.ceph.com/en/pacific/rbd/rbd-operations/undefined)
* [Encryption](https://docs.ceph.com/en/pacific/rbd/rbd-operations/undefined)
* [Config Settings \(librbd\)](https://docs.ceph.com/en/pacific/rbd/rbd-operations/undefined)
* [RBD Replay](https://docs.ceph.com/en/pacific/rbd/rbd-operations/undefined)
