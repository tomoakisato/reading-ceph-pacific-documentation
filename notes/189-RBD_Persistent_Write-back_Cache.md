# 189: RBD Persistent Write-back Cache

**クリップソース:** [189: RBD Persistent Write\-back Cache — Ceph Documentation](https://docs.ceph.com/en/pacific/rbd/rbd-persistent-write-back-cache/)

# RBD Persistent Write\-back Cache[¶](https://docs.ceph.com/en/pacific/rbd/rbd-persistent-write-back-cache/ "Permalink to this headline")

## Persistent Write\-back Cache[¶](https://docs.ceph.com/en/pacific/rbd/rbd-persistent-write-back-cache/ "Permalink to this headline")

persistentライトバックキャッシュは、librbdベースのRBDクライアントのために、持続的で耐障害性のあるライトバックキャッシュを提供します。

このキャッシュは、クラスタにフラッシュバックされる書き込みが常にクラッシュ一貫しているように、内部でチェックポイントを維持するlog\-orderedライトバック設計を使用します。クライアントキャッシュが完全に失われたとしても、ディスクイメージの一貫性は保たれますが、データは古くなったように見えるでしょう。

このキャッシュは、キャッシュデバイスとしてPMEMやSSDを使用することができます。

## Usage[¶](https://docs.ceph.com/en/pacific/rbd/rbd-persistent-write-back-cache/ "Permalink to this headline")

persistentライトバックキャッシュは、キャッシュデータを永続デバイスで管理します。設定されたディレクトリにキャッシュファイルを探して作成し、そのファイルにデータをキャッシュします。

persistentライトバックキャッシュは、排他的ロック機能がないと有効化できません。排他的ロックを獲得したときのみ、ライトバックキャッシュを有効にしようとします。

キャッシュは2つの異なるパーシステンスモードを提供します。persistent\-on\-write モードでは、キャッシュデバイスに永続化されたときにのみ書き込みが完了し、クラッシュの後でも読み出し可能です。persistent\-on\-flush モードでは、書き込みを完了するために呼び出し元のデータバッファを必要としなくなるとすぐに書き込みが完了しますが、クラッシュ後に書き込みが読み取れることは保証されません。フラッシュ要求を受信すると、データはキャッシュデバイスに永続化されます。

初期状態ではpersistent\-on\-writeモードであり、最初のフラッシュ要求を受信した後にpersistent\-on\-flushモードに切り替わります。

## Enable Persistent Write\-back Cache[¶](https://docs.ceph.com/en/pacific/rbd/rbd-persistent-write-back-cache/ "Permalink to this headline")

persistentライトバックキャッシュを有効にするには、以下のCeph設定を有効にする必要があります：

```
rbd persistent cache mode = {cache-mode}
rbd plugins = pwl_cache
```

{cache\-mode}の値には、rwl、ssd、disabledを指定します。デフォルトでは、キャッシュはdisabledになっています。

ここでは、キャッシュのコンフィギュレーション設定について説明します：

* **rbd\_persistent\_cache\_path**
* **rbd\_persistent\_cache\_size**
* **rbd\_persistent\_cache\_log\_periodic\_stats**

上記の設定は、ホストごと、プールごと、イメージごとなどに設定することができます。例えば、ホスト単位で設定する場合は、ホストのceph.confファイルの該当する[セクション](https://docs.ceph.com/en/pacific/rados/configuration/ceph-conf/#configuration-sections)にオーバーライドを追加してください。プール単位、イメージ単位などの設定は、rbdのconfig[コマンド](https://docs.ceph.com/en/pacific/man/8/rbd#commands)を参照してください。

### Cache Status[¶](https://docs.ceph.com/en/pacific/rbd/rbd-persistent-write-back-cache/ "Permalink to this headline")

persistentライトバックキャッシュは、排他的ロックの獲得時に有効になり、排他的ロックが解除されると閉じられます。キャッシュの状態を確認するには、rbd statusコマンドを使用します。

```
rbd status {pool-name}/{image-name}
```

キャッシュの状態（present、clean、キャッシュサイズ、場所など）が表示されます。現在、ステータスはキャッシュを開いたり閉じたりした時にのみ更新されるため、古く見えることがあります（例えば、キャッシュは実際はdirtyであるのに、cleanであると表示されるなど）。

For example:

```
$ rbd status rbd/foo
Watchers: none
Image cache state: {"present":"true","empty":"false","clean":"true","cache_type":"ssd","pwl_host":"sceph9","pwl_path":"/tmp/rbd-pwl.rbd.abcdef123456.pool","pwl_size":1073741824}
```

### Discard Cache[¶](https://docs.ceph.com/en/pacific/rbd/rbd-persistent-write-back-cache/ "Permalink to this headline")

キャッシュファイルを破棄するには、rbd image\-cache invalidate コマンドにプール名、イメージ名を指定します。

```
rbd image-cache invalidate {pool-name}/{image-name}
```

対応するイメージのキャッシュメタデータを削除し、キャッシュ機能を無効化し、ローカルキャッシュファイルが存在する場合はそれを削除するコマンドです。

For example:

```
$ rbd image-cache invalidate rbd/foo
```
