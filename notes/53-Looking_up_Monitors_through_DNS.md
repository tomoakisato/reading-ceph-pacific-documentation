# 53: Looking up Monitors through DNS

**クリップソース:** [53: Looking up Monitors through DNS — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/configuration/mon-lookup-dns/)

# Looking up Monitors through DNS[¶](https://docs.ceph.com/en/pacific/rados/configuration/mon-lookup-dns/ "Permalink to this headline")

バージョン11.0.0以降、RADOSはDNSによるMonitorの検索をサポートしています。

これにより、デーモンとクライアントは、ceph.conf設定ファイルにmon host設定ディレクティブを必要としません。

DNSのSRV TCPレコードを使用して、クライアントはモニターを検索できます。

これにより、クライアントとモニタの構成を少なくすることができます。DNSアップデートを使用すると、クライアントとデーモンは、モニタのトポロジーの変更を認識できます。

デフォルトでは、クライアントとデーモンは、mon\_dns\_srv\_name設定ディレクティブで設定されたceph\-monというTCPサービスを探します。

**mon dns srv name**

Description

モニタのホスト/アドレスをDNSに問い合わせるためのサービス名

Type

_String_

Default

_ceph\-mon_

## Example[¶](https://docs.ceph.com/en/pacific/rados/configuration/mon-lookup-dns/ "Permalink to this headline")

DNS検索ドメインをexample.comとした場合、DNSゾーンファイルには以下のような要素が含まれます。

まず、IPv4\(A\)またはIPv6\(AAAA\)のモニター用レコードを作成します。

```
mon1.example.com. AAAA 2001:db8::100
mon2.example.com. AAAA 2001:db8::200
mon3.example.com. AAAA 2001:db8::300
```

```
mon1.example.com. A 192.168.0.1
mon2.example.com. A 192.168.0.2
mon3.example.com. A 192.168.0.3
```

これらのレコードがあれば、3つのMonitorを指すceph\-monという名前のSRV TCPレコードを作成できます。

```
_ceph-mon._tcp.example.com. 60 IN SRV 10 20 6789 mon1.example.com.
_ceph-mon._tcp.example.com. 60 IN SRV 10 30 6789 mon2.example.com.
_ceph-mon._tcp.example.com. 60 IN SRV 20 50 6789 mon3.example.com.
```

現在、すべてのモニターはポート6789で動作しており、優先度はそれぞれ10、10、20、重みは20、30、50となっています。

モニタクライアントは、SRVレコードを参照してモニタを選択します。クラスタに同じ優先度の値を持つ複数のMonitor SRVレコードがある場合、クライアントとデーモンは、SRVのウェイトフィールドの値に比例してMonitorへの接続をロードバランスします。

上記の例では、クライアントとデーモンの約40％がmon1に接続し、約60％がmon2に接続することになります。しかし、そのどちらにも到達できない場合は、mon3がフォールバックとして再検討されます。
