# 106: ceph-clsinfo – show class object information

 # ceph\-clsinfo – show class object information[¶](https://docs.ceph.com/en/pacific/man/8/ceph-clsinfo/ "Permalink to this headline")

## Synopsis[¶](https://docs.ceph.com/en/pacific/man/8/ceph-clsinfo/ "Permalink to this headline")

**ceph\-clsinfo** \[ _options_ \] … _filename_

## Description[¶](https://docs.ceph.com/en/pacific/man/8/ceph-clsinfo/ "Permalink to this headline")

**ceph\-clsinfo** can show name, version, and architecture information about a specific class object.

## Options[¶](https://docs.ceph.com/en/pacific/man/8/ceph-clsinfo/ "Permalink to this headline")

`-n````, ``--name```[¶](https://docs.ceph.com/en/pacific/man/8/ceph-clsinfo/ "Permalink to this definition")Shows the class name

`-v````, ``--version```[¶](https://docs.ceph.com/en/pacific/man/8/ceph-clsinfo/ "Permalink to this definition")Shows the class version

`-a````, ``--arch```[¶](https://docs.ceph.com/en/pacific/man/8/ceph-clsinfo/ "Permalink to this definition")Shows the class architecture

## Availability[¶](https://docs.ceph.com/en/pacific/man/8/ceph-clsinfo/ "Permalink to this headline")

**ceph\-clsinfo** is part of Ceph, a massively scalable, open\-source, distributed storage system. Please refer to the Ceph documentation at [http://ceph.com/docs](https://docs.ceph.com/en/pacific/man/8/ceph-clsinfo/) for more information.

## See also[¶](https://docs.ceph.com/en/pacific/man/8/ceph-clsinfo/ "Permalink to this headline")

[ceph](https://docs.ceph.com/en/pacific/man/8/ceph-clsinfo/)\(8\)
