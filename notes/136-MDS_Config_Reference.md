# 136: MDS Config Reference

**クリップソース:** [136: MDS Config Reference — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/mds-config-ref/)

# MDS Config Reference[¶](https://docs.ceph.com/en/pacific/cephfs/mds-config-ref/ "Permalink to this headline")

**mds\_cache\_memory\_limit**

Description

MDSがキャッシュに適用するメモリの制限値

Type

64\-bit Integer Unsigned

Default

4G

**mds\_cache\_reservation**

Description

MDSキャッシュが維持するキャッシュ予約（メモリまたはinode）。MDSが予約に手をつけ始めると、キャッシュサイズが縮小して予約が回復するまで、クライアントのstateをリコールします。

Type

Float

Default

0.05

**mds\_cache\_mid**

Description

キャッシュLRUの新しいアイテムの挿入ポイント（先頭から）

Type

Float

Default

0.7

**mds\_dir\_commit\_ratio**

Description

Cephが\(部分更新ではなく\)完全更新を使用してコミットし始める、ダーティ・ディレクトリの割合

Type

Float

Default

0.5

**mds\_dir\_max\_commit\_size**

Description

Cephが小さなトランザクションに分割し始めるディレクトリ更新のサイズ\(MB\)

Type

32\-bit Integer

Default

10

**mds\_decay\_halflife**

Description

MDSキャッシュ温度の半減期

Type

Float

Default

5

**mds\_beacon\_interval**

Description

モニタに送信されるビーコンメッセージの頻度（秒）

Type

Float

Default

4

**mds\_beacon\_grace**

Description

CephがMDSのlaggyを宣言（し、MDSを交換）するビーコンがない期間

Type

Float

Default

15

**mds\_blocklist\_interval**

Description

OSDマップ内のfailed MDSのブロックリスト期間。注：これは、failed MDSデーモンがOSDMapブロックリストに留まる期間を制御します。これは、管理者が手動でブロックリストを作成したときにブロックリストに登録される期間には影響しません。たとえば、ceph osd blocklist addはデフォルトのブロックリスト時間を使用します。

Type

Float

Default

24.0\*60.0

**mds\_reconnect\_timeout**

Description

MDSの再起動時にクライアントが再接続するのを待つ期間（秒）

Type

Float

Default

45

**mds\_tick\_interval**

Description

MDSが内部の定期的なタスクを実行する頻度

Type

Float

Default

5

**mds\_dirstat\_min\_interval**

Description

再帰的な統計情報をツリー上に伝搬させないようにするための最小間隔（秒）

Type

Float

Default

1

**mds\_scatter\_nudge\_interval**

Description

dirstatの変化がどれだけ早く伝搬するか

Type

Float

Default

5

**mds\_client\_prealloc\_inos**

Description

クライアントセッションごとに事前割り当てを行うinode番号の数

Type

32\-bit Integer

Default

1000

**mds\_early\_reply**

Description

MDSがジャーナルにコミットする前に、クライアントにリクエスト結果を見せることを許可するかどうか

Type

Boolean

Default

true

**mds\_default\_dir\_hash**

Description

ディレクトリフラグメント間でファイルをハッシュするために使用する関数

Type

32\-bit Integer

Default

2 \(i.e., rjenkins\)

**mds\_log\_skip\_corrupt\_events**

Description

ジャーナル再生時に、破損したジャーナル・イベントをスキップするかどうか

Type

Boolean

Default

false

**mds\_log\_max\_events**

Description

トリミングを開始するジャーナル内のイベント数。制限を無効にするには \-1 に設定する

Type

32\-bit Integer

Default

\-1

**mds\_log\_max\_segments**

Description

トリミングを開始するジャーナル内のセグメント（オブジェクト）数。制限を無効にするには \-1 に設定する

Type

32\-bit Integer

Default

128

**mds\_bal\_sample\_interval**

Description

ディレクトリの温度をサンプリングする頻度（断片化の判断のため）

Type

Float

Default

3

**mds\_bal\_replicate\_threshold**

Description

Cephが他のノードへのメタデータのレプリケーションを開始する温度

Type

Float

Default

8000

**mds\_bal\_unreplicate\_threshold**

Description

Cephが他のノードへのメタデータのレプリケーションを停止する温度

Type

Float

Default

0

**mds\_bal\_split\_size**

Description

MDSがディレクトリのフラグメントをより小さなビットに分割するディレクトリサイズ

Type

32\-bit Integer

Default

10000

**mds\_bal\_split\_rd**

Description

Cephがディレクトリのフラグメント分割を開始するディレクトリread温度

Type

Float

Default

25000

**mds\_bal\_split\_wr**

Description

Cephがディレクトリのフラグメント分割を開始するディレクトリwrite温度

Type

Float

Default

10000

**mds\_bal\_split\_bits**

Description

ディレクトリフラグメントを分割するためのビット数

Type

32\-bit Integer

Default

3

**mds\_bal\_merge\_size**

Description

Cephが隣接するディレクトリフラグメントをマージしようとするディレクトリサイズ

Type

32\-bit Integer

Default

50

**mds\_bal\_interval**

Description

MDS間でワークロードが交換される頻度（秒）

Type

32\-bit Integer

Default

10

**mds\_bal\_fragment\_interval**

Description

フラグメントが分割またはマージの対象となり、フラグメントの変更が実行されるまでの遅延時間（秒）

Type

32\-bit Integer

Default

5

**mds\_bal\_fragment\_fast\_factor**

Description

フラグがスプリットサイズを超えた場合に（フラグメントインターバルをスキップして）即座にスプリットを実行する比率

Type

Float

Default

1.5

**mds\_bal\_fragment\_size\_max**

Description

新しいエントリがENOSPCで拒否され始めるフラグメントのサイズ

Type

32\-bit Integer

Default

100000

**mds\_bal\_idle\_threshold**

Description

Cephがサブツリーを親に移行し始める温度

Type

Float

Default

0

**mds\_bal\_max**

Description

Cephが停止するまでにバランサを実行する反復回数（テスト目的でのみ使用）

Type

32\-bit Integer

Default

\-1

**mds\_bal\_max\_until**

Description

Cephが停止するまでにバランサを実行する秒数（テスト目的でのみ使用）

Type

32\-bit Integer

Default

\-1

**mds\_bal\_mode**

Description

MDSの負荷を計算する方法

* 0 = ハイブリッド
* 1 = リクエストレートとレイテンシ
* 2 = CPU負荷

Type

32\-bit Integer

Default

0

**mds\_bal\_min\_rebalance**

Description

Cephがマイグレート開始するサブツリーの温度

Type

Float

Default

0.1

**mds\_bal\_min\_start**

Description

Cephがサブツリーを検索し始めるサブツリー温度

Type

Float

Default

0.2

**mds\_bal\_need\_min**

Description

受け入れるターゲットサブツリーサイズの最小部分

Type

Float

Default

0.8

**mds\_bal\_need\_max**

Description

受け入れるターゲットサブツリーサイズの最大部分

Type

Float

Default

1.2

**mds\_bal\_midchunk**

Description

Cephは、ターゲットサブツリーサイズに対してこの割合より大きいサブツリーをマイグレートする

Type

Float

Default

0.3

**mds\_bal\_minchunk**

Description

Cephは、ターゲットサブツリーサイズに対してこの割合より小さいサブツリーを無視する

Type

Float

Default

0.001

**mds\_bal\_target\_removal\_min**

Description

Cephが古いMDSターゲットをMDSマップから削除するまでのバランサーイテレーション回数

Type

32\-bit Integer

Default

5

**mds\_bal\_target\_removal\_max**

Description

Cephが古いMDSターゲットをMDSマップから削除するまでのバランサーイテレーション回数

Type

32\-bit Integer

Default

10

**mds\_replay\_interval**

Description

standby\-replayモード（"ホットスタンバイ"）時のジャーナル・ポール間隔

Type

Float

Default

1

**mds\_shutdown\_check**

Description

MDSシャットダウン時にキャッシュをポーリングする間隔

Type

32\-bit Integer

Default

0

**mds\_thrash\_exports**

Description

Cephは、ノード間でサブツリーをランダムにエクスポートする（テストのみ）

Type

32\-bit Integer

Default

0

**mds\_thrash\_fragments**

Description

Cephは、ディレクトリをランダムに断片化またはマージする

Type

32\-bit Integer

Default

0

**mds\_dump\_cache\_on\_map**

Description

Cephは、各MDSMap上のファイルにMDSキャッシュの内容をダンプする

Type

Boolean

Default

false

**mds\_dump\_cache\_after\_rejoin**

Description

Cephは、キャッシュのrejoining後（リカバリ時）、MDSキャッシュの内容をファイルにダンプする

Type

Boolean

Default

false

**mds\_verify\_scatter**

Description

Cephは、さまざまなscatter/gather不変条件が真であることをアサートする（開発者のみ対象）

Type

Boolean

Default

false

**mds\_debug\_scatterstat**

Description

Cephは、さまざまな再帰的stat不変条件が真であることをアサートする（開発者のみ対象）

Type

Boolean

Default

false

**mds\_debug\_frag**

Description

Cephは、ディレクトリの断片化の不変条件を検証する（開発者のみ対象）

Type

Boolean

Default

false

**mds\_debug\_auth\_pins**

Description

デバグ認証PINの不変条件（開発者のみ対象）

Type

Boolean

Default

false

**mds\_debug\_subtrees**

Description

デバッグサブツリーの不変条件（開発者のみ対象）

Type

Boolean

Default

false

**mds\_kill\_mdstable\_at**

Description

Cephは、MDSTableコードにMDS障害を注入する（開発者のみ対象）

Type

32\-bit Integer

Default

0

**mds\_kill\_export\_at**

Description

Cephは、サブツリーのエクスポートコードにMDS障害を注入する（開発者のみ対象）

Type

32\-bit Integer

Default

0

**mds\_kill\_import\_at**

Description

Cephは、サブツリーのインポートコードにMDS障害を注入する（開発者のみ対象）

Type

32\-bit Integer

Default

0

**mds\_kill\_link\_at**

Description

Cephは、ハードリンクコードにMDS障害を注入する（開発者のみ対象）

Type

32\-bit Integer

Default

0

**mds\_kill\_rename\_at**

Description

Cephは、リネームコードにMDS障害を注入する（開発者のみ対象）

Type

32\-bit Integer

Default

0

**mds\_wipe\_sessions**

Description

Cephは、起動時にすべてのクライアントセッションを削除する（テスト用）

Type

Boolean

Default

false

**mds\_wipe\_ino\_prealloc**

Description

Cephは起動時にinoのプレアロケーションメタデータを削除する（テスト用）

Type

Boolean

Default

false

**mds\_skip\_ino**

Description

起動時にスキップするinode番号の数（テスト用）

Type

32\-bit Integer

Default

0

**mds\_min\_caps\_per\_client**

Description

クライアントが保持できるCAPSの最小数

Type

Integer

Default

100

**mds\_max\_ratio\_caps\_per\_client**

Description

MDSキャッシュプレッシャー中にリコールできるカレントCAPSの最大比率

Type

Float

Default

0.8
