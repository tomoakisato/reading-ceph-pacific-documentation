# 312: MDS Autoscaler Module

**クリップソース:** [312: MDS Autoscaler Module — Ceph Documentation](https://docs.ceph.com/en/pacific/mgr/mds_autoscaler/)

# MDS Autoscaler Module[¶](https://docs.ceph.com/en/pacific/mgr/mds_autoscaler/ "Permalink to this headline")

MDS Autoscaler Moduleは、十分なMDSデーモンが利用できるようにファイルシステムを監視しています。これは、MDSサービスのオーケストレーターバックエンドの配置仕様を調整することで機能します。有効にするには、以下を使用します：

```
ceph mgr module enable mds_autoscaler
```

このモジュールは、以下のファイルシステム設定を監視し、プレイスメントカウントの調整を通知します：

* max\_mds ファイルシステム設定
* standby\_count\_wantedファイルシステム設定

Cephモニターデーモンは、これらの設定に従ってMDSをプロモートまたは停止する責任を負っています。mds\_autoscalerは、オーケストレータによって生成されるMDSの数を調整するだけです。
