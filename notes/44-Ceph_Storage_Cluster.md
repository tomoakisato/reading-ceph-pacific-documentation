# 44: Ceph Storage Cluster

**クリップソース:** [44: Ceph Storage Cluster — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/)

**[Report a Documentation Bug](https://docs.ceph.com/en/pacific/rados/)**

# Ceph Storage Cluster[¶](https://docs.ceph.com/en/pacific/rados/ "Permalink to this headline")

[Ceph Storage Cluster](https://docs.ceph.com/en/pacific/rados/)は、すべてのCeph導入の基盤となるものです。RADOSに基づき、Ceph Storage Clusterは2種類のデーモンで構成されています。[Ceph OSD Daemon](https://docs.ceph.com/en/pacific/rados/)\(OSD\)はデータをオブジェクトとしてストレージノードに保存し、[Ceph Monitor](https://docs.ceph.com/en/pacific/rados/)\(MON\)はクラスタマップのマスターコピーを維持します。Ceph Storage Clusterには、数千台のストレージノードが含まれる場合があります。最小限のシステムには、データレプリケーションのために、少なくとも1つのCeph Monitorと2つのCeph OSD Daemonがあります。

Ceph File System、Ceph Object Storage、およびCeph Block Devicesは、Ceph Storage Clusterからデータを読み取り、Ceph Storage Clusterにデータを書き込みます。

### Config and Deploy

Ceph Storage Clustersにはいくつかの必須設定がありますが、ほとんどの構成設定にはデフォルト値があります。一般的な展開では、展開ツールを使用してクラスタを定義し、モニタをブートストラップします。cephadmの詳細については、「[Deployment](https://docs.ceph.com/en/pacific/rados/)」を参照してください。 

* [Storage devicesConfiguring CephCommon SettingsNetworksMonitorsAuthenticationOSDsHeartbeatsLogs / DebuggingExample ceph.confRunning Multiple Clusters \(DEPRECATED\)Network SettingsMessenger v2 protocolAuth SettingsMonitor SettingsLooking up Monitors through DNSHeartbeat SettingsOSD SettingsDmClock SettingsBlueStore SettingsFileStore SettingsJournal SettingsPool, PG & CRUSH SettingsMessaging SettingsGeneral Settings](https://docs.ceph.com/en/pacific/rados/)
    [Configuration](https://docs.ceph.com/en/pacific/rados/)
* [Compatibility and StabilityDeploying a new Ceph clusterConverting an existing cluster to cephadmHost ManagementService ManagementUpgrading CephCephadm operationsClient SetupTroubleshootingCephadm Feature Planning](https://docs.ceph.com/en/pacific/rados/)
    [Deployment](https://docs.ceph.com/en/pacific/rados/)

### Operations

Ceph Storage Clusterを展開したら、クラスタの運用を開始できます。

* [Operating a ClusterHealth checksMonitoring a ClusterMonitoring OSDs and PGsUser ManagementRepairing PG inconsistenciesData Placement OverviewPoolsErasure codeCache TieringPlacement GroupsBalancerUsing the pg\-upmapCRUSH MapsManually editing a CRUSH MapStretch ClustersConfigure Monitor Election StrategiesAdding/Removing OSDsAdding/Removing MonitorsDevice ManagementBlueStore MigrationCommand ReferenceThe Ceph CommunityTroubleshooting MonitorsTroubleshooting OSDsTroubleshooting PGsLogging and DebuggingCPU ProfilingMemory Profiling](https://docs.ceph.com/en/pacific/rados/)
    [Operations](https://docs.ceph.com/en/pacific/rados/)
* [Man Pages](https://docs.ceph.com/en/pacific/rados/)

### APIs

ほとんどのCephの導入では、[Ceph Block Devices](https://docs.ceph.com/en/pacific/rados/), [Ceph Object Storage](https://docs.ceph.com/en/pacific/rados/) , [Ceph File System](https://docs.ceph.com/en/pacific/rados/).を使用します。また、Ceph Storage Clusterに直接接続するアプリケーションを開発することもできます。

* [Introduction to libradoslibrados \(C\)librados \(C\+\+\)librados \(Python\)libcephsqlite \(SQLite\)object class](https://docs.ceph.com/en/pacific/rados/)
    [APIs](https://docs.ceph.com/en/pacific/rados/)

