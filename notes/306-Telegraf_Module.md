# 306: Telegraf Module

 # Telegraf Module[¶](https://docs.ceph.com/en/pacific/mgr/telegraf/ "Permalink to this headline")

The Telegraf module collects and sends statistics series to a Telegraf agent.

The Telegraf agent can buffer, aggregate, parse and process the data before sending it to an output which can be InfluxDB, ElasticSearch and many more.

Currently the only way to send statistics to Telegraf from this module is to use the socket listener. The module can send statistics over UDP, TCP or a UNIX socket.

The Telegraf module was introduced in the 13.x _Mimic_ release.

## Enabling[¶](https://docs.ceph.com/en/pacific/mgr/telegraf/ "Permalink to this headline")

To enable the module, use the following command:

```
ceph mgr module enable telegraf
```

If you wish to subsequently disable the module, you can use the corresponding_disable_ command:

```
ceph mgr module disable telegraf
```

## Configuration[¶](https://docs.ceph.com/en/pacific/mgr/telegraf/ "Permalink to this headline")

For the telegraf module to send statistics to a Telegraf agent it is required to configure the address to send the statistics to.

Set configuration values using the following command:

```
ceph telegraf config-set <key> <value>
```

The most important settings are `address` and `interval`.

For example, a typical configuration might look like this:

```
ceph telegraf config-set address udp://:8094
ceph telegraf config-set interval 10
```

The default values for these configuration keys are:

* address: unixgram:///tmp/telegraf.sock
* interval: 15

## Socket Listener[¶](https://docs.ceph.com/en/pacific/mgr/telegraf/ "Permalink to this headline")

The module only supports sending data to Telegraf through the socket listener of the Telegraf module using the Influx data format.

A typical Telegraf configuration might be:

> \[\[inputs.socket\_listener\]\] \# service\_address = “[tcp://:8094](https://docs.ceph.com/en/pacific/mgr/telegraf/)” \# service\_address = “[tcp://127.0.0.1:http](https://docs.ceph.com/en/pacific/mgr/telegraf/)” \# service\_address = “tcp4://:8094” \# service\_address = “tcp6://:8094” \# service\_address = “tcp6://\[2001:db8::1\]:8094” service\_address = “udp://:8094” \# service\_address = “udp4://:8094” \# service\_address = “udp6://:8094” \# service\_address = “unix:///tmp/telegraf.sock” \# service\_address = “unixgram:///tmp/telegraf.sock” data\_format = “influx”

In this case the address configuration option for the module would need to be set to:

> udp://:8094

Refer to the Telegraf documentation for more configuration options.
