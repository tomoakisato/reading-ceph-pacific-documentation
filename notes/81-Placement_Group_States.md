# 81: Placement Group States

**クリップソース:** [81: Placement Group States — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/operations/pg-states/)

# Placement Group States[¶](https://docs.ceph.com/en/pacific/rados/operations/pg-states/ "Permalink to this headline")

クラスタの状態を確認する場合\(例: ceph \-wまたはceph \-sの実行\)、CephはPGの状態について報告します。PGは1つまたは複数の状態を持ちます。PGマップのPGの最適な状態は、active\+cleanです。

**_creating_**

CephはまだPGを作成中です

**_activating_**

PGは、ピアリングされていますが、まだアクティブではありません

**_active_**

Cephは、PGへの要求を処理します

**_clean_**

Cephは、PG内のすべてのオブジェクトを正しい回数レプリケートしました

**_down_**

必要なデータを持つレプリカがダウンしているため、PGがオフラインになっています

**_laggy_**

レプリカがプライマリからの新しいリースをタイムリーに認識しないため、IOが一時的に停止しています

**_wait_**

このPGのOSDのセットはちょうど変更され、IOは前のインターバルのリースが切れるまで一時的に停止されます

**_scrubbing_**

Cephは、PGのメタデータに矛盾がないかをチェックしています

**_deep_**

Cephは、保存されているチェックサムに対してPGデータをチェックしています

**_degraded_**

Cephは、PG内の一部のオブジェクトをまだ正しい回数レプリケートしていません

**_inconsistent_**

Cephは、PG内のオブジェクトの1つまたは複数のレプリカで不整合を検出します\(オブジェクトのサイズが間違っている、リカバリ終了後に1つのレプリカからオブジェクトが消失しているなど\)

**_peering_**

PGはピアリング中です

**_repair_**

CephはPGをチェックし、見つかった不整合を\(可能であれば\)修復しています

**_recovering_**

Cephは、オブジェクトとそのレプリカをマイグレーション/同期しています

**_forced\_recovery_**

そのPGの高い回復優先度は、ユーザによって強制されます

**_recovery\_wait_**

PGは回復開始を並んで待っています

**_recovery\_toofull_**

デスティネーションOSDがフルレシオを超えているため、リカバリオペレーションを待機しています

**_recovery\_unfound_**

オブジェクトが見つからなかったため、リカバリが停止しました

**_backfilling_**

Cephは、最近の操作のログから同期が必要なコンテンツを推測するのではなく、PGのコンテンツ全体をスキャンして同期しています。バックフィルはリカバリの特殊なケースです

**_forced\_backfill_**

そのPGのバックフィル優先度を高くすることが、ユーザによって強制されています

**_backfill\_wait_**

PGはバックフィル開始を並んで待っています

**_backfill\_toofull_**

デスティネーションOSDがバックフィル・フル・レシオを超えているため、バックフィル動作を待機しています

**_backfill\_unfound_**

unfoundオブジェクトのためバックフィル停止

**_incomplete_**

Cephは、PGが、発生した可能性のある書き込みに関する情報を欠いているか、健全なコピーを持っていないことを検出します。この状態が表示された場合、必要な情報を含む可能性のある故障したOSDを起動してみてください。ECプールの場合、min\_sizeを一時的に減らすと回復できる場合があります。

**_stale_**

PGはunknownな状態です \- PGマッピングが変更されてから、モニタはそのグループの更新を受信していません

**_remapped_**

PGは、CRUSHが指定したものとは異なるOSDセットに一時的にマッピングされます

**_undersized_**

PGのコピー数が設定されたプールレプリケーションレベルより少ない

**_peered_**

PGはピアリングしましたが、プールの設定されたmin\_sizeパラメータに到達するのに十分なコピーを持っていないため、クライアントIOを提供することができません。 この状態でリカバリが発生し、最終的にmin\_sizeまで回復する可能性があります。

**_snaptrim_**

スナップをトリミングする

**_snaptrim\_wait_**

スナップをトリミングするためにキューに入れられました

**_snaptrim\_error_**

スナップのトリミングでエラーが発生しました

**_unknown_**

ceph\-mgrは起動してから、まだOSDからPGの状態に関する情報を受け取っていません
