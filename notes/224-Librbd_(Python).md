# 224: Librbd (Python)

 # Librbd \(Python\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this headline")

The rbd python module provides file\-like access to RBD images.

## Example: Creating and writing to an image[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this headline")

To use rbd, you must first connect to RADOS and open an IO context:

```
cluster = rados.Rados(conffile='my_ceph.conf')
cluster.connect()
ioctx = cluster.open_ioctx('mypool')
```

Then you instantiate an :class:rbd.RBD object, which you use to create the image:

```
rbd_inst = rbd.RBD()
size = 4 * 1024**3  # 4 GiB
rbd_inst.create(ioctx, 'myimage', size)
```

To perform I/O on the image, you instantiate an :class:rbd.Image object:

```
image = rbd.Image(ioctx, 'myimage')
data = 'foo' * 200
image.write(data, 0)
```

This writes ‘foo’ to the first 600 bytes of the image. Note that data cannot be :type:unicode \- Librbd does not know how to deal with characters wider than a :c:type:char.

In the end, you will want to close the image, the IO context and the connection to RADOS:

```
image.close()
ioctx.close()
cluster.shutdown()
```

To be safe, each of these calls would need to be in a separate :finally block:

```
cluster = rados.Rados(conffile='my_ceph_conf')
try:
    cluster.connect()
    ioctx = cluster.open_ioctx('my_pool')
    try:
        rbd_inst = rbd.RBD()
        size = 4 * 1024**3  # 4 GiB
        rbd_inst.create(ioctx, 'myimage', size)
        image = rbd.Image(ioctx, 'myimage')
        try:
            data = 'foo' * 200
            image.write(data, 0)
        finally:
            image.close()
    finally:
        ioctx.close()
finally:
    cluster.shutdown()
```

This can be cumbersome, so the `Rados`, `Ioctx`, and`Image` classes can be used as context managers that close/shutdown automatically \(see [**PEP 343**](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/)\). Using them as context managers, the above example becomes:

```
with rados.Rados(conffile='my_ceph.conf') as cluster:
    with cluster.open_ioctx('mypool') as ioctx:
        rbd_inst = rbd.RBD()
        size = 4 * 1024**3  # 4 GiB
        rbd_inst.create(ioctx, 'myimage', size)
        with rbd.Image(ioctx, 'myimage') as image:
            data = 'foo' * 200
            image.write(data, 0)
```

## API Reference[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this headline")

This module is a thin wrapper around librbd.

It currently provides all the synchronous methods of librbd that do not use callbacks.

Error codes from librbd are turned into exceptions that subclass`Error`. Almost all methods may raise `Error`\(the base class of all rbd exceptions\), `PermissionError`and `IOError`, in addition to those documented for the method.

_class_ `rbd.``Image`\(_ioctx_, _name=None_, _snapshot=None_, _read\_only=False_, _image\_id=None_, _\_oncomplete=None_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")This class represents an RBD image. It is used to perform I/O on the image and interact with snapshots.

**Note**: Any method of this class may raise `ImageNotFound`if the image has been deleted.

`close`\(_self_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Release the resources used by this image object.

After this is called, this object should not be used.

`require_not_closed`\(_self_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Checks if the Image is not closed

Raises`InvalidArgument`

_class_ `rbd.``RBD`[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")This class wraps librbd CRUD functions.

`aio_open_image`\(_self_, _oncomplete_, _ioctx_, _name=None_, _snapshot=None_, _read\_only=False_, _image\_id=None_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Asynchronously open the image at the given snapshot. Specify either name or id, otherwise `InvalidArgument` is raised.

oncomplete will be called with the created Image object as well as the completion:

oncomplete\(completion, image\)

If a snapshot is specified, the image will be read\-only, unless`Image.set_snap()` is called later.

If read\-only mode is used, metadata for the [`Image`](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "rbd.Image")object \(such as which snapshots exist\) may become obsolete. See the C api for more details.

To clean up from opening the image, [`Image.close()`](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "rbd.Image.close") or`Image.aio_close()` should be called.

Parameters
* **oncomplete** \(_completion_\) – what to do when open is complete
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool the image is in
* **name** \(_str_\) – the name of the image
* **snapshot** – which snapshot to read from
* **read\_only** \(_bool_\) – whether to open the image in read\-only mode
* **image\_id** \(_str_\) – the id of the image

Returns`Completion` \- the completion object

`clone`\(_self_, _p\_ioctx_, _p\_name_, _p\_snapname_, _c\_ioctx_, _c\_name_, _features=None_, _order=None_, _stripe\_unit=None_, _stripe\_count=None_, _data\_pool=None_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Clone a parent rbd snapshot into a COW sparse child.

Parameters
* **p\_ioctx** – the parent context that represents the parent snap
* **p\_name** – the parent image name
* **p\_snapname** – the parent image snapshot name
* **c\_ioctx** – the child context that represents the new clone
* **c\_name** – the clone \(child\) name
* **features** \(_int_\) – bitmask of features to enable; if set, must include layering
* **order** \(_int_\) – the image is split into \(2\*\*order\) byte objects
* **stripe\_unit** \(_int_\) – stripe unit in bytes \(default None to let librbd decide\)
* **stripe\_count** \(_int_\) – objects to stripe over before looping
* **data\_pool** \(_str_\) – optional separate pool for data blocks

Raises`TypeError`

Raises`InvalidArgument`

Raises`ImageExists`

Raises`FunctionNotSupported`

Raises`ArgumentOutOfRange`

`config_get`\(_self_, _ioctx_, _key_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Get a pool\-level configuration override.

Parameters
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool is read
* **key** \(_str_\) – key

Returnsstr \- value

`config_list`\(_self_, _ioctx_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")List pool\-level config overrides.

Returns`ConfigPoolIterator`

`config_remove`\(_self_, _ioctx_, _key_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Remove a pool\-level configuration override.

Parameters
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool is read
* **key** \(_str_\) – key

Returnsstr \- value

`config_set`\(_self_, _ioctx_, _key_, _value_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Get a pool\-level configuration override.

Parameters
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool is read
* **key** \(_str_\) – key
* **value** \(_str_\) – value

`create`\(_self_, _ioctx_, _name_, _size_, _order=None_, _old\_format=False_, _features=None_, _stripe\_unit=None_, _stripe\_count=None_, _data\_pool=None_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Create an rbd image.

Parameters
* **ioctx** \(`rados.Ioctx`\) – the context in which to create the image
* **name** \(_str_\) – what the image is called
* **size** \(_int_\) – how big the image is in bytes
* **order** \(_int_\) – the image is split into \(2\*\*order\) byte objects
* **old\_format** \(_bool_\) – whether to create an old\-style image that is accessible by old clients, but can’t use more advanced features like layering.
* **features** \(_int_\) – bitmask of features to enable
* **stripe\_unit** \(_int_\) – stripe unit in bytes \(default None to let librbd decide\)
* **stripe\_count** \(_int_\) – objects to stripe over before looping
* **data\_pool** \(_str_\) – optional separate pool for data blocks

Raises`ImageExists`

Raises`TypeError`

Raises`InvalidArgument`

Raises`FunctionNotSupported`

`features_from_string`\(_self_, _str\_features_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Get features bitmask from str, if str\_features is empty, it will return RBD\_FEATURES\_DEFAULT.

Parameters**str\_features** \(_str_\) – feature str

Returnsint \- the features bitmask of the image

Raises`InvalidArgument`

`features_to_string`\(_self_, _features_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Convert features bitmask to str.

Parameters**features** \(_int_\) – feature bitmask

Returnsstr \- the features str of the image

Raises`InvalidArgument`

`group_create`\(_self_, _ioctx_, _name_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Create a group.

Parameters
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool is used
* **name** \(_str_\) – the name of the group

Raises`ObjectExists`

Raises`InvalidArgument`

Raises`FunctionNotSupported`

`group_list`\(_self_, _ioctx_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")List groups.

Parameters**ioctx** \(`rados.Ioctx`\) – determines which RADOS pool is read

Returnslist – a list of groups names

Raises`FunctionNotSupported`

`group_remove`\(_self_, _ioctx_, _name_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Delete an RBD group. This may take a long time, since it does not return until every image in the group has been removed from the group.

Parameters
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool the group is in
* **name** \(_str_\) – the name of the group to remove

Raises`ObjectNotFound`

Raises`InvalidArgument`

Raises`FunctionNotSupported`

`group_rename`\(_self_, _ioctx_, _src_, _dest_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Rename an RBD group.

Parameters
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool the group is in
* **src** \(_str_\) – the current name of the group
* **dest** \(_str_\) – the new name of the group

Raises`ObjectExists`

Raises`ObjectNotFound`

Raises`InvalidArgument`

Raises`FunctionNotSupported`

`list`\(_self_, _ioctx_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")List image names.

Parameters**ioctx** \(`rados.Ioctx`\) – determines which RADOS pool is read

Returnslist – a list of image names

`list2`\(_self_, _ioctx_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Iterate over the images in the pool.

Parameters**ioctx** \(`rados.Ioctx`\) – determines which RADOS pool the image is in

Returns`ImageIterator`

`migration_abort`\(_self_, _ioctx_, _image\_name_, _on\_progress=None_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Cancel a previously started but interrupted migration.

Parameters
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool the image is in
* **image\_name** \(_str_\) – the name of the image
* **on\_progress** \(_callback function_\) – optional progress callback function

Raises`ImageNotFound`

`migration_commit`\(_self_, _ioctx_, _image\_name_, _on\_progress=None_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Commit an executed RBD image migration.

Parameters
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool the image is in
* **image\_name** \(_str_\) – the name of the image
* **on\_progress** \(_callback function_\) – optional progress callback function

Raises`ImageNotFound`

`migration_execute`\(_self_, _ioctx_, _image\_name_, _on\_progress=None_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Execute a prepared RBD image migration.

Parameters
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool the image is in
* **image\_name** \(_str_\) – the name of the image
* **on\_progress** \(_callback function_\) – optional progress callback function

Raises`ImageNotFound`

`migration_prepare`\(_self_, _ioctx_, _image\_name_, _dest\_ioctx_, _dest\_image\_name_, _features=None_, _order=None_, _stripe\_unit=None_, _stripe\_count=None_, _data\_pool=None_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Prepare an RBD image migration.

Parameters
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool the image is in
* **image\_name** – the current name of the image
* **dest\_ioctx** \(`rados.Ioctx`\) – determines which pool to migration into
* **dest\_image\_name** \(_str_\) – the name of the destination image \(may be the same image\)
* **features** \(_int_\) – bitmask of features to enable; if set, must include layering
* **order** \(_int_\) – the image is split into \(2\*\*order\) byte objects
* **stripe\_unit** \(_int_\) – stripe unit in bytes \(default None to let librbd decide\)
* **stripe\_count** \(_int_\) – objects to stripe over before looping
* **data\_pool** \(_str_\) – optional separate pool for data blocks

Raises`TypeError`

Raises`InvalidArgument`

Raises`ImageExists`

Raises`FunctionNotSupported`

Raises`ArgumentOutOfRange`

`migration_prepare_import`\(_self_, _source\_spec_, _dest\_ioctx_, _dest\_image\_name_, _features=None_, _order=None_, _stripe\_unit=None_, _stripe\_count=None_, _data\_pool=None_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Prepare an RBD image migration.

Parameters
* **source\_spec** \(_str_\) – JSON\-encoded source\-spec
* **dest\_ioctx** \(`rados.Ioctx`\) – determines which pool to migration into
* **dest\_image\_name** \(_str_\) – the name of the destination image \(may be the same image\)
* **features** \(_int_\) – bitmask of features to enable; if set, must include layering
* **order** \(_int_\) – the image is split into \(2\*\*order\) byte objects
* **stripe\_unit** \(_int_\) – stripe unit in bytes \(default None to let librbd decide\)
* **stripe\_count** \(_int_\) – objects to stripe over before looping
* **data\_pool** \(_str_\) – optional separate pool for data blocks

Raises`TypeError`

Raises`InvalidArgument`

Raises`ImageExists`

Raises`FunctionNotSupported`

Raises`ArgumentOutOfRange`

`migration_status`\(_self_, _ioctx_, _image\_name_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Return RBD image migration status.

Parameters
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool the image is in
* **image\_name** \(_str_\) – the name of the image

Returns

dict \- contains the following keys:

* `source_pool_id` \(int\) \- source image pool id
* `source_pool_namespace` \(str\) \- source image pool namespace
* `source_image_name` \(str\) \- source image name
* `source_image_id` \(str\) \- source image id
* `dest_pool_id` \(int\) \- destination image pool id
* `dest_pool_namespace` \(str\) \- destination image pool namespace
* `dest_image_name` \(str\) \- destination image name
* `dest_image_id` \(str\) \- destination image id
* `state` \(int\) \- current migration state
* `state_description` \(str\) \- migration state description

Raises`ImageNotFound`

`mirror_image_info_list`\(_self_, _ioctx_, _mode\_filter=None_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Iterate over the mirror image instance ids of a pool.

Parameters
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool is read
* **mode\_filter** – list images in this image mirror mode

Returns`MirrorImageInfoIterator`

`mirror_image_instance_id_list`\(_self_, _ioctx_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Iterate over the mirror image instance ids of a pool.

Parameters**ioctx** \(`rados.Ioctx`\) – determines which RADOS pool is read

Returns`MirrorImageInstanceIdIterator`

`mirror_image_status_list`\(_self_, _ioctx_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Iterate over the mirror image statuses of a pool.

Parameters**ioctx** \(`rados.Ioctx`\) – determines which RADOS pool is read

Returns`MirrorImageStatusIterator`

`mirror_image_status_summary`\(_self_, _ioctx_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Get mirror image status summary of a pool.

Parameters**ioctx** \(`rados.Ioctx`\) – determines which RADOS pool is read

Returnslist \- a list of \(state, count\) tuples

`mirror_mode_get`\(_self_, _ioctx_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Get pool mirror mode.

Parameters**ioctx** \(`rados.Ioctx`\) – determines which RADOS pool is read

Returnsint \- pool mirror mode

`mirror_mode_set`\(_self_, _ioctx_, _mirror\_mode_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Set pool mirror mode.

Parameters
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool is written
* **mirror\_mode** \(_int_\) – mirror mode to set

`mirror_peer_add`\(_self_, _ioctx_, _site\_name_, _client\_name_, _direction=RBD\_MIRROR\_PEER\_DIRECTION\_RX\_TX_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Add mirror peer.

Parameters
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool is used
* **site\_name** \(_str_\) – mirror peer site name
* **client\_name** \(_str_\) – mirror peer client name
* **direction** \(_int_\) – the direction of the mirroring

Returnsstr \- peer uuid

`mirror_peer_bootstrap_create`\(_self_, _ioctx_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Creates a new RBD mirroring bootstrap token for an external cluster.

Parameters**ioctx** \(`rados.Ioctx`\) – determines which RADOS pool is written

Returnsstr \- bootstrap token

`mirror_peer_bootstrap_import`\(_self_, _ioctx_, _direction_, _token_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Import a bootstrap token from an external cluster to auto\-configure the mirror peer.

Parameters
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool is written
* **direction** \(_int_\) – mirror peer direction
* **token** \(_str_\) – bootstrap token

`mirror_peer_get_attributes`\(_self_, _ioctx_, _uuid_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Get optional mirror peer attributes

Parameters
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool is written
* **uuid** \(_str_\) – uuid of the mirror peer

Returns

dict \- contains the following keys:

* `mon_host` \(str\) \- monitor addresses
* `key` \(str\) \- CephX key

`mirror_peer_list`\(_self_, _ioctx_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Iterate over the peers of a pool.

Parameters**ioctx** \(`rados.Ioctx`\) – determines which RADOS pool is read

Returns`MirrorPeerIterator`

`mirror_peer_remove`\(_self_, _ioctx_, _uuid_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Remove mirror peer.

Parameters
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool is used
* **uuid** \(_str_\) – peer uuid

`mirror_peer_set_attributes`\(_self_, _ioctx_, _uuid_, _attributes_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Set optional mirror peer attributes

Parameters
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool is written
* **uuid** \(_str_\) – uuid of the mirror peer
* **attributes** \(_dict_\) – ‘mon\_host’ and ‘key’ attributes

`mirror_peer_set_client`\(_self_, _ioctx_, _uuid_, _client\_name_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Set mirror peer client name

Parameters
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool is written
* **uuid** \(_str_\) – uuid of the mirror peer
* **client\_name** \(_str_\) – client name of the mirror peer to set

`mirror_peer_set_cluster`\(_self_, _ioctx_, _uuid_, _cluster\_name_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")`mirror_peer_set_name`\(_self_, _ioctx_, _uuid_, _site\_name_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Set mirror peer site name

Parameters
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool is written
* **uuid** \(_str_\) – uuid of the mirror peer
* **site\_name** \(_str_\) – site name of the mirror peer to set

`mirror_site_name_get`\(_self_, _rados_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Get the local cluster’s friendly site name

Parameters**rados** – cluster connection

Returnsstr \- local site name

`mirror_site_name_set`\(_self_, _rados_, _site\_name_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Set the local cluster’s friendly site name

Parameters
* **rados** – cluster connection
* **site\_name** – friendly site name

`mirror_uuid_get`\(_self_, _ioctx_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Get pool mirror uuid

Parameters**ioctx** \(`rados.Ioctx`\) – determines which RADOS pool is read

Returnsste \- pool mirror uuid

`namespace_create`\(_self_, _ioctx_, _name_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Create an RBD namespace within a pool

Parameters
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool
* **name** \(_str_\) – namespace name

`namespace_exists`\(_self_, _ioctx_, _name_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Verifies if a namespace exists within a pool

Parameters
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool
* **name** \(_str_\) – namespace name

Returnsbool \- true if namespace exists

`namespace_list`\(_self_, _ioctx_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")List all namespaces within a pool

Parameters**ioctx** \(`rados.Ioctx`\) – determines which RADOS pool

Returnslist \- collection of namespace names

`namespace_remove`\(_self_, _ioctx_, _name_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Remove an RBD namespace from a pool

Parameters
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool
* **name** \(_str_\) – namespace name

`pool_init`\(_self_, _ioctx_, _force_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Initialize an RBD pool :param ioctx: determines which RADOS pool :type ioctx: `rados.Ioctx`:param force: force init :type force: bool

`pool_metadata_get`\(_self_, _ioctx_, _key_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Get pool metadata for the given key.

Parameters
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool is read
* **key** \(_str_\) – metadata key

Returnsstr \- metadata value

`pool_metadata_list`\(_self_, _ioctx_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")List pool metadata.

Returns`PoolMetadataIterator`

`pool_metadata_remove`\(_self_, _ioctx_, _key_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Remove pool metadata for the given key.

Parameters
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool is read
* **key** \(_str_\) – metadata key

Returnsstr \- metadata value

`pool_metadata_set`\(_self_, _ioctx_, _key_, _value_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Set pool metadata for the given key.

Parameters
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool is read
* **key** \(_str_\) – metadata key
* **value** \(_str_\) – metadata value

`pool_stats_get`\(_self_, _ioctx_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Return RBD pool stats

Parameters**ioctx** \(`rados.Ioctx`\) – determines which RADOS pool

Returns

dict \- contains the following keys:

* `image_count` \(int\) \- image count
* `image_provisioned_bytes` \(int\) \- image total HEAD provisioned bytes
* `image_max_provisioned_bytes` \(int\) \- image total max provisioned bytes
* `image_snap_count` \(int\) \- image snap count
* `trash_count` \(int\) \- trash image count
* `trash_provisioned_bytes` \(int\) \- trash total HEAD provisioned bytes
* `trash_max_provisioned_bytes` \(int\) \- trash total max provisioned bytes
* `trash_snap_count` \(int\) \- trash snap count

`remove`\(_self_, _ioctx_, _name_, _on\_progress=None_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Delete an RBD image. This may take a long time, since it does not return until every object that comprises the image has been deleted. Note that all snapshots must be deleted before the image can be removed. If there are snapshots left,`ImageHasSnapshots` is raised. If the image is still open, or the watch from a crashed client has not expired,`ImageBusy` is raised.

Parameters
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool the image is in
* **name** \(_str_\) – the name of the image to remove
* **on\_progress** \(_callback function_\) – optional progress callback function

Raises`ImageNotFound`, `ImageBusy`,`ImageHasSnapshots`

`rename`\(_self_, _ioctx_, _src_, _dest_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Rename an RBD image.

Parameters
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool the image is in
* **src** \(_str_\) – the current name of the image
* **dest** \(_str_\) – the new name of the image

Raises`ImageNotFound`, `ImageExists`

`trash_get`\(_self_, _ioctx_, _image\_id_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Retrieve RBD image info from trash.

Parameters
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool the image is in
* **image\_id** \(_str_\) – the id of the image to restore

Returns

dict \- contains the following keys:

* `id` \(str\) \- image id
* `name` \(str\) \- image name
* `source` \(str\) \- source of deletion
* `deletion_time` \(datetime\) \- time of deletion
* `deferment_end_time` \(datetime\) \- time that an image is allowed to be removed from trash

Raises`ImageNotFound`

`trash_list`\(_self_, _ioctx_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")List all entries from trash.

Parameters**ioctx** \(`rados.Ioctx`\) – determines which RADOS pool the image is in

Returns`TrashIterator`

`trash_move`\(_self_, _ioctx_, _name_, _delay=0_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Move an RBD image to the trash.

Parameters
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool the image is in
* **name** \(_str_\) – the name of the image to remove
* **delay** \(_int_\) – time delay in seconds before the image can be deleted from trash

Raises`ImageNotFound`

`trash_purge`\(_self_, _ioctx_, _expire\_ts=None_, _threshold=\- 1_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Delete RBD images from trash in bulk.

By default it removes images with deferment end time less than now.

The timestamp is configurable, e.g. delete images that have expired a week ago.

If the threshold is used it deletes images until X% pool usage is met.

Parameters
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool the image is in
* **expire\_ts** \(_datetime_\) – timestamp for images to be considered as expired \(UTC\)
* **threshold** \(_float_\) – percentage of pool usage to be met \(0 to 1\)

`trash_remove`\(_self_, _ioctx_, _image\_id_, _force=False_, _on\_progress=None_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Delete an RBD image from trash. If image deferment time has not expired `PermissionError` is raised.

Parameters
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool the image is in
* **image\_id** \(_str_\) – the id of the image to remove
* **force** \(_bool_\) – force remove even if deferment time has not expired
* **on\_progress** \(_callback function_\) – optional progress callback function

Raises`ImageNotFound`, `PermissionError`

`trash_restore`\(_self_, _ioctx_, _image\_id_, _name_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Restore an RBD image from trash.

Parameters
* **ioctx** \(`rados.Ioctx`\) – determines which RADOS pool the image is in
* **image\_id** \(_str_\) – the id of the image to restore
* **name** \(_str_\) – the new name of the restored image

Raises`ImageNotFound`

`version`\(_self_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Get the version number of the `librbd` C library.

Returnsa tuple of `(major, minor, extra)` components of the librbd version

_class_ `rbd.``SnapIterator`\(_Image image_\)[¶](https://docs.ceph.com/en/pacific/rbd/api/librbdpy/ "Permalink to this definition")Iterator over snapshot info for an image.

Yields a dictionary containing information about a snapshot.

Keys are:

* `id` \(int\) \- numeric identifier of the snapshot
* `size` \(int\) \- size of the image at the time of snapshot \(in bytes\)
* `name` \(str\) \- name of the snapshot
* `namespace` \(int\) \- enum for snap namespace
* `group` \(dict\) \- optional for group namespace snapshots
* `trash` \(dict\) \- optional for trash namespace snapshots
* `mirror` \(dict\) \- optional for mirror namespace snapshots
