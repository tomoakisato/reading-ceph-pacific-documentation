# 332: BlueStore Internals — Ceph Documentation

 # BlueStore Internals[¶](https://docs.ceph.com/en/pacific/dev/bluestore/ "Permalink to this headline")

## Small write strategies[¶](https://docs.ceph.com/en/pacific/dev/bluestore/ "Permalink to this headline")

* _U_: Uncompressed write of a complete, new blob.
    * write to new blob
    * kv commit
* _P_: Uncompressed partial write to unused region of an existing blob.
    * write to unused chunk\(s\) of existing blob
    * kv commit
* _W_: WAL overwrite: commit intent to overwrite, then overwrite async. Must be chunk\_size = MAX\(block\_size, csum\_block\_size\) aligned.
    * kv commit
    * wal overwrite \(chunk\-aligned\) of existing blob
* _N_: Uncompressed partial write to a new blob. Initially sparsely utilized. Future writes will either be _P_ or _W_.
    * write into a new \(sparse\) blob
    * kv commit
* _R\+W_: Read partial chunk, then to WAL overwrite.
    * read \(out to chunk boundaries\)
    * kv commit
    * wal overwrite \(chunk\-aligned\) of existing blob
* _C_: Compress data, write to new blob.
    * compress and write to new blob
    * kv commit

## Possible future modes[¶](https://docs.ceph.com/en/pacific/dev/bluestore/ "Permalink to this headline")

* _F_: Fragment lextent space by writing small piece of data into a piecemeal blob \(that collects random, noncontiguous bits of data we need to write\).
    * write to a piecemeal blob \(min\_alloc\_size or larger, but we use just one block of it\)
    * kv commit
* _X_: WAL read/modify/write on a single block \(like legacy bluestore\). No checksum.
    * kv commit
    * wal read/modify/write

## Mapping[¶](https://docs.ceph.com/en/pacific/dev/bluestore/ "Permalink to this headline")

This very roughly maps the type of write onto what we do when we encounter a given blob. In practice it’s a bit more complicated since there might be several blobs to consider \(e.g., we might be able to _W_ into one or_P_ into another\), but it should communicate a rough idea of strategy.

|                        |raw |raw \(cached\) |csum \(4 KB\) |csum \(16 KB\) |comp \(128 KB\) |
|------------------------|-----|--------------|-------------|--------------|---------------|
|128\+ KB \(over\)write |U |U |U |U |C |
|64 KB \(over\)write |U |U |U |U |U or C |
|4 KB overwrite |W |P | W |P | W |P | R\+W |P | N \(F?\) |
|100 byte overwrite |R\+W |P | W |P | R\+W |P | R\+W |P | N \(F?\) |
|100 byte append |R\+W |P | W |P | R\+W |P | R\+W |P | N \(F?\) |
|                        |     |              |             |              |               |
|4 KB clone overwrite |P | N |P | N |P | N |P | N |N \(F?\) |
|100 byte clone overwrite |P | N |P | N |P | N |P | N |N \(F?\) |
