# 64: Operating a Cluster

**クリップソース:** [64: Operating a Cluster — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/operations/operating/)

# Operating a Cluster[¶](https://docs.ceph.com/en/pacific/rados/operations/operating/ "Permalink to this headline")

## Running Ceph with systemd[¶](https://docs.ceph.com/en/pacific/rados/operations/operating/ "Permalink to this headline")

systemdをサポートするすべてのディストリビューション（CentOS 7, Fedora, Debian Jessie 8以降, SUSE）において、cephデーモンは従来のsysvinitスクリプトの代わりにsystemdネイティブファイルを使用して管理されるようになりました。 例えば

```
sudo systemctl start ceph.target       # start all daemons
sudo systemctl status ceph-osd@12      # check status of osd.12
```

ノード上のCeph systemdユニットを一覧表示するには、以下を実行します。

```
sudo systemctl status ceph*.service ceph*.target
```

### Starting all Daemons[¶](https://docs.ceph.com/en/pacific/rados/operations/operating/ "Permalink to this headline")

Ceph Nodeのすべてのデーモン\(タイプに関係なく\)を起動するには、以下を実行します。

```
sudo systemctl start ceph.target
```

### Stopping all Daemons[¶](https://docs.ceph.com/en/pacific/rados/operations/operating/ "Permalink to this headline")

Ceph Node上のすべてのデーモン\(タイプに関係なく\)を停止するには、以下を実行します。

```
sudo systemctl stop ceph*.service ceph*.target
```

### Starting all Daemons by Type[¶](https://docs.ceph.com/en/pacific/rados/operations/operating/ "Permalink to this headline")

Ceph Node上の特定のタイプのすべてのdaemonを起動するには、以下のいずれかを実行します。

```
sudo systemctl start ceph-osd.target
sudo systemctl start ceph-mon.target
sudo systemctl start ceph-mds.target
```

### Stopping all Daemons by Type[¶](https://docs.ceph.com/en/pacific/rados/operations/operating/ "Permalink to this headline")

Ceph Node上の特定のタイプのすべてのデーモンを停止するには、次のいずれかを実行します。

```
sudo systemctl stop ceph-mon*.service ceph-mon.target
sudo systemctl stop ceph-osd*.service ceph-osd.target
sudo systemctl stop ceph-mds*.service ceph-mds.target
```

### Starting a Daemon[¶](https://docs.ceph.com/en/pacific/rados/operations/operating/ "Permalink to this headline")

Ceph Nodeで特定のデーモンインスタンスを起動するには、次のいずれかを実行します。

```
sudo systemctl start ceph-osd@{id}
sudo systemctl start ceph-mon@{hostname}
sudo systemctl start ceph-mds@{hostname}
```

For example:

```
sudo systemctl start ceph-osd@1
sudo systemctl start ceph-mon@ceph-server
sudo systemctl start ceph-mds@ceph-server
```

### Stopping a Daemon[¶](https://docs.ceph.com/en/pacific/rados/operations/operating/ "Permalink to this headline")

Ceph Node上の特定のデーモンインスタンスを停止するには、次のいずれかを実行します。

```
sudo systemctl stop ceph-osd@{id}
sudo systemctl stop ceph-mon@{hostname}
sudo systemctl stop ceph-mds@{hostname}
```

For example:

```
sudo systemctl stop ceph-osd@1
sudo systemctl stop ceph-mon@ceph-server
sudo systemctl stop ceph-mds@ceph-server
```

## Running Ceph with Upstart[¶](https://docs.ceph.com/en/pacific/rados/operations/operating/ "Permalink to this headline")

### Starting all Daemons[¶](https://docs.ceph.com/en/pacific/rados/operations/operating/ "Permalink to this headline")

Ceph Nodeのすべてのデーモン\(タイプに関係なく\)を起動するには、以下を実行します。

```
sudo start ceph-all
```

### Stopping all Daemons[¶](https://docs.ceph.com/en/pacific/rados/operations/operating/ "Permalink to this headline")

Ceph Node上のすべてのデーモン\(タイプに関係なく\)を停止するには、以下を実行します。

```
sudo stop ceph-all
```

### Starting all Daemons by Type[¶](https://docs.ceph.com/en/pacific/rados/operations/operating/ "Permalink to this headline")

Ceph Node上の特定のタイプのすべてのdaemonを起動するには、以下のいずれかを実行します。

```
sudo start ceph-osd-all
sudo start ceph-mon-all
sudo start ceph-mds-all
```

### Stopping all Daemons by Type[¶](https://docs.ceph.com/en/pacific/rados/operations/operating/ "Permalink to this headline")

Ceph Node上の特定のタイプのすべてのデーモンを停止するには、次のいずれかを実行します。

```
sudo stop ceph-osd-all
sudo stop ceph-mon-all
sudo stop ceph-mds-all
```

### Starting a Daemon[¶](https://docs.ceph.com/en/pacific/rados/operations/operating/ "Permalink to this headline")

Ceph Nodeで特定のデーモンインスタンスを起動するには、次のいずれかを実行します。

```
sudo start ceph-osd id={id}
sudo start ceph-mon id={hostname}
sudo start ceph-mds id={hostname}
```

For example:

```
sudo start ceph-osd id=1
sudo start ceph-mon id=ceph-server
sudo start ceph-mds id=ceph-server
```

### Stopping a Daemon[¶](https://docs.ceph.com/en/pacific/rados/operations/operating/ "Permalink to this headline")

Ceph Node上の特定のデーモンインスタンスを停止するには、次のいずれかを実行します。

```
sudo stop ceph-osd id={id}
sudo stop ceph-mon id={hostname}
sudo stop ceph-mds id={hostname}
```

For example:

```
sudo stop ceph-osd id=1
sudo start ceph-mon id=ceph-server
sudo start ceph-mds id=ceph-server
```

## Running Ceph with sysvinit[¶](https://docs.ceph.com/en/pacific/rados/operations/operating/ "Permalink to this headline")

Cephデーモン\(またはクラスタ全体\)を起動、再起動、および停止するたびに、少なくとも1つのオプションと1つのコマンドを指定する必要があります。また、デーモンタイプまたはデーモンインスタンスを指定することもできます。

```
{commandline} [options] [commands] [daemons]
```

cephのオプションは以下の通り：

|Option       |Shortcut|Description                                                                       |
|-------------|--------|----------------------------------------------------------------------------------|
|\-\-verbose  |\-v     |冗長ログを使用する                                                                |
|\-\-valgrind |N/A     |\(DevとQAのみ\) Valgrindデバッギングを使用する                                    |
|\-\-allhosts |\-a     |ceph.confにあるすべてのノードで実行する。それ以外の場合は、localhostでのみ実行する|
|\-\-restart  |N/A     |デーモンがコアダンプした場合、自動的に再起動する                                  |
|\-\-norestart|N/A     |デーモンがコアダンプした場合、デーモンを再起動しない                              |
|\-\-conf     |\-c     |別の設定ファイルを使用する                                                        |

cephのコマンドは以下の通り：

|Command     |Description                                          |
|------------|-----------------------------------------------------|
|start       |デーモン（複数可）を起動する                         |
|stop        |デーモン（複数可）を停止する                         |
|forcestop   |デーモン（複数可）を強制的に停止させる。kill\-9と同じ|
|killall     |特定のタイプのデーモンをすべて終了させる             |
|cleanlogs   |ログディレクトリを消去する                           |
|cleanalllogs|ログディレクトリの全てを消去する                     |

サブシステムの操作では、\[daemons\]オプションに特定のデーモンタイプを追加することで、cephサービスが特定のデーモンタイプをターゲットにすることができます。デーモンタイプには以下のものがあります。

* mon
* osd
* mds
