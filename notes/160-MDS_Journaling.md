# 160: MDS Journaling

**クリップソース:** [160: MDS Journaling — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/mds-journaling/)

# MDS Journaling[¶](https://docs.ceph.com/en/pacific/cephfs/mds-journaling/ "Permalink to this headline")

## CephFS Metadata Pool[¶](https://docs.ceph.com/en/pacific/cephfs/mds-journaling/ "Permalink to this headline")

CephFSは、Ceph File Systemのファイルメタデータ（inodeとdentries）を管理するために、別の（メタデータ）プールを使用します。メタデータプールは、ファイルシステム階層を含むCeph File System内のファイルに関するすべての情報を持ちます。さらに、CephFSは、ファイルシステムジャーナル、オープンファイルテーブル、セッションマップなど、ファイルシステム内の他のエンティティに関連するメタ情報を保持します。

このドキュメントでは、Ceph Metadata Serversがジャーナリングをどのように使用し、どのように依存するかについて説明します。

## CephFS MDS Journaling[¶](https://docs.ceph.com/en/pacific/cephfs/mds-journaling/ "Permalink to this headline")

CephFSメタデータサーバは、ファイルシステム操作の実行前にメタデータイベントのジャーナルをメタデータプール内のRADOSにストリーミングします。アクティブなMDSデーモンは、CephFSのファイルおよびディレクトリのメタデータを管理します。

CephFSがジャーナリングを使用する理由は2つあります：

1. 一貫性：MDSのフェイルオーバー時に、ジャーナルイベントを再生することで、ファイルシステムの状態を一貫させることができます。また、バッキングストアへの複数の更新を必要とするメタデータ操作は、クラッシュ時の一貫性を保つためにジャーナルする必要があります（ロックなどの他の一貫性メカニズムとともに）。
2. 性能：ジャーナルの更新は（ほとんど）シーケンシャルであるため、ジャーナルへの更新は高速に行われます。さらに、更新は1回の書き込みにまとめることができるため、ファイルの異なる部分の更新に伴うディスクのシーク時間を節約することができます。また、大きなジャーナルを持つことは、standby状態のMDSがキャッシュをウォームアップするのに役立ち、MDSのフェイルオーバ時に間接的に役立つことがあります。

各アクティブなMDSは、メタデータ・プール内の独自のジャーナルを維持します。ジャーナルは複数のオブジェクトにまたがってストライピングされます。不要な（古いと判断された）ジャーナル・エントリーは、MDSによって切り捨てられます。

## Journal Events[¶](https://docs.ceph.com/en/pacific/cephfs/mds-journaling/ "Permalink to this headline")

ファイルシステムのメタデータの更新をジャーナル処理する以外に、CephFSは、クライアントセッション情報やディレクトリのインポート/エクスポート状態など、さまざまなイベントをジャーナル処理します。 これらのイベントは、必要に応じて正しい状態を再確立するためにMDSによって使用されます。例えば、Ceph MDSは、ジャーナル内の特定のイベントタイプが、クライアントエンティティタイプが再起動前にMDSとのセッションを持っていることを指定すると、再起動時にクライアントの再接続を試みます。

ジャーナルに記録されたイベントのリストを調べるために、CephFSはコマンドラインユーティリティcephfs\-journal\-toolを提供しています：

```
cephfs-journal-tool --rank=<fs>:<rank> event get list
```

cephfs\-journal\-toolは、破損したCephファイルシステムを検出して修復するためにも使用されます。\(詳細については、[cephfs\-journal\-tool](https://docs.ceph.com/en/pacific/cephfs/cephfs-journal-tool/)を参照してください\)

## Journal Event Types[¶](https://docs.ceph.com/en/pacific/cephfs/mds-journaling/ "Permalink to this headline")

以下は、MDSでジャーナルされるイベントの種類です。

1. EVENT\_COMMITTED: リクエスト\(id\)をcommittedとしてマークする
2. EVENT\_EXPORT: ディレクトリをMDSランクにマップする
3. EVENT\_FRAGMENT: ディレクトリ断片化（分割/結合）の様々な段階を追跡する
4. EVENT\_IMPORTSTART: MDSランクがディレクトリフラグメントのインポートを開始したときに記録
5. EVENT\_IMPORTFINISH: MDSランクがディレクトリフラグメントのインポートを終了したときに記録
6. EVENT\_NOOP: ジャーナル領域をスキップするオペレーションイベントタイプはない
7. EVENT\_OPEN: どのinodeがファイルハンドルを開いているか追跡する
8. EVENT\_RESETJOURNAL: ジャーナルをトランケート後のリセットとしてマークするために使用する
9. EVENT\_SESSION: 開いているクライアントセッションを追跡する
10. EVENT\_SLAVEUPDATE: \(スレーブ\)MDSに転送された操作の様々な段階を記録
11. EVENT\_SUBTREEMAP: ディレクトリinodeのディレクトリ内容（サブツリーパーティション）へのマップ
12. EVENT\_TABLECLIENT: クライアントテーブル（snap/anchor）のMDSビューの状態遷移を記録
13. EVENT\_TABLESERVER: サーバーテーブル（snap/anchor）のMDSビューの状態遷移を記録
14. EVENT\_UPDATE: inode上のファイル操作を記録
