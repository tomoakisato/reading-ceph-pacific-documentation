# 83: Balancer

**クリップソース:** [83: Balancer — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/operations/balancer/)

# Balancer[¶](https://docs.ceph.com/en/pacific/rados/operations/balancer/ "Permalink to this headline")

バランサーは、バランスの取れた配分を実現するために、OSD間のPGの配置を自動的または監視付きで最適化することができます。

## Status[¶](https://docs.ceph.com/en/pacific/rados/operations/balancer/ "Permalink to this headline")

バランサーの現在の状態を確認するには：

```
ceph balancer status
```

## Automatic balancing[¶](https://docs.ceph.com/en/pacific/rados/operations/balancer/ "Permalink to this headline")

upmapモードでは、デフォルトで自動バランス機能が有効になっています。詳しくは [Using the pg\-upmap](https://docs.ceph.com/en/pacific/rados/operations/balancer/)を参照してください。バランサーをオフにするには：

```
ceph balancer off
```

バランサーモードは、古いクライアントとの後方互換性があるcrush\-compatモードに変更することができます。OSDが均等に利用されるように、時間の経過とともにデータ配布に小さな変更を加えます。

## Throttling[¶](https://docs.ceph.com/en/pacific/rados/operations/balancer/ "Permalink to this headline")

クラスタが（たとえば、OSDが故障し、システムがまだ回復していないため）デグレードしている場合、PG分布の調整は行われません。

クラスタが健全な場合、バランサーは誤配置された \(移動が必要な\) PG の割合が 5%  \(デフォルト\)以下になるように、変更量を調整します。 target\_max\_misplaced\_ratio の閾値を調整するには：

```
ceph config set mgr target_max_misplaced_ratio .07   # 7%
```

自動バランサーの実行間にスリープする秒数を設定するには：

```
ceph config set mgr mgr/balancer/sleep_interval 60
```

自動バランスを開始する時刻を HHMM フォーマットで設定するには：

```
ceph config set mgr mgr/balancer/begin_time 0000
```

自動バランスを終了する時刻を HHMM 形式で設定するには：

```
ceph config set mgr mgr/balancer/end_time 2400
```

自動バランシングをこの曜日（crontabと同じ規則で、0か7が日曜日、1が月曜日）以降に制限するには：

```
ceph config set mgr mgr/balancer/begin_weekday 0
```

自動バランシングをこの曜日（crontabと同じ規則で、0か7が日曜日、1が月曜日）以前に制限するには：

```
ceph config set mgr mgr/balancer/end_weekday 7
```

自動バランシングが制限されるプールID（デフォルトは空文字列で、すべてのプールがバランスされることを意味する。数値のプールIDは、ceph osd pool ls detailコマンドで取得）を指定するには：

```
ceph config set mgr mgr/balancer/pool_ids 1,2,3
```

## Modes[¶](https://docs.ceph.com/en/pacific/rados/operations/balancer/ "Permalink to this headline")

現在、2つのバランサーモードがサポートされています。

1. このモードは旧来のクライアントと後方互換性があります。OSDMapとCRUSHマップを旧来のクライアントと共有する場合、最適化した重みを「real」重みとして提示します。
    このモードの主な制限は、階層のサブツリーがOSDを共有している場合、バランサーが異なる配置ルールを持つ複数のCRUSH階層を処理できないことです。 （これは通常は当てはまりません。共有OSDのスペース使用率を管理するのは難しいため、通常は推奨される構成ではありません。）
    **crush\-compat**
2. なお、upmapを使用するには、すべてのクライアントがLuminous以降である必要があります。
    **upmap**

デフォルトのモードはupmapです。 モードを調整すには：

```
ceph balancer mode crush-compat
```

## Supervised optimization[¶](https://docs.ceph.com/en/pacific/rados/operations/balancer/ "Permalink to this headline")

バランサーの動作は、いくつかのフェーズに分かれています。

1. 計画を立てる
2. 現在のPG分布、計画を実行した後のPG分布のいずれかで、データ分布の品質を評価する
3. 計画の実行

現在の分布を評価し、点数をつけるには：

```
ceph balancer eval
```

1つのプールの分布を評価するには：

```
ceph balancer eval <pool-name>
```

評価の詳細については、以下を参照してください。

```
ceph balancer eval-verbose ...
```

現在設定されているモードを使用して、計画を生成するには：

```
ceph balancer optimize <plan-name>
```

名前は、ユーザーが提供し、任意の有用な識別文字列を使用することができます。 計画の内容は、次のようにして見ることができます。

```
ceph balancer show <plan-name>
```

すべての計画を表示するには：

```
ceph balancer ls
```

古い計画を捨てるには：

```
ceph balancer rm <plan-name>
```

現在記録されている計画は、statusコマンドの一部として表示されます。

```
ceph balancer status
```

ある計画を実行した後に生じるであろう分布の質は、次のようにして計算することができます。

```
ceph balancer eval <plan-name>
```

そのプランが分布を改善することが期待できる（現在のクラスタの状態よりもスコアが低い）と仮定すると、ユーザーはそのプランを実行することができます。

```
ceph balancer execute <plan-name>
```
