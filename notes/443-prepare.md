# 443: prepare

**クリップソース:** [443: prepare — Ceph Documentation](https://docs.ceph.com/en/pacific/ceph-volume/lvm/prepare/)

# prepare[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/prepare/ "Permalink to this headline")

prepareサブコマンドは、[filestore](https://docs.ceph.com/en/pacific/glossary/#term-filestore)または[bluestore](https://docs.ceph.com/en/pacific/glossary/#term-bluestore)のセットアップを可能にします。ceph\-volume lvmで使用する前に、論理ボリュームを事前にプロビジョニングすることが推奨されます。

論理ボリュームは、メタデータを追加する以外には変更されません。

注：これは、OSDをデプロイする2つのステップの1つです。一回の呼び出しで済む方法をお探しの場合は、[create](https://docs.ceph.com/en/pacific/ceph-volume/lvm/create/#ceph-volume-lvm-create) をご覧ください。

ボリュームを識別するために、Cephで動作するようにボリューム\(または複数のボリューム\)を準備するプロセスで、ツールは[LVMタグ](https://docs.ceph.com/en/pacific/glossary/#term-LVM-tags)を使用していくつかのメタデータ情報を割り当てます。

[LVMタグ](https://docs.ceph.com/en/pacific/glossary/#term-LVM-tags)は、ボリュームを後で発見しやすくし、Cephシステムの一部であること、およびその役割\(journal、filestore、bluestoreなど\)を識別するのに役立ちます。

[bluestore](https://docs.ceph.com/en/pacific/glossary/#term-bluestore)がデフォルトですが、バックエンドを指定することができます：

* [–filestore](https://docs.ceph.com/en/pacific/ceph-volume/lvm/prepare/#ceph-volume-lvm-prepare-filestore)
* [–bluestore](https://docs.ceph.com/en/pacific/ceph-volume/lvm/prepare/#ceph-volume-lvm-prepare-bluestore)

## bluestore[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/prepare/ "Permalink to this headline")

bluestoreオブジェクトストアは、新しいOSDのデフォルトです。filestoreと比較して、デバイスに対してもう少し柔軟性があります。Bluestore は以下の構成をサポートしています：

* 1個のブロックデバイスと1個のblock.walデバイスと1個のblock.db デバイス
* 1個のブロックデバイスと1個のblock.walデバイス
* 1個のブロックデバイスと1個のblock.dbデバイス
* 1個のブロックデバイス

bluestore サブコマンドは、物理ブロックデバイス、物理ブロックデバイス上のパーティション、論理ボリュームを各種デバイスパラメータの引数として受け付けます。 物理デバイスを指定した場合、論理ボリュームが作成されます。ボリュームグループは、その名前がcephで始まる場合、作成または再利用されます。これにより、LVMをよりシンプルに使用できますが、柔軟性が損なわれます。LVの作成方法を変更するためのオプションや設定はありません。

ブロックは\-\-dataフラグで指定し、最も単純な使用例では次のようになります：

```
ceph-volume lvm prepare --bluestore --data vg/lv
```

rawデバイスも同様に指定することができます：

```
ceph-volume lvm prepare --bluestore --data /path/to/device
```

[暗号化](https://docs.ceph.com/en/pacific/ceph-volume/lvm/encryption/#ceph-volume-lvm-encryption)を有効にするには、\-\-dmcryptフラグが必要です：[442: Encryption](442-Encryption.md)

```
ceph-volume lvm prepare --bluestore --dmcrypt --data vg/lv
```

block.dbやblock.walが必要な場合は（bluestoreではオプション）、それぞれ\-\-block.dbと\-\-block.walで指定します。これらは、物理デバイス、パーティション、論理ボリュームにすることができます。

block.dbとblock.walの両パーティションは、そのまま使用できるため、論理ボリュームにはなりません。

OSDディレクトリの作成中、プロセスはtmpfsマウントを使用して、OSDに必要なすべてのファイルを配置します。これらのファイルは最初にceph\-osd \-\-mkfsによって作成され、完全にエフェメラルになります。

ブロックデバイス用に常にシンボリックリンクが作成され、オプションでblock.dbとblock.walにも作成されます。OSD IDが0のクラスタの場合、デフォルトでディレクトリは次のようになります：

```
# ls -l /var/lib/ceph/osd/ceph-0
lrwxrwxrwx. 1 ceph ceph 93 Oct 20 13:05 block -> /dev/ceph-be2b6fbd-bcf2-4c51-b35d-a35a162a02f0/osd-block-25cf0a05-2bc6-44ef-9137-79d65bd7ad62
lrwxrwxrwx. 1 ceph ceph 93 Oct 20 13:05 block.db -> /dev/sda1
lrwxrwxrwx. 1 ceph ceph 93 Oct 20 13:05 block.wal -> /dev/ceph/osd-wal-0
-rw-------. 1 ceph ceph 37 Oct 20 13:05 ceph_fsid
-rw-------. 1 ceph ceph 37 Oct 20 13:05 fsid
-rw-------. 1 ceph ceph 55 Oct 20 13:05 keyring
-rw-------. 1 ceph ceph  6 Oct 20 13:05 ready
-rw-------. 1 ceph ceph 10 Oct 20 13:05 type
-rw-------. 1 ceph ceph  2 Oct 20 13:05 whoami
```

上記の場合、ブロックにデバイスを使用しているため、ceph\-volumeは以下の規約でボリュームグループと論理ボリュームを作成します：

* ボリュームグループ名: ceph\-{cluster fsid}、またはvgが既に存在する場合は ceph\-{random uuid}
* 論理ボリューム名：osd\-block\-{osd\_fsid}

## filestore[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/prepare/ "Permalink to this headline")

filestoreオブジェクトストアOSDの論理ボリュームを準備するためのOSDバックエンドです。

OSDデータ用に論理ボリュームを、ジャーナル用に物理デバイス、パーティション、論理ボリュームを使用することができます。物理デバイスは、その上に論理ボリュームが作成されます。ボリュームグループは、cephで始まる名前で作成または再利用されます。 これらのボリュームは、データとジャーナルの最小サイズ要件に従う以外、特別な準備は必要ありません。

基本的なスタンドアロン filestore OSDのCLI呼び出しは、次のようになります：

```
ceph-volume lvm prepare --filestore --data <data block device>
```

外部ジャーナルを使用したfilestoreをデプロイする場合：

```
ceph-volume lvm prepare --filestore --data <data block device> --journal <journal block device>
```

[暗号化](https://docs.ceph.com/en/pacific/ceph-volume/lvm/encryption/#ceph-volume-lvm-encryption)を有効にするには、\-\-dmcryptフラグが必要です：

```
ceph-volume lvm prepare --filestore --dmcrypt --data <data block device> --journal <journal block device>
```

ジャーナルデバイス、データブロックデバイスともに、3つの形態をとることができます：

* 1個の物理ブロックデバイス
* 1個の物理ブロックデバイス上の1個のパーティション
* 1個の論理ボリューム

論理ボリュームを使用する場合、値はvolume\_group/logical\_volumeの形式でなければなりません。論理ボリューム名は一意性を強制されないため、誤ったボリュームを選択することを防ぐことができます。

パーティションを使用する場合、blkidで検出できるPARTUUIDを含む必要があります。これにより、デバイス名\(またはパス\)に関係なく、後で正しく識別することができます。

例：データ用の論理ボリュームとジャーナル用のパーティション /dev/sdc1 を渡す場合：

```
ceph-volume lvm prepare --filestore --data volume_group/lv_name --journal /dev/sdc1
```

データ用のベアデバイスとジャーナル用の論理ボリュームを渡す場合：

```
ceph-volume lvm prepare --filestore --data /dev/sdc --journal volume_group/journal_lv
```

生成されたuuidは、クラスタに新しいOSDを要求するために使用されます。この2つはOSDを特定するために重要であり、後に[activate](https://docs.ceph.com/en/pacific/ceph-volume/lvm/activate/#ceph-volume-lvm-activate)プロセス全体で使用されることになります。

OSDデータディレクトリは、次のような規約で作成されます：

```
/var/lib/ceph/osd/<cluster name>-<osd id>
```

この時点で、データボリュームはこの場所にマウントされ、ジャーナルボリュームはリンクされます：

```
ln -s /path/to/journal /var/lib/ceph/osd/<cluster_name>-<osd-id>/journal
```

monmapはOSDからブートストラップキーで取得します：

```
/usr/bin/ceph --cluster ceph --name client.bootstrap-osd
--keyring /var/lib/ceph/bootstrap-osd/ceph.keyring
mon getmap -o /var/lib/ceph/osd/<cluster name>-<osd id>/activate.monmap
```

マウント済みのOSDディレクトリにデータ（最初のステップで得た情報を再利用）を入力するためにceph\-osdが呼び出されます：

```
ceph-osd --cluster ceph --mkfs --mkkey -i <osd id> 
--monmap /var/lib/ceph/osd/<cluster name>-<osd id>/activate.monmap --osd-data 
/var/lib/ceph/osd/<cluster name>-<osd id> --osd-journal /var/lib/ceph/osd/<cluster name>-<osd id>/journal 
--osd-uuid <osd uuid> --keyring /var/lib/ceph/osd/<cluster name>-<osd id>/keyring 
--setuser ceph --setgroup ceph
```

## Partitioning[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/prepare/ "Permalink to this headline")

ceph\-volume lvmは現在、デバイス全体からパーティションを作成することはありません。デバイスパーティションを使用する場合、唯一の要件は、PARTUUIDを含み、blkidで発見可能であることです。fdisk と parted の両方が新しいパーティションのために自動的にこれを作成します。

例えば、新しい未フォーマットのドライブ（ここでは/dev/sdd）を使って、partedを使って新しいパーティションを作ることができます。まず、デバイス情報をリストアップします：

```
$ parted --script /dev/sdd print
Model: VBOX HARDDISK (scsi)
Disk /dev/sdd: 11.5GB
Sector size (logical/physical): 512B/512B
Disk Flags:
```

このデバイスはまだラベルも貼られていないので、パーティションを作成する前にpartedでgptラベルを作成し、parted printで再度検証することができます：

```
$ parted --script /dev/sdd mklabel gpt
$ parted --script /dev/sdd print
Model: VBOX HARDDISK (scsi)
Disk /dev/sdd: 11.5GB
Sector size (logical/physical): 512B/512B
Partition Table: gpt
Disk Flags:
```

ここで、1つのパーティションを作成し、後でblkidがceph\-volumeが必要とするPARTUUIDを見つけることができるかどうかを検証してみましょう：

```
$ parted --script /dev/sdd mkpart primary 1 100%
$ blkid /dev/sdd1
/dev/sdd1: PARTLABEL="primary" PARTUUID="16399d72-1e1f-467d-96ee-6fe371a7d0d4"
```

## Existing OSDs[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/prepare/ "Permalink to this headline")

OSDが稼働している既存のクラスタで新しいシステムを使用したい場合、考慮すべき点がいくつかあります：

警告: このプロセスはデータデバイスを強制的にフォーマットし、既存のデータがある場合はそれを破壊します。

* OSDのパスが下記の規約に従うこと：

```
/var/lib/ceph/osd/<cluster name>-<osd id>
```

* （fstabマウントポイントのような）ボリュームをマウントする他のメカニズムは存在してはならず、削除されるべきです。

IDが0で、「ceph」クラスタ名を使用する既存のOSDの1回限りの処理は次のようになります（次のコマンドはOSD内のデータをすべて破棄します）：

```
ceph-volume lvm prepare --filestore --osd-id 0 --osd-fsid E3D291C1-E7BF-4984-9794-B60D9FA139CB
```

コマンドラインツールは、OSD IDを生成するためにモニターに接触せず、後で起動できるようにLVMデバイスをフォーマットし、さらにその上にメタデータを保存します（メタデータの詳細な説明は、[メタデータ](https://docs.ceph.com/en/pacific/dev/ceph-volume/lvm/#ceph-volume-lvm-tags)を参照してください）。

## Crush device class[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/prepare/ "Permalink to this headline")

OSDのcrushデバイスクラスを設定するには、\-\-crush\-device\-classフラグを使用します。これは、bluestoreおよびfilestore OSD の両方で動作します：

```
ceph-volume lvm prepare --bluestore --data vg/lv --crush-device-class foo
```

## multipath support[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/prepare/ "Permalink to this headline")

マルチパスデバイスは、lvmが正しく設定されていれば、サポートされます。

**Leave it to LVM**

ほとんどのLinuxディストリビューションでは、LVM2パッケージのデフォルト設定でmultipath\_component\_detection=1になっているはずです。この設定では、LVMはマルチパスコンポーネントであるデバイスを無視し、それに応じてceph\-volumeはこれらのデバイスに触れないようにします。

**Using filters**

この設定が利用できない場合は、lvm.confで正しいフィルタ式を指定する必要があります。ceph\-volumeは、マルチパスデバイスとそのマルチパスコンポーネントの両方を使用することができないようにする必要があります。

## Storing metadata[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/prepare/ "Permalink to this headline")

以下のタグは、ボリューム（ジャーナルまたはデータ）またはOSDオブジェクトストアの種類に関係なく、prepareプロセスの一部として適用されます：

* cluster\_fsid
* encrypted
* osd\_fsid
* osd\_id
* crush\_device\_class

[filestore](https://docs.ceph.com/en/pacific/glossary/#term-filestore)の場合は、これらのタグが追加されます：

* journal\_device
* journal\_uuid

[bluestore](https://docs.ceph.com/en/pacific/glossary/#term-bluestore)の場合は、これらのタグが追加されます：

* block\_device
* block\_uuid
* db\_device
* db\_uuid
* wal\_device
* wal\_uuid

注：lvmタグの完全な規約は、[タグAPI](https://docs.ceph.com/en/pacific/dev/ceph-volume/lvm/#ceph-volume-lvm-tag-api)を参照してください。

## Summary[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/prepare/ "Permalink to this headline")

[bluestore](https://docs.ceph.com/en/pacific/glossary/#term-bluestore)のprepareプロセス：

1. 引数として、raw物理デバイス、物理デバイス上のパーティション、論理ボリュームを受け取ることができる
2. 任意のRAW物理デバイスに論理ボリュームを作成する
3. OSDのUUIDを生成する
4. 生成されたUUIDを再利用して、モニタにOSD IDを取得するよう依頼する
5. OSDデータディレクトリは、tmpfsマウント上に作成される
6. block, block.wal, block.db は定義されていればシンボリックリンクされる
7. activateのためにmonmapを取得する
8. ceph\-osdによってデータディレクトリが設定される
9. 論理ボリュームには、lvmタグを使用してすべてのCephメタデータが割り当てられる

[filestore](https://docs.ceph.com/en/pacific/glossary/#term-filestore)のprepareプロセス：

1. 引数として、raw物理デバイス、物理デバイス上のパーティション、論理ボリュームを受け取ることができる
2. OSDのUUIDを生成する
3. 生成されたUUIDを再利用して、モニタにOSD IDを取得するよう依頼する
4. OSDデータディレクトリを作成し、データボリュームをマウントする
5. journalは、データボリュームからジャーナルロケーションにシンボリックリンクされる
6. activateのためにmonmapを取得する
7. ceph\-osd によってデバイスがマウントされ、データディレクトリが設定される
8. データおよびジャーナルボリュームには、lvmタグを使用してすべてのCephメタデータが割り当てられる
