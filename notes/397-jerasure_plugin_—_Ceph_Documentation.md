# 397: jerasure plugin — Ceph Documentation

 # jerasure plugin[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/erasure_coding/jerasure/ "Permalink to this headline")

## Introduction[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/erasure_coding/jerasure/ "Permalink to this headline")

The parameters interpreted by the jerasure plugin are:

```
ceph osd erasure-code-profile set myprofile 
   directory=<dir>          # plugin directory absolute path
   plugin=jerasure          # plugin name (only jerasure)
   k=<k>                    # data chunks (default 2)
   m=<m>                    # coding chunks (default 2)
   technique=<technique>    # coding technique
```

The coding techniques can be chosen among _reed\_sol\_van_,_reed\_sol\_r6\_op_, _cauchy\_orig_, _cauchy\_good_, _liberation_,_blaum\_roth_ and _liber8tion_.

The _src/erasure\-code/jerasure_ directory contains the implementation. It is a wrapper around the code found at[https://github.com/ceph/jerasure](https://docs.ceph.com/en/pacific/dev/osd_internals/erasure_coding/jerasure/)and [https://github.com/ceph/gf\-complete](https://docs.ceph.com/en/pacific/dev/osd_internals/erasure_coding/jerasure/) , pinned to the latest stable version in _.gitmodules_. These repositories are copies of the upstream repositories [http://jerasure.org/jerasure/jerasure](https://docs.ceph.com/en/pacific/dev/osd_internals/erasure_coding/jerasure/) and[http://jerasure.org/jerasure/gf\-complete](https://docs.ceph.com/en/pacific/dev/osd_internals/erasure_coding/jerasure/) . The difference between the two, if any, should match pull requests against upstream.
