# 356: IANA Numbers — Ceph Documentation

 # IANA Numbers[¶](https://docs.ceph.com/en/pacific/dev/iana/ "Permalink to this headline")

## Private Enterprise Number \(PEN\) Assignment[¶](https://docs.ceph.com/en/pacific/dev/iana/ "Permalink to this headline")

50495

Organization `Ceph`.

## Port number \(monitor\)[¶](https://docs.ceph.com/en/pacific/dev/iana/ "Permalink to this headline")

3300

That’s 0xce4, or ce4h, or \(sort of\) “ceph.”
