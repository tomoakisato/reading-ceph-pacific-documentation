# 84: Using the pg-upmap

**クリップソース:** [84: Using the pg\-upmap — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/operations/upmap/)

# Using the pg\-upmap[¶](https://docs.ceph.com/en/pacific/rados/operations/upmap/ "Permalink to this headline")

Luminous v12.2.z からは、OSDMap に新しい pg\-upmap 例外テーブルがあり、クラスタが特定の PG を特定の OSD に明示的にマッピングできるようになりました。 これにより、クラスターはデータ配布を微調整して、ほとんどの場合、OSD 間で PG を完全に分散させることができます。

この新しいメカニズムの重要な注意点は、すべてのクライアントが OSDMap の新しい pg\-upmap 構造を理解する必要があることです。

## Enabling[¶](https://docs.ceph.com/en/pacific/rados/operations/upmap/ "Permalink to this headline")

新しいクラスタでは、このモジュールがデフォルトでオンになります。クラスタにはLuminous 以降のクライアントだけを持たせる必要があります。バランサをオフにするには：

```
ceph balancer off
```

既存のクラスタでこの機能を使用できるようにするには、Luminous以降クライアントのみをサポートする必要があることをクラスタに指示する必要があります：

```
ceph osd set-require-min-compat-client luminous
```

このコマンドは、Luminousより前のクライアントやデーモンがモニターに接続されている場合、失敗します。 どのバージョンのクライアントが使用されているかは、次のコマンドで確認できます：

```
ceph features
```

## Balancer module[¶](https://docs.ceph.com/en/pacific/rados/operations/upmap/ "Permalink to this headline")

ceph\-mgrのバランサーモジュールは、OSDごとにPGの数を自動的にバランスさせます。 [Balancer](https://docs.ceph.com/en/pacific/rados/operations/upmap/)を参照

## Offline optimization[¶](https://docs.ceph.com/en/pacific/rados/operations/upmap/ "Permalink to this headline")

upmapエントリーは、osdmaptoolに組み込まれたオフライン・オプティマイザで更新されます。

1. osdmapの最新版を入手する：

```
ceph osd getmap -o om
```

1. オプティマイザを実行する：

最適化は、各プールに対して個別に、または同様に使用されているプールのセットに対して行うことが強く推奨されます。 upmap\-pool オプションは複数回指定することができます。 「類似のプール」とは、同じデバイスにマッピングされ、同じ種類のデータを保存するプールを意味します（例：RBDイメージプール、yes；RGWインデックスプールとRGWデータプール、no）。

max\-optimizationsの値は、実行時に識別するupmapエントリの最大数です。 デフォルトはceph\-mgrバランサーモジュールと同じく10ですが、オフライン最適化を行う場合は、より大きな数値を使用する必要があります。追加で行う変更が見つからない（つまり、プールの分布が完璧な状態になった）場合は、早期に停止します。

max\-deviation値のデフォルトは5です。OSDPGカウントが、計算されたターゲット数からこの量以下だけ変化する場合、完全であると見なされます。

upmap\-activeオプションは、upmapモードでのアクティブバランサーの動作をシミュレートします。 OSD のバランスが取れるまで循環し続け、何ラウンド目か、各ラウンドにどれくらいの時間がかかっているかを報告します。 ラウンドの経過時間は、ceph\-mgrが次の最適化プランを計算しようとするときに消費されるCPU負荷を示します。

1. 変更を適用する：

提案された変更は、上記の例では出力ファイルout.txtに書き込まれます。 これらは、クラスタに変更を適用するために実行できる通常のceph CLIコマンドです。

上記の手順は、各プールのセットでPGの完全な分布を達成するために必要な回数だけ繰り返すことができます。

osdmaptool に \-\-debug\-osd 10 や \-\-debug\-crush 10 を渡すと、ツールが何をしているのか 詳細を見ることができます。
