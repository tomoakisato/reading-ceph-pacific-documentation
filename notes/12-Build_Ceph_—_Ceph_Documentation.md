# 12: Build Ceph — Ceph Documentation

**クリップソース:** [12: Build Ceph — Ceph Documentation](https://docs.ceph.com/en/pacific/install/build-ceph/)

# Build Ceph¶

You can get Ceph software by retrieving Ceph source code and building it yourself. To build Ceph, you need to set up a development environment, compile Ceph, and then either install in user space or build packages and install the packages.  Cephのソフトウェアは、Cephのソースコードを取得して自分でビルドすることで入手できます。Cephをビルドするには、開発環境を構築してCephをコンパイルし、ユーザースペースにインストールするか、パッケージをビルドしてインストールする必要があります。

## **Build Prerequisites[¶](https://docs.ceph.com/en/pacific/install/build-ceph/#build-prerequisites "Permalink to this headline")**

Tip

Check this section to see if there are specific prerequisites for your Linux/Unix distribution.  このセクションでは、お使いのLinux/Unixディストリビューションに特定の前提条件があるかどうかを確認します。

A debug build of Ceph may take around 40 gigabytes. If you want to build Ceph in a virtual machine \(VM\) please make sure total disk space on the VM is at least 60 gigabytes.  Cephのデバッグビルドには約40ギガバイト必要です。仮想マシン\(VM\)でCephを構築する場合は、VMの総ディスク容量が60ギガバイト以上であることを確認してください。

Please also be aware that some distributions of Linux, like CentOS, use Linux Volume Manager \(LVM\) for the default installation. LVM may reserve a large portion of disk space of a typical sized virtual disk for the operating system.  また、CentOSなどの一部のLinuxディストリビューションでは、デフォルトのインストールにLinux Volume Manager（LVM）が使用されていることにもご注意ください。LVMは、一般的なサイズの仮想ディスクのディスク領域の大部分をOS用に確保することがあります。

Before you can build Ceph source code, you need to install several libraries and tools:  Cephのソースコードをビルドする前に、いくつかのライブラリとツールをインストールする必要があります。

```
./install-deps.sh

```

Note

Some distributions that support Google’s memory profiler tool may use a different package name \(e.g., libgoogle\-perftools4\).  Google のメモリプロファイラツールをサポートしている一部のディストリビューションでは、異なるパッケージ名を使用している場合があります（例：libgoogle\-perftools4）。

## **Build Ceph[¶](https://docs.ceph.com/en/pacific/install/build-ceph/#id1 "Permalink to this headline")**

Ceph is built using cmake. To build Ceph, navigate to your cloned Ceph repository and execute the following:  Cephはcmakeを使ってビルドします。Cephをビルドするには、クローンされたCephのリポジトリに移動し、以下を実行します。

```
cd ceph
./do_cmake.sh
cd build
make

```

Note

By default do\_cmake.sh will build a debug version of ceph that may perform up to 5 times slower with certain workloads. Pass ‘\-DCMAKE\_BUILD\_TYPE=RelWithDebInfo’ to do\_cmake.sh if you would like to build a release version of the ceph executables instead.  デフォルトでは、do\_cmake.shは、特定の作業負荷で最大5倍遅くなる可能性があるcephのデバッグバージョンをビルドします。代わりにceph実行ファイルのリリースバージョンをビルドしたい場合は、do\_cmake.shに「\-DCMAKE\_BUILD\_TYPE=RelWithDebInfo」を渡します。

Hyperthreading

You can use make \-j to execute multiple jobs depending upon your system. For example, make \-j4 for a dual core processor may build faster.

See [Installing a Build](https://docs.ceph.com/en/pacific/install/install-storage-cluster#installing-a-build) to install a build in user space.  make \-j を使うと、システムに応じて複数のジョブを実行することができます。たとえば、デュアルコア・プロセッサの場合、make \-j4 を使用すると、より速くビルドすることができます。

ユーザースペースにビルドをインストールするには、「ビルドのインストール」を参照してください。

## **Build Ceph Packages[¶](https://docs.ceph.com/en/pacific/install/build-ceph/#build-ceph-packages "Permalink to this headline")**

To build packages, you must clone the [Ceph](https://docs.ceph.com/en/pacific/install/clone-source) repository. You can create installation packages from the latest code using dpkg\-buildpackage for Debian/Ubuntu or rpmbuild for the RPM Package Manager.  パッケージを構築するには、Cephのリポジトリをクローンする必要があります。Debian/Ubuntuの場合はdpkg\-buildpackage、RPMパッケージマネージャの場合はrpmbuildを使用して、最新のコードからインストールパッケージを作成できます。

Tip

When building on a multi\-core CPU, use the \-j and the number of cores \* 2. For example, use \-j4 for a dual\-core processor to accelerate the build.  マルチコアCPUでビルドする場合は、\-jとコア数\*2を使用します。例えば、デュアルコア・プロセッサの場合は、\-j4を使用してビルドを高速化します。

### Advanced Package Tool \(APT\)[¶](https://docs.ceph.com/en/pacific/install/build-ceph/#advanced-package-tool-apt "Permalink to this headline")

To create .deb packages for Debian/Ubuntu, ensure that you have cloned the [Ceph](https://docs.ceph.com/en/pacific/install/clone-source) repository, installed the [Build Prerequisites](https://docs.ceph.com/en/pacific/install/build-ceph/#build-prerequisites) and installed debhelper:  Debian/Ubuntu用の.debパッケージを作成するには、Cephリポジトリをクローンし、Build Prerequisitesをインストールし、debhelperをインストールしていることを確認してください。

```
sudo apt-get install debhelper

```

Once you have installed debhelper, you can build the packages:  debhelperをインストールしたら、パッケージをビルドすることができます。

```
sudo dpkg-buildpackage

```

For multi\-processor CPUs use the \-j option to accelerate the build.  マルチプロセッサのCPUでは、\-jオプションを使用してビルドを高速化します。

### RPM Package Manager[¶](https://docs.ceph.com/en/pacific/install/build-ceph/#rpm-package-manager "Permalink to this headline")

To create .rpm packages, ensure that you have cloned the [Ceph](https://docs.ceph.com/en/pacific/install/clone-source) repository, installed the [Build Prerequisites](https://docs.ceph.com/en/pacific/install/build-ceph/#build-prerequisites) and installed rpm\-build and rpmdevtools:  .rpmパッケージを作成するには、Cephリポジトリをクローンし、Build Prerequisitesをインストールし、rpm\-buildとrpmdevtoolsをインストールしたことを確認してください。

```
yum install rpm-build rpmdevtools

```

Once you have installed the tools, setup an RPM compilation environment:  ツールのインストールが完了したら、RPMのコンパイル環境を整えます。

```
rpmdev-setuptree

```

Fetch the source tarball for the RPM compilation environment:  RPMコンパイル環境のソースtarballを取得します。

```
wget -P ~/rpmbuild/SOURCES/ https://download.ceph.com/tarballs/ceph-<version>.tar.bz2

```

Or from the EU mirror:  あるいは、EUミラーから。

```
wget -P ~/rpmbuild/SOURCES/ http://eu.ceph.com/tarballs/ceph-<version>.tar.bz2

```

Extract the specfile:  スペックファイルを抽出します。

```
tar --strip-components=1 -C ~/rpmbuild/SPECS/ --no-anchored -xvjf ~/rpmbuild/SOURCES/ceph-<version>.tar.bz2 "ceph.spec"

```

Build the RPM packages:  RPMパッケージの構築

```
rpmbuild -ba ~/rpmbuild/SPECS/ceph.spec

```

For multi\-processor CPUs use the \-j option to accelerate the build.  マルチプロセッサのCPUでは、\-jオプションを使用してビルドを高速化します。
