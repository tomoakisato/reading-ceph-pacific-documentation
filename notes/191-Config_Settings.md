# 191: Config Settings

**クリップソース:** [191: Config Settings — Ceph Documentation](https://docs.ceph.com/en/pacific/rbd/rbd-config-ref/)

# Config Settings[¶](https://docs.ceph.com/en/pacific/rbd/rbd-config-ref/ "Permalink to this headline")

See [Block Device](https://docs.ceph.com/en/pacific/rbd/rbd-config-ref/) for additional details.

## Generic IO Settings[¶](https://docs.ceph.com/en/pacific/rbd/rbd-config-ref/ "Permalink to this headline")

**rbd\_compression\_hint**

Description

書き込み操作の際にOSDに送るヒント。compressibleに設定され、OSD bluestore\_compression\_mode設定がpassiveの場合、OSDはデータ圧縮を試みる。 incompressibleに設定され、OSD圧縮設定がaggressiveの場合、OSDはデータ圧縮を試みない

Type

Enum

Required

No

Default

none

Values

none, compressible, incompressible

**rbd\_read\_from\_replica\_policy**

Description

どの OSD が読み取り操作を受けるかを決定するための方針。defaultに設定されている場合、各PGのプライマリOSDは常に読み取り操作に使用される。balanceに設定された場合、読み取り操作はレプリカセット内でランダムに選択されたOSDに送信される。localizeに設定した場合、読み取り操作はCRUSHマップによって決定される最も近いOSDに送られる。注意：この機能は、最低限Octopusリリースと互換性のあるOSDでクラスタが構成されている必要がある

Type

Enum

Required

No

Default

default

Values

default, balance, localize

## Cache Settings[¶](https://docs.ceph.com/en/pacific/rbd/rbd-config-ref/ "Permalink to this headline")

Kernel Caching

Cephブロックデバイス用のカーネルドライバは、Linuxページキャッシュを使用してパフォーマンスを向上させることができます。

Cephブロックデバイスのユーザ空間実装（つまり、libbd）は、Linuxページキャッシュを利用できないため、"RBDキャッシング "と呼ばれる独自のインメモリキャッシングを搭載しています。RBDキャッシングは、ハードディスクキャッシングと同じように動作します。 OSがバリアやフラッシュ要求を出すと、すべてのダーティデータがOSDに書き込まれます。これは、ライトバックキャッシングの使用は、適切にフラッシュを送信するVM（すなわちLinuxカーネル\>= 2.6.32）で物理ハードディスクを使用するのと同じくらい安全であることを意味します。キャッシュは LRU \(Least Recently Used\) アルゴリズムを使用し、ライトバックモードでは、スループットを向上させるために連続したリクエストを結合させることができます。

librbdキャッシュはデフォルトで有効になっており、write\-around, write\-back, write\-throughの3種類のキャッシュポリシーがサポートされています。write\-aroundとwrite\-backポリシーでは、ストレージクラスタにrbd\_cache\_max\_dirty以上の未書き込みバイトがない限り、書き込みはすぐに返されます。write\-aroundポリシーは、write\-backポリシーとは異なり、キャッシュからの読み取り要求を処理しようとしないため、高性能の書き込みワークロードに対してより高速に動作します。write\-throughポリシーでは、データがすべてのレプリカのディスク上にある場合にのみ書き込みが返されますが、読み取りはキャッシュから行われることがあります。

フラッシュ要求を受け取る前は、キャッシュはライトスルーキャッシュのように動作し、フラッシュを送信しない古いオペレーティングシステムでも安全に動作し、クラッシュの一貫した動作を確保します。

librbdキャッシュが無効な場合、書き込みと読み出しはストレージクラスタに直接行われ、書き込みはすべてのレプリカでデータがディスク上にある場合にのみ返されます。

注：キャッシュはクライアント上のメモリにあり、各RBDイメージはそれ自身を持ちます。 キャッシュはクライアントにローカルなので、他の人がイメージにアクセスしている場合、コヒーレンシはありません。RBD上でGFSやOCFSを実行することは、キャッシュを有効にすると動作しません。

RBDのオプション設定は、設定ファイルの\[client\]セクション、または集中設定ストアで設定する必要があります。これらの設定は以下の通りです：

**rbd\_cache**

Description

RBD（RADOS Block Device）のキャッシングを有効にする

Type

Boolean

Required

No

Default

true

**rbd\_cache\_policy**

Description

librbdのキャッシングポリシーを選択

Type

Enum

Required

No

Default

writearound

Values

writearound, writeback, writethrough

**rbd\_cache\_writethrough\_until\_flush**

Description

最初はwritethroughでスタートし、最初のフラッシュ要求を受信した後にwritebackに切り替わる。有効化は、2.6.32 より古い Linux カーネルの virtio ドライバーのように、RBD ボリューム上で動作する VM が古すぎてフラッシュを送信できない場合に、保守的ではあるが安全な戦略である

Type

Boolean

Required

No

Default

true

**rbd\_cache\_size**

Description

ボリュームごとのRBDクライアントキャッシュのサイズ（バイト）

Type

64\-bit Integer

Required

No

Default

32MiB

Policies

write\-back and write\-through

**rbd\_cache\_max\_dirty**

Description

キャッシュがwrite\-backを開始するダーティリミット（バイト）。 0 の場合、write\-throughキャッシングを使用する

Type

64\-bit Integer

Required

No

Constraint

Must be less than rbd\_cache\_size.

Default

24MiB

Policies

write\-around and write\-back

**rbd\_cache\_target\_dirty**

Description

キャッシュがデータストレージへのデータ書き込みを開始する前のダーティターゲット（バイト）。キャッシュへの書き込みをブロックしない

Type

64\-bit Integer

Required

No

Constraint

Must be less than rbd\_cache\_max\_dirty.

Default

16MiB

Policies

write\-back

**rbd\_cache\_max\_dirty\_age**

Description

ダーティデータがキャッシュに残ってからライトバックが開始されるまでの時間（秒）

Type

Float

Required

No

Default

1.0

Policies

write\-back

## Read\-ahead Settings[¶](https://docs.ceph.com/en/pacific/rbd/rbd-config-ref/ "Permalink to this headline")

librbdは、小さなシーケンシャルリードを最適化するためにread\-ahead/プリフェッチをサポートしています。これは通常 VM の場合はゲスト OS によって処理されるべきですが、ブートローダは効率的なリードを発行しないかもしれません。キャッシュが無効な場合や、ポリシーがwrite\-aroundである場合、read\-aheadは自動的に無効になります。

**rbd\_readahead\_trigger\_requests**

Description

read\-aheadのトリガに必要なシーケンシャルread要求の数

Type

Integer

Required

No

Default

10

**rbd\_readahead\_max\_bytes**

Description

read\-aheadリクエストの最大サイズ。 0の場合、read\-aheadは無効となる

Type

64\-bit Integer

Required

No

Default

512KiB

**rbd\_readahead\_disable\_after\_bytes**

Description

RBDイメージからこれだけのバイトが読み込まれると、そのイメージはクローズされるまでread\-aheadが無効となる。 これにより、ゲストOSがブートされると、read\-aheadが引き継がれる。 0 の場合、read\-aheadは有効のままとなる

Type

64\-bit Integer

Required

No

Default

50MiB

## Image Features[¶](https://docs.ceph.com/en/pacific/rbd/rbd-config-ref/ "Permalink to this headline")

RBDは高度な機能をサポートしており、イメージ作成時にコマンドラインから指定するか、rbd\_default\_features=\<機能の数値の合計\>またはrbd\_default\_features=\<CLI値のカンマ区切りリスト\>でデフォルト機能を設定することが可能です。

**Layering**

Description

レイヤーを重ねることで、クローニングを可能とする

Internal value

1

CLI value

layering

Added in

v0.52 \(Bobtail\)

KRBD support

since v3.10

Default

yes

**Striping v2**

Description

ストライピングは、データを複数のオブジェクトに分散させる。ストライピングは、シーケンシャルリード/ライトのワークロードの並列処理に役立つ

Internal value

2

CLI value

striping

Added in

v0.55 \(Bobtail\)

KRBD support

since v3.10 \(default striping only, “fancy” striping added in v4.17\)

Default

yes

**Exclusive locking**

Description

この機能を有効にすると、書き込みを行う前に、クライアントがオブジェクトのロックを取得する必要がある。排他的ロックは、単一のクライアントがイメージにアクセスするときにのみ有効にする必要がある

Internal value

4

CLI value

exclusive\-lock

Added in

v0.92 \(Hammer\)

KRBD support

since v4.9

Default

yes

**Object map**

Description

オブジェクトマップのサポートは排他的ロックのサポートに依存する。ブロックデバイスはシンプロビジョニングされており、実際に書き込まれたデータのみを保存する、つまりスパースであることを意味する。オブジェクトマップサポートは、実際に存在する（デバイスにデータが保存されている）オブジェクトを追跡するのに役立つ。オブジェクトマップサポートを有効にすると、クローン作成、スパースイメージのインポートとエクスポート、削除などのI/O操作が高速化さる

Internal value

8

CLI value

object\-map

Added in

v0.93 \(Hammer\)

KRBD support

since v5.3

Default

yes

**Fast\-diff**

Description

Fast\-diffのサポートは、オブジェクトマップのサポートと排他的ロックのサポートに依存する。オブジェクトマップに別のプロパティを追加し、イメージのスナップショット間の差分をより高速に生成できるようになる。また、スナップショットやボリュームの実際のデータ使用量 \(rbddu\) の計算もより高速になる

Internal value

16

CLI value

fast\-diff

Added in

v9.0.1 \(Infernalis\)

KRBD support

since v5.3

Default

yes

**Deep\-flatten**

Description

Deep\-flatten は、rbd flatten がイメージ自体だけでなく、イメージのすべてのスナップショットに対して動作するようにする。これがないと、イメージのスナップショットはまだ親に依存しているので、スナップショットが最初に削除されるまで親を削除することができない。Deep\-flatten は親をそのクローンから独立させ、たとえそのクローンがスナップショットを持っていても、追加の OSD デバイススペースを使用することを代償に、親を独立させる。

Internal value

32

CLI value

deep\-flatten

Added in

v9.0.2 \(Infernalis\)

KRBD support

since v5.1

Default

yes

**Journaling**

Description

ジャーナリングサポートは排他的ロックのサポートに依存する。ジャーナリングは、イメージに対するすべての変更を発生順に記録する。RBDミラーリングは、ジャーナルを利用して、クラッシュコンシステントなイメージをリモートクラスタにレプリケートすることができる。 この機能を長期にわたって有効にすると、OSDスペースを大幅に消費することになるため、rbd\-mirrorに必要な分だけ管理させるのが最善である

Internal value

64

CLI value

journaling

Added in

v10.0.1 \(Jewel\)

KRBD support

no

Default

no

**Data pool**

Description

ECプールで、イメージ・データ・ブロック・オブジェクトを、イメージ・メタデータと別のプールに保存する必要があるInternal value

128

Added in

v11.1.0 \(Kraken\)

KRBD support

since v4.11

Default

no

**Operations**

Description

古いクライアントがイメージに対して特定のメンテナンス操作を行うことを制限するために使用する（例：クローン、スナップ作成）

Internal value

256

Added in

v13.0.2 \(Mimic\)

KRBD support

since v4.16

**Migrating**

Description

マイグレーション状態の時に古いクライアントがイメージを開くことを制限するために使用する

Internal value

512

Added in

v14.0.1 \(Nautilus\)

KRBD support

no

**Non\-primary**

Description

スナップショットベースのミラーリングを使用して、非プライマリイメージの変更を制限するために使用する

Internal value

1024

Added in

v15.2.0 \(Octopus\)

KRBD support

no

## QOS Settings[¶](https://docs.ceph.com/en/pacific/rbd/rbd-config-ref/ "Permalink to this headline")

librbdは、以下の設定によって制御される、イメージごとのI/O制限をサポートしています。

**rbd\_qos\_iops\_limit**

Description

IOPSの上限値

Type

Unsigned Integer

Required

No

Default

0

**rbd\_qos\_bps\_limit**

Description

I/O BPSの上限値

Type

Unsigned Integer

Required

No

Default

0

**rbd\_qos\_read\_iops\_limit**

Description

IOPS（read）の上限値

Type

Unsigned Integer

Required

No

Default

0

**rbd\_qos\_write\_iops\_limit**

Description

IOPS（write）の上限値

Type

Unsigned Integer

Required

No

Default

0

**rbd\_qos\_read\_bps\_limit**

Description

I/O BPS（read）の上限値

Type

Unsigned Integer

Required

No

Default

0

**rbd\_qos\_writ\_bps\_limit**

Description

I/O BPS（write）の上限値

Type

Unsigned Integer

Required

No

Default

0

**rbd\_qos\_iops\_burst**

Description

IOPSバーストの上限値

Type

Unsigned Integer

Required

No

Default

0

**rbd\_qos\_bps\_burst**

Description

I/O BPSバーストの上限値

Type

Unsigned Integer

Required

No

Default

0

**rbd\_qos\_read\_iops\_burst**

Description

IOPS（read）バーストの上限値

Type

Unsigned Integer

Required

No

Default

0

**rbd\_qos\_write\_iops\_burst**

Description

IOPS（write）バーストの上限値

Type

Unsigned Integer

Required

No

Default

0

**rbd\_qos\_read\_bps\_burst**

Description

I/O BPS（read）バーストの上限値

Type

Unsigned Integer

Required

No

Default

0

**rbd\_qos\_write\_bps\_burst**

Description

I/O BPS（write）バーストの上限値

Type

Unsigned Integer

Required

No

Default

0

**rbd\_qos\_iops\_burst\_seconds**

Description

IOPSバーストの時間（秒）

Type

Unsigned Integer

Required

No

Default

1

**rbd\_qos\_bps\_burst\_seconds**

Description

I/O BPSバーストの時間（秒）

Type

Unsigned Integer

Required

No

Default

1

**rbd\_qos\_read\_iops\_burst\_seconds**

Description

IOPS（read）バーストの時間（秒）

Type

Unsigned Integer

Required

No

Default

1

**rbd\_qos\_write\_iops\_burst\_seconds**

Description

IOPS（write）バーストの時間（秒）

Type

Unsigned Integer

Required

No

Default

1

**rbd\_qos\_read\_bps\_burst\_seconds**

Description

I/O BPS（read）バーストの時間（秒）

Type

Unsigned Integer

Required

No

Default

1

**rbd\_qos\_write\_bps\_burst\_seconds**

Description

I/O BPS（write）バーストの時間（秒）

Type

Unsigned Integer

Required

No

Default

1

**rbd\_qos\_schedule\_tick\_min**

Description

QoSの最小スケジュールtick（ミリ秒）

Type

Unsigned Integer

Required

No

Default

50
