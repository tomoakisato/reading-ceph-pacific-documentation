# 245: Ruby AWS::SDK Examples (aws-sdk gem ~>2)

 # Ruby [AWS::SDK](https://docs.ceph.com/en/pacific/radosgw/s3/ruby/) Examples \(aws\-sdk gem ~\>2\)[¶](https://docs.ceph.com/en/pacific/radosgw/s3/ruby/ "Permalink to this headline")

## Settings[¶](https://docs.ceph.com/en/pacific/radosgw/s3/ruby/ "Permalink to this headline")

You can setup the connection on global way:

```
Aws.config.update(
        endpoint: 'https://objects.dreamhost.com.',
        access_key_id: 'my-access-key',
        secret_access_key: 'my-secret-key',
        force_path_style: true,
        region: 'us-east-1'
)
```

and instantiate a client object:

```
s3_client = Aws::S3::Client.new
```

## Listing Owned Buckets[¶](https://docs.ceph.com/en/pacific/radosgw/s3/ruby/ "Permalink to this headline")

This gets a list of buckets that you own. This also prints out the bucket name and creation date of each bucket.

```
s3_client.list_buckets.buckets.each do |bucket|
        puts "#{bucket.name}t#{bucket.creation_date}"
end
```

The output will look something like this:

```
mahbuckat1   2011-04-21T18:05:39.000Z
mahbuckat2   2011-04-21T18:05:48.000Z
mahbuckat3   2011-04-21T18:07:18.000Z
```

## Creating a Bucket[¶](https://docs.ceph.com/en/pacific/radosgw/s3/ruby/ "Permalink to this headline")

This creates a new bucket called `my-new-bucket`

```
s3_client.create_bucket(bucket: 'my-new-bucket')
```

If you want a private bucket:

acl option accepts: \# private, public\-read, public\-read\-write, authenticated\-read

```
s3_client.create_bucket(bucket: 'my-new-bucket', acl: 'private')
```

## Listing a Bucket’s Content[¶](https://docs.ceph.com/en/pacific/radosgw/s3/ruby/ "Permalink to this headline")

This gets a list of hashes with the contents of each object This also prints out each object’s name, the file size, and last modified date.

```
s3_client.get_objects(bucket: 'my-new-bucket').contents.each do |object|
        puts "#{object.key}t#{object.size}t#{object.last-modified}"
end
```

The output will look something like this if the bucket has some files:

```
myphoto1.jpg 251262  2011-08-08T21:35:48.000Z
myphoto2.jpg 262518  2011-08-08T21:38:01.000Z
```

## Deleting a Bucket[¶](https://docs.ceph.com/en/pacific/radosgw/s3/ruby/ "Permalink to this headline")

Note:

The Bucket must be empty\! Otherwise it won’t work\!

```
s3_client.delete_bucket(bucket: 'my-new-bucket')
```

## Forced Delete for Non\-empty Buckets[¶](https://docs.ceph.com/en/pacific/radosgw/s3/ruby/ "Permalink to this headline")

First, you need to clear the bucket:

```
Aws::S3::Bucket.new('my-new-bucket', client: s3_client).clear!
```

after, you can destroy the bucket

```
s3_client.delete_bucket(bucket: 'my-new-bucket')
```

## Creating an Object[¶](https://docs.ceph.com/en/pacific/radosgw/s3/ruby/ "Permalink to this headline")

This creates a file `hello.txt` with the string `"Hello World!"`

```
s3_client.put_object(
        key: 'hello.txt',
        body: 'Hello World!',
        bucket: 'my-new-bucket',
        content_type: 'text/plain'
)
```

## Change an Object’s ACL[¶](https://docs.ceph.com/en/pacific/radosgw/s3/ruby/ "Permalink to this headline")

This makes the object `hello.txt` to be publicly readable, and `secret_plans.txt`to be private.

```
s3_client.put_object_acl(bucket: 'my-new-bucket', key: 'hello.txt', acl: 'public-read')

s3_client.put_object_acl(bucket: 'my-new-bucket', key: 'private.txt', acl: 'private')
```

## Download an Object \(to a file\)[¶](https://docs.ceph.com/en/pacific/radosgw/s3/ruby/ "Permalink to this headline")

This downloads the object `poetry.pdf` and saves it in`/home/larry/documents/`

```
s3_client.get_object(bucket: 'my-new-bucket', key: 'poetry.pdf', response_target: '/home/larry/documents/poetry.pdf')
```

## Delete an Object[¶](https://docs.ceph.com/en/pacific/radosgw/s3/ruby/ "Permalink to this headline")

This deletes the object `goodbye.txt`

```
s3_client.delete_object(key: 'goodbye.txt', bucket: 'my-new-bucket')
```

## Generate Object Download URLs \(signed and unsigned\)[¶](https://docs.ceph.com/en/pacific/radosgw/s3/ruby/ "Permalink to this headline")

This generates an unsigned download URL for `hello.txt`. This works because we made `hello.txt` public by setting the ACL above. This then generates a signed download URL for `secret_plans.txt` that will work for 1 hour. Signed download URLs will work for the time period even if the object is private \(when the time period is up, the URL will stop working\).

```
puts Aws::S3::Object.new(
        key: 'hello.txt',
        bucket_name: 'my-new-bucket',
        client: s3_client
).public_url

puts Aws::S3::Object.new(
        key: 'secret_plans.txt',
        bucket_name: 'hermes_ceph_gem',
        client: s3_client
).presigned_url(:get, expires_in: 60 * 60)
```

The output of this will look something like:

```
http://objects.dreamhost.com/my-bucket-name/hello.txt
http://objects.dreamhost.com/my-bucket-name/secret_plans.txt?Signature=XXXXXXXXXXXXXXXXXXXXXXXXXXX&Expires=1316027075&AWSAccessKeyId=XXXXXXXXXXXXXXXXXXX
```
