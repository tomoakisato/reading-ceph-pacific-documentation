# 161: File layouts

**クリップソース:** [161: File layouts — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/file-layouts/)

# File layouts[¶](https://docs.ceph.com/en/pacific/cephfs/file-layouts/ "Permalink to this headline")

ファイルのレイアウトは、コンテンツをCeph RADOSオブジェクトにどのようにマッピングするかを制御します。 ファイルのレイアウトは、仮想拡張属性\(xattrs\)を使用してread/writeできます。

レイアウト xattrs の名前は、ファイルが通常のファイルであるか、ディレクトリであるかによって異なります。 通常のファイルのレイアウトxattrsはceph.file.layoutと呼ばれ、ディレクトリのレイアウトxattrsはceph.dir.layoutと呼ばれます。 以降の例でceph.file.layoutは、ディレクトリを扱う場合はceph.dir.layoutに置き換えてください。

ヒント：お使いのlinuxにはxattrsを操作するためのコマンドがデフォルトで入っていないかもしれません、必要なパッケージは通常attrと呼ばれます。

## Layout fields[¶](https://docs.ceph.com/en/pacific/cephfs/file-layouts/ "Permalink to this headline")

pool

IDまたは名前を指定する文字列。文字列は\[a\-zA\-Z0\-9\_\-\]の文字のみを持つことができます。ファイルのデータオブジェクトをどのRADOSプールに格納するかを指定します。

pool\_namespace

\[a\-zA\-Z0\-9\_\-\]のみを含む文字列です。 データプール内で、どの RADOS ネームスペースにオブジェクトを書き込むか。 デフォルトでは空\(つまり、デフォルトのネームスペース\)。

stripe\_unit

バイト単位。 ファイルのRAID 0で使用されるデータブロックのサイズ（バイト）です。ファイルのストライプユニットはすべて同じサイズです。最後のストライプユニットは通常不完全です。つまり、ファイルの終端のデータと、固定ストライプユニットサイズ終端までの未使用の「スペース」を表します。

stripe\_count

整数。 ファイルデータのRAID 0 ストライプ を構成するstripe\_unitの数。

object\_size

バイト単位。 ファイルデータは、このサイズのRADOSオブジェクトにチャンクされます。

ヒント： RADOSはオブジェクトサイズに設定可能な制限を設けており、CephFSオブジェクトサイズを制限以上に大きくすると、書き込みに成功しない場合があります。 OSDの設定はosd\_max\_object\_sizeで、デフォルトでは128MBになっています。非常に大きなRADOSオブジェクトはクラスタの円滑な運用を妨げる可能性があるため、オブジェクトサイズの制限をデフォルトより大きくすることは推奨されません。

## Reading layouts with getfattr[¶](https://docs.ceph.com/en/pacific/cephfs/file-layouts/ "Permalink to this headline")

レイアウト情報を1つの文字列として読み取るには：

```
$ touch file
$ getfattr -n ceph.file.layout file
# file: file
ceph.file.layout="stripe_unit=4194304 stripe_count=1 object_size=4194304 pool=cephfs_data"
```

個々のレイアウトフィールドを読み取るには：

```
$ getfattr -n ceph.file.layout.pool file
# file: file
ceph.file.layout.pool="cephfs_data"
$ getfattr -n ceph.file.layout.stripe_unit file
# file: file
ceph.file.layout.stripe_unit="4194304"
$ getfattr -n ceph.file.layout.stripe_count file
# file: file
ceph.file.layout.stripe_count="1"
$ getfattr -n ceph.file.layout.object_size file
# file: file
ceph.file.layout.object_size="4194304"
```

注意：レイアウトを読み込む際、通常、プールは名前で表示されます。 しかし、稀にプールが作成されたばかりの場合、代わりにIDが出力されることがあります。

ディレクトリは、カスタマイズされるまでは明示的なレイアウトを持ちません。 レイアウトが一度も変更されていない場合、レイアウトの読み込みは失敗します。これは、レイアウトが明示されている次の祖先のディレクトリのレイアウトが使用されることを示します。

```
$ mkdir dir
$ getfattr -n ceph.dir.layout dir
dir: ceph.dir.layout: No such attribute
$ setfattr -n ceph.dir.layout.stripe_count -v 2 dir
$ getfattr -n ceph.dir.layout dir
# file: dir
ceph.dir.layout="stripe_unit=4194304 stripe_count=2 object_size=4194304 pool=cephfs_data"
```

## Writing layouts with setfattr[¶](https://docs.ceph.com/en/pacific/cephfs/file-layouts/ "Permalink to this headline")

レイアウトフィールドはsetfattrで変更します：

```
$ ceph osd lspools
0 rbd
1 cephfs_data
2 cephfs_metadata

$ setfattr -n ceph.file.layout.stripe_unit -v 1048576 file2
$ setfattr -n ceph.file.layout.stripe_count -v 8 file2
$ setfattr -n ceph.file.layout.object_size -v 10485760 file2
$ setfattr -n ceph.file.layout.pool -v 1 file2  # Setting pool by ID
$ setfattr -n ceph.file.layout.pool -v cephfs_data file2  # Setting pool by name
```

注意：setfattrを使用してファイルのレイアウトフィールドを変更する場合、このファイルは空でなければならず、そうでない場合はエラーが発生します。

```
# touch an empty file
$ touch file1
# modify layout field successfully
$ setfattr -n ceph.file.layout.stripe_count -v 3 file1

# write something to file1
$ echo "hello world" > file1
$ setfattr -n ceph.file.layout.stripe_count -v 4 file1
setfattr: file1: Directory not empty
```

## Clearing layouts[¶](https://docs.ceph.com/en/pacific/cephfs/file-layouts/ "Permalink to this headline")

ディレクトリから明示的なレイアウトを削除し、祖先のレイアウトを継承するように戻したい場合：

```
setfattr -x ceph.dir.layout mydir
```

pool\_namespace属性を設定した後、デフォルトのnamespaceを使用するようにレイアウトを変更したい場合も同様です：

```
# Create a dir and set a namespace on it
mkdir mydir
setfattr -n ceph.dir.layout.pool_namespace -v foons mydir
getfattr -n ceph.dir.layout mydir
ceph.dir.layout="stripe_unit=4194304 stripe_count=1 object_size=4194304 pool=cephfs_data_a pool_namespace=foons"

# Clear the namespace from the directory's layout
setfattr -x ceph.dir.layout.pool_namespace mydir
getfattr -n ceph.dir.layout mydir
ceph.dir.layout="stripe_unit=4194304 stripe_count=1 object_size=4194304 pool=cephfs_data_a"
```

## Inheritance of layouts[¶](https://docs.ceph.com/en/pacific/cephfs/file-layouts/ "Permalink to this headline")

ファイルは、作成時に親ディレクトリのレイアウトを引き継ぎます。 しかし、その後親ディレクトリのレイアウトを変更しても、子ディレクトリには影響しません。

```
$ getfattr -n ceph.dir.layout dir
# file: dir
ceph.dir.layout="stripe_unit=4194304 stripe_count=2 object_size=4194304 pool=cephfs_data"

# Demonstrate file1 inheriting its parent's layout
$ touch dir/file1
$ getfattr -n ceph.file.layout dir/file1
# file: dir/file1
ceph.file.layout="stripe_unit=4194304 stripe_count=2 object_size=4194304 pool=cephfs_data"

# Now update the layout of the directory before creating a second file
$ setfattr -n ceph.dir.layout.stripe_count -v 4 dir
$ touch dir/file2

# Demonstrate that file1's layout is unchanged
$ getfattr -n ceph.file.layout dir/file1
# file: dir/file1
ceph.file.layout="stripe_unit=4194304 stripe_count=2 object_size=4194304 pool=cephfs_data"

# ...while file2 has the parent directory's new layout
$ getfattr -n ceph.file.layout dir/file2
# file: dir/file2
ceph.file.layout="stripe_unit=4194304 stripe_count=4 object_size=4194304 pool=cephfs_data"
```

ディレクトリの子孫として作成されるファイルも、中間ディレクトリにレイアウトが設定されていない場合は、レイアウトを継承します：

```
$ getfattr -n ceph.dir.layout dir
# file: dir
ceph.dir.layout="stripe_unit=4194304 stripe_count=4 object_size=4194304 pool=cephfs_data"
$ mkdir dir/childdir
$ getfattr -n ceph.dir.layout dir/childdir
dir/childdir: ceph.dir.layout: No such attribute
$ touch dir/childdir/grandchild
$ getfattr -n ceph.file.layout dir/childdir/grandchild
# file: dir/childdir/grandchild
ceph.file.layout="stripe_unit=4194304 stripe_count=4 object_size=4194304 pool=cephfs_data"
```

## Adding a data pool to the File System[¶](https://docs.ceph.com/en/pacific/cephfs/file-layouts/ "Permalink to this headline")

CephFSでプールを使用する前に、Metadata Serversにプールを追加する必要があります：

```
$ ceph fs add_data_pool cephfs cephfs_data_ssd
$ ceph fs ls  # Pool should now show up
.... data pools: [cephfs_data cephfs_data_ssd ]
```

CephXキーで、クライアントがこの新しいプールにアクセスすることを許可していることを確認します。

その後、CephFSのディレクトリのレイアウトを更新して、追加したプールを使用することができます：

```
$ mkdir /mnt/cephfs/myssddir
$ setfattr -n ceph.dir.layout.pool -v cephfs_data_ssd /mnt/cephfs/myssddir
```

そのディレクトリ内に作成されたすべての新しいファイルは、そのレイアウトを継承して、新しく追加されたプールにデータを配置します。

追加したプールにファイルが作成されていても、プライマリデータプール（fsnewに渡されたもの）のオブジェクト数が増え続けていることに気がつくかもしれません。 ファイルデータはレイアウトで指定されたプールに格納されますが、すべてのファイルの少量のメタデータはプライマリデータプールに保持されます。
