# 130: Create a Ceph file system

**クリップソース:** [130: Create a Ceph file system — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/createfs/)

# Create a Ceph file system[¶](https://docs.ceph.com/en/pacific/cephfs/createfs/ "Permalink to this headline")

## Creating pools[¶](https://docs.ceph.com/en/pacific/cephfs/createfs/ "Permalink to this headline")

Cephファイルシステムには、少なくとも2つのRADOSプールが必要です。1つはデータ用、もう1つはメタデータ用です。これらのプールを構成する場合、以下のことを考慮するとよいでしょう：

* メタデータプールのレプリケーションレベルを高くする。このプールでデータが失われると、ファイルシステム全体がアクセス不能になる可能性があるため。
* メタデータプールにSSDなどの低レイテンシーストレージを使用することで、クライアントでのファイルシステム操作のレイテンシーに直接影響します。
* ファイルシステムの作成に使用されたデータプールは、「default」のデータプールであり、ハードリンク管理と災害復旧に使用される、すべてのinodeバックトレース情報の保存場所です。このため、CephFSで作成されるすべてのinodeは、defaultデータプールに少なくとも1つのオブジェクトを持ちます。ファイルシステムにECプールを計画している場合、バックトレースを更新するためのスモールオブジェクトの書き込みおよび読み取りパフォーマンスを向上させるために、通常、defaultデータプールにreplicatedプールを使用する方がよいでしょう。これとは別に、ディレクトリやファイルの階層全体\(

プールの管理の詳細については、「[プール](https://docs.ceph.com/en/pacific/rados/operations/pools/)」を参照してください。 たとえば、ファイル システムで使用するためのデフォルト設定のプールを 2 つ作成する場合、次のコマンドを実行します：

```
$ ceph osd pool create cephfs_data
$ ceph osd pool create cephfs_metadata
```

一般に、メタデータ・プールのデータ量はせいぜい数ギガバイト程度です。このため、通常はPG数を少なくすることが推奨されます。大規模なクラスタでは、64または128が実際によく使用されます。

注：ファイルシステム、メタデータプール、およびデータプールの名前には、\[a\-zA\-Z0\-9\_\-\]の文字しか使用できません。

## Creating a file system[¶](https://docs.ceph.com/en/pacific/cephfs/createfs/ "Permalink to this headline")

プールを作成したら、fsnewコマンドを使用してファイルシステムを有効にすることができます：

```
$ ceph fs new <fs_name> <metadata> <data>
```

For example:

```
$ ceph fs new cephfs cephfs_metadata cephfs_data
$ ceph fs ls
name: cephfs, metadata pool: cephfs_metadata, data pools: [cephfs_data ]
```

ファイルシステムが作成されると、MDSはアクティブな状態になります。 例えば、1つのMDSシステムで：

```
$ ceph mds stat
cephfs-1/1/1 up {0=a=up:active}
```

ファイルシステムが作成され、データシートがアクティブになると、ファイルシステムをマウントする準備ができま す。 複数のファイルシステムを作成した場合、マウントする際に使用するファイルシステムを選択します。

* [Mount CephFS](https://docs.ceph.com/en/pacific/cephfs/createfs/)
* [Mount CephFS as FUSE](https://docs.ceph.com/en/pacific/cephfs/createfs/)
* [Mount CephFS on Windows](https://docs.ceph.com/en/pacific/cephfs/createfs/)

複数のファイルシステムを作成し、クライアントがマウント時にファイルシステムを指定しない場合、ceph fs set\-defaultコマンドを使用して、クライアントが表示するファイルシステムを制御することができます。

### Adding a Data Pool to the File System[¶](https://docs.ceph.com/en/pacific/cephfs/createfs/ "Permalink to this headline")

「[ファイルシステムにデータプールを追加する](https://docs.ceph.com/en/pacific/cephfs/file-layouts/#adding-data-pool-to-file-system)」を参照してください。

## Using Erasure Coded pools with CephFS[¶](https://docs.ceph.com/en/pacific/cephfs/createfs/ "Permalink to this headline")

ECプールは、上書きを有効にしている限り、CephFSデータプールとして使用することができます：

```
ceph osd pool set my_ec_pool allow_ec_overwrites true
```

ECの上書きは、BlueStoreバックエンドでOSDSを使用する場合にのみサポートされることに注意してください。

CephFSのメタデータはRADOS OMAPデータ構造を使用して格納されるため、ECプールでは格納できないため、CephFSメタデータプールとしてECプールを使用することはできません。
