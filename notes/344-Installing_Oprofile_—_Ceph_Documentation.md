# 344: Installing Oprofile — Ceph Documentation

 # Installing Oprofile[¶](https://docs.ceph.com/en/pacific/dev/cpu-profiler/ "Permalink to this headline")

The easiest way to profile Ceph’s CPU consumption is to use the [oprofile](https://docs.ceph.com/en/pacific/dev/cpu-profiler/)system\-wide profiler.

## Installation[¶](https://docs.ceph.com/en/pacific/dev/cpu-profiler/ "Permalink to this headline")

If you are using a Debian/Ubuntu distribution, you can install `oprofile` by executing the following:

```
sudo apt-get install oprofile oprofile-gui
```

## Compiling Ceph for Profiling[¶](https://docs.ceph.com/en/pacific/dev/cpu-profiler/ "Permalink to this headline")

To compile Ceph for profiling, first clean everything.

```
make distclean
```

Then, export the following settings so that you can see callgraph output.

```
export CFLAGS="-fno-omit-frame-pointer -O2 -g"
```

Finally, compile Ceph.

```
./autogen.sh
./configure
make
```

You can use `make -j` to execute multiple jobs depending upon your system. For example:

```
make -j4
```

## Ceph Configuration[¶](https://docs.ceph.com/en/pacific/dev/cpu-profiler/ "Permalink to this headline")

Ensure that you disable `lockdep`. Consider setting logging to levels appropriate for a production cluster. See [Ceph Logging and Debugging](https://docs.ceph.com/en/pacific/dev/cpu-profiler/)for details.

See the [CPU Profiling](https://docs.ceph.com/en/pacific/dev/cpu-profiler/) section of the RADOS Troubleshooting documentation for details on using Oprofile.
