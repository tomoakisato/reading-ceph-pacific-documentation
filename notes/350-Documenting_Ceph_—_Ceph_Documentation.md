# 350: Documenting Ceph — Ceph Documentation

 # Documenting Ceph[¶](https://docs.ceph.com/en/pacific/dev/documenting/ "Permalink to this headline")

## User documentation[¶](https://docs.ceph.com/en/pacific/dev/documenting/ "Permalink to this headline")

The documentation on docs.ceph.com is generated from the restructuredText sources in `/doc/` in the Ceph git repository.

Please make sure that your changes are written in a way that is intended for end users of the software, unless you are making additions in`/doc/dev/`, which is the section for developers.

All pull requests that modify user\-facing functionality must include corresponding updates to documentation: see[Submitting Patches](https://docs.ceph.com/en/pacific/dev/documenting/) for more detail.

Check your .rst syntax is working as expected by using the “View” button in the github user interface when looking at a diff on an .rst file, or build the docs locally using the `admin/build-doc`script.

For more information about the Ceph documentation, see[Documenting Ceph](https://docs.ceph.com/en/pacific/dev/documenting/).

## Code Documentation[¶](https://docs.ceph.com/en/pacific/dev/documenting/ "Permalink to this headline")

C and C\+\+ can be documented with [Doxygen](https://docs.ceph.com/en/pacific/dev/documenting/), using the subset of Doxygen markup supported by [Breathe](https://docs.ceph.com/en/pacific/dev/documenting/).

The general format for function documentation is:

```
/**
 * Short description
 *
 * Detailed description when necessary
 *
 * preconditons, postconditions, warnings, bugs or other notes
 *
 * parameter reference
 * return value (if non-void)
 */
```

This should be in the header where the function is declared, and functions should be grouped into logical categories. The [librados C API](https://docs.ceph.com/en/pacific/dev/documenting/) provides a complete example. It is pulled into Sphinx by[librados.rst](https://docs.ceph.com/en/pacific/dev/documenting/), which is rendered at [Librados \(C\)](https://docs.ceph.com/en/pacific/dev/documenting/).

To generate the doxygen documentation in HTML format use:

```
# make doxygen
```

HTML output will be under: `build-doc/doxygen/html`

## Drawing diagrams[¶](https://docs.ceph.com/en/pacific/dev/documenting/ "Permalink to this headline")

### Graphviz[¶](https://docs.ceph.com/en/pacific/dev/documenting/ "Permalink to this headline")

You can use [Graphviz](https://docs.ceph.com/en/pacific/dev/documenting/), as explained in the [Graphviz extension documentation](https://docs.ceph.com/en/pacific/dev/documenting/).

Most of the time, you’ll want to put the actual DOT source in a separate file, like this:

```
.. graphviz:: myfile.dot
```

### Ditaa[¶](https://docs.ceph.com/en/pacific/dev/documenting/ "Permalink to this headline")

You can use [Ditaa](https://docs.ceph.com/en/pacific/dev/documenting/):

![7a209bb85d52e044e4bfddf6fda0105fcb8cb5aa598f554cf0e4659295ee8a27.png](image/7a209bb85d52e044e4bfddf6fda0105fcb8cb5aa598f554cf0e4659295ee8a27.png)

### Blockdiag[¶](https://docs.ceph.com/en/pacific/dev/documenting/ "Permalink to this headline")

If a use arises, we can integrate [Blockdiag](https://docs.ceph.com/en/pacific/dev/documenting/). It is a Graphviz\-style declarative language for drawing things, and includes:

* [block diagrams](https://docs.ceph.com/en/pacific/dev/documenting/): boxes and arrows \(automatic layout, as opposed to[Ditaa](https://docs.ceph.com/en/pacific/dev/documenting/)\)
* [sequence diagrams](https://docs.ceph.com/en/pacific/dev/documenting/): timelines and messages between them
* [activity diagrams](https://docs.ceph.com/en/pacific/dev/documenting/): subsystems and activities in them
* [network diagrams](https://docs.ceph.com/en/pacific/dev/documenting/): hosts, LANs, IP addresses etc \(with [Cisco icons](https://docs.ceph.com/en/pacific/dev/documenting/) if wanted\)

### Inkscape[¶](https://docs.ceph.com/en/pacific/dev/documenting/ "Permalink to this headline")

You can use Inkscape to generate scalable vector graphics.[https://inkscape.org/en/](https://docs.ceph.com/en/pacific/dev/documenting/) for restructedText documents.

If you generate diagrams with Inkscape, you should commit both the Scalable Vector Graphics \(SVG\) file and export a Portable Network Graphic \(PNG\) file. Reference the PNG file.

By committing the SVG file, others will be able to update the SVG diagrams using Inkscape.

HTML5 will support SVG inline.
