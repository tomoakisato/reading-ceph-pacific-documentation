# 96: Troubleshooting OSDs

**クリップソース:** [96: Troubleshooting OSDs — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-osd/)

# Troubleshooting OSDs[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-osd/ "Permalink to this headline")

OSDのトラブルシューティングを行う前に、まずモニタとネットワークを確認してください。コマンドラインでceph healthまたはceph \-sを実行し、CephがHEALTH\_OKを表示した場合、モニタにクォーラムがあることを意味します。モニタクォーラムがない場合、またはモニタの状態にエラーがある場合は、最初にモニタの問題に対処します。ネットワークは、OSDの動作とパフォーマンスに大きな影響を与える可能性があるため、ネットワークが正常に動作しているかどうかを確認します。ホスト側のドロップパケットとスイッチ側の CRC エラーを探します。

## Obtaining Data About OSDs[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-osd/ "Permalink to this headline")

OSDのトラブルシューティングの良い最初のステップは、OSDを監視している間に収集した情報（例えば、ceph osd tree）に加えて、トポロジー情報を取得することです。

### Ceph Logs[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-osd/ "Permalink to this headline")

デフォルトのパスを変更していない場合、Cephログファイルは/var/log/cephで見つかります。

```
ls /var/log/ceph
```

ログの詳細が十分に表示されない場合は、ロギングレベルを変更することができます。 ログ量が多い場合にCephが十分に機能するようにするための詳細は、「[Logging and Debugging](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-osd/)」を参照してください。

### Admin Socket[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-osd/ "Permalink to this headline")

実行時の情報を取得するには、adminソケットツールを使用します。詳細については、Cephデーモンのソケットをリストアップしてください。

```
ls /var/run/ceph
```

次に、{daemon\-name}を実際のデーモン（例：osd.0）に置き換えて、以下を実行します。

```
ceph daemon osd.0 help
```

または、{socket\-file} を指定することもできます。\(例えば、/var/run/ceph にあるもの\) を使用します。

```
ceph daemon {socket-file} help
```

adminソケットでは、以下のことが可能です。

* 実行中の設定を一覧表示する
* オペレーション履歴をダンプ
* 操作優先度キューの状態をダンプ
* 実行中操作のダンプ
* 性能カウンタのダンプ

### Display Freespace[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-osd/ "Permalink to this headline")

ファイルシステムの問題が発生する場合があります。ファイルシステムの空き容量を表示するには、dfを実行します。

```
df -h
```

df\-\-helpを実行すると、詳しい使い方がわかります。

### I/O Statistics[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-osd/ "Permalink to this headline")

iostatを使用して、I/O関連の問題を特定します。

```
iostat -x
```

### Diagnostic Messages[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-osd/ "Permalink to this headline")

カーネルから診断メッセージを取得するには、less、more、grep、tail と共に dmesg を使用します。

```
dmesg | grep scsi
```

## Stopping w/out Rebalancing[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-osd/ "Permalink to this headline")

定期的に、クラスタのサブセットに対してメンテナンスを実行したり、障害ドメイン（ラックなど）に影響する問題を解決したりする必要がある場合があります。メンテナンスのためにOSDを停止時に、CRUSHが自動的にクラスタをリバランスすることを望まない場合は、最初にクラスタをnooutに設定します。

```
ceph osd set noout
```

Luminous以降のリリースでは、影響を受けるOSDにのみフラグを設定するのが安全です。これは個別に行うことができます

```
ceph osd add-noout osd.0
ceph osd rm-noout  osd.0
```

あるいはCRUSHバケットごと一度にnooutに設定します。 RAMを追加するためにprod\-ceph\-data1701をダウンさせるとします。

```
ceph osd set-group noout prod-ceph-data1701
```

フラグが設定されると、メンテナンス作業が必要な障害ドメイン内のOSDとコロケーションされたCephサービスを停止できます。

```
systemctl stop ceph*.service ceph*.target
```

注：停止したOSD内のPGは、障害ドメイン内の問題に対処している間、デグレードされます。

メンテナンスが完了したら、OSDとその他のデーモンを再起動します。 メンテナンスの一部としてホストをリブートした場合、これらは何もしなくても戻ってくるはずです。

```
sudo systemctl start ceph.target
```

最後に、クラスタ全体のnooutフラグを解除する必要があります。

```
ceph osd unset noout
ceph osd unset-group noout prod-ceph-data1701
```

現在CephがサポートしているほとんどのLinuxディストリビューションは、サービス管理にsystemdを採用していることに注意してください。 その他のOSや古いOSでは、同等のserviceコマンドやstart/stopコマンドを発行する必要がある場合があります。

## OSD Not Running[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-osd/ "Permalink to this headline")

通常であれば、ceph\-osdデーモンを再起動するだけで、クラスタに再接続して回復することができます。

### An OSD Won’t Start[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-osd/ "Permalink to this headline")

クラスタを起動してもOSDが起動しない場合は、以下を確認してください。

* **Configuration File:**
* **Check Paths:**
* **Check Max Threadcount:**

```
sysctl -w kernel.pid_max=4194303
```

* 最大スレッド数を増やすことで問題が解決する場合は、/etc/sysctl.d 以下のファイルまたはマスターの /etc/sysctl.conf ファイル内に kernel.pid\_max 設定を含めることで恒久的にすることが可能です。例えば：

```
kernel.pid_max = 4194303
```

* **Check \`\`nf\_conntrack\`\`:**
* **Kernel Version:**
* **Segment Fault:**

### An OSD Failed[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-osd/ "Permalink to this headline")

ceph\-osdプロセスが死ぬと、生き残ったceph\-osdデーモンはmonにダウンしたように見えることを報告し、monはceph healthコマンドで新しい状態を表示します。

```
ceph health
HEALTH_WARN 1/3 in osds are down
```

具体的には、inとdownのマークがついたOSDがあるたびに警告が表示されます。 ダウンしているOSDを特定するには：

```
ceph health detail
HEALTH_WARN 1/3 in osds are down
osd.0 is down since epoch 23, last address 192.168.106.220:6800/11080
```

or

```
ceph osd tree down
```

ドライブの故障やその他の障害でceph\-osdが機能しない、再起動しない場合、/var/log/cephの下のログファイルにエラーメッセージが表示されるはずです。

デーモンがハートビート障害または suicide timeout で停止した場合、基盤となるドライブまたはファイルシステムが応答していない可能性があります。dmesg と syslog の出力をチェックして、ドライブや他のカーネルエラーを調べます。 タイムスタンプを得るために dmesg \-T のようなものを指定する必要があるかもしれません。さもなければ、古いエラーを新しいエラーと勘違いしやすくなります。

問題がソフトウェアのエラー \(アサーションの失敗やその他の予期せぬエラー\) である場合、上記のようにアーカイブやトラッカーを検索し、明確な修正や既存のバグがない場合は ceph\-devel メールリストに報告します。

### No Free Drive Space[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-osd/ "Permalink to this headline")

Cephは、データを失わないように、fullのOSDに書き込むことを防止します。運用中のクラスタでは、クラスタのOSDとプールがfullレシオに近づくと警告が表示されるはずです。mon osd full ratioのデフォルトは0.95、つまり、クライアントがデータの書き込みを停止する前に容量の95%になります。mon osd backfill full ratioのデフォルトは0.90で、これはバックフィルを開始しない容量の90%です。OSD nearfull ratioのデフォルトは0.85、つまりヘルスアラートを発生させる容量が85%です。

クラスタ内の個々のOSDは、Cephがそれらに割り当てるデータ量が異なることに注意してください。 この利用率は、各OSDに対して、以下の方法で表示できます。

```
ceph osd df
```

クラスタ/プールの全体的な余剰は、以下の方法で確認することができます。

```
ceph df
```

ceph dfが報告する未使用領域の割合ではなく、最も満杯のOSDに細心の注意を払ってください。 プールへの書き込みに失敗するためには、1つの異常なOSDが満杯になるだけで済みます。 ceph dfによって報告された各プールで利用可能なスペースは、特定のプールの一部である最も満杯のOSDに対する比率設定を考慮します。 reweight\-by\-utilizationコマンドを使用して、満杯のOSDから満杯でないOSDへデータを徐々に移動することにより、分布を平坦にすることができます。 Luminousリビジョン以降のCephリリースでは、ceph\-mgr balancerモジュールを使用して、このタスクを自動的かつ効率的に実行することもできます。

比率は調整することができます：

```
ceph osd set-nearfull-ratio <float[0.0-1.0]>
ceph osd set-full-ratio <float[0.0-1.0]>
ceph osd set-backfillfull-ratio <float[0.0-1.0]>
```

fullクラスタの問題は、OSDが小規模または非常にアンバランスなクラスタ内で故障したときに発生する可能性があります。OSDやノードがクラスタのデータの大きな割合を保持している場合、コンポーネントの故障や自然な成長の結果としてnearfullレシオやfullレシオを超えることがあります。小規模クラスタでのOSD障害に対するCephの反応をテストする場合は、十分な空きディスクスペースを残し、OSD fullratio、OSD backfillfullratio、OSD nearfullratioを一時的に下げることを検討する必要があります。

fullなceph\-osdは、ceph healthによって報告されます。

```
ceph health
HEALTH_WARN 1 nearfull osd(s)
```

Or:

```
ceph health detail
HEALTH_ERR 1 full osd(s); 1 backfillfull osd(s); 1 nearfull osd(s)
osd.3 is full at 97%
osd.4 is backfill full at 91%
osd.2 is near full at 87%
```

fullクラスタに対処する最善の方法は、新しい OSD で容量を追加し、クラスタが利用可能になったストレージにデータを再分配できるようにすることです。

レガシーなfilestore OSDがfullで起動できない場合、OSD内のPGディレクトリをいくつか削除して、スペースを取り戻すことができます。

重要：OSD上のPGディレクトリを削除する場合、別のOSD上の同じPGディレクトリを削除しないでください。少なくとも1つのOSDに、少なくとも1つのデータのコピーを維持する必要があります。 これは極端な介入であり、軽々しく行うものではありません。

詳細は、「[Monitor Config Reference](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-osd/)」を参照してください。

## OSDs are Slow/Unresponsive[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-osd/ "Permalink to this headline")

よくある問題として、OSDの動作が遅い、反応が悪いというものがあります。OSDのパフォーマンスの問題を掘り下げる前に、他のトラブルシューティングの可能性が排除されていることを確認してください。たとえば、ネットワークが適切に動作し、OSD が実行されていることを確認します。OSD が回復トラフィックを制限していないかどうかを確認します。

ヒント：新しいバージョンのCephは、OSDの回復によってシステムリソースが使い果たされないようにすることで、回復処理を改善します。これにより、upでinのOSDが利用できなくなったり、速度が低下したりします。

### Networking Issues[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-osd/ "Permalink to this headline")

Cephは分散ストレージシステムなので、OSDのピアリングとレプリケーション、障害からの回復、定期的なハートビートにはネットワークに依存しています。ネットワークの問題は、OSDのレイテンシやOSDのフラッピングの原因になることがあります。詳細については、「[Flapping OSDs](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-osd/)」を参照してください。

CephプロセスおよびCeph依存プロセスが接続、リスニングされていることを確認します。

```
netstat -a | grep ceph
netstat -l | grep ceph
sudo netstat -p | grep ceph
```

ネットワークの統計情報を確認します。

```
netstat -s
```

### Drive Configuration[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-osd/ "Permalink to this headline")

SASまたはSATAストレージ・ドライブには、1つのOSDのみを収容する必要がありますが、NVMeドライブでは2つ以上のOSDを容易に扱うことができます。ジャーナル/メタデータ、オペレーティングシステム、Cephモニタ、シスログログ、他のOSD、非Cephプロセスなど、他のプロセスがドライブを共有すると、読み取りと書き込みのスループットがボトルネックになる可能性があります。

Cephはジャーナリング後に書き込みを認識するため、高速SSDは応答時間を短縮する魅力的なオプションです。特に、レガシーなfilestore OSDにXFSまたはext4ファイルシステムを使用している場合はそうです。対照的に、Btrfs ファイルシステムは書き込みとジャーナルを同時に行うことができます。 \(ただし、本番環境では Btrfs を使用しないことをお勧めします\)。

ドライブをパーティション分割しても、総スループットやシーケンシャルリード/ライトの制限は変わりません。別パーティションでジャーナルを実行することは有効ですが、物理ドライブは別個にした方がよいでしょう。

### Bad Sectors / Fragmented Disk[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-osd/ "Permalink to this headline")

ドライブに不良ブロック、フラグメンテーション、その他のパフォーマンスを大幅に低下させる原因となるエラーがないか確認します。 dmesg、syslog ログ、smartctl \(smartmontools パッケージ\) などのツールが役に立ちます。

### Co\-resident Monitors/OSDs[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-osd/ "Permalink to this headline")

モニタは比較的軽量なプロセスですが、多くのfsync\(\)コールを発行するため、特にモニタがOSDと同じドライブで実行されている場合、他のワークロードに干渉する可能性があります。さらに、OSDと同じホストでモニタを実行する場合、以下のようなパフォーマンスの問題が発生する可能性があります。

* 古いカーネル\(3.0以前\)の実行
* syncfs\(2\) システムコールのないカーネルの実行

このような場合、同じホスト上で動作する複数のOSDは、多くのコミットを行うことで互いに足を引っ張り合うことになります。それがバースト的な書き込みにつながることが多いのです。

### Co\-resident Processes[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-osd/ "Permalink to this headline")

OSDと同じハードウェアで動作しながらCephにデータを書き込むクラウドベースのソリューション、仮想マシン、その他のアプリケーションなどの同居プロセスをスピンアップ（収束）すると、大幅なOSDレイテンシーが発生する可能性があります。一般的に、ホストをCephでの使用に最適化し、他のホストを他の処理に使用することをお勧めします。Cephの操作を他のアプリケーションから分離するという実践は、パフォーマンスの向上に役立ち、トラブルシューティングとメンテナンスを合理化できる可能性があります。

### Logging Levels[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-osd/ "Permalink to this headline")

問題を追跡するためにロギングレベルを上げ、その後ロギングレベルを下げるのを忘れた場合、OSDはディスクに大量のログを記録している可能性があります。ログレベルを高く保つつもりであれば、ログ用のデフォルトパス（/var/log/ceph/$cluster\-$name.log）にドライブをマウントすることを検討することができます。

### Recovery Throttling[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-osd/ "Permalink to this headline")

設定によっては、Cephがパフォーマンスを維持するためにリカバリレートを低下させたり、リカバリがOSDパフォーマンスに影響を与えるほどリカバリレートを増加させたりする場合があります。OSDがリカバリしているかどうかを確認します。

### Kernel Version[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-osd/ "Permalink to this headline")

実行中のカーネルのバージョンを確認してください。古いカーネルでは、Cephが性能向上のために依存する新しいバックポートがされない場合があります。

### Kernel Issues with SyncFS[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-osd/ "Permalink to this headline")

ホストごとに1つのOSDを実行して、パフォーマンスが改善されるかどうか試してみてください。古いカーネルは syncfs\(2\) をサポートするのに十分な最新のバージョンの glibc を持っていないかもしれません。

### Filesystem Issues[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-osd/ "Permalink to this headline")

現在、BlueStoreバックエンドを使用したクラスタのデプロイを推奨しています。Luminous以前のリリースを実行している場合、または以前のFilestoreバックエンドでOSDをデプロイする特別な理由がある場合、XFSを推奨します。

Btrfsやext4は使用しないことをお勧めします。 Btrfs ファイルシステムは多くの魅力的な機能を持っていますが、バグによりパフォーマンスの問題や、ENOSPC のスプリアスエラーが発生する可能性があります。 xattrの制限により、RGWに必要な長いオブジェクト名のサポートが壊れてしまうため、filestore OSDにext4を使用することはお勧めしません。

詳しくは、[Filesystem Recommendations](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-osd/)をご覧ください。 

### Insufficient RAM[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-osd/ "Permalink to this headline")

OSDデーモンあたり最低4GBのRAMを推奨し、6\-8GBから切り上げていくことをお勧めします。 通常の運用では、ceph\-osdプロセスはこの量のほんの一部しか使用していないことに気づくかもしれません。未使用のRAMがあると、余ったRAMを共同常駐アプリケーションに使用したり、各ノードのメモリ容量をケチったりしたくなるものです。 しかし、OSDがリカバリを経験すると、そのメモリ使用量は急増します。利用可能なRAMが不十分な場合、OSDのパフォーマンスは大幅に低下し、デーモンはクラッシュするか、Linux OOMKillerによって強制終了されることさえあります。

### Blocked Requests or Slow Requests[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-osd/ "Permalink to this headline")

ceph\-osdデーモンの応答が遅い場合、時間がかかりすぎているOPSを指摘するメッセージがログに記録されます。 警告のしきい値のデフォルトは30秒で、osd op complaint timeの設定で変更可能です。 この現象が発生すると、クラスタログにメッセージが記録されます。

レガシーバージョンのCephは、古いリクエストに対して文句を言います。

```
osd.0 192.168.106.220:6800/18813 312 : [WRN] old request osd_op(client.5099.0:790 fatty_26485_object789 [write 0~4096] 2.5e54f643) v4 received at 2012-03-06 15:42:56.054801 currently waiting for sub ops
```

Cephの新しいバージョンでは、要求が遅いと文句を言われます。

```
{date} {osd.num} [WRN] 1 slow requests, 1 included below; oldest blocked for > 30.005692 secs
{date} {osd.num}  [WRN] slow request 30.005692 seconds old, received at {date-time}: osd_op(client.4240.0:8 benchmark_data_ceph-1_39426_object7 [write 0~4194304] 0.69848840) v4 currently waiting for subops from [610]
```

考えられる原因としては、以下のようなものがあります。

* ドライブの故障（dmesg出力を確認する）
* カーネルファイルシステムのバグ \(dmesgの出力を確認\)
* クラスタに過負荷がかかっている（システム負荷、iostatなどを確認する）
* ceph\-osdデーモンのバグ

考えられる解決策

* CephホストからVMを削除する
* カーネルをアップグレードする
* Cephのアップグレード
* OSDのリスタート
* 故障や不具合のある部品の交換

### Debugging Slow Requests[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-osd/ "Permalink to this headline")

ceph daemon osd.\<id\> dump\_historic\_ops または ceph daemon osd.\<id\> dump\_ops\_in\_flight を実行すると、一連の操作と各操作が通過したイベントのリストが表示されます。これらを以下に簡単に説明します。

Messengerレイヤーのイベント：

* header\_read: メッセンジャーが最初にメッセージを読み上げ始めたとき
* throttled: メッセンジャーがメッセージをメモリに読み込むために、メモリスロットル空間を獲得しようとしたとき
* all\_read: メッセンジャーがメッセージを読み終えたとき
* dispatched: メッセンジャーがOSDにメッセージを伝えたとき
* initiated: これは header\_read と同じです。両方が存在するのは歴史的な奇遇です。

OSDがOPSを処理する際のイベント：

* queued\_for\_pg: OPはPGが処理するための待ち行列に入れられた
* reached\_pg: PGはOPをやり始めた
* waitingfor\*: OPは、処理を進める前に他の作業が完了するのを待っています\(たとえば、新しいOSDMap、そのオブジェクトターゲットのスクラブ、PGのピアリングの終了など、すべてメッセージで指定されたとおり\)。
* started: このOPは、OSDがやるべきこととして受け入れられ、現在実行中
* waiting for subops from: レプリカOSDにOPが送信された

FileStoreからのイベント：

* commit\_queued\_for\_journal\_write: OPはFileStoreに委ねられた
* write\_thread\_in\_journal\_buffer: OPはジャーナルのバッファにあり、（次のディスク書き込みとして）永続化されるのを待っている
* journaled\_completion\_queued: OPはディスクにジャーナルされ、そのコールバックは呼び出しのためにキューに入れられた

データが下位ストレージに渡された後のOSDからのイベント：

* op\_commit: OPはプライマリOSDによってコミット（＝ジャーナルへの書き込み）されている
* op\_applied: OPはプライマリのバッキングFSにwrite\(\)された（つまり、メモリに適用されたがディスクにはフラッシュアウトされていない）
* sub\_op\_applied: op\_applied、ただしレプリカの「subop」に対して
* sub\_op\_committed: op\_commit、ただしレプリカのsubopに対して（ECプールのみ）
* sub\_op\_commit\_rec/sub\_op\_apply\_recfrom\<X\>: プライマリは、上記を聞いたときにこれをマークするが、特定のレプリカ（つまり\<X\>）についてである
* commit\_sent: クライアント（subopの場合はプライマリーOSD）に返信を送った

これらのイベントの多くは、一見冗長に見えるが、内部コードの重要な境界を越えている（例えば、ロックを経て新しいスレッドにデータを渡すなど）。

## Flapping OSDs[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-osd/ "Permalink to this headline")

OSDがピアリングし、ハートビートをチェックするとき、クラスタ（バックエンド）ネットワークが利用可能であれば、それを使用します。詳しくは、[Monitor/OSD Interaction](https://docs.ceph.com/en/pacific/rados/troubleshooting/troubleshooting-osd/)を参照してください。

従来は、パブリック（フロントエンド）とプライベート（クラスタ／バックエンド／レプリケーション）のネットワークを分けて考えることを推奨してきました。

1. ハートビートとレプリケーション/リカバリーのトラフィック（プライベート）とクライアントおよびOSD \<\-\> Monのトラフィック（パブリック）を分離すること。 これにより、一方が他方をDoS攻撃する（その結果、障害が連鎖的に発生する）のを防ぐことができる可能性があります。
2. パブリック・トラフィックとプライベート・トラフィックの両方でのスループット追加

一般的なネットワーク技術が100Mb/sや1Gb/sであった頃、この分離はしばしば重要でした。 今日の10Gb/s、40Gb/s、25/50/100Gb/sのネットワークでは、上記の容量に関する懸念は軽減されるか、排除されることが多くなっています。例えば、OSDノードに2つのネットワークポートがある場合、1つをパブリックネットワークに、もう1つをプライベートネットワークに充てることは、パスの冗長性がないことを意味します。これは、クラスタやクライアントに大きな影響を与えることなく、ネットワークのメンテナンスや故障を切り抜ける能力を低下させます。 ボンディング（LACP）またはイコールコストルーティング（FRRなど）を使用すると、スループットヘッドルームの増加、フォールトトレランス、OSDフラッピングの減少というメリットを享受することができます。

パブリックネットワークが正常に動作しているときに、プライベートネットワーク（あるいは単一のホストリンク）が故障したり劣化したりすると、OSDはこの状況にうまく対処できないことがあります。このような場合、OSDはパブリックネットワークを使用して、自分自身をマークupしながら、お互いをモニターに報告しあいます。その後、モニタは、再びパブリック・ネットワークで、影響を受けるOSDをマークdowしたクラスタ・マップを更新して送信します。これらのOSDはモニタに「私はまだ死んでいない！」と返信し、このサイクルが繰り返されます。 私たちはこのシナリオを「フラッピング」と呼んでいますが、これを分離して修正するのは困難です。 プライベートネットワークがないため、このような厄介な現象は避けられます。OSDは通常、フラッピングなしにアップまたはダウンのどちらかになります。

何かでOSDが「バタバタ」する（マークダウンとマークアップを繰り返す）場合、モニタの状態を一時的にフリーズさせることでバタつきを強制的に停止させることができます。

```
ceph osd set noup      # prevent OSDs from getting marked up
ceph osd set nodown    # prevent OSDs from getting marked down
```

これらのフラグは、osdmapに記録されます。

```
ceph osd dump | grep flags
flags no-up,no-down
```

フラグをクリアするには：

```
ceph osd unset noup
ceph osd unset nodown
```

他にnoinとnooutという2つのフラグがサポートされており、ブートOSDがマークin（割り当てデータ）される、あるいはOSDが最終的にマークoutされるのを防ぎます（mon osd down out intervalの現在の値がどうであるかに関係なく）。注：noup, noout, nodownは、フラグがクリアされると、ブロックしていた動作が間もなく発生するという意味で、一時的なものです。 一方、noinフラグは、ブート時にOSDがマークinされるのを防ぎ、フラグが設定されている間に開始したすべてのデーモンは、その状態を維持することになります。

注意：フラッピングの原因と影響は、mon\_osd\_down\_out\_subtree\_limit、 mon\_osd\_reporter\_subtree\_level および mon\_osd\_min\_down\_reporters を慎重に調整すれば、ある程度軽減することが可能です。最適な設定の導出は、クラスタサイズ、トポロジ、使用するCephリリースに依存します。これらの相互作用は微妙で、このドキュメントの範囲外です。
