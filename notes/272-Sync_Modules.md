# 272: Sync Modules

**クリップソース:** [272: Sync Modules — Ceph Documentation](https://docs.ceph.com/en/pacific/radosgw/sync-modules/)

# Sync Modules[¶](https://docs.ceph.com/en/pacific/radosgw/sync-modules/ "Permalink to this headline")

Krakenバージョンの新機能。

Jewelで導入されたRGWの[マルチサイト](https://docs.ceph.com/en/pacific/radosgw/multisite/#multisite)機能では、複数のゾーンを作成し、そのゾーン間でデータやメタデータをミラーリングすることができました。SyncModulesは、マルチサイトのフレームワークの上に構築され、データとメタデータを異なる外部層に転送することを可能にします。同期モジュールは、データの変更が発生するたびに、一連のアクションを実行できるようにします（バケットやユーザの作成などのメタデータ操作も、データの変更と見なされます）。rgw マルチサイトの変更は最終的にリモートサイトでも一貫しているので、変更は非同期に伝搬されます。これにより、オブジェクトストレージを外部のクラウドクラスタやテープドライブを使ったカスタムバックアップソリューションにバックアップしたり、ElasticSearchでメタデータをインデックス化したりといったユースケースのロックを解除できます。

同期モジュールの設定は、ゾーンに対してローカルです。同期モジュールは、ゾーンがデータをエクスポートするか、他のゾーンで変更された データを消費のみできるかを決定します。luminousでは、[elasticsearch](https://docs.ceph.com/en/pacific/radosgw/elastic-sync-module)、rgw（ゾーン間のデータを同期するデフォルトの同期プラグイン）、log（リモートゾーンで発生したメタデータ操作を記録する簡単な同期プラグイン）がサポートされています。以下の文書は、[elasticsearch sync モジュール](https://docs.ceph.com/en/pacific/radosgw/elastic-sync-module)を使用するゾーンの例で書かれていますが、どの sync プラグインを設定する場合でも同様の手順となります。

* [ElasticSearch Sync Module](https://docs.ceph.com/en/pacific/radosgw/sync-modules/)
* [Cloud Sync Module](https://docs.ceph.com/en/pacific/radosgw/sync-modules/)
* [PubSub Module](https://docs.ceph.com/en/pacific/radosgw/sync-modules/)
* [Archive Sync Module](https://docs.ceph.com/en/pacific/radosgw/sync-modules/)

## Requirements and Assumptions[¶](https://docs.ceph.com/en/pacific/radosgw/sync-modules/ "Permalink to this headline")

[マルチサイト](https://docs.ceph.com/en/pacific/radosgw/multisite/#multisite)のドキュメントで説明されているように、us\-eastとus\-westの2つのゾーンの単純なマルチサイト構成を想定し、他のサイトからのメタデータを処理だけするゾーンである3番目のゾーンus\-east\-esを追加しましょう。 このゾーンは、us\-eastと同じcephクラスタでも異なるcephクラスタでも構いません。 このゾーンは他のゾーンからのメタデータを消費だけし、このゾーンのRGWはエンドユーザーの要求を直接処理しません。

## Configuring Sync Modules[¶](https://docs.ceph.com/en/pacific/radosgw/sync-modules/ "Permalink to this headline")

[マルチサイト](https://docs.ceph.com/en/pacific/radosgw/multisite/#multisite)のドキュメントと同様に、3つ目のゾーンを作成します：

```
# radosgw-admin zone create --rgw-zonegroup=us --rgw-zone=us-east-es 
--access-key={system-key} --secret={secret} --endpoints=http://rgw-es:80
```

同期モジュールは、以下の方法でこのゾーンに設定することができます：

```
# radosgw-admin zone modify --rgw-zone={zone-name} --tier-type={tier-type} --tier-config={set of key=value pairs}
```

例えば、elasticsearchの同期モジュールでは：

```
# radosgw-admin zone modify --rgw-zone={zone-name} --tier-type=elasticsearch 
                            --tier-config=endpoint=http://localhost:9200,num_shards=10,num_replicas=1
```

サポートされている様々なtier\-configオプションについては、[elasticsearch sync module](https://docs.ceph.com/en/pacific/radosgw/elastic-sync-module) ドキュメントを参照してください。

最後にperiodを更新します：

```
# radosgw-admin period update --commit
```

次に、ゾーン内のradosgwを起動します：

```
# systemctl start ceph-radosgw@rgw.`hostname -s`
# systemctl enable ceph-radosgw@rgw.`hostname -s`
```
