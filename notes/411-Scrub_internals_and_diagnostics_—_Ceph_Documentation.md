# 411: Scrub internals and diagnostics — Ceph Documentation

 # Scrub internals and diagnostics[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/scrub/ "Permalink to this headline")

## Scrubbing Behavior Table[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/scrub/ "Permalink to this headline")

|Flags |none |noscrub |nodeep\_scrub |noscrub/nodeep\_scrub |
|------------------------------------------------|----|-------|-------------|---------------------|
|Periodic tick |S |X |S |X |
|Periodic tick after osd\_deep\_scrub\_interval |D |D |S |X |
|Initiated scrub |S |S |S |S |
|Initiated scrub after osd\_deep\_scrub\_interval |D |D |S |S |
|Initiated deep scrub |D |D |D |D |

* X = Do nothing
* S = Do regular scrub
* D = Do deep scrub

## State variables[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/scrub/ "Permalink to this headline")

* Periodic tick state is `!must_scrub && !must_deep_scrub && !time_for_deep`
* Periodic tick after `osd_deep_scrub_interval state is !must_scrub && !must_deep_scrub && time_for_deep`
* Initiated scrub state is `must_scrub && !must_deep_scrub && !time_for_deep`
* Initiated scrub after `osd_deep_scrub_interval` state is `must_scrub && !must_deep_scrub && time_for_deep`
* Initiated deep scrub state is `must_scrub && must_deep_scrub`

## Scrub Reservations[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/scrub/ "Permalink to this headline")

An OSD daemon command dumps total local and remote reservations:

```
ceph daemon osd.<id> dump_scrub_reservations
```
