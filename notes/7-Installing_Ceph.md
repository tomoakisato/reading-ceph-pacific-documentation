# 7: Installing Ceph

**クリップソース:** [7: Installing Ceph — Ceph Documentation](https://docs.ceph.com/en/pacific/install/)

# Installing Ceph¶

Cephのインストールにはいくつかの方法があります。 自分のニーズに合った方法を選択してください。

## **Recommended methods[¶](https://docs.ceph.com/en/pacific/install/#recommended-methods "Permalink to this headline")**

[Cephadm](https://docs.ceph.com/en/pacific/cephadm/#cephadm)は、コンテナとsystemdを使用して、CLIとダッシュボードGUIとの緊密な統合により、Cephクラスタのインストールと管理を行います。

* cephadmはOctopusとそれ以降のリリースにのみ対応しています。
* cephadmは、新しいオーケストレーションAPIと完全に統合されており、クラスターのデプロイメントを管理するための新しいCLIとダッシュボードの機能を完全にサポートしています。
* cephadmには、コンテナのサポート（podmanまたはdocker）とPython 3が必要です。

[Rook](https://rook.io/)は、Kubernetesで動作するCephクラスタの導入と管理を行うとともに、KubernetesのAPIを介してストレージリソースの管理とプロビジョニングを行うことができます。 CephをKubernetesで実行する場合や、既存のCephストレージクラスタをKubernetesに接続する場合は、Rookをお勧めします。

* Rookは、Nautilusと新しいリリースのCephにのみ対応しています。
* Rookは、Kubernetes上でCephを実行する場合や、Kubernetesクラスタを既存の（外部の）Cephクラスタに接続する場合に推奨される方法です。
* Rookは、新しいオーケストレーターAPIに対応しています。CLIとダッシュボードの新しい管理機能を完全にサポートしています。

## **Other methods[¶](https://docs.ceph.com/en/pacific/install/#other-methods "Permalink to this headline")**

[ceph\-ansible](https://docs.ceph.com/ceph-ansible/)は、Ansibleを使用してCephクラスターを展開・管理します。

* ceph\-ansibleは広く普及しています。
* ceph\-ansibleは、NautliusやOctopusで導入された新しいオーケストレーターAPIとは統合されていないため、より新しい管理機能やダッシュボードの統合は利用できません。

[ceph\-deploy](https://docs.ceph.com/projects/ceph-deploy/en/latest/)は、クラスターを素早く展開するためのツールです。

_Important_

_eph\-deployは積極的にメンテナンスされなくなりました。Nautilusよりも新しいCephのバージョンではテストされていません。RHEL8、CentOS 8、またはそれ以降のオペレーティングシステムはサポートしていません。_

[DeepSea](https://github.com/SUSE/DeepSea)はSaltを使ってCephをインストールします。

[jaas.ai/ceph\-mon](https://jaas.ai/ceph-mon)は、Jujuを使用してCephをインストールします。

[github.com/openstack/puppet\-ceph](https://github.com/openstack/puppet-ceph)は、Puppet経由でCephをインストールします。

Cephは[手動でインストール](https://docs.ceph.com/en/pacific/install/index_manual/#install-manual)することもできます。[8: Installation \(Manual\)](8-Installation_\(Manual\).md)

## **Windows[¶](https://docs.ceph.com/en/pacific/install/#windows "Permalink to this headline")**

Windowsのインストールについては、[Windowsのインストールガイド](https://docs.ceph.com/en/pacific/install/windows-install)をご参照ください。
