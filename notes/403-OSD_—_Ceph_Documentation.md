# 403: OSD — Ceph Documentation

 # OSD[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/osd_overview/ "Permalink to this headline")

## Concepts[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/osd_overview/ "Permalink to this headline")

_Messenger_See src/msg/Messenger.h

Handles sending and receipt of messages on behalf of the OSD. The OSD uses two messengers:

> 1. cluster\_messenger \- handles traffic to other OSDs, monitors
> 2. client\_messenger \- handles client traffic
> > This division allows the OSD to be configured with different interfaces for client and cluster traffic.

_Dispatcher_See src/msg/Dispatcher.h

> OSD implements the Dispatcher interface. Of particular note is ms\_dispatch, which serves as the entry point for messages received via either the client or cluster messenger. Because there are two messengers, ms\_dispatch may be called from at least two threads. The osd\_lock is always held during ms\_dispatch.

_WorkQueue_See src/common/WorkQueue.h

The WorkQueue class abstracts the process of queueing independent tasks for asynchronous execution. Each OSD process contains workqueues for distinct tasks:

> 1. OpWQ: handles ops \(from clients\) and subops \(from other OSDs\). Runs in the op\_tp threadpool.
> 2. PeeringWQ: handles peering tasks and pg map advancement Runs in the op\_tp threadpool. See Peering
> 3. CommandWQ: handles commands \(pg query, etc\) Runs in the command\_tp threadpool.
> 4. RecoveryWQ: handles recovery tasks. Runs in the recovery\_tp threadpool.
> 5. SnapTrimWQ: handles snap trimming Runs in the disk\_tp threadpool. See SnapTrimmer
> 6. ScrubWQ: handles primary scrub path Runs in the disk\_tp threadpool. See Scrub
> 7. ScrubFinalizeWQ: handles primary scrub finalize Runs in the disk\_tp threadpool. See Scrub
> 8. RepScrubWQ: handles replica scrub path Runs in the disk\_tp threadpool See Scrub
> 9. RemoveWQ: Asynchronously removes old pg directories Runs in the disk\_tp threadpool See PGRemoval

_ThreadPool_See src/common/WorkQueue.h See also above.

There are 4 OSD threadpools:

> 1. op\_tp: handles ops and subops
> 2. recovery\_tp: handles recovery tasks
> 3. disk\_tp: handles disk intensive tasks
> 4. command\_tp: handles commands

_OSDMap_See src/osd/OSDMap.h

The crush algorithm takes two inputs: a picture of the cluster with status information about which nodes are up/down and in/out, and the pgid to place. The former is encapsulated by the OSDMap. Maps are numbered by _epoch_ \(epoch\_t\). These maps are passed around within the OSD as std::tr1::shared\_ptr\<const OSDMap\>.

See MapHandling

_PG_See src/osd/PG.\* src/osd/PrimaryLogPG.\*

Objects in rados are hashed into _PGs_ and _PGs_ are placed via crush onto OSDs. The PG structure is responsible for handling requests pertaining to a particular _PG_ as well as for maintaining relevant metadata and controlling recovery.

_OSDService_See src/osd/OSD.cc OSDService

The OSDService acts as a broker between PG threads and OSD state which allows PGs to perform actions using OSD services such as workqueues and messengers. This is still a work in progress. Future cleanups will focus on moving such state entirely from the OSD into the OSDService.

## Overview[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/osd_overview/ "Permalink to this headline")

> See src/ceph\_osd.cc
> 
> The OSD process represents one leaf device in the crush hierarchy. There might be one OSD process per physical machine, or more than one if, for example, the user configures one OSD instance per disk.
