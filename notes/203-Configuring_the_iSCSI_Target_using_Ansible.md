# 203: Configuring the iSCSI Target using Ansible

**クリップソース:** [203: Configuring the iSCSI Target using Ansible — Ceph Documentation](https://docs.ceph.com/en/pacific/rbd/iscsi-target-ansible/)

# Configuring the iSCSI Target using Ansible[¶](https://docs.ceph.com/en/pacific/rbd/iscsi-target-ansible/ "Permalink to this headline")

Ceph iSCSIゲートウェイは、iSCSIターゲットノードであると同時に、Cephクライアントノードでもあります。Ceph iSCSIゲートウェイは、専用ノードにプロビジョニングするか、Ceph Object Store Disk（OSD）ノードにコロケーションすることが可能です。次の手順では、Ceph iSCSIゲートウェイのインストールと基本的な動作の設定を行います。

**Requirements:**

* Ceph Luminous \(12.2.x\)またはそれ以降のクラスタが動作していること
* Red Hat Enterprise Linux/CentOS 7.5（以降）、Linux kernel v4.16（以降）
* すべてのiSCSIゲートウェイノードにインストールされたceph\-iscsiパッケージ

**Installation:**

1. Ansibleインストーラーノード（管理ノードまたは専用のデプロイメントノード）で、次の手順を実行します：

rootユーザで、ceph\-ansibleパッケージをインストールします：

```
# yum install ceph-ansible
```

/etc/ansible/hosts ファイルに、gatewayグループのエントリを追加します：

```
[iscsigws]
ceph-igw-1
ceph-igw-2
```

注：iSCSIゲートウェイをOSDノードとコロケーションさせる場合、OSDノードを\[iscigws\]セクションに追加します。

**Configuration:**

ceph\-ansibleパッケージは、/usr/share/ceph\-ansible/group\_vars/ディレクトリにiscigws.yml.sampleというファイルを配置します。このサンプルファイルのコピーをiscigws.ymlという名前で作成します。以下のAnsible変数と説明文を確認し、適宜更新します。高度な変数の完全なリストについては、iscsigws.yml.sampleを参照してください。

|Variable               |Meaning/Purpose                                                                                                                                                                                                                                                                  |
|-----------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|seed\_monitor          |各ゲートウェイは、radosとrbdの呼び出しのために、cephクラスタにアクセスする必要がある。これは、iSCSIゲートウェイに適切な/etc/ceph/ディレクトリが定義されている必要があることを意味する。seed\_monitorホストは、iSCSIゲートウェイの/etc/ceph/ディレクトリに入力するために使用される|
|cluster\_name          |カスタムストレージクラスター名を定義                                                                                                                                                                                                                                             |
|gateway\_keyring       |カスタムキーリング名を定義                                                                                                                                                                                                                                                       |
|deploy\_settings       |trueに設定すると、playbookが実行されたときに設定をデプロイする                                                                                                                                                                                                                   |
|perform\_system\_checks|これは、各ゲートウェイのマルチパスとlvmのコンフィギュレーション設定をチェックするブーリアン値。multipathdとlvmが適切に設定されていることを確認するために、少なくとも最初の実行ではtrueに設定する必要がある                                                                       |
|api\_user              |APIのユーザー名。デフォルトはadmin                                                                                                                                                                                                                                               |
|api\_password          |APIを利用するためのパスワード。デフォルトはadmin                                                                                                                                                                                                                                 |
|api\_port              |APIを利用するためのTCPポート番号。デフォルトは5000                                                                                                                                                                                                                               |
|api\_secure            |TLSを使用しなければならない場合はTrueを指定する。デフォルトはfalse。Trueの場合、ユーザーは必要な証明書と鍵のファイルを作成する必要がある。詳細については、gwcli man ファイルを参照                                                                                               |
|trusted\_ip\_list      |APIにアクセスできるIPv4またはIPv6アドレスのリスト。デフォルトでは、iSCSIゲートウェイノードのみがアクセス権を有する                                                                                                                                                               |

**Deployment:**

Ansibleインストーラーノードで、次の手順を実行します。

1. rootで、Ansibleのplaybookを実行します：

```
# cd /usr/share/ceph-ansible
# ansible-playbook site.yml --limit iscsigws
```

注意: Ansible playbookは、RPM 依存性、デーモンのセットアップ、gwcli のインストールを行い、iSCSI ターゲットの作成と RBD イメージを LUN としてエクスポートするために使用できるようにします。過去のバージョンでは、iscigws.yml で iSCSI ターゲットやクライアント、イメージ、LUN などのオブジェクトを定義できましたが、これはもうサポートされていません。

1. iSCSIゲートウェイノードから設定を確認します：

```
# gwcli ls
```

注：gwcliツールを使用してゲートウェイ、LUN、クライアントを作成するには、「[コマンドラインインターフェイスを使用したiSCSIターゲットの設定](https://docs.ceph.com/en/pacific/rbd/iscsi-target-cli)」セクションを参照してください。

重要：targetcli ツールを使用して設定を変更しようとすると、ALUA の設定ミスやパスのフェイルオーバーなどの問題が発生します。データが破損したり、iSCSI ゲートウェイ間で構成が不一致になったり、WWN 情報が不一致になり、クライアントのマルチパスの問題につながる可能性があります。

**Service Management:**

ceph\-iscsiパッケージは、構成管理ロジックと、rbd\-target\-apiというSystemdサービスをインストールします。Systemdサービスが有効な場合、rbd\-target\-apiはブート時に起動し、LinuxのI/O状態を復元します。Ansible playbook では、デプロイ時にターゲットサービスを無効にしています。以下は、rbd\-target\-api Systemd サービスと対話したときの結果です。

```
# systemctl <start|stop|restart|reload> rbd-target-api
```

* reload要求により、rbd\-target\-apiに設定を再読み込みさせ、現在の実行環境に適用します。変更はAnsibleからすべてのiSCSIゲートウェイノードに並行してデプロイされるため、通常、これは必要ありません。
    reload
* stopリクエストは、ゲートウェイのポータルインターフェースを閉じ、クライアントとの接続を切断し、カーネルから現在のLIOの設定を消去します。これにより、iSCSIゲートウェイはクリーンな状態に戻されます。クライアントが切断されると、アクティブなI/Oは、クライアント側のマルチパスレイヤーによって、他のiSCSIゲートウェイに再スケジュールされます。
    stop

**Removing the Configuration:**

ceph\-ansibleパッケージは、iSCSIゲートウェイの設定と関連するRBDイメージを削除するためのAnsible playbookを提供します。Ansible playbookは、/usr/share/ceph\-ansible/purge\_gateways.ymlです。この Ansible playbookを実行すると、実行するパージの種類を入力するプロンプトが表示されます：

_lio_ :

このモードでは、定義されているすべてのiSCSIゲートウェイでLIOの構成がパージされます。作成されたディスクは、Cephストレージクラスタ内でそのまま残されます。

_all_ :

allを選択すると、iSCSIゲートウェイ環境内で定義されたすべてのRBDイメージとともにLIO構成が削除されますが、その他の無関係なRBDイメージは削除されません。正しいモードが選択されていることを確認し、この操作でデータが削除されることを確認します。

警告： パージ操作は、iSCSIゲートウェイ環境に対して破壊的なアクションです。

警告： RBDイメージにスナップショットまたはクローンがあり、Ceph iSCSIゲートウェイ経由でエクスポートされている場合、パージ操作は失敗します。

```
[root@rh7-iscsi-client ceph-ansible]# ansible-playbook purge_gateways.yml
Which configuration elements should be purged? (all, lio or abort) [abort]: all

PLAY [Confirm removal of the iSCSI gateway configuration] *********************

GATHERING FACTS ***************************************************************
ok: [localhost]

TASK: [Exit playbook if user aborted the purge] *******************************
skipping: [localhost]

TASK: [set_fact ] *************************************************************
ok: [localhost]

PLAY [Removing the gateway configuration] *************************************

GATHERING FACTS ***************************************************************
ok: [ceph-igw-1]
ok: [ceph-igw-2]

TASK: [igw_purge | purging the gateway configuration] *************************
changed: [ceph-igw-1]
changed: [ceph-igw-2]

TASK: [igw_purge | deleting configured rbd devices] ***************************
changed: [ceph-igw-1]
changed: [ceph-igw-2]

PLAY RECAP ********************************************************************
ceph-igw-1                 : ok=3    changed=2    unreachable=0    failed=0
ceph-igw-2                 : ok=3    changed=2    unreachable=0    failed=0
localhost                  : ok=2    changed=0    unreachable=0    failed=0

```
