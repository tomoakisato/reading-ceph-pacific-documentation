# 404: OSD Throttles — Ceph Documentation

 # OSD Throttles[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/osd_throttles/ "Permalink to this headline")

There are three significant throttles in the FileStore OSD back end: wbthrottle, op\_queue\_throttle, and a throttle based on journal usage.

## WBThrottle[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/osd_throttles/ "Permalink to this headline")

The WBThrottle is defined in src/os/filestore/WBThrottle.\[h,cc\] and included in FileStore as FileStore::wbthrottle. The intention is to bound the amount of outstanding IO we need to do to flush the journal. At the same time, we don’t want to necessarily do it inline in case we might be able to combine several IOs on the same object close together in time. Thus, in FileStore::\_write, we queue the fd for asynchronous flushing and block in FileStore::\_do\_op if we have exceeded any hard limits until the background flusher catches up.

The relevant config options are filestore\_wbthrottle\*. There are different defaults for XFS and Btrfs. Each set has hard and soft limits on bytes \(total dirty bytes\), ios \(total dirty ios\), and inodes \(total dirty fds\). The WBThrottle will begin flushing when any of these hits the soft limit and will block in throttle\(\) while any has exceeded the hard limit.

Tighter soft limits will cause writeback to happen more quickly, but may cause the OSD to miss oportunities for write coalescing. Tighter hard limits may cause a reduction in latency variance by reducing time spent flushing the journal, but may reduce writeback parallelism.

## op\_queue\_throttle[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/osd_throttles/ "Permalink to this headline")

The op queue throttle is intended to bound the amount of queued but uncompleted work in the filestore by delaying threads calling queue\_transactions more and more based on how many ops and bytes are currently queued. The throttle is taken in queue\_transactions and released when the op is applied to the file system. This period includes time spent in the journal queue, time spent writing to the journal, time spent in the actual op queue, time spent waiting for the wbthrottle to open up \(thus, the wbthrottle can push back indirectly on the queue\_transactions caller\), and time spent actually applying the op to the file system. A BackoffThrottle is used to gradually delay the queueing thread after each throttle becomes more than filestore\_queue\_low\_threshhold full \(a ratio of filestore\_queue\_max\_\(bytes|ops\)\). The throttles will block once the max value is reached \(filestore\_queue\_max\_\(bytes|ops\)\).

The significant config options are: filestore\_queue\_low\_threshhold filestore\_queue\_high\_threshhold filestore\_expected\_throughput\_ops filestore\_expected\_throughput\_bytes filestore\_queue\_high\_delay\_multiple filestore\_queue\_max\_delay\_multiple

While each throttle is at less than low\_threshold of the max, no delay happens. Between low and high, the throttle will inject a per\-op delay \(per op or byte\) ramping from 0 at low to high\_delay\_multiple/expected\_throughput at high. From high to 1, the delay will ramp from high\_delay\_multiple/expected\_throughput to max\_delay\_multiple/expected\_throughput.

filestore\_queue\_high\_delay\_multiple and filestore\_queue\_max\_delay\_multiple probably do not need to be changed.

Setting these properly should help to smooth out op latencies by mostly avoiding the hard limit.

See FileStore::throttle\_ops and FileSTore::thottle\_bytes.

## journal usage throttle[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/osd_throttles/ "Permalink to this headline")

See src/os/filestore/JournalThrottle.h/cc

The intention of the journal usage throttle is to gradually slow down queue\_transactions callers as the journal fills up in order to smooth out hiccup during filestore syncs. JournalThrottle wraps a BackoffThrottle and tracks journaled but not flushed journal entries so that the throttle can be released when the journal is flushed. The configs work very similarly to the op\_queue\_throttle.

The significant config options are: journal\_throttle\_low\_threshhold journal\_throttle\_high\_threshhold filestore\_expected\_throughput\_ops filestore\_expected\_throughput\_bytes journal\_throttle\_high\_multiple journal\_throttle\_max\_multiple

```
                                                                                                   Messenger throttle (number and size)
         |-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
                                                                     FileStore op_queue throttle (number and size, includes a soft throttle based on filestore_expected_throughput_(ops|bytes))
                                                                |--------------------------------------------------------|
                                                                                                                                                                   WBThrottle
                                                                                                                       |---------------------------------------------------------------------------------------------------------|
                                                                                                                                                                 Journal (size, includes a soft throttle based on filestore_expected_throughput_bytes)
                                                                 |-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
                                                                                                                       |----------------------------------------------------------------------------------------------------> flushed ----------------> synced
                                                                                                                       |
Op: Read Header --DispatchQ--> OSD::_dispatch --OpWQ--> PG::do_request --journalq--> Journal --FileStore::OpWQ--> Apply Thread --Finisher--> op_applied ------------------------------------------------------------->  Complete
                                              |                                                                                                                                                                      |
SubOp:                                        --Messenger--> ReadHeader --DispatchQ--> OSD::_dispatch --OpWQ--> PG::do_request --journalq--> Journal --FileStore::OpWQ--> Apply Thread --Finisher--> sub_op_applied -
                                                                                                                                                                              |
                                                                                                                                                                              |-----------------------------> flushed ----------------> synced
                                                                                                                                                |------------------------------------------------------------------------------------------|
                                                                                                                                                                                        Journal (size)
                                                                                                                                                                              |---------------------------------|
                                                                                                                                                                                        WBThrottle
                                                                                                                         |-----------------------------------------------------|
                                                                                                                            FileStore op_queue throttle (number and size)
```
