# 79: Cache Tiering

**クリップソース:** [79: Cache Tiering — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/operations/cache-tiering/)

# Cache Tiering[¶](https://docs.ceph.com/en/pacific/rados/operations/cache-tiering/ "Permalink to this headline")

キャッシュ層は、バッキングストレージ層に格納されたデータのサブセットに対して、CephクライアントのI/Oパフォーマンスを向上させます。キャッシュ階層化では、キャッシュ階層として機能するように構成された比較的高速/高価なストレージデバイス（ソリッドステートドライブなど）のプールと、経済的なストレージ階層として機能するように構成されたEC付きまたは比較的低速/安価なデバイスのバッキングプールを作成する必要があります。Cephのオブジェクタはオブジェクトを配置する場所を処理し、ティアリングエージェントはキャッシュからバッキングストレージ層にオブジェクトをフラッシュするタイミングを決定します。つまり、キャッシュ層とバッキングストレージ層は、Cephクライアントから完全に透過的です。

キャッシュ階層化エージェントは、キャッシュ階層とバッキングストレージ階層間のデータの移行を自動的に処理します。しかし、管理者は cache\-mode を設定することで、この移行がどのように行われるかを設定することができます。主なシナリオは2つあります。

* **writeback**
* **readproxy**

Other cache modes are:

* readonly は、読み取り操作のみオブジェクトをキャッシュにプロモートし、書き込み操作はベース層に転送されます。このモードは、ストレージシステムによる一貫性の維持を必要としない、読み取り専用のワークロードを対象としています。\(警告: オブジェクトがベース層で更新された場合、Cephはその更新をキャッシュ内の対応するオブジェクトに同期させようとはしません。このモードはExperimentalのため、有効にするには\-yes\-i\-really\-mean\-itオプションを渡す必要があります\)。
* none は、キャッシュを完全に無効にするために使用します。

## A word of caution[¶](https://docs.ceph.com/en/pacific/rados/operations/cache-tiering/ "Permalink to this headline")

キャッシュ階層化は、ほとんどのワークロードでパフォーマンスを低下させます。 この機能を使用する前に、ユーザーは十分な注意を払う必要があります。

* _Workload dependent_
* _Difficult to benchmark_
* _Usually slower_
* _librados object enumeration_
* _Complexity_

### Known Good Workloads[¶](https://docs.ceph.com/en/pacific/rados/operations/cache-tiering/ "Permalink to this headline")

* _RGW time\-skewed_

### Known Bad Workloads[¶](https://docs.ceph.com/en/pacific/rados/operations/cache-tiering/ "Permalink to this headline")

以下の構成は、キャッシュ階層化との相性が悪いことが知られています。

* _replicatedキャッシュとECベースを持つRBD。これはよくある要求ですが、通常はうまく実行されません。 適度に歪んだワークロードでさえ、コールドオブジェクトに小さな書き込みを送信し、小さな書き込みはまだECプールでサポートされていないため、小さな（しばしば4KB）書き込みを満たすために、オブジェクト全体（通常4MB）をキャッシュに移行する必要があります。 この構成の導入に成功したユーザーはほんの一握りで、その理由は、データが極めてコールドであり（バックアップ）、パフォーマンスに対して何ら敏感でない場合のみ有効です。_
* _replicatedキャッシュとreplicatedベースのRBD。replicatedベース層を持つRBDは、ECベースの場合よりもうまくいきますが、それでもワークロードのスキューの量に大きく依存し、検証は非常に困難です。 ユーザーはワークロードを十分に理解する必要があり、キャッシュ階層化パラメータを慎重に調整する必要があります。_

## Setting Up Pools[¶](https://docs.ceph.com/en/pacific/rados/operations/cache-tiering/ "Permalink to this headline")

キャッシュ階層化を設定するには、2つのプールを用意する必要があります。1つはバッキングストレージとして、もう1つはキャッシュとして機能します。

### Setting Up a Backing Storage Pool[¶](https://docs.ceph.com/en/pacific/rados/operations/cache-tiering/ "Permalink to this headline")

バッキング・ストレージ・プールの設定には、通常、2つのシナリオのいずれかが必要です。

* **Standard Storage**
* **Erasure Coding:**

標準的なストレージシナリオでは、CRUSHルールを設定して、障害ドメインを確立します（例：OSD、ホスト、シャーシ、ラック、列など）。ルール内のすべてのストレージドライブのサイズ、速度\(RPMとスループットの両方\)、タイプが同じである場合、Ceph OSD Daemonは最適なパフォーマンスを発揮します。ルールの作成の詳細については、「[CRUSH Maps](https://docs.ceph.com/en/pacific/rados/operations/cache-tiering/)」を参照してください。ルールを作成したら、バッキングストレージプールを作成します。 

ECのシナリオでは、プール作成の引数で適切なルールが自動的に生成されます。詳細は、「[Create a Pool](https://docs.ceph.com/en/pacific/rados/operations/cache-tiering/)」を参照してください。 

以降の例では、バッキングストレージプールをコールドストレージと呼びます。

### Setting Up a Cache Pool[¶](https://docs.ceph.com/en/pacific/rados/operations/cache-tiering/ "Permalink to this headline")

キャッシュプールの設定は、標準的なストレージシナリオと同じ手順に従いますが、次のような違いがあります：キャッシュ層用のドライブは、通常、独自のサーバーに存在する高性能ドライブで、独自のCRUSHルールを持っています。 このようなルールを設定する場合、高性能ドライブを持つホストを考慮し、そうでないホストは省かなければなりません。詳細については、「[CRUSH Device Class](https://docs.ceph.com/en/pacific/rados/operations/cache-tiering/)」を参照してください。

以降の例では、キャッシュプールをホットストレージ、バッキングプールをコールドストレージと呼びます。

キャッシュ層の構成とデフォルト値については、「[Pools \- Set Pool Values](https://docs.ceph.com/en/pacific/rados/operations/cache-tiering/)」を参照してください。

## Creating a Cache Tier[¶](https://docs.ceph.com/en/pacific/rados/operations/cache-tiering/ "Permalink to this headline")

キャッシュティアの設定には、バッキングストレージプールとキャッシュプールの関連付けが必要です。

```
ceph osd tier add {storagepool} {cachepool}
```

For example

```
ceph osd tier add cold-storage hot-storage
```

キャッシュモードを設定するには：

```
ceph osd tier cache-mode {cachepool} {cache-mode}
```

For example:

```
ceph osd tier cache-mode hot-storage writeback
```

キャッシュ層は、バッキングストレージ層をオーバーレイするので、1つの追加ステップが必要です：ストレージプールからのすべてのクライアントトラフィックをキャッシュプールに誘導する必要があります。クライアントトラフィックをキャッシュプールに直接誘導するには、以下を実行します。

```
ceph osd tier set-overlay {storagepool} {cachepool}
```

For example:

```
ceph osd tier set-overlay cold-storage hot-storage
```

## Configuring a Cache Tier[¶](https://docs.ceph.com/en/pacific/rados/operations/cache-tiering/ "Permalink to this headline")

キャッシュ層にはいくつかの設定オプションがあります。キャッシュ層の設定オプションは、以下の使い方で設定することができます。

```
ceph osd pool set {cachepool} {key} {value}
```

詳しくは、「[Pools \- Set Pool Values](https://docs.ceph.com/en/pacific/rados/operations/cache-tiering/)」を参照してください。

### Target Size and Type[¶](https://docs.ceph.com/en/pacific/rados/operations/cache-tiering/ "Permalink to this headline")

Cephのプロダクションキャッシュ層は、hit\_set\_typeに[Bloom Filter](https://docs.ceph.com/en/pacific/rados/operations/cache-tiering/)を使用しています。

```
ceph osd pool set {cachepool} hit_set_type bloom
```

For example:

```
ceph osd pool set hot-storage hit_set_type bloom
```

hit\_set\_countとhit\_set\_periodは、このようなHitSetをいくつ保存するか、また各HitSetがどのくらいの時間をカバーすべきかを定義するものです。

```
ceph osd pool set {cachepool} hit_set_count 12
ceph osd pool set {cachepool} hit_set_period 14400
ceph osd pool set {cachepool} target_max_bytes 1000000000000
```

注意：hit\_set\_countが大きいと、ceph\-osdプロセスで消費されるRAMが多くなります。

時間経過に伴うアクセスのbinningにより、Cephは、Cephクライアントが一定期間内にオブジェクトに少なくとも1回アクセスしたか、または2回以上アクセスしたか\(「age」vs「temperature」\)を判断することができます。

min\_read\_recency\_for\_promote は、読み込み操作を処理する際にオブジェクトの存在を確認する HitSet 数を定義します。チェック結果は、オブジェクトを非同期にプロモートするかどうかを決定するために使用されます。この値は0とhit\_set\_countの間でなければなりません。0に設定された場合、オブジェクトは常にプロモートされます。1に設定された場合、現在のHitSetがチェックされます。そして、もしこのオブジェクトが現在のHitSetにあれば、プロモートされます。そうでなければ、昇格しません。他の値の場合、アーカイブHitSetの正確な数がチェックされます。オブジェクトが直近の min\_read\_recency\_for\_promote HitSets のいずれかに存在する場合、そのオブジェクトは昇格されます。

書き込み操作に対しても同様のパラメータ（min\_write\_recency\_for\_promote）は設定可能です。

```
ceph osd pool set {cachepool} min_read_recency_for_promote 2
ceph osd pool set {cachepool} min_write_recency_for_promote 2
```

注：期間が長く、min\_read\_recency\_for\_promoteとmin\_write\_recency\_for\_promoteの値が高いほど、ceph\-osdデーモンのRAM消費量は多くなります。特に、エージェントがキャッシュオブジェクトのフラッシュや退避を行う際に、すべての hit\_set\_count HitSet が RAM にロードされます。

### Cache Sizing[¶](https://docs.ceph.com/en/pacific/rados/operations/cache-tiering/ "Permalink to this headline")

キャッシュ階層化エージェントは、主に2つの機能を実行します。

* **Flushing:**
* **Evicting:**

#### Absolute Sizing[¶](https://docs.ceph.com/en/pacific/rados/operations/cache-tiering/ "Permalink to this headline")

キャッシュ階層化エージェントは、総バイト数または総オブジェクト数に基づいて、オブジェクトをフラッシュまたは退避させることができます。最大バイト数を指定するには：

```
ceph osd pool set {cachepool} target_max_bytes {#bytes}
```

例えば、1TBでフラッシュまたは退避するには：

```
ceph osd pool set hot-storage target_max_bytes 1099511627776
```

オブジェクトの最大数を指定する場合は：

```
ceph osd pool set {cachepool} target_max_objects {#objects}
```

例えば、1M個のオブジェクトをフラッシュまたは退避させる場合は：

```
ceph osd pool set hot-storage target_max_objects 1000000
```

注：Cephはキャッシュプールのサイズを自動的に決定することができないため、ここでは絶対サイズに関する設定が必要であり、そうしないとフラッシュ/退避が機能しません。両方の制限を指定した場合、いずれかの閾値がトリガーされると、キャッシュ階層化エージェントはフラッシュまたは退避を開始します。

注意：target\_max\_bytes または target\_max\_object に達したときのみ、すべてのクライアントリクエストがブロックされます。

#### Relative Sizing[¶](https://docs.ceph.com/en/pacific/rados/operations/cache-tiering/ "Permalink to this headline")

キャッシュ階層化エージェントは、キャッシュプールのサイズ（Absolute sizing の target\_max\_bytes / target\_max\_objects で指定）に応じて、オブジェクトのフラッシュや退避を行うことができます。 キャッシュプールがある割合の修正された（あるいはダーティな）オブジェクトで構成されている場合、キャッシュ階層化エージェントはそれらをストレージプールにフラッシュします。cache\_target\_dirty\_ratio を設定するには：

```
ceph osd pool set {cachepool} cache_target_dirty_ratio {0.0..1.0}
```

例えば、この値を0.4に設定すると、変更された（ダーティな）オブジェクトがキャッシュプールの容量の40%に達すると、フラッシュを開始します。

```
ceph osd pool set hot-storage cache_target_dirty_ratio 0.4
```

ダーティオブジェクトの容量が一定の割合に達すると、より高速にダーティオブジェクトをフラッシュします。cache\_target\_dirty\_high\_ratio を設定するには：

```
ceph osd pool set {cachepool} cache_target_dirty_high_ratio {0.0..1.0}
```

例えば、この値を0.6に設定すると、ダーティなオブジェクトがキャッシュプールの容量の60%に達したときに積極的にフラッシュを開始します。明らかに、dirty\_ratio と full\_ratio の間の値を設定する方がよいでしょう。

```
ceph osd pool set hot-storage cache_target_dirty_high_ratio 0.6
```

キャッシュプールが一定の割合の容量に達すると、キャッシュ階層化エージェントは空き容量を維持するためにオブジェクトを退避させます。cache\_target\_full\_ratio を設定するには：

```
ceph osd pool set {cachepool} cache_target_full_ratio {0.0..1.0}
```

例えば、この値を0.8に設定すると、未修正の（クリーンな）オブジェクトがキャッシュプールの容量の80%に達すると、フラッシュを開始します。

```
ceph osd pool set hot-storage cache_target_full_ratio 0.8
```

### Cache Age[¶](https://docs.ceph.com/en/pacific/rados/operations/cache-tiering/ "Permalink to this headline")

キャッシュ階層化エージェントは、最近修正された（またはダーティな）オブジェクトをバッキングストレージプールにフラッシュするまでのオブジェクトの最小エイジを指定することができます。

```
ceph osd pool set {cachepool} cache_min_flush_age {#seconds}
```

例えば、変更された（あるいはダーティな）オブジェクトを10分後にフラッシュするには：

```
ceph osd pool set hot-storage cache_min_flush_age 600
```

オブジェクトがキャッシュ層から追い出されるまでの最小エイジを指定することができます。

```
ceph osd pool {cache-tier} cache_min_evict_age {#seconds}
```

例えば、30分後にオブジェクトを退避させるには：

```
ceph osd pool set hot-storage cache_min_evict_age 1800
```

## Removing a Cache Tier[¶](https://docs.ceph.com/en/pacific/rados/operations/cache-tiering/ "Permalink to this headline")

キャッシュ層の削除は、ライトバックキャッシュかリードオンリーキャッシュかによって異なります。

### Removing a Read\-Only Cache[¶](https://docs.ceph.com/en/pacific/rados/operations/cache-tiering/ "Permalink to this headline")

読み取り専用キャッシュは、変更されたデータを持たないため、キャッシュ内のオブジェクトに対する最近の変更を失うことなく、キャッシュを無効化および削除することができます。

cache\-modeをnoneに変更すると無効になります

```
ceph osd tier cache-mode {cachepool} none
```

For example:

```
ceph osd tier cache-mode hot-storage none
```

キャッシュプールをバッキングプールから削除します

```
ceph osd tier remove {storagepool} {cachepool}
```

For example:

```
ceph osd tier remove cold-storage hot-storage
```

### Removing a Writeback Cache[¶](https://docs.ceph.com/en/pacific/rados/operations/cache-tiering/ "Permalink to this headline")

ライトバックキャッシュは、変更されたデータを持つ可能性があるため、無効化して削除する前に、キャッシュ内のオブジェクトに対する最近の変更を失わないようにするための措置を講じる必要があります。

キャッシュモードをプロキシに変更し、新規および変更されたオブジェクトがバッキングストレージプールにフラッシュされるようにします。

```
ceph osd tier cache-mode {cachepool} proxy
```

For example:

```
ceph osd tier cache-mode hot-storage proxy
```

キャッシュプールがフラッシュされたことを確認します。これには数分かかる場合があります。

```
rados -p {cachepool} ls
```

キャッシュプールにまだオブジェクトがある場合は、手動でフラッシュすることができます。例えば

```
rados -p {cachepool} cache-flush-evict-all
```

クライアントがキャッシュにトラフィックを誘導しないように、オーバーレイを削除します。

```
ceph osd tier remove-overlay {storagetier}
```

For example:

```
ceph osd tier remove-overlay cold-storage
```

最後に、バッキングストレージプールからキャッシュティアプールを削除します。

```
ceph osd tier remove {storagepool} {cachepool}
```

For example:

```
ceph osd tier remove cold-storage hot-storage
```
