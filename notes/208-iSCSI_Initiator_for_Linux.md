# 208: iSCSI Initiator for Linux

**クリップソース:** [208: iSCSI Initiator for Linux — Ceph Documentation](https://docs.ceph.com/en/pacific/rbd/iscsi-initiator-linux/)

# iSCSI Initiator for Linux[¶](https://docs.ceph.com/en/pacific/rbd/iscsi-initiator-linux/ "Permalink to this headline")

**Prerequisite:**

* iscsi\-initiator\-utils パッケージ
* device\-mapper\-multipath パッケージ

**Installing:**

iSCSIイニシエーターとマルチパスツールをインストールします：

```
# yum install iscsi-initiator-utils
# yum install device-mapper-multipath
```

**Configuring:**

1. デフォルトの /etc/multipath.conf ファイルを作成し、multipathd サービスを有効にします：

```
# mpathconf --enable --with_multipathd y
```

1. /etc/multipath.conf ファイルに以下を追加します：

```
devices {
        device {
                vendor                 "LIO-ORG"
                hardware_handler       "1 alua"
                path_grouping_policy   "failover"
                path_selector          "queue-length 0"
                failback               60
                path_checker           tur
                prio                   alua
                prio_args              exclusive_pref_bit
                fast_io_fail_tmo       25
                no_path_retry          queue
        }
}
```

1. multipathdサービスを再起動します：

```
# systemctl reload multipathd
```

**iSCSI Discovery and Setup:**

1. iSCSIゲートウェイでCHAPが設定されている場合は、/etc/iscsi/iscsid.confファイルを適宜更新して、CHAPユーザー名とパスワードを提供します。
2. ターゲットポータルをディスカバーします：

```
# iscsiadm -m discovery -t st -p 192.168.56.101
192.168.56.101:3260,1 iqn.2003-01.org.linux-iscsi.rheln1
192.168.56.102:3260,2 iqn.2003-01.org.linux-iscsi.rheln1
```

1. ターゲットにログインします：

```
# iscsiadm -m node -T iqn.2003-01.org.linux-iscsi.rheln1 -l
```

**Multipath IO Setup:**

マルチパスデーモン（multipathd）は、multipath.confの設定に基づき、自動的にデバイスをセットアップします。multipath コマンドを実行すると、各経路に優先グループを設定したフェールオーバー構成でデバイスが表示されます。

```
# multipath -ll
mpathbt (360014059ca317516a69465c883a29603) dm-1 LIO-ORG ,IBLOCK
size=1.0G features='0' hwhandler='1 alua' wp=rw
|-+- policy='queue-length 0' prio=50 status=active
| `- 28:0:0:1 sde  8:64  active ready running
`-+- policy='queue-length 0' prio=10 status=enabled
  `- 29:0:0:1 sdc  8:32  active ready running
```

これで、通常のマルチパスiSCSIディスクと同じようにRBDイメージを使用することができるはずです。
