# 286: Troubleshooting

**クリップソース:** [286: Troubleshooting — Ceph Documentation](https://docs.ceph.com/en/pacific/radosgw/troubleshooting/)

# Troubleshooting[¶](https://docs.ceph.com/en/pacific/radosgw/troubleshooting/ "Permalink to this headline")

## The Gateway Won’t Start[¶](https://docs.ceph.com/en/pacific/radosgw/troubleshooting/ "Permalink to this headline")

ゲートウェイを起動できない場合（つまり、既存のpidがない場合）、他のユーザによる既存の.asokファイルがあるかどうかを確認します。他のユーザからの .asok ファイルが存在し、実行中の pid がない場合、その .asok ファイルを削除してから、再度プロセスを開始してください。これは、あなたが root ユーザとしてプロセスを開始し、スタートアップスクリプトが www\-data ユーザまたは apache ユーザとしてプロセスを開始しようとしていて、既存の .asok がスクリプトによるデーモンの開始を妨げている場合に発生する可能性があります。

radosgw initスクリプト（/etc/init.d/radosgw）にも、何が問題になる可能性があるかについての洞察を提供できるverbose引数があります。

```
/etc/init.d/radosgw start -v
```

or

```
/etc/init.d radosgw start --verbose
```

## HTTP Request Errors[¶](https://docs.ceph.com/en/pacific/radosgw/troubleshooting/ "Permalink to this headline")

Webサーバ自体のアクセスログとエラーログを調べることは、何が起こっているのかを特定するための最初のステップです。 500エラーがある場合は、通常、radosgwデーモンとの通信に問題があることを示しています。 デーモンが実行されていること、そのソケットパスが構成されていること、Webサーバが適切な場所でデーモンを探していることを確認してください。

## Crashed radosgw process[¶](https://docs.ceph.com/en/pacific/radosgw/troubleshooting/ "Permalink to this headline")

radosgwのプロセスが停止すると、通常、Webサーバ（apache、nginxなど）から500エラーが表示されます。 このような場合、radosgwを再起動するだけでサービスを回復することができます。

クラッシュの原因を診断するには、/var/log/cephのログとcoreファイル（生成されている場合）を確認します。

## Blocked radosgw Requests[¶](https://docs.ceph.com/en/pacific/radosgw/troubleshooting/ "Permalink to this headline")

radosgw のリクエストの一部 \(または全部\) がブロックされているように見える場合、管理ソケットを介して radosgw デーモンの内部状態をある程度知ることができます。 デフォルトでは、/var/run/ceph にソケットが設定されており、以下でデーモンに問い合わせることができます：

```
ceph daemon /var/run/ceph/client.rgw help

help                list available commands
objecter_requests   show in-progress osd requests
perfcounters_dump   dump perfcounters value
perfcounters_schema dump perfcounters schema
version             get protocol version
```

特に：

```
ceph daemon /var/run/ceph/client.rgw objecter_requests
...
```

上記はRADOSクラスターで現在進行中のリクエストに関する情報をダンプします。 これにより、応答しないOSDによってブロックされている要求があるかどうかを識別できます。 たとえば、次のように表示されます：

```
{ "ops": [
      { "tid": 1858,
        "pg": "2.d2041a48",
        "osd": 1,
        "last_sent": "2012-03-08 14:56:37.949872",
        "attempts": 1,
        "object_id": "fatty_25647_object1857",
        "object_locator": "@2",
        "snapid": "head",
        "snap_context": "0=[]",
        "mtime": "2012-03-08 14:56:37.949813",
        "osd_ops": [
              "write 0~4096"]},
      { "tid": 1873,
        "pg": "2.695e9f8e",
        "osd": 1,
        "last_sent": "2012-03-08 14:56:37.970615",
        "attempts": 1,
        "object_id": "fatty_25647_object1872",
        "object_locator": "@2",
        "snapid": "head",
        "snap_context": "0=[]",
        "mtime": "2012-03-08 14:56:37.970555",
        "osd_ops": [
              "write 0~4096"]}],
"linger_ops": [],
"pool_ops": [],
"pool_stat_ops": [],
"statfs_ops": []}
```

このダンプでは、2つの要求が進行中です。 last\_sentフィールドは、RADOSリクエストが送信された時間です。 これがしばらく前のものであれば、OSDが応答していないことを示唆しています。 たとえば、リクエスト1858の場合、次のようにOSDの状態を確認することができます：

```
ceph pg map 2.d2041a48

osdmap e9 pg 2.d2041a48 (2.0) -> up [1,0] acting [1,0]
```

これは、このPGのプライマリーコピーであるosd.1を見るように指示しています：

```
ceph daemon osd.1 ops
{ "num_ops": 651,
 "ops": [
       { "description": "osd_op(client.4124.0:1858 fatty_25647_object1857 [write 0~4096] 2.d2041a48)",
         "received_at": "1331247573.344650",
         "age": "25.606449",
         "flag_point": "waiting for sub ops",
         "client_info": { "client": "client.4124",
             "tid": 1858}},
...
```

flag\_pointフィールドは、OSDが現在レプリカの応答を待っていることを示し、この場合はosd.0です。

## Java S3 API Troubleshooting[¶](https://docs.ceph.com/en/pacific/radosgw/troubleshooting/ "Permalink to this headline")

### Peer Not Authenticated[¶](https://docs.ceph.com/en/pacific/radosgw/troubleshooting/ "Permalink to this headline")

以下のようなエラーが表示されることがあります：

```
[java] INFO: Unable to execute HTTP request: peer not authenticated
```

Java SDK for S3 は、デフォルトで HTTPS を使用するため、公認認証局からの有効な証明書が必要です。Ceph Object Storageサービスをテストしているだけの場合は、いくつかの方法でこの問題を解決できます：

1. IPアドレスまたはホスト名の前に http:// を付けます。 たとえば、次のように変更します：

```
conn.setEndpoint("myserver");
```

To:

```
conn.setEndpoint("http://myserver")
```

1. 認証情報を設定した後、クライアント設定を追加し、プロトコルをProtocol.HTTPに設定します。

```
AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);

ClientConfiguration clientConfig = new ClientConfiguration();
clientConfig.setProtocol(Protocol.HTTP);

AmazonS3 conn = new AmazonS3Client(credentials, clientConfig);
```

### 405 MethodNotAllowed[¶](https://docs.ceph.com/en/pacific/radosgw/troubleshooting/ "Permalink to this headline")

405エラーが発生した場合、S3のサブドメインが正しく設定されているか確認してください。サブドメイン機能を正しく動作させるためには、DNSレコードにワイルドカードの設定が必要です。

また、デフォルトのサイトが無効になっていることを確認します。

```
[java] Exception in thread "main" Status Code: 405, AWS Service: Amazon S3, AWS Request ID: null, AWS Error Code: MethodNotAllowed, AWS Error Message: null, S3 Extended Request ID: null
```

## Numerous objects in default.rgw.meta pool[¶](https://docs.ceph.com/en/pacific/radosgw/troubleshooting/ "Permalink to this headline")

Jewel以前に作成されたクラスタでは、default.rgw.metaプールを使用したメタデータのアーカイブ機能がデフォルトで有効になっています。このアーカイブは、ユーザとバケットのメタデータのすべての古いバージョンを保持し、結果としてdefault.rgw.metaプールに大量のオブジェクトが存在することになります。

### Disabling the Metadata Heap[¶](https://docs.ceph.com/en/pacific/radosgw/troubleshooting/ "Permalink to this headline")

今後、この機能を無効にしたいユーザは、metadata\_heapフィールドに空文字列「""」を設定してください。

```
$ radosgw-admin zone get --rgw-zone=default > zone.json
[edit zone.json, setting "metadata_heap": ""]
$ radosgw-admin zone set --rgw-zone=default --infile=zone.json
$ radosgw-admin period update --commit
```

これは、default.rgw.metaプールへの新しいメタデータの書き込みを停止しますが、既存のオブジェクトやプールを削除することはありません。

### Cleaning the Metadata Heap Pool[¶](https://docs.ceph.com/en/pacific/radosgw/troubleshooting/ "Permalink to this headline")

Jewelより前に作成されたクラスタは、通常、メタデータアーカイブ機能にのみdefault.rgw.metaを使用します。

ただし、Luminous以降、radosgwはdefault.rgw.meta内のプール名前空間をまったく異なる目的で使用します。つまり、user\_keysやその他の重要なメタデータを格納します。

ユーザは、クリーンアップ手順を実行する前に、ゾーン構成を確認する必要があります。

```
$ radosgw-admin zone get --rgw-zone=default | grep default.rgw.meta
[should not match any strings]
```

プールがいかなる目的にも使用されていないことを確認した後、ユーザは安全にdefault.rgw.metaプール内のすべてのオブジェクトを削除するか、オプションとしてプール自体を削除することができます。
