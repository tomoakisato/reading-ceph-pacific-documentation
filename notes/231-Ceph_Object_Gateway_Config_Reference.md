# 231: Ceph Object Gateway Config Reference

**クリップソース:** [231: Ceph Object Gateway Config Reference — Ceph Documentation](https://docs.ceph.com/en/pacific/radosgw/config-ref/)

# Ceph Object Gateway Config Reference[¶](https://docs.ceph.com/en/pacific/radosgw/config-ref/ "Permalink to this headline")

Ceph設定ファイル（通常はceph.conf）の\[client.radosgw.{instance\-name}\]セクションに、以下の設定を追加することができます。設定にはデフォルト値が含まれる場合があります。Ceph設定ファイルで各設定を指定しない場合、デフォルト値が自動的に設定されます。

client.radosgw.{instance\-name}\]セクションで設定された設定変数は、コマンドでインスタンス名を指定しないとrgwコマンドやradosgw\-adminコマンドに適用されません。したがって、すべての RGW インスタンスやすべての radosgw\-admin オプションに適用するための変数は、インスタンス名を指定しないように \[global\] または \[client\] セクションに置くことができます。

**rgw\_frontends**

Description

HTTPフロントエンドを設定する。複数のフロントエンドを設定するには、カンマ区切りのリストで指定する。各フロントエンドの設定には、空白で区切られたオプションのリストを含めることができる。 各オプションは "key=value" あるいは "key" の形式となる。サポートするオプションの詳細については [HTTP フロントエンド](https://docs.ceph.com/en/pacific/radosgw/frontends) を参照

Type

String

Default

beastport=7480

**rgw\_data**

Description

Ceph RADOS Gatewayのデータファイルの場所

Type

String

Default

/var/lib/ceph/radosgw/$cluster\-$id

**rgw\_enable\_apis**

Description

指定されたAPIを有効にする

注：s3 API を有効にすることは、[マルチサイト](https://docs.ceph.com/en/pacific/radosgw/multisite)構成に参加することを意図した radosgw インスタンスの要件

Type

String

Default

s3,s3website,swift,swift\_auth,admin,sts,iam,notifications All APIs.

**rgw\_cache\_enabled**

Description

Ceph Object Gatewayのキャッシュが有効かどうか

Type

Boolean

Default

true

**rgw\_cache\_lru\_size**

Description

Ceph Object Gatewayのキャッシュ内のエントリ数

Type

Integer

Default

10000

**rgw\_socket\_path**

Description

ドメインソケットのソケットpath。FastCgiExternalServer はこのソケットを使用する。ソケットpathを指定しない場合、Ceph Object Gatewayは外部サーバとして実行されない。ここで指定するpathは、rgw.confファイルで指定されたpathと同じである必要がある

Type

String

Default

N/A

**rgw\_fcgi\_socket\_backlog**

Description

fcgiのソケットバックログ

Type

Integer

Default

1024

**rgw\_host**

Description

Ceph Object Gatewayインスタンスのホスト。IPアドレスまたはホスト名を指定できる

Type

String

Default

[0.0.0.0](http://0.0.0.0)

**rgw\_port**

Description

インスタンスがリクエストをリッスンするポート。指定しない場合、Ceph Object Gatewayは外部のFastCGIを実行する

Type

String

Default

None

**rgw\_dns\_name**

Description

ドメインのDNS名。リージョン内のhostnames設定も参照

Type

String

Default

None

**rgw\_script\_uri**

Description

SCRIPT\_URI がリクエストに設定されていない場合の代替値

Type

String

Default

None

**rgw\_request\_uri**

Description

REQUEST\_URI がリクエストに設定されていない場合の代替値

Type

String

Default

None

**rgw\_print\_continue**

Description

動作している場合は、100\-continueを有効にする

Type

Boolean

Default

true

**rgw\_remote\_addr\_param**

Description

リモートアドレスのパラメータ。例えば、リモートアドレスを含むHTTPフィールドや、リバースプロキシが動作している場合はX\-Forwarded\-Forアドレスなど

Type

String

Default

REMOTE\_ADDR

**rgw\_op\_thread\_timeout**

Description

開いているスレッドのタイムアウト（秒）

Type

Integer

Default

600

**rgw\_op\_thread\_suicide\_timeout**

Description

Ceph Object Gatewayのプロセスが終了するまでのタイムアウト時間（秒）。0に設定すると無効

Type

Integer

Default

0

**rgw\_thread\_pool\_size**

Description

スレッドプールのサイズ

Type

Integer

Default

100 threads.

**rgw\_num\_control\_oids**

Description

rgwインスタンス間のキャッシュ同期に使用する通知オブジェクトの数

Type

Integer

Default

8

**rgw\_init\_timeout**

Description

Ceph Object Gatewayが初期化をあきらめるまでの秒数

Type

Integer

Default

30

**rgw\_mime\_types\_file**

Description

MIME\-typesファイルのpathと場所。オブジェクトタイプのSwift自動検出のために使用

Type

String

Default

/etc/mime.types

**rgw\_s3\_success\_create\_obj\_status**

Description

create\-obj の代替成功ステータス応答

Type

Integer

Default

0

**rgw\_resolve\_cname**

Description

rgwがリクエストホスト名フィールドのDNS CNAMEレコードを使用するかどうか （ホスト名がrgw dns nameと等しくない場合）

Type

Boolean

Default

false

**rgw\_obj\_stripe\_size**

Description

Ceph Object Gatewayオブジェクトのオブジェクトストライプのサイズ。ストライピングの詳細については、「[アーキテクチャ](https://docs.ceph.com/en/pacific/architecture#data-striping)」を参照

Type

Integer

Default

4\<\<20

**rgw\_extended\_http\_attrs**

Description

エンティティ（ユーザー、バケット、オブジェクト）に設定可能な属性セットを追加する。追加属性は、POSTメソッドでエンティティを配置したり変更したりする際に、HTTPヘッダーフィールドを通じて設定することができる。設定した場合、追加属性は、エンティティ上でGET/HEADを行う際にHTTPフィールドとして返される

Type

String

Default

None

Example

“content\_foo, content\_bar, x\-foo\-bar”

**rgw\_exit\_timeout\_secs**

Description

プロセスが無条件に終了するまでの待ち時間（秒）

Type

Integer

Default

120

**rgw\_get\_obj\_window\_size**

Description

1つのオブジェクト要求に対するウィンドウサイズ（バイト）

Type

Integer

Default

16\<\<20

**rgw\_get\_obj\_max\_req\_size**

Description

Ceph Storage Clusterに送信される1つのget操作の最大要求サイズ

Type

Integer

Default

4\<\<20

**rgw\_relaxed\_s3\_bucket\_names**

Description

USリージョンバケットのS3バケット名ルールの緩和を有効にする

Type

Boolean

Default

false

**rgw\_list\_buckets\_max\_chunk**

Description

ユーザーバケットのリスト時に、1回の操作で取得できるバケットの最大数

Type

Integer

Default

1000

**rgw\_override\_bucket\_index\_max\_shards**

Description

バケットインデックスオブジェクトのシャード数。0はシャードがないことを示す。あまり大きな値\(例えば1000\)を設定すると、バケットのリストアップにかかるコストが増加するため、推奨しない。この変数は、radosgw\-admin コマンドに自動的に適用されるように、clientセクションまたはglobalセクションで設定されるべき

Type

Integer

Default

0

**rgw\_curl\_wait\_timeout\_ms**

Description

特定のcurlコールのタイムアウト（ミリ秒）

Type

Integer

Default

1000

**rgw\_copy\_obj\_progress**

Description

ロングコピー操作時にオブジェクトの進捗状況を出力する

Type

Boolean

Default

true

**rgw\_copy\_obj\_progress\_every\_bytes**

Description

コピー進捗出力間の最小バイト数

Type

Integer

Default

1024\*1024

**rgw\_admin\_entry**

Description

管理者権限のリクエストURLのエントリポイント

Type

String

Default

admin

**rgw\_content\_length\_compat**

Description

CONTENT\_LENGTH と HTTP\_CONTENT\_LENGTH の両方が設定された FCGI リクエストの互換性処理を有効にする

Type

Boolean

Default

false

**rgw\_bucket\_quota\_ttl**

Description

キャッシュされたクォータ情報が信頼される時間（秒）。 このタイムアウトの後、クォータ情報はクラスタから再フェッチされる

Type

Integer

Default

600

**rgw\_user\_quota\_bucket\_sync\_interval**

Description

クラスタに同期する前にバケットクォータ情報を蓄積する時間（秒）。 この間、他のRGWインスタンスには、このインスタンスでの操作によるバケットクォータの統計情報の変化が見えない

Type

Integer

Default

180

**rgw\_user\_quota\_sync\_interval**

Description

クラスタに同期する前にユーザークオータ情報を蓄積する時間（秒）。 この間、他のRGWインスタンスには、このインスタンスでの操作によるユーザークォータ統計の変化が見えない

Type

Integer

Default

180

**rgw\_bucket\_default\_quota\_max\_objects**

Description

バケットあたりの最大オブジェクト数のデフォルト。他のクォータが指定されていない場合、新規ユーザーに対して設定される。既存のユーザーには影響しない。この変数は、radosgw\-admin コマンドに自動的に適用されるように、clientセクションまたはglobalセクションで設定されるべき

Type

Integer

Default

\-1

**rgw\_bucket\_default\_quota\_max\_size**

Description

バケットあたりの最大容量のデフォルト（バイト）。他のクォータが指定されていない場合、新規ユーザーに対して設定される。既存のユーザーには影響しない

Type

Integer

Default

\-1

**rgw\_user\_default\_quota\_max\_objects**

Description

ユーザーの最大オブジェクト数のデフォルト。これは、そのユーザーが所有するすべてのバケット内のすべてのオブジェクトを含む。他のクォータが指定されていない場合、新規ユーザーに対して設定される。既存のユーザーには影響しない

Type

Integer

Default

\-1

**rgw\_user\_default\_quota\_max\_size**

Description

他のクォータが指定されていない場合に、新規ユーザーに設定される最大サイズクォータ値（バイト）。 既存のユーザーには影響しない

Type

Integer

Default

\-1

**rgw\_verify\_ssl**

Description

リクエストを行う際に、SSL証明書を確認する

Type

Boolean

Default

true

## Lifecycle Settings[¶](https://docs.ceph.com/en/pacific/radosgw/config-ref/ "Permalink to this headline")

バケットのライフサイクルを設定することで、オブジェクトをライフタイムを通じて効果的に保存するように管理できます。過去のリリースでは、シングルスレッド処理によってライフサイクル処理速度が制限されていました。Nautilusリリースでは、この問題が解決され、追加のCeph Object Gatewayインスタンス間でバケットライフサイクルの並列スレッド処理が可能になり、インデックスのシャード列挙が順不同に置き換えられました。

ライフサイクル処理の積極性を高めるための、2つのオプションがあります：

**rgw\_lc\_max\_worker**

Description

並行して実行されるライフサイクルワーカスレッドの数。それによってバケットとインデックスシャードを同時に処理する

Type

Integer

Default

3

**rgw\_lc\_max\_wp\_worker**

Description

各ライフサイクルワーカーのワークプールにあるスレッドの数。このオプションは、各バケットの処理を高速化するのに役立つ

特定のワークロードに基づいてこれらの値をチューニングすることで、ライフサイクル処理の積極性を高めることができます。バケット数が多い（数千）ワークロードでは、rgw\_lc\_max\_worker の値をデフォルトの 3 から増やすことを検討し、バケット数は少ないがバケットあたりのオブジェクト数が多い（数十万）ワークロードでは、rgw\_lc\_max\_wp\_worker をデフォルト値の 3 から減らすことを検討することになります。

NOTE

これらの値を調整する場合は、現在のクラスタパフォーマンスとCeph Object Gatewayの使用率を検証してから行ってください。

## Garbage Collection Settings[¶](https://docs.ceph.com/en/pacific/radosgw/config-ref/ "Permalink to this headline")

Ceph Object Gatewayは、新しいオブジェクトのためのストレージを直ちに割り当てます。

Ceph Object Gatewayは、ゲートウェイがバケットインデックスからオブジェクトを削除してからしばらくすると、Ceph Storageクラスタ内の削除または上書きされたオブジェクトが使用しているストレージ領域をパージします。Ceph Storageクラスタから削除されたオブジェクトデータをパージするプロセスは、ガーベッジコレクションまたはGCと呼ばれています。

ガベージコレクション待ちオブジェクトのキューを表示するには、次のように実行します：

```
$ radosgw-admin gc list

Note: specify ``--include-all`` to list all entries, including unexpired
```

ガベージコレクションはバックグラウンドアクティビティであり、管理者のCeph Object Gatewayの設定によって、継続的に実行することも、負荷が低いときに実行することもあります。デフォルトでは、Ceph Object GatewayはGCオペレーションを継続的に実行します。GCオペレーションは通常のCeph Object Gatewayオペレーションの一部であるため、オブジェクトの削除オペレーションでは、ガベージコレクションの対象となるオブジェクトがほとんどの時間存在します。

作業負荷によっては、ガベージコレクションの活動速度を上回ってしまうことがあります。これは特に、多くのオブジェクトが短期間保存され、その後削除されるような、削除量の多いワークロードに当てはまります。このような場合、管理者は、以下の設定パラメータを使用して、ガベージコレクション操作の優先順位を上げることができます。

**rgw\_gc\_max\_objs**

Description

1回のガベージコレクション処理サイクルで処理可能なオブジェクトの最大数。最初の導入後は、この値を変更しないこと

Type

Integer

Default

32

**rgw\_gc\_obj\_min\_wait**

Description

削除されたオブジェクトがガベージコレクション処理されるまでの最小待ち時間

Type

Integer

Default

2\*3600

**rgw\_gc\_processor\_max\_time**

Description

連続する2つのガベージコレクション処理サイクル間の最大時間

Type

Integer

Default

3600

**rgw\_gc\_processor\_period**

Description

ガベージコレクション処理のサイクルタイム

Type

Integer

Default

3600

**rgw\_gc\_max\_concurrent\_io**

Description

RGWガベージコレクションスレッドが古いデータをパージする際の同時I/O操作の最大数

Type

Integer

Default

10

Deleteの重いワークロードのためのガベージコレクションのチューニング

Ceph Garbage Collectionをより積極的にチューニングするための最初のステップとして、次のオプションをデフォルトの構成値から増やすことをお勧めします：

rgw\_gc\_max\_concurrent\_io=20

rgw\_gc\_max\_trim\_chunk=64

NOTE

これらの値を変更するには、RGWサービスの再起動が必要です。

これらの値をデフォルトから増やした後は、ガベージコレクション中のクラスタのパフォーマンスを監視して、値を増やしたことによるパフォーマンスの悪影響がないことを確認してください。

## Multisite Settings[¶](https://docs.ceph.com/en/pacific/radosgw/config-ref/ "Permalink to this headline")

バージョンJewelの新機能です。

各\[client.radosgw.{instance\-name}\]インスタンスの下のCeph設定ファイルに、以下の設定を含めることができます。

**rgw\_zone**

Description

ゲートウェイインスタンスのゾーン名。ゾーンが設定されていない場合、クラスタ全体のデフォルトはコマンド radosgw\-admin zone default で設定することができる

Type

String

Default

None

**rgw\_zonegroup**

Description

ゲートウェイインスタンスのゾーングループ名。zonegroup が設定されていない場合、クラスタ全体のデフォルトは、コマンド radosgw\-admin zone group default で設定することができる

Type

String

Default

None

**rgw\_realm**

Description

ゲートウェイインスタンスのレルム名。レルムが設定されていない場合、クラスタ全体のデフォルトを radosgw\-admin realm default コマンドで設定することができます。

Type

String

Default

None

**rgw\_run\_sync\_thread**

Description

レルム内に同期すべき他のゾーンがある場合、データとメタデータの同期を処理するスレッドを生成する

Type

Boolean

Default

true

**rgw\_data\_log\_window**

Description

データログエントリのウィンドウ（秒）

Type

Integer

Default

30

**rgw\_data\_log\_changes\_size**

Description

データ変更ログを保持するためのメモリ内エントリ数

Type

Integer

Default

1000

**rgw\_data\_log\_obj\_prefix**

Description

データログのオブジェクト名プレフィックス

Type

String

Default

data\_log

**rgw\_data\_log\_num\_shards**

Description

データ変更ログを保存するシャード\(オブジェクト\)数

Type

Integer

Default

128

**rgw\_md\_log\_max\_shards**

Description

メタデータログの最大シャード数

Type

Integer

Default

64

重要：rgw\_data\_log\_num\_shardsとrgw\_md\_log\_max\_shardsの値は、同期開始後に変更しないでください。

## 

## S3 Settings[¶](https://docs.ceph.com/en/pacific/radosgw/config-ref/ "Permalink to this headline")

**rgw\_s3\_auth\_use\_ldap**

Description

S3認証がLDAPを使うかどうか

Type

Boolean

Default

false

## Swift Settings[¶](https://docs.ceph.com/en/pacific/radosgw/config-ref/ "Permalink to this headline")

**rgw\_enforce\_swift\_acls**

Description

Swiftアクセスコントロールリスト（ACL）設定を強制する

Type

Boolean

Default

true

**rgw\_swift\_token\_expiration**

Description

Swiftトークンを失効させる時間（秒）

Type

Integer

Default

24\*3600

**rgw\_swift\_url**

Description

Ceph Object Gateway Swift APIのURL

Type

String

Default

None

**rgw\_swift\_url\_prefix**

Description

Swift APIのURLプレフィックス。S3 APIエンドポイントと区別するために使用する。デフォルトは swift で、Swift API を http://host:port/swift/v1 \(rgwswiftaccountinurl が有効な場合は http://host:port/swift/v1/AUTH\_%\(tenant\_id\)\) の URL で利用できるようにする

互換性のために、この設定変数に空の文字列を設定すると、デフォルトのswiftトが使用される

警告：このオプションを / に設定した場合、rgw enable apis を修正して s3 を除外することで、S3 API を無効にする必要があります。rgw swift url prefix=/でradosgwを運用し、S3 APIとSwift APIの両方を同時にサポートすることは不可能です。もし、プレフィックスなしで両方のAPIをサポートする必要がある場合は、代わりに複数のradosgwインスタンスを異なるホスト（またはポート）でリッスンするように配置し、S3用とSwift用に有効にします。

Default

swift

Example

“/swift\-testing”

**rgw\_swift\_auth\_url**

Description

v1 認証トークンを検証するためのデフォルトURL \(Swift の内部認証を使用していない場合\)

Type

String

Default

None

**rgw\_swift\_auth\_entry**

Description

Swift 認証 URL のエントリポイント

Type

String

Default

auth

**rgw\_swift\_account\_in\_url**

Description

Swift API の URL に Swift のアカウント名を含めるかどうか

false \(デフォルト\) に設定すると、Swift API は http://host:port/\<rgw\_swift\_url\_prefix\>/v1 のような URL で待ち受け、アカウント名 \(radosgw が [Keystone 統合](https://docs.ceph.com/en/pacific/radosgw/keystone)で設定されていれば一般的に Keystone プロジェクトの UUID\) はリクエストヘッダーから推定されるようになります。

true に設定すると、Swift API の URL は http://host:port/\<rgw\_swift\_url\_prefix\>/v1/AUTH\_\<account\_name\> \(または http://host:port/\<rgw\_swift\_url\_prefix\>/v1/AUTH\_\<keystone\_project\_id\>\) となり、それに応じて Keystone オブジェクトストアのエンドポイントに AUTH\_%\(tenant\_id\)s サフィックスが付くように設定しなければならなくなります。

radosgw でpublicly\-readableなコンテナとtemporary URL をサポートするには、このオプションを true に設定（し、 Keystone サービスカタログを更新）する必要がある。

Type

Boolean

Default

false

**rgw\_swift\_versioning\_enabled**

Description

OpenStack Object Storage APIのObject Versioningを有効にする。これにより、クライアントはバージョン管理されるべきコンテナに X\-Versions\-Location 属性を付けることができる。この属性は、アーカイブバージョンを保存しているコンテナの名前を指定する。アクセス制御の検証のため、バージョン管理するコンテナと同じユーザーが所有する必要がある（ACLは考慮されない）。これらのコンテナは、S3オブジェクトのバージョニングメカニズムでバージョニングすることはできない

OpenStack SwiftがDELETE操作を処理するために理解する、X\-History\-Location属性は、現在サポートされていない

Type

Boolean

Default

false

**rgw\_trust\_forwarded\_https**

Description

接続がセキュアかどうかを判断するときに、プロキシによって送信されるForwardedヘッダーとX\-Forwarded\-Protoヘッダーを信頼する。サーバー側の暗号化などの一部の機能に必要。

radosgwの前にあるプロキシがSSLターミネーションに使用されている場合、radosgwは着信http接続がセキュアかどうかを知らない。（radosgwの前に信頼できるプロキシがない場合は、この設定を有効にしないこと。有効にすると悪意のあるユーザーがこれらのヘッダーを任意の要求で設定できるようになる。）

Type

Boolean

Default

false

## Logging Settings[¶](https://docs.ceph.com/en/pacific/radosgw/config-ref/ "Permalink to this headline")

**rgw\_log\_nonexistent\_bucket**

Description

Ceph Object Gatewayが存在しないバケットへの要求をログに記録する

Type

Boolean

Default

false

**rgw\_log\_object\_name**

Description

オブジェクト名のロギングフォーマット。フォーマット指定子の詳細については、ページ日付を参照

Type

Date

Default

%Y\-%m\-%d\-%H\-%i\-%n

**rgw\_log\_object\_name\_utc**

Description

ログを記録するオブジェクトの名前にUTC時間を含むかどうか。falseの場合、ローカル時刻を使用する

Type

Boolean

Default

false

**rgw\_usage\_max\_shards**

Description

usageログの最大シャード数

Type

Integer

Default

32

**rgw\_usage\_max\_user\_shards**

Description

1人のユーザーのusageログに使用する最大シャード数

Type

Integer

Default

1

**rgw\_enable\_ops\_log**

Description

Ceph Object Gatewayの操作が成功するたびにログを残す

Type

Boolean

Default

false

**rgw\_enable\_usage\_log**

Description

usage ログを有効にする

Type

Boolean

Default

false

**rgw\_ops\_log\_rados**

Description

操作ログをCeph Storage Clusterバックエンドに書き込むかどうか

Type

Boolean

Default

true

**rgw\_ops\_log\_socket\_path**

Description

操作ログを書き込むためのUnixドメインソケット

Type

String

Default

None

**rgw\_ops\_log\_file\_path**

Description

操作ログを書き込むためのファイル

Type

String

Default

None

**rgw\_ops\_log\_data\_backlog**

Description

Unixドメインソケットに書き込まれるオペレーションログの最大データバックログサイズ

Type

Integer

Default

5\<\<20

**rgw\_usage\_log\_flush\_threshold**

Description

同期的にフラッシュする、usage ログ内のダーティなマージ済みエントリの数

Type

Integer

Default

1024

**rgw\_usage\_log\_tick\_interval**

Description

保留中のusage ログデータをフラッシュする間隔（秒）

Type

Integer

Default

30

**rgw\_log\_http\_headers**

Description

opsログエントリーに含めるHTTPヘッダのカンマ区切りリスト。 ヘッダ名は大文字と小文字を区別せず、アンダースコアで区切られた完全なヘッダ名を使用する

Type

String

Default

None

Example

“http\_x\_forwarded\_for, http\_x\_special\_k”

**rgw\_intent\_log\_object\_name**

Description

intentログオブジェクト名のログフォーマット。フォーマット指定子の詳細については、manpageを参照

Type

Date

Default

%Y\-%m\-%d\-%i\-%n

**rgw\_intent\_log\_object\_name\_utc**

Description

intentログオブジェクト名にUTC時間を使用するかどうか。falseの場合、ローカルタイムを使用する

Type

Boolean

Default

false

## Keystone Settings[¶](https://docs.ceph.com/en/pacific/radosgw/config-ref/ "Permalink to this headline")

**rgw\_keystone\_url**

Description

KeystoneサーバーのURL

Type

String

Default

None

**rgw\_keystone\_api\_version**

Description

Keystone サーバーとの通信に使用する、OpenStack Identity API のバージョン \(2 または 3\)

Type

Integer

Default

2

**rgw\_keystone\_admin\_domain**

Description

OpenStack Identity API v3 を使用する際の、管理者権限を持つ OpenStack ドメインの名前

Type

String

Default

None

**rgw\_keystone\_admin\_project**

Description

OpenStack Identity API v3 を使用する際の、管理者権限を持つ OpenStack プロジェクトの名前。 指定しない場合は、rgw keystone admin tenant の値が代わりに使用される

Type

String

Default

None

**rgw\_keystone\_admin\_token**

Description

Keystone の管理者トークン \(共有シークレット\)。Ceph RGWでは、管理者トークンによる認証は、管理者資格情報 \(rgw\_keystone\_admin\_user, rgw\_keystone\_admin\_password, rgw\_keystone\_admin\_tenant, rgw\_keystone\_admin\_project, rgw\_keystone\_admin\_domain\) による認証よりも優先される。Keystone の管理者トークンは廃止さたが、古い環境と統合するために使用することができる。 代わりに rgw\_keystone\_admin\_token\_path を設定して、トークンの公開を回避することが望ましい

Type

String

Default

None

**rgw\_keystone\_admin\_token\_path**

Description

Keystone の管理者トークン \(共有シークレット\) を含むファイルへのpath。 Ceph RadosGW では、管理者トークンによる認証は、管理者資格情報 \(rgw\_keystone\_admin\_user, rgw\_keystone\_admin\_password, rgw\_keystone\_admin\_tenant, rgw\_keystone\_admin\_project, rgw\_keystone\_admin\_domain\) による認証よりも優先される。Keystone の管理者トークンは廃止されたが、古い環境と統合するために使用することができる

Type

String

Default

None

**rgw\_keystone\_admin\_tenant**

Description

OpenStack Identity API v2使用時の、管理者権限を持つOpenStackテナント（サービステナント）の名前

Type

String

Default

None

**rgw\_keystone\_admin\_user**

Description

OpenStack Identity API v2 を使用した Keystone 認証において、管理者権限を持つ OpenStack ユーザー（サービスユーザー）の名前

Type

String

Default

None

**rgw\_keystone\_admin\_password**

Description

OpenStack Identity APIv2を使用する場合のOpenStack管理者ユーザーのパスワード。 トークンの公開を回避するために、代わりに rgw\_keystone\_admin\_password\_pathを設定することを推奨

Type

String

Default

None

**rgw\_keystone\_admin\_password\_path**

Description

OpenStack Identity API v2 を使用する際の OpenStack 管理者ユーザーのパスワードを含むファイルのpath

Type

String

Default

None

**rgw\_keystone\_accepted\_roles**

Description

リクエスト処理に必要なロール

Type

String

Default

Member,admin

**rgw\_keystone\_token\_cache\_size**

Description

各Keystoneトークンキャッシュの最大エントリ数

Type

Integer

Default

10000

**rgw\_keystone\_revocation\_interval**

Description

トークンの失効チェックの間隔（秒）

Type

Integer

Default

15\*60

**rgw\_keystone\_verify\_ssl**

Description

keystoneへのトークン要求時にSSL証明書を検証する

Type

Boolean

Default

true

## Server\-side encryption Settings[¶](https://docs.ceph.com/en/pacific/radosgw/config-ref/ "Permalink to this headline")

**rgw\_crypt\_s3\_kms\_backend**

Description

SSE\-KMS 暗号化キーが格納される場所。サポートされているKMSシステムは、OpenStack Barbican（barbican、デフォルト）およびHashiCorp Vault（vault）

Type

String

Default

None

## Barbican Settings[¶](https://docs.ceph.com/en/pacific/radosgw/config-ref/ "Permalink to this headline")

**rgw\_barbican\_url**

Description

BarbicanサーバーのURL

Type

String

Default

None

**rgw\_keystone\_barbican\_user**

Description

[Encryption](https://docs.ceph.com/en/pacific/radosgw/encryption) に使用する [Barbican](https://docs.ceph.com/en/pacific/radosgw/barbican) シークレットにアクセスできる OpenStack ユーザーの名前

Type

String

Default

None

**rgw\_keystone\_barbican\_password**

Description

[Barbican](https://docs.ceph.com/en/pacific/radosgw/barbican)ユーザーに関連付けられたパスワード

Type

String

Default

None

**rgw\_keystone\_barbican\_tenant**

Description

OpenStack Identity API v2 を使用する際に [Barbican](https://docs.ceph.com/en/pacific/radosgw/barbican) ユーザーに関連付けられた OpenStack テナントの名前

Type

String

Default

None

**rgw\_keystone\_barbican\_project**

Description

OpenStack Identity API v3 を使用する際に [Barbican](https://docs.ceph.com/en/pacific/radosgw/barbican) ユーザーに関連付けられた OpenStack プロジェクトの名前

Type

String

Default

None

**rgw\_keystone\_barbican\_domain**

Description

OpenStack Identity API v3 を使用する際に [Barbican](https://docs.ceph.com/en/pacific/radosgw/barbican) ユーザーに関連付けられた OpenStack ドメインの名前

Type

String

Default

None

## HashiCorp Vault Settings[¶](https://docs.ceph.com/en/pacific/radosgw/config-ref/ "Permalink to this headline")

**rgw\_crypt\_vault\_auth**

Description

使用する認証方法の種類。現在サポートされているのは、tokenのみType

String

Default

token

**rgw\_crypt\_vault\_token\_file**

Description

認証方式がトークンの場合、トークン・ファイルのpath。トークン・ファイルは Rados Gateway のみ読み取ることができるべき

Type

String

Default

None

**rgw\_crypt\_vault\_addr**

Description

Vaultサーバーのベースアドレス（例：http://vaultserver:8200）

Type

String

Default

None

**rgw\_crypt\_vault\_prefix**

Description

Vault秘密URLプレフィックス（例：/v1/secret/data）。秘密空間のサブセットへのアクセスを制限するために使用できる

Type

String

Default

None

**rgw\_crypt\_vault\_secret\_engine**

Description

暗号鍵の取得に使用するVault 秘密エンジン：kv\-v2、transitのいずれかを選択

Type

String

Default

None

**rgw\_crypt\_vault\_namespace**

Description

設定する場合、Vault Namespaceは、同じVault Enterpriseインスタンス上のチームと個人に対してテナント分離を提供する（例：acme/tenant1）

Type

String

Default

None

### QoS settings[¶](https://docs.ceph.com/en/pacific/radosgw/config-ref/ "Permalink to this headline")

バージョンNautilusの新機能です。

civetwebフロントエンドには、接続ごとにスレッドを使用するスレッドモデルがあり、接続の受け入れは、rgw\_thread\_pool\_sizeによって自動的にスロットルされます。 新しいbeastフロントエンドは、新たな接続の受け入れをスレッドプールのサイズで制限しないため、Nautilusリリースでスケジューラの抽象化を導入し、リクエストをスケジュールする将来のメソッドをサポートします。

現在、スケジューラーのデフォルトはthrottlerで、アクティブな接続を設定された制限値までスロットルします。mClockに基づくQoSは現在実験段階であり、本番環境にはまだ推奨されません。dmclock\_client op queueの現在の実装では、RGW Opsをadmin、auth \(swift auth, sts\) metadata & data requestsに分割しています。

**rgw\_max\_concurrent\_requests**

Description

Beastフロントエンドが処理するHTTPリクエストの最大同時実行数。これを調整することで、高負荷時のメモリ使用量を制限することができる

Type

Integer

Default

1024

**rgw\_scheduler\_type**

Description

使用するRGWスケジューラ。有効な値は throttler\` と \`\`dmclock' 。現在のところ、デフォルトは Beast フロントエンドリクエストをスロットルする throttler 。 dmclock\` は \*experimental\* で、\`dmclock\` を experimental\_feature\_enabled 設定オプションに含む必要がある

以下のオプションは、実験的なdmclockスケジューラを調整するものです。dmclock についての詳細は、[QoS Based on mClock](https://docs.ceph.com/en/pacific/rados/configuration/osd-config-ref/#dmclock-qos) を参照してください。以下のフラグの op\_class は、admin, auth, metadata, data のいずれかです。

**rgw\_dmclock\_\<op\_class\>\_res**

Description

op\_class リクエストの mclock 予約

Type

float

Default

100.0

**rgw\_dmclock\_\<op\_class\>\_wgt**

Description

op\_class要求に対するmclockの重み付け

Type

float

Default

1.0

**rgw\_dmclock\_\<op\_class\>\_lim**

Description

op\_class要求のmclock制限

Type

float

Default

0.0
