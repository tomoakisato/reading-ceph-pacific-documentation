# 157: mount.fuse.ceph – mount ceph-fuse from /etc/fstab. — Ceph Documentation

 # mount.fuse.ceph – mount ceph\-fuse from /etc/fstab.[¶](https://docs.ceph.com/en/pacific/man/8/mount.fuse.ceph/ "Permalink to this headline")

## Synopsis[¶](https://docs.ceph.com/en/pacific/man/8/mount.fuse.ceph/ "Permalink to this headline")

**mount.fuse.ceph** \[\-h\] \[\-o OPTIONS \[_OPTIONS_ …\]\] device \[_device_ …\] mountpoint \[_mountpoint_ …\]

## Description[¶](https://docs.ceph.com/en/pacific/man/8/mount.fuse.ceph/ "Permalink to this headline")

**mount.fuse.ceph** is a helper for mounting ceph\-fuse from`/etc/fstab`.

To use mount.fuse.ceph, add an entry in `/etc/fstab` like:

```
DEVICE    PATH        TYPE        OPTIONS
none      /mnt/ceph   fuse.ceph   ceph.id=admin,_netdev,defaults  0 0
none      /mnt/ceph   fuse.ceph   ceph.name=client.admin,_netdev,defaults  0 0
none      /mnt/ceph   fuse.ceph   ceph.id=myuser,ceph.conf=/etc/ceph/foo.conf,_netdev,defaults  0 0
```

ceph\-fuse options are specified in the `OPTIONS` column and must begin with ‘`ceph.`’ prefix. This way ceph related fs options will be passed to ceph\-fuse and others will be ignored by ceph\-fuse.

## Options[¶](https://docs.ceph.com/en/pacific/man/8/mount.fuse.ceph/ "Permalink to this headline")

`ceph.id``=<username>`[¶](https://docs.ceph.com/en/pacific/man/8/mount.fuse.ceph/ "Permalink to this definition")Specify that the ceph\-fuse will authenticate as the given user.

`ceph.name``=client.admin`[¶](https://docs.ceph.com/en/pacific/man/8/mount.fuse.ceph/ "Permalink to this definition")Specify that the ceph\-fuse will authenticate as client.admin

`ceph.conf``=/etc/ceph/foo.conf`[¶](https://docs.ceph.com/en/pacific/man/8/mount.fuse.ceph/ "Permalink to this definition")Sets ‘conf’ option to /etc/ceph/foo.conf via ceph\-fuse command line.

Any valid ceph\-fuse options can be passed this way.

## Additional Info[¶](https://docs.ceph.com/en/pacific/man/8/mount.fuse.ceph/ "Permalink to this headline")

The old format /etc/fstab entries are also supported:

```
DEVICE                              PATH        TYPE        OPTIONS
id=admin                            /mnt/ceph   fuse.ceph   defaults   0 0
id=myuser,conf=/etc/ceph/foo.conf   /mnt/ceph   fuse.ceph   defaults   0 0
```

## Availability[¶](https://docs.ceph.com/en/pacific/man/8/mount.fuse.ceph/ "Permalink to this headline")

**mount.fuse.ceph** is part of Ceph, a massively scalable, open\-source, distributed storage system. Please refer to the Ceph documentation at [http://ceph.com/docs](https://docs.ceph.com/en/pacific/man/8/mount.fuse.ceph/) for more information.

## See also[¶](https://docs.ceph.com/en/pacific/man/8/mount.fuse.ceph/ "Permalink to this headline")

[ceph\-fuse](https://docs.ceph.com/en/pacific/man/8/mount.fuse.ceph/)\(8\),[ceph](https://docs.ceph.com/en/pacific/man/8/mount.fuse.ceph/)\(8\)
