# 119: rados – rados object storage utility

 # rados – rados object storage utility[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this headline")

## Synopsis[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this headline")

**rados** \[ _options_ \] \[ _command_ \]

## Description[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this headline")

**rados** is a utility for interacting with a Ceph object storage cluster \(RADOS\), part of the Ceph distributed storage system.

## Global Options[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this headline")

`--object-locator`` object_locator`[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Set object\_locator for operation.

`-p`` pool``, ``--pool`` pool`[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Interact with the given pool. Required by most commands.

`--target-pool`` pool`[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Select target pool by name.

`--pgid```[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")As an alternative to `--pool`, `--pgid` also allow users to specify the PG id to which the command will be directed. With this option, certain commands like `ls` allow users to limit the scope of the command to the given PG.

`-N`` namespace``, ``--namespace`` namespace`[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Specify the rados namespace to use for the object.

`--all```[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Use with ls to list objects in all namespaces. Put in CEPH\_ARGS environment variable to make this the default.

`--default```[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Use with ls to list objects in default namespace. Takes precedence over –all in case –all is in environment.

`-s`` snap``, ``--snap`` snap`[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Read from the given pool snapshot. Valid for all pool\-specific read operations.

`--create```[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Create the pool or directory that was specified.

`-i`` infile`[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")will specify an input file to be passed along as a payload with the command to the monitor cluster. This is only used for specific monitor commands.

`-m`` monaddress[:port]`[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Connect to specified monitor \(instead of looking through ceph.conf\).

`-b`` block_size`[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Set the block size for put/get/append ops and for write benchmarking.

`--striper```[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Uses the striping API of rados rather than the default one. Available for stat, stat2, get, put, append, truncate, rm, ls and all xattr related operation.

`-O`` object_size``, ``--object-size`` object_size`[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Set the object size for put/get ops and for write benchmarking.

`--max-objects```[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Set the max number of objects for write benchmarking.

`--lock-cookie`` locker-cookie`[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Will set the lock cookie for acquiring advisory lock \(lock get command\). If the cookie is not empty, this option must be passed to lock break command to find the correct lock when releasing lock.

`--target-locator```[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Use with cp to specify the locator of the new object.

`--target-nspace```[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Use with cp to specify the namespace of the new object.

## Bench options[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this headline")

`-t`` N``, ``--concurrent-ios``=N`[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Set number of concurrent I/O operations.

`--show-time```[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Prefix output with date/time.

`--no-verify```[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Do not verify contents of read objects.

`--write-object```[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Write contents to the objects.

`--write-omap```[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Write contents to the omap.

`--write-xattr```[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Write contents to the extended attributes.

## Load gen options[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this headline")

`--num-objects```[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Total number of objects.

`--min-object-size```[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Min object size.

`--max-object-size```[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Max object size.

`--min-op-len```[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Min io size of operations.

`--max-op-len```[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Max io size of operations.

`--max-ops```[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Max number of operations.

`--max-backlog```[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Max backlog size.

`--read-percent```[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Percent of operations that are read.

`--target-throughput```[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Target throughput \(in bytes\).

`--run-length```[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Total time \(in seconds\).

`--offset-align```[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")At what boundary to align random op offsets.

## Cache pools options[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this headline")

`--with-clones```[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Include clones when doing flush or evict.

## OMAP options[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this headline")

`--omap-key-file`` file`[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Read the omap key from a file.

## Generic options[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this headline")

`-c`` FILE``, ``--conf`` FILE`[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Read configuration from the given configuration file.

`--id`` ID`[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Set ID portion of my name.

`-n`` TYPE.ID``, ``--name`` TYPE.ID`[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Set cephx user name.

`--cluster`` NAME`[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Set cluster name \(default: ceph\).

`--setuser`` USER`[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Set uid to user or uid \(and gid to user’s gid\).

`--setgroup`` GROUP`[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Set gid to group or gid.

`--version```[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this definition")Show version and quit.

## Global commands[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this headline")

**lspools**List object pools

**df**Show utilization statistics, including disk usage \(bytes\) and object counts, over the entire system and broken down by pool.

**list\-inconsistent\-pg** _pool_List inconsistent PGs in given pool.

**list\-inconsistent\-obj** _pgid_List inconsistent objects in given PG.

**list\-inconsistent\-snapset** _pgid_List inconsistent snapsets in given PG.

## Pool specific commands[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this headline")

**get** _name_ _outfile_Read object name from the cluster and write it to outfile.

**put** _name_ _infile_ \[–offset offset\]Write object name with start offset \(default:0\) to the cluster with contents from infile.**Warning:** The put command creates a single RADOS object, sized just as large as your input file. Unless your objects are of reasonable and consistent sizes, that is probably not what you want – consider using RGW/S3, CephFS, or RBD instead.

**append** _name_ _infile_Append object name to the cluster with contents from infile.

**rm** _name_Remove object name.

**listwatchers** _name_List the watchers of object name.

**ls** _outfile_List objects in the given pool and write to outfile. Instead of `--pool` if `--pgid` will be specified, `ls` will only list the objects in the given PG.

**lssnap**List snapshots for given pool.

**clonedata** _srcname_ _dstname_ –object\-locator _key_Clone object byte data from _srcname_ to _dstname_. Both objects must be stored with the locator key _key_ \(usually either _srcname_ or _dstname_\). Object attributes and omap keys are not copied or cloned.

**mksnap** _foo_Create pool snapshot named _foo_.

**rmsnap** _foo_Remove pool snapshot named _foo_.

**bench** _seconds_ _mode_ \[ \-b _objsize_ \] \[ \-t _threads_ \]Benchmark for _seconds_. The mode can be _write_, _seq_, or_rand_. _seq_ and _rand_ are read benchmarks, either sequential or random. Before running one of the reading benchmarks, run a write benchmark with the _–no\-cleanup_ option. The default object size is 4 MB, and the default number of simulated threads \(parallel writes\) is 16. The _–run\-name \<label\>_ option is useful for benchmarking a workload test from multiple clients. The _\<label\>_is an arbitrary object name. It is “benchmark\_last\_metadata” by default, and is used as the underlying object name for “read” and “write” ops. Note: \-b _objsize_ option is valid only in _write_ mode. Note: _write_ and _seq_ must be run on the same host otherwise the objects created by _write_ will have names that will fail _seq_.

**cleanup** \[ –run\-name _run\_name_ \] \[ –prefix _prefix_ \]Clean up a previous benchmark operation. Note: the default run\-name is “benchmark\_last\_metadata”

**listxattr** _name_List all extended attributes of an object.

**getxattr** _name_ _attr_Dump the extended attribute value of _attr_ of an object.

**setxattr** _name_ _attr_ _value_Set the value of _attr_ in the extended attributes of an object.

**rmxattr** _name_ _attr_Remove _attr_ from the extended attributes of an object.

**stat** _name_Get stat \(ie. mtime, size\) of given object

**stat2** _name_Get stat \(similar to stat, but with high precision time\) of given object

**listomapkeys** _name_List all the keys stored in the object map of object name.

**listomapvals** _name_List all key/value pairs stored in the object map of object name. The values are dumped in hexadecimal.

**getomapval** \[ –omap\-key\-file _file_ \] _name_ _key_ \[ _out\-file_ \]Dump the hexadecimal value of key in the object map of object name. If the optional _out\-file_ argument is not provided, the value will be written to standard output.

**setomapval** \[ –omap\-key\-file _file_ \] _name_ _key_ \[ _value_ \]Set the value of key in the object map of object name. If the optional_value_ argument is not provided, the value will be read from standard input.

**rmomapkey** \[ –omap\-key\-file _file_ \] _name_ _key_Remove key from the object map of object name.

**getomapheader** _name_Dump the hexadecimal value of the object map header of object name.

**setomapheader** _name_ _value_Set the value of the object map header of object name.

**export** _filename_Serialize pool contents to a file or standard output.n”

**import** \[–dry\-run\] \[–no\-overwrite\] \< filename | \- \>Load pool contents from a file or standard input

## Examples[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this headline")

To view cluster utilization:

```
rados df
```

To get a list object in pool foo sent to stdout:

```
rados -p foo ls -
```

To get a list of objects in PG 0.6:

```
rados --pgid 0.6 ls
```

To write an object:

```
rados -p foo put myobject blah.txt
```

To create a snapshot:

```
rados -p foo mksnap mysnap
```

To delete the object:

```
rados -p foo rm myobject
```

To read a previously snapshotted version of an object:

```
rados -p foo -s mysnap get myobject blah.txt.old
```

To list inconsistent objects in PG 0.6:

```
rados list-inconsistent-obj 0.6 --format=json-pretty
```

## Availability[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this headline")

**rados** is part of Ceph, a massively scalable, open\-source, distributed storage system. Please refer to the Ceph documentation at [http://ceph.com/docs](https://docs.ceph.com/en/pacific/man/8/rados/) for more information.

## See also[¶](https://docs.ceph.com/en/pacific/man/8/rados/ "Permalink to this headline")

[ceph](https://docs.ceph.com/en/pacific/man/8/rados/)\(8\)
