# 408: PGPool — Ceph Documentation

 # PGPool[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/pgpool/ "Permalink to this headline")

PGPool is a structure used to manage and update the status of removed snapshots. It does this by maintaining two fields, cached\_removed\_snaps \- the current removed snap set and newly\_removed\_snaps \- newly removed snaps in the last epoch. In OSD::load\_pgs the osd map is recovered from the pg’s file store and passed down to OSD::\_get\_pool where a PGPool object is initialised with the map.

With each new map we receive we call PGPool::update with the new map. In that function we build a list of newly removed snaps \(pg\_pool\_t::build\_removed\_snaps\) and merge that with our cached\_removed\_snaps. This function included checks to make sure we only do this update when things have changed or there has been a map gap.

When we activate the pg we initialise the snap trim queue from cached\_removed\_snaps and subtract the purged\_snaps we have already purged leaving us with the list of snaps that need to be trimmed. Trimming is later performed asynchronously by the snap\_trim\_wq.
