# 69: Repairing PG inconsistencies

**クリップソース:** [69: Repairing PG inconsistencies — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/operations/pg-repair/)

# Repairing PG inconsistencies[¶](https://docs.ceph.com/en/pacific/rados/operations/pg-repair/ "Permalink to this headline")

時々、PGが「不整合」になることがあります。

PGをactive\+cleanの状態に戻すには、まずどのPGが不整合になったかを判断し、それに対して「pg repair」コマンドを実行する必要があります。

このページでは、PGを診断するためのコマンドと、不整合になったPGを修復するためのコマンドを紹介します。

## Commands for Diagnosing Placement\-group Problems[¶](https://docs.ceph.com/en/pacific/rados/operations/pg-repair/ "Permalink to this headline")

このセクションのコマンドは、壊れたPGを診断するさまざまな方法を提供します。

次のコマンドは、cephクラスタの健全性の高レベル\(詳細度は低い\)概要を提供します。

```
# ceph health detail
```

次のコマンドは、PGの状態に関するより詳細な情報を提供します。

```
# ceph pg dump --format=json-pretty
```

次のコマンドは、不整合なPGを一覧表示します。

```
# rados list-inconsistent-pg {pool}
```

次のコマンドは、不整合なradosオブジェクトを一覧表示します。

```
# rados list-inconsistent-obj {pgid}
```

次のコマンドは、指定されたPG内の不整合なスナップセットを一覧表示します。

```
# rados list-inconsistent-snapset {pgid}
```

## Commands for Repairing Placement Groups[¶](https://docs.ceph.com/en/pacific/rados/operations/pg-repair/ "Permalink to this headline")

壊れたPGを修復するコマンドの形式は次のとおりです。

```
# ceph pg repair {pgid}
```

ここで、{pgid}は影響を受けるPGのidです。

For example:

```
# ceph pg repair 1.4
```

## More Information on Placement Group Repair[¶](https://docs.ceph.com/en/pacific/rados/operations/pg-repair/ "Permalink to this headline")

Cephは、クラスタに格納されているオブジェクトのチェックサムを保存および更新します。PGに対してスクラブを実行すると、OSDはそのレプリカの中から信頼できるコピーを選択しようとします。

考えられるすべてのケースのうち、一貫しているのは1つのケースのみです。

ディープスクラブの後、Cephはディスクから読み取られたオブジェクトのチェックサムを計算し、以前に記録されたチェックサムと比較します。

現在のチェックサムと以前に記録されたチェックサムが一致しない場合、それは不整合となります。

複製プールの場合、オブジェクトの任意のレプリカのチェックサムと権威あるコピーのチェックサムとの間に不一致があれば、それは不整合であることを意味します。

"pg repair" コマンドは、さまざまな種類の不整合を修正しようとします。

"pg repair" が不整合なPGを見つけた場合、不整合なコピーのダイジェストを権威あるコピーのダイジェストで上書きしようと試みます。

"pg repair" が不整合な複製プールを発見した場合、不整合なコピーを紛失としてマークします。

複製プールの場合のリカバリは、"pg repair" の範囲外です。

ECプールおよびBluestoreプールでは、osd\_scrub\_auto\_repair \(設定デフォルト「false」\)がtrueに設定され、最大osd\_scrub\_auto\_repair\_num\_errors \(設定デフォルト 5\) 個エラーが検出されるとCephにより自動修復されます。

"pg repair" は全ての問題を解決するわけではありません。

Cephは、PGに不整合が見つかった場合、PGを自動的に修復することはありません。

オブジェクトやomapのチェックサムは、常に利用できるとは限りません。

チェックサムはインクリメンタルに計算されます。

複製されたオブジェクトが非連続的に更新される場合、更新に関わる書き込み操作によってオブジェクトが変更され、そのチェックサムが無効になります。

チェックサムを再計算する際に、オブジェクト全体は読み込まれません。

"ceph pg repair "は、filestoreの場合のようにチェックサムが利用できない場合でも、修復することが可能です。

複製されたfilestoreプールが問題になっている場合、ユーザは "ceph pg repair "よりも手動での修復を好むかもしれません。

この段落の資料はfilestoreに関連するもので、bluestoreは独自の内部チェックサムを持っています。

matched\-recordのチェックサムや計算されたチェックサムは、権威あるコピーが実際に権威あるものであることを証明することはできません。

利用可能なチェックサムがない場合、「pg repair」はプライマリ上のデータを優先します。

これは、破損していないレプリカであるかもしれませんし、そうでないかもしれません。

このため、不整合が発見された場合、人間の介入が必要になります。

人間の介入とは、時には "ceph\-objectstore\-tool "を使うことを意味します。

## External Links[¶](https://docs.ceph.com/en/pacific/rados/operations/pg-repair/ "Permalink to this headline")

https://ceph.io/geen\-categorie/ceph\-manually\-repair\-object/ \- このページには、PGの修復のウォークスルーがあります。PGの修復をしたいが、したことがない場合に読むことをお勧めします。
