# 296: Ceph Dashboard

**クリップソース:** [296: Ceph Dashboard — Ceph Documentation](https://docs.ceph.com/en/pacific/mgr/dashboard/)

# Ceph Dashboard[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

## Overview[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

Ceph Dashboardは、組み込みのWebベースのCeph管理および監視アプリケーションで、これを通じてクラスタ内のさまざまな側面とリソースを検査および管理できます。[Ceph Manager Daemon](https://docs.ceph.com/en/pacific/mgr/dashboard/)モジュールとして実装されています。

Ceph Luminousに同梱されていたオリジナルのCeph Dashboardは、Cephクラスタのランタイム情報とパフォーマンスデータを閲覧できるシンプルな読み取り専用ビューとしてスタートしました。当初の目的を達成するために、非常にシンプルなアーキテクチャを使用していました。しかし、CLIよりもWebUIを好むユーザがCephを簡単に管理できるように、よりリッチなWebベースの管理機能に対する要求が高まっていました。

新しいCeph Dashboardモジュールは、Ceph ManagerにWebベースの監視と管理を追加します。この新しいモジュールのアーキテクチャと機能は、openATTIC Ceph管理・監視ツールから派生し、それに触発されたものです。開発は、SUSEのopenATTICチームが積極的に推進し、Red Hatなどの企業やCephコミュニティのメンバーからもサポートを受けています。

ダッシュボードモジュールのバックエンドコードは、CherryPyフレームワークを使用し、カスタムREST APIを実装しています。WebUIの実装はAngular/TypeScriptをベースにしており、オリジナルのダッシュボードからの機能と、openATTICのスタンドアロン版用に独自に開発した新機能の両方を含んでいます。Ceph Dashboardモジュールは、ceph\-mgrがホストするWebサーバを介して、情報と統計情報をグラフィカルに表示するアプリケーションとして実装されています。

### Feature Overview[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

ダッシュボードには以下の機能があります：

* **Multi\-User and Role Management**
* **Single Sign\-On \(SSO\)**
* **SSL/TLS support**
* **Auditing**
* **Internationalization \(I18N\)**

Ceph Dashboardには以下の監視・管理機能があります：

* **Overall cluster health**
* **Embedded Grafana Dashboards**
* **Cluster logs**
* **Hosts**
* **Performance counters**
* **Monitors**
* **Monitoring**
* **Configuration Editor**
* **Pools**
* **OSDs**
* **Device management**
* **iSCSI**
* **RBD**
* **RBD mirroring**
* **CephFS**
* **Object Gateway**
* **NFS**
* **Ceph Manager Modules**

### Overview of the Dashboard Landing Page[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

クラスタ全体のステータス、パフォーマンス、キャパシティメトリクスを表示します。クラスタの変更に対する即時フィードバックを表示し、ダッシュボードのサブページに簡単にアクセスできます。

#### Status[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

* **Cluster Status**
* **Hosts**
* **Monitors**
* **OSDs**
* **Managers**
* **Object Gateway**
* **Metadata Servers**
* **iSCSI Gateways**

#### Capacity[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

* **Raw Capacity**
* **Objects**
* **PG Status**
* **Pools**
* **PGs per OSD**

#### Performance[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

* **Client READ/Write**
* **Client Throughput**
* **Recovery throughput**
* **Scrubbing**

### Supported Browsers[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

Ceph Dashboardは、主に以下のWebブラウザを使用してテストおよび開発されています。

|Browser                                                       |Versions               |
|--------------------------------------------------------------|-----------------------|
|[Chrome](https://docs.ceph.com/en/pacific/mgr/dashboard/)     |latest 2 major versions|
|[Firefox](https://docs.ceph.com/en/pacific/mgr/dashboard/)    |latest 2 major versions|
|[Firefox ESR](https://docs.ceph.com/en/pacific/mgr/dashboard/)|latest major version   |

Ceph Dashboardは古いブラウザでも動作する可能性がありますが、互換性は保証できませんので、ブラウザを最新の状態に保つことをお勧めします。

## Enabling[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

ceph\-mgr\-dashboardを配布パッケージからインストールした場合、パッケージ管理システムが必要なすべての依存関係をインストールするようにします。

Cephをソースから構築し、開発環境からダッシュボードを起動する場合は、ソースディレクトリsrc/pybind/mgr/dashboardのREADME.rstおよびHACKING.rstファイルをご覧ください。

実行中のCephクラスタ内でCeph Dashboardを有効にするには：

```
$ ceph mgr module enable dashboard
```

## Configuration[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

### SSL/TLS Support[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

ダッシュボードへのすべてのHTTP接続は、デフォルトでSSL/TLSで保護されています。

ダッシュボードを起動させるために、自己署名証明書を生成してインストールすることができます：

```
$ ceph dashboard create-self-signed-cert
```

ほとんどのウェブブラウザは、自己署名証明書について文句を言い、ダッシュボードへの安全な接続を確立する前に明示的な確認を要求することに留意してください。

デプロイメントを適切に保護し、警告を取り除くには、認証局（CA）から発行された証明書を使用する必要があります。

例えば、次のようなコマンドでキーペアを生成することができます：

```
$ openssl req -new -nodes -x509 
  -subj "/O=IT/CN=ceph-mgr-dashboard" -days 3650 
  -keyout dashboard.key -out dashboard.crt -extensions v3_ca
```

そして、dashboard.crt ファイルは CA によって署名されている必要があります。これが完了したら、以下のコマンドを実行して、Cephマネージャインスタンスに対して有効にします：

```
$ ceph dashboard set-ssl-certificate -i dashboard.crt
$ ceph dashboard set-ssl-certificate-key -i dashboard.key
```

各マネージャインスタンスに一意の証明書が必要な場合は、次のようにインスタンスの名前を含めることができます \($name は ceph\-mgr インスタンスの名前、通常はホスト名です\)：

```
$ ceph dashboard set-ssl-certificate $name -i dashboard.crt
$ ceph dashboard set-ssl-certificate-key $name -i dashboard.key
```

また、この設定値により、SSLを無効にすることができます：

```
$ ceph config set mgr mgr/dashboard/ssl false
```

これは、ダッシュボードが上流サーバのSSLをサポートしないプロキシの背後で実行される場合、あるいは必要としないその他の状況において有用です。詳細については、「[Proxy Configuration](https://docs.ceph.com/en/pacific/mgr/dashboard/)」を参照してください。

警告：SSLを無効にすると、ユーザー名とパスワードが暗号化されずにダッシュボードに送信されるため、注意が必要です。

注：SSL証明書および鍵を変更した後は、Cephマネージャのプロセスを再起動する必要があります。これは、ceph mgr fail mgrを実行するか、ダッシュボードモジュールを無効にして再度有効にすることで実現できます\(これはマネージャ自体を再起動するトリガーにもなります\)。

```
$ ceph mgr module disable dashboard
$ ceph mgr module enable dashboard
```

### Host Name and Port[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

ほとんどのWebアプリケーションと同様に、ダッシュボードはTCP/IPアドレスとTCPポートにバインドされます。

デフォルトでは、ダッシュボードをホストするceph\-mgrデーモン\(つまり、現在アクティブなマネージャ\)は、SSLが無効の場合、TCPポート8443または8080にバインドします。

特定のアドレスが設定されていない場合、Webアプリは利用可能なすべてのIPv4およびIPv6アドレスに対応する「:」にバインドされます。

これらのデフォルトは、コンフィギュレーションキー機能を使って、以下のようにクラスタ全体のレベルで変更することができます（したがって、すべてのマネージャインスタンスに適用されます）。

```
$ ceph config set mgr mgr/dashboard/server_addr $IP
$ ceph config set mgr mgr/dashboard/server_port $PORT
$ ceph config set mgr mgr/dashboard/ssl_server_port $PORT
```

ceph\-mgrはそれぞれダッシュボードのインスタンスをホストしているため、別々に設定する必要がある場合があります。特定のマネージャインスタンスのIPアドレスとポートは、次のコマンドで変更できます：

```
$ ceph config set mgr mgr/dashboard/$name/server_addr $IP
$ ceph config set mgr mgr/dashboard/$name/server_port $PORT
$ ceph config set mgr mgr/dashboard/$name/ssl_server_port $PORT
```

$nameをダッシュボードをホストするceph\-mgrインスタンスのIDに置き換えます。

注意：ceph mgr servicesコマンドを実行すると、現在設定されているすべてのエンドポイントが表示されます。ダッシュボードにアクセスするためのURLを取得するためには、dashboardキーを探します。

### Username and Password[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

ログインできるようにするには、ユーザアカウントを作成し、少なくとも1つのロールと関連付ける必要があります。お客様が使用できる1セットの定義済みシステム・ロールを提供しています。詳しくは、「[User and Role Management](https://docs.ceph.com/en/pacific/mgr/dashboard/)」セクションを参照してください。

administrator ロールを持つユーザを作成するには、次のコマンドを使用します：

```
$ ceph dashboard ac-user-create <username> -i <file-containing-password> administrator
```

### Account Lock\-out[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

ユーザが間違った認証情報を何度も入力した場合、そのユーザアカウントを無効にします。ブルートフォース攻撃や辞書攻撃を防ぐために、デフォルトで有効になっています。ロックアウトの試行回数のデフォルト値は、それぞれ以下のコマンドで取得または設定できます：

```
$ ceph dashboard get-account-lockout-attempts
$ ceph dashboard set-account-lockout-attempts <value:int>
```

警告：この機能は、ロックアウトの試行回数をデフォルトで0回に設定することで無効にできますが、この機能を無効にすると、ブルートフォース攻撃や辞書ベースの攻撃に対してより脆弱なアカウントになります。この機能を無効にするには、次のようにします：

```
$ ceph dashboard set-account-lockout-attempts 0
```

### Enable a Locked User[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

無効なログインを何度も試みた結果、ユーザアカウントが無効になった場合、管理者が手動で有効にする必要があります。これは、次のコマンドで行うことができます：

```
$ ceph dashboard ac-user-enable <username>
```

### Accessing the Dashboard[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

これで、Web ブラウザ \(JavaScript 対応\) を使用して、マネージャーインスタンスが実行されているホスト名または IP アドレスと選択した TCP ポートのいずれかを指定して、ダッシュボードにアクセスできます：e.g., http\(s\)://\<$IP\>:\<$PORT\>/.

ダッシュボードページが表示され、事前定義されたユーザ名とパスワードが要求されます。

### Enabling the Object Gateway Management Frontend[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

cephadmでRGWをデプロイすると、ダッシュボードで使用されるRGWの認証情報が自動的に設定されます。また、手動で強制的にクレデンシャルを設定することも可能です：

```
$ ceph dashboard set-rgw-credentials
```

これにより、システム内の各レルムに対して、uidがdashboardのRGWユーザが作成されます。

RGW  admin API でカスタム 'admin' リソースを設定した場合は、ここにも設定します：

```
$ ceph dashboard set-rgw-api-admin-resource <admin_resource>
```

Object Gateway のセットアップで自己署名証明書を使用している場合は、ダッシュボードで証明書の検証を無効にして、不明な CA によって署名された証明書やホスト名と一致しない証明書などによる接続拒否を回避する必要があります：

```
$ ceph dashboard set-rgw-api-ssl-verify False
```

Object Gatewayのリクエスト処理に時間がかかりすぎて、ダッシュボードがタイムアウトになる場合は、タイムアウトの値を必要に応じて設定することができます：

```
$ ceph dashboard set-rest-requests-timeout <seconds>
```

初期値は45秒です。

### Enabling iSCSI Management[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

Ceph Dashboardは、Ceph iSCSI Gatewayのrbd\-target\-apiサービスが提供するREST APIを使用して、iSCSIターゲットを管理できます。iSCSIゲートウェイにインストールされ、有効になっていることを確認してください。

注意:Ceph DashboardのiSCSI管理機能は、ceph\-iscsiプロジェクトの最新バージョン3に依存しています。オペレーティングシステムが正しいバージョンを提供していることを確認してください。そうしないと、ダッシュボードで管理機能が有効になりません。

ceph\-iscsi REST APIがHTTPSモードで設定されており、自己署名証明書を使用している場合、ceph\-iscsi APIにアクセスする際にSSL証明書の検証を行わないようにダッシュボードを設定する必要があります。

API SSL検証を無効にするには、次のコマンドを実行します：

```
$ ceph dashboard set-iscsi-api-ssl-verification false
```

利用可能なiSCSIゲートウェイは、次のコマンドを使用して定義する必要があります：

```
$ ceph dashboard iscsi-gateway-list
$ # Gateway URL format for a new gateway: <scheme>://<username>:<password>@<host>[:port]
$ ceph dashboard iscsi-gateway-add -i <file-containing-gateway-url> [<gateway_name>]
$ ceph dashboard iscsi-gateway-rm <gateway_name>
```

### Enabling the Embedding of Grafana Dashboards[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

GrafanaはPrometheusからデータを取得しています。Grafanaは他のデータソースも使用できますが、私たちが提供するGrafanaダッシュボードにはPrometheusに特化したクエリが含まれています。そのため、弊社のGrafanaダッシュボードには、データソースとしてPrometheusが必要です。Ceph Prometheus Moduleモジュールは、そのデータをPrometheusのExpositionフォーマットでエクスポートします。これらのGrafanaダッシュボードは、PrometheusモジュールとNode exporterからのメトリック名に依存しています。Node exporterは、マシンメトリクスを提供する別のアプリケーションです。

注意：Prometheusのセキュリティモデルは、信頼されないユーザーがPrometheusのHTTPエンドポイントおよびログにアクセスできることを前提としています。信頼されないユーザーは、Prometheusが収集したデータベースに含まれるすべての（メタ）データ、さまざまな運用・デバッグ情報にアクセスすることができます。

しかし、PrometheusのHTTP APIは読み取り専用の操作に限定されています。APIを使用して設定を変更することはできませんし、シークレットが公開されることもありません。さらに、Prometheusにはサービス拒否攻撃の影響を緩和するための対策がいくつか組み込まれています。

詳細については、Prometheusのセキュリティモデル\<https://prometheus.io/docs/operating/security/\>をご覧ください。

#### Installation and Configuration using cephadm[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

GrafanaとPrometheusはCephadmを使用してインストールすることができます。これらは、cephadmによって自動的に設定されます。PrometheusとGrafanaのインストールと設定にcephadmを使用する方法の詳細については、[Monitoring Services](https://docs.ceph.com/en/pacific/mgr/dashboard/)のドキュメントを参照してください。

#### Manual Installation and Configuration[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

以下は、GrafanaとPrometheusを手動で設定する手順です。Prometheus、Grafana、Node exporterを適切なホストにインストールした後、以下の手順に進みます。

1. Ceph Managerモジュールとして提供されているCeph Exporterを実行し、有効化します：

```
$ ceph mgr module enable prometheus
```

詳細は、[Prometheus Module](https://docs.ceph.com/en/pacific/mgr/dashboard/)のドキュメントに記載されています。

1. Prometheusに対応するscrapeの設定を追加します。これは以下のようになります：

```
global:
  scrape_interval: 5s

scrape_configs:
  - job_name: 'prometheus'
    static_configs:
      - targets: ['localhost:9090']
  - job_name: 'ceph'
    static_configs:
      - targets: ['localhost:9283']
  - job_name: 'node-exporter'
    static_configs:
      - targets: ['localhost:9100']
```

注：上記の例では、Prometheusは自分自身（ポート9090）、Cephの内部データをエクスポートするCephマネージャモジュールprometheus（ポート9283）、各ホストのOSおよびハードウェアメトリクスを提供するNode Exporter（ポート9100）からデータをかき集めるよう設定されていることに注意してください。

設定によっては、Node Exporterのホスト名を変更したり、追加の設定エントリを追加する必要があります。デフォルトのTCPポートを変更する必要があることはほとんどありません。

さらに、prometheus mgrモジュールが提供するCeph固有のデータのターゲットを複数用意する必要はありません。しかし、既存のすべてのCephマネージャからCeph固有データをスクレイピングするようにPrometheusを設定することが推奨されます。これにより、ビルトインの高可用性メカニズムが実現し、1つのCeph Managerがダウンした場合、マネージャホストで実行されているサービスが別のマネージャホストで自動的に再起動されるようになります。

1. [Grafana Web UI](https://docs.ceph.com/en/pacific/mgr/dashboard/)
2. vonage\-status\-panelとgrafana\-piechart\-panelのプラグインをインストールします：

```
grafana-cli plugins install vonage-status-panel
grafana-cli plugins install grafana-piechart-panel
```

1. Grafanaにダッシュボードを追加します：

ダッシュボードは、ダッシュボードのJSONファイルをインポートすることでGrafanaに追加することができます。JSONファイルをダウンロードするには以下のコマンドを使用します：

```
wget https://raw.githubusercontent.com/ceph/ceph/master/monitoring/grafana/dashboards/<Dashboard-name>.json
```

ダッシュボードの各種JSONファイルは、[こちら](https://docs.ceph.com/en/pacific/mgr/dashboard/)でご覧いただけます。

例えば、ceph\-clusterの概要の場合、次のようになります：

```
wget https://raw.githubusercontent.com/ceph/ceph/master/monitoring/grafana/dashboards/ceph-cluster.json
```

また、独自のダッシュボードを作成することも可能です。

1. /etc/grafana/grafana.ini で匿名モードを設定します：

```
[auth.anonymous]
enabled = true
org_name = Main Org.
org_role = Viewer
```

Grafanaの新しいバージョン（6.2.0\-beta1以降）では、allow\_embeddingという新しい設定が導入されています。Ceph DashboardでのGrafana統合を動作させるには、デフォルトがfalseであるため、この設定を明示的にtrueに設定する必要があります。

```
[security]
allow_embedding = true
```

#### Enabling RBD\-Image monitoring[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

RBDイメージの監視は、パフォーマンスに大きな影響を与えるため、デフォルトでは無効になっています。詳しくは[RBD IO統計情報](https://docs.ceph.com/en/pacific/mgr/dashboard/)をご覧ください。無効化すると、Grafanaでは概要と詳細のダッシュボードが空になり、Prometheusではメトリクスが表示されなくなります。

#### Configuring Dashboard[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

GrafanaとPrometheusのセットアップが完了したら、Ceph DashboardがGrafanaにアクセスするために使用する接続情報を設定する必要があります。

どのURLでGrafanaインスタンスが動作/展開されているかをダッシュボードに伝える必要があります：

```
$ ceph dashboard set-grafana-api-url <grafana-server-url>  # default: ''
```

urlの形式は: \<プロトコル＞:＜IPアドレス＞:＜ポート＞.

注：Ceph Dashboardは、iframe HTMLエレメントを介してGrafanaダッシュボードを埋め込みます。GrafanaがSSL/TLSサポートなしで構成されている場合、ダッシュボードにSSLサポートが有効になっていると（デフォルトです）、ほとんどのブラウザは安全でないコンテンツの埋め込みをブロックします。上記のように有効にした後、埋め込まれた Grafana ダッシュボードが表示されない場合は、mixedコンテンツのブロックを解除する方法について、ブラウザのドキュメントを確認してください。または、Grafana の SSL/TLS サポートを有効にすることを検討してください。

Grafanaに自己署名証明書を使用している場合、ダッシュボードの証明書検証を無効にして、不明なCAによって署名された証明書やホスト名と一致しない証明書の結果として生じる接続拒否を回避してください：

```
$ ceph dashboard set-grafana-api-ssl-verify False
```

また、Grafanaに直接アクセスして、クラスタを監視することもできます。

注：Ceph Dashboardの設定情報は、設定を解除することも可能です。例えば、上記で設定したGrafana API URLをクリアする場合：

```
$ ceph dashboard reset-grafana-api-url
```

#### Alternative URL for Browsers[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

Ceph Dashboardバックエンドは、フロントエンドがGrafana Dashboardsをロードする前に、Grafana URLが存在することを確認できる必要があります。Ceph DashboardにGrafanaが実装されている性質上、Ceph DashboardでGrafanaグラフを表示できるようにするには、2つのコネクションが必要であることを意味します：

* バックエンド\(Ceph Mgrモジュール\)は、要求されたグラフの存在を確認する必要があります。この要求が成功すると、フロントエンドに、Grafanaに安全にアクセスできることを知らせます。
* そして、フロントエンドは、iframeを使用してユーザーのブラウザから直接Grafanaグラフを要求します。Grafanaインスタンスは、Ceph Dashboardを経由することなく直接アクセスされます。

さて、お使いの環境によっては、ユーザーのブラウザがCeph Dashboardで設定したURLに直接アクセスすることが困難な場合があります。この問題を解決するために、フロントエンド（ユーザーのブラウザ）がGrafanaにアクセスするために使用すべきURLを伝えるためだけに使用される別のURLを設定することができます。この設定は、Cephadmによって設定されるGRAFANA\_API\_URLとは異なり、自動的に変更されることはありません（cephadmが監視サービスの展開に使用されている場合のみ）。

フロントエンドに返されるURLを変更するには、以下のコマンドを実行します：

```
$ ceph dashboard set-grafana-frontend-api-url <grafana-server-url>
```

このオプションに値が設定されていない場合は、単に GRAFANA\_API\_URL オプションの値にフォールバックします。設定されている場合、GrafanaにアクセスするためにこのURLを使用するようにブラウザに指示します。

### Enabling Single Sign\-On \(SSO\)[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

Ceph Dashboardは、SAML 2.0プロトコルによるユーザの外部認証に対応しています。認証はDashboardで実行されるため、まずユーザアカウントを作成し、必要なロールに関連付ける必要があります。ただし、認証プロセスは既存のIdP\(Identity Provider\)によって実行することができます。

注：Ceph DashboardのSSOサポートは、oneloginのpython\-samlライブラリに依存しています。ディストリビューションのパッケージ管理を使用するか、Pythonのpipインストーラを使用して、このライブラリがシステムにインストールされていることを確認してください。

Ceph DashboardでSSOを設定するには、以下のコマンドを使用する必要があります：

```
$ ceph dashboard sso setup saml2 <ceph_dashboard_base_url> <idp_metadata> {<idp_username_attribute>} {<idp_entity_id>} {<sp_x_509_cert>} {<sp_private_key>}
```

Parameters:

* **\<ceph\_dashboard\_base\_url\>**
* **\<idp\_metadata\>**
* **\<idp\_username\_attribute\>**
* **\<idp\_entity\_id\>**
* **\<sp\_x\_509\_cert\> / \<sp\_private\_key\>**

注：SAMLリクエストのissuer値は、このパターンに従います：  \<ceph\_dashboard\_base\_url\>/auth/saml2/metadata

現在の SAML 2.0 構成を表示するには、以下のコマンドを使用します：

```
$ ceph dashboard sso show saml2
```

注：onelogin\_settingsの詳細については、[oneloginのドキュメント](https://docs.ceph.com/en/pacific/mgr/dashboard/)をご確認ください。

SSOを無効にする場合：

```
$ ceph dashboard sso disable
```

SSOが有効になっているかどうかを確認するには：

```
$ ceph dashboard sso status
```

SSOを有効にするには：

```
$ ceph dashboard sso enable saml2
```

### Enabling Prometheus Alerting[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

Prometheusをアラートに利用するためには、[アラートルール](https://docs.ceph.com/en/pacific/mgr/dashboard/)を定義する必要があります。これらは[Alertmanager](https://docs.ceph.com/en/pacific/mgr/dashboard/)によって管理されます。AlertmanagerはPrometheusからのアラートを受信して管理するため、まだ使用していない場合は[インストール](https://docs.ceph.com/en/pacific/mgr/dashboard/)してください。

Alertmanagerの機能は、3つの異なる方法でダッシュボードで使用することができます：

1. ダッシュボードの通知レシーバーを利用する
2. Prometheus Alertmanager APIを使用する
3. 両方のソースを同時に使用する

3つの方法とも、アラートを通知します。両方のソースを使用すれば二重に通知されることはありませんが、サイレンスを管理するために少なくとも Alertmanager API を使用する必要があります。

1. ダッシュボードの通知レシーバーを利用する

これにより、Alertmanagerから[設定](https://docs.ceph.com/en/pacific/mgr/dashboard/)された通知を受け取ることができます。通知が送信されると、ダッシュボード内に通知が届きますが、アラートの管理はできません。

Alertmanagerの設定に、ダッシュボードの受信機と新しいルートを追加します。これは以下のようになります：

```
route:
  receiver: 'ceph-dashboard'
...
receivers:
  - name: 'ceph-dashboard'
    webhook_configs:
    - url: '<url-to-dashboard>/api/prometheus_receiver'
```

Alertmanagerがダッシュボードの観点からあなたのSSL証明書を有効であるとみなすことを確認してください。正しい設定の詳細については、[\<http\_config\>のドキュメント](https://docs.ceph.com/en/pacific/mgr/dashboard/)を確認してください。

1. PrometheusのAPIとAlertmanagerを利用する

これにより、アラートとサイレンスを管理することができ、「Cluster」メニューエントリの「Monitoring」セクションの「Active Alerts」、「All Alerts」、「Silences」タブが有効になります。

アラートは、名前、ジョブ、重要度、状態、開始時間によって並べ替えが可能です。残念ながら、アラートがいつAlertmanagerによる通知で発信されたかを設定に基づいて知ることはできません。そのため、ダッシュボードではアラートに目に見える変更があった場合に、変更されたアラートをユーザーに通知します。

サイレンスは、ID、作成者、状態、開始時間、更新時間、終了時間でソートすることができます。サイレンスは様々な方法で作成することができ、期限切れにすることも可能です。

1. ゼロから作る
2. 選択したアラートをベースに作成
3. 期限切れのサイレンスから再作成
4. サイレンスを更新する（再作成して期限切れにする（Alertmanagerのデフォルトの動作））

使用するには、Alertmanagerサーバーのホストとポートを指定します：

```
$ ceph dashboard set-alertmanager-api-host <alertmanager-host:port>  # default: ''
```

For example:

```
$ ceph dashboard set-alertmanager-api-host 'http://localhost:9093'
```

設定されたすべてのアラートを確認できるようにするには、Prometheus APIへのURLを設定する必要があります。このAPIを使用すると、新しいサイレンスが対応するアラートに一致するかどうかを確認する際にもUIが役に立ちます。

```
$ ceph dashboard set-prometheus-api-host <prometheus-host:port>  # default: ''
```

For example:

```
$ ceph dashboard set-prometheus-api-host 'http://localhost:9090'
```

ホスト設定後、ブラウザのダッシュボードウィンドウまたはタブを更新してください。

1. 両方の方式を使う

両方のメソッドの動作は、迷惑な重複通知がポップアップすることを通して、互いに邪魔にならないように設定されています。

PrometheusやAlertmanagerのセットアップで自己署名証明書を使用している場合、不明なCAによって署名された証明書やホスト名と一致しない証明書による接続拒否を避けるために、ダッシュボードで証明書の検証を無効にする必要があります。

* For Prometheus:
* For Alertmanager:

## User and Role Management[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

### Password Policy[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

デフォルトでは、パスワードポリシー機能が有効になっており、以下のチェックが含まれています：

* パスワードがN文字より長いか？
* 新旧のパスワードは同じものか？

パスワードポリシー機能は、完全にオンまたはオフにすることができます：

```
$ ceph dashboard set-pwd-policy-enabled <true|false>
```

また、以下の個別チェックのON/OFFが可能です：

```
$ ceph dashboard set-pwd-policy-check-length-enabled <true|false>
$ ceph dashboard set-pwd-policy-check-oldpwd-enabled <true|false>
$ ceph dashboard set-pwd-policy-check-username-enabled <true|false>
$ ceph dashboard set-pwd-policy-check-exclusion-list-enabled <true|false>
$ ceph dashboard set-pwd-policy-check-complexity-enabled <true|false>
$ ceph dashboard set-pwd-policy-check-sequential-chars-enabled <true|false>
$ ceph dashboard set-pwd-policy-check-repetitive-chars-enabled <true|false>
```

さらに、パスワードポリシーを設定するために、以下のオプションが用意されています。

* パスワードの最小長（デフォルトは8）

```
$ ceph dashboard set-pwd-policy-min-length <N>
```

* パスワードの複雑さの最小値（デフォルトは10）

```
$ ceph dashboard set-pwd-policy-min-complexity <N>
```

パスワードの複雑さは、パスワードに含まれる各文字を分類することで算出されます。複雑さのカウントは0から始まり、文字は以下のルールで評価され、与えられた順番で評価されます。

文字が数字の場合、1増加する

小文字のASCII文字であれば1増加する

ASCII 文字の大文字の場合は 2増加する

\!"\#$%&'\(\)\*\+,\-./:;\<=\>?@\[\]^\_\`{|}~のような特殊文字の場合は3増加する

上記のルールで分類されていないキャラクターは、5増加する

* パスワードに使用できない単語をカンマ区切りで列挙したもの：

```
$ ceph dashboard set-pwd-policy-exclusion-list <word>[,...]
```

### User Accounts[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

Ceph Dashboardは、複数のユーザアカウントをサポートしています。各ユーザーアカウントは、ユーザー名、パスワード\(bcryptを使用して暗号化して保存\)、任意の名前、任意の電子メールアドレスで構成されます。

Web UIで新規ユーザーを作成する場合、初回ログイン時に新しいパスワードを割り当てるオプションを設定することが可能です。

ユーザアカウントはモニタの設定データベースに保存され、すべてのceph\-mgrインスタンスで利用できます。

ユーザーアカウントを管理するためのCLIコマンド：

* _ユーザーを表示：_

```
$ ceph dashboard ac-user-show [<username>]
```

* _ユーザーの作成：_

```
$ ceph dashboard ac-user-create [--enabled] [--force-password] [--pwd_update_required] <username> -i <file-containing-password> [<rolename>] [<name>] [<email>] [<pwd_expiration_date>]
```

パスワードポリシーのチェックを回避するには、force\-password オプションを使用します。pwd\_update\_required オプションを追加し、新しく作成されたユーザーが最初のログイン後にパスワードを変更しなければならないようにします。

* _ユーザーの削除：_

```
$ ceph dashboard ac-user-delete <username>
```

* _パスワードの変更：_

```
$ ceph dashboard ac-user-set-password [--force-password] <username> -i <file-containing-password>
```

* _パスワードのハッシュ変更：_

```
$ ceph dashboard ac-user-set-password-hash <username> -i <file-containing-password-hash>
```

ハッシュはbcryptハッシュとソルトで、例えば$2b$12$Pt3Vq/rDt2y9glTPSV.VFegiLkQeIpddtkhoFetNApYmIJOY8gau2.などでなければなりません。これは、外部データベースからユーザーをインポートするために使用することができます。

* _ユーザー（名前、メールアドレス）を変更：_

```
$ ceph dashboard ac-user-set-info <username> <name> <email>
```

* _ユーザーを無効化：_

```
$ ceph dashboard ac-user-disable <username>
```

* _ユーザーを有効化：_

```
$ ceph dashboard ac-user-enable <username>
```

### User Roles and Permissions[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

ユーザーアカウントは、ダッシュボードのどの機能にアクセスできるかを定義する一連のロールと関連付けられています。

Dashboard の機能/モジュールは、セキュリティスコープ内にグループ化されています。セキュリティスコープはあらかじめ定義されており、静的なものです。現在利用可能なセキュリティスコープは以下の通りです：

* **hosts**
* **config\-opt**
* **pool**
* **osd**
* **monitor**
* **rbd\-image**
* **rbd\-mirroring**
* **iscsi**
* **rgw**
* **cephfs**
* **nfs\-ganesha**
* **manager**
* **log**
* **grafana**
* **prometheus**
* **dashboard\-settings**

ロールは、セキュリティスコープと一連のパーミッションの間のマッピングを指定します。パーミッションには4つのタイプがあります：

* **read**
* **create**
* **update**
* **delete**

Python辞書形式のロール指定の例については、以下を参照してください：

```
# example of a role
{
  'role': 'my_new_role',
  'description': 'My new role',
  'scopes_permissions': {
    'pool': ['read', 'create'],
    'rbd-image': ['read', 'create', 'update', 'delete']
  }
}
```

上記のロールにより、ユーザーはプール管理に関連する機能の読み取り権限と作成権限を持ち、RBDイメージ管理に関連する機能の全権限を持つことが指示されます。

Dashboardには、システムロールと呼ぶ定義済みのロールのセットが用意されており、Ceph Dashboardの新規インストールですぐに使用できます。

システムロールの一覧：

* **administrator**
* **read\-only**
* **block\-manager**
* **rgw\-manager**
* **cluster\-manager**
* **pool\-manager**
* **cephfs\-manager**

利用可能なロールの一覧は、以下のコマンドで取得することができます：

```
$ ceph dashboard ac-role-show [<rolename>]
```

CLIを使用して新しいロールを作成することもできます。利用可能なコマンドは以下のとおりです：

* _ロール作成：_

```
$ ceph dashboard ac-role-create <rolename> [<description>]
```

* _ロール削除：_

```
$ ceph dashboard ac-role-delete <rolename>
```

* _ロールにスコープパーミッションを追加：_

```
$ ceph dashboard ac-role-add-scope-perms <rolename> <scopename> <permission> [<permission>...]
```

* _ロールからスコープパーミッションを削除：_

```
$ ceph dashboard ac-role-del-scope-perms <rolename> <scopename>
```

ユーザーにロールを割り当てるには、次のコマンドを使用します：

* _ユーザーロールを設定：_

```
$ ceph dashboard ac-user-set-roles <username> <rolename> [<rolename>...]
```

* _ユーザーロールの追加：_

```
$ ceph dashboard ac-user-add-roles <username> <rolename> [<rolename>...]
```

* _ユーザーロールを削除：_

```
$ ceph dashboard ac-user-del-roles <username> <rolename> [<rolename>...]
```

### Example of User and Custom Role Creation[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

このセクションでは、RBDイメージの管理、Cephプールの表示と作成が可能で、他のスコープへの読み取り専用アクセスを持つユーザアカウントを作成するコマンドの完全な例を示します。

1. _ユーザーを作成します。_

```
$ ceph dashboard ac-user-create bob -i <file-containing-password>
```

1. _ロールを作成し、スコープの権限を指定します。_

```
$ ceph dashboard ac-role-create rbd/pool-manager
$ ceph dashboard ac-role-add-scope-perms rbd/pool-manager rbd-image read create update delete
$ ceph dashboard ac-role-add-scope-perms rbd/pool-manager pool read create
```

1. _ユーザーにロールを関連付けます。_

```
$ ceph dashboard ac-user-set-roles bob rbd/pool-manager read-only
```

## Proxy Configuration[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

複数のceph\-mgrインスタンスを持つCephクラスタでは、現在アクティブなceph\-mgrデーモン上で動作しているダッシュボードのみが、受信リクエストに応答します。スタンバイ状態のceph\-mgrインスタンスでダッシュボードのTCPポートに接続すると、アクティブマネージャのダッシュボードURLへのHTTPリダイレクト\(303\)を受け取ります。これにより、ダッシュボードにアクセスするために、任意のceph\-mgrインスタンスにブラウザを向けることができます。

ダッシュボードに到達するための固定URLを確立したい場合、またはマネージャノードへの直接接続を許可したくない場合、アクティブなceph\-mgrインスタンスに受信要求を自動的に転送するプロキシを設定することが可能です。

### Configuring a URL Prefix[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

リバースプロキシ経由でダッシュボードにアクセスする場合、URLプレフィックスでサービスしたい場合があります。ダッシュボードがプレフィックスを含むハイパーリンクを使用するようにするには、url\_prefixの設定を行うことができます：

```
ceph config set mgr mgr/dashboard/url_prefix $PREFIX
```

これで、http://$IP:$PORT/$PREFIX/でダッシュボードにアクセスできるようになります。

### Disable the redirection[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

ダッシュボードがHAProxyのような負荷分散プロキシの後ろにある場合、内部の（解決できない）URLがフロントエンドクライアントに公開される状況を防ぐために、リダイレクトを無効にしたい場合があります。次のコマンドを使用すると、アクティブなダッシュボードにリダイレクトする代わりに、HTTPエラー（デフォルトでは500）で応答するようになります。

```
$ ceph config set mgr mgr/dashboard/standby_behaviour "error"
```

リダイレクトの設定をデフォルトに戻すには、次のコマンドを使用します：

```
$ ceph config set mgr mgr/dashboard/standby_behaviour "redirect"
```

### Configure the error status code[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

リダイレクトを無効にした場合、待機系ダッシュボードのHTTPステータスコードをカスタマイズしたい場合があります。これを行うには、コマンドを実行する必要があります：

```
$ ceph config set mgr mgr/dashboard/standby_error_status_code 503
```

### HAProxy example configuration[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

以下は、HAProxyを使用したSSL/TLSパススルーの設定例です。

なお、この設定は以下の条件下で動作します。ダッシュボードがフェイルオーバーした場合、フロントエンドクライアントは HTTP リダイレクト \(303\) 応答を受け取ることがあり、解決不可能なホストにリダイレクトされます。これは、2つのHAProxyヘルスチェックの間でフェイルオーバーが発生した場合に起こります。この状況では、以前アクティブだったダッシュボードノードが、新しいアクティブなノードを指す303で応答するようになります。このような状況を防ぐには、スタンバイ ノードでのリダイレクトを無効にすることを検討する必要があります。

```
defaults
  log global
  option log-health-checks
  timeout connect 5s
  timeout client 50s
  timeout server 450s

frontend dashboard_front
  mode http
  bind *:80
  option httplog
  redirect scheme https code 301 if !{ ssl_fc }

frontend dashboard_front_ssl
  mode tcp
  bind *:443
  option tcplog
  default_backend dashboard_back_ssl

backend dashboard_back_ssl
  mode tcp
  option httpchk GET /
  http-check expect status 200
  server x <HOST>:<PORT> ssl check verify none
  server y <HOST>:<PORT> ssl check verify none
  server z <HOST>:<PORT> ssl check verify none
```

## Auditing API Requests[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

REST APIは、PUT、POST、DELETE要求をCeph監査ログに記録できます。この機能はデフォルトでは無効になっていますが、次のコマンドで有効にできます：

```
$ ceph dashboard set-audit-api-enabled <true|false>
```

有効にすると、各リクエストごとに以下のパラメータがログに記録されます：

* from \- リクエストの発信元 e.g. https://\[::1\]:44410
* path \- REST APIのパス e.g. /api/auth
* method \- e.g. PUT, POST or DELETE
* user \- ユーザー名、それ以外は'None'

要求ペイロード（引数とその値）のロギングはデフォルトで有効になっています。この動作を無効にするには、次のコマンドを実行します：

```
$ ceph dashboard set-audit-api-log-payload <true|false>
```

ログエントリーは次のようになります：

```
2018-10-22 15:27:01.302514 mgr.x [INF] [DASHBOARD] from='https://[::ffff:127.0.0.1]:37022' path='/api/rgw/user/klaus' method='PUT' user='admin' params='{"max_buckets": "1000", "display_name": "Klaus Mustermann", "uid": "klaus", "suspended": "0", "email": "klaus.mustermann@ceph.com"}'
```

## NFS\-Ganesha Management[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

ダッシュボードでは、NFS クラスタと NFS エクスポートを管理するために使用される NFS モジュールを有効にする必要があります。詳細については、「[CephFSとRGW Exports over NFS](https://docs.ceph.com/en/pacific/mgr/dashboard/)」を参照してください。

## Plug\-ins[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

プラグインは、Ceph Dashboardの機能をモジュール方式および疎結合方式で拡張します。

### Feature Toggles[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

このプラグインを使用すると、Ceph Dashboardから一部の機能をオンデマンドで有効化または無効化することができます。機能が無効化されると：

* そのフロントエンドのエレメント（ウェブページ、メニュー項目、チャートなど）は非表示になります。
* その関連するREST APIエンドポイントは、それ以上のリクエストを拒否します（404, Not Found Error）。

このプラグインの主な目的は、ダッシュボードが公開するワークフローをアドホックにカスタマイズできるようにすることです。さらに、最小限の設定負荷とサービスへの影響なしに、Experimentalな機能を動的に有効にすることができます。

有効化／無効化できる機能の一覧：

* Image Management: rbd
    Mirroring: mirroring
    iSCSI: iscsi
    **Block \(RBD\)**
* **Filesystem \(Cephfs\)**
* **Objects \(RGW\)**
* **NFS**

デフォルトでは、すべての機能が有効になっています。

機能の一覧と現在のステータスを取得するには：

```
$ ceph dashboard feature status
Feature 'cephfs': 'enabled'
Feature 'iscsi': 'enabled'
Feature 'mirroring': 'enabled'
Feature 'rbd': 'enabled'
Feature 'rgw': 'enabled'
Feature 'nfs': 'enabled'
```

単一または複数の機能のステータスを有効または無効にするには：

```
$ ceph dashboard feature disable iscsi mirroring
Feature 'iscsi': disabled
Feature 'mirroring': disabled
```

機能のステータスが変更された後、APIのRESTエンドポイントはその変更に即座に対応しますが、フロントエンドのUI要素については、その反映に最大で20秒かかる場合があります。

### Debug[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

デバッグモードに応じてダッシュボードの挙動をカスタマイズできるプラグインです。以下のコマンドで有効化、無効化、チェックが可能です：

```
$ ceph dashboard debug status
Debug: 'disabled'
$ ceph dashboard debug enable
Debug: 'enabled'
$ ceph dashboard debug disable
Debug: 'disabled'
```

デフォルトでは、無効になっています。これは、実稼働環境でのデプロイメントに推奨される設定です。必要であれば、再起動することなく、デバッグモードを有効にすることができます。現在、デバッグモードを無効にするとCherryPyの本番環境と同じになり、有効にするとtest\_suiteのデフォルトが使われます（詳しくは[CherryPy Environments](https://docs.ceph.com/en/pacific/mgr/dashboard/)を参照してください）。

また、Cherrypyが対応していないバージョンでは、リクエストのuuid（unique\_id）を追加しています。さらに、エラーレスポンスとログメッセージにunique\_idを表示します。

### Message of the day \(MOTD\)[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

Ceph Dashboardの上部に設定された日替わりメッセージを表示します。

MOTDの重要度は、情報、警告、危険の3つの重要度によって設定することができます。MOTDは一定時間後に失効することができ、これはUIに表示されなくなることを意味します。有効期限を指定するには、次の構文を使用します。Ns|m|h|d|w は、秒、分、時、日、週を表します。MOTDが2時間後に期限切れとなる場合は2h、5週間の場合は5wを使用します。有効期限のないMOTDを設定する場合は、0を使用します。

MOTDを設定する場合は、以下のコマンドを実行します：

```
$ ceph dashboard motd set <severity:info|warning|danger> <expires> <message>
```

設定されたMOTDを表示するには：

```
$ ceph dashboard motd get
```

設定したMOTD runをクリアするには：

```
$ ceph dashboard motd clear
```

重要度「info」「warning」のMOTDは、ユーザーが閉じることができます。infoのMOTDは、ローカルストレージのCookieをクリアするか、別の深刻度のMOTDが表示されるまで表示されなくなります。warningの深刻度のMOTDは、新しいセッションで再び表示されます。

## Troubleshooting the Dashboard[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

### Locating the Dashboard[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

Ceph Dashboardの場所がわからない場合は、次のコマンドを実行します：

```
$ ceph mgr services | jq .dashboard
"https://host:port"
```

このコマンドは、Ceph Dashboardが配置されているURLを返します。https://\<host\>:\<port\>/。

注：多くのCephツールは、結果をJSON形式で返します。JSONデータでの作業を容易にするため、jqコマンドラインユーティリティをインストールすることをお勧めします。

### Accessing the Dashboard[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

Ceph Dashboardにアクセスできない場合は、次のコマンドを実行します：

1. Ceph Dashboardモジュールが有効になっていることを確認します：

```
$ ceph mgr module ls | jq .enabled_modules
```

コマンドの戻り値にCeph Dashboardモジュールが表示されていることを確認します。上記のコマンドの出力の一部を抜粋した例です：

```
[
  "dashboard",
  "iostat",
  "restful"
]
```

1. 表示されていない場合は、以下のコマンドでモジュールをアクティベートしてください：

```
$ ceph mgr module enable dashboard
```

Ceph Dashboardおよび/またはceph\-mgrのログファイルで、エラーがないか確認します。

ceph\-mgrログメッセージがファイルに書き込まれるかどうかを確認します：

```
$ ceph config get mgr log_to_file
true
```

ログファイルの場所を取得します（デフォルトでは/var/log/ceph/\<クラスタ名\>\-\<デーモン名\>.log）：

```
$ ceph config get mgr log_file
/var/log/ceph/$cluster-$name.log
```

1. SSL/TSLサポートが正しく設定されていることを確認します：

SSL/TSLサポートが有効かどうか確認します：

```
$ ceph config get mgr mgr/dashboard/ssl
```

コマンドがtrueを返した場合、以下の方法で証明書が存在することを確認します：

```
$ ceph config-key get mgr/dashboard/crt
```

and:

```
$ ceph config-key get mgr/dashboard/key
```

trueを返さない場合は、以下のコマンドを実行して自己署名証明書を生成するか、「[SSL/TLSサポート](https://docs.ceph.com/en/pacific/mgr/dashboard/)」で説明されている手順に従ってください： 

```
$ ceph dashboard create-self-signed-cert
```

### Trouble Logging into the Dashboard[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

Ceph Dashboardにログインできず、以下のエラーが表示された場合は、以下の手順で確認してください：
![invalid-credentials.png](image/invalid-credentials.png)

1. ユーザー認証が正しいかどうか確認してください。Ceph Dashboardにログインしようとしたときに上記のような通知メッセージが表示される場合は、間違った認証情報を使用している可能性があります。ユーザ名とパスワードを再度確認し、キーボードの大文字ロックが誤って有効になっていないことを確認してください。
2. ユーザー認証が正しいのに、同じエラーが発生する場合は、そのユーザーアカウントが存在するかどうかを確認してください：

```
$ ceph dashboard ac-user-show <username>
```

このコマンドは、あなたのユーザーデータを返します。ユーザーが存在しない場合：

```
$ Error ENOENT: User <username> does not exist
```

1. ユーザーが有効かどうかを確認します：

```
$ ceph dashboard ac-user-show <username> | jq .enabled
true
```

ユーザーのenabledがtrueに設定されているかどうか確認します。有効になっていない場合：

```
$ ceph dashboard ac-user-enable <username>
```

詳しくは、「[ユーザーと役割の管理](https://docs.ceph.com/en/pacific/mgr/dashboard/)」をご覧ください。

### A Dashboard Feature is Not Working[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

バックエンドでエラーが発生した場合、通常フロントエンドにエラー通知が届きます。以下のシナリオを実行してデバッグしてください。

1. Ceph Dashboardおよびceph\-mgrのログファイルにエラーがないか確認します。500 Internal Server Errorなどのキーワードの後にtracebackを付けて検索すると、これらのエラーを見つけることができます。トレースバックの末尾には、発生した正確なエラーの詳細が記載されています。
2. WebブラウザーのJavascriptコンソールで、エラーが発生していないか確認してください。

### Ceph Dashboard Logs[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

#### Dashboard Debug Flag[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

このフラグを有効にすると、エラートレースバックがバックエンドのレスポンスに含まれるようになります。

Ceph Dashboardでこのフラグを有効にするには、ClusterからManager modulesに移動します。Dashboardモジュールを選択し、editボタンをクリックします。debugチェックボックスをクリックし、更新します。

CLIで有効にするには、次のコマンドを実行します：

```
$ ceph dashboard debug enable
```

#### Setting Logging Level of Dashboard Module[¶](https://docs.ceph.com/en/pacific/mgr/dashboard/ "Permalink to this headline")

ログレベルをdebugに設定すると、ログがより冗長になり、デバッグに役立ちます。

1. マネージャーデーモンのロギングレベルを上げるには：

```
$ ceph tell mgr config set debug_mgr 20
```

1. ダッシュボードまたはCLIを使用して、Ceph Dashboardモジュールのログレベルを調整します：

ClusterからManager modulesに移動します。Dashboard モジュールを選択し、editボタンをクリックします。log\_levelの設定を変更します。

CLIで調整する場合は、以下のコマンドを実行します：

```
$ bin/ceph config set mgr mgr/dashboard/log_level debug
```

\#.  ログレベルが高くなると、ログ量がかなり多くなり、ファイルシステムが簡単にいっぱいになってしまうことがあります。この一時的なログの増加を元に戻すために、1時間後、1日後、または1週間後のカレンダーリマインダを設定します。 これは次のようなものです：

```
$ ceph config log
...
--- 11 --- 2020-11-07 11:11:11.960659 --- mgr.x/dashboard/log_level = debug ---
...
$ ceph config reset 11
```
