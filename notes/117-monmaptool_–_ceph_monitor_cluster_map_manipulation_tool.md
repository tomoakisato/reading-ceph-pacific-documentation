# 117: monmaptool – ceph monitor cluster map manipulation tool

 # monmaptool – ceph monitor cluster map manipulation tool[¶](https://docs.ceph.com/en/pacific/man/8/monmaptool/ "Permalink to this headline")

## Synopsis[¶](https://docs.ceph.com/en/pacific/man/8/monmaptool/ "Permalink to this headline")

**monmaptool** \<action\> \[options\] _mapfilename_

## Description[¶](https://docs.ceph.com/en/pacific/man/8/monmaptool/ "Permalink to this headline")

**monmaptool** is a utility to create, view, and modify a monitor cluster map for the Ceph distributed storage system. The monitor map specifies the only fixed addresses in the Ceph distributed system. All other daemons bind to arbitrary addresses and register themselves with the monitors.

When creating a map with –create, a new monitor map with a new, random UUID will be created. It should be followed by one or more monitor addresses.

The default Ceph monitor port for messenger protocol v1 is 6789, and 3300 for protocol v2.

Multiple actions can be performed per invocation.

## Options[¶](https://docs.ceph.com/en/pacific/man/8/monmaptool/ "Permalink to this headline")

`--print```[¶](https://docs.ceph.com/en/pacific/man/8/monmaptool/ "Permalink to this definition")print a plaintext dump of the map, after any modifications are made.

`--feature-list`` [plain|parseable]`[¶](https://docs.ceph.com/en/pacific/man/8/monmaptool/ "Permalink to this definition")list the enabled features as well as the available ones.

By default, a human readable output is produced.

`--create```[¶](https://docs.ceph.com/en/pacific/man/8/monmaptool/ "Permalink to this definition")create a new monitor map with a new UUID \(and with it, a new, empty Ceph cluster\).

`--clobber```[¶](https://docs.ceph.com/en/pacific/man/8/monmaptool/ "Permalink to this definition")allow monmaptool to create a new mapfilename in place of an existing map.

Only useful when _–create_ is used.

`--generate```[¶](https://docs.ceph.com/en/pacific/man/8/monmaptool/ "Permalink to this definition")generate a new monmap based on the values on the command line or specified in the ceph configuration. This is, in order of preference,

> 1. `--monmap filename` to specify a monmap to load
> 2. `--mon-host 'host1,ip2'` to specify a list of hosts or ip addresses
> 3. `[mon.foo]` sections containing `mon addr` settings in the config. Note that this method is not recommended and support will be removed in a future release.

`--filter-initial-members```[¶](https://docs.ceph.com/en/pacific/man/8/monmaptool/ "Permalink to this definition")filter the initial monmap by applying the `mon initial members`setting. Monitors not present in that list will be removed, and initial members not present in the map will be added with dummy addresses.

`--add`` name ip[:port]`[¶](https://docs.ceph.com/en/pacific/man/8/monmaptool/ "Permalink to this definition")add a monitor with the specified ip:port to the map.

If the _nautilus_ feature is set, and the port is not, the monitor will be added for both messenger protocols.

`--addv`` name [protocol:ip:port[,...]]`[¶](https://docs.ceph.com/en/pacific/man/8/monmaptool/ "Permalink to this definition")add a monitor with the specified version:ip:port to the map.

`--rm`` name`[¶](https://docs.ceph.com/en/pacific/man/8/monmaptool/ "Permalink to this definition")remove the monitor with the specified name from the map.

`--fsid`` uuid`[¶](https://docs.ceph.com/en/pacific/man/8/monmaptool/ "Permalink to this definition")set the fsid to the given uuid. If not specified with _–create_, a random fsid will be generated.

`--feature-set`` value [--optional|--persistent]`[¶](https://docs.ceph.com/en/pacific/man/8/monmaptool/ "Permalink to this definition")enable a feature.

`--feature-unset`` value [--optional|--persistent]`[¶](https://docs.ceph.com/en/pacific/man/8/monmaptool/ "Permalink to this definition")disable a feature.

`--enable-all-features```[¶](https://docs.ceph.com/en/pacific/man/8/monmaptool/ "Permalink to this definition")enable all supported features.

`--set-min-mon-release`` release`[¶](https://docs.ceph.com/en/pacific/man/8/monmaptool/ "Permalink to this definition")set the min\_mon\_release.

## Example[¶](https://docs.ceph.com/en/pacific/man/8/monmaptool/ "Permalink to this headline")

To create a new map with three monitors \(for a fresh Ceph cluster\):

```
monmaptool --create --add nodeA 192.168.0.10 --add nodeB 192.168.0.11 
  --add nodeC 192.168.0.12 --enable-all-features --clobber monmap
```

To display the contents of the map:

```
monmaptool --print monmap
```

To replace one monitor:

```
monmaptool --rm nodeA monmap
monmaptool --add nodeA 192.168.0.9 monmap
```

## Availability[¶](https://docs.ceph.com/en/pacific/man/8/monmaptool/ "Permalink to this headline")

**monmaptool** is part of Ceph, a massively scalable, open\-source, distributed storage system. Please refer to the Ceph documentation at [http://ceph.com/docs](https://docs.ceph.com/en/pacific/man/8/monmaptool/)for more information.

## See also[¶](https://docs.ceph.com/en/pacific/man/8/monmaptool/ "Permalink to this headline")

[ceph](https://docs.ceph.com/en/pacific/man/8/monmaptool/)\(8\),[crushtool](https://docs.ceph.com/en/pacific/man/8/monmaptool/)\(8\),
