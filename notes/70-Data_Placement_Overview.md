# 70: Data Placement Overview

**クリップソース:** [70: Data Placement Overview — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/operations/data-placement/)

# Data Placement Overview[¶](https://docs.ceph.com/en/pacific/rados/operations/data-placement/ "Permalink to this headline")

Cephは、RADOSクラスタ全体のデータオブジェクトを動的に保存、複製、リバランスします。 

多くの異なるユーザが無数のOSDに目的別にオブジェクトを異なるプールに保存するため、Cephの操作にはデータ配置計画が必要です。 

Cephの主なデータ配置計画の概念は以下のとおりです。

* **Pools:**
* **Placement Groups:**
* **CRUSH Maps:**
* **Balancer:**

最初にテスト・クラスターをセットアップするときは、デフォルト値を使用できます。大規模なCephクラスタの計画を開始したら、データの配置操作については、プール、PG、CRUSHを参照してください。
