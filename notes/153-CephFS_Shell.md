# 153: CephFS Shell

**クリップソース:** [153: CephFS Shell — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/)

# CephFS Shell[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

CephFS Shellは、[Ceph File System](https://docs.ceph.com/en/pacific/glossary/#term-Ceph-File-System)と直接対話するシェルのようなコマンドを提供します。

このツールは、非対話型モードだけでなく、対話型モードでも使用することができます。前者の場合、cephfs\-shellはシェルセッションを開き、与えられたコマンドが終了した後、プロンプト文字列を表示して無期限に待機します。シェルセッションが終了すると、最後に実行したコマンドの戻り値で終了します。非対話型モードでは、cephfs\-shellはコマンドを発行し、コマンドの実行完了直後にコマンドの戻り値で終了します。

CephFS Shellの動作は、cephfs\-shell.confを使用して微調整することができます。詳細は、「[CephFS Shell設定ファイル](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/#cephfs-shell-configuration-file)」を参照してください。

Usage :

cephfs\-shell \[options\] \[command\] cephfs\-shell \[options\] – \[command, command,…\]

Options :

\-c, \-\-config FILE

cephfs\-shell.confへのパス

\-b, \-\-batch FILE

バッチファイルへのパス

\-t, \-\-test FILE

テスト用のFILE内のトランスクリプトへのパス

注意：cephfs\-shellの実行には、最新バージョンのcmd2モジュールが必要です。CephFSをソースからインストールした場合、ビルドディレクトリでcephfs\-shellを実行します。また、virtualenvを使用して以下のように実行することも可能です：

```
[build]$ python3 -m venv venv && source venv/bin/activate && pip3 install cmd2
[build]$ source vstart_environment.sh && source venv/bin/activate && python3 ../src/tools/cephfs/cephfs-shell
```

## Commands[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

### mkdir[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

ディレクトリが存在しない場合は、作成する

Usage :

mkdir \[\-option\] \<directory\>…

    * directory \- 作成されるディレクトリの名前

Options :

\-m MODE

新しいディレクトリのアクセスモード

\-p, \-\-parent

必要に応じて親ディレクトリを作成する。このオプションを指定した場合、すでにディレクトリが存在してもエラーにはならない

### put[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

ローカルファイルシステムからCephファイルシステムにファイル/ディレクトリをコピーする

Usage :

put \[options\] \<source\_path\> \[target\_path\]

    * . は，ローカルの作業ディレクトリにあるすべてのファイル/ディレクトリをコピーする
        \- は、標準入力から入力を読み込む
        source\_path \- cephfsにコピーされるローカルファイル/ディレクトリのパス
    * . は、ファイル/ディレクトリがリモート作業ディレクトリにコピーされる
        target\_path \- ファイル/ディレクトリのコピー先となるリモートディレクトリのパス

Options :

\-f, \-\-force

宛先が既に存在する場合は上書きする

### get[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

Ceph File SystemからLocal File Systemにファイルをコピーする

Usage :

get \[options\] \<source\_path\> \[target\_path\]

    * . は、リモートの作業ディレクトリにあるすべてのファイル/ディレクトリをコピーする
        source\_path \- ローカルファイルシステムにコピーされるリモートファイル/ディレクトリのパス
    * . は、ファイル/ディレクトリがローカルの作業ディレクトリにコピーされる
        \- は、 標準出力に出力を書き込む
        target\_path \-ファイル/ディレクトリのコピー先となるローカルディレクトリのパス

Options:

\-f, \-\-force

宛先が既に存在する場合は上書きする

### ls[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

現在の作業ディレクトリにあるすべてのファイルとディレクトリをリストアップする

Usage :

ls \[option\] \[directory\]…

    * デフォルトでは、現在の作業ディレクトリのファイル/ディレクトリがリストアップされる
        directory \- リストアップされるファイル/ディレクトリのディレクトリ名

Options:

\-l, \-\-long

長い形式のリスト\-パーミションを表示

\-r, \-\-reverse

リバースソート

\-H

ヒューマンリーダブル

\-a, \-all

.で始まるエントリーは無視する

\-S

ファイルサイズでソート

### cat[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

ファイルを連結して標準出力に出力する

Usage :

cat  \<file\>….

    * file \- ファイル名

### cd[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

現在の作業ディレクトリを変更する

Usage :

cd \[directory\]

    * '.'の場合、カレントディレクトリの親ディレクトリに移動する
        directory \- パス/ディレクトリ名。ディレクトリが記載されていない場合は、ルートディレクトリに変更する

### cwd[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

現在の作業ディレクトリを取得する

Usage :

cwd

### quit/Ctrl \+ D[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

シェルを閉じる

### chmod[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

ファイル/ディレクトリのパーミッションを変更する

Usage :

chmod \<mode\> \<file/directory\>

### mv[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

ファイル／ディレクトリを移動元から移動先に移動するUsage :

mv \<source\_path\> \<destination\_path\>

### rmdir[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

ディレクトリを削除する

Usage :

rmdir \<directory\_name\>…..

### rm[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

ファイルを削除する

Usage :

rm \<file\_name/pattern\>…

### write[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

ファイルの作成と書き込み

Usage :

write \<file\_name\> \<Enter Data\> Ctrl\+D Exit.

### lls[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

指定されたディレクトリにあるすべてのファイルとディレクトリをリストアップする。パスが指定されていない場合は、現在のローカルディレクトリのファイルとディレクトリがリストアップされる

Usage:

lls \<path\>…..

### lcd[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

指定されたローカルディレクトリに移動する

Usage :

lcd \<path\>

### lpwd[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

現在のローカルディレクトリの絶対パスを表示する

Usage :

lpwd

### umask[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

ファイルモード作成マスクの設定・取得

Usage :

umask \[mode\]

### alias[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

エイリアスの定義または表示

Usage:

alias \[name\] | \[\<name\> \<value\>\]

    * name \- 検索、追加、置換されるエイリアスの名前
    * value \- エイリアスの解決方法 \(追加または置換の場合\) これにはスペースを含めることができ、引用符で囲む必要はない

### run\_pyscript[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

コンソール内でPythonスクリプトを実行する

Usage:

run\_pyscript \<script\_path\> \[script\_arguments\]

    * コンソールコマンドは cmd \("your command"\) を使ってこのスクリプトの中で実行することができる。スペースを含むパスや引数は引用符で囲む必要がある

注：このコマンドは、cmd2バージョン0.9.13以下ではpyscriptとして利用可能

### py[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

Pythonのコマンド、シェル、スクリプトを呼び出す

Usage :

py \<command\>: Python commandを実行する。 py: Python の対話型モードに入る

### shortcuts[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

利用可能なショートカット（エイリアス）を一覧表示

Usage :

shortcuts

### history[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

過去に入力したコマンドの表示、実行、編集、保存

Usage :

history \[\-h\] \[\-r | \-e | \-s | \-o FILE | \-t TRANSCRIPT\] \[arg\]

Options:

\-h

このヘルプメッセージを表示し、終了する

\-r

選択された履歴を実行する

\-e

選択した履歴項目を編集し、実行する

\-s

スクリプト形式; 分離行なし

\-o FILE

スクリプトファイルにコマンドを出力する

\-t TRANSCRIPT

コマンドと結果をトランスクリプトファイルに出力する

### unalias[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

エイリアスのアンセット

Usage :

unalias \[\-a\] name \[name …\]

    * name \- 設定解除されるエイリアスの名前

Options:

\-a

すべてのエイリアス定義を削除

### set[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

設定可能なパラメータを設定したり、パラメータの現在の設定値を表示

Usage :

set \[\-h\] \[\-a\] \[\-l\] \[settable \[settable …\]\]

* 設定可能なパラメータのリストとその値について、引数なしで呼び出す

Options :

\-h

このヘルプメッセージを表示し、終了する

\-a

リードオンリー設定も表示

\-l

パラメータの機能を記述する

### edit[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

テキストエディタでファイルを編集する

Usage:

edit \[file\_path\]

    * file\_path \- エディタで開くファイルへのパス

### run\_script[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

ASCIIまたはUTF\-8テキストとしてエンコードされたスクリプトファイル内のコマンドを実行する。スクリプト内の各コマンドは、改行で区切られている必要がある

Usage:

run\_script \<file\_path\>

    * file\_path \- スクリプトを指すファイルパス

注：このコマンドは、cmd2バージョン0.9.13以下ではloadとして利用可能

### shell[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

OSのプロンプトと同じように、コマンドを実行する

Usage:

shell \<command\> \[arguments\]

### locate[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

ファイルシステム内の項目を検索する

Usage:

locate \[options\] \<name\>

Options :

\-c

見つかったアイテムの数を数える

\-i

ケースを無視する

### stat[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

ファイルの状態を表示する

Usage :

stat \[\-h\] \<file\_name\> \[file\_name …\]

Options :

\-h

ヘルプメッセージを表示する

### snap[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

スナップショットの作成と削除

Usage:

snap {create|delete} \<snap\_name\> \<dir\_name\>

    * snap\_name \- 作成または削除するスナップショット名
    * dir\_name \- スナップショットを作成あるいは削除するディレクトリ

### setxattr[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

ファイルへの拡張属性の設定

Usage :

setxattr \[\-h\] \<path\> \<name\> \<value\>

    * path \- ファイルへのパス
    * name \- 取得または設定する拡張属性名
    * value \- 設定する拡張属性値

Options:

\-h, \-\-help

ヘルプメッセージを表示する

### getxattr[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

パスに関連付けられた名前の拡張属性値を取得する

Usage :

getxattr \[\-h\] \<path\> \<name\>

    * path \- ファイルへのパス
    * name \- 取得または設定する拡張属性名

Options:

\-h, \-\-help

ヘルプメッセージを表示する

### listxattr[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

パスに関連する拡張属性名の一覧

Usage :

listxattr \[\-h\] \<path\>

    * path \- ファイルへのパス

Options:

\-h, \-\-help

ヘルプメッセージを表示する

### df[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

ディスクの空き容量を表示

Usage :

df \[\-h\] \[file \[file …\]\]

    * file \- ファイル名

Options:

\-h, \-\-help

ヘルプメッセージを表示する

### du[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

ディレクトリのディスク使用量を表示する

Usage :

du \[\-h\] \[\-r\] \[paths \[paths …\]\]

    * paths \- ディレクトリ名

Options:

\-h, \-\-help

ヘルプメッセージを表示する

\-r

すべてのディレクトリの再帰的なディスク使用量

### quota[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

ディレクトリのクォータ管理

Usage :

quota \[\-h\] \[–max\_bytes \[MAX\_BYTES\]\] \[–max\_files \[MAX\_FILES\]\] {get,set} path

    * {get,set} \- クォータオペレーションタイプ
    * path \- ディレクトリの名前

Options :

\-h, \-\-help

ヘルプメッセージを表示する

\-\-max\_bytes MAX\_BYTES

このディレクトリの下にあるデータの最大累積サイズを設定する

\-\-max\_files MAX\_FILES

このディレクトリツリーの下にあるファイルの総数を設定する

## CephFS Shell Configuration File[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

デフォルトでは、CephFS Shellは、環境変数CEPHFS\_SHELL\_CONFで指定されたパスから、ユーザのホームディレクトリ\(~/.cephfs\-shell.conf\)にあるcephfs\-shell.confを探します。

現在、CephFS Shellは依存関係のあるcmd2からすべてのオプションを継承しています。したがって、これらのオプションは、システムにインストールされているcmd2のバージョンによって異なる場合があります。これらのオプションの説明については、cmd2ドキュメントを参照してください。

以下は、cephfs\-shell.confのサンプルです：

```
[cephfs-shell]
prompt = CephFS:~/>>>
continuation_prompt = >

quiet = False
timing = False
colors = True
debug = False

abbrev = False
autorun_on_edit = False
echo = False
editor = vim
feedback_to_output = False
locals_in_py = True

```

## Exit Code[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell/ "Permalink to this headline")

cephfsシェルは以下の終了コードを返します。

|Error Type                                   |Exit Code|
|---------------------------------------------|---------|
|Miscellaneous                                |1        |
|Keyboard Interrupt                           |2        |
|Operation not permitted                      |3        |
|Permission denied                            |4        |
|No such file or directory                    |5        |
|I/O error                                    |6        |
|No space left on device                      |7        |
|File exists                                  |8        |
|No data available                            |9        |
|Invalid argument                             |10       |
|Operation not supported on transport endpoint|11       |
|Range error                                  |12       |
|Operation would block                        |13       |
|Directory not empty                          |14       |
|Not a directory                              |15       |
|Disk quota exceeded                          |16       |
|Broken pipe                                  |17       |
|Cannot send after transport endpoint shutdown|18       |
|Connection aborted                           |19       |
|Connection refused                           |20       |
|Connection reset                             |21       |
|Interrupted function call                    |22       |
