# 15: Deploying a new Ceph cluster

**クリップソース:** [15: Deploying a new Ceph cluster — Ceph Documentation](https://docs.ceph.com/en/pacific/cephadm/install/)

# Deploying a new Ceph cluster¶

Cephadmは、1台のホストで「ブートストラップ」して新しいCephクラスタを作成し、クラスタを拡張して追加のホストを包含し、必要なサービスを展開します。

## **Requirements[¶](https://docs.ceph.com/en/pacific/cephadm/install/#requirements "Permalink to this headline")**

* Python 3
* Systemd
* コンテナを実行するためのPodmanまたはDocker
* 時刻同期（chronyやNTPなど）
* LVM2によるストレージデバイスのプロビジョニング

最近のLinuxディストリビューションであれば十分です。 依存関係にあるものは、以下のブートストラッププロセスによって自動的にインストールされます。

Podmanと互換性のあるCephのバージョンの表は、「 [Compatibility With Podman Versions](https://docs.ceph.com/en/pacific/cephadm/compatibility/#cephadm-compatibility-with-podman) 」を参照してください。PodmanのすべてのバージョンがCephと互換性があるわけではありません。

## **Install cephadm[¶](https://docs.ceph.com/en/pacific/cephadm/install/#install-cephadm "Permalink to this headline")**

cephadmコマンドは

1. 新しいクラスタを起動する
2. Ceph CLIが動作するコンテナ型シェルの起動
3. コンテナ化されたCephのデーモンのデバッグに役立つ。

cephadmのインストールには2つの方法があります：

1. [curlベースのインストール](https://docs.ceph.com/en/pacific/cephadm/install/#cephadm-install-curl)
2. [ディストリビューション固有のインストール方法](https://docs.ceph.com/en/pacific/cephadm/install/#cephadm-install-distros)

### curl\-based installation[¶](https://docs.ceph.com/en/pacific/cephadm/install/#curl-based-installation "Permalink to this headline")

* curlを使用して、スタンドアロン・スクリプトの最新バージョンを取得します。

```
# curl --silent --remote-name --location https://github.com/ceph/ceph/raw/octopus/src/cephadm/cephadm
```

cephadmスクリプトを実行可能にします。

```
# chmod +x cephadm
```

このスクリプトは、カレントディレクトリから直接実行できます。

```
# ./cephadm <arguments...>
```

* クラスタを起動するにはスタンドアロンのスクリプトで十分ですが、ホストにcephadmコマンドがインストールされていると便利です。cephadmコマンドを提供するパッケージをインストールするには、次のコマンドを実行します。

```
# ./cephadm add-repo --release octopus
# ./cephadm install
```

cephadmがPATHに入っていることを確認します。

```
# which cephadm
```

コマンドが成功すると、次のような結果が得られます。

```
/usr/sbin/cephadm
```

### distribution\-specific installations[¶](https://docs.ceph.com/en/pacific/cephadm/install/#distribution-specific-installations "Permalink to this headline")

Important

このセクションで説明するcephadmのインストール方法は、上記のcurlベースの方法とは異なります。上記のcurlベースの方法またはこのセクションのいずれかの方法を使用し、curlベースの方法とこれらのいずれかの方法の両方は使用しないでください。

一部のLinuxディストリビューションには、最新のCephパッケージがすでに含まれている場合があります。 その場合は、cephadmを直接インストールできます。たとえば、以下のようになります。

_In Ubuntu:_

```
# apt install -y cephadm
```

_In Fedora:_

```
# dnf -y install cephadm
```

_In SUSE:_

```
# zypper install -y cephadm
```

## **Bootstrap a new cluster[¶](https://docs.ceph.com/en/pacific/cephadm/install/#bootstrap-a-new-cluster "Permalink to this headline")**

### What to know before you bootstrap[¶](https://docs.ceph.com/en/pacific/cephadm/install/#what-to-know-before-you-bootstrap "Permalink to this headline")

 新しいCephクラスタを作成する最初のステップは、Cephクラスタの最初のホストでcephadm bootstrapコマンドを実行することです。Cephクラスタの最初のホストでcephadm bootstrapコマンドを実行すると、Cephクラスタの最初の「モニタデーモン」が作成され、そのモニタデーモンにはIPアドレスが必要です。Cephクラスタの最初のホストのIPアドレスをceph bootstrapコマンドに渡す必要があるため、そのホストのIPアドレスを知っておく必要があります。

Note

複数のネットワークやインタフェースがある場合は、Cephクラスタにアクセスするすべてのホストからアクセスできるものを必ず選択してください。

### Running the bootstrap command[¶](https://docs.ceph.com/en/pacific/cephadm/install/#running-the-bootstrap-command "Permalink to this headline")

ceph bootstrapコマンドを実行します。

```
# cephadm bootstrap --mon-ip *<mon-ip>*
```

このコマンドは：

* ローカルホスト上に新しいクラスタのモニタとマネージャデーモンを作成します。
* Cephクラスタ用の新しいSSHキーを生成し、ルートユーザの/root/.ssh/authorized\_keysファイルに追加します。
* 公開鍵のコピーを/etc/ceph/ceph.pubに書き込みます。
* 最小限の設定ファイルを/etc/ceph/ceph.confに書き込みます。このファイルは新しいクラスターとの通信に必要です。
* client.adminの管理用（特権的な！）秘密鍵のコピーを/etc/ceph/ceph.client.admin.keyringに書き込みます。
* ブートストラップホストに \_admin ラベルを追加します。デフォルトでは、このラベルをつけたホストは、/etc/ceph/ceph.confと/etc/ceph.client.admin.keyringのコピーを\(も\)取得します。

### Further information about cephadm bootstrap[¶](https://docs.ceph.com/en/pacific/cephadm/install/#further-information-about-cephadm-bootstrap "Permalink to this headline")

デフォルトのブートストラップの動作は、ほとんどのユーザーにとって機能します。しかし、cephadmのブートストラップについてすぐに詳しく知りたい場合は、以下のリストをお読みください。

Also, you can run cephadm bootstrap \-h to see all of cephadm’s available options.  

また、cephadm bootstrap \-hを実行すると、cephadmの利用可能なオプションがすべて表示されます。

* デフォルトでは、Cephのデーモンはログ出力をstdout/stderrに送信し、これがコンテナランタイム\(dockerまたはpodman\)によってピックアップされ、\(ほとんどのシステムで\)journaldに送信されます。 Cephに従来のログファイルを/var/log/ceph/$fsidに書き込ませたい場合は、ブートストラップ時に\-\-log\-to\-fileオプションを使用します。
* 大規模なCephクラスターでは、\(Cephクラスターの外部の\)パブリックネットワークトラフィックと\(Cephクラスターの内部の\)クラスタートラフィックを分離するとパフォーマンスが向上します。内部のクラスタトラフィックは、レプリケーション、リカバリ、およびOSDデーモン間のハートビートを処理します。 bootstrapサブコマンドに\-\-cluster\-networkオプションを指定することで、[クラスタネットワーク](https://docs.ceph.com/en/pacific/rados/configuration/network-config-ref/#cluster-network)を定義できます。このパラメータでは、CIDR表記のサブネットを定義する必要があります（例：10.90.90.0/24またはfe80::/64）。
* 新しいクラスタへのアクセスに必要なファイルを/etc/cephに書き込みます。この集中的な場所により、ホストにインストールされたCephパッケージ\(cephadmコマンドラインインタフェースへのアクセスを提供するパッケージなど\)がこれらのファイルを見つけることができます。
* ただし、cephadmでデプロイされたデーモンコンテナには、/etc/cephはまったく必要ありません。\-\-output\-dir \*\<ディレクトリ\>\*オプションを使用して、別のディレクトリ\(たとえば、.ディレクトリ\)に配置します。これにより、同じホスト上の既存のCeph設定\(cephadmまたはその他\)との衝突を回避できる場合があります。
* Cephの初期設定オプションは、標準的なiniスタイルの設定ファイルに記述し、\-\-config \*\<config\-file\>\*オプションを使用することで、新しいクラスタに渡すことができます。 たとえば、以下のようになります。

```
$ cat <<EOF > initial-ceph.conf
[global]
osd crush chooseleaf type = 0
EOF
$ ./cephadm bootstrap --config initial-ceph.conf ...
```

* \-\-ssh\-user \*\<user\>\*オプションを使用すると、cephadmがホストへの接続に使用するsshユーザを選択できます。関連するssh鍵は/home/\*\<user\>\*/.ssh/authorized\_keysに追加されます。このオプションで指定したユーザーは、パスワードなしのsudoアクセス権を持っている必要があります。
* \-\-registry\-url \<url of registry\>
    \-\-registry\-username \<username of account on registry\>
    \-\-registry\-password \<password of account on registry\>
    OR
    \-\-registry\-json \<json file with login info\>
    Cephadmは、コンテナを引き出すためにこのレジストリへのログインを試み、ログイン情報を設定データベースに保存します。クラスタに追加された他のホストも、認証されたレジストリを利用できるようになります。
    ログインを必要とする認証済みのレジストリでコンテナを使用する場合は、3つの引数を追加することができます。

## **Enable Ceph CLI[¶](https://docs.ceph.com/en/pacific/cephadm/install/#enable-ceph-cli "Permalink to this headline")**

Cephadmでは、ホストにCephのパッケージをインストールする必要はありません。 ただし、cephコマンドに簡単にアクセスできるようにすることをお勧めします。 これを行うにはいくつかの方法があります。

* cephadm shellコマンドは、Cephのすべてのパッケージがインストールされたコンテナ内でbashシェルを起動します。デフォルトでは、ホスト上の/etc/cephに設定ファイルとキーリングファイルが見つかると、それらがコンテナ環境に渡され、シェルが完全に機能するようになります。MONホストで実行した場合、cephadm shellは、デフォルトの設定を使用する代わりに、MONコンテナから設定を推論することに注意してください。\-\-mount \<path\>が指定された場合、ホストの\<path\>\(ファイルまたはディレクトリ\)がコンテナ内の/mntの下に表示されます。

```
# cephadm shell
```

* cephのコマンドを実行するには、次のようなコマンドを実行することもできます。

```
# cephadm shell -- ceph -s
```

* ceph、rbd、mount.ceph\(CephFSファイルシステムのマウント用\)など、すべてのcephコマンドを含むceph\-commonパッケージをインストールすることができます。

```
# cephadm add-repo --release octopus
# cephadm install ceph-common
```

cephコマンドがアクセス可能であることを確認します：

```
# ceph -v
```

cephコマンドがクラスタに接続できることを確認します：

```
# ceph status
```

## **Adding Hosts[¶](https://docs.ceph.com/en/pacific/cephadm/install/#adding-hosts "Permalink to this headline")**

次に、「[Adding Hosts](https://docs.ceph.com/en/pacific/cephadm/host-management/#cephadm-adding-hosts)」に従って、すべてのホストをクラスタに追加します。[23: Host Management](23-Host_Management.md)

デフォルトでは、ceph.confファイルとclient.adminキーリングのコピーが、\_adminラベルを持つすべてのホストの/etc/cephに維持されますが、最初はブートストラップホストにのみ適用されます。通常は、Ceph CLI\(cephadmシェル経由など\)が複数のホストで簡単にアクセスできるように、1つまたは複数の他のホストに\_adminラベルを付与することをお勧めします。追加のホストに\_adminラベルを追加するには、以下の手順に従います。

```
# ceph orch host label add *<host>* _admin
```

## **Adding additional MONs[¶](https://docs.ceph.com/en/pacific/cephadm/install/#adding-additional-mons "Permalink to this headline")**

一般的なCephクラスタでは、3～5台のモニタデーモンが異なるホストに分散しています。 クラスタに5つ以上のノードがある場合は、5つのモニタを展開することをお勧めします。

追加のMONをデプロイするには、「[Deploying additional monitors](https://docs.ceph.com/en/pacific/cephadm/services/mon/#deploy-additional-monitors) 」を参照してください。[25: MON Service](25-MON_Service.md)

## **Adding Storage[¶](https://docs.ceph.com/en/pacific/cephadm/install/#adding-storage "Permalink to this headline")**

クラスタにストレージを追加するには、利用可能で未使用のデバイスを消費するようにCephに指示するかのどちらかです。

```
# ceph orch apply osd --all-available-devices
```

または、より詳しい説明は「[Deploy OSDs](https://docs.ceph.com/en/pacific/cephadm/services/osd/#cephadm-deploy-osds)」をご覧ください。[27: OSD Service](27-OSD_Service.md)

## **Using Ceph[¶](https://docs.ceph.com/en/pacific/cephadm/install/#using-ceph "Permalink to this headline")**

Ceph Filesystemを使用するには、「[Deploy CephFS](https://docs.ceph.com/en/pacific/cephadm/services/mds/#orchestrator-cli-cephfs)」に従います。[29: MDS Service](29-MDS_Service.md)

Ceph Object Gatewayを使用するには、「[Deploy RGWs](https://docs.ceph.com/en/pacific/cephadm/services/rgw/#cephadm-deploy-rgw)」に従います。[28: RGW Service](28-RGW_Service.md)

NFSを使用するには、「[NFS Service](https://docs.ceph.com/en/pacific/cephadm/services/nfs/#deploy-cephadm-nfs-ganesha)」に従ってください。[30: NFS Service](30-NFS_Service.md)

iSCSIを使用するには、「[Deploying iSCSI](https://docs.ceph.com/en/pacific/cephadm/services/iscsi/#cephadm-iscsi) 」に従ってください。[31: iSCSI Service](31-iSCSI_Service.md)
