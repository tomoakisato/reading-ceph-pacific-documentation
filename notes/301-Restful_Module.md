# 301: Restful Module

**クリップソース:** [301: Restful Module — Ceph Documentation](https://docs.ceph.com/en/pacific/mgr/restful/)

# Restful Module[¶](https://docs.ceph.com/en/pacific/mgr/restful/ "Permalink to this headline")

RESTfulモジュールは、SSLで保護された接続を介してクラスタの状態へのREST APIアクセスを提供します。Enabling[¶](https://docs.ceph.com/en/pacific/mgr/restful/ "Permalink to this headline")

restful モジュールを有効化：

```
ceph mgr module enable restful
```

APIエンドポイントを利用する前に、以下のSSL証明書を設定する必要があります。 デフォルトでは、モジュールは、ホスト上のすべてのIPv4およびIPv6アドレス上のポート8003でHTTPS要求を受け入れます。

## Securing[¶](https://docs.ceph.com/en/pacific/mgr/restful/ "Permalink to this headline")

restfulへの接続はすべてSSLで保護されます。 コマンドで自己署名証明書を生成することができます：

```
ceph restful create-self-signed-cert
```

自己署名証明書の場合、ほとんどのクライアントは、接続を許可するか警告メッセージを抑制するためのフラグが必要であることに注意してください。 たとえば、ceph\-mgrデーモンが同じホスト上にある場合、以下のようになります：

```
curl -k https://localhost:8003/
```

デプロイを適切に保護するために、組織の認証局によって署名された証明書を使用する必要があります。 たとえば、次のようなコマンドでキーペアを生成することができます：

```
openssl req -new -nodes -x509 
  -subj "/O=IT/CN=ceph-mgr-restful" 
  -days 3650 -keyout restful.key -out restful.crt -extensions v3_ca
```

そして、この restful.crt は、あなたの組織の CA（証明機関）によって署名されている必要があります。 それが完了したら、それを使って設定することができます：

```
ceph config-key set mgr/restful/$name/crt -i restful.crt
ceph config-key set mgr/restful/$name/key -i restful.key
```

ここで、$nameはceph\-mgrインスタンスの名前\(通常はホスト名\)です。すべてのマネージャインスタンスが同じ証明書を共有する場合、$nameの部分を省略できます：

```
ceph config-key set mgr/restful/crt -i restful.crt
ceph config-key set mgr/restful/key -i restful.key
```

## Configuring IP and port[¶](https://docs.ceph.com/en/pacific/mgr/restful/ "Permalink to this headline")

他のRESTful APIエンドポイントと同様に、restfulはIPとポートにバインドされます。 デフォルトでは、現在アクティブなceph\-mgrデーモンは、ポート8003と、ホスト上の利用可能なIPv4またはIPv6アドレスにバインドされます。

ceph\-mgrはそれぞれrestfulのインスタンスをホストしているため、別々に設定する必要がある場合もあります。IPとポートは、設定キー機能で変更できます：

```
ceph config set mgr mgr/restful/$name/server_addr $IP
ceph config set mgr mgr/restful/$name/server_port $PORT
```

ここで、$nameはceph\-mgrデーモンのID\(通常はホスト名\)です。

これらの設定は、マネージャに依存せず、クラスタ全体で構成することも可能です。 例えば、以下のようになります：

```
ceph config set mgr mgr/restful/server_addr $IP
ceph config set mgr mgr/restful/server_port $PORT
```

ポートが設定されていない場合、restfulはポート8003にバインドされます。アドレスが設定されていない場合、restfulは利用可能なすべてのIPv4およびIPv6アドレスに対応する::にバインドされます。

## Creating an API User[¶](https://docs.ceph.com/en/pacific/mgr/restful/ "Permalink to this headline")

APIユーザーを作成するには、以下のコマンドを実行します：

```
ceph restful create-key <username>
```

\<username\>は、希望するユーザー名に置き換えてください。例えば、apiという名前のユーザーを作成する場合：

```
$ ceph restful create-key api
52dffd92-a103-4a10-bfce-5b60f48f764e
```

_ceph restfu lcreate\-key api_から生成されたUUIDは、ユーザーの鍵として機能します。

APIキーを一覧表示するには、以下のコマンドを実行してください：

```
ceph restful list-keys
```

ceph restful list\-keysコマンドは、JSONで出力されます：

```
{
      "api": "52dffd92-a103-4a10-bfce-5b60f48f764e"
}
```

APIを使用したユーザーテストを行うために、curlを使用することができます。以下はその例です：

```
curl -k https://api:52dffd92-a103-4a10-bfce-5b60f48f764e@<ceph-mgr>:<port>/server
```

上記の場合、サーバーのエンドポイントから情報を取得するためにGETを使用しています。

## Load balancer[¶](https://docs.ceph.com/en/pacific/mgr/restful/ "Permalink to this headline")

restfulは、その時点でアクティブなマネージャーでのみ起動することに注意してください。Cephクラスタの状態を照会して、どのマネージャがアクティブであるかを確認します\(例: ceph mgr dump\)。 どのマネージャデーモンが現在アクティブであるかにかかわらず、一貫したURLでAPIを利用できるようにするために、ロードバランサフロントエンドを設定して、利用可能なマネージャエンドポイントにトラフィックを誘導することができます。

## Available methods[¶](https://docs.ceph.com/en/pacific/mgr/restful/ "Permalink to this headline")

/ docエンドポイントに移動して、使用可能なエンドポイントと各エンドポイントに実装されているHTTPメソッドの完全なリストを確認できます。

例えば、/osd/\<id\>エンドポイントのPATCHメソッドを使用して、OSD id 1のステートupを設定したい場合、以下のcurlコマンドを使用することができます：

```
echo -En '{"up": true}' | curl --request PATCH --data @- --silent --insecure --user <user> 'https://<ceph-mgr>:<port>/osd/1'
```

または、pythonを使用して行うことができます：

```
$ python
>> import requests
>> result = requests.patch(
       'https://<ceph-mgr>:<port>/osd/1',
       json={"up": True},
       auth=("<user>", "<password>")
   )
>> print result.json()

```

restfulモジュールに実装されている他のエンドポイントには、以下のようなものがあります。

* /config/cluster: 
* /config/osd: 
* /crush/rule: 
* /mon: 
* /osd: 
* /pool: 
* /pool/\<arg\>: 
* /request: 
* /request/\<arg\>: 
* /server: 

## The /request endpoint[¶](https://docs.ceph.com/en/pacific/mgr/restful/ "Permalink to this headline")

リクエストエンドポイントを使用すると、DELETE、POST、PATCHのいずれかのメソッドでスケジュールしたリクエストの状態をポーリングすることができます。これらのメソッドは、実行が終了するまでに時間がかかる可能性があるため、デフォルトでは非同期です。リクエストURLに ?wait=1 を追加することで、この挙動を変更することができます。そうすると、返されたリクエストは常に完了します。

src/mon/MonCommands.hで定義されているceph monコマンドのパススルーを提供する/requestメソッドのPOSTメソッドについて考えてみましょう：

```
COMMAND("osd ls " 
        "name=epoch,type=CephInt,range=0,req=false", 
        "show all OSD ids", "osd", "r", "cli,rest")
```

プレフィックスは osd ls です。オプション引数のnameはepochで、typeはCephInt、つまり整数です。コマンドをスケジュールするには、次のようなPOSTリクエストが必要です：

```
$ python
>> import requests
>> result = requests.post(
       'https://<ceph-mgr>:<port>/request',
       json={'prefix': 'osd ls', 'epoch': 0},
       auth=("<user>", "<password>")
   )
>> print result.json()
```
