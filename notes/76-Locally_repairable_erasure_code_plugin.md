# 76: Locally repairable erasure code plugin

**クリップソース:** [76: Locally repairable erasure code plugin — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-lrc/)

# Locally repairable erasure code plugin[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-lrc/ "Permalink to this headline")

jerasureプラグインでは、ECオブジェクトが複数のOSDに格納されている場合、1つのOSDの損失から回復するためには、他のk個のOSDから読み出す必要があります。例えば、k=8、m=4で構成される場合、1つのOSDの損失から回復するためには、他の8つのOSDから読み出す必要があります。

lrc データ消去コードプラグインは、ローカルパリティチャンクを作成し、生存している OSD の数を少なくしてリカバリを可能にします。例えば、lrc が k=8、m=4、l=4 で構成されている場合、4 つの OSD ごとに追加のパリティ チャンクが作成されます。一つのOSDが失われたとき、8つのOSDの代わりに4つのOSDで回復することができます。

## Erasure code profile examples[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-lrc/ "Permalink to this headline")

### Reduce recovery bandwidth between hosts[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-lrc/ "Permalink to this headline")

すべてのホストが同じスイッチに接続されている場合、おそらく興味深いユースケースではないでしょうが、帯域幅の使用量削減は実際に観察することができます。

```
$ ceph osd erasure-code-profile set LRCprofile 
     plugin=lrc 
     k=4 m=2 l=3 
     crush-failure-domain=host
$ ceph osd pool create lrcpool erasure LRCprofile
```

### Reduce recovery bandwidth between racks[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-lrc/ "Permalink to this headline")

Fireflyでは、プライマリOSDがロストチャンクと同じラックにある場合にのみ、帯域幅の減少が観察されます：

```
$ ceph osd erasure-code-profile set LRCprofile 
     plugin=lrc 
     k=4 m=2 l=3 
     crush-locality=rack 
     crush-failure-domain=host
$ ceph osd pool create lrcpool erasure LRCprofile
```

## Create an lrc profile[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-lrc/ "Permalink to this headline")

新しい lrc ECプロファイルを作成するには：

```
ceph osd erasure-code-profile set {name} 
     plugin=lrc 
     k={data-chunks} 
     m={coding-chunks} 
     l={locality} 
     [crush-root={root}] 
     [crush-locality={bucket-type}] 
     [crush-failure-domain={bucket-type}] 
     [crush-device-class={device-class}] 
     [directory={directory}] 
     [--force]
```

ここで：

**k={datachunks}**

Description

各オブジェクトはデータチャンク部分に分割され、それぞれが異なるOSDに格納される

Type

Integer

Required

Yes.

Example

4

**m={coding\-chunks}**

Description

各オブジェクトのコーディングチャンクを計算し、異なるOSDに保存する。コーディングチャンクの数は、データを失うことなくダウンすることができるOSDの数でもある。

Type

Integer

Required

Yes.

Example

2

**l={locality}**

Description

コーディングチャンクとデータチャンクを大きさの局所性を持つセットにグループ化する。例えば、k=4、m=2 の場合、locality=3 であれば、3 個のグループが 2 つ作成される。各セットは、他のセットからチャンクを読み取ることなく回復することができる。

Type

Integer

Required

Yes.

Example

3

**crush\-root={root}**

Description

CRUSHルールの最初のステップに使用されるCRUSHバケットの名前

Type

String

Required

No.

Default

default

**crush\-locality={bucket\-type}**

Description

l で定義されたチャンクの各セットが格納される CRUSH バケットのタイプ。例えば、rackに設定すると、l個のチャンクの各グループは異なるラックに配置される。step choose rack のような CRUSH 規則のステップを作成する際に使用される。設定されていない場合、そのようなグループ化は行われない。

Type

String

Required

No.

**crush\-failure\-domain={bucket\-type}**

Description

同じ障害ドメインを持つバケットに、2つのチャンクが存在しないことを確認する。例えば、障害ドメインがhostの場合、2つのチャンクが同じhostに保存されることはない。step chooseleaf hostのようなCRUSHルールのステップを作成するために使用される

Type

String

Required

No.

Default

host

**crush\-device\-class={device\-class}**

Description

CRUSHマップのcrushデバイスクラス名を使って、特定のクラスのデバイス（例：ssdやhdd）に配置を制限する

Type

String

Required

No.

Default

**directory={directory}**

Description

ECプラグインの読み込み元となるディレクトリ名

Type

String

Required

No.

Default

/usr/lib/ceph/erasure\-code

**\-\-force**

Description

同名の既存プロファイルを上書きする

Type

String

Required

No.

## Low level plugin configuration[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-lrc/ "Permalink to this headline")

kとmの和はlパラメータの倍数でなければなりません。しかし、低レベルの設定パラメータはこの制限を強制しないので、特定の目的のために使用するのが有利な場合があります。例えば、4チャンクのグループと3チャンクのグループの2つを定義することが可能です。また、データセンターとデータセンター内のラックなど、再帰的にローカリティセットを定義することも可能です。k/m/lは、低レベルの構成を生成することによって実装されます。

lrc erasure code プラグインは、ECの技術を再帰的に適用し、一部のチャンクの損失から回復するために、ほとんどの場合、利用可能なチャンクのサブセットを必要とするだけです。

例えば、3つのコーディングステップを記述する場合、以下のようになります。

```
chunk nr    01234567
step 1      _cDD_cDD
step 2      cDDD____
step 3      ____cDDD
```

ここで、cはデータチャンクDから計算されたコーディングチャンクであり、チャンク7の損失は最後の4つのチャンクで回復することができます。また、チャンク2の損失は、最初の4つのチャンクで回復することができます。

## Erasure code profile examples using low level configuration[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-lrc/ "Permalink to this headline")

### Minimal testing[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-lrc/ "Permalink to this headline")

厳密には、K=2 M=1 のECプロファイルを使用することと同等です。DDはK=2、cはM=1、jerasureプラグインはデフォルトで使用されることを意味します。

```
$ ceph osd erasure-code-profile set LRCprofile 
     plugin=lrc 
     mapping=DD_ 
     layers='[ [ "DDc", "" ] ]'
$ ceph osd pool create lrcpool erasure LRCprofile
```

### Reduce recovery bandwidth between hosts[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-lrc/ "Permalink to this headline")

すべてのホストが同じスイッチに接続されている場合、おそらく興味深いユースケースではないでしょうが、帯域幅の使用量の減少は実際に観察することができます。チャンクのレイアウトは異なりますが、k=4, m=2, l=3 と同等です。

```
$ ceph osd erasure-code-profile set LRCprofile 
     plugin=lrc 
     mapping=__DD__DD 
     layers='[
               [ "_cDD_cDD", "" ],
               [ "cDDD____", "" ],
               [ "____cDDD", "" ],
             ]'
$ ceph osd pool create lrcpool erasure LRCprofile
```

### Reduce recovery bandwidth between racks[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-lrc/ "Permalink to this headline")

Fireflyでは、プライマリOSDが失われたチャンクと同じラックにある場合にのみ、帯域幅の減少が観察されます。

```
$ ceph osd erasure-code-profile set LRCprofile 
     plugin=lrc 
     mapping=__DD__DD 
     layers='[
               [ "_cDD_cDD", "" ],
               [ "cDDD____", "" ],
               [ "____cDDD", "" ],
             ]' 
     crush-steps='[
                     [ "choose", "rack", 2 ],
                     [ "chooseleaf", "host", 4 ],
                    ]'
$ ceph osd pool create lrcpool erasure LRCprofile
```

### Testing with different Erasure Code backends[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-lrc/ "Permalink to this headline")

LRCは、デフォルトのECバックエンドとしてjerasureを使用するようになりました。低レベルの設定を使用して、レイヤーごとにECバックエンド/アルゴリズムを指定することが可能です。layers='\[ \[ "DDc", "" \]の第2引数は、実際にこのレベルで使用されるECプロファイルです。以下の例では、lrcpoolで使用されるcauchy手法を持つISAバックエンドを指定しています。

```
$ ceph osd erasure-code-profile set LRCprofile 
     plugin=lrc 
     mapping=DD_ 
     layers='[ [ "DDc", "plugin=isa technique=cauchy" ] ]'
$ ceph osd pool create lrcpool erasure LRCprofile

```

また、各層ごとに異なるECプロファイルを使用することも可能です。

```
$ ceph osd erasure-code-profile set LRCprofile 
     plugin=lrc 
     mapping=__DD__DD 
     layers='[
               [ "_cDD_cDD", "plugin=isa technique=cauchy" ],
               [ "cDDD____", "plugin=isa" ],
               [ "____cDDD", "plugin=jerasure" ],
             ]'
$ ceph osd pool create lrcpool erasure LRCprofile
```

## Erasure coding and decoding algorithm[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-lrc/ "Permalink to this headline")

layerの記述に見られるステップは、

```
chunk nr    01234567

step 1      _cDD_cDD
step 2      cDDD____
step 3      ____cDDD
```

順番に適用されます。例えば、4Kのオブジェクトを符号化する場合、まずステップ1を経て、1Kのチャンク（大文字のDが4つ）に分割されます。それを順にチャンク2、3、6、7に格納します。これらから、2つのコーディングチャンクを算出します（2つの小文字のc）。このコーディングチャンクはそれぞれチャンク1とチャンク5に格納されます。

ステップ2は、ステップ1で作成されたコンテンツを同様に再利用し、位置0に単一のコーディングチャンクcを格納します。読みやすくするためにアンダースコア（\_）でマークした最後の4つのチャンクは無視されます。

ステップ 3 では、位置 4 に 1 つの符号化チャンク c を格納します。ステップ1で作成された3つのチャンクは、このコーディングチャンクの計算に使用されます。つまり、ステップ1からのコーディングチャンクは、ステップ3でデータチャンクになります。

チャンク2が失われた場合、

```
chunk nr    01234567

step 1      _c D_cDD
step 2      cD D____
step 3      __ _cDDD
```

デコーディングでは、ステップ3の次にステップ2、最後にステップ1というように、ステップを逆順に歩くことで復元を試みます。

ステップ3はチャンク2について何も知らない（アンダースコアである）ので、スキップされます。

チャンク 0 に格納されたステップ 2 の符号化チャンクによって、チャンク 2 の内容を復元することができます。回復すべきチャンクはもうないので、ステップ1を考慮することなく処理を停止します。

チャンク2を回復するには、チャンク0、1、3を読み出し、チャンク2を書き戻す必要があります。

チャンク2、3、6が失われた場合、

```
chunk nr    01234567

step 1      _c  _c D
step 2      cD  __ _
step 3      __  cD D
```

ステップ3では、チャンク6の内容を復元することができます。

```
chunk nr    01234567

step 1      _c  _cDD
step 2      cD  ____
step 3      __  cDDD
```

ステップ2は、2つのチャンク（2、3）が欠落しており、欠落した1つのチャンクからしか回復できないため、回復に失敗し、スキップされます。

チャンク1、5に格納されたステップ1からの符号化チャンクによって、チャンク2、3の内容を復元することができます。

```
chunk nr    01234567

step 1      _cDD_cDD
step 2      cDDD____
step 3      ____cDDD
```

## Controlling CRUSH placement[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-lrc/ "Permalink to this headline")

デフォルトのCRUSHルールは、異なるホスト上にあるOSDを提供します。例えば

```
chunk nr    01234567

step 1      _cDD_cDD
step 2      cDDD____
step 3      ____cDDD
```

は、各チャンクに1つずつ、ちょうど8つのOSDを必要とします。ホストが隣接する2つのラックにある場合、最初の4つのチャンクは最初のラックに、最後の4つは2番目のラックに置くことができます。そのため、1つのOSDの損失から回復するために、2つのラック間の帯域幅を使用する必要はありません。

例えば、こんな感じです。

```
crush-steps='[ [ "choose", "rack", 2 ], [ "chooseleaf", "host", 4 ] ]'
```

ラックタイプの2つのCRUSHバケットを選択し、それぞれについて、ホストタイプの異なるバケットに位置する4つのOSDを選択するルールを作成します。

また、CRUSHルールは、より細かい制御を行うために手動で作成することも可能です。
