# 61: Messaging

**クリップソース:** [61: Messaging — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/configuration/ms-ref/)

# Messaging[¶](https://docs.ceph.com/en/pacific/rados/configuration/ms-ref/ "Permalink to this headline")

## General Settings[¶](https://docs.ceph.com/en/pacific/rados/configuration/ms-ref/ "Permalink to this headline")

**ms\_tcp\_nodelay**

Description

メッセンジャのTCPセッションでNagleのアルゴリズムを無効にする

Type

Boolean

Required

No

Default

true

**ms\_initial\_backoff**

Description

障害発生時に再接続するまでの初期待ち時間

Type

Double

Required

No

Default

.2

**ms\_max\_backoff**

Description

障害発生時に再接続するまでの最大待ち時間

Type

Double

Required

No

Default

15.0

**ms\_nocrc**

Description

ネットワークメッセージのCRCを無効にする。 CPUが制限されている場合、パフォーマンスを向上させることができる

Type

Boolean

Required

No

Default

false

**ms\_die\_on\_bad\_msg**

Description

デバッグオプション、設定しない

Type

Boolean

Required

No

Default

false

**ms\_dispatch\_throttle\_bytes**

Description

ディスパッチ待ちのメッセージの総サイズ

Type

64\-bit Unsigned Integer

Required

No

Default

100\<\<20

**ms\_bind\_ipv6**

Description

デーモンをIPv4ではなく、IPv6アドレスにバインドする。デーモンまたはクラスタIPを指定する場合は不要

Type

Boolean

Required

No

Default

false

**ms\_rwthread\_stack\_bytes**

Description

スタックサイズのデバッグオプション、設定しない

Type

64\-bit Unsigned Integer

Required

No

Default

1024\<\<10

**ms\_tcp\_read\_timeout**

Description

メッセンジャがアイドル状態の接続を閉じるまでの時間（秒）

Type

64\-bit Unsigned Integer

Required

No

Default

900

**ms\_inject\_socket\_failures**

Description

デバッグオプション、設定しない

Type

64\-bit Unsigned Integer

Required

No

Default

0

## Async messenger options[¶](https://docs.ceph.com/en/pacific/rados/configuration/ms-ref/ "Permalink to this headline")

**ms\_async\_transport\_type**

Description

Async Messengerが使用するトランスポートタイプ。posix, dpdk, rdma のいずれかを指定する。Posix は標準的な TCP/IP ネットワークを使用し、これがデフォルト。他のトランスポートは実験的なもので、サポートが制限されている場合がある

Type

String

Required

No

Default

posix

**ms\_async\_op\_threads**

Description

各非同期メッセンジャーインスタンスで使用されるワーカスレッドの初期数。少なくともレプリカの最大数と同じにする必要があるが、CPU コア数が少ない場合や 1 台のサーバで多くの OSD をホストする場合は減らすことができる

Type

64\-bit Unsigned Integer

Required

No

Default

3

**ms\_async\_max\_op\_threads**

Description

各非同期メッセンジャーインスタンスで使用するワーカスレッドの最大数。マシンの CPU 数に制限がある場合は低い値に設定し、CPU が十分に活用されていない場合 \(つまり、1 つ以上の CPU が I/O 操作中に常に 100% 負荷になっている場合\) には高く設定する。

Type

64\-bit Unsigned Integer

Required

No

Default

5

**ms\_async\_send\_inline**

Description

非同期メッセンジャーのスレッドからメッセージをキューイングして送信するのではなく、メッセージを生成したスレッドから直接送信する。このオプションは、多くの CPU コアを持つシステムでパフォーマンスが低下することが知られているため、デフォルトで無効になっている。

Type

Boolean

Required

No

Default

false
