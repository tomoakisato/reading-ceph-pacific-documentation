# 131: CephFS Administrative commands

**クリップソース:** [131: CephFS Administrative commands — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/administration/)

# CephFS Administrative commands[¶](https://docs.ceph.com/en/pacific/cephfs/administration/ "Permalink to this headline")

## File Systems[¶](https://docs.ceph.com/en/pacific/cephfs/administration/ "Permalink to this headline")

注：ファイルシステム、メタデータプール、およびデータプールの名前には、\[a\-zA\-Z0\-9\_\-\]の文字しか使用できません。

これらのコマンドは、Cephクラスタ内のCephFSファイルシステムに対して操作します。デフォルトでは1つのファイルシステムしか許可されないことに注意してください。複数のファイルシステムを作成できるようにするには、ceph fs flag set enable\_multiple trueを使用します。

```
fs new <file system name> <metadata pool name> <data pool name>
```

このコマンドは、新しいファイルシステムを作成します。ファイル・システム名とメタデータ・プール名は自明です。指定したデータプールはデフォルトのデータプールで、一度設定すると変更することはできません。各ファイルシステムにはランクに割り当てられたMDSデーモンのセットがあるため、新しいファイルシステムに対応するために十分なスタンバイデーモンを用意してください。

```
fs ls
```

すべてのファイルシステムを名前でリストアップします。

```
fs dump [epoch]
```

これにより、指定されたエポック（デフォルト：current）でFSMapがダンプされます。これには、すべてのファイルシステム設定、MDSデーモンとそれらが保持するランク、スタンバイMDSデーモンのリストが含まれます。

```
fs rm <file system name> [--yes-i-really-mean-it]
```

CephFSファイルシステムを破棄します。これにより、FSMapからファイルシステムの状態に関する情報が消去されます。メタデータプールとデータプールは変更されないため、個別に破棄する必要があります。

```
fs get <file system name>
```

設定やランクなど、指定されたファイルシステムに関する情報を取得します。これは、fsdump コマンドによる同じ情報のサブセットです。

```
fs set <file system name> <var> <val>
```

ファイルシステム上の設定を変更します。これらの設定は、指定されたファイルシステムに固有のものであり、他のファイルシステムには影響しません。

```
fs add_data_pool <file system name> <pool name/id>
```

ファイルシステムにデータプールを追加します。このプールは、ファイルデータを格納する代替場所として、ファイルレイアウトに使用することができます。

```
fs rm_data_pool <file system name> <pool name/id>
```

このコマンドは、ファイルシステムのデータプールの一覧から指定したプールを削除します。 削除されたデータプールのレイアウトを持つファイルがある場合、そのファイルデータは使用できなくなります。defaultデータプール（ファイルシステム作成時）を削除することはできません。

## Settings[¶](https://docs.ceph.com/en/pacific/cephfs/administration/ "Permalink to this headline")

```
fs set <fs name> max_file_size <size in bytes>
```

CephFSには設定可能な最大ファイルサイズがあり、デフォルトでは1TBとなっています。CephFSに大きなファイルを保存することが予想される場合は、この上限を高く設定することをお勧めします。これは64ビットフィールドです。

max\_file\_sizeを0に設定しても、制限は無効にはなりません。単に、クライアントが空のファイルしか作れないように制限するだけです。

## Maximum file sizes and performance[¶](https://docs.ceph.com/en/pacific/cephfs/administration/ "Permalink to this headline")

CephFSは、ファイルに追加したり、ファイルのサイズを設定したりする時点で、最大ファイルサイズ制限を適用します。 何かの保存方法には影響しません。

ユーザーが（必ずしもデータを書き込むことなく）巨大なサイズのファイルを作成した場合、削除などの操作により、MDSは、（ファイルサイズに応じて）存在し得る範囲内のRADOSオブジェクトが本当に存在するかどうかを確認するために大量の操作を行わなければならなくなることがあります。

max\_file\_sizeの設定により、ユーザーが例えばエクサバイトのようなサイズのファイルを作成し、データシートが統計や削除などの操作の際にオブジェクトを列挙しようとする際に負荷がかかることを防ぐことができます。

## Taking the cluster down[¶](https://docs.ceph.com/en/pacific/cephfs/administration/ "Permalink to this headline")

CephFSクラスタの停止は、downフラグを設定することで行われます：

```
fs set <fs_name> down true
```

クラスタをオンラインに戻すには：

```
fs set <fs_name> down false
```

これにより、max\_mdsの以前の値も復元されます。MDSデーモンは、ジャーナルがメタデータプールにフラッシュされ、すべてのクライアントI/Oが停止されるような方法で停止されます。

## Taking the cluster down rapidly for deletion or disaster recovery[¶](https://docs.ceph.com/en/pacific/cephfs/administration/ "Permalink to this headline")

ファイルシステムを迅速に削除したり（テスト用）、ファイルシステムとMDSデーモンを迅速に停止させるには、fs failコマンドを使用します。

```
fs fail <fs_name>
```

このコマンドは、ファイルシステムフラグ（joinableフラグ）を設定して、スタンバイがファイルシステムでアクティブ化されないようにします。

この作業は、次のようにして手動で行うこともできます：

```
fs set <fs_name> joinable false
```

その後、オペレータはすべてのランクに失敗し、MDSデーモンがスタンバイとして再起 動することになります。ファイルシステムはデグレードされた状態で残されます。

```
# For all ranks, 0-N:
mds fail <fs_name>:<n>
```

すべてのランクが非アクティブになったら、ファイルシステムを削除するか、他の目的（おそらく災害復旧）のためにこの状態のままにしておくことができます。

クラスタを復活させるには、joinableフラグを設定するだけです：

```
fs set <fs_name> joinable true
```

## Daemons[¶](https://docs.ceph.com/en/pacific/cephfs/administration/ "Permalink to this headline")

MDSを操作するほとんどのコマンドは\<role\>引数をとり、その引数は3つの形式のうちの1つです：

```
<fs_name>:<rank>
<fs_id>:<rank>
<rank>
```

MDSデーモンを操作するためのコマンド：

```
mds fail <gid/name/role>
```

MDSデーモンをfailedとマークします。 これは、MDSデーモンがmonへのメッセージ送信にmds\_beacon\_grace秒失敗した場合にクラスタが行うことと同じです。 デーモンがアクティブで、適切なスタンバイが利用可能な場合、mds failを使用すると、スタンバイへのフェイルオーバーが強制されます。

MDSデーモンが実際にはまだ動作していた場合、mds failを使用するとデーモンが再起動されます。 それがアクティブであり、スタンバイが利用可能であった場合、「failed」デーモンはスタンバイとして復帰します。

```
tell mds.<daemon name> command ...
```

MDSデーモンにコマンドを送信します。すべてのデーモンにコマンドを送信するには、mds.\*を使用します。利用可能なコマンドを知るには、ceph tell mds.\* helpを使用します。

```
mds metadata <gid/name/role>
```

モニターが知っている指定されたMDSに関するメタデータを取得します。

```
mds repaired <role>
```

ファイルシステムランクをrepairedとマークします。このコマンドは、その名前が示すように、MDSを変更するのではなく、damagedとマークされたファイルシステムのランクを操作します。

## Required Client Features[¶](https://docs.ceph.com/en/pacific/cephfs/administration/ "Permalink to this headline")

クライアントがCephFSと通信するためにサポートしなければならない機能を設定することが望ましい場合があります。これらの機能を持たないクライアントは、他のクライアントを混乱させたり、驚くような動作をする可能性があります。または、古いクライアントやバグがある可能性のあるクライアントが接続できないように、新しい機能を要求することもあります。

ファイルシステムの必要なクライアント機能を操作するためのコマンド：

```
fs required_client_features <fs name> add reply_encoding
fs required_client_features <fs name> rm reply_encoding
```

CephFSの全機能を一覧表示するには：

```
fs feature ls
```

新しく追加された機能がないクライアントは、自動的に退去します。

現在のCephFSの機能と、それらが最初に出たリリースを紹介します：

|Feature           |Ceph release|Upstream Kernel|
|------------------|------------|---------------|
|jewel             |jewel       |4.5            |
|kraken            |kraken      |4.13           |
|luminous          |luminous    |4.13           |
|mimic             |mimic       |4.19           |
|reply\_encoding   |nautilus    |5.1            |
|reclaim\_client   |nautilus    |N/A            |
|lazy\_caps\_wanted|nautilus    |5.1            |
|multi\_reconnect  |nautilus    |5.1            |
|deleg\_ino        |octopus     |5.6            |
|metric\_collect   |pacific     |N/A            |
|alternate\_name   |pacific     |PLANNED        |

CephFSの機能説明

```
reply_encoding
```

クライアントがこの機能をサポートしている場合、MDSはリクエスト応答を拡張可能なフォーマットでエンコードします。

```
reclaim_client
```

MDSは、新しいクライアントが他の（死んだ）クライアントの状態を再取得することを可能にします。この機能は、NFS\-Ganeshaで使用されています。

```
lazy_caps_wanted
```

古くなったクライアントが再開するとき、クライアントがこの機能をサポートしていれば、mdsは明示的に要求されたCAPSだけを再発行する必要があります。

```
multi_reconnect
```

mdsのフェイルオーバー時に、クライアントはmdsに再接続メッセージを送信し、キャッシュの状態を再確立します。MDSがこの機能をサポートしている場合、クライアントは大きな再接続メッセージを複数のメッセージに分割することができます。

```
deleg_ino
```

クライアントがこの機能をサポートしている場合、MDSはinode番号をクライアントに委譲します。クライアントが非同期ファイル作成を行うには、委任されたinode番号を持つことが前提条件となります。

```
metric_collect
```

MDSがこの機能をサポートしている場合、クライアントはMDSにパフォーマンスメトリックを送信することができます。

```
alternate_name
```

クライアントは、ディレクトリエントリに「代替名」を設定し、理解することができます。これは、暗号化されたファイル名のサポートに使用するものです。

## Global settings[¶](https://docs.ceph.com/en/pacific/cephfs/administration/ "Permalink to this headline")

```
fs flag set <flag name> <flag val> [<confirmation string>]
```

グローバルなCephFSフラグを設定します\(つまり、特定のファイルシステムに固有のフラグではありません\)。現在、フラグ設定は「enable\_multiple」のみで、複数のCephFSファイルシステムを持つことが可能です。

フラグによっては、"\-yes\-i\-really\-mean\-it "などの文字列で自分の意思を確認するよう求められるものもあります。これらは、特に危険な行為に対して設定されているため、よく考えて行動してください。

## Advanced[¶](https://docs.ceph.com/en/pacific/cephfs/administration/ "Permalink to this headline")

これらのコマンドは、通常の操作では必要なく、例外的な状況で使用するために存在します。 これらのコマンドを誤って使用すると、ファイルシステムにアクセスできなくなるなど、深刻な問題が発生する可能性があります。

```
mds rmfailed
```

これにより、failedセットからランクが削除されます。

```
fs reset <file system name>
```

このコマンドは、名前とプールを除き、ファイルシステムの状態をデフォルトにリセットします。0 以外のランクは、停止したセットに保存されます。

```
fs new <file system name> <metadata pool name> <data pool name> --fscid <fscid> --force
```

このコマンドは、特定のfscid（ファイルシステムクラスターID）を持つファイルシステムを作成します。例えば、モニター・データベースが失われ、再構築された後、アプリケーションがファイル・システムのIDが安定することを期待する場合に、これを実行するとよいでしょう。したがって、ファイルシステムIDは常に新しいファイルシステムで増加し続けるわけではありません。
