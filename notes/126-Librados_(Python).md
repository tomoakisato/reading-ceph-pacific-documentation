# 126: Librados (Python)

 # Librados \(Python\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this headline")

The `rados` module is a thin Python wrapper for `librados`.

## Installation[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this headline")

To install Python libraries for Ceph, see [Getting librados for Python](https://docs.ceph.com/en/pacific/rados/api/python/).

## Getting Started[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this headline")

You can create your own Ceph client using Python. The following tutorial will show you how to import the Ceph Python module, connect to a Ceph cluster, and perform object operations as a `client.admin` user.

Note:

To use the Ceph Python bindings, you must have access to a running Ceph cluster. To set one up quickly, see [Getting Started](https://docs.ceph.com/en/pacific/rados/api/python/).

First, create a Python source file for your Ceph client. ::linenossudo vim client.py

### Import the Module[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this headline")

To use the `rados` module, import it into your source file.

|```
1
```

|```
     import rados
```

|
|-------|-----------------------|

### Configure a Cluster Handle[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this headline")

Before connecting to the Ceph Storage Cluster, create a cluster handle. By default, the cluster handle assumes a cluster named `ceph` \(i.e., the default for deployment tools, and our Getting Started guides too\), and a`client.admin` user name. You may change these defaults to suit your needs.

To connect to the Ceph Storage Cluster, your application needs to know where to find the Ceph Monitor. Provide this information to your application by specifying the path to your Ceph configuration file, which contains the location of the initial Ceph monitors.

|```
1
2
3
4
5
6
```

|```
     import rados, sys

     #Create Handle Examples.
     cluster = rados.Rados(conffile='ceph.conf')
     cluster = rados.Rados(conffile=sys.argv[1])
     cluster = rados.Rados(conffile = 'ceph.conf', conf = dict (keyring = '/path/to/keyring'))
```

|
|------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|

Ensure that the `conffile` argument provides the path and file name of your Ceph configuration file. You may use the `sys` module to avoid hard\-coding the Ceph configuration path and file name.

Your Python client also requires a client keyring. For this example, we use the`client.admin` key by default. If you would like to specify the keyring when creating the cluster handle, you may use the `conf` argument. Alternatively, you may specify the keyring path in your Ceph configuration file. For example, you may add something like the following line to you Ceph configuration file:

```
keyring = /path/to/ceph.client.admin.keyring
```

For additional details on modifying your configuration via Python, see [Configuration](https://docs.ceph.com/en/pacific/rados/api/python/).

### Connect to the Cluster[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this headline")

Once you have a cluster handle configured, you may connect to the cluster. With a connection to the cluster, you may execute methods that return information about the cluster.

|```
 1
 2
 3
 4
 5
 6
 7
 8
 9
10
11
12
13
14
15
```

|```
     import rados, sys

     cluster = rados.Rados(conffile='ceph.conf')
     print "nlibrados version: " + str(cluster.version())
     print "Will attempt to connect to: " + str(cluster.conf_get('mon host'))

     cluster.connect()
     print "nCluster ID: " + cluster.get_fsid()

     print "nnCluster Statistics"
     print "=================="
     cluster_stats = cluster.get_cluster_stats()

     for key, value in cluster_stats.iteritems():
             print key, value
```

|
|------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|

By default, Ceph authentication is `on`. Your application will need to know the location of the keyring. The `python-ceph` module doesn’t have the default location, so you need to specify the keyring path. The easiest way to specify the keyring is to add it to the Ceph configuration file. The following Ceph configuration file example uses the `client.admin` keyring.

|```
1
2
3
```

|```
     [global]
     # ... elided configuration
     keyring=/path/to/keyring/ceph.client.admin.keyring
```

|
|---------|---------------------------------------------------------------------------------------------------------|

### Manage Pools[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this headline")

When connected to the cluster, the `Rados` API allows you to manage pools. You can list pools, check for the existence of a pool, create a pool and delete a pool.

|```
 1
 2
 3
 4
 5
 6
 7
 8
 9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
```

|```
     print "nnPool Operations"
     print "==============="

     print "nAvailable Pools"
     print "----------------"
     pools = cluster.list_pools()

     for pool in pools:
             print pool

     print "nCreate 'test' Pool"
     print "------------------"
     cluster.create_pool('test')

     print "nPool named 'test' exists: " + str(cluster.pool_exists('test'))
     print "nVerify 'test' Pool Exists"
     print "-------------------------"
     pools = cluster.list_pools()

     for pool in pools:
             print pool

     print "nDelete 'test' Pool"
     print "------------------"
     cluster.delete_pool('test')
     print "nPool named 'test' exists: " + str(cluster.pool_exists('test'))
```

|
|----------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|

### Input/Output Context[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this headline")

Reading from and writing to the Ceph Storage Cluster requires an input/output context \(ioctx\). You can create an ioctx with the `open_ioctx()` or`open_ioctx2()` method of the `Rados` class. The `ioctx_name` parameter is the name of the pool and `pool_id` is the ID of the pool you wish to use.

|```
1
```

|```
     ioctx = cluster.open_ioctx('data')
```

|
|-------|---------------------------------------------|

or

|```
1
```

|```
     ioctx = cluster.open_ioctx2(pool_id)
```

|
|-------|-----------------------------------------------|

Once you have an I/O context, you can read/write objects, extended attributes, and perform a number of other operations. After you complete operations, ensure that you close the connection. For example:

|```
1
2
```

|```
     print "nClosing the connection."
     ioctx.close()
```

|
|--------|-------------------------------------------------------------|

### Writing, Reading and Removing Objects[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this headline")

Once you create an I/O context, you can write objects to the cluster. If you write to an object that doesn’t exist, Ceph creates it. If you write to an object that exists, Ceph overwrites it \(except when you specify a range, and then it only overwrites the range\). You may read objects \(and object ranges\) from the cluster. You may also remove objects from the cluster. For example:

|```
1
2
3
4
5
6
7
8
```

|```
print "nWriting object 'hw' with contents 'Hello World!' to pool 'data'."
ioctx.write_full("hw", "Hello World!")

print "nnContents of object 'hw'n------------------------n"
print ioctx.read("hw")

print "nRemoving object 'hw'"
ioctx.remove_object("hw")
```

|
|--------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|

### Writing and Reading XATTRS[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this headline")

Once you create an object, you can write extended attributes \(XATTRs\) to the object and read XATTRs from the object. For example:

|```
1
2
3
4
5
```

|```
print "nnWriting XATTR 'lang' with value 'en_US' to object 'hw'"
ioctx.set_xattr("hw", "lang", "en_US")

print "nnGetting XATTR 'lang' from object 'hw'n"
print ioctx.get_xattr("hw", "lang")
```

|
|-----------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|

### Listing Objects[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this headline")

If you want to examine the list of objects in a pool, you may retrieve the list of objects and iterate over them with the object iterator. For example:

|```
 1
 2
 3
 4
 5
 6
 7
 8
 9
10
```

|```
object_iterator = ioctx.list_objects()

while True :

        try :
                rados_object = object_iterator.next()
                print "Object contents = " + rados_object.read()

        except StopIteration :
                break
```

|
|--------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|

The `Object` class provides a file\-like interface to an object, allowing you to read and write content and extended attributes. Object operations using the I/O context provide additional functionality and asynchronous capabilities.

## Cluster Handle API[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this headline")

The `Rados` class provides an interface into the Ceph Storage Daemon.

### Configuration[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this headline")

The `Rados` class provides methods for getting and setting configuration values, reading the Ceph configuration file, and parsing arguments. You do not need to be connected to the Ceph Storage Cluster to invoke the following methods. See [Storage Cluster Configuration](https://docs.ceph.com/en/pacific/rados/api/python/) for details on settings.

`Rados.``conf_get`\(_option_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Rados.conf\_get\(self, unicode option: str\) \-\> Optional\[str\]

Get the value of a configuration option

Parameters**option** \(`str`\) – which option to read

Return type`Optional`\[`str`\]

Returnsstr \- value of the option or None

Raises`TypeError`

`Rados.``conf_set`\(_option_, _val_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Rados.conf\_set\(self, unicode option: str, unicode val: str\)

Set the value of a configuration option

Parameters
* **option** \(`str`\) – which option to set
* **option** – value of the option

Raises`TypeError`, `ObjectNotFound`

`Rados.``conf_read_file`\(_path_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Rados.conf\_read\_file\(self, path: Optional\[str\] = None\)

Configure the cluster handle using a Ceph config file.

Parameters**path** \(`Optional`\[`str`\]\) – path to the config file

`Rados.``conf_parse_argv`\(_args_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Rados.conf\_parse\_argv\(self, args: Sequence\[str\]\)

Parse known arguments from args, and remove; returned args contain only those unknown to ceph

`Rados.``version`\(\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Rados.version\(self\) \-\> Version

Get the version number of the `librados` C library.

Return type`Version`

Returnsa tuple of `(major, minor, extra)` components of the librados version

### Connection Management[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this headline")

Once you configure your cluster handle, you may connect to the cluster, check the cluster `fsid`, retrieve cluster statistics, and disconnect \(shutdown\) from the cluster. You may also assert that the cluster handle is in a particular state \(e.g., “configuring”, “connecting”, etc.\).

`Rados.``connect`\(_timeout_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Rados.connect\(self, timeout: int = 0\)

Connect to the cluster. Use shutdown\(\) to release resources.

`Rados.``shutdown`\(\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Rados.shutdown\(self\)

Disconnects from the cluster. Call this explicitly when a Rados.connect\(\)ed object is no longer used.

`Rados.``get_fsid`\(\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Rados.get\_fsid\(self\) \-\> str

Get the fsid of the cluster as a hexadecimal string.

Raises`Error`

Return type`str`

Returnsstr \- cluster fsid

`Rados.``get_cluster_stats`\(\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Rados.get\_cluster\_stats\(self\) \-\> Dict\[str, int\]

Read usage info about the cluster

This tells you total space, space used, space available, and number of objects. These are not updated immediately when data is written, they are eventually consistent.

Return type`Dict`\[`str`, `int`\]

Returns

dict \- contains the following keys:

* `kb` \(int\) \- total space
* `kb_used` \(int\) \- space used
* `kb_avail` \(int\) \- free space available
* `num_objects` \(int\) \- number of objects

_class_ `rados.``Rados`[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")`require_state`\(_\*args_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Checks if the Rados object is in a special state

Parameters**args** – Any number of states to check as separate arguments

Raises`RadosStateError`

### Pool Operations[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this headline")

To use pool operation methods, you must connect to the Ceph Storage Cluster first. You may list the available pools, create a pool, check to see if a pool exists, and delete a pool.

`Rados.``list_pools`\(\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Rados.list\_pools\(self\) \-\> List\[str\]

Gets a list of pool names.

Return type`List`\[`str`\]

Returnslist \- of pool names.

`Rados.``create_pool`\(_pool\_name_, _crush\_rule_, _auid_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Rados.create\_pool\(self, unicode pool\_name: str, crush\_rule: Optional\[int\] = None, auid: Optional\[int\] = None\)

Create a pool: \- with default settings: if crush\_rule=None and auid=None \- with a specific CRUSH rule: crush\_rule given \- with a specific auid: auid given \- with a specific CRUSH rule and auid: crush\_rule and auid given

Parameters
* **pool\_name** \(`str`\) – name of the pool to create
* **crush\_rule** \(`Optional`\[`int`\]\) – rule to use for placement in the new pool
* **auid** \(`Optional`\[`int`\]\) – id of the owner of the new pool

Raises`TypeError`, `Error`

`Rados.``pool_exists`\(_pool\_name_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Rados.pool\_exists\(self, unicode pool\_name: str\) \-\> bool

Checks if a given pool exists.

Parameters**pool\_name** \(`str`\) – name of the pool to check

Raises`TypeError`, `Error`

Return type`bool`

Returnsbool \- whether the pool exists, false otherwise.

`Rados.``delete_pool`\(_pool\_name_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Rados.delete\_pool\(self, unicode pool\_name: str\)

Delete a pool and all data inside it.

The pool is removed from the cluster immediately, but the actual data is deleted in the background.

Parameters**pool\_name** \(`str`\) – name of the pool to delete

Raises`TypeError`, `Error`

### CLI Commands[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this headline")

The Ceph CLI command is internally using the following librados Python binding methods.

In order to send a command, choose the correct method and choose the correct target.

`Rados.``mon_command`\(_cmd_, _inbuf_, _timeout_, _target_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Send a command to the mon.

mon\_command\[\_target\]\(cmd, inbuf, outbuf, outbuflen, outs, outslen\)

Parameters
* **cmd** \(`str`\) – JSON formatted string.
* **inbuf** \(`bytes`\) – optional string.
* **timeout** \(`int`\) – This parameter is ignored.
* **target** \(`Union`\[`int`, `str`, `None`\]\) – name or rank of a specific mon. Optional

Return type`Tuple`\[`int`, `bytes`, `str`\]

Returns\(int ret, string outbuf, string outs\)

Example:

```
>>> import json
>>> c = Rados(conffile='/etc/ceph/ceph.conf')
>>> c.connect()
>>> cmd = json.dumps({"prefix": "osd safe-to-destroy", "ids": ["2"], "format": "json"})
>>> c.mon_command(cmd, b'')
```

`Rados.``osd_command`\(_osdid_, _cmd_, _inbuf_, _timeout_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")osd\_command\(osdid, cmd, inbuf, outbuf, outbuflen, outs, outslen\)

Return type`Tuple`\[`int`, `bytes`, `str`\]

Returns\(int ret, string outbuf, string outs\)

`Rados.``mgr_command`\(_cmd_, _inbuf_, _timeout_, _target_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Return type`Tuple`\[`int`, `str`, `bytes`\]

Returns\(int ret, string outbuf, string outs\)

`Rados.``pg_command`\(_pgid_, _cmd_, _inbuf_, _timeout_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")pg\_command\(pgid, cmd, inbuf, outbuf, outbuflen, outs, outslen\)

Return type`Tuple`\[`int`, `bytes`, `str`\]

Returns\(int ret, string outbuf, string outs\)

## Input/Output Context API[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this headline")

To write data to and read data from the Ceph Object Store, you must create an Input/Output context \(ioctx\). The Rados class provides open\_ioctx\(\)and open\_ioctx2\(\) methods. The remaining `ioctx` operations involve invoking methods of the Ioctx and other classes.

`Rados.``open_ioctx`\(_ioctx\_name_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Rados.open\_ioctx\(self, unicode ioctx\_name: str\) \-\> Ioctx

Create an io context

The io context allows you to perform operations within a particular pool.

Parameters**ioctx\_name** \(`str`\) – name of the pool

Raises`TypeError`, `Error`

Return type`Ioctx`

ReturnsIoctx \- Rados Ioctx object

`Ioctx.``require_ioctx_open`\(\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Ioctx.require\_ioctx\_open\(self\)

Checks if the rados.Ioctx object state is ‘open’

RaisesIoctxStateError

`Ioctx.``get_stats`\(\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Ioctx.get\_stats\(self\) \-\> Dict\[str, int\]

Get pool usage statistics

Return type`Dict`\[`str`, `int`\]

Returns

dict \- contains the following keys:

* `num_bytes` \(int\) \- size of pool in bytes
* `num_kb` \(int\) \- size of pool in kbytes
* `num_objects` \(int\) \- number of objects in the pool
* `num_object_clones` \(int\) \- number of object clones
* `num_object_copies` \(int\) \- number of object copies
* `num_objects_missing_on_primary` \(int\) \- number of objetsmissing on primary
* `num_objects_unfound` \(int\) \- number of unfound objects
* `num_objects_degraded` \(int\) \- number of degraded objects
* `num_rd` \(int\) \- bytes read
* `num_rd_kb` \(int\) \- kbytes read
* `num_wr` \(int\) \- bytes written
* `num_wr_kb` \(int\) \- kbytes written

`Ioctx.``get_last_version`\(\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Ioctx.get\_last\_version\(self\) \-\> int

Return the version of the last object read or written to.

This exposes the internal version number of the last object read or written via this io context

Return type`int`

Returnsversion of the last object used

`Ioctx.``close`\(\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Ioctx.close\(self\)

Close a rados.Ioctx object.

This just tells librados that you no longer need to use the io context. It may not be freed immediately if there are pending asynchronous requests on it, but you should not use an io context again after calling this function on it.

### Object Operations[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this headline")

The Ceph Storage Cluster stores data as objects. You can read and write objects synchronously or asynchronously. You can read and write from offsets. An object has a name \(or key\) and data.

`Ioctx.``aio_write`\(_object\_name_, _to\_write_, _offset_, _oncomplete_, _onsafe_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Ioctx.aio\_write\(self, unicode object\_name: str, bytes to\_write: bytes, offset: int = 0, oncomplete: Optional\[Callable\[\[Completion\], None\]\] = None, onsafe: Optional\[Callable\[\[Completion\], None\]\] = None\) \-\> Completion

Write data to an object asynchronously

Queues the write and returns.

Parameters
* **object\_name** \(`str`\) – name of the object
* **to\_write** \(`bytes`\) – data to write
* **offset** \(`int`\) – byte offset in the object to begin writing at
* **oncomplete** \(`Optional`\[`Callable`\[\[`Completion`\], `None`\]\]\) – what to do when the write is safe and complete in memory on all replicas
* **onsafe** \(`Optional`\[`Callable`\[\[`Completion`\], `None`\]\]\) – what to do when the write is safe and complete on storage on all replicas

Raises`Error`

Return type`Completion`

Returnscompletion object

`Ioctx.``aio_write_full`\(_object\_name_, _to\_write_, _oncomplete_, _onsafe_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Ioctx.aio\_write\_full\(self, unicode object\_name: str, bytes to\_write: bytes, oncomplete: Optional\[Callable\] = None, onsafe: Optional\[Callable\] = None\) \-\> Completion

Asynchronously write an entire object

The object is filled with the provided data. If the object exists, it is atomically truncated and then written. Queues the write and returns.

Parameters
* **object\_name** \(`str`\) – name of the object
* **to\_write** \(`bytes`\) – data to write
* **oncomplete** \(`Optional`\[`Callable`\]\) – what to do when the write is safe and complete in memory on all replicas
* **onsafe** \(`Optional`\[`Callable`\]\) – what to do when the write is safe and complete on storage on all replicas

Raises`Error`

Return type`Completion`

Returnscompletion object

`Ioctx.``aio_append`\(_object\_name_, _to\_append_, _oncomplete_, _onsafe_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Ioctx.aio\_append\(self, unicode object\_name: str, bytes to\_append: bytes, oncomplete: Optional\[Callable\] = None, onsafe: Optional\[Callable\] = None\) \-\> Completion

Asynchronously append data to an object

Queues the write and returns.

Parameters
* **object\_name** \(`str`\) – name of the object
* **to\_append** \(`bytes`\) – data to append
* **offset** – byte offset in the object to begin writing at
* **oncomplete** \(`Optional`\[`Callable`\]\) – what to do when the write is safe and complete in memory on all replicas
* **onsafe** \(`Optional`\[`Callable`\]\) – what to do when the write is safe and complete on storage on all replicas

Raises`Error`

Return type`Completion`

Returnscompletion object

`Ioctx.``write`\(_key_, _data_, _offset_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Ioctx.write\(self, unicode key: str, bytes data: bytes, offset: int = 0\)

Write data to an object synchronously

Parameters
* **key** \(`str`\) – name of the object
* **data** \(`bytes`\) – data to write
* **offset** \(`int`\) – byte offset in the object to begin writing at

Raises`TypeError`

Raises`LogicError`

Returnsint \- 0 on success

`Ioctx.``write_full`\(_key_, _data_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Ioctx.write\_full\(self, unicode key: str, bytes data: bytes\)

Write an entire object synchronously.

The object is filled with the provided data. If the object exists, it is atomically truncated and then written.

Parameters
* **key** \(`str`\) – name of the object
* **data** \(`bytes`\) – data to write

Raises`TypeError`

Raises`Error`

Returnsint \- 0 on success

`Ioctx.``aio_flush`\(\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Ioctx.aio\_flush\(self\)

Block until all pending writes in an io context are safe

Raises`Error`

`Ioctx.``set_locator_key`\(_loc\_key_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Ioctx.set\_locator\_key\(self, unicode loc\_key: str\)

Set the key for mapping objects to pgs within an io context.

The key is used instead of the object name to determine which placement groups an object is put in. This affects all subsequent operations of the io context \- until a different locator key is set, all objects in this io context will be placed in the same pg.

Parameters**loc\_key** \(`str`\) – the key to use as the object locator, or NULL to discard any previously set key

Raises`TypeError`

`Ioctx.``aio_read`\(_object\_name_, _length_, _offset_, _oncomplete_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Ioctx.aio\_read\(self, unicode object\_name: str, length: int, offset: int, oncomplete: Optional\[Callable\] = None\) \-\> Completion

Asynchronously read data from an object

oncomplete will be called with the returned read value as well as the completion:

oncomplete\(completion, data\_read\)

Parameters
* **object\_name** \(`str`\) – name of the object to read from
* **length** \(`int`\) – the number of bytes to read
* **offset** \(`int`\) – byte offset in the object to begin reading from
* **oncomplete** \(`Optional`\[`Callable`\]\) – what to do when the read is complete

Raises`Error`

Return type`Completion`

Returnscompletion object

`Ioctx.``read`\(_key_, _length_, _offset_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Ioctx.read\(self, unicode key: str, length: int = 8192, offset: int = 0\) \-\> bytes

Read data from an object synchronously

Parameters
* **key** \(`str`\) – name of the object
* **length** \(`int`\) – the number of bytes to read \(default=8192\)
* **offset** \(`int`\) – byte offset in the object to begin reading at

Raises`TypeError`

Raises`Error`

Return type`bytes`

Returnsstr \- data read from object

`Ioctx.``stat`\(_key_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Ioctx.stat\(self, unicode key: str\) \-\> Tuple\[int, time.struct\_time\]

Get object stats \(size/mtime\)

Parameters**key** \(`str`\) – the name of the object to get stats from

Raises`TypeError`

Raises`Error`

Return type`Tuple`\[`int`, `struct_time`\]

Returns\(size,timestamp\)

`Ioctx.``trunc`\(_key_, _size_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Ioctx.trunc\(self, unicode key: str, size: int\)

Resize an object

If this enlarges the object, the new area is logically filled with zeroes. If this shrinks the object, the excess data is removed.

Parameters
* **key** \(`str`\) – the name of the object to resize
* **size** \(`int`\) – the new size of the object in bytes

Raises`TypeError`

Raises`Error`

Returnsint \- 0 on success, otherwise raises error

`Ioctx.``remove_object`\(_key_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Ioctx.remove\_object\(self, unicode key: str\) \-\> bool

Delete an object

This does not delete any snapshots of the object.

Parameters**key** \(`str`\) – the name of the object to delete

Raises`TypeError`

Raises`Error`

Return type`bool`

Returnsbool \- True on success

### Object Extended Attributes[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this headline")

You may set extended attributes \(XATTRs\) on an object. You can retrieve a list of objects or XATTRs and iterate over them.

`Ioctx.``set_xattr`\(_key_, _xattr\_name_, _xattr\_value_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Ioctx.set\_xattr\(self, unicode key: str, unicode xattr\_name: str, bytes xattr\_value: bytes\) \-\> bool

Set an extended attribute on an object.

Parameters
* **key** \(`str`\) – the name of the object to set xattr to
* **xattr\_name** \(`str`\) – which extended attribute to set
* **xattr\_value** \(`bytes`\) – the value of the extended attribute

Raises`TypeError`

Raises`Error`

Return type`bool`

Returnsbool \- True on success, otherwise raise an error

`Ioctx.``get_xattrs`\(_oid_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Ioctx.get\_xattrs\(self, unicode oid: str\) \-\> XattrIterator

Start iterating over xattrs on an object.

Parameters**oid** \(`str`\) – the name of the object to get xattrs from

Raises`TypeError`

Raises`Error`

Return type`XattrIterator`

ReturnsXattrIterator

`XattrIterator.``__next__`\(\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Get the next xattr on the object

RaisesStopIteration

Returnspair \- of name and value of the next Xattr

`Ioctx.``get_xattr`\(_key_, _xattr\_name_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Ioctx.get\_xattr\(self, unicode key: str, unicode xattr\_name: str\) \-\> bytes

Get the value of an extended attribute on an object.

Parameters
* **key** \(`str`\) – the name of the object to get xattr from
* **xattr\_name** \(`str`\) – which extended attribute to read

Raises`TypeError`

Raises`Error`

Return type`bytes`

Returnsstr \- value of the xattr

`Ioctx.``rm_xattr`\(_key_, _xattr\_name_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Ioctx.rm\_xattr\(self, unicode key: str, unicode xattr\_name: str\) \-\> bool

Removes an extended attribute on from an object.

Parameters
* **key** \(`str`\) – the name of the object to remove xattr from
* **xattr\_name** \(`str`\) – which extended attribute to remove

Raises`TypeError`

Raises`Error`

Return type`bool`

Returnsbool \- True on success, otherwise raise an error

## Object Interface[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this headline")

From an I/O context, you can retrieve a list of objects from a pool and iterate over them. The object interface provide makes each object look like a file, and you may perform synchronous operations on the objects. For asynchronous operations, you should use the I/O context methods.

`Ioctx.``list_objects`\(\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Ioctx.list\_objects\(self\) \-\> ObjectIterator

Get ObjectIterator on rados.Ioctx object.

Return type`ObjectIterator`

ReturnsObjectIterator

`ObjectIterator.``__next__`\(\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")Get the next object name and locator in the pool

RaisesStopIteration

Returnsnext rados.Ioctx Object

`Object.``read`\(_length=1024 \* 1024_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")`Object.``write`\(_string\_to\_write_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")`Object.``get_xattrs`\(\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")`Object.``get_xattr`\(_xattr\_name_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")`Object.``set_xattr`\(_xattr\_name_, _xattr\_value_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")`Object.``rm_xattr`\(_xattr\_name_\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")`Object.``stat`\(\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")`Object.``remove`\(\)[¶](https://docs.ceph.com/en/pacific/rados/api/python/ "Permalink to this definition")
