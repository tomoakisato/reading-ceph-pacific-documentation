# 36: Basic Ceph Client Setup

**クリップソース:** [36: Basic Ceph Client Setup — Ceph Documentation](https://docs.ceph.com/en/pacific/cephadm/client-setup/)

# Basic Ceph Client Setup[¶](https://docs.ceph.com/en/pacific/cephadm/client-setup/ "Permalink to this headline")

Client machines require some basic configuration to interact with Ceph clusters. This section describes how to configure a client machine so that it can interact with a Ceph cluster.

注:ほとんどのクライアントマシンでは、ceph\-commonパッケージとその依存関係のみをインストールする必要があります。このようなセットアップでは、基本的なcephコマンドとradosコマンドのほか、mount.cephやrbdなどのその他のコマンドも提供されます。

## Config File Setup[¶](https://docs.ceph.com/en/pacific/cephadm/client-setup/ "Permalink to this headline")

クライアントマシンでは、通常、本格的なクラスタメンバーよりも小さな設定ファイル（ここでは「コンフィグファイル」と呼ぶことがあります）が必要になります。最小限の設定ファイルを作成するには、クライアントとして設定されているホストまたはクラスタ・デーモンを実行しているホストにログインし、次のコマンドを実行します。

```
# ceph config generate-minimal-conf
```

このコマンドは、クライアントがCephモニタに到達する方法を示す最小限の設定ファイルを生成します。このファイルの内容は、通常、/etc/ceph/ceph.confにインストールします。

## Keyring Setup[¶](https://docs.ceph.com/en/pacific/cephadm/client-setup/ "Permalink to this headline")

ほとんどのCephクラスターは、認証を有効にして動作します。これは、クライアントがクラスタ内のマシンと通信するためには鍵が必要であることを意味します。client.fs用の認証情報を含むキーリングファイルを生成するには、実行中のクラスタメンバーにログインして次のコマンドを実行します。

```
$ ceph auth get-or-create client.fs
```

結果の出力は、キーリングファイル（通常は/etc/ceph/ceph.keyring）に導かれます。

クライアントキーリングの配布と管理についてより深く理解するには、「[Client keyrings and configs](https://docs.ceph.com/en/pacific/cephadm/client-setup/).」をお読みください。

ceph.conf設定ファイルをbare\_configラベルでタグ付けされたホストに配布する方法を説明する例を見るには、[/etc/ceph/ceph.conf](https://docs.ceph.com/en/pacific/cephadm/client-setup/)と呼ばれるセクションの「Distributing ceph.conf to tagged with bare\_config」を読みます。[35: Cephadm Operations](35-Cephadm_Operations.md)
