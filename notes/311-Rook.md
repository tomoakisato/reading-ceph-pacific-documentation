# 311: Rook

**クリップソース:** [311: Rook — Ceph Documentation](https://docs.ceph.com/en/pacific/mgr/rook/)

# Rook[¶](https://docs.ceph.com/en/pacific/mgr/rook/ "Permalink to this headline")

Rook（https://rook.io/）は、Kubernetesクラスタ内でCephを実行することができるオーケストレーションツールです。

rookモジュールは、Cephのオーケストレータフレームワーク\(dashboardなどのモジュールがクラスタサービスを制御するために使用\)とRookの間の統合を提供します。

オーケストレーターのモジュールは、他のモジュールにサービスを提供するだけで、そのモジュールがユーザーインターフェースを提供します。 rook モジュールを試すには、Orchestrator CLI モジュールを使用するとよいでしょう。

## Requirements[¶](https://docs.ceph.com/en/pacific/mgr/rook/ "Permalink to this headline")

* KubernetesでRookを使用してセットアップされたceph\-monおよびceph\-mgrサービス
* Rook 0.9 以降のバージョン

## Configuration[¶](https://docs.ceph.com/en/pacific/mgr/rook/ "Permalink to this headline")

Rookクラスタのceph\-mgrデーモンはKubernetesポッドとして動作しているため、rookモジュールは明示的な設定なしにKubernetes APIに接続できます。

## Development[¶](https://docs.ceph.com/en/pacific/mgr/rook/ "Permalink to this headline")

開発者の方は、「[Hacking on Ceph in Kubernetes with Rook](https://docs.ceph.com/en/pacific/mgr/rook/)」で開発環境を構築して作業する手順をご覧ください。
