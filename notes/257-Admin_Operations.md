# 257: Admin Operations

 # Admin Operations[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

An admin API request will be done on a URI that starts with the configurable ‘admin’ resource entry point. Authorization for the admin API duplicates the S3 authorization mechanism. Some operations require that the user holds special administrative capabilities. The response entity type \(XML or JSON\) may be specified as the ‘format’ option in the request and defaults to JSON if not specified.

## Get Usage[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

Request bandwidth usage information.

Note: this feature is disabled by default, can be enabled by setting `rgw
enable usage log = true` in the appropriate section of ceph.conf. For changes in ceph.conf to take effect, radosgw process restart is needed.

capsusage=read

### Syntax[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

```
GET /{admin}/usage?format=json HTTP/1.1
Host: {fqdn}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`uid`

DescriptionThe user for which the information is requested. If not specified will apply to all users.

TypeString

Example`foo_user`

RequiredNo

`start`

DescriptionDate and \(optional\) time that specifies the start time of the requested data.

TypeString

Example`2012-09-25 16:00:00`

RequiredNo

`end`

DescriptionDate and \(optional\) time that specifies the end time of the requested data \(non\-inclusive\).

TypeString

Example`2012-09-25 16:00:00`

RequiredNo

`show-entries`

DescriptionSpecifies whether data entries should be returned.

TypeBoolean

ExampleTrue \[True\]

RequiredNo

`show-summary`

DescriptionSpecifies whether data summary should be returned.

TypeBoolean

ExampleTrue \[True\]

RequiredNo

### Response Entities[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

If successful, the response contains the requested information.

`usage`

DescriptionA container for the usage information.

TypeContainer

`entries`

DescriptionA container for the usage entries information.

TypeContainer

`user`

DescriptionA container for the user data information.

TypeContainer

`owner`

DescriptionThe name of the user that owns the buckets.

TypeString

`bucket`

DescriptionThe bucket name.

TypeString

`time`

DescriptionTime lower bound for which data is being specified \(rounded to the beginning of the first relevant hour\).

TypeString

`epoch`

DescriptionThe time specified in seconds since 1/1/1970.

TypeString

`categories`

DescriptionA container for stats categories.

TypeContainer

`entry`

DescriptionA container for stats entry.

TypeContainer

`category`

DescriptionName of request category for which the stats are provided.

TypeString

`bytes_sent`

DescriptionNumber of bytes sent by the RADOS Gateway.

TypeInteger

`bytes_received`

DescriptionNumber of bytes received by the RADOS Gateway.

TypeInteger

`ops`

DescriptionNumber of operations.

TypeInteger

`successful_ops`

DescriptionNumber of successful operations.

TypeInteger

`summary`

DescriptionA container for stats summary.

TypeContainer

`total`

DescriptionA container for stats summary aggregated total.

TypeContainer

### Special Error Responses[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

TBD.

## Trim Usage[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

Remove usage information. With no dates specified, removes all usage information.

Note: this feature is disabled by default, can be enabled by setting `rgw
enable usage log = true` in the appropriate section of ceph.conf. For changes in ceph.conf to take effect, radosgw process restart is needed.

capsusage=write

### Syntax[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

```
DELETE /{admin}/usage?format=json HTTP/1.1
Host: {fqdn}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`uid`

DescriptionThe user for which the information is requested. If not specified will apply to all users.

TypeString

Example`foo_user`

RequiredNo

`start`

DescriptionDate and \(optional\) time that specifies the start time of the requested data.

TypeString

Example`2012-09-25 16:00:00`

RequiredNo

`end`

DescriptionDate and \(optional\) time that specifies the end time of the requested data \(none inclusive\).

TypeString

Example`2012-09-25 16:00:00`

RequiredNo

`remove-all`

DescriptionRequired when uid is not specified, in order to acknowledge multi user data removal.

TypeBoolean

ExampleTrue \[False\]

RequiredNo

### Special Error Responses[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

TBD.

## Get User Info[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

Get user information.

capsusers=read

### Syntax[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

```
GET /{admin}/user?format=json HTTP/1.1
Host: {fqdn}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`uid`

DescriptionThe user for which the information is requested.

TypeString

Example`foo_user`

RequiredYes

### Response Entities[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

If successful, the response contains the user information.

`user`

DescriptionA container for the user data information.

TypeContainer

`user_id`

DescriptionThe user id.

TypeString

Parent`user`

`display_name`

DescriptionDisplay name for the user.

TypeString

Parent`user`

`suspended`

DescriptionTrue if the user is suspended.

TypeBoolean

Parent`user`

`max_buckets`

DescriptionThe maximum number of buckets to be owned by the user.

TypeInteger

Parent`user`

`subusers`

DescriptionSubusers associated with this user account.

TypeContainer

Parent`user`

`keys`

DescriptionS3 keys associated with this user account.

TypeContainer

Parent`user`

`swift_keys`

DescriptionSwift keys associated with this user account.

TypeContainer

Parent`user`

`caps`

DescriptionUser capabilities.

TypeContainer

Parent`user`

### Special Error Responses[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

None.

## Create User[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

Create a new user. By default, a S3 key pair will be created automatically and returned in the response. If only one of `access-key` or `secret-key`is provided, the omitted key will be automatically generated. By default, a generated key is added to the keyring without replacing an existing key pair. If `access-key` is specified and refers to an existing key owned by the user then it will be modified.

New in version Luminous.

A `tenant` may either be specified as a part of uid or as an additional request param.

capsusers=write

### Syntax[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

```
PUT /{admin}/user?format=json HTTP/1.1
Host: {fqdn}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`uid`

DescriptionThe user ID to be created.

TypeString

Example`foo_user`

RequiredYes

A tenant name may also specified as a part of `uid`, by following the syntax`tenant$user`, refer to [Multitenancy](https://docs.ceph.com/en/pacific/radosgw/adminops/) for more details.

`display-name`

DescriptionThe display name of the user to be created.

TypeString

Example`foo user`

RequiredYes

`email`

DescriptionThe email address associated with the user.

TypeString

Example`foo@bar.com`

RequiredNo

`key-type`

DescriptionKey type to be generated, options are: swift, s3 \(default\).

TypeString

Example`s3` \[`s3`\]

RequiredNo

`access-key`

DescriptionSpecify access key.

TypeString

Example`ABCD0EF12GHIJ2K34LMN`

RequiredNo

`secret-key`

DescriptionSpecify secret key.

TypeString

Example`0AbCDEFg1h2i34JklM5nop6QrSTUV+WxyzaBC7D8`

RequiredNo

`user-caps`

DescriptionUser capabilities.

TypeString

Example`usage=read, write; users=read`

RequiredNo

`generate-key`

DescriptionGenerate a new key pair and add to the existing keyring.

TypeBoolean

ExampleTrue \[True\]

RequiredNo

`max-buckets`

DescriptionSpecify the maximum number of buckets the user can own.

TypeInteger

Example500 \[1000\]

RequiredNo

`suspended`

DescriptionSpecify whether the user should be suspended.

TypeBoolean

ExampleFalse \[False\]

RequiredNo

New in version Jewel.

`tenant`

Descriptionthe Tenant under which a user is a part of.

Typestring

Exampletenant1

RequiredNo

### Response Entities[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

If successful, the response contains the user information.

`user`

DescriptionA container for the user data information.

TypeContainer

`tenant`

DescriptionThe tenant which user is a part of.

TypeString

Parent`user`

`user_id`

DescriptionThe user id.

TypeString

Parent`user`

`display_name`

DescriptionDisplay name for the user.

TypeString

Parent`user`

`suspended`

DescriptionTrue if the user is suspended.

TypeBoolean

Parent`user`

`max_buckets`

DescriptionThe maximum number of buckets to be owned by the user.

TypeInteger

Parent`user`

`subusers`

DescriptionSubusers associated with this user account.

TypeContainer

Parent`user`

`keys`

DescriptionS3 keys associated with this user account.

TypeContainer

Parent`user`

`swift_keys`

DescriptionSwift keys associated with this user account.

TypeContainer

Parent`user`

`caps`

DescriptionUser capabilities.

TypeContainer

Parent`user`

### Special Error Responses[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`UserExists`

DescriptionAttempt to create existing user.

Code409 Conflict

`InvalidAccessKey`

DescriptionInvalid access key specified.

Code400 Bad Request

`InvalidKeyType`

DescriptionInvalid key type specified.

Code400 Bad Request

`InvalidSecretKey`

DescriptionInvalid secret key specified.

Code400 Bad Request

`InvalidKeyType`

DescriptionInvalid key type specified.

Code400 Bad Request

`KeyExists`

DescriptionProvided access key exists and belongs to another user.

Code409 Conflict

`EmailExists`

DescriptionProvided email address exists.

Code409 Conflict

`InvalidCapability`

DescriptionAttempt to grant invalid admin capability.

Code400 Bad Request

## Modify User[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

Modify a user.

capsusers=write

### Syntax[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

```
POST /{admin}/user?format=json HTTP/1.1
Host: {fqdn}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`uid`

DescriptionThe user ID to be modified.

TypeString

Example`foo_user`

RequiredYes

`display-name`

DescriptionThe display name of the user to be modified.

TypeString

Example`foo user`

RequiredNo

`email`

DescriptionThe email address to be associated with the user.

TypeString

Example`foo@bar.com`

RequiredNo

`generate-key`

DescriptionGenerate a new key pair and add to the existing keyring.

TypeBoolean

ExampleTrue \[False\]

RequiredNo

`access-key`

DescriptionSpecify access key.

TypeString

Example`ABCD0EF12GHIJ2K34LMN`

RequiredNo

`secret-key`

DescriptionSpecify secret key.

TypeString

Example`0AbCDEFg1h2i34JklM5nop6QrSTUV+WxyzaBC7D8`

RequiredNo

`key-type`

DescriptionKey type to be generated, options are: swift, s3 \(default\).

TypeString

Example`s3`

RequiredNo

`user-caps`

DescriptionUser capabilities.

TypeString

Example`usage=read, write; users=read`

RequiredNo

`max-buckets`

DescriptionSpecify the maximum number of buckets the user can own.

TypeInteger

Example500 \[1000\]

RequiredNo

`suspended`

DescriptionSpecify whether the user should be suspended.

TypeBoolean

ExampleFalse \[False\]

RequiredNo

`op-mask`

DescriptionThe op\-mask of the user to be modified.

TypeString

Example`read, write, delete, *`

RequiredNo

### Response Entities[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

If successful, the response contains the user information.

`user`

DescriptionA container for the user data information.

TypeContainer

`user_id`

DescriptionThe user id.

TypeString

Parent`user`

`display_name`

DescriptionDisplay name for the user.

TypeString

Parent`user`

`suspended`

DescriptionTrue if the user is suspended.

TypeBoolean

Parent`user`

`max_buckets`

DescriptionThe maximum number of buckets to be owned by the user.

TypeInteger

Parent`user`

`subusers`

DescriptionSubusers associated with this user account.

TypeContainer

Parent`user`

`keys`

DescriptionS3 keys associated with this user account.

TypeContainer

Parent`user`

`swift_keys`

DescriptionSwift keys associated with this user account.

TypeContainer

Parent`user`

`caps`

DescriptionUser capabilities.

TypeContainer

Parent`user`

### Special Error Responses[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`InvalidAccessKey`

DescriptionInvalid access key specified.

Code400 Bad Request

`InvalidKeyType`

DescriptionInvalid key type specified.

Code400 Bad Request

`InvalidSecretKey`

DescriptionInvalid secret key specified.

Code400 Bad Request

`KeyExists`

DescriptionProvided access key exists and belongs to another user.

Code409 Conflict

`EmailExists`

DescriptionProvided email address exists.

Code409 Conflict

`InvalidCapability`

DescriptionAttempt to grant invalid admin capability.

Code400 Bad Request

## Remove User[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

Remove an existing user.

capsusers=write

### Syntax[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

```
DELETE /{admin}/user?format=json HTTP/1.1
Host: {fqdn}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`uid`

DescriptionThe user ID to be removed.

TypeString

Example`foo_user`

RequiredYes.

`purge-data`

DescriptionWhen specified the buckets and objects belonging to the user will also be removed.

TypeBoolean

ExampleTrue

RequiredNo

### Response Entities[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

None

### Special Error Responses[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

None.

## Create Subuser[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

Create a new subuser \(primarily useful for clients using the Swift API\). Note that in general for a subuser to be useful, it must be granted permissions by specifying `access`. As with user creation if`subuser` is specified without `secret`, then a secret key will be automatically generated.

capsusers=write

### Syntax[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

```
PUT /{admin}/user?subuser&format=json HTTP/1.1
Host {fqdn}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`uid`

DescriptionThe user ID under which a subuser is to be created.

TypeString

Example`foo_user`

RequiredYes

`subuser`

DescriptionSpecify the subuser ID to be created.

TypeString

Example`sub_foo`

RequiredYes

`secret-key`

DescriptionSpecify secret key.

TypeString

Example`0AbCDEFg1h2i34JklM5nop6QrSTUV+WxyzaBC7D8`

RequiredNo

`key-type`

DescriptionKey type to be generated, options are: swift \(default\), s3.

TypeString

Example`swift` \[`swift`\]

RequiredNo

`access`

DescriptionSet access permissions for sub\-user, should be one of `read, write, readwrite, full`.

TypeString

Example`read`

RequiredNo

`generate-secret`

DescriptionGenerate the secret key.

TypeBoolean

ExampleTrue \[False\]

RequiredNo

### Response Entities[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

If successful, the response contains the subuser information.

`subusers`

DescriptionSubusers associated with the user account.

TypeContainer

`id`

DescriptionSubuser id.

TypeString

Parent`subusers`

`permissions`

DescriptionSubuser access to user account.

TypeString

Parent`subusers`

### Special Error Responses[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`SubuserExists`

DescriptionSpecified subuser exists.

Code409 Conflict

`InvalidKeyType`

DescriptionInvalid key type specified.

Code400 Bad Request

`InvalidSecretKey`

DescriptionInvalid secret key specified.

Code400 Bad Request

`InvalidAccess`

DescriptionInvalid subuser access specified.

Code400 Bad Request

## Modify Subuser[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

Modify an existing subuser

capsusers=write

### Syntax[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

```
POST /{admin}/user?subuser&format=json HTTP/1.1
Host {fqdn}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`uid`

DescriptionThe user ID under which the subuser is to be modified.

TypeString

Example`foo_user`

RequiredYes

`subuser`

DescriptionThe subuser ID to be modified.

TypeString

Example`sub_foo`

RequiredYes

`generate-secret`

DescriptionGenerate a new secret key for the subuser, replacing the existing key.

TypeBoolean

ExampleTrue \[False\]

RequiredNo

`secret`

DescriptionSpecify secret key.

TypeString

Example`0AbCDEFg1h2i34JklM5nop6QrSTUV+WxyzaBC7D8`

RequiredNo

`key-type`

DescriptionKey type to be generated, options are: swift \(default\), s3 .

TypeString

Example`swift` \[`swift`\]

RequiredNo

`access`

DescriptionSet access permissions for sub\-user, should be one of `read, write, readwrite, full`.

TypeString

Example`read`

RequiredNo

### Response Entities[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

If successful, the response contains the subuser information.

`subusers`

DescriptionSubusers associated with the user account.

TypeContainer

`id`

DescriptionSubuser id.

TypeString

Parent`subusers`

`permissions`

DescriptionSubuser access to user account.

TypeString

Parent`subusers`

### Special Error Responses[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`InvalidKeyType`

DescriptionInvalid key type specified.

Code400 Bad Request

`InvalidSecretKey`

DescriptionInvalid secret key specified.

Code400 Bad Request

`InvalidAccess`

DescriptionInvalid subuser access specified.

Code400 Bad Request

## Remove Subuser[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

Remove an existing subuser

capsusers=write

### Syntax[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

```
DELETE /{admin}/user?subuser&format=json HTTP/1.1
Host {fqdn}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`uid`

DescriptionThe user ID under which the subuser is to be removed.

TypeString

Example`foo_user`

RequiredYes

`subuser`

DescriptionThe subuser ID to be removed.

TypeString

Example`sub_foo`

RequiredYes

`purge-keys`

DescriptionRemove keys belonging to the subuser.

TypeBoolean

ExampleTrue \[True\]

RequiredNo

### Response Entities[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

None.

### Special Error Responses[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

None.

## Create Key[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

Create a new key. If a `subuser` is specified then by default created keys will be swift type. If only one of `access-key` or `secret-key` is provided the committed key will be automatically generated, that is if only `secret-key` is specified then `access-key` will be automatically generated. By default, a generated key is added to the keyring without replacing an existing key pair. If `access-key` is specified and refers to an existing key owned by the user then it will be modified. The response is a container listing all keys of the same type as the key created. Note that when creating a swift key, specifying the option`access-key` will have no effect. Additionally, only one swift key may be held by each user or subuser.

capsusers=write

### Syntax[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

```
PUT /{admin}/user?key&format=json HTTP/1.1
Host {fqdn}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`uid`

DescriptionThe user ID to receive the new key.

TypeString

Example`foo_user`

RequiredYes

`subuser`

DescriptionThe subuser ID to receive the new key.

TypeString

Example`sub_foo`

RequiredNo

`key-type`

DescriptionKey type to be generated, options are: swift, s3 \(default\).

TypeString

Example`s3` \[`s3`\]

RequiredNo

`access-key`

DescriptionSpecify the access key.

TypeString

Example`AB01C2D3EF45G6H7IJ8K`

RequiredNo

`secret-key`

DescriptionSpecify the secret key.

TypeString

Example`0ab/CdeFGhij1klmnopqRSTUv1WxyZabcDEFgHij`

RequiredNo

`generate-key`

DescriptionGenerate a new key pair and add to the existing keyring.

TypeBoolean

ExampleTrue \[`True`\]

RequiredNo

### Response Entities[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`keys`

DescriptionKeys of type created associated with this user account.

TypeContainer

`user`

DescriptionThe user account associated with the key.

TypeString

Parent`keys`

`access-key`

DescriptionThe access key.

TypeString

Parent`keys`

`secret-key`

DescriptionThe secret key

TypeString

Parent`keys`

### Special Error Responses[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`InvalidAccessKey`

DescriptionInvalid access key specified.

Code400 Bad Request

`InvalidKeyType`

DescriptionInvalid key type specified.

Code400 Bad Request

`InvalidSecretKey`

DescriptionInvalid secret key specified.

Code400 Bad Request

`InvalidKeyType`

DescriptionInvalid key type specified.

Code400 Bad Request

`KeyExists`

DescriptionProvided access key exists and belongs to another user.

Code409 Conflict

## Remove Key[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

Remove an existing key.

capsusers=write

### Syntax[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

```
DELETE /{admin}/user?key&format=json HTTP/1.1
Host {fqdn}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`access-key`

DescriptionThe S3 access key belonging to the S3 key pair to remove.

TypeString

Example`AB01C2D3EF45G6H7IJ8K`

RequiredYes

`uid`

DescriptionThe user to remove the key from.

TypeString

Example`foo_user`

RequiredNo

`subuser`

DescriptionThe subuser to remove the key from.

TypeString

Example`sub_foo`

RequiredNo

`key-type`

DescriptionKey type to be removed, options are: swift, s3. NOTE: Required to remove swift key.

TypeString

Example`swift`

RequiredNo

### Special Error Responses[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

None.

### Response Entities[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

None.

## Get Bucket Info[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

Get information about a subset of the existing buckets. If `uid` is specified without `bucket` then all buckets belonging to the user will be returned. If`bucket` alone is specified, information for that particular bucket will be retrieved.

capsbuckets=read

### Syntax[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

```
GET /{admin}/bucket?format=json HTTP/1.1
Host {fqdn}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`bucket`

DescriptionThe bucket to return info on.

TypeString

Example`foo_bucket`

RequiredNo

`uid`

DescriptionThe user to retrieve bucket information for.

TypeString

Example`foo_user`

RequiredNo

`stats`

DescriptionReturn bucket statistics.

TypeBoolean

ExampleTrue \[False\]

RequiredNo

### Response Entities[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

If successful the request returns a buckets container containing the desired bucket information.

`stats`

DescriptionPer bucket information.

TypeContainer

`buckets`

DescriptionContains a list of one or more bucket containers.

TypeContainer

`bucket`

DescriptionContainer for single bucket information.

TypeContainer

Parent`buckets`

`name`

DescriptionThe name of the bucket.

TypeString

Parent`bucket`

`pool`

DescriptionThe pool the bucket is stored in.

TypeString

Parent`bucket`

`id`

DescriptionThe unique bucket id.

TypeString

Parent`bucket`

`marker`

DescriptionInternal bucket tag.

TypeString

Parent`bucket`

`owner`

DescriptionThe user id of the bucket owner.

TypeString

Parent`bucket`

`usage`

DescriptionStorage usage information.

TypeContainer

Parent`bucket`

`index`

DescriptionStatus of bucket index.

TypeString

Parent`bucket`

### Special Error Responses[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`IndexRepairFailed`

DescriptionBucket index repair failed.

Code409 Conflict

## Check Bucket Index[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

Check the index of an existing bucket. NOTE: to check multipart object accounting with `check-objects`, `fix` must be set to True.

capsbuckets=write

### Syntax[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

```
GET /{admin}/bucket?index&format=json HTTP/1.1
Host {fqdn}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`bucket`

DescriptionThe bucket to return info on.

TypeString

Example`foo_bucket`

RequiredYes

`check-objects`

DescriptionCheck multipart object accounting.

TypeBoolean

ExampleTrue \[False\]

RequiredNo

`fix`

DescriptionAlso fix the bucket index when checking.

TypeBoolean

ExampleFalse \[False\]

RequiredNo

### Response Entities[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`index`

DescriptionStatus of bucket index.

TypeString

### Special Error Responses[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`IndexRepairFailed`

DescriptionBucket index repair failed.

Code409 Conflict

## Remove Bucket[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

Delete an existing bucket.

capsbuckets=write

### Syntax[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

```
DELETE /{admin}/bucket?format=json HTTP/1.1
Host {fqdn}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`bucket`

DescriptionThe bucket to remove.

TypeString

Example`foo_bucket`

RequiredYes

`purge-objects`

DescriptionRemove a buckets objects before deletion.

TypeBoolean

ExampleTrue \[False\]

RequiredNo

### Response Entities[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

None.

### Special Error Responses[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`BucketNotEmpty`

DescriptionAttempted to delete non\-empty bucket.

Code409 Conflict

`ObjectRemovalFailed`

DescriptionUnable to remove objects.

Code409 Conflict

## Unlink Bucket[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

Unlink a bucket from a specified user. Primarily useful for changing bucket ownership.

capsbuckets=write

### Syntax[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

```
POST /{admin}/bucket?format=json HTTP/1.1
Host {fqdn}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`bucket`

DescriptionThe bucket to unlink.

TypeString

Example`foo_bucket`

RequiredYes

`uid`

DescriptionThe user ID to unlink the bucket from.

TypeString

Example`foo_user`

RequiredYes

### Response Entities[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

None.

### Special Error Responses[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`BucketUnlinkFailed`

DescriptionUnable to unlink bucket from specified user.

Code409 Conflict

## Link Bucket[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

Link a bucket to a specified user, unlinking the bucket from any previous user.

capsbuckets=write

### Syntax[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

```
PUT /{admin}/bucket?format=json HTTP/1.1
Host {fqdn}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`bucket`

DescriptionThe bucket name to unlink.

TypeString

Example`foo_bucket`

RequiredYes

`bucket-id`

DescriptionThe bucket id to unlink.

TypeString

Example`dev.6607669.420`

RequiredNo

`uid`

DescriptionThe user ID to link the bucket to.

TypeString

Example`foo_user`

RequiredYes

### Response Entities[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`bucket`

DescriptionContainer for single bucket information.

TypeContainer

`name`

DescriptionThe name of the bucket.

TypeString

Parent`bucket`

`pool`

DescriptionThe pool the bucket is stored in.

TypeString

Parent`bucket`

`id`

DescriptionThe unique bucket id.

TypeString

Parent`bucket`

`marker`

DescriptionInternal bucket tag.

TypeString

Parent`bucket`

`owner`

DescriptionThe user id of the bucket owner.

TypeString

Parent`bucket`

`usage`

DescriptionStorage usage information.

TypeContainer

Parent`bucket`

`index`

DescriptionStatus of bucket index.

TypeString

Parent`bucket`

### Special Error Responses[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`BucketUnlinkFailed`

DescriptionUnable to unlink bucket from specified user.

Code409 Conflict

`BucketLinkFailed`

DescriptionUnable to link bucket to specified user.

Code409 Conflict

## Remove Object[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

Remove an existing object. NOTE: Does not require owner to be non\-suspended.

capsbuckets=write

### Syntax[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

```
DELETE /{admin}/bucket?object&format=json HTTP/1.1
Host {fqdn}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`bucket`

DescriptionThe bucket containing the object to be removed.

TypeString

Example`foo_bucket`

RequiredYes

`object`

DescriptionThe object to remove.

TypeString

Example`foo.txt`

RequiredYes

### Response Entities[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

None.

### Special Error Responses[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`NoSuchObject`

DescriptionSpecified object does not exist.

Code404 Not Found

`ObjectRemovalFailed`

DescriptionUnable to remove objects.

Code409 Conflict

## Get Bucket or Object Policy[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

Read the policy of an object or bucket.

capsbuckets=read

### Syntax[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

```
GET /{admin}/bucket?policy&format=json HTTP/1.1
Host {fqdn}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`bucket`

DescriptionThe bucket to read the policy from.

TypeString

Example`foo_bucket`

RequiredYes

`object`

DescriptionThe object to read the policy from.

TypeString

Example`foo.txt`

RequiredNo

### Response Entities[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

If successful, returns the object or bucket policy

`policy`

DescriptionAccess control policy.

TypeContainer

### Special Error Responses[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`IncompleteBody`

DescriptionEither bucket was not specified for a bucket policy request or bucket and object were not specified for an object policy request.

Code400 Bad Request

## Add A User Capability[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

Add an administrative capability to a specified user.

capsusers=write

### Syntax[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

```
PUT /{admin}/user?caps&format=json HTTP/1.1
Host {fqdn}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`uid`

DescriptionThe user ID to add an administrative capability to.

TypeString

Example`foo_user`

RequiredYes

`user-caps`

DescriptionThe administrative capability to add to the user.

TypeString

Example`usage=read,write;user=write`

RequiredYes

### Response Entities[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

If successful, the response contains the user’s capabilities.

`user`

DescriptionA container for the user data information.

TypeContainer

Parent`user`

`user_id`

DescriptionThe user id.

TypeString

Parent`user`

`caps`

DescriptionUser capabilities.

TypeContainer

Parent`user`

### Special Error Responses[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`InvalidCapability`

DescriptionAttempt to grant invalid admin capability.

Code400 Bad Request

### Example Request[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

```
PUT /{admin}/user?caps&user-caps=usage=read,write;user=write&format=json HTTP/1.1
Host: {fqdn}
Content-Type: text/plain
Authorization: {your-authorization-token}
```

## Remove A User Capability[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

Remove an administrative capability from a specified user.

capsusers=write

### Syntax[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

```
DELETE /{admin}/user?caps&format=json HTTP/1.1
Host {fqdn}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`uid`

DescriptionThe user ID to remove an administrative capability from.

TypeString

Example`foo_user`

RequiredYes

`user-caps`

DescriptionThe administrative capabilities to remove from the user.

TypeString

Example`usage=read, write`

RequiredYes

### Response Entities[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

If successful, the response contains the user’s capabilities.

`user`

DescriptionA container for the user data information.

TypeContainer

Parent`user`

`user_id`

DescriptionThe user id.

TypeString

Parent`user`

`caps`

DescriptionUser capabilities.

TypeContainer

Parent`user`

### Special Error Responses[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`InvalidCapability`

DescriptionAttempt to remove an invalid admin capability.

Code400 Bad Request

`NoSuchCap`

DescriptionUser does not possess specified capability.

Code404 Not Found

## Quotas[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

The Admin Operations API enables you to set quotas on users and on bucket owned by users. See [Quota Management](https://docs.ceph.com/en/pacific/radosgw/adminops/) for additional details. Quotas include the maximum number of objects in a bucket and the maximum storage size in megabytes.

To view quotas, the user must have a `users=read` capability. To set, modify or disable a quota, the user must have `users=write` capability. See the [Admin Guide](https://docs.ceph.com/en/pacific/radosgw/adminops/) for details.

Valid parameters for quotas include:

* **Bucket:** The `bucket` option allows you to specify a quota for buckets owned by a user.
* **Maximum Objects:** The `max-objects` setting allows you to specify the maximum number of objects. A negative value disables this setting.
* **Maximum Size:** The `max-size` option allows you to specify a quota for the maximum number of bytes. The `max-size-kb` option allows you to specify it in KiB. A negative value disables this setting.
* **Quota Type:** The `quota-type` option sets the scope for the quota. The options are `bucket` and `user`.
* **Enable/Disable Quota:** The `enabled` option specifies whether the quota should be enabled. The value should be either ‘True’ or ‘False’.

### Get User Quota[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

To get a quota, the user must have `users` capability set with `read`permission.

```
GET /admin/user?quota&uid=<uid>&quota-type=user
```

### Set User Quota[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

To set a quota, the user must have `users` capability set with `write`permission.

```
PUT /admin/user?quota&uid=<uid>&quota-type=user
```

The content must include a JSON representation of the quota settings as encoded in the corresponding read operation.

### Get Bucket Quota[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

To get a quota, the user must have `users` capability set with `read`permission.

```
GET /admin/user?quota&uid=<uid>&quota-type=bucket
```

### Set Bucket Quota[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

To set a quota, the user must have `users` capability set with `write`permission.

```
PUT /admin/user?quota&uid=<uid>&quota-type=bucket
```

The content must include a JSON representation of the quota settings as encoded in the corresponding read operation.

### Set Quota for an Individual Bucket[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

To set a quota, the user must have `buckets` capability set with `write`permission.

```
PUT /admin/bucket?quota&uid=<uid>&bucket=<bucket-name>&quota
```

The content must include a JSON representation of the quota settings as mentioned in Set Bucket Quota section above.

## Standard Error Responses[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`AccessDenied`

DescriptionAccess denied.

Code403 Forbidden

`InternalError`

DescriptionInternal server error.

Code500 Internal Server Error

`NoSuchUser`

DescriptionUser does not exist.

Code404 Not Found

`NoSuchBucket`

DescriptionBucket does not exist.

Code404 Not Found

`NoSuchKey`

DescriptionNo such access key.

Code404 Not Found

## Binding libraries[¶](https://docs.ceph.com/en/pacific/radosgw/adminops/ "Permalink to this headline")

`Golang`

> * [IrekFasikhov/go\-rgwadmin](https://docs.ceph.com/en/pacific/radosgw/adminops/)
> * [QuentinPerez/go\-radosgw](https://docs.ceph.com/en/pacific/radosgw/adminops/)

`Java`

> * [twonote/radosgw\-admin4j](https://docs.ceph.com/en/pacific/radosgw/adminops/)

`Python`

> * [UMIACS/rgwadmin](https://docs.ceph.com/en/pacific/radosgw/adminops/)
> * [valerytschopp/python\-radosgw\-admin](https://docs.ceph.com/en/pacific/radosgw/adminops/)
