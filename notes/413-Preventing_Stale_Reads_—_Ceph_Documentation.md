# 413: Preventing Stale Reads — Ceph Documentation

 # Preventing Stale Reads[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/stale_read/ "Permalink to this headline")

We write synchronously to all replicas before sending an ack to the client, which ensures that we do not introduce potential inconsistency in the write path. However, we only read from one replica, and the client will use whatever OSDMap is has to identify which OSD to read from. In most cases, this is fine: either the client map is correct, or the OSD that we think is the primary for the object knows that it is not the primary anymore, and can feed the client an updated map indicating a newer primary.

They key is to ensure that this is _always_ true. In particular, we need to ensure that an OSD that is fenced off from its peers and has not learned about a map update does not continue to service read requests from similarly stale clients at any point after which a new primary may have been allowed to make a write.

We accomplish this via a mechanism that works much like a read lease. Each pool may have a `read_lease_interval` property which defines how long this is, although by default we simply set it to`osd_pool_default_read_lease_ratio` \(default: .8\) times the`osd_heartbeat_grace`. \(This way the lease will generally have expired by the time we mark a failed OSD down.\)

## readable\_until[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/stale_read/ "Permalink to this headline")

Primary and replica both track a couple of values:

* _readable\_until_ is how long we are allowed to service \(read\) requests before _our_ “lease” expires.
* _readable\_until\_ub_ is an upper bound on _readable\_until_ for any OSD in the acting set.

The primary manages these two values by sending _pg\_lease\_t_ messages to replicas that increase the upper bound. Once all acting OSDs have acknowledged they’ve seen the higher bound, the primary increases its own _readable\_until_ and shares that \(in a subsequent _pg\_lease\_t_message\). The resulting invariant is that any acting OSDs’_readable\_until_ is always \<= any acting OSDs’ _readable\_until\_ub_.

In order to avoid any problems with clock skew, we use monotonic clocks \(which are only accurate locally and unaffected by time adjustments\) throughout to manage these leases. Peer OSDs calculate upper and lower bounds on the deltas between OSD\-local clocks, allowing the primary to share timestamps based on its local clock while replicas translate that to an appropriate bound in for their own local clocks.

## Prior Intervals[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/stale_read/ "Permalink to this headline")

Whenever there is an interval change, we need to have an upper bound on the _readable\_until_ values for any OSDs in the prior interval. All OSDs from that interval have this value \(_readable\_until\_ub_\), and share it as part of the pg\_history\_t during peering.

Because peering may involve OSDs that were not already communicating before and may not have bounds on their clock deltas, the bound in_pg\_history\_t_ is shared as a simple duration before the upper bound expires. This means that the bound slips forward in time due to the transit time for the peering message, but that is generally quite short, and moving the bound later in time is safe since it is an_upper_ bound.

## PG “laggy” state[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/stale_read/ "Permalink to this headline")

While the PG is active, _pg\_lease\_t_ and _pg\_lease\_ack\_t_ messages are regularly exchanged. However, if a client request comes in and the lease has expired \(_readable\_until_ has passed\), the PG will go into a_LAGGY_ state and request will be blocked. Once the lease is renewed, the request\(s\) will be requeued.

## PG “wait” state[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/stale_read/ "Permalink to this headline")

If peering completes but the prior interval’s OSDs may still be readable, the PG will go into the _WAIT_ state until sufficient time has passed. Any OSD requests will block during that period. Recovery may proceed while in this state, since the logical, user\-visible content of objects does not change.

## Dead OSDs[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/stale_read/ "Permalink to this headline")

Generally speaking, we need to wait until prior intervals’ OSDs _know_that they should no longer be readable. If an OSD is known to have crashed \(e.g., because the process is no longer running, which we may infer because we get a ECONNREFUSED error\), then we can infer that it is not readable.

Similarly, if an OSD is marked down, gets a map update telling it so, and then informs the monitor that it knows it was marked down, we can similarly infer that it is not still serving requests for a prior interval.

When a PG is in the _WAIT_ state, it will watch new maps for OSDs’_dead\_epoch_ value indicating they are aware of their dead\-ness. If all down OSDs from prior interval are so aware, we can exit the WAIT state early.
