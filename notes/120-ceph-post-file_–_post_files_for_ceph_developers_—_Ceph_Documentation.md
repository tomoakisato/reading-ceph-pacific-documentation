# 120: ceph-post-file – post files for ceph developers — Ceph Documentation

 # ceph\-post\-file – post files for ceph developers[¶](https://docs.ceph.com/en/pacific/man/8/ceph-post-file/ "Permalink to this headline")

## Synopsis[¶](https://docs.ceph.com/en/pacific/man/8/ceph-post-file/ "Permalink to this headline")

**ceph\-post\-file** \[\-d _description\] \[\-u \*user_\] _file or dir_ …

## Description[¶](https://docs.ceph.com/en/pacific/man/8/ceph-post-file/ "Permalink to this headline")

**ceph\-post\-file** will upload files or directories to ceph.com for later analysis by Ceph developers.

Each invocation uploads files or directories to a separate directory with a unique tag. That tag can be passed to a developer or referenced in a bug report \([http://tracker.ceph.com/](https://docs.ceph.com/en/pacific/man/8/ceph-post-file/)\). Once the upload completes, the directory is marked non\-readable and non\-writeable to prevent access or modification by other users.

## Warning[¶](https://docs.ceph.com/en/pacific/man/8/ceph-post-file/ "Permalink to this headline")

Basic measures are taken to make posted data be visible only to developers with access to ceph.com infrastructure. However, users should think twice and/or take appropriate precautions before posting potentially sensitive data \(for example, logs or data directories that contain Ceph secrets\).

## Options[¶](https://docs.ceph.com/en/pacific/man/8/ceph-post-file/ "Permalink to this headline")

`-d`` *description*``, ``--description`` *description*`[¶](https://docs.ceph.com/en/pacific/man/8/ceph-post-file/ "Permalink to this definition")Add a short description for the upload. This is a good opportunity to reference a bug number. There is no default value.

`-u`` *user*`[¶](https://docs.ceph.com/en/pacific/man/8/ceph-post-file/ "Permalink to this definition")Set the user metadata for the upload. This defaults to whoami\`@\`hostname \-f.

## Examples[¶](https://docs.ceph.com/en/pacific/man/8/ceph-post-file/ "Permalink to this headline")

To upload a single log:

```
ceph-post-file /var/log/ceph/ceph-mon.`hostname`.log
```

To upload several directories:

```
ceph-post-file -d 'mon data directories' /var/log/ceph/mon/*
```

## Availability[¶](https://docs.ceph.com/en/pacific/man/8/ceph-post-file/ "Permalink to this headline")

**ceph\-post\-file** is part of Ceph, a massively scalable, open\-source, distributed storage system. Please refer to the Ceph documentation at [http://ceph.com/docs](https://docs.ceph.com/en/pacific/man/8/ceph-post-file/) for more information.

## See also[¶](https://docs.ceph.com/en/pacific/man/8/ceph-post-file/ "Permalink to this headline")

[ceph](https://docs.ceph.com/en/pacific/man/8/ceph-post-file/)\(8\),[ceph\-debugpack](https://docs.ceph.com/en/pacific/man/8/ceph-post-file/)\(8\),
