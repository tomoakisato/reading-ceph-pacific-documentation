# 194: Kernel Module Operations

**クリップソース:** [194: Kernel Module Operations — Ceph Documentation](https://docs.ceph.com/en/pacific/rbd/rbd-ko/)

# Kernel Module Operations[¶](https://docs.ceph.com/en/pacific/rbd/rbd-ko/ "Permalink to this headline")

重要: カーネルモジュール操作を使用するには、Cephクラスタが稼動している必要があります。

## Get a List of Images[¶](https://docs.ceph.com/en/pacific/rbd/rbd-ko/ "Permalink to this headline")

ブロックデバイスのイメージをマウントするには、まずイメージのリストを返します。

```
rbd list
```

## Map a Block Device[¶](https://docs.ceph.com/en/pacific/rbd/rbd-ko/ "Permalink to this headline")

イメージ名とカーネルモジュールの対応付けには、rbdを使用します。イメージ名、プール名、ユーザ名を指定する必要があります。rbdはRBDカーネルモジュールがまだロードされていない場合、代わりにロードします。

```
sudo rbd device map {pool-name}/{image-name} --id {user-name}
```

For example:

```
sudo rbd device map rbd/myimage --id admin
```

cephx認証を使用する場合、シークレットも指定する必要があります。 これはキーリングで指定する場合もあれば、シークレットが含まれるファイルで指定する場合もあります。

```
sudo rbd device map rbd/myimage --id admin --keyring /path/to/keyring
sudo rbd device map rbd/myimage --id admin --keyfile /path/to/file
```

## Show Mapped Block Devices[¶](https://docs.ceph.com/en/pacific/rbd/rbd-ko/ "Permalink to this headline")

rbdでカーネルモジュールにマッピングされたブロックデバイスイメージを表示するには、device list引数を指定します。

```
rbd device list
```

## Unmapping a Block Device[¶](https://docs.ceph.com/en/pacific/rbd/rbd-ko/ "Permalink to this headline")

rbdコマンドでブロックデバイス・イメージをアンマップするには、device unmap引数とデバイス名（慣習的にブロックデバイス・イメージ名と同じ）を指定します。

```
sudo rbd device unmap /dev/rbd/{poolname}/{imagename}
```

For example:

```
sudo rbd device unmap /dev/rbd/rbd/foo
```
