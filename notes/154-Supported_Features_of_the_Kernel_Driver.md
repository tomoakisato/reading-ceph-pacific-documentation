# 154: Supported Features of the Kernel Driver

**クリップソース:** [154: Supported Features of the Kernel Driver — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/kernel-features/)

# Supported Features of the Kernel Driver[¶](https://docs.ceph.com/en/pacific/cephfs/kernel-features/ "Permalink to this headline")

カーネルドライバは、cephのコアコードとは別に開発されているため、FUSEドライバとは機能の実装が異なる場合があります。以下では、カーネルドライバにおける各種CephFS機能の実装状況について詳しく説明します。

## Inline data[¶](https://docs.ceph.com/en/pacific/cephfs/kernel-features/ "Permalink to this headline")

インラインデータはFireflyのリリースで導入されました。この機能はメインラインのCephFSでは非推奨となっており、将来のカーネルリリースで削除される可能性があります。

Linux カーネルクライアント \>= 3.19 では、インラインデータの読み込みと、ファイルデータの変更時に既存のインラインデータを RADOS オブジェクトに変換することができます。現在、Linuxカーネルクライアントは、ファイルデータをインラインデータとして保存しません。

詳しくは、「[実験的機能](https://docs.ceph.com/en/pacific/cephfs/experimental-features)」をご覧ください。

## Quotas[¶](https://docs.ceph.com/en/pacific/cephfs/kernel-features/ "Permalink to this headline")

クォータはhammerリリースで初めて導入されました。QuotaのディスクフォーマットはMimicリリースで更新されました。4.17以降のLinuxカーネルクライアントは新しいフォーマットのquotaをサポートすることができます。現在、どのLinuxカーネルクライアントも古い形式のquotaをサポートしていません。

詳しくは[クォータ](https://docs.ceph.com/en/pacific/cephfs/quota)をご覧ください。

## Multiple file systems within a Ceph cluster[¶](https://docs.ceph.com/en/pacific/cephfs/kernel-features/ "Permalink to this headline")

この機能はJewelのリリースで導入されました。Linux カーネルクライアント 4.7 以降でサポート可能です。

詳しくは、「[実験的機能](https://docs.ceph.com/en/pacific/cephfs/experimental-features)」をご覧ください。

## Multiple active metadata servers[¶](https://docs.ceph.com/en/pacific/cephfs/kernel-features/ "Permalink to this headline")

この機能は、Luminousのリリースからサポートされています。複数のアクティブなMDSがある場合、Linux カーネルクライアント \>= 4.14 を使用することが推奨されます。

## Snapshots[¶](https://docs.ceph.com/en/pacific/cephfs/kernel-features/ "Permalink to this headline")

この機能はMimicのリリースからサポートされています。スナップショットを使用する場合は、Linux カーネルクライアント \>= 4.17 を使用することが推奨されます。
