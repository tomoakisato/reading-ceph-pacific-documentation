# 305: Hello World Module

**クリップソース:** [305: Hello World Module — Ceph Documentation](https://docs.ceph.com/en/pacific/mgr/hello/)

# Hello World Module[¶](https://docs.ceph.com/en/pacific/mgr/hello/ "Permalink to this headline")

This is a simple module skeleton for documentation purposes.

## Enabling[¶](https://docs.ceph.com/en/pacific/mgr/hello/ "Permalink to this headline")

helloモジュールを有効にします：

```
ceph mgr module enable hello
```

有効になっていることを確認するには：

```
ceph mgr module ls
```

モジュールファイル（src/pybind/mgr/hello/module.py）を編集した後、実行することで変更を確認することができます：

```
ceph mgr module disable hello
ceph mgr module enable hello
```

or:

```
init-ceph restart mgr
```

モジュールを実行するには、以下を実行します：

```
ceph hello
```

ログは以下にあります：

```
build/out/mgr.x.log
```

## Documenting[¶](https://docs.ceph.com/en/pacific/mgr/hello/ "Permalink to this headline")

新しいmgrモジュールを追加したら、必ずそのドキュメントをdoc/mgr/module\_name.rstに追加してください。また、新しいモジュールへのリンクを doc/mgr/index.rst に追加してください。
