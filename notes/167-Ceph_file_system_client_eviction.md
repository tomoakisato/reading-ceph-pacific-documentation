# 167: Ceph file system client eviction

**クリップソース:** [167: Ceph file system client eviction — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/eviction/)

# Ceph file system client eviction[¶](https://docs.ceph.com/en/pacific/cephfs/eviction/ "Permalink to this headline")

ファイルシステムクライアントが応答しないなど挙動不審な場合、ファイルシステムへのアクセスを強制的に終了させることが必要な場合があります。 このプロセスは、退去（eviction）と呼ばれます。

CephFSクライアントを退去させると、MDSデーモンおよびOSDデーモンとそれ以上の通信ができなくなります。 クライアントがファイルシステムに対してバッファリングI/Oを実行していた場合、フラッシュされていないデータは失われます。

クライアントは（MDSとの通信に失敗した場合）自動的に、またはシステム管理者が手動で退去させることができます。

クライアントの退去処理は、あらゆる種類のクライアントに適用されます。これには、FUSEマウント、カーネルマウント、nfs\-ganeshaゲートウェイ、libcephfsを使用するプロセスが含まれます。

## Automatic client eviction[¶](https://docs.ceph.com/en/pacific/cephfs/eviction/ "Permalink to this headline")

クライアントが自動的に退去させられる状況は、3つあります。

1. activeなMDSデーモンにおいて、クライアントがファイルシステム変数session\_autoclose秒（デフォルトでは300秒）を超えてMDSと通信していない場合、自動的に退去させられます。
2. activeなMDSデーモンにおいて、クライアントが構成オプションmds\_cap\_revoke\_eviction\_timeout秒を超えてCAPS取り消しメッセージに応答しなかった場合。 デフォルトで無効化されます。
3. MDSの起動時（フェイルオーバー時を含む）、MDSは再接続と呼ばれる状態を通過します。 この状態では、すべてのクライアントが新しいMDSデーモンに接続するのを待ちます。 時間（mds\_reconnect\_timeout、デフォルトでは45秒）内に接続できないクライアントがあった場合、そのクライアントは退去させられます。

上記の状況のいずれかが発生した場合、クラスタログに警告メッセージが送信されます。

## Manual client eviction[¶](https://docs.ceph.com/en/pacific/cephfs/eviction/ "Permalink to this headline")

管理者がクライアントを手動で退去させたいと思うことがあります。 これは、クライアントが死亡し、管理者がそのセッションがタイムアウトするのを待ちたくない場合、またはクライアントが誤動作しており、管理者がクライアントノードにアクセスできずアンマウントできない場合などがあります。

まず、クライアントのリストを検査することが有効です：

```
ceph tell mds.0 client ls

[
    {
        "id": 4305,
        "num_leases": 0,
        "num_caps": 3,
        "state": "open",
        "replay_requests": 0,
        "completed_requests": 0,
        "reconnecting": false,
        "inst": "client.4305 172.21.9.34:0/422650892",
        "client_metadata": {
            "ceph_sha1": "ae81e49d369875ac8b569ff3e3c456a31b8f3af5",
            "ceph_version": "ceph version 12.0.0-1934-gae81e49 (ae81e49d369875ac8b569ff3e3c456a31b8f3af5)",
            "entity_id": "0",
            "hostname": "senta04",
            "mount_point": "/tmp/tmpcMpF1b/mnt.0",
            "pid": "29377",
            "root": "/"
        }
    }
]
```

退去させたいクライアントを特定したら、そのユニークなID、または他の属性を使用してそれを識別することができます：

```
# These all work
ceph tell mds.0 client evict id=4305
ceph tell mds.0 client evict client_metadata.=4305
```

## Advanced: Un\-blocklisting a client[¶](https://docs.ceph.com/en/pacific/cephfs/eviction/ "Permalink to this headline")

通常、ブロックリストに登録されたクライアントは、サーバーに再接続することができません。アンマウントしてから、新たにマウントする必要があります。

しかし、状況によっては、退去させられたクライアントが再接続を試みることを許可することが有用な場合があります。

CephFSはRADOS OSDブロックリストを使用してクライアントの退去を制御しているため、ブロックリストから削除することでCephFSクライアントの再接続を許可することができます。

```
$ ceph osd blocklist ls
listed 1 entries
127.0.0.1:0/3710147553 2018-03-19 11:32:24.716146
$ ceph osd blocklist rm 127.0.0.1:0/3710147553
un-blocklisting 127.0.0.1:0/3710147553
```

これを行うと、ブロックリスト上のクライアントがバッファリングI/Oを実行していたファイルに他のクライアントがアクセスした場合に、データ整合性が危険にさらされる可能性があります。 また、完全に機能するクライアントになることも保証されていません。退去後に健全なクライアントを取り戻す最良の方法は、クライアントをアンマウントして、新たにマウントすることです。

この方法でクライアントの再接続を試みる場合、FUSEクライアントでclient\_reconnect\_staleをtrueに設定し、クライアントに再接続を試みるよう促すことも有用です。

## Advanced: Configuring blocklisting[¶](https://docs.ceph.com/en/pacific/cephfs/eviction/ "Permalink to this headline")

クライアントホストの速度が遅い、またはネットワークの信頼性が低いなどの理由でクライアントの強制退去が頻繁に発生し、根本的な問題を解決できない場合、MDSに厳しさを緩和するよう依頼することができます。

遅いクライアントに対しては、単にMDSのセッションをドロップすることで対応することができますが、クライアントがセッションを再び開くことを許可し、OSDとの通信を継続することを許可します。 このモードを有効にするには、MDSノードでmds\_session\_blocklist\_on\_timeoutをfalseに設定します。

手動退去の場合に同等の動作をさせるには、 mds\_session\_blocklist\_on\_evict を false に設定します。

ブロックリストが無効な場合、クライアント退去は退去コマンドを送信したMDSに対してのみ効果があることに注意してください。 複数のactive MDSデーモンが存在するシステムでは、各active デーモンに退去コマンドを送信する必要があります。 ブロックリストが有効な場合（デフォルト）、ブロックリストは他のMDSにも伝達されるため、1つのMDSに退去コマンドを送信するだけで十分です。

## Background: Blocklisting and OSD epoch barrier[¶](https://docs.ceph.com/en/pacific/cephfs/eviction/ "Permalink to this headline")

クライアントがブロックリストに登録された後、他のクライアントやMDSデーモンがブロックリストエントリーを含む最新のOSDMapを持っていることを確認してから、ブロックリストに登録されたクライアントがアクセスしていたかもしれないデータオブジェクトにアクセスしようとする必要があります。

これは、内部の「osdmap epoch barrier」機構を利用して確保されます。

バリアの目的は、同じRADOSオブジェクトに触れることを可能にするCAPSを渡すクライアントが、ENOSPCでキャンセルされた操作、または退去でブロックリストに登録されたクライアントと競合しないように、最近のOSDマップを持っていることを保証することです。

具体的には、エポックバリアが設定されている場合は：

* クライアント退去（当該クライアントがブロックリストに登録され、他のクライアントが同じオブジェクトに触れるにはブロックリスト後のエポックを待つ必要がある）
* クライアントにおけるOSDマップのFULLフラグ処理（当該クライアントがFULLエポック以前のOSD OPSをキャンセルすることがあるため、他のクライアントは同じオブジェクトに触れる前にFULLエポック以降まで待つ必要がある）
* MDS起動時（バリアエポックを保持しないため、再起動後は常に最新のOSDマップが必要）

これはグローバルな値であることに注意してください。これをノード単位で管理することもできますが、以下の理由でそうしません：

* もっと複雑になってしまう
* inodeごとに4バイトのメモリを余分に使うことになる
* ほとんどの場合、誰もが最新のOSDマップを持っているので、効率的ではない。そして、誰もが待つよりもバリアを通り抜ける
* このバリアは非常に稀なケースで行われるため、ノード単位の粒度がもたらすメリットは非常に稀

エポックバリアはすべてのCAPSメッセージと一緒に送信され、メッセージの受信者に、このOSDエポックを見るまでOSDへのRADOS操作をそれ以上送信しないように指示します。 これは主に（ファイルへのデータ書き込みを行う）クライアントに適用されますが、ファイルサイズの調査やファイルの削除などはMDSから行われるため、MDSにも適用されます。
