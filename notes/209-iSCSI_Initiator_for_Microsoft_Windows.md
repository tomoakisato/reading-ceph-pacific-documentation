# 209: iSCSI Initiator for Microsoft Windows

**クリップソース:** [209: iSCSI Initiator for Microsoft Windows — Ceph Documentation](https://docs.ceph.com/en/pacific/rbd/iscsi-initiator-win/)

# iSCSI Initiator for Microsoft Windows[¶](https://docs.ceph.com/en/pacific/rbd/iscsi-initiator-win/ "Permalink to this headline")

**Prerequisite:**

* Microsoft Windows Server 2016 or later

**iSCSI Initiator, Discovery and Setup:**

1. iSCSIイニシエータドライバとMPIOツールをインストールします。
2. MPIOプログラムを起動し、「Discover Multi\-Paths」タブをクリックし、「Add support for iSCSI devices」にチェックを入れて、「Add」をクリックします。この際、再起動が必要です。
3. iSCSI Initiator Propertiesウィンドウの「Discovery」タブで、ターゲットポータルを追加します。Ceph iSCSIゲートウェイのIPアドレスまたはDNS名とPortを入力します。
4. 「Targets」タブで、ターゲットを選択し、「Connect」をクリックします。
5. 「Connect To Target」ウィンドウで、「Enable multi\-path」オプションを選択し、「Advanced」ボタンをクリックします。
6. 「Connet using」セクションで、「Target portal IP」を選択します。「Enable CHAP login on」を選択し、「Ceph iSCSI Ansible client credentials」セクションから「Name」と「Target secret」の値を入力し、「OK」をクリックします。
7. iSCSIゲートウェイのセットアップ時に定義した各ターゲットポータルについて、ステップ5と6を繰り返します。

**Multipath IO Setup:**

MPIOのロードバランシングポリシーの設定、タイムアウトやリトライオプションの設定は、PowerShellのmpclaimコマンドを使用します。残りは iSCSI Initiator toolで行います。

注：PowerShellからPDORemovePeriodオプションを120秒に増やすことが推奨されています。この値は、アプリケーションに応じて調整する必要がある場合があります。すべてのパスがダウンし、120秒が経過すると、オペレーティングシステムはIOリクエストに失敗するようになります。

```
Set-MPIOSetting -NewPDORemovePeriod 120
```

```
mpclaim.exe -l -m 1
```

```
mpclaim -s -m
MSDSM-wide Load Balance Policy: Fail Over Only
```

1. iSCSI Initiator ツールを使用して、「Targets」タブから、「Devices…」ボタンをクリックします。
2. Devicesウィンドウからディスクを選択し、「MPIO...」ボタンをクリックします。
3. 「Device Details」ウィンドウに、各ターゲットポータルへのパスが表示されます。ceph\-ansibleセットアップ方法を使用する場合、iSCSIゲートウェイはALUAを使用してiSCSIイニシエータにどのパスとiSCSIゲートウェイをプライマリパスとして使用すべきかを知らせます。ロードバランシングポリシー 「Fail Over Only」を選択する必要があります。

```
mpclaim -s -d $MPIO_DISK_ID
```

注：ceph\-ansibleセットアップ方式では、LUNを所有するiSCSIゲートウェイノードへのパスであるActive/Optimizedパスが1つ、その他のiSCSIゲートウェイノードにはActive/Unoptimizedパスが1つ存在することになります。

**Tuning:**

以下のレジストリ設定を使用することを検討してください：

* Windows Disk Timeout

```
HKEY_LOCAL_MACHINE\System\CurrentControlSet\Services\Disk
```

```
TimeOutValue = 65
```

* Microsoft iSCSI Initiator Driver

```
HKEY_LOCAL_MACHINE\\SYSTEM\CurrentControlSet\Control\Class\{4D36E97B-E325-11CE-BFC1-08002BE10318}\<Instance_Number>\Parameters
```

```
LinkDownTime = 25
SRBTimeoutDelta = 15
```
