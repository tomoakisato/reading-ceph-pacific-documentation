# 214: rbd – manage rados block device (RBD) images

 # rbd – manage rados block device \(RBD\) images[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this headline")

## Synopsis[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this headline")

**rbd** \[ \-c _ceph.conf_ \] \[ \-m _monaddr_ \] \[–cluster _cluster\-name_\] \[ \-p | –pool _pool_ \] \[ _command_ … \]

## Description[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this headline")

**rbd** is a utility for manipulating rados block device \(RBD\) images, used by the Linux rbd driver and the rbd storage driver for QEMU/KVM. RBD images are simple block devices that are striped over objects and stored in a RADOS object store. The size of the objects the image is striped over must be a power of two.

## Options[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this headline")

`-c`` ceph.conf``, ``--conf`` ceph.conf`[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this definition")Use ceph.conf configuration file instead of the default /etc/ceph/ceph.conf to determine monitor addresses during startup.

`-m`` monaddress[:port]`[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this definition")Connect to specified monitor \(instead of looking through ceph.conf\).

`--cluster`` cluster-name`[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this definition")Use different cluster name as compared to default cluster name _ceph_.

`-p`` pool-name``, ``--pool`` pool-name`[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this definition")Interact with the given pool. Required by most commands.

`--namespace`` namespace-name`[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this definition")Use a pre\-defined image namespace within a pool

`--no-progress```[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this definition")Do not output progress information \(goes to standard error by default for some commands\).

## Parameters[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this headline")

`--image-format`` format-id`[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this definition")Specifies which object layout to use. The default is 2.

* format 1 \- \(deprecated\) Use the original format for a new rbd image. This format is understood by all versions of librbd and the kernel rbd module, but does not support newer features like cloning.
* format 2 \- Use the second rbd format, which is supported by librbd since the Bobtail release and the kernel rbd module since kernel 3.10 \(except for “fancy” striping, which is supported since kernel 4.17\). This adds support for cloning and is more easily extensible to allow more features in the future.

`-s`` size-in-M/G/T``, ``--size`` size-in-M/G/T`[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this definition")Specifies the size of the new rbd image or the new size of the existing rbd image in M/G/T. If no suffix is given, unit M is assumed.

`--object-size`` size-in-B/K/M`[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this definition")Specifies the object size in B/K/M. Object size will be rounded up the nearest power of two; if no suffix is given, unit B is assumed. The default object size is 4M, smallest is 4K and maximum is 32M.

`--stripe-unit`` size-in-B/K/M`[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this definition")Specifies the stripe unit size in B/K/M. If no suffix is given, unit B is assumed. See striping section \(below\) for more details.

`--stripe-count`` num`[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this definition")Specifies the number of objects to stripe over before looping back to the first object. See striping section \(below\) for more details.

`--snap`` snap`[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this definition")Specifies the snapshot name for the specific operation.

`--id`` username`[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this definition")Specifies the username \(without the `client.` prefix\) to use with the map command.

`--keyring`` filename`[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this definition")Specifies a keyring file containing a secret for the specified user to use with the map command. If not specified, the default keyring locations will be searched.

`--keyfile`` filename`[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this definition")Specifies a file containing the secret key of `--id user` to use with the map command. This option is overridden by `--keyring` if the latter is also specified.

`--shared`` lock-tag`[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this definition")Option for lock add that allows multiple clients to lock the same image if they use the same tag. The tag is an arbitrary string. This is useful for situations where an image must be open from more than one client at once, like during live migration of a virtual machine, or for use underneath a clustered file system.

`--format`` format`[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this definition")Specifies output formatting \(default: plain, json, xml\)

`--pretty-format```[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this definition")Make json or xml formatted output more human\-readable.

`-o`` krbd-options``, ``--options`` krbd-options`[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this definition")Specifies which options to use when mapping or unmapping an image via the rbd kernel driver. krbd\-options is a comma\-separated list of options \(similar to mount\(8\) mount options\). See kernel rbd \(krbd\) options section below for more details.

`--read-only```[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this definition")Map the image read\-only. Equivalent to \-o ro.

`--image-feature`` feature-name`[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this definition")Specifies which RBD format 2 feature should be enabled when creating an image. Multiple features can be enabled by repeating this option multiple times. The following features are supported:

* layering: layering support
* striping: striping v2 support
* exclusive\-lock: exclusive locking support
* object\-map: object map support \(requires exclusive\-lock\)
* fast\-diff: fast diff calculations \(requires object\-map\)
* deep\-flatten: snapshot flatten support
* journaling: journaled IO support \(requires exclusive\-lock\)
* data\-pool: erasure coded pool support

`--image-shared```[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this definition")Specifies that the image will be used concurrently by multiple clients. This will disable features that are dependent upon exclusive ownership of the image.

`--whole-object```[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this definition")Specifies that the diff should be limited to the extents of a full object instead of showing intra\-object deltas. When the object map feature is enabled on an image, limiting the diff to the object extents will dramatically improve performance since the differences can be computed by examining the in\-memory object map instead of querying RADOS for each object within the image.

`--limit```[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this definition")Specifies the limit for the number of snapshots permitted.

## Commands[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this headline")

**bench** –io\-type \<read | write | readwrite | rw\> \[–io\-size _size\-in\-B/K/M/G/T_\] \[–io\-threads _num\-ios\-in\-flight_\] \[–io\-total _size\-in\-B/K/M/G/T_\] \[–io\-pattern seq | rand\] \[–rw\-mix\-read _read proportion in readwrite_\] _image\-spec_Generate a series of IOs to the image and measure the IO throughput and latency. If no suffix is given, unit B is assumed for both –io\-size and –io\-total. Defaults are: –io\-size 4096, –io\-threads 16, –io\-total 1G, –io\-pattern seq, –rw\-mix\-read 50.

**children** _snap\-spec_List the clones of the image at the given snapshot. This checks every pool, and outputs the resulting poolname/imagename.

This requires image format 2.

**clone** \[–object\-size _size\-in\-B/K/M_\] \[–stripe\-unit _size\-in\-B/K/M_ –stripe\-count _num_\] \[–image\-feature _feature\-name_\] \[–image\-shared\] _parent\-snap\-spec_ _child\-image\-spec_Will create a clone \(copy\-on\-write child\) of the parent snapshot. Object size will be identical to that of the parent image unless specified. Size will be the same as the parent snapshot. The –stripe\-unit and –stripe\-count arguments are optional, but must be used together.

The parent snapshot must be protected \(see rbd snap protect\). This requires image format 2.

**config global get** _config\-entity_ _key_Get a global\-level configuration override.

**config global list** \[–format plain | json | xml\] \[–pretty\-format\] _config\-entity_List global\-level configuration overrides.

**config global set** _config\-entity_ _key_ _value_Set a global\-level configuration override.

**config global remove** _config\-entity_ _key_Remove a global\-level configuration override.

**config image get** _image\-spec_ _key_Get an image\-level configuration override.

**config image list** \[–format plain | json | xml\] \[–pretty\-format\] _image\-spec_List image\-level configuration overrides.

**config image set** _image\-spec_ _key_ _value_Set an image\-level configuration override.

**config image remove** _image\-spec_ _key_Remove an image\-level configuration override.

**config pool get** _pool\-name_ _key_Get a pool\-level configuration override.

**config pool list** \[–format plain | json | xml\] \[–pretty\-format\] _pool\-name_List pool\-level configuration overrides.

**config pool set** _pool\-name_ _key_ _value_Set a pool\-level configuration override.

**config pool remove** _pool\-name_ _key_Remove a pool\-level configuration override.

**cp** \(_src\-image\-spec_ | _src\-snap\-spec_\) _dest\-image\-spec_Copy the content of a src\-image into the newly created dest\-image. dest\-image will have the same size, object size, and image format as src\-image.

**create** \(\-s | –size _size\-in\-M/G/T_\) \[–image\-format _format\-id_\] \[–object\-size _size\-in\-B/K/M_\] \[–stripe\-unit _size\-in\-B/K/M_ –stripe\-count _num_\] \[–thick\-provision\] \[–no\-progress\] \[–image\-feature _feature\-name_\]… \[–image\-shared\] _image\-spec_Will create a new rbd image. You must also specify the size via –size. The –stripe\-unit and –stripe\-count arguments are optional, but must be used together. If the –thick\-provision is enabled, it will fully allocate storage for the image at creation time. It will take a long time to do. Note: thick provisioning requires zeroing the contents of the entire image.

**deep cp** \(_src\-image\-spec_ | _src\-snap\-spec_\) _dest\-image\-spec_Deep copy the content of a src\-image into the newly created dest\-image. Dest\-image will have the same size, object size, image format, and snapshots as src\-image.

**device list** \[\-t | –device\-type _device\-type_\] \[–format plain | json | xml\] –pretty\-formatShow the rbd images that are mapped via the rbd kernel module \(default\) or other supported device.

**device map** \[\-t | –device\-type _device\-type_\] \[–read\-only\] \[–exclusive\] \[\-o | –options _device\-options_\] _image\-spec_ | _snap\-spec_Map the specified image to a block device via the rbd kernel module \(default\) or other supported device \(_nbd_ on Linux or _ggate_ on FreeBSD\).

The –options argument is a comma separated list of device type specific options \(opt1,opt2=val,…\).

**device unmap** \[\-t | –device\-type _device\-type_\] \[\-o | –options _device\-options_\] _image\-spec_ | _snap\-spec_ | _device\-path_Unmap the block device that was mapped via the rbd kernel module \(default\) or other supported device.

The –options argument is a comma separated list of device type specific options \(opt1,opt2=val,…\).

**diff** \[–from\-snap _snap\-name_\] \[–whole\-object\] _image\-spec_ | _snap\-spec_Dump a list of byte extents in the image that have changed since the specified start snapshot, or since the image was created. Each output line includes the starting offset \(in bytes\), the length of the region \(in bytes\), and either ‘zero’ or ‘data’ to indicate whether the region is known to be zeros or may contain other data.

**du** \[\-p | –pool _pool\-name_\] \[_image\-spec_ | _snap\-spec_\] \[–merge\-snapshots\]Will calculate the provisioned and actual disk usage of all images and associated snapshots within the specified pool. It can also be used against individual images and snapshots.

If the RBD fast\-diff feature is not enabled on images, this operation will require querying the OSDs for every potential object within the image.

The –merge\-snapshots will merge snapshots used space into their parent images.

**encryption format** _image\-spec_ _format_ _passphrase\-file_ \[–cipher\-alg _alg_\]Formats image to an encrypted format. All data previously written to the image will become unreadable. A cloned image cannot be formatted, although encrypted images can be cloned. Supported formats: _luks1_, _luks2_. Supported cipher algorithms: _aes\-128_, _aes\-256_ \(default\).

**export** \[–export\-format _format \(1 or 2\)_\] \(_image\-spec_ | _snap\-spec_\) \[_dest\-path_\]Export image to dest path \(use \- for stdout\). The –export\-format accepts ‘1’ or ‘2’ currently. Format 2 allow us to export not only the content of image, but also the snapshots and other properties, such as image\_order, features.

**export\-diff** \[–from\-snap _snap\-name_\] \[–whole\-object\] \(_image\-spec_ | _snap\-spec_\) _dest\-path_Export an incremental diff for an image to dest path \(use \- for stdout\). If an initial snapshot is specified, only changes since that snapshot are included; otherwise, any regions of the image that contain data are included. The end snapshot is specified using the standard –snap option or @snap syntax \(see below\). The image diff format includes metadata about image size changes, and the start and end snapshots. It efficiently represents discarded or ‘zero’ regions of the image.

**feature disable** _image\-spec_ _feature\-name_…Disable the specified feature on the specified image. Multiple features can be specified.

**feature enable** _image\-spec_ _feature\-name_…Enable the specified feature on the specified image. Multiple features can be specified.

**flatten** _image\-spec_If image is a clone, copy all shared blocks from the parent snapshot and make the child independent of the parent, severing the link between parent snap and child. The parent snapshot can be unprotected and deleted if it has no further dependent clones.

This requires image format 2.

**group create** _group\-spec_Create a group.

**group image add** _group\-spec_ _image\-spec_Add an image to a group.

**group image list** _group\-spec_List images in a group.

**group image remove** _group\-spec_ _image\-spec_Remove an image from a group.

**group ls** \[\-p | –pool _pool\-name_\]List rbd groups.

**group rename** _src\-group\-spec_ _dest\-group\-spec_Rename a group. Note: rename across pools is not supported.

**group rm** _group\-spec_Delete a group.

**group snap create** _group\-snap\-spec_Make a snapshot of a group.

**group snap list** _group\-spec_List snapshots of a group.

**group snap rm** _group\-snap\-spec_Remove a snapshot from a group.

**group snap rename** _group\-snap\-spec_ _snap\-name_Rename group’s snapshot.

**group snap rollback** _group\-snap\-spec_Rollback group to snapshot.

**image\-meta get** _image\-spec_ _key_Get metadata value with the key.

**image\-meta list** _image\-spec_Show metadata held on the image. The first column is the key and the second column is the value.

**image\-meta remove** _image\-spec_ _key_Remove metadata key with the value.

**image\-meta set** _image\-spec_ _key_ _value_Set metadata key with the value. They will displayed in image\-meta list.

**import** \[–export\-format _format \(1 or 2\)_\] \[–image\-format _format\-id_\] \[–object\-size _size\-in\-B/K/M_\] \[–stripe\-unit _size\-in\-B/K/M_ –stripe\-count _num_\] \[–image\-feature _feature\-name_\]… \[–image\-shared\] _src\-path_ \[_image\-spec_\]Create a new image and imports its data from path \(use \- for stdin\). The import operation will try to create sparse rbd images if possible. For import from stdin, the sparsification unit is the data block size of the destination image \(object size\).

The –stripe\-unit and –stripe\-count arguments are optional, but must be used together.

The –export\-format accepts ‘1’ or ‘2’ currently. Format 2 allow us to import not only the content of image, but also the snapshots and other properties, such as image\_order, features.

**import\-diff** _src\-path_ _image\-spec_Import an incremental diff of an image and applies it to the current image. If the diff was generated relative to a start snapshot, we verify that snapshot already exists before continuing. If there was an end snapshot we verify it does not already exist before applying the changes, and create the snapshot when we are done.

**info** _image\-spec_ | _snap\-spec_Will dump information \(such as size and object size\) about a specific rbd image. If image is a clone, information about its parent is also displayed. If a snapshot is specified, whether it is protected is shown as well.

**journal client disconnect** _journal\-spec_Flag image journal client as disconnected.

**journal export** \[–verbose\] \[–no\-error\] _src\-journal\-spec_ _path\-name_Export image journal to path \(use \- for stdout\). It can be make a backup of the image journal especially before attempting dangerous operations.

Note that this command may not always work if the journal is badly corrupted.

**journal import** \[–verbose\] \[–no\-error\] _path\-name_ _dest\-journal\-spec_Import image journal from path \(use \- for stdin\).

**journal info** _journal\-spec_Show information about image journal.

**journal inspect** \[–verbose\] _journal\-spec_Inspect and report image journal for structural errors.

**journal reset** _journal\-spec_Reset image journal.

**journal status** _journal\-spec_Show status of image journal.

**lock add** \[–shared _lock\-tag_\] _image\-spec_ _lock\-id_Lock an image. The lock\-id is an arbitrary name for the user’s convenience. By default, this is an exclusive lock, meaning it will fail if the image is already locked. The –shared option changes this behavior. Note that locking does not affect any operation other than adding a lock. It does not protect an image from being deleted.

**lock ls** _image\-spec_Show locks held on the image. The first column is the locker to use with the lock remove command.

**lock rm** _image\-spec_ _lock\-id_ _locker_Release a lock on an image. The lock id and locker are as output by lock ls.

**ls** \[\-l | –long\] \[_pool\-name_\]Will list all rbd images listed in the rbd\_directory object. With \-l, also show snapshots, and use longer\-format output including size, parent \(if clone\), format, etc.

**merge\-diff** _first\-diff\-path_ _second\-diff\-path_ _merged\-diff\-path_Merge two continuous incremental diffs of an image into one single diff. The first diff’s end snapshot must be equal with the second diff’s start snapshot. The first diff could be \- for stdin, and merged diff could be \- for stdout, which enables multiple diff files to be merged using something like ‘rbd merge\-diff first second \- | rbd merge\-diff \- third result’. Note this command currently only support the source incremental diff with stripe\_count == 1

**migration abort** _image\-spec_Cancel image migration. This step may be run after successful or failed migration prepare or migration execute steps and returns the image to its initial \(before migration\) state. All modifications to the destination image are lost.

**migration commit** _image\-spec_Commit image migration. This step is run after a successful migration prepare and migration execute steps and removes the source image data.

**migration execute** _image\-spec_Execute image migration. This step is run after a successful migration prepare step and copies image data to the destination.

**migration prepare** \[–order _order_\] \[–object\-size _object\-size_\] \[–image\-feature _image\-feature_\] \[–image\-shared\] \[–stripe\-unit _stripe\-unit_\] \[–stripe\-count _stripe\-count_\] \[–data\-pool _data\-pool_\] \[–import\-only\] \[–source\-spec _json_\] \[–source\-spec\-path _path_\] _src\-image\-spec_ \[_dest\-image\-spec_\]Prepare image migration. This is the first step when migrating an image, i.e. changing the image location, format or other parameters that can’t be changed dynamically. The destination can match the source, and in this case _dest\-image\-spec_ can be omitted. After this step the source image is set as a parent of the destination image, and the image is accessible in copy\-on\-write mode by its destination spec.

An image can also be migrated from a read\-only import source by adding the_–import\-only_ optional and providing a JSON\-encoded _–source\-spec_ or a path to a JSON\-encoded source\-spec file using the _–source\-spec\-path_optionals.

**mirror image demote** _image\-spec_Demote a primary image to non\-primary for RBD mirroring.

**mirror image disable** \[–force\] _image\-spec_Disable RBD mirroring for an image. If the mirroring is configured in `image` mode for the image’s pool, then it can be explicitly disabled mirroring for each image within the pool.

**mirror image enable** _image\-spec_ _mode_Enable RBD mirroring for an image. If the mirroring is configured in `image` mode for the image’s pool, then it can be explicitly enabled mirroring for each image within the pool.

The mirror image mode can either be `journal` \(default\) or`snapshot`. The `journal` mode requires the RBD journaling feature.

**mirror image promote** \[–force\] _image\-spec_Promote a non\-primary image to primary for RBD mirroring.

**mirror image resync** _image\-spec_Force resync to primary image for RBD mirroring.

**mirror image status** _image\-spec_Show RBD mirroring status for an image.

**mirror pool demote** \[_pool\-name_\]Demote all primary images within a pool to non\-primary. Every mirroring enabled image will demoted in the pool.

**mirror pool disable** \[_pool\-name_\]Disable RBD mirroring by default within a pool. When mirroring is disabled on a pool in this way, mirroring will also be disabled on any images \(within the pool\) for which mirroring was enabled explicitly.

**mirror pool enable** \[_pool\-name_\] _mode_Enable RBD mirroring by default within a pool. The mirroring mode can either be `pool` or `image`. If configured in `pool` mode, all images in the pool with the journaling feature enabled are mirrored. If configured in `image` mode, mirroring needs to be explicitly enabled \(by `mirror image enable` command\) on each image.

**mirror pool info** \[_pool\-name_\]Show information about the pool mirroring configuration. It includes mirroring mode, peer UUID, remote cluster name, and remote client name.

**mirror pool peer add** \[_pool\-name_\] _remote\-cluster\-spec_Add a mirroring peer to a pool._remote\-cluster\-spec_ is \[_remote client name_@\]_remote cluster name_.

The default for _remote client name_ is “client.admin”.

This requires mirroring mode is enabled.

**mirror pool peer remove** \[_pool\-name_\] _uuid_Remove a mirroring peer from a pool. The peer uuid is available from `mirror pool info` command.

**mirror pool peer set** \[_pool\-name_\] _uuid_ _key_ _value_Update mirroring peer settings. The key can be either `client` or `cluster`, and the value is corresponding to remote client name or remote cluster name.

**mirror pool promote** \[–force\] \[_pool\-name_\]Promote all non\-primary images within a pool to primary. Every mirroring enabled image will promoted in the pool.

**mirror pool status** \[–verbose\] \[_pool\-name_\]Show status for all mirrored images in the pool. With –verbose, also show additionally output status details for every mirroring image in the pool.

**mirror snapshot schedule add** \[\-p | –pool _pool_\] \[–namespace _namespace_\] \[–image _image_\] _interval_ \[_start\-time_\]Add mirror snapshot schedule.

**mirror snapshot schedule list** \[\-R | –recursive\] \[–format _format_\] \[–pretty\-format\] \[\-p | –pool _pool_\] \[–namespace _namespace_\] \[–image _image_\]List mirror snapshot schedule.

**mirror snapshot schedule remove** \[\-p | –pool _pool_\] \[–namespace _namespace_\] \[–image _image_\] _interval_ \[_start\-time_\]Remove mirror snapshot schedule.

**mirror snapshot schedule status** \[\-p | –pool _pool_\] \[–format _format_\] \[–pretty\-format\] \[–namespace _namespace_\] \[–image _image_\]Show mirror snapshot schedule status.

**mv** _src\-image\-spec_ _dest\-image\-spec_Rename an image. Note: rename across pools is not supported.

**namespace create** _pool\-name_/_namespace\-name_Create a new image namespace within the pool.

**namespace list** _pool\-name_List image namespaces defined within the pool.

**namespace remove** _pool\-name_/_namespace\-name_Remove an empty image namespace from the pool.

**object\-map check** _image\-spec_ | _snap\-spec_Verify the object map is correct.

**object\-map rebuild** _image\-spec_ | _snap\-spec_Rebuild an invalid object map for the specified image. An image snapshot can be specified to rebuild an invalid object map for a snapshot.

**pool init** \[_pool\-name_\] \[–force\]Initialize pool for use by RBD. Newly created pools must initialized prior to use.

**resize** \(\-s | –size _size\-in\-M/G/T_\) \[–allow\-shrink\] _image\-spec_Resize rbd image. The size parameter also needs to be specified. The –allow\-shrink option lets the size be reduced.

**rm** _image\-spec_Delete an rbd image \(including all data blocks\). If the image has snapshots, this fails and nothing is deleted.

**snap create** _snap\-spec_Create a new snapshot. Requires the snapshot name parameter specified.

**snap limit clear** _image\-spec_Remove any previously set limit on the number of snapshots allowed on an image.

**snap limit set** \[–limit\] _limit_ _image\-spec_Set a limit for the number of snapshots allowed on an image.

**snap ls** _image\-spec_Dump the list of snapshots inside a specific image.

**snap protect** _snap\-spec_Protect a snapshot from deletion, so that clones can be made of it \(see rbd clone\). Snapshots must be protected before clones are made; protection implies that there exist dependent cloned children that refer to this snapshot. rbd clone will fail on a nonprotected snapshot.

This requires image format 2.

**snap purge** _image\-spec_Remove all unprotected snapshots from an image.

**snap rename** _src\-snap\-spec_ _dest\-snap\-spec_Rename a snapshot. Note: rename across pools and images is not supported.

**snap rm** \[–force\] _snap\-spec_Remove the specified snapshot.

**snap rollback** _snap\-spec_Rollback image content to snapshot. This will iterate through the entire blocks array and update the data head content to the snapshotted version.

**snap unprotect** _snap\-spec_Unprotect a snapshot from deletion \(undo snap protect\). If cloned children remain, snap unprotect fails. \(Note that clones may exist in different pools than the parent snapshot.\)

This requires image format 2.

**sparsify** \[–sparse\-size _sparse\-size_\] _image\-spec_Reclaim space for zeroed image extents. The default sparse size is 4096 bytes and can be changed via –sparse\-size option with the following restrictions: it should be power of two, not less than 4096, and not larger than image object size.

**status** _image\-spec_Show the status of the image, including which clients have it open.

**trash ls** \[_pool\-name_\]List all entries from trash.

**trash mv** _image\-spec_Move an image to the trash. Images, even ones actively in\-use by clones, can be moved to the trash and deleted at a later time.

**trash purge** \[_pool\-name_\]Remove all expired images from trash.

**trash restore** _image\-id_Restore an image from trash.

**trash rm** _image\-id_Delete an image from trash. If image deferment time has not expired you can not removed it unless use force. But an actively in\-use by clones or has snapshots can not be removed.

**trash purge schedule add** \[\-p | –pool _pool_\] \[–namespace _namespace_\] _interval_ \[_start\-time_\]Add trash purge schedule.

**trash purge schedule list** \[\-R | –recursive\] \[–format _format_\] \[–pretty\-format\] \[\-p | –pool _pool_\] \[–namespace _namespace_\]List trash purge schedule.

**trash purge schedule remove** \[\-p | –pool _pool_\] \[–namespace _namespace_\] _interval_ \[_start\-time_\]Remove trash purge schedule.

**trash purge schedule status** \[\-p | –pool _pool_\] \[–format _format_\] \[–pretty\-format\] \[–namespace _namespace_\]Show trash purge schedule status.

**watch** _image\-spec_Watch events on image.

## Image, snap, group and journal specs[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this headline")

_image\-spec_ is \[_pool\-name_/\[_namespace\-name_/\]\]_image\-name_
_snap\-spec_ is \[_pool\-name_/\[_namespace\-name_/\]\]_image\-name_@_snap\-name_
_group\-spec_ is \[_pool\-name_/\[_namespace\-name_/\]\]_group\-name_
_group\-snap\-spec_ is \[_pool\-name_/\[_namespace\-name_/\]\]_group\-name_@_snap\-name_
_journal\-spec_ is \[_pool\-name_/\[_namespace\-name_/\]\]_journal\-name_

The default for _pool\-name_ is “rbd” and _namespace\-name_ is “”. If an image name contains a slash character \(‘/’\), _pool\-name_ is required.

The _journal\-name_ is _image\-id_.

You may specify each name individually, using –pool, –namespace, –image, and –snap options, but this is discouraged in favor of the above spec syntax.

## Striping[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this headline")

RBD images are striped over many objects, which are then stored by the Ceph distributed object store \(RADOS\). As a result, read and write requests for the image are distributed across many nodes in the cluster, generally preventing any single node from becoming a bottleneck when individual images get large or busy.

The striping is controlled by three parameters:

`object-size```[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this definition")The size of objects we stripe over is a power of two. It will be rounded up the nearest power of two. The default object size is 4 MB, smallest is 4K and maximum is 32M.

`stripe_unit```[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this definition")Each \[_stripe\_unit_\] contiguous bytes are stored adjacently in the same object, before we move on to the next object.

`stripe_count```[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this definition")After we write \[_stripe\_unit_\] bytes to \[_stripe\_count_\] objects, we loop back to the initial object and write another stripe, until the object reaches its maximum size. At that point, we move on to the next \[_stripe\_count_\] objects.

By default, \[_stripe\_unit_\] is the same as the object size and \[_stripe\_count_\] is 1. Specifying a different \[_stripe\_unit_\] and/or \[_stripe\_count_\] is often referred to as using “fancy” striping and requires format 2.

## Kernel rbd \(krbd\) options[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this headline")

Most of these options are useful mainly for debugging and benchmarking. The default values are set in the kernel and may therefore depend on the version of the running kernel.

Per client instance rbd device map options:

* fsid=aaaaaaaa\-bbbb\-cccc\-dddd\-eeeeeeeeeeee \- FSID that should be assumed by the client.
* ip=a.b.c.d\[:p\] \- IP and, optionally, port the client should use.
* share \- Enable sharing of client instances with other mappings \(default\).
* noshare \- Disable sharing of client instances with other mappings.
* crc \- Enable CRC32C checksumming for msgr1 on\-the\-wire protocol \(default\). For msgr2.1 protocol this option is ignored: full checksumming is always on in ‘crc’ mode and always off in ‘secure’ mode.
* nocrc \- Disable CRC32C checksumming for msgr1 on\-the\-wire protocol. Note that only payload checksumming is disabled, header checksumming is always on. For msgr2.1 protocol this option is ignored.
* cephx\_require\_signatures \- Require msgr1 message signing feature \(since 3.19, default\). This option is deprecated and will be removed in the future as the feature has been supported since the Bobtail release.
* nocephx\_require\_signatures \- Don’t require msgr1 message signing feature \(since 3.19\). This option is deprecated and will be removed in the future.
* tcp\_nodelay \- Disable Nagle’s algorithm on client sockets \(since 4.0, default\).
* notcp\_nodelay \- Enable Nagle’s algorithm on client sockets \(since 4.0\).
* cephx\_sign\_messages \- Enable message signing for msgr1 on\-the\-wire protocol \(since 4.4, default\). For msgr2.1 protocol this option is ignored: message signing is built into ‘secure’ mode and not offered in ‘crc’ mode.
* nocephx\_sign\_messages \- Disable message signing for msgr1 on\-the\-wire protocol \(since 4.4\). For msgr2.1 protocol this option is ignored.
* mount\_timeout=x \- A timeout on various steps in rbd device map andrbd device unmap sequences \(default is 60 seconds\). In particular, since 4.2 this can be used to ensure that rbd device unmap eventually times out when there is no network connection to a cluster.
* osdkeepalive=x \- OSD keepalive timeout \(default is 5 seconds\).
* osd\_idle\_ttl=x \- OSD idle TTL \(default is 60 seconds\).

Per mapping \(block device\) rbd device map options:

* rw \- Map the image read\-write \(default\). Overridden by –read\-only.
* ro \- Map the image read\-only. Equivalent to –read\-only.
* queue\_depth=x \- queue depth \(since 4.2, default is 128 requests\).
* lock\_on\_read \- Acquire exclusive lock on reads, in addition to writes and discards \(since 4.9\).
* exclusive \- Disable automatic exclusive lock transitions \(since 4.12\). Equivalent to –exclusive.
* lock\_timeout=x \- A timeout on waiting for the acquisition of exclusive lock \(since 4.17, default is 0 seconds, meaning no timeout\).
* notrim \- Turn off discard and write zeroes offload support to avoid deprovisioning a fully provisioned image \(since 4.17\). When enabled, discard requests will fail with \-EOPNOTSUPP, write zeroes requests will fall back to manually zeroing.
* abort\_on\_full \- Fail write requests with \-ENOSPC when the cluster is full or the data pool reaches its quota \(since 5.0\). The default behaviour is to block until the full condition is cleared.
* alloc\_size \- Minimum allocation unit of the underlying OSD object store backend \(since 5.1, default is 64K bytes\). This is used to round off and drop discards that are too small. For bluestore, the recommended setting is bluestore\_min\_alloc\_size \(typically 64K for hard disk drives and 16K for solid\-state drives\). For filestore with filestore\_punch\_hole = false, the recommended setting is image object size \(typically 4M\).
* crush\_location=x \- Specify the location of the client in terms of CRUSH hierarchy \(since 5.8\). This is a set of key\-value pairs separated from each other by ‘|’, with keys separated from values by ‘:’. Note that ‘|’ may need to be quoted or escaped to avoid it being interpreted as a pipe by the shell. The key is the bucket type name \(e.g. rack, datacenter or region with default bucket types\) and the value is the bucket name. For example, to indicate that the client is local to rack “myrack”, data center “mydc” and region “myregion”:
    ```
    crush_location=rack:myrack|datacenter:mydc|region:myregion
    ```
    Each key\-value pair stands on its own: “myrack” doesn’t need to reside in “mydc”, which in turn doesn’t need to reside in “myregion”. The location is not a path to the root of the hierarchy but rather a set of nodes that are matched independently, owning to the fact that bucket names are unique within a CRUSH map. “Multipath” locations are supported, so it is possible to indicate locality for multiple parallel hierarchies:
    ```
    crush_location=rack:myrack1|rack:myrack2|datacenter:mydc
    ```
* read\_from\_replica=no \- Disable replica reads, always pick the primary OSD \(since 5.8, default\).
* read\_from\_replica=balance \- When issued a read on a replicated pool, pick a random OSD for serving it \(since 5.8\).
    This mode is safe for general use only since Octopus \(i.e. after “ceph osd require\-osd\-release octopus”\). Otherwise it should be limited to read\-only workloads such as images mapped read\-only everywhere or snapshots.
* read\_from\_replica=localize \- When issued a read on a replicated pool, pick the most local OSD for serving it \(since 5.8\). The locality metric is calculated against the location of the client given with crush\_location; a match with the lowest\-valued bucket type wins. For example, with default bucket types, an OSD in a matching rack is closer than an OSD in a matching data center, which in turn is closer than an OSD in a matching region.
    This mode is safe for general use only since Octopus \(i.e. after “ceph osd require\-osd\-release octopus”\). Otherwise it should be limited to read\-only workloads such as images mapped read\-only everywhere or snapshots.
* compression\_hint=none \- Don’t set compression hints \(since 5.8, default\).
* compression\_hint=compressible \- Hint to the underlying OSD object store backend that the data is compressible, enabling compression in passive mode \(since 5.8\).
* compression\_hint=incompressible \- Hint to the underlying OSD object store backend that the data is incompressible, disabling compression in aggressive mode \(since 5.8\).
* ms\_mode=legacy \- Use msgr1 on\-the\-wire protocol \(since 5.11, default\).
* ms\_mode=crc \- Use msgr2.1 on\-the\-wire protocol, select ‘crc’ mode, also referred to as plain mode \(since 5.11\). If the daemon denies ‘crc’ mode, fail the connection.
* ms\_mode=secure \- Use msgr2.1 on\-the\-wire protocol, select ‘secure’ mode \(since 5.11\). ‘secure’ mode provides full in\-transit encryption ensuring both confidentiality and authenticity. If the daemon denies ‘secure’ mode, fail the connection.
* ms\_mode=prefer\-crc \- Use msgr2.1 on\-the\-wire protocol, select ‘crc’ mode \(since 5.11\). If the daemon denies ‘crc’ mode in favor of ‘secure’ mode, agree to ‘secure’ mode.
* ms\_mode=prefer\-secure \- Use msgr2.1 on\-the\-wire protocol, select ‘secure’ mode \(since 5.11\). If the daemon denies ‘secure’ mode in favor of ‘crc’ mode, agree to ‘crc’ mode.
* udev \- Wait for udev device manager to finish executing all matching “add” rules and release the device before exiting \(default\). This option is not passed to the kernel.
* noudev \- Don’t wait for udev device manager. When enabled, the device may not be fully usable immediately on exit.

rbd device unmap options:

* force \- Force the unmapping of a block device that is open \(since 4.9\). The driver will wait for running requests to complete and then unmap; requests sent to the driver after initiating the unmap will be failed.
* udev \- Wait for udev device manager to finish executing all matching “remove” rules and clean up after the device before exiting \(default\). This option is not passed to the kernel.
* noudev \- Don’t wait for udev device manager.

## Examples[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this headline")

To create a new rbd image that is 100 GB:

```
rbd create mypool/myimage --size 102400
```

To use a non\-default object size \(8 MB\):

```
rbd create mypool/myimage --size 102400 --object-size 8M
```

To delete an rbd image \(be careful\!\):

```
rbd rm mypool/myimage
```

To create a new snapshot:

```
rbd snap create mypool/myimage@mysnap
```

To create a copy\-on\-write clone of a protected snapshot:

```
rbd clone mypool/myimage@mysnap otherpool/cloneimage
```

To see which clones of a snapshot exist:

```
rbd children mypool/myimage@mysnap
```

To delete a snapshot:

```
rbd snap rm mypool/myimage@mysnap
```

To map an image via the kernel with cephx enabled:

```
rbd device map mypool/myimage --id admin --keyfile secretfile
```

To map an image via the kernel with different cluster name other than default _ceph_:

```
rbd device map mypool/myimage --cluster cluster-name
```

To unmap an image:

```
rbd device unmap /dev/rbd0
```

To create an image and a clone from it:

```
rbd import --image-format 2 image mypool/parent
rbd snap create mypool/parent@snap
rbd snap protect mypool/parent@snap
rbd clone mypool/parent@snap otherpool/child
```

To create an image with a smaller stripe\_unit \(to better distribute small writes in some workloads\):

```
rbd create mypool/myimage --size 102400 --stripe-unit 65536B --stripe-count 16
```

To change an image from one image format to another, export it and then import it as the desired image format:

```
rbd export mypool/myimage@snap /tmp/img
rbd import --image-format 2 /tmp/img mypool/myimage2
```

To lock an image for exclusive use:

```
rbd lock add mypool/myimage mylockid
```

To release a lock:

```
rbd lock remove mypool/myimage mylockid client.2485
```

To list images from trash:

```
rbd trash ls mypool
```

To defer delete an image \(use _–expires\-at_ to set expiration time, default is now\):

```
rbd trash mv mypool/myimage --expires-at "tomorrow"
```

To delete an image from trash \(be careful\!\):

```
rbd trash rm mypool/myimage-id
```

To force delete an image from trash \(be careful\!\):

```
rbd trash rm mypool/myimage-id  --force
```

To restore an image from trash:

```
rbd trash restore mypool/myimage-id
```

To restore an image from trash and rename it:

```
rbd trash restore mypool/myimage-id --image mynewimage
```

## Availability[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this headline")

**rbd** is part of Ceph, a massively scalable, open\-source, distributed storage system. Please refer to the Ceph documentation at [http://ceph.com/docs](https://docs.ceph.com/en/pacific/man/8/rbd/) for more information.

## See also[¶](https://docs.ceph.com/en/pacific/man/8/rbd/ "Permalink to this headline")

[ceph](https://docs.ceph.com/en/pacific/man/8/rbd/)\(8\),[rados](https://docs.ceph.com/en/pacific/man/8/rbd/)\(8\)
