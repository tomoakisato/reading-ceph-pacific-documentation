# 308: iostat

**クリップソース:** [308: iostat — Ceph Documentation](https://docs.ceph.com/en/pacific/mgr/iostat/)

# iostat[¶](https://docs.ceph.com/en/pacific/mgr/iostat/ "Permalink to this headline")

このモジュールは、Cephクラスタで行われている現在のスループットとIOPSを表示します。

## Enabling[¶](https://docs.ceph.com/en/pacific/mgr/iostat/ "Permalink to this headline")

iostatモジュールが有効になっているかどうかを確認するには：

```
ceph mgr module ls
```

モジュールを有効化するには：

```
ceph mgr module enable iostat
```

モジュールを実行するには、以下を実行します：

```
ceph iostat
```

統計情報を出力する頻度を変更するには、\-pオプションを使用します：

```
ceph iostat -p <period in seconds>
```

例えば、5秒ごとに統計情報を表示する場合は、次のコマンドを使用します：

```
ceph iostat -p 5
```

モジュールを停止する場合は、Ctrl\-Cを押します。
