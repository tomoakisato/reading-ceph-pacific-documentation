# 253: Tutorial

**クリップソース:** [253: Tutorial — Ceph Documentation](https://docs.ceph.com/en/pacific/radosgw/swift/tutorial/)

# Tutorial[¶](https://docs.ceph.com/en/pacific/radosgw/swift/tutorial/ "Permalink to this headline")

Swift 互換の API チュートリアルは、単純なコンテナベースのオブジェクトのライフサイクルをフォローします。最初のステップでは、クライアントと RADOS Gateway サーバ間の接続を設定する必要があります。その後、オブジェクトのメタデータの追加と取得を含む、コンテナとオブジェクトのライフサイクルに従うことができます。以下の言語のコード例をご覧ください：

* [Java](https://docs.ceph.com/en/pacific/radosgw/swift/tutorial/)
* [Python](https://docs.ceph.com/en/pacific/radosgw/swift/tutorial/)
* [Ruby](https://docs.ceph.com/en/pacific/radosgw/swift/tutorial/)

![e322f5f15ef9007fde9c2917398bd5c23ba4af3bc485ba15260b2ab6361a72ac.png](image/e322f5f15ef9007fde9c2917398bd5c23ba4af3bc485ba15260b2ab6361a72ac.png)
