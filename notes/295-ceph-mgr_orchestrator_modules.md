# 295: ceph-mgr orchestrator modules

 # ceph\-mgr orchestrator modules[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this headline")

Warning:

This is developer documentation, describing Ceph internals that are only relevant to people writing ceph\-mgr orchestrator modules.

In this context, _orchestrator_ refers to some external service that provides the ability to discover devices and create Ceph services. This includes external projects such as Rook.

An _orchestrator module_ is a ceph\-mgr module \([ceph\-mgr module developer’s guide](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/)\) which implements common management operations using a particular orchestrator.

Orchestrator modules subclass the `Orchestrator` class: this class is an interface, it only provides method definitions to be implemented by subclasses. The purpose of defining this common interface for different orchestrators is to enable common UI code, such as the dashboard, to work with various different backends.

Behind all the abstraction, the purpose of orchestrator modules is simple: enable Ceph to do things like discover available hardware, create and destroy OSDs, and run MDS and RGW services.

A tutorial is not included here: for full and concrete examples, see the existing implemented orchestrator modules in the Ceph source tree.

## Glossary[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this headline")

Stateful servicea daemon that uses local storage, such as OSD or mon.

Stateless servicea daemon that doesn’t use any local storage, such as an MDS, RGW, nfs\-ganesha, iSCSI gateway.

Labelarbitrary string tags that may be applied by administrators to hosts. Typically administrators use labels to indicate which hosts should run which kinds of service. Labels are advisory \(from human input\) and do not guarantee that hosts have particular physical capabilities.

Drive groupcollection of block devices with common/shared OSD formatting \(typically one or more SSDs acting as journals/dbs for a group of HDDs\).

Placementchoice of which host is used to run a service.

## Key Concepts[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this headline")

The underlying orchestrator remains the source of truth for information about whether a service is running, what is running where, which hosts are available, etc. Orchestrator modules should avoid taking any internal copies of this information, and read it directly from the orchestrator backend as much as possible.

Bootstrapping hosts and adding them to the underlying orchestration system is outside the scope of Ceph’s orchestrator interface. Ceph can only work on hosts when the orchestrator is already aware of them.

Where possible, placement of stateless services should be left up to the orchestrator.

## Completions and batching[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this headline")

All methods that read or modify the state of the system can potentially be long running. Therefore the module needs to schedule those operations.

Each orchestrator module implements its own underlying mechanisms for completions. This might involve running the underlying operations in threads, or batching the operations up before later executing in one go in the background. If implementing such a batching pattern, the module would do no work on any operation until it appeared in a list of completions passed into _process_.

## Error Handling[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this headline")

The main goal of error handling within orchestrator modules is to provide debug information to assist users when dealing with deployment errors.

_class_ `orchestrator.``OrchestratorError`\(_msg_, _errno=\- 22_, _event\_kind\_subject=None_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")General orchestrator specific error.

Used for deployment, configuration or user errors.

It’s not intended for programming errors or orchestrator internal errors.

_class_ `orchestrator.``NoOrchestrator`\(_msg='No orchestrator configured \(try \`ceph orch set backend\`\)'_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")No orchestrator in configured.

_class_ `orchestrator.``OrchestratorValidationError`\(_msg_, _errno=\- 22_, _event\_kind\_subject=None_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")Raised when an orchestrator doesn’t support a specific feature.

In detail, orchestrators need to explicitly deal with different kinds of errors:

1. No orchestrator configured
    See [`NoOrchestrator`](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "orchestrator.NoOrchestrator").
2. An orchestrator doesn’t implement a specific method.
    For example, an Orchestrator doesn’t support `add_host`.
    In this case, a `NotImplementedError` is raised.
3. Missing features within implemented methods.
    E.g. optional parameters to a command that are not supported by the backend \(e.g. the hosts field in `Orchestrator.apply_mons()` command with the rook backend\).
    See [`OrchestratorValidationError`](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "orchestrator.OrchestratorValidationError").
4. Input validation errors
    The `orchestrator` module and other calling modules are supposed to provide meaningful error messages.
    See [`OrchestratorValidationError`](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "orchestrator.OrchestratorValidationError").
5. Errors when actually executing commands
    The resulting Completion should contain an error string that assists in understanding the problem. In addition, `Completion.is_errored()` is set to `True`
6. Invalid configuration in the orchestrator modules
    This can be tackled similar to 5.

All other errors are unexpected orchestrator issues and thus should raise an exception that are then logged into the mgr log file. If there is a completion object at that point,`Completion.result()` may contain an error message.

## Excluded functionality[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this headline")

* Ceph’s orchestrator interface is not a general purpose framework for managing linux servers – it is deliberately constrained to manage the Ceph cluster’s services only.
* Multipathed storage is not handled \(multipathing is unnecessary for Ceph clusters\). Each drive is assumed to be visible only on a single host.

## Host management[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this headline")

`Orchestrator.``add_host`\(_host\_spec_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")Add a host to the orchestrator inventory.

Parameters**host** – hostname

Return type`OrchResult`\[`str`\]

`Orchestrator.``remove_host`\(_host_, _force_, _offline_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")Remove a host from the orchestrator inventory.

Parameters**host** \(`str`\) – hostname

Return type`OrchResult`\[`str`\]

`Orchestrator.``get_hosts`\(\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")Report the hosts in the cluster.

Return type`OrchResult`\[`List`\[`HostSpec`\]\]

Returnslist of HostSpec

`Orchestrator.``update_host_addr`\(_host_, _addr_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")Update a host’s address

Parameters
* **host** \(`str`\) – hostname
* **addr** \(`str`\) – address \(dns name or IP\)

Return type`OrchResult`\[`str`\]

`Orchestrator.``add_host_label`\(_host_, _label_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")Add a host label

Return type`OrchResult`\[`str`\]

`Orchestrator.``remove_host_label`\(_host_, _label_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")Remove a host label

Return type`OrchResult`\[`str`\]

_class_ `orchestrator.``HostSpec`\(_hostname_, _addr=None_, _labels=None_, _status=None_, _location=None_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")Information about hosts. Like e.g. `kubectl get nodes`

## Devices[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this headline")

`Orchestrator.``get_inventory`\(_host\_filter=None_, _refresh=False_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")Returns something that was created by ceph\-volume inventory.

Return type`OrchResult`\[`List`\[`InventoryHost`\]\]

Returnslist of InventoryHost

_class_ `orchestrator.``InventoryFilter`\(_labels=None_, _hosts=None_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")When fetching inventory, use this filter to avoid unnecessarily scanning the whole estate.

Typical use: filter by host when presenting UI workflow for configuringa particular server. filter by label when not all of estate is Ceph servers, and we want to only learn about the Ceph servers. filter by label when we are interested particularly in e.g. OSD servers.

_class_ `ceph.deployment.inventory.``Devices`\(_devices_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")A container for Device instances with reporting

_class_ `ceph.deployment.inventory.``Device`\(_path_, _sys\_api=None_, _available=None_, _rejected\_reasons=None_, _lvs=None_, _device\_id=None_, _lsm\_data=None_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")
## Placement[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this headline")

A [Daemon Placement](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/) defines the placement of daemons of a specific service.

In general, stateless services do not require any specific placement rules as they can run anywhere that sufficient system resources are available. However, some orchestrators may not include the functionality to choose a location in this way. Optionally, you can specify a location when creating a stateless service.

_class_ `ceph.deployment.service_spec.``PlacementSpec`\(_label=None_, _hosts=None_, _count=None_, _count\_per\_host=None_, _host\_pattern=None_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")For APIs that need to specify a host subset

_classmethod_ `from_string`\(_arg_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")A single integer is parsed as a count: \>\>\> PlacementSpec.from\_string\(‘3’\) PlacementSpec\(count=3\)

A list of names is parsed as host specifications: \>\>\> PlacementSpec.from\_string\(‘host1 host2’\) PlacementSpec\(hosts=\[HostPlacementSpec\(hostname=’host1’, network=’’, name=’’\), HostPlacementSpec\(hostname=’host2’, network=’’, name=’’\)\]\)

You can also prefix the hosts with a count as follows: \>\>\> PlacementSpec.from\_string\(‘2 host1 host2’\) PlacementSpec\(count=2, hosts=\[HostPlacementSpec\(hostname=’host1’, network=’’, name=’’\), HostPlacementSpec\(hostname=’host2’, network=’’, name=’’\)\]\)

You can spefify labels using label:\<label\>\>\>\> PlacementSpec.from\_string\(‘label:mon’\) PlacementSpec\(label=’mon’\)

Labels als support a count: \>\>\> PlacementSpec.from\_string\(‘3 label:mon’\) PlacementSpec\(count=3, label=’mon’\)

fnmatch is also supported: \>\>\> PlacementSpec.from\_string\(‘data\[1\-3\]’\) PlacementSpec\(host\_pattern=’data\[1\-3\]’\)

```
>>> PlacementSpec.from_string(None)
PlacementSpec()
```

Return type[`PlacementSpec`](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "ceph.deployment.service_spec.PlacementSpec")

`host_pattern`_: Optional\[str\]_[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")fnmatch patterns to select hosts. Can also be a single host.

`pretty_str`\(\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")```
>>> 
... ps = PlacementSpec(...)  # For all placement specs:
... PlacementSpec.from_string(ps.pretty_str()) == ps
```

Return type`str`

## Services[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this headline")

_class_ `orchestrator.``ServiceDescription`\(_spec_, _container\_image\_id=None_, _container\_image\_name=None_, _service\_url=None_, _last\_refresh=None_, _created=None_, _deleted=None_, _size=0_, _running=0_, _events=None_, _virtual\_ip=None_, _ports=\[\]_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")For responding to queries about the status of a particular service, stateful or stateless.

This is not about health or performance monitoring of services: it’s about letting the orchestrator tell Ceph whether and where a service is scheduled in the cluster. When an orchestrator tells Ceph “it’s running on host123”, that’s not a promise that the process is literally up this second, it’s a description of where the orchestrator has decided the service should run.

_class_ `ceph.deployment.service_spec.``ServiceSpec`\(_service\_type_, _service\_id=None_, _placement=None_, _count=None_, _config=None_, _unmanaged=False_, _preview\_only=False_, _networks=None_\)Details of service creation.

Request to the orchestrator for a cluster of daemons such as MDS, RGW, iscsi gateway, MONs, MGRs, Prometheus

This structure is supposed to be enough information to start the services.

_classmethod_ `from_json`\(_cls_, _json\_spec_\)Initialize ‘ServiceSpec’ object data from a json structure

There are two valid styles for service specs:

the “old” style:

```
service_type: nfs
service_id: foo
pool: mypool
namespace: myns
```

and the “new” style:

```
service_type: nfs
service_id: foo
config:
  some_option: the_value
networks: [10.10.0.0/16]
spec:
  pool: mypool
  namespace: myns
```

In [https://tracker.ceph.com/issues/45321](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/) we decided that we’d like to prefer the new style as it is more readable and provides a better understanding of what fields are special for a give service type.

Note, we’ll need to stay compatible with both versions for the the next two major releases \(octoups, pacific\).

Parameters**json\_spec** \(`Dict`\) – A valid dict with ServiceSpec

Return type~ServiceSpecT

`networks`_: List\[str\]_A list of network identities instructing the daemons to only bind on the particular networks in that list. In case the cluster is distributed across multiple networks, you can add multiple networks. See[Networks and Ports](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/),[Specifying Networks](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/) and [Specifying Networks](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/).

`placement`_: [ceph.deployment.service\_spec.PlacementSpec](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "ceph.deployment.service_spec.PlacementSpec")_See [Daemon Placement](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/).

`service_id`The name of the service. Required for `iscsi`, `mds`, `nfs`, `osd`, `rgw`,`container`, `ingress`

`service_type`The type of the service. Needs to be either a Ceph service \(`mon`, `crash`, `mds`, `mgr`, `osd` or`rbd-mirror`\), a gateway \(`nfs` or `rgw`\), part of the monitoring stack \(`alertmanager`, `grafana`, `node-exporter` or`prometheus`\) or \(`container`\) for custom containers.

`unmanaged`If set to `true`, the orchestrator will not deploy nor remove any daemon associated with this service. Placement and all other properties will be ignored. This is useful, if you do not want this service to be managed temporarily. For cephadm, See [Disabling automatic deployment of daemons](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/)

`Orchestrator.``describe_service`\(_service\_type=None_, _service\_name=None_, _refresh=False_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")Describe a service \(of any kind\) that is already configured in the orchestrator. For example, when viewing an OSD in the dashboard we might like to also display information about the orchestrator’s view of the service \(like the kubernetes pod ID\).

When viewing a CephFS filesystem in the dashboard, we would use this to display the pods being currently run for MDS daemons.

Return type`OrchResult`\[`List`\[`ServiceDescription`\]\]

Returnslist of ServiceDescription objects.

`Orchestrator.``service_action`\(_action_, _service\_name_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")Perform an action \(start/stop/reload\) on a service \(i.e., all daemons providing the logical service\).

Parameters
* **action** \(`str`\) – one of “start”, “stop”, “restart”, “redeploy”, “reconfig”
* **service\_name** \(`str`\) – service\_type \+ ‘.’ \+ service\_id \(e.g. “mon”, “mgr”, “mds.mycephfs”, “rgw.realm.zone”, …\)

Return typeOrchResult

`Orchestrator.``remove_service`\(_service\_name_, _force=False_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")Remove a service \(a collection of daemons\).

Return type`OrchResult`\[`str`\]

ReturnsNone

## Daemons[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this headline")

`Orchestrator.``list_daemons`\(_service\_name=None_, _daemon\_type=None_, _daemon\_id=None_, _host=None_, _refresh=False_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")Describe a daemon \(of any kind\) that is already configured in the orchestrator.

Return type`OrchResult`\[`List`\[`DaemonDescription`\]\]

Returnslist of DaemonDescription objects.

`Orchestrator.``remove_daemons`\(_names_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")Remove specific daemon\(s\).

Return type`OrchResult`\[`List`\[`str`\]\]

ReturnsNone

`Orchestrator.``daemon_action`\(_action_, _daemon\_name_, _image=None_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")Perform an action \(start/stop/reload\) on a daemon.

Parameters
* **action** \(`str`\) – one of “start”, “stop”, “restart”, “redeploy”, “reconfig”
* **daemon\_name** \(`str`\) – name of daemon
* **image** \(`Optional`\[`str`\]\) – Container image when redeploying that daemon

Return typeOrchResult

## OSD management[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this headline")

`Orchestrator.``create_osds`\(_drive\_group_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")Create one or more OSDs within a single Drive Group.

The principal argument here is the drive\_group member of OsdSpec: other fields are advisory/extensible for any finer\-grained OSD feature enablement \(choice of backing store, compression/encryption, etc\).

Return type`OrchResult`\[`str`\]

`Orchestrator.``blink_device_light`\(_ident\_fault_, _on_, _locations_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")Instructs the orchestrator to enable or disable either the ident or the fault LED.

Parameters
* **ident\_fault** \(`str`\) – either `"ident"` or `"fault"`
* **on** \(`bool`\) – `True` = on.
* **locations** \(`List`\[`DeviceLightLoc`\]\) – See [`orchestrator.DeviceLightLoc`](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "orchestrator.DeviceLightLoc")

Return type`OrchResult`\[`List`\[`str`\]\]

_class_ `orchestrator.``DeviceLightLoc`\(_host_, _dev_, _path_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")Describes a specific device on a specific host. Used for enabling or disabling LEDs on devices.

hostname as in [`orchestrator.Orchestrator.get_hosts()`](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "orchestrator.Orchestrator.get_hosts")

device\_id: e.g. `ABC1234DEF567-1R1234_ABC8DE0Q`.See `ceph osd metadata | jq '.[].device_ids'`

### OSD Replacement[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this headline")

See [Replacing an OSD](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/) for the underlying process.

Replacing OSDs is fundamentally a two\-staged process, as users need to physically replace drives. The orchestrator therefore exposes this two\-staged process.

Phase one is a call to [`Orchestrator.remove_daemons()`](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "orchestrator.Orchestrator.remove_daemons") with `destroy=True` in order to mark the OSD as destroyed.

Phase two is a call to [`Orchestrator.create_osds()`](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "orchestrator.Orchestrator.create_osds") with a Drive Group with

[`DriveGroupSpec.osd_id_claims`](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "ceph.deployment.drive_group.DriveGroupSpec.osd_id_claims") set to the destroyed OSD ids.

## Services[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this headline")

`Orchestrator.``add_daemon`\(_spec_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")Create daemons daemon\(s\) for unmanaged services

Return type`OrchResult`\[`List`\[`str`\]\]

`Orchestrator.``apply_mon`\(_spec_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")Update mon cluster

Return type`OrchResult`\[`str`\]

`Orchestrator.``apply_mgr`\(_spec_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")Update mgr cluster

Return type`OrchResult`\[`str`\]

`Orchestrator.``apply_mds`\(_spec_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")Update MDS cluster

Return type`OrchResult`\[`str`\]

`Orchestrator.``apply_rbd_mirror`\(_spec_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")Update rbd\-mirror cluster

Return type`OrchResult`\[`str`\]

_class_ `ceph.deployment.service_spec.``RGWSpec`\(_service\_type='rgw'_, _service\_id=None_, _placement=None_, _rgw\_realm=None_, _rgw\_zone=None_, _rgw\_frontend\_port=None_, _rgw\_frontend\_ssl\_certificate=None_, _rgw\_frontend\_type=None_, _unmanaged=False_, _ssl=False_, _preview\_only=False_, _config=None_, _networks=None_, _subcluster=None_\)Settings to configure a \(multisite\) Ceph RGW

```
service_type: rgw
service_id: myrealm.myzone
spec:
    rgw_realm: myrealm
    rgw_zone: myzone
    ssl: true
    rgw_frontend_port: 1234
    rgw_frontend_type: beast
    rgw_frontend_ssl_certificate: ...
```

See also: [Service Specification](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/)

`Orchestrator.``apply_rgw`\(_spec_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")Update RGW cluster

Return type`OrchResult`\[`str`\]

_class_ `ceph.deployment.service_spec.``NFSServiceSpec`\(_service\_type='nfs'_, _service\_id=None_, _placement=None_, _unmanaged=False_, _preview\_only=False_, _config=None_, _networks=None_, _port=None_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")`Orchestrator.``apply_nfs`\(_spec_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")Update NFS cluster

Return type`OrchResult`\[`str`\]

## Upgrades[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this headline")

`Orchestrator.``upgrade_available`\(\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")Report on what versions are available to upgrade to

Return type`OrchResult`

ReturnsList of strings

`Orchestrator.``upgrade_start`\(_image_, _version_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")Return type`OrchResult`\[`str`\]

`Orchestrator.``upgrade_status`\(\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")If an upgrade is currently underway, report on where we are in the process, or if some error has occurred.

Return type`OrchResult`\[`UpgradeStatusSpec`\]

ReturnsUpgradeStatusSpec instance

_class_ `orchestrator.``UpgradeStatusSpec`[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")
## Utility[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this headline")

`Orchestrator.``available`\(\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")Report whether we can talk to the orchestrator. This is the place to give the user a meaningful message if the orchestrator isn’t running or can’t be contacted.

This method may be called frequently \(e.g. every page load to conditionally display a warning banner\), so make sure it’s not too expensive. It’s okay to give a slightly stale status \(e.g. based on a periodic background ping of the orchestrator\) if that’s necessary to make this method fast.

Note:

True doesn’t mean that the desired functionality is actually available in the orchestrator. I.e. this won’t work as expected:

```
>>> 
... if OrchestratorClientMixin().available()[0]:  # wrong.
...     OrchestratorClientMixin().get_hosts()
```

Returnsboolean representing whether the module is available/usable

Returnsstring describing any error

Return type`Tuple`\[`bool`, `str`, `Dict`\[`str`, `Any`\]\]

Returnsdict containing any module specific information

`Orchestrator.``get_feature_set`\(\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")Describes which methods this orchestrator implements

Note:

True doesn’t mean that the desired functionality is actually possible in the orchestrator. I.e. this won’t work as expected:

```
>>> 
... api = OrchestratorClientMixin()
... if api.get_feature_set()['get_hosts']['available']:  # wrong.
...     api.get_hosts()
```

It’s better to ask for forgiveness instead:

```
>>> 
... try:
...     OrchestratorClientMixin().get_hosts()
... except (OrchestratorError, NotImplementedError):
...     ...
```

Return type`Dict`\[`str`, `dict`\]

ReturnsDict of API method names to `{'available': True or False}`

## Client Modules[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this headline")

_class_ `orchestrator.``OrchestratorClientMixin`[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")A module that inherents from OrchestratorClientMixin can directly call all `Orchestrator` methods without manually calling remote.

Every interface method from `Orchestrator` is converted into a stub method that internally calls `OrchestratorClientMixin._oremote()`

```
>>> class MyModule(OrchestratorClientMixin):
...    def func(self):
...        completion = self.add_host('somehost')  # calls `_oremote()`
...        self.log.debug(completion.result)
```

Note:

Orchestrator implementations should not inherit from OrchestratorClientMixin. Reason is, that OrchestratorClientMixin magically redirects all methods to the “real” implementation of the orchestrator.

```
>>> import mgr_module
>>> 
... class MyImplentation(mgr_module.MgrModule, Orchestrator):
...     def __init__(self, ...):
...         self.orch_client = OrchestratorClientMixin()
...         self.orch_client.set_mgr(self.mgr))
```

`set_mgr`\(_mgr_\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator_modules/ "Permalink to this definition")Useable in the Dashbord that uses a global `mgr`

Return type`None`
