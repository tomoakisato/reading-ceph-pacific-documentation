# 249: Service Operations

 # Service Operations[¶](https://docs.ceph.com/en/pacific/radosgw/swift/serviceops/ "Permalink to this headline")

To retrieve data about our Swift\-compatible service, you may execute `GET`requests using the `X-Storage-Url` value retrieved during authentication.

## List Containers[¶](https://docs.ceph.com/en/pacific/radosgw/swift/serviceops/ "Permalink to this headline")

A `GET` request that specifies the API version and the account will return a list of containers for a particular user account. Since the request returns a particular user’s containers, the request requires an authentication token. The request cannot be made anonymously.

### Syntax[¶](https://docs.ceph.com/en/pacific/radosgw/swift/serviceops/ "Permalink to this headline")

```
GET /{api version}/{account} HTTP/1.1
Host: {fqdn}
X-Auth-Token: {auth-token}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/swift/serviceops/ "Permalink to this headline")

`limit`

DescriptionLimits the number of results to the specified value.

TypeInteger

RequiredNo

`format`

DescriptionDefines the format of the result.

TypeString

Valid Values`json` | `xml`

RequiredNo

`marker`

DescriptionReturns a list of results greater than the marker value.

TypeString

RequiredNo

### Response Entities[¶](https://docs.ceph.com/en/pacific/radosgw/swift/serviceops/ "Permalink to this headline")

The response contains a list of containers, or returns with an HTTP 204 response code

`account`

DescriptionA list for account information.

TypeContainer

`container`

DescriptionThe list of containers.

TypeContainer

`name`

DescriptionThe name of a container.

TypeString

`bytes`

DescriptionThe size of the container.

TypeInteger
