# 98: Logging and Debugging

**クリップソース:** [98: Logging and Debugging — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/troubleshooting/log-and-debug/)

# Logging and Debugging[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/log-and-debug/ "Permalink to this headline")

通常、Cephの設定にデバッグを追加する場合は、実行時に追加します。また、クラスタの起動時に問題が発生した場合、Ceph設定ファイルにCephデバッグログを追加することも可能です。Cephログファイルは、/var/log/ceph \(デフォルト\)の下で参照できます。

ヒント:デバッグ出力でシステムが遅くなる場合、レイテンシがレースコンディションを隠している可能性があります。

ログの記録はリソースを必要とします。クラスタの特定の領域で問題が発生した場合、クラスタのその領域のログを有効にしてください。たとえば、OSDは正常に動作しているが、メタデータ・サーバーに問題がある場合、問題のある特定のメタデータ・サーバー・インスタンスのデバッグ・ロギングを有効にすることから始める必要があります。必要に応じて、各サブシステムに対してロギングを有効にします。

重要：Verboseログは1時間に1GB以上のデータを生成する可能性があります。OSのディスクが容量に達した場合、ノードは動作を停止します。

Cephのロギングを有効にしたり、そのレートを上げたりする場合は、OSディスクに十分なディスク容量があることを確認します。 ログファイルのローテーションの詳細については、「[Accelerating Log Rotation](https://docs.ceph.com/en/pacific/rados/troubleshooting/log-and-debug/)」を参照してください。システムが順調に稼働している場合、不要なデバッグ設定を削除して、クラスタが最適に動作するようにします。デバッグ出力メッセージのログは遅く、クラスタを運用する際のリソースの無駄遣いになります。

利用可能な設定の詳細については、「[Subsystem, Log and Debug Settings](https://docs.ceph.com/en/pacific/rados/troubleshooting/log-and-debug/)」を参照してください。

## Runtime[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/log-and-debug/ "Permalink to this headline")

実行時のコンフィギュレーション設定を確認したい場合は、デーモンが動作しているホストにログインし、以下を実行する必要があります：

```
ceph daemon {daemon-name} config show | less
```

For example,:

```
ceph daemon osd.0 config show | less
```

Cephのデバッグ出力\(すなわちdout\(\)\)を実行時に有効にするには、ceph tellコマンドを使用して実行時設定に引数を注入します。

```
ceph tell {daemon-type}.{daemon id or *} config set {name} {value}
```

{daemon\-type} を osd、mon、mds のいずれかに置き換えてください。実行時の設定を特定のタイプのすべてのデーモンに適用するには、\*を使用するか、または特定のデーモンのIDを指定します。たとえば、osd.0という名前のceph\-osdデーモンのデバッグログを増加させるには、以下を実行します：

```
ceph tell osd.0 config set debug_osd 0/5
```

ceph tellコマンドは、モニタを経由します。モニタにバインドできない場合でも、設定を変更したいデーモンのホストにceph daemonでログインすれば、変更を行うことができます。たとえば、以下のようになります：

```
sudo ceph daemon osd.0 config set debug_osd 0/5
```

利用可能な設定の詳細については、「[Subsystem, Log and Debug Settings](https://docs.ceph.com/en/pacific/rados/troubleshooting/log-and-debug/)」を参照してください。

## Boot Time[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/log-and-debug/ "Permalink to this headline")

Cephのデバッグ出力\(すなわちdout\(\)\)を起動時に有効にするには、Cephの設定ファイルに設定を追加する必要があります。各デーモンに共通するサブシステムは、設定ファイルの\[global\]の下に設定することができます。特定のデーモンのサブシステムは、設定ファイルのデーモンセクション\(例: \[mon\]、\[osd\]、\[mds\]\)の下に設定されます。例えば：

```
[global]
        debug ms = 1/5

[mon]
        debug mon = 20
        debug paxos = 1/5
        debug auth = 2

[osd]
        debug osd = 1/5
        debug filestore = 1/5
        debug journal = 1
        debug monc = 5/20

[mds]
        debug mds = 1
        debug mds balancer = 1

```

詳しくは、「[Subsystem, Log and Debug Settings](https://docs.ceph.com/en/pacific/rados/troubleshooting/log-and-debug/)」を参照してください。

## Accelerating Log Rotation[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/log-and-debug/ "Permalink to this headline")

OSディスクがフルに近いの場合、/etc/logrotate.d/cephにあるCephログローテーションファイルを修正することで、ログローテーションを加速させることができます。ローテーション頻度の後にサイズ設定を追加し、ログがサイズ設定を超えた場合に、\(cronjobで\)ログローテーションを加速させることができます。例えば、デフォルトの設定は以下のようになります。：

```
rotate 7
weekly
compress
sharedscripts
```

サイズ設定を追加します。

```
rotate 7
weekly
size 500M
compress
sharedscripts
```

そして、ユーザー空間のcrontabエディタを起動します。

```
crontab -e
```

最後に、etc/logrotate.d/cephファイルをチェックするエントリを追加します。

```
30 * * * * /usr/sbin/logrotate /etc/logrotate.d/ceph >/dev/null 2>&1
```

例では、etc/logrotate.d/cephファイルを30分ごとにチェックします。

## Valgrind[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/log-and-debug/ "Permalink to this headline")

デバッグでは、メモリやスレッドの問題を追跡する必要がある場合もあります。Valgrindでは、単一のデーモン、デーモンの種類、クラスタ全体を実行することができます。Valgrindは、Cephの開発またはデバッグの場合にのみ使用する必要があります。Valgrindは計算量が多く、それ以外の場合はシステムの速度が低下します。Valgrindのメッセージは標準エラー出力に記録されます。

## Subsystem, Log and Debug Settings[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/log-and-debug/ "Permalink to this headline")

多くの場合、サブシステムを経由したデバッグ・ログ出力を有効にします。

### Ceph Subsystems[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/log-and-debug/ "Permalink to this headline")

各サブシステムは、出力ログと、メモリ内のログのログレベルを持ちます。ログファイルレベルとデバッグログ用のメモリレベルを設定することで、これらのサブシステムにそれぞれ異なる値を設定することができます。Cephのログレベルは1から20のスケールで動作し、1がterse、20がverboseです 。一般に、メモリ内のログは、以下の場合を除き、出力ログに送信されません：

* 致命的なシグナルが発生するか
* ソースコードのアサートがトリガーされるか、または
* 要求に応じて。 詳細については、

デバッグ・ロギング設定は、ログ・レベルとメモリ・レベルに単一の値を取ることができ、両者を同じ値として設定します。たとえば、debug ms=5を指定すると、Cephはログレベルおよびメモリレベルとして5を扱います。また、これらを別々に指定することもできます。最初の設定はログレベル、2番目の設定はメモリレベルです。 これらはスラッシュ\(/\)で区切る必要があります。たとえば、msサブシステムのデバッグ・ログ・レベルを1、メモリ・レベルを5に設定したい場合、debug ms=1/5と指定します。たとえば、以下のようになります：

```
debug {subsystem} = {log-level}/{memory-level}
#for example
debug mds balancer = 1/20
```

次の表は、Cephサブシステムとそのデフォルトのログレベルおよびメモリレベルの一覧です。ログ記録作業が完了したら、サブシステムをデフォルトレベルまたは通常の運用に適したレベルに復元します。

|Subsystem   |Log Level|Memory Level|
|------------|---------|------------|
|default     |0        |5           |
|lockdep     |0        |1           |
|context     |0        |1           |
|crush       |1        |1           |
|mds         |1        |5           |
|mdsbalancer |1        |5           |
|mdslocker   |1        |5           |
|mdslog      |1        |5           |
|mdslogexpire|1        |5           |
|mdsmigrator |1        |5           |
|buffer      |0        |1           |
|timer       |0        |1           |
|filer       |0        |1           |
|striper     |0        |1           |
|objecter    |0        |1           |
|rados       |0        |5           |
|rbd         |0        |5           |
|rbdmirror   |0        |5           |
|rbdreplay   |0        |5           |
|journaler   |0        |5           |
|objectcacher|0        |5           |
|client      |0        |5           |
|osd         |1        |5           |
|optracker   |0        |5           |
|objclass    |0        |5           |
|filestore   |1        |3           |
|journal     |1        |3           |
|ms          |0        |5           |
|mon         |1        |5           |
|monc        |0        |10          |
|paxos       |1        |5           |
|tp          |0        |5           |
|auth        |1        |5           |
|crypto      |1        |5           |
|finisher    |1        |1           |
|reserver    |1        |1           |
|heartbeatmap|1        |5           |
|perfcounter |1        |5           |
|rgw         |1        |5           |
|rgwsync     |1        |5           |
|civetweb    |1        |10          |
|javaclient  |1        |5           |
|asok        |1        |5           |
|throttle    |1        |1           |
|refs        |0        |0           |
|compressor  |1        |5           |
|bluestore   |1        |5           |
|bluefs      |1        |5           |
|bdev        |1        |3           |
|kstore      |1        |5           |
|rocksdb     |4        |5           |
|leveldb     |4        |5           |
|memdb       |4        |5           |
|fuse        |1        |5           |
|mgr         |1        |5           |
|mgrc        |1        |5           |
|dpdk        |1        |5           |
|eventtrace  |1        |5           |

### Logging Settings[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/log-and-debug/ "Permalink to this headline")

Ceph構成ファイルでは、ログおよびデバッグの設定は必須ではありませんが、必要に応じてデフォルトの設定をオーバーライドすることができます。Cephは以下の設定をサポートしています：

**log file**

Description

クラスタのロギファイルの場所

Type

String

Required

No

Default

/var/log/ceph/$cluster\-$name.log

**log max new**

Description

新しいログファイルの最大数

Type

Integer

Required

No

Default

1000

**log max recent**

Description

ログファイルに含める最近のイベントの最大数

Type

Integer

Required

No

Default

10000

**log to file**

Description

ロギングメッセージをファイルに表示するかどうか

Type

Boolean

Required

No

Default

true

**log to stderr**

Description

ロギングメッセージを標準エラー出力に表示するかどうか

Type

Boolean

Required

No

Default

false

**err to stderr**

Description

エラーメッセージを標準エラー出力に表示するかどうか

Type

Boolean

Required

No

Default

true

**log to syslog**

Description

ロギングメッセージがsyslogに表示されるかどうか

Type

Boolean

Required

No

Default

false

**err to syslog**

Description

エラーメッセージを syslog に表示するかどうか

Type

Boolean

Required

No

Default

false

**log flush on exit**

Description

Cephが終了後にログファイルをフラッシュするかどうか

Type

Boolean

Required

No

Default

false

**clog to monitors**

Description

clogメッセージがモニターに送信されるかどうか

Type

Boolean

Required

No

Default

true

**clog to syslog**

Description

clogメッセージがsyslogに送信されるかどうか

Type

Boolean

Required

No

Default

false

**mon cluster log to syslog**

Description

クラスタログを syslog に出力するかどうか

Type

Boolean

Required

No

Default

false

**mon cluster log file**

Description

クラスタのログファイルの場所。Cephには、クラスタと監査という2つのチャネルがあります。このオプションは、チャネルからログファイルへのマッピングを表し、そのチャネルのログエントリが送信される場所を指定します。デフォルトのエントリは、明示的に指定されていないチャネルに対するフォールバックマッピングです。したがって、以下のデフォルト設定では、クラスタログは$cluster.logに送信され、監査ログは$cluster.audit.logに送信され、$clusterは実際のクラスタ名で置き換えられます。

Type

String

Required

No

Default

default=/var/log/ceph/$cluster.$channel.log,cluster=/var/log/ceph/$cluster.log

### OSD[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/log-and-debug/ "Permalink to this headline")

**osd debug dropping probability**

Description

?

Type

Double

Required

No

Default

0

**osd debug drop ping duration**

Description

Type

Integer

Required

No

Default

0

**osd debug drop pg create probability**

Description

Type

Integer

Required

No

Default

0

**osd debug drop pg create duration**

Description

?

Type

Double

Required

No

Default

1

**osd min pg log entries**

Description

PGのログエントリーの最小数

Type

32\-bit Unsigned Integer

Required

No

Default

250

**osd op log threshold**

Description

1回のパスで表示されるopログメッセージの数

Type

Integer

Required

No

Default

5

### Filestore[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/log-and-debug/ "Permalink to this headline")

**filestore debug omap check**

Description

同期に関するデバッグ・チェック。これは高価な操作です

Type

Boolean

Required

No

Default

false

### MDS[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/log-and-debug/ "Permalink to this headline")

**mds debug scatter stat**

Description

Cephは、さまざまな再帰的stat不変量が真であることを表明する\(開発者のみ対象\)

Type

Boolean

Required

No

Default

false

**mds debug frag**

Description

Cephは、都合の良いときにディレクトリの断片化の不変条件を検証する\(開発者のみ\)

Type

Boolean

Required

No

Default

false

**mds debug auth pins**

Description

デバッグ認証ピンの不変条件（開発者のみ）

Type

Boolean

Required

No

Default

false

**mds debug sub trees**

Description

デバッグサブツリーの不変条件（開発者のみ）

Type

Boolean

Required

No

Default

false

### RADOS Gateway[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/log-and-debug/ "Permalink to this headline")

**rgw log nonexistent bucket**

Description

存在しないバケットを記録すべきか

Type

Boolean

Required

No

Default

false

**rgw log object name**

Description

オブジェクトの名前をログに記録すべきかどうか // コードを表示する日付を指定

Type

String

Required

No

Default

%Y\-%m\-%d\-%H\-%i\-%n

**rgw log object name utc**

Description

オブジェクトのログ名にUTCが含まれているか

Type

Boolean

Required

No

Default

false

**rgw enable ops log**

Description

RGWのすべての操作のロギングを有効にする

Type

Boolean

Required

No

Default

true

**rgw enable usage log**

Description

RGWの帯域幅使用量のロギングを有効にする

Type

Boolean

Required

No

Default

false

**rgw usage log flush threshold**

Description

保留中のログデータをフラッシュする閾値

Type

Integer

Required

No

Default

1024

**rgw usage log tick interval**

Description

保留中のログデータをs秒ごとにフラッシュする

Type

Integer

Required

No

Default

30

**rgw intent log object name**

Description

Type

String

Required

No

Default

%Y\-%m\-%d\-%i\-%n

**rgw intent log object name utc**

Description

インテントログオブジェクト名にUTCタイムスタンプを含める

Type

Boolean

Required

No

Default

false

[1](https://docs.ceph.com/en/pacific/rados/troubleshooting/log-and-debug/)

まれに20を超えるレベルもあり、非常に冗長である
