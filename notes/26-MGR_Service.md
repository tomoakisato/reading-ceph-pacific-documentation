# 26: MGR Service

**クリップソース:** [26: MGR Service — Ceph Documentation](https://docs.ceph.com/en/pacific/cephadm/services/mgr/)

# MGR Service[¶](https://docs.ceph.com/en/pacific/cephadm/services/mgr/ "Permalink to this headline")

cephadm MGRサービスは、[Ceph Dashboard](https://docs.ceph.com/en/pacific/cephadm/services/mgr/) やcephadm managerモジュールなど、さまざまなモジュールをホストしています。

[296: Ceph Dashboard](296-Ceph_Dashboard.md)

## Specifying Networks[¶](https://docs.ceph.com/en/pacific/cephadm/services/mgr/ "Permalink to this headline")

MGRサービスは、ネットワーク内の特定のIPへのバインディングのみをサポートしています。

スペックファイルの例（デフォルトの配置を利用した場合）。

```
service_type: mgr
networks:
- 192.169.142.0/24

```

### Allow co\-location of MGR daemons[¶](https://docs.ceph.com/en/pacific/cephadm/services/mgr/ "Permalink to this headline")

ホストが1台だけの展開シナリオでは、cephadmは少なくとも2つのMGRデーモンを展開する必要があります。詳細については、 [ceph\-mgr administrator’s guide](https://docs.ceph.com/en/pacific/cephadm/services/mgr/) のmgr\_standby\_modulesを参照してください。

[293: ceph\-mgr administrator’s guide](293-ceph-mgr_administrator’s_guide.md)

### Further Reading[¶](https://docs.ceph.com/en/pacific/cephadm/services/mgr/ "Permalink to this headline")

* [Ceph Manager Daemon](https://docs.ceph.com/en/pacific/cephadm/services/mgr/)
* [Manually deploying a MGR daemon](https://docs.ceph.com/en/pacific/cephadm/services/mgr/)
