# 330: Ceph Internals — Ceph Documentation

 # Ceph Internals[¶](https://docs.ceph.com/en/pacific/dev/internals/ "Permalink to this headline")

Note:

If you’re looking for how to use Ceph as a library from your own software, please see [API Documentation](https://docs.ceph.com/en/pacific/dev/internals/).

You can start a development mode Ceph cluster, after compiling the source, with:

```
cd build
OSD=3 MON=3 MGR=3 ../src/vstart.sh -n -x
# check that it's there
bin/ceph health
```

Mailing list

The `dev@ceph.io` list is for discussion about the development of Ceph, its interoperability with other technology, and the operations of the project itself. Subscribe by sending a message to `dev-request@ceph.io`with the line:

```
subscribe ceph-devel
```

in the body of the message.

The [ceph\-devel@vger.kernel.org](https://docs.ceph.com/en/pacific/dev/internals/) list is for discussion and patch review for the Linux kernel Ceph client component. Subscribe by sending a message to `majordomo@vger.kernel.org` with the line:

```
subscribe ceph-devel
```

in the body of the message.
