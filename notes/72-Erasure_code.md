# 72: Erasure code

**クリップソース:** [72: Erasure code — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/operations/erasure-code/)

# Erasure code[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code/ "Permalink to this headline")

Cephプールは、OSD\(ほとんどの場合、ディスクごとに1つのOSDがあるため、ディスク\)の損失を維持するためにタイプに関連付けられます。プール作成時のデフォルトの選択肢はreplicatedで、すべてのオブジェクトが複数のディスクにコピーされることを意味します。スペースを節約するために、代わりにECプールタイプを使用することができます。

## Creating a sample erasure coded pool[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code/ "Permalink to this headline")

最も単純なEC化プールはRAID5と同等で、少なくとも3台のホストを必要とします。

```
$ ceph osd pool create ecpool erasure
pool 'ecpool' created
$ echo ABCDEFGHI | rados --pool ecpool put NYAN -
$ rados --pool ecpool get NYAN -
ABCDEFGHI
```

## Erasure code profiles[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code/ "Permalink to this headline")

デフォルトのECプロファイルは、2つのOSDの損失を維持します。これは、サイズ3の複製されたプールと同等ですが、1TBのデータを保存するために3TBの代わりに2TBを必要とします。デフォルト・プロファイルは、次のように表示できます。

```
$ ceph osd erasure-code-profile get default
k=2
m=2
plugin=jerasure
crush-failure-domain=host
technique=reed_sol_van
```

正しいプロファイルを選択することは、プール作成後に変更することができないため重要です。異なるプロファイルを持つ新しいプールを作成し、以前のプールから新しいプールにすべてのオブジェクトを移動する必要があります。

プロファイルの最も重要なパラメータは、K、M、crush\-failure\-domain であり、これらはストレージのオーバーヘッドとデータの耐久性を定義するものだからです。例えば、目的のアーキテクチャが、67%のオーバーヘッドのストレージで2つのラックの損失を維持しなければならない場合、次のプロファイルを定義することができます。

```
$ ceph osd erasure-code-profile set myprofile 
   k=3 
   m=2 
   crush-failure-domain=rack
$ ceph osd pool create ecpool erasure myprofile
$ echo ABCDEFGHI | rados --pool ecpool put NYAN -
$ rados --pool ecpool get NYAN -
ABCDEFGHI
```

NYANオブジェクトは3つに分割され（K=3）、さらに2つのチャンクが作成されます（M=2）。Mの値は、どのように多くのOSDがデータを失うことなく同時に失われることができるかを定義します。crush\-failure\-domain=rack は、2つのチャンクが同じラックに保存されないことを保証するCRUSHルールを作成します。

詳細については、[erasure code profiles](https://docs.ceph.com/en/pacific/rados/operations/erasure-code/)のドキュメントを参照してください。 [73: Erasure code profiles](73-Erasure_code_profiles.md)

## Erasure Coding with Overwrites[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code/ "Permalink to this headline")

デフォルトでは、ECプールは、RGWのように完全なオブジェクトの書き込みと追加を実行する用途でのみ機能します。

Luminous以降では、ECプールの部分書き込みは、プールごとの設定で有効にすることができます。これにより、RBDとCephFSは、ECプールにデータを保存できます。

```
ceph osd pool set ec_pool allow_ec_overwrites true
```

bluestoreのチェックサムは、ディープスクラブ中にビットロットやその他の破損を検出するために使用されるため、これはbluestore OSDに存在するプールでのみ有効にすることが可能です。EC オーバーライトでfilestoreを使用すると、安全でないことに加え、bluestore と比較してパフォーマンスが低下します。

ECプールはomapをサポートしていないため、RBDやCephFSで使用するには、データをECプールに、メタデータをreplicatedプールに保存するように指示する必要があります。RBDの場合、これはイメージ作成時にECプールを\-data\-poolとして使用することを意味します。

```
rbd create --size 1G --data-pool ec_pool replicated_pool/image_name
```

CephFSの場合、ファイルシステム作成時またはファイルレイアウトにより、ECプールをデフォルトのデータプールとして設定できます。

## Erasure coded pool and cache tiering[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code/ "Permalink to this headline")

ECプールは、replicatedプールよりも多くのリソースを必要とし、omapなどのいくつかの機能が欠けています。これらの制限を克服するために、ECプールの前にキャッシュ階層を設定することができます。

例えば、プールのホットストレージが高速ストレージでできている場合：

```
$ ceph osd tier add ecpool hot-storage
$ ceph osd tier cache-mode hot-storage writeback
$ ceph osd tier set-overlay ecpool hot-storage
```

上記は、ホットストレージプールを ecpool の階層としてライトバックモードで配置し、ecpool への書き込みと読み出しがホットストレージを使用し、その柔軟性と速度の恩恵を受けるようにします。

詳細については、[cache tiering](https://docs.ceph.com/en/pacific/rados/operations/erasure-code/)のドキュメントを参照してください。[79: Cache Tiering](79-Cache_Tiering.md)

## Erasure coded pool recovery[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code/ "Permalink to this headline")

ECプールは、いくつかのシャードを失った場合、他のシャードからそれを回復する必要があります。これには一般に、残りのシャードから読み込んでデータを再構築し、新しいピアに書き込むことが必要です。Octopusでは、ECプールは、少なくともK個のシャードが利用可能である限り、回復することができます。\(K個より少ないシャードでは、データを失ったことになります\!）

Octopus以前は、ECプールは、min\_sizeがKより大きい場合でも、少なくともmin\_sizeのシャードを利用できる必要がありました\(書き込みとデータの損失を防ぐために、一般的にmin\_sizeはK\+2以上であることを推奨しています\)。この保守的な決定は、新しいプールモードを設計する際に慎重を期して行われましたが、これは、OSDを失ったがデータ損失はないプールが、min\_sizeを変更するための手動介入なしに回復およびアクティブになることができないことを意味しました。

## Glossary[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code/ "Permalink to this headline")

**_chunk_**

エンコーディング関数が呼ばれると、同じ大きさのチャンクが返されます。データチャンクは元のオブジェクトを再構築するために連結することができ、コーディングチャンクは失われたチャンクを再構築するために使用することができます。

**_K_**

データチャンクの数、つまり元のオブジェクトが分割されるチャンクの数。例えば、K = 2の場合、10KBのオブジェクトは5KBずつのK個のオブジェクトに分割されます。

**_M_**

コーディングチャンクの数、すなわちエンコーディング関数によって計算される追加チャンクの数。コーディングチャンクが2つあれば、データを失うことなく2つのOSDをoutにできることを意味します。Table of content[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code/ "Permalink to this headline")

* [Erasure code profiles](https://docs.ceph.com/en/pacific/rados/operations/erasure-code/)
* [Jerasure erasure code plugin](https://docs.ceph.com/en/pacific/rados/operations/erasure-code/)
* [ISA erasure code plugin](https://docs.ceph.com/en/pacific/rados/operations/erasure-code/)
* [Locally repairable erasure code plugin](https://docs.ceph.com/en/pacific/rados/operations/erasure-code/)
* [SHEC erasure code plugin](https://docs.ceph.com/en/pacific/rados/operations/erasure-code/)
* [CLAY code plugin](https://docs.ceph.com/en/pacific/rados/operations/erasure-code/)
