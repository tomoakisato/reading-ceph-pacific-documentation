# 210: iSCSI Initiator for VMware ESX

**クリップソース:** [210: iSCSI Initiator for VMware ESX — Ceph Documentation](https://docs.ceph.com/en/pacific/rbd/iscsi-initiator-esx/)

# iSCSI Initiator for VMware ESX[¶](https://docs.ceph.com/en/pacific/rbd/iscsi-initiator-esx/ "Permalink to this headline")

**Prerequisite:**

* VMware ESX 6.5以降（Virtual Machine compatibility 6.5とVMFS 6を使用する）

**iSCSI Discovery and Multipath Device Setup:**

以下の手順では、デフォルトのvSphere Webクライアントとesxcliを使用します。

1. ![esx_chap.png](image/esx_chap.png)
    「Navigator」から「Storage」をクリックし、「Adapters」タブを選択します。そこから「Configure iSCSI」を右クリックします。
    Enable Software iSCSI
2. ![esx_iscsi_recov_timeout.png](image/esx_iscsi_recov_timeout.png)
    Name & alias "セクションのイニシエータ名が、gwcli セットアップ中にクライアントを作成したときに使用した名前、または ansible client\_connections クライアント変数で使用したイニシエータ名と異なる場合、 ESX ホストに ssh して以下の esxcli コマンドを実行して名前を変更します。
    Software iSCSI のアダプタ名を取得します：
    Set Initiator Name

```
> esxcli iscsi adapter list
> Adapter  Driver     State   UID            Description
> -------  ---------  ------  -------------  ----------------------
> vmhba64  iscsi_vmk  online  iscsi.vmhba64  iSCSI Software Adapter
```

この例では、ソフトウェアiSCSIアダプタはvmhba64で、イニシエータ名はiqn.1994\-05.com.redhat:rh7\-clientとなっています：

```
> esxcli iscsi adapter set -A vmhba64 -n iqn.1994-05.com.redhat:rh7-client
```

1. Setup CHAP

![esx_web_client_storage_main.png](image/esx_web_client_storage_main.png)

CHAP 認証セクションを展開し、「Do not use CHAP unless required by target」を選択し、gwcli auth コマンドまたは ansible client\_connections credentials 変数で使用した CHAP 認証を入力します。

Mutual CHAP認証の項目は、「CHAPを使用しない」が選択されている必要があります。

警告： ウェブクライアントには、要求されたCHAP設定が必ずしも初期に使用されないというバグがあります。iSCSIゲートウェイのカーネルログで、このエラーが表示されます：

```
> kernel: CHAP user or password not set for Initiator ACL
> kernel: Security negotiation failed.
> kernel: iSCSI Login negotiation failed.
```

これを回避するために、esxcli コマンドで CHAP 設定を行います。ここで、authname はユーザー名、secret は前の例で使用したパスワードです：

```
> esxcli iscsi adapter auth chap set --direction=uni --authname=myiscsiusername --secret=myiscsipassword --level=discouraged -A vmhba64
```

1. Configure iSCSI Settings

![esx_config_iscsi_main.png](image/esx_config_iscsi_main.png)

詳細設定を展開し、「RecoveryTimeout」を25に設定します。

1. Set the discovery address

%\!\(EXTRA markdown.ResourceType=, string=, string=\)

Dynamic targetsセクションで、「Add dynamic target」をクリックし、「Addresses」の下に、gwcli セクションの iSCSI ゲートウェイのセットアップステージで追加したゲートウェイ IP アドレスの 1 つ、または ansible gateway\_ip\_list 変数に設定した IP を追加してください。ゲートウェイがセットアップされているため、ディスカバリー中にすべての iSCSI ポータルが返されるので、1 つのアドレスのみを追加する必要があります。

最後に「Save configuration」ボタンをクリックします。Devicesタブに、RBDイメージが表示されているはずです。

LUN は自動的に設定され、ALUA SATP と MRU PSP を使用する必要があります。他の SATP と PSP は使用してはいけません。これは、esxcli コマンドで確認することができます：

```
> esxcli storage nmp path list -d eui.your_devices_id
```
