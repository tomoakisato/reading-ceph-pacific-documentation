# 453: activate

**クリップソース:** [453: activate — Ceph Documentation](https://docs.ceph.com/en/pacific/ceph-volume/simple/activate/)

# activate[¶](https://docs.ceph.com/en/pacific/ceph-volume/simple/activate/ "Permalink to this headline")

[scan](https://docs.ceph.com/en/pacific/ceph-volume/simple/scan/#ceph-volume-simple-scan)が完了し、OSDに取り込まれたすべてのメタデータが /etc/ceph/osd/{id}\-{uuid}.json に保存されると、OSDは「activate」する準備が整います。

このアクティベーションプロセスは、ブート時に起動しようとするUDEV/ceph\-diskの相互作用を防ぐために、ceph\-disk systemdユニットをすべて無効化します。

ceph\-diskユニットの無効化は、ceph\-volume simple activateを直接呼び出す場合のみ行われ、システムの起動時にsystemdから呼び出される場合は回避されます。

アクティベーションプロセスでは、OSD id と OSD uuid の両方を使用する必要があります パースされた OSD をアクティベーションするには：

```
ceph-volume simple activate 0 6cc43680-4f6e-4feb-92ff-9c7ba204120e
```

上記のコマンドは、下記のJSONのコンフィギュレーションが見つかると想定しています：

```
/etc/ceph/osd/0-6cc43680-4f6e-4feb-92ff-9c7ba204120e.json
```

JSONファイルのパスを直接使用することも可能です：

```
ceph-volume simple activate --file /etc/ceph/osd/0-6cc43680-4f6e-4feb-92ff-9c7ba204120e.json
```

## requiring uuids[¶](https://docs.ceph.com/en/pacific/ceph-volume/simple/activate/ "Permalink to this headline")

OSD uuidは、正しいOSDが起動されていることを確認するための追加ステップとして要求されています。同じ ID を持つ以前の OSD が存在し、間違ったものを起動することになる可能性は十分にあります。

### Discovery[¶](https://docs.ceph.com/en/pacific/ceph-volume/simple/activate/ "Permalink to this headline")

ceph\-volumeによって事前にスキャンされたOSDでは、blkidとlvmを使用して検出プロセス検出プロセスにより、デバイスが別のシステムに再利用されたり、（/dev/sda1 のように永続的でない名前の場合など）名前が変わったりしても、正しく検出されるようになります。が実行されます。現在、GPTパーティションとLVM論理ボリュームを持つデバイスのみサポートします。

GPTパーティションはPARTUUIDを持ち、blkidを呼び出して問い合わせることができ、論理ボリュームはlv\_uuidを持ち、lvs（論理ボリュームをリストするLVMツール）に対して問い合わせることができます。

検出プロセスにより、デバイスが別のシステムに再利用されたり、（/dev/sda1 のように永続的でない名前の場合など）名前が変わったりしても、正しく検出されるようになります。

どのデバイスがどのOSDに対応するかをマッピングするために使用されるJSON設定ファイルは、アクティベーションの一部としてマウントとシンボリックリンク作成を行います。

シンボリックリンクを常に正しくするため、OSDディレクトリにシンボリックリンクが存在する場合は、シンボリックリンクを作成し直します。

systemd ユニットが OSD id と OSD uuid をキャプチャし、それを永続化します。内部的には、アクティベーションによって、以下のように有効化されます：

```
systemctl enable ceph-volume@simple-$id-$uuid
```

For example:

```
systemctl enable ceph-volume@simple-0-8715BEB4-15C5-49DE-BA6F-401086EC7B41
```

上記は、0のIDと8715BEB4\-15C5\-49DE\-BA6F\-401086EC7B41のUUIDを持つOSDの検出プロセスを開始します。

systemdプロセスは、OSDとそのデバイスを識別するために必要な情報を渡してactivateするように呼び出され、以下のように進みます：

\# 対応する場所にデバイスをマウントします \(慣習的にこれは /var/lib/ceph/osd/\<clustername\>\-\<osdid\>/\)

\# 使用されるオブジェクトストア（filestoreまたはbluestore）に関係なく、すべての必要なデバイスがその OSD に対して準備できていて、適切にリンクされていることを確認します。シンボリックリンクは常に再作成され、正しいデバイスがリンクされていることを保証します。

\# ceph\-osd@0 systemd ユニットを起動します。
