# 320: Commit merging: scope and cadence

 # Commit merging: scope and cadence[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/merging/ "Permalink to this headline")

Commits are merged into branches according to criteria specific to each phase of the Ceph release lifecycle. This chapter codifies these criteria.

## Development releases \(i.e. x.0.z\)[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/merging/ "Permalink to this headline")

### What ?[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/merging/ "Permalink to this headline")

* Features
* Bug fixes

### Where ?[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/merging/ "Permalink to this headline")

Features are merged to the _master_ branch. Bug fixes should be merged to the corresponding named branch \(e.g. _nautilus_ for 14.0.z, _pacific_ for 16.0.z, etc.\). However, this is not mandatory \- bug fixes and documentation enhancements can be merged to the _master_ branch as well, since the _master_branch is itself occasionally merged to the named branch during the development releases phase. In either case, if a bug fix is important it can also be flagged for backport to one or more previous stable releases.

### When ?[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/merging/ "Permalink to this headline")

After each stable release, candidate branches for previous releases enter phase 2 \(see below\). For example: the _jewel_ named branch was created when the _infernalis_ release candidates entered phase 2. From this point on,_master_ was no longer associated with _infernalis_. After he named branch of the next stable release is created, _master_ will be occasionally merged into it.

### Branch merges[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/merging/ "Permalink to this headline")

* The latest stable release branch is merged periodically into master.
* The master branch is merged periodically into the branch of the stable release.
* The master is merged into the stable release branch immediately after each development \(x.0.z\) release.

## Stable release candidates \(i.e. x.1.z\) phase 1[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/merging/ "Permalink to this headline")

### What ?[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/merging/ "Permalink to this headline")

* Bug fixes only

### Where ?[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/merging/ "Permalink to this headline")

The stable release branch \(e.g. _jewel_ for 10.0.z, _luminous_for 12.0.z, etc.\) or _master_. Bug fixes should be merged to the named branch corresponding to the stable release candidate \(e.g. _jewel_ for 10.1.z\) or to _master_. During this phase, all commits to _master_ will be merged to the named branch, and vice versa. In other words, it makes no difference whether a commit is merged to the named branch or to_master_ \- it will make it into the next release candidate either way.

### When ?[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/merging/ "Permalink to this headline")

After the first stable release candidate is published, i.e. after the x.1.0 tag is set in the release branch.

### Branch merges[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/merging/ "Permalink to this headline")

* The stable release branch is merged periodically into _master_.
* The _master_ branch is merged periodically into the stable release branch.
* The _master_ branch is merged into the stable release branch immediately after each x.1.z release candidate.

## Stable release candidates \(i.e. x.1.z\) phase 2[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/merging/ "Permalink to this headline")

### What ?[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/merging/ "Permalink to this headline")

* Bug fixes only

### Where ?[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/merging/ "Permalink to this headline")

The stable release branch \(e.g. _mimic_ for 13.0.z, _octopus_ for 15.0.z ,etc.\). During this phase, all commits to the named branch will be merged into_master_. Cherry\-picking to the named branch during release candidate phase 2 is performed manually since the official backporting process begins only when the release is pronounced “stable”.

### When ?[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/merging/ "Permalink to this headline")

After Sage Weil announces that it is time for phase 2 to happen.

### Branch merges[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/merging/ "Permalink to this headline")

* The stable release branch is occasionally merged into master.

## Stable releases \(i.e. x.2.z\)[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/merging/ "Permalink to this headline")

### What ?[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/merging/ "Permalink to this headline")

* Bug fixes
* Features are sometime accepted
* Commits should be cherry\-picked from _master_ when possible
* Commits that are not cherry\-picked from _master_ must pertain to a bug unique to the stable release
* See also the [backport HOWTO](https://docs.ceph.com/en/pacific/dev/developer_guide/merging/) document

### Where ?[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/merging/ "Permalink to this headline")

The stable release branch \(_hammer_ for 0.94.x, _infernalis_ for 9.2.x, etc.\)

### When ?[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/merging/ "Permalink to this headline")

After the stable release is published, i.e. after the “vx.2.0” tag is set in the release branch.

### Branch merges[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/merging/ "Permalink to this headline")

Never
