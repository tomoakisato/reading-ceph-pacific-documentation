# 33: Monitoring Services

**クリップソース:** [33: Monitoring Services — Ceph Documentation](https://docs.ceph.com/en/pacific/cephadm/services/monitoring/)

# Monitoring Services[¶](https://docs.ceph.com/en/pacific/cephadm/services/monitoring/ "Permalink to this headline")

Ceph Dashboardは、Prometheus、Grafana、および関連ツールを使用して、クラスタの使用率とパフォーマンスに関する詳細なメトリクスを保存して視覚化します。 Cephのユーザーには3つのオプションがあります。

1. cephadmにこれらのサービスを展開および設定させます。 これは、\-\-skip\-monitoring\-stackオプションが使用されていない限り、新しいクラスタをブートストラップするときのデフォルトです。
2. これらのサービスを手動でデプロイおよび設定します。 これは、環境に既存のprometheusサービスがあるユーザーにお勧めします\(また、CephがRookを使用してKubernetesで実行されている場合も同様です\)。
3. モニタリングスタックを完全にスキップします。 一部のCephダッシュボードのグラフが使用できなくなります。

モニタリングスタックは、[Prometheus](https://docs.ceph.com/en/pacific/cephadm/services/monitoring/)、Prometheusエクスポーター\([Prometheus Module](https://docs.ceph.com/en/pacific/cephadm/services/monitoring/), [Node exporter](https://docs.ceph.com/en/pacific/cephadm/services/monitoring/)\)、 [Prometheus Alert Manager](https://docs.ceph.com/en/pacific/cephadm/services/monitoring/)、[Grafana](https://docs.ceph.com/en/pacific/cephadm/services/monitoring/)で構成されています。 

注意：Prometheusのセキュリティモデルは、信頼されていないユーザーがPrometheusのHTTPエンドポイントとログにアクセスできることを前提としています。信用されていないユーザーは、Prometheusが収集したデータベースに含まれるすべての（メタ）データに加えて、さまざまな操作情報やデバッグ情報にアクセスすることができます。

しかし、PrometheusのHTTP APIは、読み取り専用の操作に限定されています。APIを使って設定を変更することはできませんし、シークレットも公開されません。さらに、Prometheusには、サービス拒否攻撃の影響を軽減するためのいくつかの対策が組み込まれています。

より詳細な情報については、Prometheusのセキュリティモデル\<https://prometheus.io/docs/operating/security/\>をご覧ください。

## Deploying monitoring with cephadm[¶](https://docs.ceph.com/en/pacific/cephadm/services/monitoring/ "Permalink to this headline")

cephadmのデフォルトの動作は、基本的な監視スタックを展開することです。 ただし、監視スタックのないCephクラスタがあり、そこに監視スタックを追加したい場合もあります。\(監視スタックのないCephクラスターになってしまった場合、以下のような方法があります。クラスタのインストール時にcephadmに\-\-skip\-monitoringstackオプションを渡した場合や、\(監視スタックがない\)既存のクラスタをcephadm管理に変換した場合などが考えられます\)。

モニタリングが行われていないCephクラスタにモニタリングを設定するには、以下の手順に従います。

クラスタの各ノードにNode\-exporterサービスを導入します。 ノード・エクスポーターは、CPUやメモリの使用率など、ホストレベルのメトリクスを提供します。

```
# ceph orch apply node-exporter
```

alertmanagerをデプロイします。

```
# ceph orch apply alertmanager
```

Prometheusを導入する。Prometheusは1台で十分ですが、HA\(High Availablility\)のためには2台導入したほうがよいでしょう。

```
# ceph orch apply prometheus
```

or

```
# ceph orch apply prometheus --placement 'count:2'
```

grafanaをデプロイします。

```
# ceph orch apply grafana
```

### Networks and Ports[¶](https://docs.ceph.com/en/pacific/cephadm/services/monitoring/ "Permalink to this headline")

すべての監視サービスは、バインドするネットワークとポートを yaml サービス仕様で設定できます。

スペックファイルの例

```
service_type: grafana
service_name: grafana
placement:
  count: 1
networks:
- 192.169.142.0/24
spec:
  port: 4200
```

### Using custom images[¶](https://docs.ceph.com/en/pacific/cephadm/services/monitoring/ "Permalink to this headline")

他のイメージに基づいて監視コンポーネントをインストールまたはアップグレードすることが可能です。 そのためには、まず使用するイメージの名前を設定に保存しておく必要があります。 以下の設定オプションがあります。

* container\_image\_prometheus
* container\_image\_grafana
* container\_image\_alertmanager
* container\_image\_node\_exporter

カスタムイメージは_ceph config_コマンドで設定できます。

```
ceph config set mgr mgr/cephadm/<option_name> <value>
```

For example

```
ceph config set mgr mgr/cephadm/container_image_prometheus prom/prometheus:v1.4.1
```

イメージを変更したタイプの監視スタックデーモンが既に動作している場合、新しいイメージを実際に使用するためには、デーモンを再デプロイする必要があります。

例えば、prometheusのイメージを変更した場合、次のようになります。

```
# ceph orch redeploy prometheus
```

カスタムイメージを設定すると、デフォルト値がオーバライドされます（上書きはされません）。 デフォルト値は、アップデートが可能になると変更されます。カスタムイメージを設定すると、カスタムイメージを設定したコンポーネントを自動的に更新することはできません。 アップデートをインストールできるようにするには、手動で設定（イメージ名とタグ）を更新する必要があります。

代わりに推奨される方法を選択した場合は、以前に設定したカスタムイメージをリセットすることができます。 それ以降は、再びデフォルト値が使用されます。 _ceph config rm_を使用して設定オプションをリセットする

```
ceph config rm mgr mgr/cephadm/<option_name>
```

For example

```
ceph config rm mgr mgr/cephadm/container_image_prometheus
```

### Using custom configuration files[¶](https://docs.ceph.com/en/pacific/cephadm/services/monitoring/ "Permalink to this headline")

cephadmのテンプレートをオーバーライドすることで、監視サービスの設定ファイルを完全にカスタマイズすることができます。

内部的には、cephadmはすでにJinja2テンプレートを使用してすべての監視コンポーネントの設定ファイルを生成しています。Prometheus、Grafana、Alertmanagerの設定をカスタマイズするには、各サービス用のJinja2テンプレートを保存して、設定の生成に使用することができます。このテンプレートは、その種類のサービスがデプロイまたは再構成されるたびに評価されます。このようにして、カスタム設定が保存され、将来のサービスのデプロイ時に自動的に適用されるようになります。

注意:カスタムテンプレートの設定は、cephadmのデフォルト設定が変更された場合にも保存されます。更新された設定を使用する場合は、カスタムテンプレートを手動で移行する必要があります。

#### Option names[¶](https://docs.ceph.com/en/pacific/cephadm/services/monitoring/ "Permalink to this headline")

cephadmによって生成されるファイルの以下のテンプレートをオーバーライドできます。これらは、cephconfig\-keysetで保存するときに使用される名前です。

* services/alertmanager/alertmanager.yml
* services/grafana/ceph\-dashboard.yml
* services/grafana/grafana.ini
* services/prometheus/prometheus.yml

cephadmが現在使用しているファイルテンプレートは、src/pybind/mgr/cephadm/templatesで調べることができます。

* services/alertmanager/alertmanager.yml.j2
* services/grafana/ceph\-dashboard.yml.j2
* services/grafana/grafana.ini.j2
* services/prometheus/prometheus.yml.j2

#### Usage[¶](https://docs.ceph.com/en/pacific/cephadm/services/monitoring/ "Permalink to this headline")

次のコマンドは、1行の値を適用します。

```
ceph config-key set mgr/cephadm/<option_name> <value>
```

ファイルの内容をテンプレートとして設定するには、\-i 引数を使用します。

```
ceph config-key set mgr/cephadm/<option_name> -i $PWD/<filename>
```

注意：config\-keyの入力にファイルを使用する場合は、ファイルの絶対パスを使用する必要があります。

次に、サービスの設定ファイルを再作成する必要があります。これにはreconfigを使用します。詳細は以下の例を参照してください。

#### Example[¶](https://docs.ceph.com/en/pacific/cephadm/services/monitoring/ "Permalink to this headline")

```
# set the contents of ./prometheus.yml.j2 as template
ceph config-key set mgr/cephadm/services/prometheus/prometheus.yml 
  -i $PWD/prometheus.yml.j2

# reconfig the prometheus service
ceph orch reconfig prometheus
```

## Deploying monitoring without cephadm[¶](https://docs.ceph.com/en/pacific/cephadm/services/monitoring/ "Permalink to this headline")

既存のprometheusモニタリングインフラストラクチャをお持ちの場合、または自分で管理したい場合は、Cephクラスタと統合するように設定する必要があります。

* ceph\-mgrデーモンでprometheusモジュールを有効にする。デフォルトでは、ceph\-mgrは、ceph\-mgrデーモンを実行している各ホストのポート9283にprometheusメトリクスを提示します。 これらをスクレイプするようにprometheusを設定します。
* ダッシュボードのPrometheusベースのアラートを有効にするには、「 
* Grafanaとのダッシュボードの統合を有効にするには、「 

## Disabling monitoring[¶](https://docs.ceph.com/en/pacific/cephadm/services/monitoring/ "Permalink to this headline")

モニタリングを無効にし、それをサポートするソフトウェアを削除するには、以下のコマンドを実行します。

```
$ ceph orch rm grafana
$ ceph orch rm prometheus --force   # this will delete metrics data collected so far
$ ceph orch rm node-exporter
$ ceph orch rm alertmanager
$ ceph mgr module disable prometheus
```

[Removing a Service](https://docs.ceph.com/en/pacific/cephadm/services/monitoring/)も参照してください。

## Setting up RBD\-Image monitoring[¶](https://docs.ceph.com/en/pacific/cephadm/services/monitoring/ "Permalink to this headline")

パフォーマンス上の理由から、RBDイメージの監視はデフォルトでは無効になっています。詳細については、[RBD IO statistics](https://docs.ceph.com/en/pacific/cephadm/services/monitoring/)を参照してください。無効にすると、Grafanaでは概要と詳細のダッシュボードが空のままになり、Prometheusではメトリクスが表示されなくなります。 

## Setting up Grafana[¶](https://docs.ceph.com/en/pacific/cephadm/services/monitoring/ "Permalink to this headline")

### Manually setting the Grafana URL[¶](https://docs.ceph.com/en/pacific/cephadm/services/monitoring/ "Permalink to this headline")

Cephadmは、1つのケースを除いて、Prometheus、Grafana、Alertmanagerを自動的に設定します。

一部のセットアップでは、Dashboardユーザのブラウザが、Ceph Dashboardで設定されたGrafanaのURLにアクセスできないことがあります。これは、クラスタとアクセスするユーザが異なるDNSゾーンにいる場合に起こります。

このような場合は、Ceph Dashboardの設定オプションを使用して、ユーザのブラウザがGrafanaにアクセスする際に使用するURLを設定できます。この値は、cephadmによって変更されることはありません。この設定オプションを設定するには、次のコマンドを実行します。

```
$ ceph dashboard set-grafana-frontend-api-url <grafana-server-api>
```

サービスがデプロイされるまで、1～2分かかることがあります。サービスがデプロイされた後、_ceph orch ls_コマンドを実行すると、以下のように表示されるはずです。

```
$ ceph orch ls
NAME           RUNNING  REFRESHED  IMAGE NAME                                      IMAGE ID        SPEC
alertmanager       1/1  6s ago     docker.io/prom/alertmanager:latest              0881eb8f169f  present
crash              2/2  6s ago     docker.io/ceph/daemon-base:latest-master-devel  mix           present
grafana            1/1  0s ago     docker.io/pcuzner/ceph-grafana-el8:latest       f77afcf0bcf6   absent
node-exporter      2/2  6s ago     docker.io/prom/node-exporter:latest             e5a616e4b9cf  present
prometheus         1/1  6s ago     docker.io/prom/prometheus:latest                e935122ab143  present
```

### Configuring SSL/TLS for Grafana[¶](https://docs.ceph.com/en/pacific/cephadm/services/monitoring/ "Permalink to this headline")

cephadmは、cephのキー/バリューストアで定義された証明書を使用してGrafanaを展開します。証明書が指定されていない場合、cephadmはGrafanaサービスのデプロイ時に自己署名証明書を生成します。

カスタム証明書は、次のコマンドを使用して設定できます。

```
# ceph config-key set mgr/cephadm/grafana_key -i $PWD/key.pem
# ceph config-key set mgr/cephadm/grafana_crt -i $PWD/certificate.pem
```

すでにGrafanaを導入している場合は、サービスに対してreconfigを実行し、設定を更新します。

```
# ceph orch reconfig grafana
```

reconfigコマンドは、Ceph Dashboardの適切なURLも設定します。

## Setting up Alertmanager[¶](https://docs.ceph.com/en/pacific/cephadm/services/monitoring/ "Permalink to this headline")

### Adding Alertmanager webhooks[¶](https://docs.ceph.com/en/pacific/cephadm/services/monitoring/ "Permalink to this headline")

Alertmanagerの設定に新しいWebhookを追加するには、次のように追加のWebhook urlsを追加します。

```
service_type: alertmanager
spec:
  user_data:
    default_webhook_urls:
    - "https://foo"
    - "https://bar"
```

default\_webhook\_urlsは、デフォルトの受信機の\<webhook\_configs\>構成に追加されるURLのリストです。

サービスに対してreconfigを実行し、設定を更新します。

```
# ceph orch reconfig alertmanager
```

## Further Reading[¶](https://docs.ceph.com/en/pacific/cephadm/services/monitoring/ "Permalink to this headline")

* [Prometheus Module](https://docs.ceph.com/en/pacific/cephadm/services/monitoring/)
