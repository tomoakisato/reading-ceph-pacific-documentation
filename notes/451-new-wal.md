# 451: new-wal

**クリップソース:** [451: new\-wal — Ceph Documentation](https://docs.ceph.com/en/pacific/ceph-volume/lvm/newwal/)

# new\-wal[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/newwal/ "Permalink to this headline")

指定された論理ボリュームをWALボリュームとして指定されたOSDにアタッチします。論理ボリュームの形式はvg/lvです。OSDにすでにWALがアタッチされている場合は失敗します。

vgname/lvnameをWALボリュームとしてOSD 1にアタッチするには：

```
ceph-volume lvm new-wal --osd-id 1 --osd-fsid 55BD4219-16A7-4037-BC20-0F158EFCC83D --target vgname/new_wal
```
