# 49: Network Configuration Reference

**クリップソース:** [49: Network Configuration Reference — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/configuration/network-config-ref/)

# Network Configuration Reference[¶](https://docs.ceph.com/en/pacific/rados/configuration/network-config-ref/ "Permalink to this headline")

ネットワークの設定は、高パフォーマンスのCeph Storage Clusterを構築するために重要です。Ceph Storage Clusterは、Cephクライアントに代わって要求のルーティングやディスパッチを実行しません。代わりに、CephクライアントはCeph OSDデーモンに直接要求を出します。Ceph OSDデーモンはCephクライアントに代わってデータレプリケーションを実行するため、レプリケーションなどによってCeph Storage Clusterのネットワークに追加の負荷がかかります。

当社のクイックスタート構成では、モニタのIPアドレスとデーモンのホスト名のみを設定する簡単なCeph構成ファイルを提供しています。クラスタネットワークを指定しない限り、Cephは1つの「パブリック」ネットワークを想定しています。Cephはパブリックネットワークのみでも問題なく機能しますが、大規模なクラスタでは2つ目の「クラスタ」ネットワークを使用すると、パフォーマンスが大幅に向上する場合があります。

Ceph Storage Clusterをパブリック\(クライアント、フロントサイド\)ネットワークとクラスタ\(プライベート、レプリケーション、バックサイド\)ネットワークの2つのネットワークで実行することは可能です。 ただし、この方法ではネットワークの設定\(ハードウェアとソフトウェアの両方\)が複雑になり、通常は全体のパフォーマンスに大きな影響はありません。 このため、耐障害性とキャパシティを考慮したデュアルNICシステムでは、これらのインターフェイスをアクティブ/アクティブに結合するか、FRRなどを用いたレイヤー3マルチパス戦略を導入することをお勧めします。

複雑ではあっても2つのネットワークを使用したい場合は、各Cephノードに複数のネットワークインタフェースまたはVLANが必要です。詳細は「[Hardware Recommendations \- Networks](https://docs.ceph.com/en/pacific/rados/configuration/network-config-ref/)」を参照してください。

## IP Tables[¶](https://docs.ceph.com/en/pacific/rados/configuration/network-config-ref/ "Permalink to this headline")

デフォルトでは、デーモンは6800:7300の範囲のポートにバインドします。この範囲は自由に設定できます。IPテーブルを設定する前に、デフォルトのiptablesの設定を確認してください。

```
sudo iptables -L
```

一部のLinuxディストリビューションでは、すべてのネットワークインターフェースからのSSH以外のすべてのインバウンドリクエストを拒否するルールが含まれています。例えば、以下のようなものです。

```
REJECT all -- anywhere anywhere reject-with icmp-host-prohibited
```

最初はパブリックネットワークとクラスタネットワークの両方でこれらのルールを削除し、Cephノードのポートをハード化する準備ができたら適切なルールに置き換える必要があります。

### Monitor IP Tables[¶](https://docs.ceph.com/en/pacific/rados/configuration/network-config-ref/ "Permalink to this headline")

Ceph Monitorsは、デフォルトでポート3300と6789をリッスンします。また、Ceph Monitorsは常にパブリックネットワーク上で動作します。以下の例を使用してルールを追加する際には、{iface}をパブリックネットワークのインターフェース\(eth0、eth1など\)、{ip\-address}をパブリックネットワークのIPアドレス、{netmask}をパブリックネットワークのネットマスクに置き換えることを確認してください。

```
sudo iptables -A INPUT -i {iface} -p tcp -s {ip-address}/{netmask} --dport 6789 -j ACCEPT
```

### MDS and Manager IP Tables[¶](https://docs.ceph.com/en/pacific/rados/configuration/network-config-ref/ "Permalink to this headline")

Ceph Metadata ServerまたはCeph Managerは、6800番ポートから始まるパブリックネットワーク上の最初の利用可能なポートでリッスンします。この動作は決定論的ではないので、同じホストで複数のOSDまたはMDSを実行している場合や、短い時間内にデーモンを再起動した場合は、デーモンがより高いポートにバインドされることに注意してください。デフォルトでは6800～7300の全範囲を開放する必要があります。 以下の例でルールを追加する際には、{iface}をパブリックネットワークのインターフェース（eth0、eth1など）に、{ip\-address}をパブリックネットワークのIPアドレスに、{netmask}をパブリックネットワークのネットマスクに置き換えることを確認してください。

For example:

```
sudo iptables -A INPUT -i {iface} -m multiport -p tcp -s {ip-address}/{netmask} --dports 6800:7300 -j ACCEPT
```

### OSD IP Tables[¶](https://docs.ceph.com/en/pacific/rados/configuration/network-config-ref/ "Permalink to this headline")

デフォルトでは、Ceph OSDデーモンは、6800番ポートから始まるCeph Nodeの最初の利用可能なポートにバインドします。 この動作は決定論的ではないため、同じホストで複数のOSDまたはMDSを実行している場合や、短い時間内にデーモンを再起動した場合は、デーモンはより高いポートにバインドされることに注意してください。Ceph Node上の各Ceph OSDデーモンは、最大4つのポートを使用できます。

1. クライアントやモニターとの会話
2. 他のOSDにデータを送るため
3. 各インターフェイスのハートビート用に2つ

デーモンが故障し、ポートを手放さずに再起動した場合、再起動したデーモンは新しいポートにバインドします。この可能性に対応するため、6800～7300のポート範囲全体を開放する必要があります。

パブリックネットワークとクラスタネットワークを別々に設定した場合、クライアントはパブリックネットワークを使用して接続し、他のCeph OSDデーモンはクラスタネットワークを使用して接続するため、パブリックネットワークとクラスタネットワークの両方にルールを追加する必要があります。以下の例を使用してルールを追加する場合は、{iface}をネットワークインタフェース\(eth0、eth1など\)、{ip\-address}をIPアドレス、{netmask}をパブリックネットワークまたはクラスタネットワークのネットマスクに置き換えることを確認してください。例えば、以下のようになります。

```
sudo iptables -A INPUT -i {iface}  -m multiport -p tcp -s {ip-address}/{netmask} --dports 6800:7300 -j ACCEPT
```

ヒント:Ceph Metadata ServerをCeph OSDデーモンと同じCephノードで実行する場合は、パブリックネットワークの設定ステップを統合できます。

## Ceph Networks[¶](https://docs.ceph.com/en/pacific/rados/configuration/network-config-ref/ "Permalink to this headline")

Cephのネットワークを設定するには、設定ファイルの\[global\]セクションにネットワーク設定を追加する必要があります。5分間のクイックスタートでは、クライアントとサーバが同じネットワークとサブネット上にある1つのパブリックネットワークを想定した、些細なCeph設定ファイルを提供しています。Cephはパブリックネットワークのみでも十分に機能します。ただし、Cephでは、パブリックネットワークに複数のIPネットワークやサブネットマスクを設定するなど、より具体的な条件を設定することができます。また、OSDハートビート、オブジェクトレプリケーション、リカバリのトラフィックを処理するために、別のクラスタネットワークを確立することもできます。設定したIPアドレスと、ネットワークのお客様がサービスにアクセスする際に使用する公開用のIPアドレスを混同しないようにしてください。一般的な内部IPネットワークは、192.168.0.0または10.0.0.0です。

ヒント：パブリックネットワークまたはクラスターネットワークに複数のIPアドレスとサブネットマスクを指定した場合、ネットワーク内のサブネットが相互にルーティング可能である必要があります。また、各IPアドレス/サブネットをIPテーブルに含め、必要に応じてポートを開くようにしてください。

注:CephはサブネットにCIDR表記を使用します\(例: 10.0.0.0/24\)。

ネットワークの設定が完了したら、クラスタを再起動するか、各デーモンを再起動します。Cephのデーモンは動的にバインドされるため、ネットワーク構成を変更してもクラスタ全体を一度に再起動する必要はありません。

### Public Network[¶](https://docs.ceph.com/en/pacific/rados/configuration/network-config-ref/ "Permalink to this headline")

パブリックネットワークを設定するには、Cephの設定ファイルの\[global\]セクションに次のオプションを追加します。

```
[global]
        # ... elided configuration
        public_network = {public-network/netmask}
```

### Cluster Network[¶](https://docs.ceph.com/en/pacific/rados/configuration/network-config-ref/ "Permalink to this headline")

クラスタネットワークを宣言した場合、OSDはハートビート、オブジェクトレプリケーション、リカバリの各トラフィックをクラスタネットワーク上でルーティングします。これにより、単一のネットワークを使用する場合に比べてパフォーマンスが向上する場合があります。クラスタネットワークを設定するには、Ceph設定ファイルの\[global\]セクションに次のオプションを追加します。

```
[global]
        # ... elided configuration
        cluster_network = {cluster-network/netmask}
```

セキュリティを高めるために、クラスター・ネットワークはパブリック・ネットワークやインターネットから到達できないようにしてください。

### IPv4/IPv6 Dual Stack Mode[¶](https://docs.ceph.com/en/pacific/rados/configuration/network-config-ref/ "Permalink to this headline")

IPv4/IPv6デュアルスタックモードで動作させ、パブリックネットワークやクラスターネットワークを定義したい場合は、それぞれにIPv4とIPv6の両方のネットワークを指定する必要があります。

```
[global]
        # ... elided configuration
        public_network = {IPv4 public-network/netmask}, {IPv6 public-network/netmask}
```

これは、Cephが両方のアドレスファミリーに対して有効なIPアドレスを見つけることができるようにするためです。

IPv4またはIPv6のみのスタック環境にしたい場合は、msのバインドオプションを正しく設定してください。

注:IPv4へのバインドはデフォルトで有効になっているため、IPv6にバインドするオプションを追加するだけで、実際にはデュアルスタックモードになります。IPv6だけを使用したい場合は、IPv4を無効にしてIPv6を有効にしてください。下記のBindを参照してください。

## Ceph Daemons[¶](https://docs.ceph.com/en/pacific/rados/configuration/network-config-ref/ "Permalink to this headline")

モニターデーモンは、それぞれ特定のIPアドレスにバインドするように設定されています。 これらのアドレスは通常、お使いの展開ツールで設定されます。 Cephクラスタの他のコンポーネントは、通常はceph.confファイルの\[global\]セクションで指定されるmonhost構成オプションを使用してモニタを検出します。

```
[global]
    mon_host = 10.0.0.2, 10.0.0.3, 10.0.0.4
```

mon\_hostには、IPアドレスのリスト、またはDNSで検索される名前を指定します。 複数のAまたはAAAAレコードを持つDNS名の場合は、すべてのレコードを検索してモニターを発見します。 1つのモニターに到達すると、他のすべての現在のモニターが検出されるため、mon host設定オプションは、クライアントが現在オンラインの1つのモニターに到達できるように、十分に最新の状態にしておく必要があります。

MGRデーモン、OSDデーモン、MDSデーモンは、利用可能な任意のアドレスにバインドするため、特別な設定は必要ありません。 ただし、public addr（OSDデーモンの場合はcluster addr）設定オプションで、特定のIPアドレスを指定してバインドすることができます。 例えば、以下のようになります。

```
[osd.0]
        public addr = {host-public-ip-address}
        cluster addr = {host-cluster-ip-address}
```

One NIC OSD in a Two Network Cluster

一般に、2つのネットワークを持つクラスタに単一のネットワークインタフェースを持つOSDホストを展開することはお勧めしません。ただし、Ceph設定ファイルの\[osd.n\]セクションにpublic\_addrエントリを追加して、OSDホストをパブリックネットワークで動作させることで、これを実現することができます。さらに、パブリックネットワークとクラスタネットワークがお互いにトラフィックをルーティングできる必要がありますが、セキュリティ上の理由からお勧めできません。

## Network Config Settings[¶](https://docs.ceph.com/en/pacific/rados/configuration/network-config-ref/ "Permalink to this headline")

ネットワーク構成の設定は必要ありません。Cephは、クラスタネットワークを特に設定しない限り、すべてのホストが動作するパブリックネットワークを想定しています。

### Public Network[¶](https://docs.ceph.com/en/pacific/rados/configuration/network-config-ref/ "Permalink to this headline")

パブリックネットワークの設定では、パブリックネットワークのIPアドレスやサブネットを具体的に定義することができます。静的IPアドレスを特別に割り当てたり、特定のデーモンのpublic\_addr設定を使ってpublic\_network設定を上書きすることができます。

**public\_network**

Description

パブリック（フロントサイド）ネットワークのIPアドレスとネットマスク（例：192.168.0.0/24）です。グローバル\]で設定します。コンマで区切ったサブネットを指定することもできます。

Type

_{ip\-address}/{netmask}\[,{ip\-address}/{netmask}\]_

Required

_No_

Default

_N/A_

**public\_addr**

Description

パブリック（フロントサイド）ネットワークのIPアドレスです。各デーモンごとに設定します。

Type

_IP Address_

Required

_No_

Default

_N/A_

### Cluster Network[¶](https://docs.ceph.com/en/pacific/rados/configuration/network-config-ref/ "Permalink to this headline")

クラスタネットワーク構成では、クラスタネットワークを宣言し、クラスタネットワークのIPアドレスとサブネットを具体的に定義することができます。特定のOSDデーモンに対しては、静的IPアドレスを特別に割り当てたり、cluster\_addr設定を使用してcluster\_network設定を上書きすることができます。

**cluster\_network**

Description

クラスタ（バックサイド）ネットワークのIPアドレスとネットマスク（例：10.0.0.0/24）。 グローバル\]で設定します。コンマで区切ったサブネットを指定することもできます。

Type

_{ip\-address}/{netmask}\[,{ip\-address}/{netmask}\]_

Required

_No_

Default

_N/A_

**cluster\_addr**

Description

クラスタ（バックサイド）ネットワークのIPアドレスです。デーモンごとに設定します。

Type

_Address_

Required

_No_

Default

_N/A_

### Bind[¶](https://docs.ceph.com/en/pacific/rados/configuration/network-config-ref/ "Permalink to this headline")

バインド設定では、Ceph OSDおよびMDSデーモンが使用するデフォルトのポート範囲を設定します。デフォルトの範囲は6800:7300です。IPテーブルの設定で、設定したポート範囲を使用できることを確認してください。

CephデーモンがIPv4アドレスではなくIPv6アドレスにバインドできるようにすることもできます。

**ms\_bind\_port\_min**

Description

OSDまたはMDSデーモンがバインドする最小のポート番号。

Type

_32\-bit Integer_

Default

_6800_

Required

_No_

**ms\_bind\_port\_max**

Description

OSDまたはMDSデーモンがバインドする最大ポート番号。

Type

_32\-bit Integer_

Default

_7300_

Required

_No._

**ms\_bind\_ipv4**

Description

CephのデーモンがIPv4アドレスにバインドすることを有効にします。

Type

_Boolean_

Default

_true_

Required

_No_

**ms\_bind\_ipv6**

Description

CephデーモンのIPv6アドレスへのバインドを有効にします。

Type

_Boolean_

Default

_false_

Required

_No_

**public\_bind\_addr**

Description

一部の動的な導入環境では、Ceph MONデーモンが、ネットワーク内の他のピアにアドバタイズされたpublic\_addrとは異なるIPアドレスにローカルでバインドすることがあります。環境では、ルーティングルールが正しく設定されていることを確認する必要があります。public\_bind\_addrが設定されている場合、Ceph Monitorデーモンはローカルでそのアドレスにバインドし、monmapsでpublic\_addrを使用してピアにアドレスをアドバタイズします。この動作はMonitorデーモンに限定されます。

Type

_IP Address_

Required

_No_

Default

_N/A_

### TCP[¶](https://docs.ceph.com/en/pacific/rados/configuration/network-config-ref/ "Permalink to this headline")

CephはデフォルトでTCPバッファリングを無効にします。

**ms\_tcp\_nodelay**

Description

Cephはms\_tcp\_nodelayを有効にして、各要求がすぐに送信されるようにしています\(バッファリングなし\)。Nagleのアルゴリズムを無効にすると、ネットワークトラフィックが増加し、遅延が発生する可能性があります。小さなパケットが大量に発生する場合は、ms\_tcp\_nodelayを無効にしてみてください。

Type

_Boolean_

Required

_No_

Default

_true_

**ms\_tcp\_rcvbuf**

Description

ネットワーク接続の受信側のソケットバッファのサイズを指定します。デフォルトでは無効です。

Type

_32\-bit Integer_

Required

_No_

Default

_0_

**ms\_tcp\_read\_timeout**

Description

クライアントまたはデーモンが別のCephデーモンに要求を行い、未使用の接続をドロップしない場合、_ms tcp read timeout_は指定された秒数の後に接続をアイドルとして定義します。

Type

_Unsigned 64\-bit Integer_

Required

_No_

Default

_900 15 minutes._
