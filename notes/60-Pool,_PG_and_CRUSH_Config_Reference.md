# 60: Pool, PG and CRUSH Config Reference

**クリップソース:** [60: Pool, PG and CRUSH Config Reference — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/configuration/pool-pg-config-ref/)

# Pool, PG and CRUSH Config Reference[¶](https://docs.ceph.com/en/pacific/rados/configuration/pool-pg-config-ref/ "Permalink to this headline")

プールを作成し、それぞれにPG数を設定する際、特にデフォルトをオーバーライドしない場合、Cephはデフォルト値を使用します。一部のデフォルトをオーバーライドすることをお勧めします。具体的には、プールのレプリカサイズを設定し、PGのデフォルト数をオーバーライドすることをお勧めします。これらの値は、プール コマンドを実行するときに具体的に設定することができます。また、Ceph設定ファイルの\[global\]セクションに新しい値を追加することで、デフォルトをオーバーライドすることができます。

```
[global]

デフォルトでは、CephはRADOSオブジェクトのレプリカを3つ作成する。
オブジェクトの4つのコピー、つまりプライマリコピーと3つのレプリカコピーを維持したい場合は、「osd_pool_default_size」に示すようにデフォルト値を再設定する。 
デグレード状態でCephがより少ない数のコピーを書き込むようにするには、「osd_pool_default_min_size」を「osd_pool_default_size」値より小さい数値に設定する

	osd_pool_default_size = 3  # Write an object 3 times.
	osd_pool_default_min_size = 2 # Allow writing two copies in a degraded state.

現実的なPG数を確保する。
OSDあたり約100個を推奨する。
例：OSDの総数×100÷レプリカ数（＝osdプールのデフォルトサイズ）。
つまり、10個のOSDとosdプールのデフォルト・サイズ＝4の場合、およそ次のように推奨される。
(100 * 10) / 4 = 250.
常に最も近い2のべき乗を使用する

	osd_pool_default_pg_num = 256
	osd_pool_default_pgp_num = 256

```

**mon\_max\_pool\_pg\_num**

Description

1プールあたりのPG最大数

Type

Integer

Default

65536

**mon\_pg\_create\_interval**

Description

同じCeph OSD DaemonでPGを作成する間の秒数

Type

Float

Default

30.0

**mon\_pg\_stuck\_threshold**

Description

PGがスタックしていると判断できる秒数

Type

32\-bit Integer

Default

300

**mon\_pg\_min\_inactive**

Description

mon\_pg\_stuck\_threshold よりも長く非アクティブだった PG のカウントがこの設定を超えた場合、HEALTH\_ERR を発生させる。正でない数値は無効を意味し、決してERRにならない。

Type

Integer

Default

1

**mon\_pg\_warn\_min\_per\_osd**

Description

OSD の平均 PG 数がこの数値を下回ると HEALTH\_WARN を発生させる。正でない場合は無効となる

Type

Integer

Default

30

**mon\_pg\_warn\_min\_objects**

Description

クラスタ内のRADOSオブジェクトの総数がこの数以下の場合に警告を表示しない

Type

Integer

Default

1000

**mon\_pg\_warn\_min\_pool\_objects**

Description

プール内のRADOSオブジェクト数がこの数以下の場合に警告を表示しない

Type

Integer

Default

1000

**mon\_pg\_check\_down\_all\_threshold**

Description

down OSDのパーセンテージ閾値で、それを超えるとすべてのPGが古くなっていないかチェックする

Type

Float

Default

0.5

**mon\_pg\_warn\_max\_object\_skew**

Description

いずれかのプールの PG あたりの平均 RADOS オブジェクト数が 全プールの PG あたりの平均 RADOS オブジェクト数のmon\_pg\_warn\_max\_object\_skew 倍より大きい場合に HEALTH\_WARN を発生させる。ゼロまたは正でない数値はこれを無効にする。このオプションは、ceph\-mgrデーモンに適用されることに注意

Type

Float

Default

10

**mon\_delta\_reset\_interval**

Description

PGデルタを0にリセットするまでの非アクティブな秒数。各プールの使用領域のデルタを記録しておくことで、例えばリカバリの進捗やキャッシュ層のパフォーマンスを把握しやすくなる。しかし、あるプールでアクティビティが報告されていない場合は、そのプールのデルタの履歴をリセットするだけ

Type

Integer

Default

10

**mon\_osd\_max\_op\_age**

Description

問題とするオペレーションの年齢（2の累乗）。もしリクエストがこの制限時間より長くブロックされた場合、 HEALTH\_WARN が発生する

Type

Float

Default

32.0

**osd\_pg\_bits**

Description

Ceph OSDデーモンごとのPGビット

Type

32\-bit Integer

Default

6

**osd\_pgp\_bits**

Description

Ceph OSD DaemonあたりのPGPビット数

Type

32\-bit Integer

Default

6

**osd\_crush\_chooseleaf\_type**

Description

CRUSHルールでchooseleafに使用するバケット・タイプ。名前ではなく、ランクを使用する

Type

32\-bit Integer

Default

1. 通常、1つまたは複数のCeph OSD Daemonを含むホスト

**osd\_crush\_initial\_weight**

Description

新規に追加されるOSDの初期CRUSHウエイト

Type

Double

Default

新しく追加されたOSDのサイズをTBで指定する。デフォルトでは、新しく追加されたOSDの初期CRUSHウェイトは、TB単位でそのデバイスサイズに設定される。詳しくは、[Weighting Bucket Items](https://docs.ceph.com/en/pacific/rados/configuration/pool-pg-config-ref/)を参照

**osd\_pool\_default\_crush\_rule**

Description

レプリケートプール作成時に使用するデフォルトのCRUSHルール

Type

8\-bit Integer

Default

\-1、つまり「一番小さい数字のIDを持つルールを選んでそれを使え」という意味。 これは、ルール0がない場合にプール作成を機能させるためのもの

**osd\_pool\_erasure\_code\_stripe\_unit**

Description

EC付きプールのオブジェクトストライプのチャンクのデフォルトサイズをバイト単位で設定する。サイズ S のすべてのオブジェクトは N 個のストライプとして保存され、各データチャンクは stripeunit バイトを受け取る。N\*stripeunitバイトの各ストライプは個別に符号化/復号化される。このオプションは、ECプロファイルのstripe\_unit設定によって上書きされることがある

Type

Unsigned 32\-bit Integer

Default

4096

**osd\_pool\_default\_size**

Description

プール内のオブジェクトのレプリカの数を設定する。デフォルト値はceph osd pool set {pool\-name} size {size}と同じ

Type

32\-bit Integer

Default

3

**osd\_pool\_default\_min\_size**

Description

クライアントへの書き込み操作を承認するために、プール内のオブジェクトの書き込み済みレプリカの最小数を設定する。 最小値を満たさない場合、Cephはクライアントへの書き込みを承認しないため、データ損失が発生する可能性がある。この設定により、デグレードモードでの運用時に最小数のレプリカが確保される

Type

32\-bit Integer

Default

0, これは特定の最小値がないことを意味する。0の場合、最小値はsize\-\(size/2\)となる

**osd\_pool\_default\_pg\_num**

Description

プールのデフォルトPG数。デフォルト値は、mkpoolでのpg\_numと同じ

Type

32\-bit Integer

Default

32

**osd\_pool\_default\_pgp\_num**

Description

プールに対するデフォルトの配置のためのPG数。デフォルト値は、mkpoolのpgp\_numと同じ。PGとPGPは\(今のところ\)等しいはず

Type

32\-bit Integer

Default

8

**osd\_pool\_default\_flags**

Description

新しいプールのデフォルト・フラグ

Type

32\-bit Integer

Default

0

**osd\_max\_pgls**

Description

リストアップするPGの最大数。大きな数を要求するクライアントは、Ceph OSD Daemonを制限する可能性がある

Type

Unsigned 64\-bit Integer

Default

1024

Note

デフォルトで問題ないはず

**osd\_min\_pg\_log\_entries**

Description

ログファイルをトリミングする際に維持するPGログの最小数

Type

32\-bit Int Unsigned

Default

250

**osd\_max\_pg\_log\_entries**

Description

ログファイルをトリミングする際に維持するPGログの最大数

Type

32\-bit Int Unsigned

Default

10000

**osd\_default\_data\_pool\_replay\_window**

Description

OSDがクライアントのリクエストのリプレイを待機する時間（秒）

Type

32\-bit Integer

Default

45

**osd\_max\_pg\_per\_osd\_hard\_ratio**

Description

OSDが新しいPG作成を拒否するまでに、クラスタが許容するOSDあたりのPG数の比率。OSD は、提供する PG の数が _osd\_max\_pg\_per\_osd\_hard\_ratio \* mon\_max\_pg\_per\_osd_ を超えると、新しい PG の作成を停止する

Type

Float

Default

2

**osd\_recovery\_priority**

Description

ワークキューにおけるリカバリの優先度

Type

Integer

Default

5

**osd\_recovery\_op\_priority**

Description

poolがオーバーライドしない場合、リカバリ操作に使用されるデフォルトの優先度

Type

Integer

Default

3
