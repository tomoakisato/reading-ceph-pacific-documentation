# 201: iSCSI Gateway Requirements

**クリップソース:** [201: iSCSI Gateway Requirements — Ceph Documentation](https://docs.ceph.com/en/pacific/rbd/iscsi-requirements/)

# iSCSI Gateway Requirements[¶](https://docs.ceph.com/en/pacific/rbd/iscsi-requirements/ "Permalink to this headline")

高可用性のCeph iSCSIゲートウェイソリューションを実現するには、2～4台のiSCSIゲートウェイノードをプロビジョニングすることが推奨されます。

ハードウェアの推奨環境については、「[ハードウェアの推奨環境](https://docs.ceph.com/en/pacific/start/hardware-recommendations/#hardware-recommendations)」をご覧ください。

注：iSCSIゲートウェイノードでは、メモリフットプリントはマッピングされたRBDイメージの関数であり、大きくなる可能性があります。マップされるRBDイメージの数に応じて、メモリ要件を計画してください。

CephモニタまたはOSDに特定のiSCSIゲートウェイオプションはありませんが、イニシエータタイムアウトの可能性を減らすために、ダウンしたOSDを検出するためのデフォルトのハートビート間隔を低くすることが重要です。以下の構成オプションが推奨されます：

```
[osd]
osd heartbeat grace = 20
osd heartbeat interval = 5
```

* Cephモニタノードからの実行状態の更新

```
ceph tell <daemon_type>.<id> config set <parameter_name> <new_value>
```

```
ceph tell osd.* config set osd_heartbeat_grace 20
ceph tell osd.* config set osd_heartbeat_interval 5
```

* 各OSDノードでの実行状態の更新

```
ceph daemon <daemon_type>.<id> config set osd_client_watch_timeout 15
```

```
ceph daemon osd.0 config set osd_heartbeat_grace 20
ceph daemon osd.0 config set osd_heartbeat_interval 5
```

Cephの構成オプションの設定の詳細については、「[Cephの構成](https://docs.ceph.com/en/pacific/rados/configuration/ceph-conf/#configuring-ceph)」を参照してください。 これらの設定は、必ず/etc/ceph.conf、またはMimic以降のリリースでは集中設定ストアに保存してください。
