# 405: Partial Object Recovery — Ceph Documentation

 # Partial Object Recovery[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/partial_object_recovery/ "Permalink to this headline")

Partial Object Recovery improves the efficiency of log\-based recovery \(vs backfill\). Original log\-based recovery calculates missing\_set based on pg\_log differences.

The whole object should be recovery from one OSD to another if the object is indicated modified by pg\_log regardless of how much content in the object is really modified. That means a 4M object, which is just modified 4k inside, should recovery the whole 4M object rather than the modified 4k content. In addition, object map should be also recovered even if it is not modified at all.

Partial Object Recovery is designed to solve the problem mentioned above. In order to achieve the goals, two things should be done:

1. logging where the object is modified is necessary
2. logging whether the object\_map of an object is modified is also necessary

class ObjectCleanRegion is introduced to do what we want. clean\_offsets is a variable of interval\_set\<uint64\_t\> and is used to indicate the unmodified content in an object. clean\_omap is a variable of bool indicating whether object\_map is modified. new\_object means that osd does not exist for an object max\_num\_intervals is an upbound of the number of intervals in clean\_offsets so that the memory cost of clean\_offsets is always bounded.

The shortest clean interval will be trimmed if the number of intervals in clean\_offsets exceeds the boundary.

> etc. max\_num\_intervals=2, clean\_offsets:{\[5~10\], \[20~5\]}
> 
> then new interval \[30~10\] will evict out the shortest one \[20~5\]
> 
> finally, clean\_offsets becomes {\[5~10\], \[30~10\]}

## Procedures for Partial Object Recovery[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/partial_object_recovery/ "Permalink to this headline")

Firstly, OpContext and pg\_log\_entry\_t should contain ObjectCleanRegion. In do\_osd\_ops\(\), finish\_copyfrom\(\), finish\_promote\(\), corresponding content in ObjectCleanRegion should mark dirty so that trace the modification of an object. Also update ObjectCleanRegion in OpContext to its pg\_log\_entry\_t.

Secondly, pg\_missing\_set can build and rebuild correctly. when calculating pg\_missing\_set during peering process, also merge ObjectCleanRegion in each pg\_log\_entry\_t.

> etc. object aa has pg\_log:26’101 {\[0~4096, 8192~MAX\], false}
> 
> 26’104 {0~8192, 12288~MAX, false}
> 
> 28’108 {\[0~12288, 16384~MAX\], true}
> 
> missing\_set for object aa: merge pg\_log above –\> {\[0~4096, 16384~MAX\], true}. which means 4096~16384 is modified and object\_map is also modified on version 28’108

Also, OSD may be crash after merge log. Therefore, we need to read\_log and rebuild pg\_missing\_set. For example, pg\_log is:

> object aa: 26’101 {\[0~4096, 8192~MAX\], false}
> 
> object bb: 26’102 {\[0~4096, 8192~MAX\], false}
> 
> object cc: 26’103 {\[0~4096, 8192~MAX\], false}
> 
> object aa: 26’104 {0~8192, 12288~MAX, false}
> 
> object dd: 26’105 {\[0~4096, 8192~MAX\], false}
> 
> object aa: 28’108 {\[0~12288, 16384~MAX\], true}

Originally, if bb,cc,dd is recovered, and aa is not. So we need to rebuild pg\_missing\_set for object aa, and find aa is modified on version 28’108. If version in object\_info is 26’96 \< 28’108, we don’t need to consider 26’104 and 26’101 because the whole object will be recovered. However, Partial Object Recovery should also require us to rebuild ObjectCleanRegion.

Knowing whether the object is modified is not enough.

Therefore, we also need to traverse the pg\_log before, that says 26’104 and 26’101 also \> object\_info\(26’96\) and rebuild pg\_missing\_set for object aa based on those three logs: 28’108, 26’104, 26’101. The way how to merge logs is the same as mentioned above

Finally, finish the push and pull process based on pg\_missing\_set. Updating copy\_subset in recovery\_info based on ObjectCleanRegion in pg\_missing\_set. copy\_subset indicates the intervals of content need to pull and push.

The complicated part here is submit\_push\_data and serval cases should be considered separately. what we need to consider is how to deal with the object data, object data makes up of omap\_header, xattrs, omap, data:

case 1: first && complete: since object recovering is finished in a single PushOp, we would like to preserve the original object and overwrite on the object directly. Object will not be removed and touch a new one.

> issue 1: As object is not removed, old xattrs remain in the old object but maybe updated in new object. Overwriting for the same key or adding new keys is correct, but removing keys will be wrong. In order to solve this issue, We need to remove the all original xattrs in the object, and then update new xattrs.
> 
> issue 2: As object is not removed, object\_map may be recovered depending on the clean\_omap. Therefore, if recovering clean\_omap, we need to remove old omap of the object for the same reason since omap updating may also be a deletion. Thus, in this case, we should do:
> 
> > 1. clear xattrs of the object
> > 2. clear omap of the object if omap recovery is needed
> > 3. truncate the object into recovery\_info.size
> > 4. recovery omap\_header
> > 5. recovery xattrs, and recover omap if needed
> > 6. punch zeros for original object if fiemap tells nothing there
> > 7. overwrite object content which is modified
> > 8. finish recovery

case 2: first && \!complete: object recovering should be done in multiple times. Here, target\_oid will indicate a new temp\_object in pgid\_TEMP, so the issues are a bit difference.

> issue 1: As object is newly created, there is no need to deal with xattrs
> 
> issue 2: As object is newly created, and object\_map may not be transmitted depending on clean\_omap. Therefore, if clean\_omap is true, we need to clone object\_map from original object. issue 3: As object is newly created, and unmodified data will not be transmitted. Therefore, we need to clone unmodified data from the original object. Thus, in this case, we should do:
> 
> > 1. remove the temp object
> > 2. create a new temp object
> > 3. set alloc\_hint for the new temp object
> > 4. truncate new temp object to recovery\_info.size
> > 5. recovery omap\_header
> > 6. clone object\_map from original object if omap is clean
> > 7. clone unmodified object\_data from original object
> > 8. punch zeros for the new temp object
> > 9. recovery xattrs, and recover omap if needed
> > 10. overwrite object content which is modified
> > 11. remove the original object
> > 12. move and rename the new temp object to replace the original object
> > 13. finish recovery
