# 227: Pool Placement and Storage Classes

**クリップソース:** [227: Pool Placement and Storage Classes — Ceph Documentation](https://docs.ceph.com/en/pacific/radosgw/placement/)

# [Pool Placement and Storage Classes](https://docs.ceph.com/en/pacific/radosgw/placement/)[¶](https://docs.ceph.com/en/pacific/radosgw/placement/ "Permalink to this headline")

Contents

* [Pool Placement and Storage Classes](https://docs.ceph.com/en/pacific/radosgw/placement/)
    * [Placement Targets](https://docs.ceph.com/en/pacific/radosgw/placement/)
    * [Storage Classes](https://docs.ceph.com/en/pacific/radosgw/placement/)
    * [Zonegroup/Zone Configuration](https://docs.ceph.com/en/pacific/radosgw/placement/)
        * [Adding a Placement Target](https://docs.ceph.com/en/pacific/radosgw/placement/)
        * [Adding a Storage Class](https://docs.ceph.com/en/pacific/radosgw/placement/)
    * [Customizing Placement](https://docs.ceph.com/en/pacific/radosgw/placement/)
        * [Default Placement](https://docs.ceph.com/en/pacific/radosgw/placement/)
        * [User Placement](https://docs.ceph.com/en/pacific/radosgw/placement/)
        * [S3 Bucket Placement](https://docs.ceph.com/en/pacific/radosgw/placement/)
        * [Swift Bucket Placement](https://docs.ceph.com/en/pacific/radosgw/placement/)
    * [Using Storage Classes](https://docs.ceph.com/en/pacific/radosgw/placement/)

## [Placement Targets](https://docs.ceph.com/en/pacific/radosgw/placement/)[¶](https://docs.ceph.com/en/pacific/radosgw/placement/ "Permalink to this headline")

Jewelバージョンの新機能です。

プレースメントターゲットは、特定のバケットにどのプールを関連付けるかを制御します。バケットのプレースメントターゲットは、作成時に選択され、変更することはできません。radosgw\-admin bucket stats コマンドは、その placement\_rule を表示します。

ゾーングループ設定は、"default\-placement"という名前の初期ターゲットを持つプレースメントターゲットの リストを含んでいます。次に、ゾーン設定は、各ゾーングループ・プレースメントターゲット名をそのローカル ストレージにマップします。このゾーンプレースメント情報には、バケットインデックス用の index\_pool 名、不完全なマルチパートアップロードに関するメタデータ用の data\_extra\_pool 名、各ストレージクラス用の data\_pool 名を含みます。

## [Storage Classes](https://docs.ceph.com/en/pacific/radosgw/placement/)[¶](https://docs.ceph.com/en/pacific/radosgw/placement/ "Permalink to this headline")

Nautilus バージョンの新機能です。

ストレージクラスは、オブジェクトデータの配置をカスタマイズするために使用されます。S3 バケット Lifecycleルールは、ストレージクラス間のオブジェクトの移行を自動化することができます。

ストレージクラスは、プレースメントターゲットの観点から定義されます。各ゾーングループのプレースメントターゲットは、"STANDARD"という名前の初期クラスで利用可能なストレージクラスをリストアップします。ゾーン設定は、ゾーングループの各ストレージクラスに対してdata\_poolプール名を指定する責任を負っています。

## [Zonegroup/Zone Configuration](https://docs.ceph.com/en/pacific/radosgw/placement/)[¶](https://docs.ceph.com/en/pacific/radosgw/placement/ "Permalink to this headline")

プレースメント設定は、ゾーングループとゾーン上のradosgw\-adminコマンドで行います。

ゾーングループのプレースメント設定を問い合わせるには：

```
$ radosgw-admin zonegroup get
{
    "id": "ab01123f-e0df-4f29-9d71-b44888d67cd5",
    "name": "default",
    "api_name": "default",
    ...
    "placement_targets": [
        {
            "name": "default-placement",
            "tags": [],
            "storage_classes": [
                "STANDARD"
            ]
        }
    ],
    "default_placement": "default-placement",
    ...
}
```

ゾーンのプレースメント設定を問い合わせるには：

```
$ radosgw-admin zone get
{
    "id": "557cdcee-3aae-4e9e-85c7-2f86f5eddb1f",
    "name": "default",
    "domain_root": "default.rgw.meta:root",
    ...
    "placement_pools": [
        {
            "key": "default-placement",
            "val": {
                "index_pool": "default.rgw.buckets.index",
                "storage_classes": {
                    "STANDARD": {
                        "data_pool": "default.rgw.buckets.data"
                    }
                },
                "data_extra_pool": "default.rgw.buckets.non-ec",
                "index_type": 0
            }
        }
    ],
    ...
}
```

注: 以前にマルチサイト構成を行っていない場合、default のゾーンとゾーングループが作成され、Ceph Object Gatewayが再起動するまで、ゾーン/ゾーングループの変更は有効になりません。マルチサイト用のレルムを作成している場合、radosgw\-admin period update \-\-commitで変更がコミットされると、ゾーン/ゾーングループの変更が有効になります。

### [Adding a Placement Target](https://docs.ceph.com/en/pacific/radosgw/placement/)[¶](https://docs.ceph.com/en/pacific/radosgw/placement/ "Permalink to this headline")

temporaryという名前の新しいプレースメントターゲットを作成するには、まずそれをゾーングループに追加することから始めます：

```
$ radosgw-admin zonegroup placement add 
      --rgw-zonegroup default 
      --placement-id temporary
```

そして、そのターゲットのゾーンプレースメント情報を提供します：

```
$ radosgw-admin zone placement add 
      --rgw-zone default 
      --placement-id temporary 
      --data-pool default.rgw.temporary.data 
      --index-pool default.rgw.temporary.index 
      --data-extra-pool default.rgw.temporary.non-ec
```

### [Adding a Storage Class](https://docs.ceph.com/en/pacific/radosgw/placement/)[¶](https://docs.ceph.com/en/pacific/radosgw/placement/ "Permalink to this headline")

GLACIERという新しいストレージクラスをdefault\-placementターゲットに追加するには、まずゾーングループに追加することから始めます：

```
$ radosgw-admin zonegroup placement add 
      --rgw-zonegroup default 
      --placement-id default-placement 
      --storage-class GLACIER
```

次に、そのストレージクラスのゾーンプレースメント情報を提供します：

```
$ radosgw-admin zone placement add 
      --rgw-zone default 
      --placement-id default-placement 
      --storage-class GLACIER 
      --data-pool default.rgw.glacier.data 
      --compression lz4
```

## [Customizing Placement](https://docs.ceph.com/en/pacific/radosgw/placement/)[¶](https://docs.ceph.com/en/pacific/radosgw/placement/ "Permalink to this headline")

### [Default Placement](https://docs.ceph.com/en/pacific/radosgw/placement/)[¶](https://docs.ceph.com/en/pacific/radosgw/placement/ "Permalink to this headline")

デフォルトでは、新しいバケットはゾーングループのdefault\_placementターゲットが使用されます。このゾーングループの設定は、次のようにして変更することができます：

```
$ radosgw-admin zonegroup placement default 
      --rgw-zonegroup default 
      --placement-id new-placement
```

### [User Placement](https://docs.ceph.com/en/pacific/radosgw/placement/)[¶](https://docs.ceph.com/en/pacific/radosgw/placement/ "Permalink to this headline")

Ceph Object Gatewayのユーザは、ユーザ情報に空でないdefault\_placementフィールドを設定することで、ゾーングループのデフォルトのプレースメントターゲットをオーバーライドできます。同様に、default\_storage\_classは、デフォルトでオブジェクトに適用されるSTANDARDストレージクラスをオーバーライドすることができます。

```
$ radosgw-admin user info --uid testid
{
    ...
    "default_placement": "",
    "default_storage_class": "",
    "placement_tags": [],
    ...
}
```

ゾーングループのプレースメントターゲットに何らかのタグが含まれている場合、ユーザー情報のplacement\_tagsフィールドに少なくとも1つの一致するタグが含まれていなければ、ユーザーはそのプレースメントターゲットでバケットを作成することができなくなります。これは、特定の種類のストレージへのアクセスを制限するのに便利です。

radosgw\-admin コマンドは、これらのフィールドを直接修正することができます：

```
$ radosgw-admin user modify 
      --uid <user-id> 
      --placement-id <default-placement-id> 
      --storage-class <default-storage-class> 
      --tags <tag1,tag2>
```

### [S3 Bucket Placement](https://docs.ceph.com/en/pacific/radosgw/placement/)[¶](https://docs.ceph.com/en/pacific/radosgw/placement/ "Permalink to this headline")

S3プロトコルでバケットを作成する際、LocationConstraintの一部としてプレースメントターゲットを提供することで、ユーザーとゾーングループからのデフォルトのプレースメントターゲットをオーバーライドすることが可能です。

通常、LocationConstraintはゾーングループのapi\_nameと一致する必要があります：

```
<LocationConstraint>default</LocationConstraint>
```

api\_nameにコロンに続けて、カスタムのプレースメントターゲットを追加することができます：

```
<LocationConstraint>default:new-placement</LocationConstraint>
```

### [Swift Bucket Placement](https://docs.ceph.com/en/pacific/radosgw/placement/)[¶](https://docs.ceph.com/en/pacific/radosgw/placement/ "Permalink to this headline")

Swiftプロトコルでバケットを作成する際、HTTPヘッダーのX\-Storage\-Policyでプレースメントターゲットを指定することができます：

```
X-Storage-Policy: new-placement
```

## [Using Storage Classes](https://docs.ceph.com/en/pacific/radosgw/placement/)[¶](https://docs.ceph.com/en/pacific/radosgw/placement/ "Permalink to this headline")

すべてのプレースメントターゲットは、デフォルトで新しいオブジェクトに適用される"STANDARD"ストレージクラスを持っています。ユーザーはこのデフォルトをdefault\_storage\_classでオーバーライドすることができます。

デフォルトでないストレージクラスにオブジェクトを作成するには、リクエストと一緒にHTTPヘッダーにそのストレージクラス名を指定します。S3プロトコルはX\-Amz\-Storage\-Classヘッダを使用し、SwiftプロトコルはX\-Object\-Storage\-Classヘッダを使用します。

python boto3のようなAWS S3 SDKを使用する場合、デフォルトでないストレージクラスがAWS S3で許可されたストレージクラスの一つとして呼び出されることが重要です、さもなければSDKはリクエストをドロップし例外が発生します。

S3 Object Lifecycle Managementは、Transitionアクションを使用して、ストレージクラス間でオブジェクトデータを移動するために使用することができます。
