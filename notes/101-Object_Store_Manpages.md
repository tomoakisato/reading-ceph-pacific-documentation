# 101: Object Store Manpages

**クリップソース:** [101: Object Store Manpages — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/man/)

# Object Store Manpages[¶](https://docs.ceph.com/en/pacific/rados/man/ "Permalink to this headline")

* [ceph\-volume – Ceph OSD deployment and inspection tool](https://docs.ceph.com/en/pacific/rados/man/)
* [ceph\-volume\-systemd – systemd ceph\-volume helper tool](https://docs.ceph.com/en/pacific/rados/man/)
* [ceph – ceph administration tool](https://docs.ceph.com/en/pacific/rados/man/)
* [ceph\-authtool – ceph keyring manipulation tool](https://docs.ceph.com/en/pacific/rados/man/)
* [ceph\-clsinfo – show class object information](https://docs.ceph.com/en/pacific/rados/man/)
* [ceph\-conf – ceph conf file tool](https://docs.ceph.com/en/pacific/rados/man/)
* [ceph\-debugpack – ceph debug packer utility](https://docs.ceph.com/en/pacific/rados/man/)
* [ceph\-dencoder – ceph encoder/decoder utility](https://docs.ceph.com/en/pacific/rados/man/)
* [ceph\-mon – ceph monitor daemon](https://docs.ceph.com/en/pacific/rados/man/)
* [ceph\-osd – ceph object storage daemon](https://docs.ceph.com/en/pacific/rados/man/)
* [ceph\-kvstore\-tool – ceph kvstore manipulation tool](https://docs.ceph.com/en/pacific/rados/man/)
* [ceph\-run – restart daemon on core dump](https://docs.ceph.com/en/pacific/rados/man/)
* [ceph\-syn – ceph synthetic workload generator](https://docs.ceph.com/en/pacific/rados/man/)
* [crushtool – CRUSH map manipulation tool](https://docs.ceph.com/en/pacific/rados/man/)
* [librados\-config – display information about librados](https://docs.ceph.com/en/pacific/rados/man/)
* [monmaptool – ceph monitor cluster map manipulation tool](https://docs.ceph.com/en/pacific/rados/man/)
* [osdmaptool – ceph osd cluster map manipulation tool](https://docs.ceph.com/en/pacific/rados/man/)
* [rados – rados object storage utility](https://docs.ceph.com/en/pacific/rados/man/)
