# 207: Configuring the iSCSI Initiators

**クリップソース:** [207: Configuring the iSCSI Initiators — Ceph Documentation](https://docs.ceph.com/en/pacific/rbd/iscsi-initiators/)

# Configuring the iSCSI Initiators[¶](https://docs.ceph.com/en/pacific/rbd/iscsi-initiators/ "Permalink to this headline")

* [iSCSI Initiator for Linux](https://docs.ceph.com/en/pacific/rbd/iscsi-initiators/)
* [iSCSI Initiator for Microsoft Windows](https://docs.ceph.com/en/pacific/rbd/iscsi-initiators/)
* 警告： 複数のiSCSIゲートウェイを介して1つのRBDイメージをエクスポートする場合、SCSI persistent group reservations（PGR）とSCSI 2ベースのreservationsを使用するアプリケーションはサポートされていません。
    [iSCSI Initiator for VMware ESX](https://docs.ceph.com/en/pacific/rbd/iscsi-initiators/)
