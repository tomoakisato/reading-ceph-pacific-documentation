# 221: rbd-replay – replay rados block device (RBD) workloads

 # rbd\-replay – replay rados block device \(RBD\) workloads[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay/ "Permalink to this headline")

## Synopsis[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay/ "Permalink to this headline")

**rbd\-replay** \[ _options_ \] _replay\_file_

## Description[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay/ "Permalink to this headline")

**rbd\-replay** is a utility for replaying rados block device \(RBD\) workloads.

## Options[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay/ "Permalink to this headline")

`-c`` ceph.conf``, ``--conf`` ceph.conf`[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay/ "Permalink to this definition")Use ceph.conf configuration file instead of the default /etc/ceph/ceph.conf to determine monitor addresses during startup.

`-p`` pool``, ``--pool`` pool`[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay/ "Permalink to this definition")Interact with the given pool. Defaults to ‘rbd’.

`--latency-multiplier```[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay/ "Permalink to this definition")Multiplies inter\-request latencies. Default: 1.

`--read-only```[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay/ "Permalink to this definition")Only replay non\-destructive requests.

`--map-image`` rule`[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay/ "Permalink to this definition")Add a rule to map image names in the trace to image names in the replay cluster. A rule of image1@snap1=image2@snap2 would map snap1 of image1 to snap2 of image2.

`--dump-perf-counters```[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay/ "Permalink to this definition")**Experimental**Dump performance counters to standard out before an image is closed. Performance counters may be dumped multiple times if multiple images are closed, or if the same image is opened and closed multiple times. Performance counters and their meaning may change between versions.

## Examples[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay/ "Permalink to this headline")

To replay workload1 as fast as possible:

```
rbd-replay --latency-multiplier=0 workload1
```

To replay workload1 but use test\_image instead of prod\_image:

```
rbd-replay --map-image=prod_image=test_image workload1
```

## Availability[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay/ "Permalink to this headline")

**rbd\-replay** is part of Ceph, a massively scalable, open\-source, distributed storage system. Please refer to the Ceph documentation at [http://ceph.com/docs](https://docs.ceph.com/en/pacific/man/8/rbd-replay/) for more information.

## See also[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay/ "Permalink to this headline")

[rbd\-replay\-prep](https://docs.ceph.com/en/pacific/man/8/rbd-replay/)\(8\),[rbd](https://docs.ceph.com/en/pacific/man/8/rbd-replay/)\(8\)
