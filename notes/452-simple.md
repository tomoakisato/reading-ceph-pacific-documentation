# 452: simple

**クリップソース:** [452: simple — Ceph Documentation](https://docs.ceph.com/en/pacific/ceph-volume/simple/)

# simple[¶](https://docs.ceph.com/en/pacific/ceph-volume/simple/ "Permalink to this headline")

Implements the functionality needed to manage OSDs from the simple subcommand: ceph\-volumesimple

**Command Line Subcommands**

* [scan](https://docs.ceph.com/en/pacific/ceph-volume/simple/scan/#ceph-volume-simple-scan)
* [activate](https://docs.ceph.com/en/pacific/ceph-volume/simple/activate/#ceph-volume-simple-activate)
* [systemd](https://docs.ceph.com/en/pacific/ceph-volume/simple/systemd/#ceph-volume-simple-systemd)

管理を引き継ぐことで、起動時にデバイスをトリガーするすべてのceph\-disk systemdユニットを無効にし、基本的なJSON設定（カスタマイズ可能）とOSDを起動するsystemdに依存するようにします。

このプロセスには2つのステップがあります：

1. 実行中のOSDやデータデバイスを
2. スキャンしたOSDを

スキャンは、ceph\-volumeがOSDを起動するために必要なすべてを推論するので、起動が必要になったときに、ceph\-diskから妨害を受けることなくOSDを正常に起動することができます。

起動プロセスの一環として、udevイベントに反応するceph\-disk用のsystemdユニットは、完全に非アクティブになるように/dev/nullにリンクされます。
