# 315: alerts send

   [Report a Documentation Bug](https://docs.ceph.com/en/pacific/api/mon_command_api/) 
 # alerts send[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

\(re\)send alerts immediately

Example command:

```
ceph alerts send
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# balancer dump[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Show an optimization plan

Example command:

```
ceph balancer dump my_plan
```

Parameters:

* **plan**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# balancer eval[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Evaluate data distribution for the current cluster or specific pool or specific plan

Example command:

```
ceph balancer eval my_option
```

Parameters:

* **option**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# balancer eval\-verbose[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Evaluate data distribution for the current cluster or specific pool or specific plan \(verbosely\)

Example command:

```
ceph balancer eval-verbose my_option
```

Parameters:

* **option**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# balancer execute[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Execute an optimization plan

Example command:

```
ceph balancer execute my_plan
```

Parameters:

* **plan**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# balancer ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

List all plans

Example command:

```
ceph balancer ls
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# balancer mode[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set balancer mode

Example command:

```
ceph balancer mode none
```

Parameters:

* **mode**: CephChoices strings=\(none crush\-compat upmap\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# balancer off[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Disable automatic balancing

Example command:

```
ceph balancer off
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# balancer on[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Enable automatic balancing

Example command:

```
ceph balancer on
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# balancer optimize[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Run optimizer to create a new plan

Example command:

```
ceph balancer optimize --plan=string --pools=string
```

Parameters:

* **plan**: \(string\)
* **pools**: \(can be repeated\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# balancer pool add[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Enable automatic balancing for specific pools

Example command:

```
ceph balancer pool add --pools=string
```

Parameters:

* **pools**: \(can be repeated\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# balancer pool ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

List automatic balancing pools. Note that empty list means all existing pools will be automatic balancing targets, which is the default behaviour of balancer.

Example command:

```
ceph balancer pool ls
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# balancer pool rm[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Disable automatic balancing for specific pools

Example command:

```
ceph balancer pool rm --pools=string
```

Parameters:

* **pools**: \(can be repeated\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# balancer reset[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Discard all optimization plans

Example command:

```
ceph balancer reset
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# balancer rm[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Discard an optimization plan

Example command:

```
ceph balancer rm my_plan
```

Parameters:

* **plan**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# balancer show[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Show details of an optimization plan

Example command:

```
ceph balancer show my_plan
```

Parameters:

* **plan**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# balancer status[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Show balancer status

Example command:

```
ceph balancer status
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# cephadm check\-host[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Check whether we can access and manage a remote host

Example command:

```
ceph cephadm check-host my_host my_addr
```

Parameters:

* **host**: \(string\)
* **addr**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# cephadm clear\-exporter\-config[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Clear the SSL configuration used by cephadm exporter daemons

Example command:

```
ceph cephadm clear-exporter-config
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# cephadm clear\-key[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Clear cluster SSH key

Example command:

```
ceph cephadm clear-key
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# cephadm clear\-ssh\-config[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Clear the ssh\_config file

Example command:

```
ceph cephadm clear-ssh-config
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# cephadm config\-check disable[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Disable a specific configuration check

Example command:

```
ceph cephadm config-check disable my_check_name
```

Parameters:

* **check\_name**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# cephadm config\-check enable[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Enable a specific configuration check

Example command:

```
ceph cephadm config-check enable my_check_name
```

Parameters:

* **check\_name**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# cephadm config\-check ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

List the available configuration checks and their current state

Example command:

```
ceph cephadm config-check ls plain
```

Parameters:

* **format**: CephChoices strings=\(plain json json\-pretty yaml\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# cephadm config\-check status[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Show whether the configuration checker feature is enabled/disabled

Example command:

```
ceph cephadm config-check status
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# cephadm generate\-exporter\-config[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Generate default SSL crt/key and token for cephadm exporter daemons

Example command:

```
ceph cephadm generate-exporter-config
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# cephadm generate\-key[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Generate a cluster SSH key \(if not present\)

Example command:

```
ceph cephadm generate-key
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# cephadm get\-exporter\-config[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Show the current cephadm\-exporter configuraion \(JSON\)’

Example command:

```
ceph cephadm get-exporter-config
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# cephadm get\-extra\-ceph\-conf[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get extra ceph conf that is appended

Example command:

```
ceph cephadm get-extra-ceph-conf
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# cephadm get\-pub\-key[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Show SSH public key for connecting to cluster hosts

Example command:

```
ceph cephadm get-pub-key
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# cephadm get\-ssh\-config[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Returns the ssh config as used by cephadm

Example command:

```
ceph cephadm get-ssh-config
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# cephadm get\-user[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Show user for SSHing to cluster hosts

Example command:

```
ceph cephadm get-user
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# cephadm osd activate[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Start OSD containers for existing OSDs

Example command:

```
ceph cephadm osd activate --host=string
```

Parameters:

* **host**: \(can be repeated\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# cephadm prepare\-host[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Prepare a remote host for use with cephadm

Example command:

```
ceph cephadm prepare-host my_host my_addr
```

Parameters:

* **host**: \(string\)
* **addr**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# cephadm registry\-login[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set custom registry login info by providing url, username and password or json file with login info \(\-i \<file\>\)

Example command:

```
ceph cephadm registry-login my_url my_username my_password
```

Parameters:

* **url**: \(string\)
* **username**: \(string\)
* **password**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# cephadm set\-exporter\-config[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set custom cephadm\-exporter configuration from a json file \(\-i \<file\>\). JSON must contain crt, key, token and port

Example command:

```
ceph cephadm set-exporter-config
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# cephadm set\-extra\-ceph\-conf[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Text that is appended to all daemon’s ceph.conf. Mainly a workaround, till config generate\-minimal\-conf generates a complete ceph.conf. Warning: this is a dangerous operation.

Example command:

```
ceph cephadm set-extra-ceph-conf
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# cephadm set\-priv\-key[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set cluster SSH private key \(use \-i \<private\_key\>\)

Example command:

```
ceph cephadm set-priv-key
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# cephadm set\-pub\-key[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set cluster SSH public key \(use \-i \<public\_key\>\)

Example command:

```
ceph cephadm set-pub-key
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# cephadm set\-ssh\-config[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the ssh\_config file \(use \-i \<ssh\_config\>\)

Example command:

```
ceph cephadm set-ssh-config
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# cephadm set\-user[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set user for SSHing to cluster hosts, passwordless sudo will be needed for non\-root users

Example command:

```
ceph cephadm set-user my_user
```

Parameters:

* **user**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# count[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Do some counting

Example command:

```
ceph count 1
```

Parameters:

* **num**: CephInt

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# crash archive[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Acknowledge a crash and silence health warning\(s\)

Example command:

```
ceph crash archive my_id
```

Parameters:

* **id**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# crash archive\-all[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Acknowledge all new crashes and silence health warning\(s\)

Example command:

```
ceph crash archive-all
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# crash info[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

show crash dump metadata

Example command:

```
ceph crash info my_id
```

Parameters:

* **id**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# crash json\_report[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Crashes in the last \<hours\> hours

Example command:

```
ceph crash json_report my_hours
```

Parameters:

* **hours**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# crash ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Show new and archived crash dumps

Example command:

```
ceph crash ls
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# crash ls\-new[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Show new crash dumps

Example command:

```
ceph crash ls-new
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# crash post[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Add a crash dump \(use \-i \<jsonfile\>\)

Example command:

```
ceph crash post
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# crash prune[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Remove crashes older than \<keep\> days

Example command:

```
ceph crash prune my_keep
```

Parameters:

* **keep**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# crash rm[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Remove a saved crash \<id\>

Example command:

```
ceph crash rm my_id
```

Parameters:

* **id**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# crash stat[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Summarize recorded crashes

Example command:

```
ceph crash stat
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard create\-self\-signed\-cert[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Create self signed certificate

Example command:

```
ceph dashboard create-self-signed-cert
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard get\-account\-lockout\-attempts[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the ACCOUNT\_LOCKOUT\_ATTEMPTS option value

Example command:

```
ceph dashboard get-account-lockout-attempts
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-alertmanager\-api\-host[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the ALERTMANAGER\_API\_HOST option value

Example command:

```
ceph dashboard get-alertmanager-api-host
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-alertmanager\-api\-ssl\-verify[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the ALERTMANAGER\_API\_SSL\_VERIFY option value

Example command:

```
ceph dashboard get-alertmanager-api-ssl-verify
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-audit\-api\-enabled[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the AUDIT\_API\_ENABLED option value

Example command:

```
ceph dashboard get-audit-api-enabled
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-audit\-api\-log\-payload[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the AUDIT\_API\_LOG\_PAYLOAD option value

Example command:

```
ceph dashboard get-audit-api-log-payload
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-enable\-browsable\-api[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the ENABLE\_BROWSABLE\_API option value

Example command:

```
ceph dashboard get-enable-browsable-api
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-ganesha\-clusters\-rados\-pool\-namespace[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the GANESHA\_CLUSTERS\_RADOS\_POOL\_NAMESPACE option value

Example command:

```
ceph dashboard get-ganesha-clusters-rados-pool-namespace
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-grafana\-api\-password[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the GRAFANA\_API\_PASSWORD option value

Example command:

```
ceph dashboard get-grafana-api-password
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-grafana\-api\-ssl\-verify[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the GRAFANA\_API\_SSL\_VERIFY option value

Example command:

```
ceph dashboard get-grafana-api-ssl-verify
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-grafana\-api\-url[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the GRAFANA\_API\_URL option value

Example command:

```
ceph dashboard get-grafana-api-url
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-grafana\-api\-username[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the GRAFANA\_API\_USERNAME option value

Example command:

```
ceph dashboard get-grafana-api-username
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-grafana\-frontend\-api\-url[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the GRAFANA\_FRONTEND\_API\_URL option value

Example command:

```
ceph dashboard get-grafana-frontend-api-url
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-grafana\-update\-dashboards[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the GRAFANA\_UPDATE\_DASHBOARDS option value

Example command:

```
ceph dashboard get-grafana-update-dashboards
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-iscsi\-api\-ssl\-verification[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the ISCSI\_API\_SSL\_VERIFICATION option value

Example command:

```
ceph dashboard get-iscsi-api-ssl-verification
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-jwt\-token\-ttl[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the JWT token TTL in seconds

Example command:

```
ceph dashboard get-jwt-token-ttl
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-prometheus\-api\-host[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the PROMETHEUS\_API\_HOST option value

Example command:

```
ceph dashboard get-prometheus-api-host
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-prometheus\-api\-ssl\-verify[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the PROMETHEUS\_API\_SSL\_VERIFY option value

Example command:

```
ceph dashboard get-prometheus-api-ssl-verify
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-pwd\-policy\-check\-complexity\-enabled[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the PWD\_POLICY\_CHECK\_COMPLEXITY\_ENABLED option value

Example command:

```
ceph dashboard get-pwd-policy-check-complexity-enabled
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-pwd\-policy\-check\-exclusion\-list\-enabled[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the PWD\_POLICY\_CHECK\_EXCLUSION\_LIST\_ENABLED option value

Example command:

```
ceph dashboard get-pwd-policy-check-exclusion-list-enabled
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-pwd\-policy\-check\-length\-enabled[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the PWD\_POLICY\_CHECK\_LENGTH\_ENABLED option value

Example command:

```
ceph dashboard get-pwd-policy-check-length-enabled
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-pwd\-policy\-check\-oldpwd\-enabled[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the PWD\_POLICY\_CHECK\_OLDPWD\_ENABLED option value

Example command:

```
ceph dashboard get-pwd-policy-check-oldpwd-enabled
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-pwd\-policy\-check\-repetitive\-chars\-enabled[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the PWD\_POLICY\_CHECK\_REPETITIVE\_CHARS\_ENABLED option value

Example command:

```
ceph dashboard get-pwd-policy-check-repetitive-chars-enabled
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-pwd\-policy\-check\-sequential\-chars\-enabled[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the PWD\_POLICY\_CHECK\_SEQUENTIAL\_CHARS\_ENABLED option value

Example command:

```
ceph dashboard get-pwd-policy-check-sequential-chars-enabled
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-pwd\-policy\-check\-username\-enabled[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the PWD\_POLICY\_CHECK\_USERNAME\_ENABLED option value

Example command:

```
ceph dashboard get-pwd-policy-check-username-enabled
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-pwd\-policy\-enabled[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the PWD\_POLICY\_ENABLED option value

Example command:

```
ceph dashboard get-pwd-policy-enabled
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-pwd\-policy\-exclusion\-list[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the PWD\_POLICY\_EXCLUSION\_LIST option value

Example command:

```
ceph dashboard get-pwd-policy-exclusion-list
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-pwd\-policy\-min\-complexity[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the PWD\_POLICY\_MIN\_COMPLEXITY option value

Example command:

```
ceph dashboard get-pwd-policy-min-complexity
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-pwd\-policy\-min\-length[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the PWD\_POLICY\_MIN\_LENGTH option value

Example command:

```
ceph dashboard get-pwd-policy-min-length
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-rest\-requests\-timeout[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the REST\_REQUESTS\_TIMEOUT option value

Example command:

```
ceph dashboard get-rest-requests-timeout
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-rgw\-api\-access\-key[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the RGW\_API\_ACCESS\_KEY option value

Example command:

```
ceph dashboard get-rgw-api-access-key
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-rgw\-api\-admin\-resource[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the RGW\_API\_ADMIN\_RESOURCE option value

Example command:

```
ceph dashboard get-rgw-api-admin-resource
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-rgw\-api\-secret\-key[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the RGW\_API\_SECRET\_KEY option value

Example command:

```
ceph dashboard get-rgw-api-secret-key
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-rgw\-api\-ssl\-verify[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the RGW\_API\_SSL\_VERIFY option value

Example command:

```
ceph dashboard get-rgw-api-ssl-verify
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-user\-pwd\-expiration\-span[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the USER\_PWD\_EXPIRATION\_SPAN option value

Example command:

```
ceph dashboard get-user-pwd-expiration-span
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-user\-pwd\-expiration\-warning\-1[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the USER\_PWD\_EXPIRATION\_WARNING\_1 option value

Example command:

```
ceph dashboard get-user-pwd-expiration-warning-1
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard get\-user\-pwd\-expiration\-warning\-2[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the USER\_PWD\_EXPIRATION\_WARNING\_2 option value

Example command:

```
ceph dashboard get-user-pwd-expiration-warning-2
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard grafana dashboards update[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Push dashboards to Grafana

Example command:

```
ceph dashboard grafana dashboards update
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-account\-lockout\-attempts[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the ACCOUNT\_LOCKOUT\_ATTEMPTS option to its default value

Example command:

```
ceph dashboard reset-account-lockout-attempts
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-alertmanager\-api\-host[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the ALERTMANAGER\_API\_HOST option to its default value

Example command:

```
ceph dashboard reset-alertmanager-api-host
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-alertmanager\-api\-ssl\-verify[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the ALERTMANAGER\_API\_SSL\_VERIFY option to its default value

Example command:

```
ceph dashboard reset-alertmanager-api-ssl-verify
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-audit\-api\-enabled[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the AUDIT\_API\_ENABLED option to its default value

Example command:

```
ceph dashboard reset-audit-api-enabled
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-audit\-api\-log\-payload[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the AUDIT\_API\_LOG\_PAYLOAD option to its default value

Example command:

```
ceph dashboard reset-audit-api-log-payload
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-enable\-browsable\-api[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the ENABLE\_BROWSABLE\_API option to its default value

Example command:

```
ceph dashboard reset-enable-browsable-api
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-ganesha\-clusters\-rados\-pool\-namespace[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the GANESHA\_CLUSTERS\_RADOS\_POOL\_NAMESPACE option to its default value

Example command:

```
ceph dashboard reset-ganesha-clusters-rados-pool-namespace
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-grafana\-api\-password[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the GRAFANA\_API\_PASSWORD option to its default value

Example command:

```
ceph dashboard reset-grafana-api-password
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-grafana\-api\-ssl\-verify[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the GRAFANA\_API\_SSL\_VERIFY option to its default value

Example command:

```
ceph dashboard reset-grafana-api-ssl-verify
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-grafana\-api\-url[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the GRAFANA\_API\_URL option to its default value

Example command:

```
ceph dashboard reset-grafana-api-url
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-grafana\-api\-username[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the GRAFANA\_API\_USERNAME option to its default value

Example command:

```
ceph dashboard reset-grafana-api-username
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-grafana\-frontend\-api\-url[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the GRAFANA\_FRONTEND\_API\_URL option to its default value

Example command:

```
ceph dashboard reset-grafana-frontend-api-url
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-grafana\-update\-dashboards[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the GRAFANA\_UPDATE\_DASHBOARDS option to its default value

Example command:

```
ceph dashboard reset-grafana-update-dashboards
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-iscsi\-api\-ssl\-verification[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the ISCSI\_API\_SSL\_VERIFICATION option to its default value

Example command:

```
ceph dashboard reset-iscsi-api-ssl-verification
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-prometheus\-api\-host[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the PROMETHEUS\_API\_HOST option to its default value

Example command:

```
ceph dashboard reset-prometheus-api-host
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-prometheus\-api\-ssl\-verify[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the PROMETHEUS\_API\_SSL\_VERIFY option to its default value

Example command:

```
ceph dashboard reset-prometheus-api-ssl-verify
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-pwd\-policy\-check\-complexity\-enabled[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the PWD\_POLICY\_CHECK\_COMPLEXITY\_ENABLED option to its default value

Example command:

```
ceph dashboard reset-pwd-policy-check-complexity-enabled
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-pwd\-policy\-check\-exclusion\-list\-enabled[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the PWD\_POLICY\_CHECK\_EXCLUSION\_LIST\_ENABLED option to its default value

Example command:

```
ceph dashboard reset-pwd-policy-check-exclusion-list-enabled
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-pwd\-policy\-check\-length\-enabled[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the PWD\_POLICY\_CHECK\_LENGTH\_ENABLED option to its default value

Example command:

```
ceph dashboard reset-pwd-policy-check-length-enabled
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-pwd\-policy\-check\-oldpwd\-enabled[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the PWD\_POLICY\_CHECK\_OLDPWD\_ENABLED option to its default value

Example command:

```
ceph dashboard reset-pwd-policy-check-oldpwd-enabled
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-pwd\-policy\-check\-repetitive\-chars\-enabled[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the PWD\_POLICY\_CHECK\_REPETITIVE\_CHARS\_ENABLED option to its default value

Example command:

```
ceph dashboard reset-pwd-policy-check-repetitive-chars-enabled
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-pwd\-policy\-check\-sequential\-chars\-enabled[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the PWD\_POLICY\_CHECK\_SEQUENTIAL\_CHARS\_ENABLED option to its default value

Example command:

```
ceph dashboard reset-pwd-policy-check-sequential-chars-enabled
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-pwd\-policy\-check\-username\-enabled[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the PWD\_POLICY\_CHECK\_USERNAME\_ENABLED option to its default value

Example command:

```
ceph dashboard reset-pwd-policy-check-username-enabled
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-pwd\-policy\-enabled[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the PWD\_POLICY\_ENABLED option to its default value

Example command:

```
ceph dashboard reset-pwd-policy-enabled
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-pwd\-policy\-exclusion\-list[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the PWD\_POLICY\_EXCLUSION\_LIST option to its default value

Example command:

```
ceph dashboard reset-pwd-policy-exclusion-list
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-pwd\-policy\-min\-complexity[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the PWD\_POLICY\_MIN\_COMPLEXITY option to its default value

Example command:

```
ceph dashboard reset-pwd-policy-min-complexity
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-pwd\-policy\-min\-length[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the PWD\_POLICY\_MIN\_LENGTH option to its default value

Example command:

```
ceph dashboard reset-pwd-policy-min-length
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-rest\-requests\-timeout[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the REST\_REQUESTS\_TIMEOUT option to its default value

Example command:

```
ceph dashboard reset-rest-requests-timeout
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-rgw\-api\-access\-key[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the RGW\_API\_ACCESS\_KEY option to its default value

Example command:

```
ceph dashboard reset-rgw-api-access-key
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-rgw\-api\-admin\-resource[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the RGW\_API\_ADMIN\_RESOURCE option to its default value

Example command:

```
ceph dashboard reset-rgw-api-admin-resource
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-rgw\-api\-secret\-key[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the RGW\_API\_SECRET\_KEY option to its default value

Example command:

```
ceph dashboard reset-rgw-api-secret-key
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-rgw\-api\-ssl\-verify[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the RGW\_API\_SSL\_VERIFY option to its default value

Example command:

```
ceph dashboard reset-rgw-api-ssl-verify
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-user\-pwd\-expiration\-span[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the USER\_PWD\_EXPIRATION\_SPAN option to its default value

Example command:

```
ceph dashboard reset-user-pwd-expiration-span
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-user\-pwd\-expiration\-warning\-1[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the USER\_PWD\_EXPIRATION\_WARNING\_1 option to its default value

Example command:

```
ceph dashboard reset-user-pwd-expiration-warning-1
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard reset\-user\-pwd\-expiration\-warning\-2[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset the USER\_PWD\_EXPIRATION\_WARNING\_2 option to its default value

Example command:

```
ceph dashboard reset-user-pwd-expiration-warning-2
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-account\-lockout\-attempts[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the ACCOUNT\_LOCKOUT\_ATTEMPTS option value

Example command:

```
ceph dashboard set-account-lockout-attempts my_value
```

Parameters:

* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-alertmanager\-api\-host[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the ALERTMANAGER\_API\_HOST option value

Example command:

```
ceph dashboard set-alertmanager-api-host my_value
```

Parameters:

* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-alertmanager\-api\-ssl\-verify[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the ALERTMANAGER\_API\_SSL\_VERIFY option value

Example command:

```
ceph dashboard set-alertmanager-api-ssl-verify my_value
```

Parameters:

* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-audit\-api\-enabled[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the AUDIT\_API\_ENABLED option value

Example command:

```
ceph dashboard set-audit-api-enabled my_value
```

Parameters:

* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-audit\-api\-log\-payload[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the AUDIT\_API\_LOG\_PAYLOAD option value

Example command:

```
ceph dashboard set-audit-api-log-payload my_value
```

Parameters:

* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-enable\-browsable\-api[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the ENABLE\_BROWSABLE\_API option value

Example command:

```
ceph dashboard set-enable-browsable-api my_value
```

Parameters:

* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-ganesha\-clusters\-rados\-pool\-namespace[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the GANESHA\_CLUSTERS\_RADOS\_POOL\_NAMESPACE option value

Example command:

```
ceph dashboard set-ganesha-clusters-rados-pool-namespace my_value
```

Parameters:

* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-grafana\-api\-password[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the GRAFANA\_API\_PASSWORD option value read from \-i \<file\>

Example command:

```
ceph dashboard set-grafana-api-password
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-grafana\-api\-ssl\-verify[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the GRAFANA\_API\_SSL\_VERIFY option value

Example command:

```
ceph dashboard set-grafana-api-ssl-verify my_value
```

Parameters:

* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-grafana\-api\-url[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the GRAFANA\_API\_URL option value

Example command:

```
ceph dashboard set-grafana-api-url my_value
```

Parameters:

* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-grafana\-api\-username[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the GRAFANA\_API\_USERNAME option value

Example command:

```
ceph dashboard set-grafana-api-username my_value
```

Parameters:

* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-grafana\-frontend\-api\-url[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the GRAFANA\_FRONTEND\_API\_URL option value

Example command:

```
ceph dashboard set-grafana-frontend-api-url my_value
```

Parameters:

* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-grafana\-update\-dashboards[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the GRAFANA\_UPDATE\_DASHBOARDS option value

Example command:

```
ceph dashboard set-grafana-update-dashboards my_value
```

Parameters:

* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-iscsi\-api\-ssl\-verification[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the ISCSI\_API\_SSL\_VERIFICATION option value

Example command:

```
ceph dashboard set-iscsi-api-ssl-verification my_value
```

Parameters:

* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-jwt\-token\-ttl[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the JWT token TTL in seconds

Example command:

```
ceph dashboard set-jwt-token-ttl 1
```

Parameters:

* **seconds**: CephInt

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-prometheus\-api\-host[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the PROMETHEUS\_API\_HOST option value

Example command:

```
ceph dashboard set-prometheus-api-host my_value
```

Parameters:

* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-prometheus\-api\-ssl\-verify[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the PROMETHEUS\_API\_SSL\_VERIFY option value

Example command:

```
ceph dashboard set-prometheus-api-ssl-verify my_value
```

Parameters:

* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-pwd\-policy\-check\-complexity\-enabled[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the PWD\_POLICY\_CHECK\_COMPLEXITY\_ENABLED option value

Example command:

```
ceph dashboard set-pwd-policy-check-complexity-enabled my_value
```

Parameters:

* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-pwd\-policy\-check\-exclusion\-list\-enabled[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the PWD\_POLICY\_CHECK\_EXCLUSION\_LIST\_ENABLED option value

Example command:

```
ceph dashboard set-pwd-policy-check-exclusion-list-enabled my_value
```

Parameters:

* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-pwd\-policy\-check\-length\-enabled[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the PWD\_POLICY\_CHECK\_LENGTH\_ENABLED option value

Example command:

```
ceph dashboard set-pwd-policy-check-length-enabled my_value
```

Parameters:

* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-pwd\-policy\-check\-oldpwd\-enabled[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the PWD\_POLICY\_CHECK\_OLDPWD\_ENABLED option value

Example command:

```
ceph dashboard set-pwd-policy-check-oldpwd-enabled my_value
```

Parameters:

* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-pwd\-policy\-check\-repetitive\-chars\-enabled[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the PWD\_POLICY\_CHECK\_REPETITIVE\_CHARS\_ENABLED option value

Example command:

```
ceph dashboard set-pwd-policy-check-repetitive-chars-enabled my_value
```

Parameters:

* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-pwd\-policy\-check\-sequential\-chars\-enabled[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the PWD\_POLICY\_CHECK\_SEQUENTIAL\_CHARS\_ENABLED option value

Example command:

```
ceph dashboard set-pwd-policy-check-sequential-chars-enabled my_value
```

Parameters:

* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-pwd\-policy\-check\-username\-enabled[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the PWD\_POLICY\_CHECK\_USERNAME\_ENABLED option value

Example command:

```
ceph dashboard set-pwd-policy-check-username-enabled my_value
```

Parameters:

* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-pwd\-policy\-enabled[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the PWD\_POLICY\_ENABLED option value

Example command:

```
ceph dashboard set-pwd-policy-enabled my_value
```

Parameters:

* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-pwd\-policy\-exclusion\-list[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the PWD\_POLICY\_EXCLUSION\_LIST option value

Example command:

```
ceph dashboard set-pwd-policy-exclusion-list my_value
```

Parameters:

* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-pwd\-policy\-min\-complexity[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the PWD\_POLICY\_MIN\_COMPLEXITY option value

Example command:

```
ceph dashboard set-pwd-policy-min-complexity my_value
```

Parameters:

* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-pwd\-policy\-min\-length[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the PWD\_POLICY\_MIN\_LENGTH option value

Example command:

```
ceph dashboard set-pwd-policy-min-length my_value
```

Parameters:

* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-rest\-requests\-timeout[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the REST\_REQUESTS\_TIMEOUT option value

Example command:

```
ceph dashboard set-rest-requests-timeout my_value
```

Parameters:

* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-rgw\-api\-access\-key[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the RGW\_API\_ACCESS\_KEY option value read from \-i \<file\>

Example command:

```
ceph dashboard set-rgw-api-access-key
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-rgw\-api\-admin\-resource[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the RGW\_API\_ADMIN\_RESOURCE option value

Example command:

```
ceph dashboard set-rgw-api-admin-resource my_value
```

Parameters:

* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-rgw\-api\-secret\-key[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the RGW\_API\_SECRET\_KEY option value read from \-i \<file\>

Example command:

```
ceph dashboard set-rgw-api-secret-key
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-rgw\-api\-ssl\-verify[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the RGW\_API\_SSL\_VERIFY option value

Example command:

```
ceph dashboard set-rgw-api-ssl-verify my_value
```

Parameters:

* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-user\-pwd\-expiration\-span[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the USER\_PWD\_EXPIRATION\_SPAN option value

Example command:

```
ceph dashboard set-user-pwd-expiration-span my_value
```

Parameters:

* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-user\-pwd\-expiration\-warning\-1[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the USER\_PWD\_EXPIRATION\_WARNING\_1 option value

Example command:

```
ceph dashboard set-user-pwd-expiration-warning-1 my_value
```

Parameters:

* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard set\-user\-pwd\-expiration\-warning\-2[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the USER\_PWD\_EXPIRATION\_WARNING\_2 option value

Example command:

```
ceph dashboard set-user-pwd-expiration-warning-2 my_value
```

Parameters:

* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard sso disable[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Disable Single Sign\-On

Example command:

```
ceph dashboard sso disable
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard sso enable saml2[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Enable SAML2 Single Sign\-On

Example command:

```
ceph dashboard sso enable saml2
```

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard sso setup saml2[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Setup SAML2 Single Sign\-On

Example command:

```
ceph dashboard sso setup saml2 --ceph_dashboard_base_url=string --idp_metadata=string --idp_username_attribute=string --idp_entity_id=string --sp_x_509_cert=/path/to/file --sp_private_key=/path/to/file
```

Parameters:

* **ceph\_dashboard\_base\_url**: \(string\)
* **idp\_metadata**: \(string\)
* **idp\_username\_attribute**: \(string\)
* **idp\_entity\_id**: \(string\)
* **sp\_x\_509\_cert**: CephFilepath
* **sp\_private\_key**: CephFilepath

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# dashboard sso show saml2[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Show SAML2 configuration

Example command:

```
ceph dashboard sso show saml2
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# dashboard sso status[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get Single Sign\-On status

Example command:

```
ceph dashboard sso status
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# device light[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Enable or disable the device light. Default type is ident ‘Usage: device light \(on|off\) \<devid\> \[ident|fault\] \[–force\]’

Example command:

```
ceph device light --enable=on --devid=string --light_type=ident --force
```

Parameters:

* **enable**: CephChoices strings=\(on off\)
* **devid**: \(string\)
* **light\_type**: CephChoices strings=\(ident fault\)
* **force**: CephBool

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# device ls\-lights[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

List currently active device indicator lights

Example command:

```
ceph device ls-lights
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# fs clone cancel[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Cancel an pending or ongoing clone operation.

Example command:

```
ceph fs clone cancel my_vol_name my_clone_name my_group_name
```

Parameters:

* **vol\_name**: \(string\)
* **clone\_name**: \(string\)
* **group\_name**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# fs clone status[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get status on a cloned subvolume.

Example command:

```
ceph fs clone status my_vol_name my_clone_name my_group_name
```

Parameters:

* **vol\_name**: \(string\)
* **clone\_name**: \(string\)
* **group\_name**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# fs perf stats[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

retrieve ceph fs performance stats

Example command:

```
ceph fs perf stats my_mds_rank my_client_id my_client_ip
```

Parameters:

* **mds\_rank**: \(string\)
* **client\_id**: \(string\)
* **client\_ip**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# fs status[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Show the status of a CephFS filesystem

Example command:

```
ceph fs status my_fs
```

Parameters:

* **fs**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# fs subvolume authorize[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Allow a cephx auth ID access to a subvolume

Example command:

```
ceph fs subvolume authorize --vol_name=string --sub_name=string --auth_id=string --group_name=string --access_level=string --tenant_id=string --allow_existing_id
```

Parameters:

* **vol\_name**: \(string\)
* **sub\_name**: \(string\)
* **auth\_id**: \(string\)
* **group\_name**: \(string\)
* **access\_level**: \(string\)
* **tenant\_id**: \(string\)
* **allow\_existing\_id**: CephBool

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# fs subvolume authorized\_list[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

List auth IDs that have access to a subvolume

Example command:

```
ceph fs subvolume authorized_list my_vol_name my_sub_name my_group_name
```

Parameters:

* **vol\_name**: \(string\)
* **sub\_name**: \(string\)
* **group\_name**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# fs subvolume create[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Create a CephFS subvolume in a volume, and optionally, with a specific size \(in bytes\), a specific data pool layout, a specific mode, in a specific subvolume group and in separate RADOS namespace

Example command:

```
ceph fs subvolume create --vol_name=string --sub_name=string --size=1 --group_name=string --pool_layout=string --uid=1 --gid=1 --mode=string --namespace_isolated
```

Parameters:

* **vol\_name**: \(string\)
* **sub\_name**: goodchars= `[A-Za-z0-9-_.]`
* **size**: CephInt
* **group\_name**: \(string\)
* **pool\_layout**: \(string\)
* **uid**: CephInt
* **gid**: CephInt
* **mode**: \(string\)
* **namespace\_isolated**: CephBool

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# fs subvolume deauthorize[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Deny a cephx auth ID access to a subvolume

Example command:

```
ceph fs subvolume deauthorize --vol_name=string --sub_name=string --auth_id=string --group_name=string
```

Parameters:

* **vol\_name**: \(string\)
* **sub\_name**: \(string\)
* **auth\_id**: \(string\)
* **group\_name**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# fs subvolume evict[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Evict clients based on auth IDs and subvolume mounted

Example command:

```
ceph fs subvolume evict --vol_name=string --sub_name=string --auth_id=string --group_name=string
```

Parameters:

* **vol\_name**: \(string\)
* **sub\_name**: \(string\)
* **auth\_id**: \(string\)
* **group\_name**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# fs subvolume getpath[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the mountpath of a CephFS subvolume in a volume, and optionally, in a specific subvolume group

Example command:

```
ceph fs subvolume getpath my_vol_name my_sub_name my_group_name
```

Parameters:

* **vol\_name**: \(string\)
* **sub\_name**: \(string\)
* **group\_name**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# fs subvolume info[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the metadata of a CephFS subvolume in a volume, and optionally, in a specific subvolume group

Example command:

```
ceph fs subvolume info my_vol_name my_sub_name my_group_name
```

Parameters:

* **vol\_name**: \(string\)
* **sub\_name**: \(string\)
* **group\_name**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# fs subvolume ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

List subvolumes

Example command:

```
ceph fs subvolume ls my_vol_name my_group_name
```

Parameters:

* **vol\_name**: \(string\)
* **group\_name**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# fs subvolume pin[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set MDS pinning policy for subvolume

Example command:

```
ceph fs subvolume pin --vol_name=string --sub_name=string --pin_type=export --pin_setting=string --group_name=string
```

Parameters:

* **vol\_name**: \(string\)
* **sub\_name**: \(string\)
* **pin\_type**: CephChoices strings=\(export distributed random\)
* **pin\_setting**: \(string\)
* **group\_name**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# fs subvolume resize[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Resize a CephFS subvolume

Example command:

```
ceph fs subvolume resize --vol_name=string --sub_name=string --new_size=string --group_name=string --no_shrink
```

Parameters:

* **vol\_name**: \(string\)
* **sub\_name**: \(string\)
* **new\_size**: \(string\)
* **group\_name**: \(string\)
* **no\_shrink**: CephBool

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# fs subvolume rm[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Delete a CephFS subvolume in a volume, and optionally, in a specific subvolume group, force deleting a cancelled or failed clone, and retaining existing subvolume snapshots

Example command:

```
ceph fs subvolume rm --vol_name=string --sub_name=string --group_name=string --force --retain_snapshots
```

Parameters:

* **vol\_name**: \(string\)
* **sub\_name**: \(string\)
* **group\_name**: \(string\)
* **force**: CephBool
* **retain\_snapshots**: CephBool

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# fs subvolume snapshot clone[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Clone a snapshot to target subvolume

Example command:

```
ceph fs subvolume snapshot clone --vol_name=string --sub_name=string --snap_name=string --target_sub_name=string --pool_layout=string --group_name=string --target_group_name=string
```

Parameters:

* **vol\_name**: \(string\)
* **sub\_name**: \(string\)
* **snap\_name**: \(string\)
* **target\_sub\_name**: \(string\)
* **pool\_layout**: \(string\)
* **group\_name**: \(string\)
* **target\_group\_name**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# fs subvolume snapshot create[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Create a snapshot of a CephFS subvolume in a volume, and optionally, in a specific subvolume group

Example command:

```
ceph fs subvolume snapshot create --vol_name=string --sub_name=string --snap_name=string --group_name=string
```

Parameters:

* **vol\_name**: \(string\)
* **sub\_name**: \(string\)
* **snap\_name**: \(string\)
* **group\_name**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# fs subvolume snapshot info[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the metadata of a CephFS subvolume snapshot and optionally, in a specific subvolume group

Example command:

```
ceph fs subvolume snapshot info --vol_name=string --sub_name=string --snap_name=string --group_name=string
```

Parameters:

* **vol\_name**: \(string\)
* **sub\_name**: \(string\)
* **snap\_name**: \(string\)
* **group\_name**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# fs subvolume snapshot ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

List subvolume snapshots

Example command:

```
ceph fs subvolume snapshot ls my_vol_name my_sub_name my_group_name
```

Parameters:

* **vol\_name**: \(string\)
* **sub\_name**: \(string\)
* **group\_name**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# fs subvolume snapshot protect[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

\(deprecated\) Protect snapshot of a CephFS subvolume in a volume, and optionally, in a specific subvolume group

Example command:

```
ceph fs subvolume snapshot protect --vol_name=string --sub_name=string --snap_name=string --group_name=string
```

Parameters:

* **vol\_name**: \(string\)
* **sub\_name**: \(string\)
* **snap\_name**: \(string\)
* **group\_name**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# fs subvolume snapshot rm[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Delete a snapshot of a CephFS subvolume in a volume, and optionally, in a specific subvolume group

Example command:

```
ceph fs subvolume snapshot rm --vol_name=string --sub_name=string --snap_name=string --group_name=string --force
```

Parameters:

* **vol\_name**: \(string\)
* **sub\_name**: \(string\)
* **snap\_name**: \(string\)
* **group\_name**: \(string\)
* **force**: CephBool

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# fs subvolume snapshot unprotect[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

\(deprecated\) Unprotect a snapshot of a CephFS subvolume in a volume, and optionally, in a specific subvolume group

Example command:

```
ceph fs subvolume snapshot unprotect --vol_name=string --sub_name=string --snap_name=string --group_name=string
```

Parameters:

* **vol\_name**: \(string\)
* **sub\_name**: \(string\)
* **snap\_name**: \(string\)
* **group\_name**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# fs subvolumegroup create[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Create a CephFS subvolume group in a volume, and optionally, with a specific data pool layout, and a specific numeric mode

Example command:

```
ceph fs subvolumegroup create --vol_name=string --group_name=string --pool_layout=string --uid=1 --gid=1 --mode=string
```

Parameters:

* **vol\_name**: \(string\)
* **group\_name**: goodchars= `[A-Za-z0-9-_.]`
* **pool\_layout**: \(string\)
* **uid**: CephInt
* **gid**: CephInt
* **mode**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# fs subvolumegroup getpath[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Get the mountpath of a CephFS subvolume group in a volume

Example command:

```
ceph fs subvolumegroup getpath my_vol_name my_group_name
```

Parameters:

* **vol\_name**: \(string\)
* **group\_name**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# fs subvolumegroup ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

List subvolumegroups

Example command:

```
ceph fs subvolumegroup ls my_vol_name
```

Parameters:

* **vol\_name**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# fs subvolumegroup pin[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set MDS pinning policy for subvolumegroup

Example command:

```
ceph fs subvolumegroup pin --vol_name=string --group_name=string --pin_type=export --pin_setting=string
```

Parameters:

* **vol\_name**: \(string\)
* **group\_name**: \(string\)
* **pin\_type**: CephChoices strings=\(export distributed random\)
* **pin\_setting**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# fs subvolumegroup rm[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Delete a CephFS subvolume group in a volume

Example command:

```
ceph fs subvolumegroup rm my_vol_name my_group_name --force
```

Parameters:

* **vol\_name**: \(string\)
* **group\_name**: \(string\)
* **force**: CephBool

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# fs subvolumegroup snapshot create[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Create a snapshot of a CephFS subvolume group in a volume

Example command:

```
ceph fs subvolumegroup snapshot create my_vol_name my_group_name my_snap_name
```

Parameters:

* **vol\_name**: \(string\)
* **group\_name**: \(string\)
* **snap\_name**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# fs subvolumegroup snapshot ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

List subvolumegroup snapshots

Example command:

```
ceph fs subvolumegroup snapshot ls my_vol_name my_group_name
```

Parameters:

* **vol\_name**: \(string\)
* **group\_name**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# fs subvolumegroup snapshot rm[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Delete a snapshot of a CephFS subvolume group in a volume

Example command:

```
ceph fs subvolumegroup snapshot rm --vol_name=string --group_name=string --snap_name=string --force
```

Parameters:

* **vol\_name**: \(string\)
* **group\_name**: \(string\)
* **snap\_name**: \(string\)
* **force**: CephBool

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# fs volume create[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Create a CephFS volume

Example command:

```
ceph fs volume create my_name my_placement
```

Parameters:

* **name**: goodchars= `[A-Za-z0-9-_.]`
* **placement**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# fs volume ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

List volumes

Example command:

```
ceph fs volume ls
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# fs volume rm[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Delete a FS volume by passing –yes\-i\-really\-mean\-it flag

Example command:

```
ceph fs volume rm my_vol_name my_yes-i-really-mean-it
```

Parameters:

* **vol\_name**: \(string\)
* **yes\-i\-really\-mean\-it**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# hello[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Say hello

Example command:

```
ceph hello my_person_name
```

Parameters:

* **person\_name**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# influx config\-set[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set a configuration value

Example command:

```
ceph influx config-set my_key my_value
```

Parameters:

* **key**: \(string\)
* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# influx config\-show[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Show current configuration

Example command:

```
ceph influx config-show
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# influx send[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Force sending data to Influx

Example command:

```
ceph influx send
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# k8sevents ceph[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

List Ceph events tracked & sent to the kubernetes cluster

Example command:

```
ceph k8sevents ceph
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# k8sevents clear\-config[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Clear external kubernetes configuration settings

Example command:

```
ceph k8sevents clear-config
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# k8sevents ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

List all current Kuberenetes events from the Ceph namespace

Example command:

```
ceph k8sevents ls
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# k8sevents set\-access[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set kubernetes access credentials. \<key\> must be cacrt or token and use \-i \<filename\> syntax \(e.g., ceph k8sevents set\-access cacrt \-i /root/ca.crt\).

Example command:

```
ceph k8sevents set-access my_key
```

Parameters:

* **key**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# k8sevents set\-config[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set kubernetes config paramters. \<key\> must be server or namespace \(e.g., ceph k8sevents set\-config server [https://localhost:30433](https://docs.ceph.com/en/pacific/api/mon_command_api/)\).

Example command:

```
ceph k8sevents set-config my_key my_value
```

Parameters:

* **key**: \(string\)
* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# k8sevents status[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Show the status of the data gathering threads

Example command:

```
ceph k8sevents status
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# mgr self\-test background start[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Activate a background workload \(one of command\_spam, throw\_exception\)

Example command:

```
ceph mgr self-test background start my_workload
```

Parameters:

* **workload**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# mgr self\-test background stop[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Stop background workload if any is running

Example command:

```
ceph mgr self-test background stop
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# mgr self\-test cluster\-log[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Create an audit log record.

Example command:

```
ceph mgr self-test cluster-log my_channel my_priority my_message
```

Parameters:

* **channel**: \(string\)
* **priority**: \(string\)
* **message**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# mgr self\-test config get[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Peek at a configuration value

Example command:

```
ceph mgr self-test config get my_key
```

Parameters:

* **key**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# mgr self\-test config get\_localized[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Peek at a configuration value \(localized variant\)

Example command:

```
ceph mgr self-test config get_localized my_key
```

Parameters:

* **key**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# mgr self\-test health clear[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Clear health checks by name. If no names provided, clear all.

Example command:

```
ceph mgr self-test health clear --checks=string
```

Parameters:

* **checks**: \(can be repeated\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# mgr self\-test health set[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set a health check from a JSON\-formatted description.

Example command:

```
ceph mgr self-test health set my_checks
```

Parameters:

* **checks**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# mgr self\-test insights\_set\_now\_offset[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set the now time for the insights module.

Example command:

```
ceph mgr self-test insights_set_now_offset my_hours
```

Parameters:

* **hours**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# mgr self\-test module[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Run another module’s self\_test\(\) method

Example command:

```
ceph mgr self-test module my_module
```

Parameters:

* **module**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# mgr self\-test python\-version[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Query the version of the embedded Python runtime

Example command:

```
ceph mgr self-test python-version
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# mgr self\-test remote[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Test inter\-module calls

Example command:

```
ceph mgr self-test remote
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# mgr self\-test run[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Run mgr python interface tests

Example command:

```
ceph mgr self-test run
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Start, stop, restart, redeploy, or reconfig an entire service \(i.e. all daemons\)

Example command:

```
ceph orch start my_service_name
```

Parameters:

* **action**: CephChoices strings=\(start stop restart redeploy reconfig\)
* **service\_name**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch apply[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Update the size or placement for a service or apply a large yaml spec

Example command:

```
ceph orch apply --service_type=mon --placement=string --dry_run --format=plain --unmanaged --no_overwrite
```

Parameters:

* **service\_type**: CephChoices strings=\(mon mgr rbd\-mirror cephfs\-mirror crash alertmanager grafana node\-exporter prometheus mds rgw nfs iscsi cephadm\-exporter\)
* **placement**: \(string\)
* **dry\_run**: CephBool
* **format**: CephChoices strings=\(plain json json\-pretty yaml\)
* **unmanaged**: CephBool
* **no\_overwrite**: CephBool

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch apply iscsi[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Scale an iSCSI service

Example command:

```
ceph orch apply iscsi --pool=string --api_user=string --api_password=string --trusted_ip_list=string --placement=string --unmanaged --dry_run --format=plain --no_overwrite
```

Parameters:

* **pool**: \(string\)
* **api\_user**: \(string\)
* **api\_password**: \(string\)
* **trusted\_ip\_list**: \(string\)
* **placement**: \(string\)
* **unmanaged**: CephBool
* **dry\_run**: CephBool
* **format**: CephChoices strings=\(plain json json\-pretty yaml\)
* **no\_overwrite**: CephBool

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch apply mds[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Update the number of MDS instances for the given fs\_name

Example command:

```
ceph orch apply mds --fs_name=string --placement=string --dry_run --unmanaged --format=plain --no_overwrite
```

Parameters:

* **fs\_name**: \(string\)
* **placement**: \(string\)
* **dry\_run**: CephBool
* **unmanaged**: CephBool
* **format**: CephChoices strings=\(plain json json\-pretty yaml\)
* **no\_overwrite**: CephBool

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch apply nfs[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Scale an NFS service

Example command:

```
ceph orch apply nfs --svc_id=string --placement=string --format=plain --port=1 --dry_run --unmanaged --no_overwrite
```

Parameters:

* **svc\_id**: \(string\)
* **placement**: \(string\)
* **format**: CephChoices strings=\(plain json json\-pretty yaml\)
* **port**: CephInt
* **dry\_run**: CephBool
* **unmanaged**: CephBool
* **no\_overwrite**: CephBool

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch apply osd[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Create OSD daemon\(s\) using a drive group spec

Example command:

```
ceph orch apply osd --all_available_devices --format=plain --unmanaged --dry_run --no_overwrite
```

Parameters:

* **all\_available\_devices**: CephBool
* **format**: CephChoices strings=\(plain json json\-pretty yaml\)
* **unmanaged**: CephBool
* **dry\_run**: CephBool
* **no\_overwrite**: CephBool

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch apply rgw[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Update the number of RGW instances for the given zone

Example command:

```
ceph orch apply rgw --svc_id=string --placement=string --realm=string --zone=string --port=1 --ssl --dry_run --format=plain --unmanaged --no_overwrite
```

Parameters:

* **svc\_id**: \(string\)
* **placement**: \(string\)
* **realm**: \(string\)
* **zone**: \(string\)
* **port**: CephInt
* **ssl**: CephBool
* **dry\_run**: CephBool
* **format**: CephChoices strings=\(plain json json\-pretty yaml\)
* **unmanaged**: CephBool
* **no\_overwrite**: CephBool

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch cancel[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Cancel ongoing background operations

Example command:

```
ceph orch cancel
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch client\-keyring ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

List client keyrings under cephadm management

Example command:

```
ceph orch client-keyring ls plain
```

Parameters:

* **format**: CephChoices strings=\(plain json json\-pretty yaml\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# orch client\-keyring rm[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Remove client keyring from cephadm management

Example command:

```
ceph orch client-keyring rm my_entity
```

Parameters:

* **entity**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch client\-keyring set[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Add or update client keyring under cephadm management

Example command:

```
ceph orch client-keyring set --entity=string --placement=string --owner=string --mode=string
```

Parameters:

* **entity**: \(string\)
* **placement**: \(string\)
* **owner**: \(string\)
* **mode**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch daemon[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Start, stop, restart, \(redeploy,\) or reconfig a specific daemon

Example command:

```
ceph orch daemon start my_name
```

Parameters:

* **action**: CephChoices strings=\(start stop restart reconfig\)
* **name**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch daemon add[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Add daemon\(s\)

Example command:

```
ceph orch daemon add mon my_placement
```

Parameters:

* **daemon\_type**: CephChoices strings=\(mon mgr rbd\-mirror cephfs\-mirror crash alertmanager grafana node\-exporter prometheus mds rgw nfs iscsi cephadm\-exporter\)
* **placement**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch daemon add iscsi[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Start iscsi daemon\(s\)

Example command:

```
ceph orch daemon add iscsi --pool=string --api_user=string --api_password=string --trusted_ip_list=string --placement=string
```

Parameters:

* **pool**: \(string\)
* **api\_user**: \(string\)
* **api\_password**: \(string\)
* **trusted\_ip\_list**: \(string\)
* **placement**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch daemon add mds[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Start MDS daemon\(s\)

Example command:

```
ceph orch daemon add mds my_fs_name my_placement
```

Parameters:

* **fs\_name**: \(string\)
* **placement**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch daemon add nfs[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Start NFS daemon\(s\)

Example command:

```
ceph orch daemon add nfs my_svc_id my_placement
```

Parameters:

* **svc\_id**: \(string\)
* **placement**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch daemon add osd[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Create an OSD service. Either –svc\_arg=host:drives

Example command:

```
ceph orch daemon add osd my_svc_arg
```

Parameters:

* **svc\_arg**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch daemon add rgw[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Start RGW daemon\(s\)

Example command:

```
ceph orch daemon add rgw --svc_id=string --placement=string --port=1 --ssl
```

Parameters:

* **svc\_id**: \(string\)
* **placement**: \(string\)
* **port**: CephInt
* **ssl**: CephBool

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch daemon redeploy[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Redeploy a daemon \(with a specifc image\)

Example command:

```
ceph orch daemon redeploy my_name my_image
```

Parameters:

* **name**: \(string\)
* **image**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch daemon rm[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Remove specific daemon\(s\)

Example command:

```
ceph orch daemon rm --names=string --force
```

Parameters:

* **names**: \(can be repeated\)
* **force**: CephBool

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch device ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

List devices on a host

Example command:

```
ceph orch device ls --hostname=string --format=plain --refresh --wide
```

Parameters:

* **hostname**: \(can be repeated\)
* **format**: CephChoices strings=\(plain json json\-pretty yaml\)
* **refresh**: CephBool
* **wide**: CephBool

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# orch device zap[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Zap \(erase\!\) a device so it can be re\-used

Example command:

```
ceph orch device zap my_hostname my_path --force
```

Parameters:

* **hostname**: \(string\)
* **path**: \(string\)
* **force**: CephBool

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch host add[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Add a host

Example command:

```
ceph orch host add --hostname=string --addr=string --labels=string --maintenance
```

Parameters:

* **hostname**: \(string\)
* **addr**: \(string\)
* **labels**: \(can be repeated\)
* **maintenance**: CephBool

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch host drain[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

drain all daemons from a host

Example command:

```
ceph orch host drain my_hostname
```

Parameters:

* **hostname**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch host label add[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Add a host label

Example command:

```
ceph orch host label add my_hostname my_label
```

Parameters:

* **hostname**: \(string\)
* **label**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch host label rm[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Remove a host label

Example command:

```
ceph orch host label rm my_hostname my_label
```

Parameters:

* **hostname**: \(string\)
* **label**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch host ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

List hosts

Example command:

```
ceph orch host ls plain
```

Parameters:

* **format**: CephChoices strings=\(plain json json\-pretty yaml\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# orch host maintenance enter[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Prepare a host for maintenance by shutting down and disabling all Ceph daemons \(cephadm only\)

Example command:

```
ceph orch host maintenance enter my_hostname --force
```

Parameters:

* **hostname**: \(string\)
* **force**: CephBool

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch host maintenance exit[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Return a host from maintenance, restarting all Ceph daemons \(cephadm only\)

Example command:

```
ceph orch host maintenance exit my_hostname
```

Parameters:

* **hostname**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch host ok\-to\-stop[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Check if the specified host can be safely stopped without reducing availability

Example command:

```
ceph orch host ok-to-stop my_hostname
```

Parameters:

* **hostname**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch host rm[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Remove a host

Example command:

```
ceph orch host rm my_hostname --force --offline
```

Parameters:

* **hostname**: \(string\)
* **force**: CephBool
* **offline**: CephBool

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch host set\-addr[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Update a host address

Example command:

```
ceph orch host set-addr my_hostname my_addr
```

Parameters:

* **hostname**: \(string\)
* **addr**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

List services known to orchestrator

Example command:

```
ceph orch ls --service_type=string --service_name=string --export --format=plain --refresh
```

Parameters:

* **service\_type**: \(string\)
* **service\_name**: \(string\)
* **export**: CephBool
* **format**: CephChoices strings=\(plain json json\-pretty yaml\)
* **refresh**: CephBool

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# orch osd rm[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Remove OSD daemons

Example command:

```
ceph orch osd rm --osd_id=string --replace --force --zap
```

Parameters:

* **osd\_id**: \(can be repeated\)
* **replace**: CephBool
* **force**: CephBool
* **zap**: CephBool

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch osd rm status[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Status of OSD removal operation

Example command:

```
ceph orch osd rm status plain
```

Parameters:

* **format**: CephChoices strings=\(plain json json\-pretty yaml\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch osd rm stop[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Cancel ongoing OSD removal operation

Example command:

```
ceph orch osd rm stop --osd_id=string
```

Parameters:

* **osd\_id**: \(can be repeated\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch pause[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Pause orchestrator background work

Example command:

```
ceph orch pause
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch ps[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

List daemons known to orchestrator

Example command:

```
ceph orch ps --hostname=string --service_name=string --daemon_type=string --daemon_id=string --format=plain --refresh
```

Parameters:

* **hostname**: \(string\)
* **service\_name**: \(string\)
* **daemon\_type**: \(string\)
* **daemon\_id**: \(string\)
* **format**: CephChoices strings=\(plain json json\-pretty yaml\)
* **refresh**: CephBool

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# orch resume[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Resume orchestrator background work \(if paused\)

Example command:

```
ceph orch resume
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch rm[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Remove a service

Example command:

```
ceph orch rm my_service_name --force
```

Parameters:

* **service\_name**: \(string\)
* **force**: CephBool

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch set backend[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Select orchestrator module backend

Example command:

```
ceph orch set backend my_module_name
```

Parameters:

* **module\_name**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch status[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Report configured backend and its status

Example command:

```
ceph orch status --detail plain
```

Parameters:

* **detail**: CephBool
* **format**: CephChoices strings=\(plain json json\-pretty yaml\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# orch upgrade check[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Check service versions vs available and target containers

Example command:

```
ceph orch upgrade check my_image my_ceph_version
```

Parameters:

* **image**: \(string\)
* **ceph\_version**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch upgrade ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Check for available versions \(or tags\) we can upgrade to

Example command:

```
ceph orch upgrade ls my_image --tags
```

Parameters:

* **image**: \(string\)
* **tags**: CephBool

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# orch upgrade pause[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Pause an in\-progress upgrade

Example command:

```
ceph orch upgrade pause
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch upgrade resume[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Resume paused upgrade

Example command:

```
ceph orch upgrade resume
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch upgrade start[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Initiate upgrade

Example command:

```
ceph orch upgrade start my_image my_ceph_version
```

Parameters:

* **image**: \(string\)
* **ceph\_version**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch upgrade status[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Check service versions vs available and target containers

Example command:

```
ceph orch upgrade status
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# orch upgrade stop[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Stop an in\-progress upgrade

Example command:

```
ceph orch upgrade stop
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# osd perf counters get[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

fetch osd perf counters

Example command:

```
ceph osd perf counters get 1
```

Parameters:

* **query\_id**: CephInt

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# osd perf query add[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

add osd perf query

Example command:

```
ceph osd perf query add client_id
```

Parameters:

* **query**: CephChoices strings=\(client\_id rbd\_image\_id all\_subkeys\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# osd perf query remove[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

remove osd perf query

Example command:

```
ceph osd perf query remove 1
```

Parameters:

* **query\_id**: CephInt

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# osd status[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Show the status of OSDs within a bucket, or all

Example command:

```
ceph osd status my_bucket
```

Parameters:

* **bucket**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# progress[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Show progress of recovery operations

Example command:

```
ceph progress
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# progress clear[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Reset progress tracking

Example command:

```
ceph progress clear
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# progress json[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Show machine readable progress information

Example command:

```
ceph progress json
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# progress off[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Disable progress tracking

Example command:

```
ceph progress off
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# progress on[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Enable progress tracking

Example command:

```
ceph progress on
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# rbd mirror snapshot schedule add[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Add rbd mirror snapshot schedule

Example command:

```
ceph rbd mirror snapshot schedule add my_level_spec my_interval my_start_time
```

Parameters:

* **level\_spec**: \(string\)
* **interval**: \(string\)
* **start\_time**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# rbd mirror snapshot schedule list[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

List rbd mirror snapshot schedule

Example command:

```
ceph rbd mirror snapshot schedule list my_level_spec
```

Parameters:

* **level\_spec**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# rbd mirror snapshot schedule remove[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Remove rbd mirror snapshot schedule

Example command:

```
ceph rbd mirror snapshot schedule remove my_level_spec my_interval my_start_time
```

Parameters:

* **level\_spec**: \(string\)
* **interval**: \(string\)
* **start\_time**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# rbd mirror snapshot schedule status[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Show rbd mirror snapshot schedule status

Example command:

```
ceph rbd mirror snapshot schedule status my_level_spec
```

Parameters:

* **level\_spec**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# rbd perf image counters[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Retrieve current RBD IO performance counters

Example command:

```
ceph rbd perf image counters my_pool_spec write_ops
```

Parameters:

* **pool\_spec**: \(string\)
* **sort\_by**: CephChoices strings=\(write\_ops write\_bytes write\_latency read\_ops read\_bytes read\_latency\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# rbd perf image stats[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Retrieve current RBD IO performance stats

Example command:

```
ceph rbd perf image stats my_pool_spec write_ops
```

Parameters:

* **pool\_spec**: \(string\)
* **sort\_by**: CephChoices strings=\(write\_ops write\_bytes write\_latency read\_ops read\_bytes read\_latency\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# rbd task add flatten[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Flatten a cloned image asynchronously in the background

Example command:

```
ceph rbd task add flatten my_image_spec
```

Parameters:

* **image\_spec**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# rbd task add migration abort[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Abort a prepared migration asynchronously in the background

Example command:

```
ceph rbd task add migration abort my_image_spec
```

Parameters:

* **image\_spec**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# rbd task add migration commit[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Commit an executed migration asynchronously in the background

Example command:

```
ceph rbd task add migration commit my_image_spec
```

Parameters:

* **image\_spec**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# rbd task add migration execute[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Execute an image migration asynchronously in the background

Example command:

```
ceph rbd task add migration execute my_image_spec
```

Parameters:

* **image\_spec**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# rbd task add remove[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Remove an image asynchronously in the background

Example command:

```
ceph rbd task add remove my_image_spec
```

Parameters:

* **image\_spec**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# rbd task add trash remove[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Remove an image from the trash asynchronously in the background

Example command:

```
ceph rbd task add trash remove my_image_id_spec
```

Parameters:

* **image\_id\_spec**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# rbd task cancel[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Cancel a pending or running asynchronous task

Example command:

```
ceph rbd task cancel my_task_id
```

Parameters:

* **task\_id**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# rbd task list[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

List pending or running asynchronous tasks

Example command:

```
ceph rbd task list my_task_id
```

Parameters:

* **task\_id**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# rbd trash purge schedule add[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Add rbd trash purge schedule

Example command:

```
ceph rbd trash purge schedule add my_level_spec my_interval my_start_time
```

Parameters:

* **level\_spec**: \(string\)
* **interval**: \(string\)
* **start\_time**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# rbd trash purge schedule list[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

List rbd trash purge schedule

Example command:

```
ceph rbd trash purge schedule list my_level_spec
```

Parameters:

* **level\_spec**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# rbd trash purge schedule remove[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Remove rbd trash purge schedule

Example command:

```
ceph rbd trash purge schedule remove my_level_spec my_interval my_start_time
```

Parameters:

* **level\_spec**: \(string\)
* **interval**: \(string\)
* **start\_time**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _w_

Command Flags:

* _mgr_

# rbd trash purge schedule status[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Show rbd trash purge schedule status

Example command:

```
ceph rbd trash purge schedule status my_level_spec
```

Parameters:

* **level\_spec**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# restful create\-key[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Create an API key with this name

Example command:

```
ceph restful create-key my_key_name
```

Parameters:

* **key\_name**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# restful create\-self\-signed\-cert[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Create localized self signed certificate

Example command:

```
ceph restful create-self-signed-cert
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# restful delete\-key[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Delete an API key with this name

Example command:

```
ceph restful delete-key my_key_name
```

Parameters:

* **key\_name**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# restful list\-keys[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

List all API keys

Example command:

```
ceph restful list-keys
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# restful restart[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Restart API server

Example command:

```
ceph restful restart
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# telegraf config\-set[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set a configuration value

Example command:

```
ceph telegraf config-set my_key my_value
```

Parameters:

* **key**: \(string\)
* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# telegraf config\-show[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Show current configuration

Example command:

```
ceph telegraf config-show
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# telegraf send[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Force sending data to Telegraf

Example command:

```
ceph telegraf send
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# telemetry off[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Disable telemetry reports from this cluster

Example command:

```
ceph telemetry off
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# telemetry on[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Enable telemetry reports from this cluster

Example command:

```
ceph telemetry on my_license
```

Parameters:

* **license**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# telemetry send[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Force sending data to Ceph telemetry

Example command:

```
ceph telemetry send --endpoint=ceph --license=string
```

Parameters:

* **endpoint**: CephChoices strings=\(ceph device\) \(can be repeated\)
* **license**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# telemetry show[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Show last report or report to be sent

Example command:

```
ceph telemetry show --channels=string
```

Parameters:

* **channels**: \(can be repeated\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# telemetry show\-all[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Show report of all channels

Example command:

```
ceph telemetry show-all
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# telemetry show\-device[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Show last device report or device report to be sent

Example command:

```
ceph telemetry show-device
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# telemetry status[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Show current configuration

Example command:

```
ceph telemetry status
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# zabbix config\-set[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set a configuration value

Example command:

```
ceph zabbix config-set my_key my_value
```

Parameters:

* **key**: \(string\)
* **value**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# zabbix config\-show[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Show current configuration

Example command:

```
ceph zabbix config-show
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# zabbix discovery[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Discovering Zabbix data

Example command:

```
ceph zabbix discovery
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

Command Flags:

* _mgr_

# zabbix send[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Force sending data to Zabbix

Example command:

```
ceph zabbix send
```

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

Command Flags:

* _mgr_

# add\_bootstrap\_peer\_hint[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

add peer address as potential bootstrap peer for cluster bringup

Example command:

```
ceph add_bootstrap_peer_hint 0.0.0.0
```

Parameters:

* **addr**: CephIPAddr

Ceph Module:

* _mon_

Required Permissions:

* _rw_

# add\_bootstrap\_peer\_hintv[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

add peer address vector as potential bootstrap peer for cluster bringup

Example command:

```
ceph add_bootstrap_peer_hintv my_addrv
```

Parameters:

* **addrv**: \(string\)

Ceph Module:

* _mon_

Required Permissions:

* _rw_

# auth add[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

add auth info for \<entity\> from input file, or random key if no input is given, and/or any caps specified in the command

Example command:

```
ceph auth add --entity=string --caps=string
```

Parameters:

* **entity**: \(string\)
* **caps**: \(can be repeated\)

Ceph Module:

* _auth_

Required Permissions:

* _rwx_

# auth caps[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

update caps for \<name\> from caps specified in the command

Example command:

```
ceph auth caps --entity=string --caps=string
```

Parameters:

* **entity**: \(string\)
* **caps**: \(can be repeated\)

Ceph Module:

* _auth_

Required Permissions:

* _rwx_

# auth del[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

delete all caps for \<name\>

Example command:

```
ceph auth del my_entity
```

Parameters:

* **entity**: \(string\)

Ceph Module:

* _auth_

Required Permissions:

* _rwx_

Command Flags:

* _deprecated_

# auth export[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

write keyring for requested entity, or master keyring if none given

Example command:

```
ceph auth export my_entity
```

Parameters:

* **entity**: \(string\)

Ceph Module:

* _auth_

Required Permissions:

* _rx_

# auth get[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

write keyring file with requested key

Example command:

```
ceph auth get my_entity
```

Parameters:

* **entity**: \(string\)

Ceph Module:

* _auth_

Required Permissions:

* _rx_

# auth get\-key[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

display requested key

Example command:

```
ceph auth get-key my_entity
```

Parameters:

* **entity**: \(string\)

Ceph Module:

* _auth_

Required Permissions:

* _rx_

# auth get\-or\-create[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

add auth info for \<entity\> from input file, or random key if no input given, and/or any caps specified in the command

Example command:

```
ceph auth get-or-create --entity=string --caps=string
```

Parameters:

* **entity**: \(string\)
* **caps**: \(can be repeated\)

Ceph Module:

* _auth_

Required Permissions:

* _rwx_

# auth get\-or\-create\-key[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

get, or add, key for \<name\> from system/caps pairs specified in the command. If key already exists, any given caps must match the existing caps for that key.

Example command:

```
ceph auth get-or-create-key --entity=string --caps=string
```

Parameters:

* **entity**: \(string\)
* **caps**: \(can be repeated\)

Ceph Module:

* _auth_

Required Permissions:

* _rwx_

# auth import[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

auth import: read keyring file from \-i \<file\>

Example command:

```
ceph auth import
```

Ceph Module:

* _auth_

Required Permissions:

* _rwx_

# auth list[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

list authentication state

Example command:

```
ceph auth list
```

Ceph Module:

* _auth_

Required Permissions:

* _rx_

Command Flags:

* _deprecated_

# auth ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

list authentication state

Example command:

```
ceph auth ls
```

Ceph Module:

* _auth_

Required Permissions:

* _rx_

# auth print\-key[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

display requested key

Example command:

```
ceph auth print-key my_entity
```

Parameters:

* **entity**: \(string\)

Ceph Module:

* _auth_

Required Permissions:

* _rx_

# auth print\_key[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

display requested key

Example command:

```
ceph auth print_key my_entity
```

Parameters:

* **entity**: \(string\)

Ceph Module:

* _auth_

Required Permissions:

* _rx_

# auth rm[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

remove all caps for \<name\>

Example command:

```
ceph auth rm my_entity
```

Parameters:

* **entity**: \(string\)

Ceph Module:

* _auth_

Required Permissions:

* _rwx_

# compact[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

cause compaction of monitor’s leveldb/rocksdb storage

Example command:

```
ceph compact
```

Ceph Module:

* _mon_

Required Permissions:

* _rw_

# config assimilate\-conf[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Assimilate options from a conf, and return a new, minimal conf file

Example command:

```
ceph config assimilate-conf
```

Ceph Module:

* _config_

Required Permissions:

* _rw_

# config dump[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Show all configuration option\(s\)

Example command:

```
ceph config dump
```

Ceph Module:

* _mon_

Required Permissions:

* _r_

# config generate\-minimal\-conf[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Generate a minimal ceph.conf file

Example command:

```
ceph config generate-minimal-conf
```

Ceph Module:

* _config_

Required Permissions:

* _r_

# config get[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Show configuration option\(s\) for an entity

Example command:

```
ceph config get string my_key
```

Parameters:

* **who**: \(string\)
* **key**: \(string\)

Ceph Module:

* _config_

Required Permissions:

* _r_

# config help[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Describe a configuration option

Example command:

```
ceph config help my_key
```

Parameters:

* **key**: \(string\)

Ceph Module:

* _config_

Required Permissions:

* _r_

# config log[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Show recent history of config changes

Example command:

```
ceph config log 1
```

Parameters:

* **num**: CephInt

Ceph Module:

* _config_

Required Permissions:

* _r_

# config ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

List available configuration options

Example command:

```
ceph config ls
```

Ceph Module:

* _config_

Required Permissions:

* _r_

# config reset[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Revert configuration to a historical version specified by \<num\>

Example command:

```
ceph config reset 1
```

Parameters:

* **num**: CephInt range= `0`

Ceph Module:

* _config_

Required Permissions:

* _rw_

# config rm[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Clear a configuration option for one or more entities

Example command:

```
ceph config rm string my_name
```

Parameters:

* **who**: \(string\)
* **name**: \(string\)

Ceph Module:

* _config_

Required Permissions:

* _rw_

# config set[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set a configuration option for one or more entities

Example command:

```
ceph config set --who=string --name=string --value=string --force
```

Parameters:

* **who**: \(string\)
* **name**: \(string\)
* **value**: \(string\)
* **force**: CephBool

Ceph Module:

* _config_

Required Permissions:

* _rw_

# config show[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Show running configuration

Example command:

```
ceph config show string my_key
```

Parameters:

* **who**: \(string\)
* **key**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

# config show\-with\-defaults[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Show running configuration \(including compiled\-in defaults\)

Example command:

```
ceph config show-with-defaults string
```

Parameters:

* **who**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

# config\-key del[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

delete \<key\>

Example command:

```
ceph config-key del my_key
```

Parameters:

* **key**: \(string\)

Ceph Module:

* _config\-key_

Required Permissions:

* _rw_

Command Flags:

* _deprecated_

# config\-key dump[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

dump keys and values \(with optional prefix\)

Example command:

```
ceph config-key dump my_key
```

Parameters:

* **key**: \(string\)

Ceph Module:

* _config\-key_

Required Permissions:

* _r_

# config\-key exists[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

check for \<key\>’s existence

Example command:

```
ceph config-key exists my_key
```

Parameters:

* **key**: \(string\)

Ceph Module:

* _config\-key_

Required Permissions:

* _r_

# config\-key get[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

get \<key\>

Example command:

```
ceph config-key get my_key
```

Parameters:

* **key**: \(string\)

Ceph Module:

* _config\-key_

Required Permissions:

* _r_

# config\-key list[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

list keys

Example command:

```
ceph config-key list
```

Ceph Module:

* _config\-key_

Required Permissions:

* _r_

Command Flags:

* _deprecated_

# config\-key ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

list keys

Example command:

```
ceph config-key ls
```

Ceph Module:

* _config\-key_

Required Permissions:

* _r_

# config\-key put[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

put \<key\>, value \<val\>

Example command:

```
ceph config-key put my_key my_val
```

Parameters:

* **key**: \(string\)
* **val**: \(string\)

Ceph Module:

* _config\-key_

Required Permissions:

* _rw_

Command Flags:

* _deprecated_

# config\-key rm[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

rm \<key\>

Example command:

```
ceph config-key rm my_key
```

Parameters:

* **key**: \(string\)

Ceph Module:

* _config\-key_

Required Permissions:

* _rw_

# config\-key set[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set \<key\> to value \<val\>

Example command:

```
ceph config-key set my_key my_val
```

Parameters:

* **key**: \(string\)
* **val**: \(string\)

Ceph Module:

* _config\-key_

Required Permissions:

* _rw_

# connection scores dump[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

show the scores used in connectivity\-based elections

Example command:

```
ceph connection scores dump
```

Ceph Module:

* _mon_

Required Permissions:

* _rwx_

# connection scores reset[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

reset the scores used in connectivity\-based elections

Example command:

```
ceph connection scores reset
```

Ceph Module:

* _mon_

Required Permissions:

* _rwx_

# device info[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Show information about a device

Example command:

```
ceph device info my_devid
```

Parameters:

* **devid**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

# device ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Show devices

Example command:

```
ceph device ls
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

# device ls\-by\-daemon[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Show devices associated with a daemon

Example command:

```
ceph device ls-by-daemon string
```

Parameters:

* **who**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

# device ls\-by\-host[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Show devices on a host

Example command:

```
ceph device ls-by-host my_host
```

Parameters:

* **host**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

# device rm\-life\-expectancy[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Clear predicted device life expectancy

Example command:

```
ceph device rm-life-expectancy my_devid
```

Parameters:

* **devid**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

# device set\-life\-expectancy[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set predicted device life expectancy

Example command:

```
ceph device set-life-expectancy my_devid my_from my_to
```

Parameters:

* **devid**: \(string\)
* **from**: \(string\)
* **to**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

# df[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

show cluster free space stats

Example command:

```
ceph df detail
```

Parameters:

* **detail**: CephChoices strings=\(detail\)

Ceph Module:

* _mon_

Required Permissions:

* _r_

# dump\_historic\_ops[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

dump\_historic\_ops

Example command:

```
ceph dump_historic_ops
```

Ceph Module:

* _mon_

Required Permissions:

* _r_

# features[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

report of connected features

Example command:

```
ceph features
```

Ceph Module:

* _mon_

Required Permissions:

* _r_

# fs add\_data\_pool[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

add data pool \<pool\>

Example command:

```
ceph fs add_data_pool my_fs_name my_pool
```

Parameters:

* **fs\_name**: \(string\)
* **pool**: \(string\)

Ceph Module:

* _mds_

Required Permissions:

* _rw_

# fs authorize[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

add auth for \<entity\> to access file system \<filesystem\> based on following directory and permissions pairs

Example command:

```
ceph fs authorize --filesystem=string --entity=string --caps=string
```

Parameters:

* **filesystem**: \(string\)
* **entity**: \(string\)
* **caps**: \(can be repeated\)

Ceph Module:

* _auth_

Required Permissions:

* _rwx_

# fs compat[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

manipulate compat settings

Example command:

```
ceph fs compat --fs_name=string --subop=rm_compat --feature=1 --feature_str=string
```

Parameters:

* **fs\_name**: \(string\)
* **subop**: CephChoices strings=\(rm\_compat rm\_incompat add\_compat add\_incompat\)
* **feature**: CephInt
* **feature\_str**: \(string\)

Ceph Module:

* _fs_

Required Permissions:

* _rw_

# fs compat show[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

show fs compatibility settings

Example command:

```
ceph fs compat show my_fs_name
```

Parameters:

* **fs\_name**: \(string\)

Ceph Module:

* _mds_

Required Permissions:

* _r_

# fs dump[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

dump all CephFS status, optionally from epoch

Example command:

```
ceph fs dump 1
```

Parameters:

* **epoch**: CephInt range= `0`

Ceph Module:

* _mds_

Required Permissions:

* _r_

# fs fail[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

bring the file system down and all of its ranks

Example command:

```
ceph fs fail my_fs_name
```

Parameters:

* **fs\_name**: \(string\)

Ceph Module:

* _fs_

Required Permissions:

* _rw_

# fs feature ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

list available cephfs features to be set/unset

Example command:

```
ceph fs feature ls
```

Ceph Module:

* _mds_

Required Permissions:

* _r_

# fs flag set[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Set a global CephFS flag

Example command:

```
ceph fs flag set enable_multiple my_val --yes_i_really_mean_it
```

Parameters:

* **flag\_name**: CephChoices strings=\(enable\_multiple\)
* **val**: \(string\)
* **yes\_i\_really\_mean\_it**: CephBool

Ceph Module:

* _fs_

Required Permissions:

* _rw_

# fs get[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

get info about one filesystem

Example command:

```
ceph fs get my_fs_name
```

Parameters:

* **fs\_name**: \(string\)

Ceph Module:

* _fs_

Required Permissions:

* _r_

# fs ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

list filesystems

Example command:

```
ceph fs ls
```

Ceph Module:

* _fs_

Required Permissions:

* _r_

# fs mirror disable[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

disable mirroring for a ceph filesystem

Example command:

```
ceph fs mirror disable my_fs_name
```

Parameters:

* **fs\_name**: \(string\)

Ceph Module:

* _mds_

Required Permissions:

* _rw_

# fs mirror enable[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

enable mirroring for a ceph filesystem

Example command:

```
ceph fs mirror enable my_fs_name
```

Parameters:

* **fs\_name**: \(string\)

Ceph Module:

* _mds_

Required Permissions:

* _rw_

# fs mirror peer\_add[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

add a mirror peer for a ceph filesystem

Example command:

```
ceph fs mirror peer_add --fs_name=string --uuid=string --remote_cluster_spec=string --remote_fs_name=string
```

Parameters:

* **fs\_name**: \(string\)
* **uuid**: \(string\)
* **remote\_cluster\_spec**: \(string\)
* **remote\_fs\_name**: \(string\)

Ceph Module:

* _mds_

Required Permissions:

* _rw_

# fs mirror peer\_remove[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

remove a mirror peer for a ceph filesystem

Example command:

```
ceph fs mirror peer_remove my_fs_name my_uuid
```

Parameters:

* **fs\_name**: \(string\)
* **uuid**: \(string\)

Ceph Module:

* _mds_

Required Permissions:

* _rw_

# fs new[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

make new filesystem using named pools \<metadata\> and \<data\>

Example command:

```
ceph fs new --fs_name=string --metadata=string --data=string --force --allow_dangerous_metadata_overlay --fscid=0
```

Parameters:

* **fs\_name**: goodchars= `[A-Za-z0-9-_.]`
* **metadata**: \(string\)
* **data**: \(string\)
* **force**: CephBool
* **allow\_dangerous\_metadata\_overlay**: CephBool
* **fscid**: CephInt range= `0`

Ceph Module:

* _fs_

Required Permissions:

* _rw_

# fs required\_client\_features[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

add/remove required features of clients

Example command:

```
ceph fs required_client_features my_fs_name add my_val
```

Parameters:

* **fs\_name**: \(string\)
* **subop**: CephChoices strings=\(add rm\)
* **val**: \(string\)

Ceph Module:

* _mds_

Required Permissions:

* _rw_

# fs reset[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

disaster recovery only: reset to a single\-MDS map

Example command:

```
ceph fs reset my_fs_name --yes_i_really_mean_it
```

Parameters:

* **fs\_name**: \(string\)
* **yes\_i\_really\_mean\_it**: CephBool

Ceph Module:

* _fs_

Required Permissions:

* _rw_

# fs rm[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

disable the named filesystem

Example command:

```
ceph fs rm my_fs_name --yes_i_really_mean_it
```

Parameters:

* **fs\_name**: \(string\)
* **yes\_i\_really\_mean\_it**: CephBool

Ceph Module:

* _fs_

Required Permissions:

* _rw_

# fs rm\_data\_pool[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

remove data pool \<pool\>

Example command:

```
ceph fs rm_data_pool my_fs_name my_pool
```

Parameters:

* **fs\_name**: \(string\)
* **pool**: \(string\)

Ceph Module:

* _mds_

Required Permissions:

* _rw_

# fs set[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set fs parameter \<var\> to \<val\>

Example command:

```
ceph fs set --fs_name=string --var=max_mds --val=string --yes_i_really_mean_it --yes_i_really_really_mean_it
```

Parameters:

* **fs\_name**: \(string\)
* **var**: CephChoices strings=\(max\_mds max\_file\_size allow\_new\_snaps inline\_data cluster\_down allow\_dirfrags balancer standby\_count\_wanted session\_timeout session\_autoclose allow\_standby\_replay down joinable min\_compat\_client\)
* **val**: \(string\)
* **yes\_i\_really\_mean\_it**: CephBool
* **yes\_i\_really\_really\_mean\_it**: CephBool

Ceph Module:

* _mds_

Required Permissions:

* _rw_

# fs set\-default[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set the default to the named filesystem

Example command:

```
ceph fs set-default my_fs_name
```

Parameters:

* **fs\_name**: \(string\)

Ceph Module:

* _fs_

Required Permissions:

* _rw_

# fs set\_default[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set the default to the named filesystem

Example command:

```
ceph fs set_default my_fs_name
```

Parameters:

* **fs\_name**: \(string\)

Ceph Module:

* _fs_

Required Permissions:

* _rw_

Command Flags:

* _deprecated_

# fsid[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

show cluster FSID/UUID

Example command:

```
ceph fsid
```

Ceph Module:

* _mon_

Required Permissions:

* _r_

# health[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

show cluster health

Example command:

```
ceph health detail
```

Parameters:

* **detail**: CephChoices strings=\(detail\)

Ceph Module:

* _mon_

Required Permissions:

* _r_

# health mute[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

mute health alert

Example command:

```
ceph health mute my_code my_ttl --sticky
```

Parameters:

* **code**: \(string\)
* **ttl**: \(string\)
* **sticky**: CephBool

Ceph Module:

* _mon_

Required Permissions:

* _w_

# health unmute[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

unmute existing health alert mute\(s\)

Example command:

```
ceph health unmute my_code
```

Parameters:

* **code**: \(string\)

Ceph Module:

* _mon_

Required Permissions:

* _w_

# heap[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

show heap usage info \(available only if compiled with tcmalloc\)

Example command:

```
ceph heap dump my_value
```

Parameters:

* **heapcmd**: CephChoices strings=\(dump start\_profiler stop\_profiler release stats\)
* **value**: \(string\)

Ceph Module:

* _mon_

Required Permissions:

* _rw_

# injectargs[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

inject config arguments into monitor

Example command:

```
ceph injectargs --injected_args=string
```

Parameters:

* **injected\_args**: \(can be repeated\)

Ceph Module:

* _mon_

Required Permissions:

* _rw_

# log[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

log supplied text to the monitor log

Example command:

```
ceph log --logtext=string
```

Parameters:

* **logtext**: \(can be repeated\)

Ceph Module:

* _mon_

Required Permissions:

* _rw_

# log last[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

print last few lines of the cluster log

Example command:

```
ceph log last 1 debug *
```

Parameters:

* **num**: CephInt range= `1`
* **level**: CephChoices strings=\(debug info sec warn error\)
* **channel**: CephChoices strings=\(\* cluster audit cephadm\)

Ceph Module:

* _mon_

Required Permissions:

* _r_

# mds add\_data\_pool[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

add data pool \<pool\>

Example command:

```
ceph mds add_data_pool my_pool
```

Parameters:

* **pool**: \(string\)

Ceph Module:

* _mds_

Required Permissions:

* _rw_

Command Flags:

* _obsolete_

# mds cluster\_down[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

take MDS cluster down

Example command:

```
ceph mds cluster_down
```

Ceph Module:

* _mds_

Required Permissions:

* _rw_

Command Flags:

* _obsolete_

# mds cluster\_up[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

bring MDS cluster up

Example command:

```
ceph mds cluster_up
```

Ceph Module:

* _mds_

Required Permissions:

* _rw_

Command Flags:

* _obsolete_

# mds compat rm\_compat[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

remove compatible feature

Example command:

```
ceph mds compat rm_compat 1
```

Parameters:

* **feature**: CephInt range= `0`

Ceph Module:

* _mds_

Required Permissions:

* _rw_

Command Flags:

* _deprecated_

# mds compat rm\_incompat[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

remove incompatible feature

Example command:

```
ceph mds compat rm_incompat 1
```

Parameters:

* **feature**: CephInt range= `0`

Ceph Module:

* _mds_

Required Permissions:

* _rw_

Command Flags:

* _deprecated_

# mds compat show[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

show mds compatibility settings

Example command:

```
ceph mds compat show
```

Ceph Module:

* _mds_

Required Permissions:

* _r_

Command Flags:

* _deprecated_

# mds count\-metadata[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

count MDSs by metadata field property

Example command:

```
ceph mds count-metadata my_property
```

Parameters:

* **property**: \(string\)

Ceph Module:

* _mds_

Required Permissions:

* _r_

# mds deactivate[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

clean up specified MDS rank \(use with set max\_mds to shrink cluster\)

Example command:

```
ceph mds deactivate my_role
```

Parameters:

* **role**: \(string\)

Ceph Module:

* _mds_

Required Permissions:

* _rw_

Command Flags:

* _obsolete_

# mds dump[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

dump legacy MDS cluster info, optionally from epoch

Example command:

```
ceph mds dump 1
```

Parameters:

* **epoch**: CephInt range= `0`

Ceph Module:

* _mds_

Required Permissions:

* _r_

Command Flags:

* _obsolete_

# mds fail[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Mark MDS failed: trigger a failover if a standby is available

Example command:

```
ceph mds fail my_role_or_gid
```

Parameters:

* **role\_or\_gid**: \(string\)

Ceph Module:

* _mds_

Required Permissions:

* _rw_

# mds getmap[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

get MDS map, optionally from epoch

Example command:

```
ceph mds getmap 1
```

Parameters:

* **epoch**: CephInt range= `0`

Ceph Module:

* _mds_

Required Permissions:

* _r_

Command Flags:

* _obsolete_

# mds metadata[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

fetch metadata for mds \<role\>

Example command:

```
ceph mds metadata string
```

Parameters:

* **who**: \(string\)

Ceph Module:

* _mds_

Required Permissions:

* _r_

# mds newfs[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

make new filesystem using pools \<metadata\> and \<data\>

Example command:

```
ceph mds newfs 1 1 --yes_i_really_mean_it
```

Parameters:

* **metadata**: CephInt range= `0`
* **data**: CephInt range= `0`
* **yes\_i\_really\_mean\_it**: CephBool

Ceph Module:

* _mds_

Required Permissions:

* _rw_

Command Flags:

* _obsolete_

# mds ok\-to\-stop[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

check whether stopping the specified MDS would reduce immediate availability

Example command:

```
ceph mds ok-to-stop --ids=string
```

Parameters:

* **ids**: \(can be repeated\)

Ceph Module:

* _mds_

Required Permissions:

* _r_

# mds remove\_data\_pool[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

remove data pool \<pool\>

Example command:

```
ceph mds remove_data_pool my_pool
```

Parameters:

* **pool**: \(string\)

Ceph Module:

* _mds_

Required Permissions:

* _rw_

Command Flags:

* _obsolete_

# mds repaired[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

mark a damaged MDS rank as no longer damaged

Example command:

```
ceph mds repaired my_role
```

Parameters:

* **role**: \(string\)

Ceph Module:

* _mds_

Required Permissions:

* _rw_

# mds rm[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

remove nonactive mds

Example command:

```
ceph mds rm 1
```

Parameters:

* **gid**: CephInt range= `0`

Ceph Module:

* _mds_

Required Permissions:

* _rw_

# mds rm\_data\_pool[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

remove data pool \<pool\>

Example command:

```
ceph mds rm_data_pool my_pool
```

Parameters:

* **pool**: \(string\)

Ceph Module:

* _mds_

Required Permissions:

* _rw_

Command Flags:

* _obsolete_

# mds set[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set mds parameter \<var\> to \<val\>

Example command:

```
ceph mds set max_mds my_val --yes_i_really_mean_it
```

Parameters:

* **var**: CephChoices strings=\(max\_mds max\_file\_size inline\_data allow\_new\_snaps allow\_multimds allow\_multimds\_snaps allow\_dirfrags\)
* **val**: \(string\)
* **yes\_i\_really\_mean\_it**: CephBool

Ceph Module:

* _mds_

Required Permissions:

* _rw_

Command Flags:

* _obsolete_

# mds set\_max\_mds[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set max MDS index

Example command:

```
ceph mds set_max_mds 1
```

Parameters:

* **maxmds**: CephInt range= `0`

Ceph Module:

* _mds_

Required Permissions:

* _rw_

Command Flags:

* _obsolete_

# mds stop[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

stop mds

Example command:

```
ceph mds stop my_role
```

Parameters:

* **role**: \(string\)

Ceph Module:

* _mds_

Required Permissions:

* _rw_

Command Flags:

* _obsolete_

# mds tell[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

send command to particular mds

Example command:

```
ceph mds tell --who=string --args=string
```

Parameters:

* **who**: \(string\)
* **args**: \(can be repeated\)

Ceph Module:

* _mds_

Required Permissions:

* _rw_

Command Flags:

* _obsolete_

# mds versions[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

check running versions of MDSs

Example command:

```
ceph mds versions
```

Ceph Module:

* _mds_

Required Permissions:

* _r_

# mgr count\-metadata[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

count ceph\-mgr daemons by metadata field property

Example command:

```
ceph mgr count-metadata my_property
```

Parameters:

* **property**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

# mgr dump[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

dump the latest MgrMap

Example command:

```
ceph mgr dump 1
```

Parameters:

* **epoch**: CephInt range= `0`

Ceph Module:

* _mgr_

Required Permissions:

* _r_

# mgr fail[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

treat the named manager daemon as failed

Example command:

```
ceph mgr fail string
```

Parameters:

* **who**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

# mgr metadata[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

dump metadata for all daemons or a specific daemon

Example command:

```
ceph mgr metadata string
```

Parameters:

* **who**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _r_

# mgr module disable[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

disable mgr module

Example command:

```
ceph mgr module disable my_module
```

Parameters:

* **module**: \(string\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

# mgr module enable[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

enable mgr module

Example command:

```
ceph mgr module enable my_module --force
```

Parameters:

* **module**: \(string\)
* **force**: CephChoices strings=\(–force\)

Ceph Module:

* _mgr_

Required Permissions:

* _rw_

# mgr module ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

list active mgr modules

Example command:

```
ceph mgr module ls
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

# mgr services[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

list service endpoints provided by mgr modules

Example command:

```
ceph mgr services
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

# mgr stat[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

dump basic info about the mgr cluster state

Example command:

```
ceph mgr stat
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

# mgr versions[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

check running versions of ceph\-mgr daemons

Example command:

```
ceph mgr versions
```

Ceph Module:

* _mgr_

Required Permissions:

* _r_

# mon add disallowed\_leader[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

prevent the named mon from being a leader

Example command:

```
ceph mon add disallowed_leader my_name
```

Parameters:

* **name**: \(string\)

Ceph Module:

* _mon_

Required Permissions:

* _rw_

# mon add name=location,type=CephString,n=N,goodchars=\[A\-Za\-z0\-9\-\_.=\],req=false[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

add new monitor named \<name\> at \<addr\>, possibly with CRUSH location \<location\>

Example command:

```
ceph mon add name=location,type=CephString,n=N,goodchars=[A-Za-z0-9-_.=],req=false my_name 0.0.0.0
```

Parameters:

* **name**: \(string\)
* **addr**: CephIPAddr

Ceph Module:

* _mon_

Required Permissions:

* _rw_

# mon count\-metadata[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

count mons by metadata field property

Example command:

```
ceph mon count-metadata my_property
```

Parameters:

* **property**: \(string\)

Ceph Module:

* _mon_

Required Permissions:

* _r_

# mon dump[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

dump formatted monmap \(optionally from epoch\)

Example command:

```
ceph mon dump 1
```

Parameters:

* **epoch**: CephInt range= `0`

Ceph Module:

* _mon_

Required Permissions:

* _r_

# mon enable\-msgr2[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

enable the msgr2 protocol on port 3300

Example command:

```
ceph mon enable-msgr2
```

Ceph Module:

* _mon_

Required Permissions:

* _rw_

# mon enable\_stretch\_mode[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

enable stretch mode, changing the peering rules and failure handling on all pools with \<tiebreaker\_mon\> as the tiebreaker and setting \<dividing\_bucket\> locations as the units for stretching across

Example command:

```
ceph mon enable_stretch_mode my_tiebreaker_mon my_new_crush_rule my_dividing_bucket
```

Parameters:

* **tiebreaker\_mon**: \(string\)
* **new\_crush\_rule**: \(string\)
* **dividing\_bucket**: \(string\)

Ceph Module:

* _mon_

Required Permissions:

* _rw_

# mon feature ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

list available mon map features to be set/unset

Example command:

```
ceph mon feature ls --with-value
```

Parameters:

* **with\_value**: CephChoices strings=\(–with\-value\)

Ceph Module:

* _mon_

Required Permissions:

* _r_

# mon feature set[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set provided feature on mon map

Example command:

```
ceph mon feature set my_feature_name --yes_i_really_mean_it
```

Parameters:

* **feature\_name**: \(string\)
* **yes\_i\_really\_mean\_it**: CephBool

Ceph Module:

* _mon_

Required Permissions:

* _rw_

# mon getmap[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

get monmap

Example command:

```
ceph mon getmap 1
```

Parameters:

* **epoch**: CephInt range= `0`

Ceph Module:

* _mon_

Required Permissions:

* _r_

# mon metadata[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

fetch metadata for mon \<id\>

Example command:

```
ceph mon metadata my_id
```

Parameters:

* **id**: \(string\)

Ceph Module:

* _mon_

Required Permissions:

* _r_

# mon ok\-to\-add\-offline[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

check whether adding a mon and not starting it would break quorum

Example command:

```
ceph mon ok-to-add-offline
```

Ceph Module:

* _mon_

Required Permissions:

* _r_

# mon ok\-to\-rm[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

check whether removing the specified mon would break quorum

Example command:

```
ceph mon ok-to-rm my_id
```

Parameters:

* **id**: \(string\)

Ceph Module:

* _mon_

Required Permissions:

* _r_

# mon ok\-to\-stop[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

check whether mon\(s\) can be safely stopped without reducing immediate availability

Example command:

```
ceph mon ok-to-stop --ids=string
```

Parameters:

* **ids**: \(can be repeated\)

Ceph Module:

* _mon_

Required Permissions:

* _r_

# mon remove[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

remove monitor named \<name\>

Example command:

```
ceph mon remove my_name
```

Parameters:

* **name**: \(string\)

Ceph Module:

* _mon_

Required Permissions:

* _rw_

Command Flags:

* _deprecated_

# mon rm[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

remove monitor named \<name\>

Example command:

```
ceph mon rm my_name
```

Parameters:

* **name**: \(string\)

Ceph Module:

* _mon_

Required Permissions:

* _rw_

# mon rm disallowed\_leader[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

allow the named mon to be a leader again

Example command:

```
ceph mon rm disallowed_leader my_name
```

Parameters:

* **name**: \(string\)

Ceph Module:

* _mon_

Required Permissions:

* _rw_

# mon scrub[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

scrub the monitor stores

Example command:

```
ceph mon scrub
```

Ceph Module:

* _mon_

Required Permissions:

* _rw_

# mon set election\_strategy[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set the election strategy to use; choices classic, disallow, connectivity

Example command:

```
ceph mon set election_strategy my_strategy
```

Parameters:

* **strategy**: \(string\)

Ceph Module:

* _mon_

Required Permissions:

* _rw_

# mon set\-addrs[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set the addrs \(IPs and ports\) a specific monitor binds to

Example command:

```
ceph mon set-addrs my_name my_addrs
```

Parameters:

* **name**: \(string\)
* **addrs**: \(string\)

Ceph Module:

* _mon_

Required Permissions:

* _rw_

# mon set\-rank[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set the rank for the specified mon

Example command:

```
ceph mon set-rank my_name 1
```

Parameters:

* **name**: \(string\)
* **rank**: CephInt

Ceph Module:

* _mon_

Required Permissions:

* _rw_

# mon set\-weight[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set the weight for the specified mon

Example command:

```
ceph mon set-weight my_name 1
```

Parameters:

* **name**: \(string\)
* **weight**: CephInt range= `0..65535`

Ceph Module:

* _mon_

Required Permissions:

* _rw_

# mon set\_location name=args,type=CephString,n=N,goodchars=\[A\-Za\-z0\-9\-\_.=\][¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

specify location \<args\> for the monitor \<name\>, using CRUSH bucket names

Example command:

```
ceph mon set_location name=args,type=CephString,n=N,goodchars=[A-Za-z0-9-_.=] my_name
```

Parameters:

* **name**: \(string\)

Ceph Module:

* _mon_

Required Permissions:

* _rw_

# mon set\_new\_tiebreaker[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

switch the stretch tiebreaker to be the named mon

Example command:

```
ceph mon set_new_tiebreaker my_name --yes_i_really_mean_it
```

Parameters:

* **name**: \(string\)
* **yes\_i\_really\_mean\_it**: CephBool

Ceph Module:

* _mon_

Required Permissions:

* _rw_

# mon stat[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

summarize monitor status

Example command:

```
ceph mon stat
```

Ceph Module:

* _mon_

Required Permissions:

* _r_

# mon versions[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

check running versions of monitors

Example command:

```
ceph mon versions
```

Ceph Module:

* _mon_

Required Permissions:

* _r_

# mon\_status[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

report status of monitors

Example command:

```
ceph mon_status
```

Ceph Module:

* _mon_

Required Permissions:

* _r_

# node ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

list all nodes in cluster \[type\]

Example command:

```
ceph node ls all
```

Parameters:

* **type**: CephChoices strings=\(all osd mon mds mgr\)

Ceph Module:

* _mon_

Required Permissions:

* _r_

# ops[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

show the ops currently in flight

Example command:

```
ceph ops
```

Ceph Module:

* _mon_

Required Permissions:

* _r_

# osd add\-nodown[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

mark osd\(s\) \<id\> \[\<id\>…\] as nodown, or use \<all|any\> to mark all osds as nodown

Example command:

```
ceph osd add-nodown --ids=string
```

Parameters:

* **ids**: \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

Command Flags:

* _deprecated_

# osd add\-noin[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

mark osd\(s\) \<id\> \[\<id\>…\] as noin, or use \<all|any\> to mark all osds as noin

Example command:

```
ceph osd add-noin --ids=string
```

Parameters:

* **ids**: \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

Command Flags:

* _deprecated_

# osd add\-noout[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

mark osd\(s\) \<id\> \[\<id\>…\] as noout, or use \<all|any\> to mark all osds as noout

Example command:

```
ceph osd add-noout --ids=string
```

Parameters:

* **ids**: \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

Command Flags:

* _deprecated_

# osd add\-noup[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

mark osd\(s\) \<id\> \[\<id\>…\] as noup, or use \<all|any\> to mark all osds as noup

Example command:

```
ceph osd add-noup --ids=string
```

Parameters:

* **ids**: \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

Command Flags:

* _deprecated_

# osd blacklist[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

add \(optionally until \<expire\> seconds from now\) or remove \<addr\> from blacklist

Example command:

```
ceph osd blacklist add entityaddr 0.0
```

Parameters:

* **blacklistop**: CephChoices strings=\(add rm\)
* **addr**: CephEntityAddr
* **expire**: CephFloat range= `0.0`

Ceph Module:

* _osd_

Required Permissions:

* _rw_

Command Flags:

* _deprecated_

# osd blacklist clear[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

clear all blacklisted clients

Example command:

```
ceph osd blacklist clear
```

Ceph Module:

* _osd_

Required Permissions:

* _rw_

Command Flags:

* _deprecated_

# osd blacklist ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

show blacklisted clients

Example command:

```
ceph osd blacklist ls
```

Ceph Module:

* _osd_

Required Permissions:

* _r_

Command Flags:

* _deprecated_

# osd blocked\-by[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

print histogram of which OSDs are blocking their peers

Example command:

```
ceph osd blocked-by
```

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd blocklist[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

add \(optionally until \<expire\> seconds from now\) or remove \<addr\> from blocklist

Example command:

```
ceph osd blocklist add entityaddr 0.0
```

Parameters:

* **blocklistop**: CephChoices strings=\(add rm\)
* **addr**: CephEntityAddr
* **expire**: CephFloat range= `0.0`

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd blocklist clear[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

clear all blocklisted clients

Example command:

```
ceph osd blocklist clear
```

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd blocklist ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

show blocklisted clients

Example command:

```
ceph osd blocklist ls
```

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd count\-metadata[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

count OSDs by metadata field property

Example command:

```
ceph osd count-metadata my_property
```

Parameters:

* **property**: \(string\)

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd create[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

create new osd \(with optional UUID and ID\)

Example command:

```
ceph osd create uuid osd.0
```

Parameters:

* **uuid**: CephUUID
* **id**: CephOsdName

Ceph Module:

* _osd_

Required Permissions:

* _rw_

Command Flags:

* _deprecated_

# osd crush add name=args,type=CephString,n=N,goodchars=\[A\-Za\-z0\-9\-\_.=\][¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

add or update crushmap position and weight for \<name\> with \<weight\> and location \<args\>

Example command:

```
ceph osd crush add name=args,type=CephString,n=N,goodchars=[A-Za-z0-9-_.=] osd.0 0.0
```

Parameters:

* **id**: CephOsdName
* **weight**: CephFloat range= `0.0`

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd crush add\-bucket name=args,type=CephString,n=N,goodchars=\[A\-Za\-z0\-9\-\_.=\],req=false[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

add no\-parent \(probably root\) crush bucket \<name\> of type \<type\> to location \<args\>

Example command:

```
ceph osd crush add-bucket name=args,type=CephString,n=N,goodchars=[A-Za-z0-9-_.=],req=false my_name my_type
```

Parameters:

* **name**: goodchars= `[A-Za-z0-9-_.]`
* **type**: \(string\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd crush class create[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

create crush device class \<class\>

Example command:

```
ceph osd crush class create my_class
```

Parameters:

* **class**: goodchars= `[A-Za-z0-9-_]`

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd crush class ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

list all crush device classes

Example command:

```
ceph osd crush class ls
```

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd crush class ls\-osd[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

list all osds belonging to the specific \<class\>

Example command:

```
ceph osd crush class ls-osd my_class
```

Parameters:

* **class**: goodchars= `[A-Za-z0-9-_]`

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd crush class rename[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

rename crush device class \<srcname\> to \<dstname\>

Example command:

```
ceph osd crush class rename my_srcname my_dstname
```

Parameters:

* **srcname**: goodchars= `[A-Za-z0-9-_]`
* **dstname**: goodchars= `[A-Za-z0-9-_]`

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd crush class rm[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

remove crush device class \<class\>

Example command:

```
ceph osd crush class rm my_class
```

Parameters:

* **class**: goodchars= `[A-Za-z0-9-_]`

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd crush create\-or\-move name=args,type=CephString,n=N,goodchars=\[A\-Za\-z0\-9\-\_.=\][¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

create entry or move existing entry for \<name\> \<weight\> at/to location \<args\>

Example command:

```
ceph osd crush create-or-move name=args,type=CephString,n=N,goodchars=[A-Za-z0-9-_.=] osd.0 0.0
```

Parameters:

* **id**: CephOsdName
* **weight**: CephFloat range= `0.0`

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd crush dump[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

dump crush map

Example command:

```
ceph osd crush dump
```

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd crush get\-device\-class[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

get classes of specified osd\(s\) \<id\> \[\<id\>…\]

Example command:

```
ceph osd crush get-device-class --ids=string
```

Parameters:

* **ids**: \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd crush get\-tunable[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

get crush tunable \<tunable\>

Example command:

```
ceph osd crush get-tunable straw_calc_version
```

Parameters:

* **tunable**: CephChoices strings=\(straw\_calc\_version\)

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd crush link name=args,type=CephString,n=N,goodchars=\[A\-Za\-z0\-9\-\_.=\][¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

link existing entry for \<name\> under location \<args\>

Example command:

```
ceph osd crush link name=args,type=CephString,n=N,goodchars=[A-Za-z0-9-_.=] my_name
```

Parameters:

* **name**: \(string\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd crush ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

list items beneath a node in the CRUSH tree

Example command:

```
ceph osd crush ls my_node
```

Parameters:

* **node**: goodchars= `[A-Za-z0-9-_.]`

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd crush move name=args,type=CephString,n=N,goodchars=\[A\-Za\-z0\-9\-\_.=\][¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

move existing entry for \<name\> to location \<args\>

Example command:

```
ceph osd crush move name=args,type=CephString,n=N,goodchars=[A-Za-z0-9-_.=] my_name
```

Parameters:

* **name**: goodchars= `[A-Za-z0-9-_.]`

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd crush remove[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

remove \<name\> from crush map \(everywhere, or just at \<ancestor\>\)

Example command:

```
ceph osd crush remove my_name my_ancestor
```

Parameters:

* **name**: goodchars= `[A-Za-z0-9-_.]`
* **ancestor**: goodchars= `[A-Za-z0-9-_.]`

Ceph Module:

* _osd_

Required Permissions:

* _rw_

Command Flags:

* _deprecated_

# osd crush rename\-bucket[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

rename bucket \<srcname\> to \<dstname\>

Example command:

```
ceph osd crush rename-bucket my_srcname my_dstname
```

Parameters:

* **srcname**: goodchars= `[A-Za-z0-9-_.]`
* **dstname**: goodchars= `[A-Za-z0-9-_.]`

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd crush reweight[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

change \<name\>’s weight to \<weight\> in crush map

Example command:

```
ceph osd crush reweight my_name 0.0
```

Parameters:

* **name**: goodchars= `[A-Za-z0-9-_.]`
* **weight**: CephFloat range= `0.0`

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd crush reweight\-all[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

recalculate the weights for the tree to ensure they sum correctly

Example command:

```
ceph osd crush reweight-all
```

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd crush reweight\-subtree[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

change all leaf items beneath \<name\> to \<weight\> in crush map

Example command:

```
ceph osd crush reweight-subtree my_name 0.0
```

Parameters:

* **name**: goodchars= `[A-Za-z0-9-_.]`
* **weight**: CephFloat range= `0.0`

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd crush rm[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

remove \<name\> from crush map \(everywhere, or just at \<ancestor\>\)

Example command:

```
ceph osd crush rm my_name my_ancestor
```

Parameters:

* **name**: goodchars= `[A-Za-z0-9-_.]`
* **ancestor**: goodchars= `[A-Za-z0-9-_.]`

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd crush rm\-device\-class[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

remove class of the osd\(s\) \<id\> \[\<id\>…\],or use \<all|any\> to remove all.

Example command:

```
ceph osd crush rm-device-class --ids=string
```

Parameters:

* **ids**: \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd crush rule create\-erasure name=profile,type=CephString,req=false,goodchars=\[A\-Za\-z0\-9\-\_.=\][¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

create crush rule \<name\> for erasure coded pool created with \<profile\> \(default default\)

Example command:

```
ceph osd crush rule create-erasure name=profile,type=CephString,req=false,goodchars=[A-Za-z0-9-_.=] my_name
```

Parameters:

* **name**: goodchars= `[A-Za-z0-9-_.]`

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd crush rule create\-replicated[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

create crush rule \<name\> for replicated pool to start from \<root\>, replicate across buckets of type \<type\>, use devices of type \<class\> \(ssd or hdd\)

Example command:

```
ceph osd crush rule create-replicated --name=string --root=string --type=string --class=string
```

Parameters:

* **name**: goodchars= `[A-Za-z0-9-_.]`
* **root**: goodchars= `[A-Za-z0-9-_.]`
* **type**: goodchars= `[A-Za-z0-9-_.]`
* **class**: goodchars= `[A-Za-z0-9-_.]`

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd crush rule create\-simple[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

create crush rule \<name\> to start from \<root\>, replicate across buckets of type \<type\>, using a choose mode of \<firstn|indep\> \(default firstn; indep best for erasure pools\)

Example command:

```
ceph osd crush rule create-simple --name=string --root=string --type=string --mode=firstn
```

Parameters:

* **name**: goodchars= `[A-Za-z0-9-_.]`
* **root**: goodchars= `[A-Za-z0-9-_.]`
* **type**: goodchars= `[A-Za-z0-9-_.]`
* **mode**: CephChoices strings=\(firstn indep\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd crush rule dump[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

dump crush rule \<name\> \(default all\)

Example command:

```
ceph osd crush rule dump my_name
```

Parameters:

* **name**: goodchars= `[A-Za-z0-9-_.]`

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd crush rule list[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

list crush rules

Example command:

```
ceph osd crush rule list
```

Ceph Module:

* _osd_

Required Permissions:

* _r_

Command Flags:

* _deprecated_

# osd crush rule ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

list crush rules

Example command:

```
ceph osd crush rule ls
```

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd crush rule ls\-by\-class[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

list all crush rules that reference the same \<class\>

Example command:

```
ceph osd crush rule ls-by-class my_class
```

Parameters:

* **class**: goodchars= `[A-Za-z0-9-_.]`

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd crush rule rename[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

rename crush rule \<srcname\> to \<dstname\>

Example command:

```
ceph osd crush rule rename my_srcname my_dstname
```

Parameters:

* **srcname**: goodchars= `[A-Za-z0-9-_.]`
* **dstname**: goodchars= `[A-Za-z0-9-_.]`

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd crush rule rm[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

remove crush rule \<name\>

Example command:

```
ceph osd crush rule rm my_name
```

Parameters:

* **name**: goodchars= `[A-Za-z0-9-_.]`

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd crush set[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set crush map from input file

Example command:

```
ceph osd crush set 1
```

Parameters:

* **prior\_version**: CephInt

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd crush set name=args,type=CephString,n=N,goodchars=\[A\-Za\-z0\-9\-\_.=\][¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

update crushmap position and weight for \<name\> to \<weight\> with location \<args\>

Example command:

```
ceph osd crush set name=args,type=CephString,n=N,goodchars=[A-Za-z0-9-_.=] osd.0 0.0
```

Parameters:

* **id**: CephOsdName
* **weight**: CephFloat range= `0.0`

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd crush set\-all\-straw\-buckets\-to\-straw2[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

convert all CRUSH current straw buckets to use the straw2 algorithm

Example command:

```
ceph osd crush set-all-straw-buckets-to-straw2
```

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd crush set\-device\-class[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set the \<class\> of the osd\(s\) \<id\> \[\<id\>…\],or use \<all|any\> to set all.

Example command:

```
ceph osd crush set-device-class --class=string --ids=string
```

Parameters:

* **class**: \(string\)
* **ids**: \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd crush set\-tunable[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set crush tunable \<tunable\> to \<value\>

Example command:

```
ceph osd crush set-tunable straw_calc_version 1
```

Parameters:

* **tunable**: CephChoices strings=\(straw\_calc\_version\)
* **value**: CephInt

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd crush show\-tunables[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

show current crush tunables

Example command:

```
ceph osd crush show-tunables
```

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd crush swap\-bucket[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

swap existing bucket contents from \(orphan\) bucket \<source\> and \<target\>

Example command:

```
ceph osd crush swap-bucket my_source my_dest --yes_i_really_mean_it
```

Parameters:

* **source**: goodchars= `[A-Za-z0-9-_.]`
* **dest**: goodchars= `[A-Za-z0-9-_.]`
* **yes\_i\_really\_mean\_it**: CephBool

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd crush tree[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

dump crush buckets and items in a tree view

Example command:

```
ceph osd crush tree --show-shadow
```

Parameters:

* **shadow**: CephChoices strings=\(–show\-shadow\)

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd crush tunables[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set crush tunables values to \<profile\>

Example command:

```
ceph osd crush tunables legacy
```

Parameters:

* **profile**: CephChoices strings=\(legacy argonaut bobtail firefly hammer jewel optimal default\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd crush unlink[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

unlink \<name\> from crush map \(everywhere, or just at \<ancestor\>\)

Example command:

```
ceph osd crush unlink my_name my_ancestor
```

Parameters:

* **name**: goodchars= `[A-Za-z0-9-_.]`
* **ancestor**: goodchars= `[A-Za-z0-9-_.]`

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd crush weight\-set create[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

create a weight\-set for a given pool

Example command:

```
ceph osd crush weight-set create poolname flat
```

Parameters:

* **pool**: CephPoolname
* **mode**: CephChoices strings=\(flat positional\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd crush weight\-set create\-compat[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

create a default backward\-compatible weight\-set

Example command:

```
ceph osd crush weight-set create-compat
```

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd crush weight\-set dump[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

dump crush weight sets

Example command:

```
ceph osd crush weight-set dump
```

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd crush weight\-set ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

list crush weight sets

Example command:

```
ceph osd crush weight-set ls
```

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd crush weight\-set reweight[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set weight for an item \(bucket or osd\) in a pool’s weight\-set

Example command:

```
ceph osd crush weight-set reweight --pool=poolname --item=string --weight=0.0
```

Parameters:

* **pool**: CephPoolname
* **item**: \(string\)
* **weight**: CephFloat range= `0.0` \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd crush weight\-set reweight\-compat[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set weight for an item \(bucket or osd\) in the backward\-compatible weight\-set

Example command:

```
ceph osd crush weight-set reweight-compat --item=string --weight=0.0
```

Parameters:

* **item**: \(string\)
* **weight**: CephFloat range= `0.0` \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd crush weight\-set rm[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

remove the weight\-set for a given pool

Example command:

```
ceph osd crush weight-set rm poolname
```

Parameters:

* **pool**: CephPoolname

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd crush weight\-set rm\-compat[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

remove the backward\-compatible weight\-set

Example command:

```
ceph osd crush weight-set rm-compat
```

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd deep\-scrub[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

initiate deep scrub on osd \<who\>, or use \<all|any\> to deep scrub all

Example command:

```
ceph osd deep-scrub string
```

Parameters:

* **who**: \(string\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd destroy[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

mark osd as being destroyed. Keeps the ID intact \(allowing reuse\), but removes cephx keys, config\-key data and lockbox keys, rendering data permanently unreadable.

Example command:

```
ceph osd destroy osd.0 --force --yes_i_really_mean_it
```

Parameters:

* **id**: CephOsdName
* **force**: CephBool
* **yes\_i\_really\_mean\_it**: CephBool

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd df[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

show OSD utilization

Example command:

```
ceph osd df plain class my_filter
```

Parameters:

* **output\_method**: CephChoices strings=\(plain tree\)
* **filter\_by**: CephChoices strings=\(class name\)
* **filter**: \(string\)

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd down[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set osd\(s\) \<id\> \[\<id\>…\] down, or use \<any|all\> to set all osds down

Example command:

```
ceph osd down --ids=string --definitely_dead
```

Parameters:

* **ids**: \(can be repeated\)
* **definitely\_dead**: CephBool

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd dump[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

print summary of OSD map

Example command:

```
ceph osd dump 1
```

Parameters:

* **epoch**: CephInt range= `0`

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd erasure\-code\-profile get[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

get erasure code profile \<name\>

Example command:

```
ceph osd erasure-code-profile get my_name
```

Parameters:

* **name**: goodchars= `[A-Za-z0-9-_.]`

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd erasure\-code\-profile ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

list all erasure code profiles

Example command:

```
ceph osd erasure-code-profile ls
```

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd erasure\-code\-profile rm[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

remove erasure code profile \<name\>

Example command:

```
ceph osd erasure-code-profile rm my_name
```

Parameters:

* **name**: goodchars= `[A-Za-z0-9-_.]`

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd erasure\-code\-profile set[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

create erasure code profile \<name\> with \[\<key\[=value\]\> …\] pairs. Add a –force at the end to override an existing profile \(VERY DANGEROUS\)

Example command:

```
ceph osd erasure-code-profile set --name=string --profile=string --force
```

Parameters:

* **name**: goodchars= `[A-Za-z0-9-_.]`
* **profile**: \(can be repeated\)
* **force**: CephBool

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd find[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

find osd \<id\> in the CRUSH map and show its location

Example command:

```
ceph osd find osd.0
```

Parameters:

* **id**: CephOsdName

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd force\-create\-pg[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

force creation of pg \<pgid\>

Example command:

```
ceph osd force-create-pg 0 --yes_i_really_mean_it
```

Parameters:

* **pgid**: CephPgid
* **yes\_i\_really\_mean\_it**: CephBool

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd force\_healthy\_stretch\_mode[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

force a healthy stretch mode, requiring the full number of CRUSH buckets to peer and letting all non\-tiebreaker monitors be elected leader

Example command:

```
ceph osd force_healthy_stretch_mode --yes_i_really_mean_it
```

Parameters:

* **yes\_i\_really\_mean\_it**: CephBool

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd force\_recovery\_stretch\_mode[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

try and force a recovery stretch mode, increasing the pool size to its non\-failure value if currently degraded and all monitor buckets are up

Example command:

```
ceph osd force_recovery_stretch_mode --yes_i_really_mean_it
```

Parameters:

* **yes\_i\_really\_mean\_it**: CephBool

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd get\-require\-min\-compat\-client[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

get the minimum client version we will maintain compatibility with

Example command:

```
ceph osd get-require-min-compat-client
```

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd getcrushmap[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

get CRUSH map

Example command:

```
ceph osd getcrushmap 1
```

Parameters:

* **epoch**: CephInt range= `0`

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd getmap[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

get OSD map

Example command:

```
ceph osd getmap 1
```

Parameters:

* **epoch**: CephInt range= `0`

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd getmaxosd[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

show largest OSD id

Example command:

```
ceph osd getmaxosd
```

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd in[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set osd\(s\) \<id\> \[\<id\>…\] in, can use \<any|all\> to automatically set all previously out osds in

Example command:

```
ceph osd in --ids=string
```

Parameters:

* **ids**: \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd info[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

print osd’s {id} information \(instead of all osds from map\)

Example command:

```
ceph osd info osd.0
```

Parameters:

* **id**: CephOsdName

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd last\-stat\-seq[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

get the last pg stats sequence number reported for this osd

Example command:

```
ceph osd last-stat-seq osd.0
```

Parameters:

* **id**: CephOsdName

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd lost[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

mark osd as permanently lost. THIS DESTROYS DATA IF NO MORE REPLICAS EXIST, BE CAREFUL

Example command:

```
ceph osd lost osd.0 --yes_i_really_mean_it
```

Parameters:

* **id**: CephOsdName
* **yes\_i\_really\_mean\_it**: CephBool

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

show all OSD ids

Example command:

```
ceph osd ls 1
```

Parameters:

* **epoch**: CephInt range= `0`

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd ls\-tree[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

show OSD ids under bucket \<name\> in the CRUSH map

Example command:

```
ceph osd ls-tree my_name 1
```

Parameters:

* **name**: \(string\)
* **epoch**: CephInt range= `0`

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd lspools[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

list pools

Example command:

```
ceph osd lspools
```

Ceph Module:

* _osd_

Required Permissions:

* _r_

Command Flags:

* _deprecated_

# osd map[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

find pg for \<object\> in \<pool\> with \[namespace\]

Example command:

```
ceph osd map poolname objectname my_nspace
```

Parameters:

* **pool**: CephPoolname
* **object**: CephObjectname
* **nspace**: \(string\)

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd metadata[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

fetch metadata for osd {id} \(default all\)

Example command:

```
ceph osd metadata osd.0
```

Parameters:

* **id**: CephOsdName

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd new[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Create a new OSD. If supplied, the id to be replaced needs to exist and have been previously destroyed. Reads secrets from JSON file via\-i \<file\> \(see man page\).

Example command:

```
ceph osd new uuid osd.0
```

Parameters:

* **uuid**: CephUUID
* **id**: CephOsdName

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd numa\-status[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

show NUMA status of OSDs

Example command:

```
ceph osd numa-status
```

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd ok\-to\-stop[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

check whether osd\(s\) can be safely stopped without reducing immediate data availability

Example command:

```
ceph osd ok-to-stop --ids=string --max=1
```

Parameters:

* **ids**: \(can be repeated\)
* **max**: CephInt

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd out[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set osd\(s\) \<id\> \[\<id\>…\] out, or use \<any|all\> to set all osds out

Example command:

```
ceph osd out --ids=string
```

Parameters:

* **ids**: \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd pause[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

pause osd

Example command:

```
ceph osd pause
```

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd perf[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

print dump of OSD perf summary stats

Example command:

```
ceph osd perf
```

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd pg\-temp[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set pg\_temp mapping pgid:\[\<id\> \[\<id\>…\]\] \(developers only\)

Example command:

```
ceph osd pg-temp --pgid=0 --id=osd.0
```

Parameters:

* **pgid**: CephPgid
* **id**: CephOsdName \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd pg\-upmap[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set pg\_upmap mapping \<pgid\>:\[\<id\> \[\<id\>…\]\] \(developers only\)

Example command:

```
ceph osd pg-upmap --pgid=0 --id=osd.0
```

Parameters:

* **pgid**: CephPgid
* **id**: CephOsdName \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd pg\-upmap\-items[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set pg\_upmap\_items mapping \<pgid\>:{\<id\> to \<id\>, \[…\]} \(developers only\)

Example command:

```
ceph osd pg-upmap-items --pgid=0 --id=osd.0
```

Parameters:

* **pgid**: CephPgid
* **id**: CephOsdName \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd pool application disable[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

disables use of an application \<app\> on pool \<poolname\>

Example command:

```
ceph osd pool application disable poolname my_app --yes_i_really_mean_it
```

Parameters:

* **pool**: CephPoolname
* **app**: \(string\)
* **yes\_i\_really\_mean\_it**: CephBool

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd pool application enable[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

enable use of an application \<app\> \[cephfs,rbd,rgw\] on pool \<poolname\>

Example command:

```
ceph osd pool application enable poolname my_app --yes_i_really_mean_it
```

Parameters:

* **pool**: CephPoolname
* **app**: goodchars= `[A-Za-z0-9-_.]`
* **yes\_i\_really\_mean\_it**: CephBool

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd pool application get[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

get value of key \<key\> of application \<app\> on pool \<poolname\>

Example command:

```
ceph osd pool application get poolname my_app my_key
```

Parameters:

* **pool**: CephPoolname
* **app**: \(string\)
* **key**: \(string\)

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd pool application rm[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

removes application \<app\> metadata key \<key\> on pool \<poolname\>

Example command:

```
ceph osd pool application rm poolname my_app my_key
```

Parameters:

* **pool**: CephPoolname
* **app**: \(string\)
* **key**: \(string\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd pool application set name=value,type=CephString,goodchars=\[A\-Za\-z0\-9\-\_.=\][¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

sets application \<app\> metadata key \<key\> to \<value\> on pool \<poolname\>

Example command:

```
ceph osd pool application set name=value,type=CephString,goodchars=[A-Za-z0-9-_.=] poolname my_app my_key
```

Parameters:

* **pool**: CephPoolname
* **app**: \(string\)
* **key**: goodchars= `[A-Za-z0-9-_.]`

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd pool cancel\-force\-backfill[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

restore normal recovery priority of specified pool \<who\>

Example command:

```
ceph osd pool cancel-force-backfill --who=poolname
```

Parameters:

* **who**: CephPoolname \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd pool cancel\-force\-recovery[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

restore normal recovery priority of specified pool \<who\>

Example command:

```
ceph osd pool cancel-force-recovery --who=poolname
```

Parameters:

* **who**: CephPoolname \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd pool create[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

create pool

Example command:

```
ceph osd pool create --pool=poolname --pg_num=0 --pgp_num=0 --pool_type=replicated --erasure_code_profile=string --rule=string --expected_num_objects=0 --size=0 --pg_num_min=0 --autoscale_mode=on --target_size_bytes=0 --target_size_ratio=0
```

Parameters:

* **pool**: CephPoolname
* **pg\_num**: CephInt range= `0`
* **pgp\_num**: CephInt range= `0`
* **pool\_type**: CephChoices strings=\(replicated erasure\)
* **erasure\_code\_profile**: goodchars= `[A-Za-z0-9-_.]`
* **rule**: \(string\)
* **expected\_num\_objects**: CephInt range= `0`
* **size**: CephInt range= `0`
* **pg\_num\_min**: CephInt range= `0`
* **autoscale\_mode**: CephChoices strings=\(on off warn\)
* **target\_size\_bytes**: CephInt range= `0`
* **target\_size\_ratio**: CephFloat range= `0..1`

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd pool deep\-scrub[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

initiate deep\-scrub on pool \<who\>

Example command:

```
ceph osd pool deep-scrub --who=poolname
```

Parameters:

* **who**: CephPoolname \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd pool delete[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

delete pool

Example command:

```
ceph osd pool delete --pool=poolname --pool2=poolname --yes_i_really_really_mean_it --yes_i_really_really_mean_it_not_faking
```

Parameters:

* **pool**: CephPoolname
* **pool2**: CephPoolname
* **yes\_i\_really\_really\_mean\_it**: CephBool
* **yes\_i\_really\_really\_mean\_it\_not\_faking**: CephBool

Ceph Module:

* _osd_

Required Permissions:

* _rw_

Command Flags:

* _deprecated_

# osd pool force\-backfill[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

force backfill of specified pool \<who\> first

Example command:

```
ceph osd pool force-backfill --who=poolname
```

Parameters:

* **who**: CephPoolname \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd pool force\-recovery[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

force recovery of specified pool \<who\> first

Example command:

```
ceph osd pool force-recovery --who=poolname
```

Parameters:

* **who**: CephPoolname \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd pool get[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

get pool parameter \<var\>

Example command:

```
ceph osd pool get poolname size
```

Parameters:

* **pool**: CephPoolname
* **var**: CephChoices strings=\(size min\_size pg\_num pgp\_num crush\_rule hashpspool nodelete nopgchange nosizechange write\_fadvise\_dontneed noscrub nodeep\-scrub hit\_set\_type hit\_set\_period hit\_set\_count hit\_set\_fpp use\_gmt\_hitset target\_max\_objects target\_max\_bytes cache\_target\_dirty\_ratio cache\_target\_dirty\_high\_ratio cache\_target\_full\_ratio cache\_min\_flush\_age cache\_min\_evict\_age erasure\_code\_profile min\_read\_recency\_for\_promote all min\_write\_recency\_for\_promote fast\_read hit\_set\_grade\_decay\_rate hit\_set\_search\_last\_n scrub\_min\_interval scrub\_max\_interval deep\_scrub\_interval recovery\_priority recovery\_op\_priority scrub\_priority compression\_mode compression\_algorithm compression\_required\_ratio compression\_max\_blob\_size compression\_min\_blob\_size csum\_type csum\_min\_block csum\_max\_block allow\_ec\_overwrites fingerprint\_algorithm pg\_autoscale\_mode pg\_autoscale\_bias pg\_num\_min target\_size\_bytes target\_size\_ratio dedup\_tier dedup\_chunk\_algorithm dedup\_cdc\_chunk\_size\)

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd pool get\-quota[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

obtain object or byte limits for pool

Example command:

```
ceph osd pool get-quota poolname
```

Parameters:

* **pool**: CephPoolname

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd pool ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

list pools

Example command:

```
ceph osd pool ls detail
```

Parameters:

* **detail**: CephChoices strings=\(detail\)

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd pool mksnap[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

make snapshot \<snap\> in \<pool\>

Example command:

```
ceph osd pool mksnap poolname my_snap
```

Parameters:

* **pool**: CephPoolname
* **snap**: \(string\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd pool rename[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

rename \<srcpool\> to \<destpool\>

Example command:

```
ceph osd pool rename poolname poolname
```

Parameters:

* **srcpool**: CephPoolname
* **destpool**: CephPoolname

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd pool repair[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

initiate repair on pool \<who\>

Example command:

```
ceph osd pool repair --who=poolname
```

Parameters:

* **who**: CephPoolname \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd pool rm[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

remove pool

Example command:

```
ceph osd pool rm --pool=poolname --pool2=poolname --yes_i_really_really_mean_it --yes_i_really_really_mean_it_not_faking
```

Parameters:

* **pool**: CephPoolname
* **pool2**: CephPoolname
* **yes\_i\_really\_really\_mean\_it**: CephBool
* **yes\_i\_really\_really\_mean\_it\_not\_faking**: CephBool

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd pool rmsnap[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

remove snapshot \<snap\> from \<pool\>

Example command:

```
ceph osd pool rmsnap poolname my_snap
```

Parameters:

* **pool**: CephPoolname
* **snap**: \(string\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd pool scrub[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

initiate scrub on pool \<who\>

Example command:

```
ceph osd pool scrub --who=poolname
```

Parameters:

* **who**: CephPoolname \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd pool set[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set pool parameter \<var\> to \<val\>

Example command:

```
ceph osd pool set --pool=poolname --var=size --val=string --yes_i_really_mean_it
```

Parameters:

* **pool**: CephPoolname
* **var**: CephChoices strings=\(size min\_size pg\_num pgp\_num pgp\_num\_actual crush\_rule hashpspool nodelete nopgchange nosizechange write\_fadvise\_dontneed noscrub nodeep\-scrub hit\_set\_type hit\_set\_period hit\_set\_count hit\_set\_fpp use\_gmt\_hitset target\_max\_bytes target\_max\_objects cache\_target\_dirty\_ratio cache\_target\_dirty\_high\_ratio cache\_target\_full\_ratio cache\_min\_flush\_age cache\_min\_evict\_age min\_read\_recency\_for\_promote min\_write\_recency\_for\_promote fast\_read hit\_set\_grade\_decay\_rate hit\_set\_search\_last\_n scrub\_min\_interval scrub\_max\_interval deep\_scrub\_interval recovery\_priority recovery\_op\_priority scrub\_priority compression\_mode compression\_algorithm compression\_required\_ratio compression\_max\_blob\_size compression\_min\_blob\_size csum\_type csum\_min\_block csum\_max\_block allow\_ec\_overwrites fingerprint\_algorithm pg\_autoscale\_mode pg\_autoscale\_bias pg\_num\_min target\_size\_bytes target\_size\_ratio dedup\_tier dedup\_chunk\_algorithm dedup\_cdc\_chunk\_size\)
* **val**: \(string\)
* **yes\_i\_really\_mean\_it**: CephBool

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd pool set\-quota[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set object or byte limit on pool

Example command:

```
ceph osd pool set-quota poolname max_objects my_val
```

Parameters:

* **pool**: CephPoolname
* **field**: CephChoices strings=\(max\_objects max\_bytes\)
* **val**: \(string\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd pool stats[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

obtain stats from all pools, or from specified pool

Example command:

```
ceph osd pool stats poolname
```

Parameters:

* **pool\_name**: CephPoolname

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd primary\-affinity[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

adjust osd primary\-affinity from 0.0 \<= \<weight\> \<= 1.0

Example command:

```
ceph osd primary-affinity osd.0 0.0
```

Parameters:

* **id**: CephOsdName
* **weight**: CephFloat range= `0.0..1.0`

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd primary\-temp[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set primary\_temp mapping pgid:\<id\>|\-1 \(developers only\)

Example command:

```
ceph osd primary-temp 0 osd.0
```

Parameters:

* **pgid**: CephPgid
* **id**: CephOsdName

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd purge[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

purge all osd data from the monitors including the OSD id and CRUSH position

Example command:

```
ceph osd purge osd.0 --force --yes_i_really_mean_it
```

Parameters:

* **id**: CephOsdName
* **force**: CephBool
* **yes\_i\_really\_mean\_it**: CephBool

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd purge\-new[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

purge all traces of an OSD that was partially created but never started

Example command:

```
ceph osd purge-new osd.0 --yes_i_really_mean_it
```

Parameters:

* **id**: CephOsdName
* **yes\_i\_really\_mean\_it**: CephBool

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd repair[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

initiate repair on osd \<who\>, or use \<all|any\> to repair all

Example command:

```
ceph osd repair string
```

Parameters:

* **who**: \(string\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd require\-osd\-release[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set the minimum allowed OSD release to participate in the cluster

Example command:

```
ceph osd require-osd-release luminous --yes_i_really_mean_it
```

Parameters:

* **release**: CephChoices strings=\(luminous mimic nautilus octopus pacific\)
* **yes\_i\_really\_mean\_it**: CephBool

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd reweight[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

reweight osd to 0.0 \< \<weight\> \< 1.0

Example command:

```
ceph osd reweight osd.0 0.0
```

Parameters:

* **id**: CephOsdName
* **weight**: CephFloat range= `0.0..1.0`

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd reweight\-by\-pg[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

reweight OSDs by PG distribution \[overload\-percentage\-for\- consideration, default 120\]

Example command:

```
ceph osd reweight-by-pg --oload=1 --max_change=0.0 --max_osds=1 --pools=poolname
```

Parameters:

* **oload**: CephInt
* **max\_change**: CephFloat
* **max\_osds**: CephInt
* **pools**: CephPoolname \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd reweight\-by\-utilization[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

reweight OSDs by utilization \[overload\-percentage\-for\-consideration, default 120\]

Example command:

```
ceph osd reweight-by-utilization --oload=1 --max_change=0.0 --max_osds=1 --no_increasing=--no-increasing
```

Parameters:

* **oload**: CephInt
* **max\_change**: CephFloat
* **max\_osds**: CephInt
* **no\_increasing**: CephChoices strings=\(–no\-increasing\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd reweightn[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

reweight osds with {\<id\>: \<weight\>,…}

Example command:

```
ceph osd reweightn my_weights
```

Parameters:

* **weights**: \(string\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd rm[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

remove osd\(s\) \<id\> \[\<id\>…\], or use \<any|all\> to remove all osds

Example command:

```
ceph osd rm --ids=string
```

Parameters:

* **ids**: \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

Command Flags:

* _deprecated_

# osd rm\-nodown[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

allow osd\(s\) \<id\> \[\<id\>…\] to be marked down \(if they are currently marked as nodown\), can use \<all|any\> to automatically filter out all nodown osds

Example command:

```
ceph osd rm-nodown --ids=string
```

Parameters:

* **ids**: \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

Command Flags:

* _deprecated_

# osd rm\-noin[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

allow osd\(s\) \<id\> \[\<id\>…\] to be marked in \(if they are currently marked as noin\), can use \<all|any\> to automatically filter out all noin osds

Example command:

```
ceph osd rm-noin --ids=string
```

Parameters:

* **ids**: \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

Command Flags:

* _deprecated_

# osd rm\-noout[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

allow osd\(s\) \<id\> \[\<id\>…\] to be marked out \(if they are currently marked as noout\), can use \<all|any\> to automatically filter out all noout osds

Example command:

```
ceph osd rm-noout --ids=string
```

Parameters:

* **ids**: \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

Command Flags:

* _deprecated_

# osd rm\-noup[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

allow osd\(s\) \<id\> \[\<id\>…\] to be marked up \(if they are currently marked as noup\), can use \<all|any\> to automatically filter out all noup osds

Example command:

```
ceph osd rm-noup --ids=string
```

Parameters:

* **ids**: \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

Command Flags:

* _deprecated_

# osd rm\-pg\-upmap[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

clear pg\_upmap mapping for \<pgid\> \(developers only\)

Example command:

```
ceph osd rm-pg-upmap 0
```

Parameters:

* **pgid**: CephPgid

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd rm\-pg\-upmap\-items[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

clear pg\_upmap\_items mapping for \<pgid\> \(developers only\)

Example command:

```
ceph osd rm-pg-upmap-items 0
```

Parameters:

* **pgid**: CephPgid

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd safe\-to\-destroy[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

check whether osd\(s\) can be safely destroyed without reducing data durability

Example command:

```
ceph osd safe-to-destroy --ids=string
```

Parameters:

* **ids**: \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd scrub[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

initiate scrub on osd \<who\>, or use \<all|any\> to scrub all

Example command:

```
ceph osd scrub string
```

Parameters:

* **who**: \(string\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd set[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set \<key\>

Example command:

```
ceph osd set full --yes_i_really_mean_it
```

Parameters:

* **key**: CephChoices strings=\(full pause noup nodown noout noin nobackfill norebalance norecover noscrub nodeep\-scrub notieragent nosnaptrim pglog\_hardlimit\)
* **yes\_i\_really\_mean\_it**: CephBool

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd set\-backfillfull\-ratio[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set usage ratio at which OSDs are marked too full to backfill

Example command:

```
ceph osd set-backfillfull-ratio 0.0
```

Parameters:

* **ratio**: CephFloat range= `0.0..1.0`

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd set\-full\-ratio[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set usage ratio at which OSDs are marked full

Example command:

```
ceph osd set-full-ratio 0.0
```

Parameters:

* **ratio**: CephFloat range= `0.0..1.0`

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd set\-group[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set \<flags\> for batch osds or crush nodes, \<flags\> must be a comma\- separated subset of {noup,nodown,noin,noout}

Example command:

```
ceph osd set-group --flags=string --who=string
```

Parameters:

* **flags**: \(string\)
* **who**: \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd set\-nearfull\-ratio[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set usage ratio at which OSDs are marked near\-full

Example command:

```
ceph osd set-nearfull-ratio 0.0
```

Parameters:

* **ratio**: CephFloat range= `0.0..1.0`

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd set\-require\-min\-compat\-client[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set the minimum client version we will maintain compatibility with

Example command:

```
ceph osd set-require-min-compat-client my_version --yes_i_really_mean_it
```

Parameters:

* **version**: \(string\)
* **yes\_i\_really\_mean\_it**: CephBool

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd setcrushmap[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set crush map from input file

Example command:

```
ceph osd setcrushmap 1
```

Parameters:

* **prior\_version**: CephInt

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd setmaxosd[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set new maximum osd value

Example command:

```
ceph osd setmaxosd 1
```

Parameters:

* **newmax**: CephInt range= `0`

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd stat[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

print summary of OSD map

Example command:

```
ceph osd stat
```

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd stop[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

stop the corresponding osd daemons and mark them as down

Example command:

```
ceph osd stop --ids=string
```

Parameters:

* **ids**: \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd test\-reweight\-by\-pg[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

dry run of reweight OSDs by PG distribution \[overload\-percentage\-for\- consideration, default 120\]

Example command:

```
ceph osd test-reweight-by-pg --oload=1 --max_change=0.0 --max_osds=1 --pools=poolname
```

Parameters:

* **oload**: CephInt
* **max\_change**: CephFloat
* **max\_osds**: CephInt
* **pools**: CephPoolname \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd test\-reweight\-by\-utilization[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

dry run of reweight OSDs by utilization \[overload\-percentage\-for\- consideration, default 120\]

Example command:

```
ceph osd test-reweight-by-utilization --oload=1 --max_change=0.0 --max_osds=1 --no_increasing
```

Parameters:

* **oload**: CephInt
* **max\_change**: CephFloat
* **max\_osds**: CephInt
* **no\_increasing**: CephBool

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd tier add[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

add the tier \<tierpool\> \(the second one\) to base pool \<pool\> \(the first one\)

Example command:

```
ceph osd tier add poolname poolname --force-nonempty
```

Parameters:

* **pool**: CephPoolname
* **tierpool**: CephPoolname
* **force\_nonempty**: CephChoices strings=\(–force\-nonempty\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd tier add\-cache[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

add a cache \<tierpool\> \(the second one\) of size \<size\> to existing pool \<pool\> \(the first one\)

Example command:

```
ceph osd tier add-cache poolname poolname 1
```

Parameters:

* **pool**: CephPoolname
* **tierpool**: CephPoolname
* **size**: CephInt range= `0`

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd tier cache\-mode[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

specify the caching mode for cache tier \<pool\>

Example command:

```
ceph osd tier cache-mode poolname writeback --yes_i_really_mean_it
```

Parameters:

* **pool**: CephPoolname
* **mode**: CephChoices strings=\(writeback readproxy readonly none\)
* **yes\_i\_really\_mean\_it**: CephBool

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd tier remove[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

remove the tier \<tierpool\> \(the second one\) from base pool \<pool\> \(the first one\)

Example command:

```
ceph osd tier remove poolname poolname
```

Parameters:

* **pool**: CephPoolname
* **tierpool**: CephPoolname

Ceph Module:

* _osd_

Required Permissions:

* _rw_

Command Flags:

* _deprecated_

# osd tier remove\-overlay[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

remove the overlay pool for base pool \<pool\>

Example command:

```
ceph osd tier remove-overlay poolname
```

Parameters:

* **pool**: CephPoolname

Ceph Module:

* _osd_

Required Permissions:

* _rw_

Command Flags:

* _deprecated_

# osd tier rm[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

remove the tier \<tierpool\> \(the second one\) from base pool \<pool\> \(the first one\)

Example command:

```
ceph osd tier rm poolname poolname
```

Parameters:

* **pool**: CephPoolname
* **tierpool**: CephPoolname

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd tier rm\-overlay[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

remove the overlay pool for base pool \<pool\>

Example command:

```
ceph osd tier rm-overlay poolname
```

Parameters:

* **pool**: CephPoolname

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd tier set\-overlay[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

set the overlay pool for base pool \<pool\> to be \<overlaypool\>

Example command:

```
ceph osd tier set-overlay poolname poolname
```

Parameters:

* **pool**: CephPoolname
* **overlaypool**: CephPoolname

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd tree[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

print OSD tree

Example command:

```
ceph osd tree --epoch=0 --states=up
```

Parameters:

* **epoch**: CephInt range= `0`
* **states**: CephChoices strings=\(up down in out destroyed\) \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd tree\-from[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

print OSD tree in bucket

Example command:

```
ceph osd tree-from --bucket=string --epoch=0 --states=up
```

Parameters:

* **bucket**: \(string\)
* **epoch**: CephInt range= `0`
* **states**: CephChoices strings=\(up down in out destroyed\) \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd unpause[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

unpause osd

Example command:

```
ceph osd unpause
```

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd unset[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

unset \<key\>

Example command:

```
ceph osd unset full
```

Parameters:

* **key**: CephChoices strings=\(full pause noup nodown noout noin nobackfill norebalance norecover noscrub nodeep\-scrub notieragent nosnaptrim\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd unset\-group[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

unset \<flags\> for batch osds or crush nodes, \<flags\> must be a comma\- separated subset of {noup,nodown,noin,noout}

Example command:

```
ceph osd unset-group --flags=string --who=string
```

Parameters:

* **flags**: \(string\)
* **who**: \(can be repeated\)

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# osd utilization[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

get basic pg distribution stats

Example command:

```
ceph osd utilization
```

Ceph Module:

* _osd_

Required Permissions:

* _r_

# osd versions[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

check running versions of OSDs

Example command:

```
ceph osd versions
```

Ceph Module:

* _osd_

Required Permissions:

* _r_

# pg cancel\-force\-backfill[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

restore normal backfill priority of \<pgid\>

Example command:

```
ceph pg cancel-force-backfill --pgid=0
```

Parameters:

* **pgid**: CephPgid \(can be repeated\)

Ceph Module:

* _pg_

Required Permissions:

* _rw_

# pg cancel\-force\-recovery[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

restore normal recovery priority of \<pgid\>

Example command:

```
ceph pg cancel-force-recovery --pgid=0
```

Parameters:

* **pgid**: CephPgid \(can be repeated\)

Ceph Module:

* _pg_

Required Permissions:

* _rw_

# pg debug[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

show debug info about pgs

Example command:

```
ceph pg debug unfound_objects_exist
```

Parameters:

* **debugop**: CephChoices strings=\(unfound\_objects\_exist degraded\_pgs\_exist\)

Ceph Module:

* _pg_

Required Permissions:

* _r_

# pg deep\-scrub[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

start deep\-scrub on \<pgid\>

Example command:

```
ceph pg deep-scrub 0
```

Parameters:

* **pgid**: CephPgid

Ceph Module:

* _pg_

Required Permissions:

* _rw_

# pg dump[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

show human\-readable versions of pg map \(only ‘all’ valid with plain\)

Example command:

```
ceph pg dump --dumpcontents=all
```

Parameters:

* **dumpcontents**: CephChoices strings=\(all summary sum delta pools osds pgs pgs\_brief\) \(can be repeated\)

Ceph Module:

* _pg_

Required Permissions:

* _r_

# pg dump\_json[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

show human\-readable version of pg map in json only

Example command:

```
ceph pg dump_json --dumpcontents=all
```

Parameters:

* **dumpcontents**: CephChoices strings=\(all summary sum pools osds pgs\) \(can be repeated\)

Ceph Module:

* _pg_

Required Permissions:

* _r_

# pg dump\_pools\_json[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

show pg pools info in json only

Example command:

```
ceph pg dump_pools_json
```

Ceph Module:

* _pg_

Required Permissions:

* _r_

# pg dump\_stuck[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

show information about stuck pgs

Example command:

```
ceph pg dump_stuck --stuckops=inactive --threshold=1
```

Parameters:

* **stuckops**: CephChoices strings=\(inactive unclean stale undersized degraded\) \(can be repeated\)
* **threshold**: CephInt

Ceph Module:

* _pg_

Required Permissions:

* _r_

# pg force\-backfill[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

force backfill of \<pgid\> first

Example command:

```
ceph pg force-backfill --pgid=0
```

Parameters:

* **pgid**: CephPgid \(can be repeated\)

Ceph Module:

* _pg_

Required Permissions:

* _rw_

# pg force\-recovery[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

force recovery of \<pgid\> first

Example command:

```
ceph pg force-recovery --pgid=0
```

Parameters:

* **pgid**: CephPgid \(can be repeated\)

Ceph Module:

* _pg_

Required Permissions:

* _rw_

# pg getmap[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

get binary pg map to \-o/stdout

Example command:

```
ceph pg getmap
```

Ceph Module:

* _pg_

Required Permissions:

* _r_

# pg ls[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

list pg with specific pool, osd, state

Example command:

```
ceph pg ls --pool=1 --states=string
```

Parameters:

* **pool**: CephInt
* **states**: \(can be repeated\)

Ceph Module:

* _pg_

Required Permissions:

* _r_

# pg ls\-by\-osd[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

list pg on osd \[osd\]

Example command:

```
ceph pg ls-by-osd --osd=osd.0 --pool=1 --states=string
```

Parameters:

* **osd**: CephOsdName
* **pool**: CephInt
* **states**: \(can be repeated\)

Ceph Module:

* _pg_

Required Permissions:

* _r_

# pg ls\-by\-pool[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

list pg with pool = \[poolname\]

Example command:

```
ceph pg ls-by-pool --poolstr=string --states=string
```

Parameters:

* **poolstr**: \(string\)
* **states**: \(can be repeated\)

Ceph Module:

* _pg_

Required Permissions:

* _r_

# pg ls\-by\-primary[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

list pg with primary = \[osd\]

Example command:

```
ceph pg ls-by-primary --osd=osd.0 --pool=1 --states=string
```

Parameters:

* **osd**: CephOsdName
* **pool**: CephInt
* **states**: \(can be repeated\)

Ceph Module:

* _pg_

Required Permissions:

* _r_

# pg map[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

show mapping of pg to osds

Example command:

```
ceph pg map 0
```

Parameters:

* **pgid**: CephPgid

Ceph Module:

* _pg_

Required Permissions:

* _r_

# pg repair[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

start repair on \<pgid\>

Example command:

```
ceph pg repair 0
```

Parameters:

* **pgid**: CephPgid

Ceph Module:

* _pg_

Required Permissions:

* _rw_

# pg repeer[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

force a PG to repeer

Example command:

```
ceph pg repeer 0
```

Parameters:

* **pgid**: CephPgid

Ceph Module:

* _osd_

Required Permissions:

* _rw_

# pg scrub[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

start scrub on \<pgid\>

Example command:

```
ceph pg scrub 0
```

Parameters:

* **pgid**: CephPgid

Ceph Module:

* _pg_

Required Permissions:

* _rw_

# pg stat[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

show placement group status.

Example command:

```
ceph pg stat
```

Ceph Module:

* _pg_

Required Permissions:

* _r_

# quorum enter[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

force monitor back into quorum

Example command:

```
ceph quorum enter
```

Ceph Module:

* _mon_

Required Permissions:

* _rw_

# quorum exit[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

force monitor out of the quorum

Example command:

```
ceph quorum exit
```

Ceph Module:

* _mon_

Required Permissions:

* _rw_

# quorum\_status[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

report status of monitor quorum

Example command:

```
ceph quorum_status
```

Ceph Module:

* _mon_

Required Permissions:

* _r_

# report[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

report full status of cluster, optional title tag strings

Example command:

```
ceph report --tags=string
```

Parameters:

* **tags**: \(can be repeated\)

Ceph Module:

* _mon_

Required Permissions:

* _r_

# scrub[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

scrub the monitor stores

Example command:

```
ceph scrub
```

Ceph Module:

* _mon_

Required Permissions:

* _rw_

Command Flags:

* _obsolete_

# service dump[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

dump service map

Example command:

```
ceph service dump
```

Ceph Module:

* _service_

Required Permissions:

* _r_

# service status[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

dump service state

Example command:

```
ceph service status
```

Ceph Module:

* _service_

Required Permissions:

* _r_

# sessions[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

list existing sessions

Example command:

```
ceph sessions
```

Ceph Module:

* _mon_

Required Permissions:

* _r_

# smart[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

Query health metrics for underlying device

Example command:

```
ceph smart my_devid
```

Parameters:

* **devid**: \(string\)

Ceph Module:

* _mon_

Required Permissions:

* _rw_

# status[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

show cluster status

Example command:

```
ceph status
```

Ceph Module:

* _mon_

Required Permissions:

* _r_

# sync\_force[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

force sync of and clear monitor store

Example command:

```
ceph sync_force --yes-i-really-mean-it
```

Parameters:

* **validate**: CephChoices strings=\(–yes\-i\-really\-mean\-it\)

Ceph Module:

* _mon_

Required Permissions:

* _rw_

# tell[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

send a command to a specific daemon

Example command:

```
ceph tell --target=name --args=string
```

Parameters:

* **target**: CephName
* **args**: \(can be repeated\)

Ceph Module:

* _mon_

Required Permissions:

* _rw_

# time\-sync\-status[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

show time sync status

Example command:

```
ceph time-sync-status
```

Ceph Module:

* _mon_

Required Permissions:

* _r_

# version[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

show mon daemon version

Example command:

```
ceph version
```

Ceph Module:

* _mon_

Required Permissions:

* _r_

# versions[¶](https://docs.ceph.com/en/pacific/api/mon_command_api/ "Permalink to this headline")

check running versions of ceph daemons

Example command:

```
ceph versions
```

Ceph Module:

* _mon_

Required Permissions:

* _r_

 
 
