# 273: ElasticSearch Sync Module

**クリップソース:** [273: ElasticSearch Sync Module — Ceph Documentation](https://docs.ceph.com/en/pacific/radosgw/elastic-sync-module/)

# ElasticSearch Sync Module[¶](https://docs.ceph.com/en/pacific/radosgw/elastic-sync-module/ "Permalink to this headline")

Kraken バージョンの新機能。

注：2020年5月31日現在、Elasticsearch 6以下のみ対応しています。ElasticSearch 7には対応していません。

この同期モジュールは、他のゾーンからElasticSearchにメタデータを書き込みます。 luminousの時点で、これはElasticSearchに保存するデータフィールドのjsonです。

```
{
     "_index" : "rgw-gold-ee5863d6",
     "_type" : "object",
     "_id" : "34137443-8592-48d9-8ca7-160255d52ade.34137.1:object1:null",
     "_score" : 1.0,
     "_source" : {
       "bucket" : "testbucket123",
       "name" : "object1",
       "instance" : "null",
       "versioned_epoch" : 0,
       "owner" : {
         "id" : "user1",
         "display_name" : "user1"
       },
       "permissions" : [
         "user1"
       ],
       "meta" : {
         "size" : 712354,
         "mtime" : "2017-05-04T12:54:16.462Z",
         "etag" : "7ac66c0f148de9519b8bd264312c4d64"
       }
     }
   }
```

## ElasticSearch tier type configurables[¶](https://docs.ceph.com/en/pacific/radosgw/elastic-sync-module/ "Permalink to this headline")

* endpoint

アクセスするElasticsearchサーバのエンドポイントを指定する

* num\_shards \(integer\)

データシンクの初期化時にElasticsearchに設定するシャード数。init後は変更できないことに注意すること。変更すると、Elasticsearchインデックスの再構築とデータシンク処理の再initが必要になる

* num\_replicas \(integer\)

データシンクの初期化時にElasticsearchに設定するレプリカ数

* explicit\_custom\_meta \(true | false\)

ユーザのカスタムメタデータをすべてインデックス化するか、あるいはどのカスタムメタデータのエントリをインデックス化するかをユーザが（バケットレベルで）設定する必要があるかどうか。デフォルトは false

* index\_buckets\_list \(comma separated list of strings\)

空の場合、すべてのバケットがインデックス化される。空でない場合は、指定したバケットのみがインデックス化される。バケットのプレフィックス（例：foo\*）やサフィックス（例：\*bar）を指定することが可能

* approved\_owners\_list \(comma separated list of strings\)

空の場合、すべての所有者のバケットがインデックス化され（他の制限に従う）、空でない場合、指定した所有者が所有するバケットのみがインデックス化される。サフィックスとプリフィックスを指定することもできる

* override\_index\_path \(string\)

空でない場合、この文字列がElasticsearchインデックスパスとして使用される。空の場合は、インデックスパスは同期の初期化時に決定され、生成される

## End user metadata queries[¶](https://docs.ceph.com/en/pacific/radosgw/elastic-sync-module/ "Permalink to this headline")

Luminous バージョンの新機能です。

ElasticSearchクラスタはオブジェクトのメタデータを保存するため、ElasticSearchエンドポイントを一般に公開せず、クラスタ管理者のみがアクセスできるようにすることが重要です。メタデータのクエリをエンドユーザに公開する場合、ユーザは自分のメタデータのみを照会し、他のユーザのメタデータは照会しないようにしたいため、RGWのような方法でElasticSearchクラスタがユーザを認証しなければならず、問題が発生する可能性があります。

Luminous では、メタデータマスターゾーンの RGW がエンドユーザーのリクエストに対応できるようになりました。これにより、elasticsearchのエンドポイントを公開することなく、また、RGW自身がエンドユーザのリクエストを認証できるため、認証・認可の問題も解決されます。この目的のために、RGWはバケットAPIにelasticsearchリクエストを処理するための新しいクエリを導入します。これらのリクエストはすべて、メタデータマスターゾーンに送信される必要があります。

### Syntax[¶](https://docs.ceph.com/en/pacific/radosgw/elastic-sync-module/ "Permalink to this headline")

#### Get an elasticsearch query[¶](https://docs.ceph.com/en/pacific/radosgw/elastic-sync-module/ "Permalink to this headline")

```
GET /{bucket}?query={query-expr}
```

リクエストのパラメータ：

* max\-keys: 返送する最大エントリ数
* marker: ページめくりマーカ

expression:=\[\(\]\<arg\>\<op\>\<value\>\[\)\]\[\<and|or\>...\]

opは以下のいずれか：\<, \<=, ==, \>=, \>

For example

```
GET /?query=name==foo
```

上記は、ユーザがread権限を持ち、かつ 'foo' という名前のインデックスを持つキーをすべて返します。

出力は、S3のlist bucketsレスポンスと同様のXMLのキーリストとなります。

#### Configure custom metadata fields[¶](https://docs.ceph.com/en/pacific/radosgw/elastic-sync-module/ "Permalink to this headline")

どのカスタムメタデータの項目を（指定したバケットの下で）インデックス化するか、またこれらのキーの型が何かを定義します。明示的なカスタムメタデータのインデックス作成（explicit\_custom\_meta）が設定されている場合、rgw が指定されたカスタムメタデータ値をインデックス化するのに必要です。明示的なカスタムメタデータのインデックス作成（explicit\_custom\_meta）が設定されていない場合は、インデックス化するメタデータキーが string 以外である場合に必要です。

```
POST /{bucket}?mdsearch
x-amz-meta-search: <key [; type]> [, ...]
```

複数のメタデータフィールドはカンマで区切る必要があり、フィールドのタイプは ; で強制することができます。現在許可されている型は、string\(デフォルト\)、integer、dateです。

例えば、カスタムオブジェクトのメタデータ x\-amz\-meta\-year を int、 x\-amz\-meta\-date を date 型、 x\-amz\-meta\-title を string でインデックスしたい場合、次のようにします：

```
POST /mybooks?mdsearch
x-amz-meta-search: x-amz-meta-year;int, x-amz-meta-release-date;date, x-amz-meta-title;string
```

#### Delete custom metadata configuration[¶](https://docs.ceph.com/en/pacific/radosgw/elastic-sync-module/ "Permalink to this headline")

カスタムメターデータバケット設定を削除します：

```
DELETE /<bucket>?mdsearch
```

#### Get custom metadata configuration[¶](https://docs.ceph.com/en/pacific/radosgw/elastic-sync-module/ "Permalink to this headline")

カスタムメターデータバケット設定を取得します：

```
GET /<bucket>?mdsearch
```
