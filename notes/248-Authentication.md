# 248: Authentication

 # Authentication[¶](https://docs.ceph.com/en/pacific/radosgw/swift/auth/ "Permalink to this headline")

Swift API requests that require authentication must contain an`X-Storage-Token` authentication token in the request header. The token may be retrieved from RADOS Gateway, or from another authenticator. To obtain a token from RADOS Gateway, you must create a user. For example:

```
sudo radosgw-admin user create --subuser="{username}:{subusername}" --uid="{username}"
--display-name="{Display Name}" --key-type=swift --secret="{password}" --access=full
```

For details on RADOS Gateway administration, see [radosgw\-admin](https://docs.ceph.com/en/pacific/radosgw/swift/auth/).

Note:

For those used to the Swift API this is implementing the Swift auth v1.0 API, as such{username} above is generally equivalent to a Swift account and {subusername}is a user under that account.

## Auth Get[¶](https://docs.ceph.com/en/pacific/radosgw/swift/auth/ "Permalink to this headline")

To authenticate a user, make a request containing an `X-Auth-User` and a`X-Auth-Key` in the header.

### Syntax[¶](https://docs.ceph.com/en/pacific/radosgw/swift/auth/ "Permalink to this headline")

```
GET /auth HTTP/1.1
Host: swift.radosgwhost.com
X-Auth-User: johndoe
X-Auth-Key: R7UUOLFDI2ZI9PRCQ53K
```

### Request Headers[¶](https://docs.ceph.com/en/pacific/radosgw/swift/auth/ "Permalink to this headline")

`X-Auth-User`

DescriptionThe key RADOS GW username to authenticate.

TypeString

RequiredYes

`X-Auth-Key`

DescriptionThe key associated to a RADOS GW username.

TypeString

RequiredYes

### Response Headers[¶](https://docs.ceph.com/en/pacific/radosgw/swift/auth/ "Permalink to this headline")

The response from the server should include an `X-Auth-Token` value. The response may also contain a `X-Storage-Url` that provides the`{api version}/{account}` prefix that is specified in other requests throughout the API documentation.

`X-Storage-Token`

DescriptionThe authorization token for the `X-Auth-User` specified in the request.

TypeString

`X-Storage-Url`

DescriptionThe URL and `{api version}/{account}` path for the user.

TypeString

A typical response looks like this:

```
    HTTP/1.1 204 No Content
    Date: Mon, 16 Jul 2012 11:05:33 GMT
    Server: swift
    X-Storage-Url: https://swift.radosgwhost.com/v1/ACCT-12345
    X-Auth-Token: UOlCCC8TahFKlWuv9DB09TWHF0nDjpPElha0kAa
    Content-Length: 0
Content-Type: text/plain; charset=UTF-8
```
