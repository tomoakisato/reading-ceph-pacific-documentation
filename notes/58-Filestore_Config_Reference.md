# 58: Filestore Config Reference

**クリップソース:** [58: Filestore Config Reference — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/configuration/filestore-config-ref/)

# Filestore Config Reference[¶](https://docs.ceph.com/en/pacific/rados/configuration/filestore-config-ref/ "Permalink to this headline")

ファイルストアのバックエンドは、新しいOSDを作成する際のデフォルトではなくなりましたが、ファイルストアOSDはまだサポートされています。

**filestore debug omap check**

Description

同期時のデバッグチェック。高価。デバッグ用のみ

Type

_Boolean_

Required

_No_

Default

_false_

## Extended Attributes[¶](https://docs.ceph.com/en/pacific/rados/configuration/filestore-config-ref/ "Permalink to this headline")

拡張属性（XATTR）は、ファイルストアOSDにとって重要です。ファイルシステムによっては、XATTRに格納できるバイト数に制限があるものがあります。さらに、場合によっては、ファイルシステムがXATTRを格納する代替方法と比較して高速でないことがあります。以下の設定は、基礎となるファイルシステムに外在するXATTRの格納方法を使用することで、パフォーマンスを向上させるのに役立つ場合があります。

Ceph XATTRは、基礎となるファイルシステムが提供するXATTRを使用して、inline xattrとして格納されます\(サイズ制限が課されていない場合\)。サイズ制限がある場合 \(たとえばext4で合計4KB\)、filestore\_max\_inline\_xattr\_sizeまたはfilestore\_max\_inline\_xattrsのいずれかの閾値に達すると、一部のCeph XATTRはキー/バリュー・データベースに格納されます。

**filestore\_max\_inline\_xattr\_size**

Description

ファイルシステム（XFS, Btrfs, EXT4など）に格納されるXATTRの1オブジェクトあたりの最大サイズ。ファイルシステムが扱えるサイズより大きくてはならない。デフォルトの値0は、基礎となるファイルシステムに固有の値を使用することを意味する

Type

_Unsigned 32\-bit Integer_

Required

_No_

Default

_0_

**filestore\_max\_inline\_xattr\_size\_xfs**

Description

XFSファイルシステムに格納されるXATTRの最大サイズ。filestore\_max\_inline\_xattr\_size == 0の場合のみ使用される

Type

_Unsigned 32\-bit Intege_r

Required

_No_

Default

_65536_

**filestore\_max\_inline\_xattr\_size\_btrfs**

Description

Btrfs ファイルシステムに保存される XATTR の最大サイズ。filestore\_max\_inline\_xattr\_size == 0 の場合のみ使用される

Type

_Unsigned 32\-bit Integer_

Required

No

Default

2048

**filestore\_max\_inline\_xattr\_size\_other**

Description

他のファイルシステムに格納されているXATTRの最大サイズ。filestore\_max\_inline\_xattr\_size == 0 の場合のみ使用される

Type

Unsigned 32\-bit Integer

Required

No

Default

512

**filestore\_max\_inline\_xattrs**

Description

オブジェクトごとにファイルシステムに格納されるXATTRの最大数。デフォルトの0は、基礎となるファイルシステムに固有の値を使用することを意味する

Type

32\-bit Integer

Required

No

Default

0

**filestore\_max\_inline\_xattrs\_xfs**

Description

オブジェクトごとにXFSファイルシステムに格納されるXATTRの最大数。filestore\_max\_inline\_xattrs == 0の場合のみ使用されます。

Type

32\-bit Integer

Required

No

Default

10

**filestore\_max\_inline\_xattrs\_btrfs**

Description

オブジェクトごとに Btrfs ファイルシステムに保存される XATTR の最大数。filestore\_max\_inline\_xattrs == 0 の場合のみ使用される

Type

32\-bit Integer

Required

No

Default

10

**filestore\_max\_inline\_xattrs\_other**

Description

オブジェクトごとに他のファイルシステムに格納されるXATTRの最大数。filestore\_max\_inline\_xattrs == 0の場合のみ使用される

Type

32\-bit Integer

Required

No

Default

2

## Synchronization Intervals[¶](https://docs.ceph.com/en/pacific/rados/configuration/filestore-config-ref/ "Permalink to this headline")

Filestoreは定期的に書き込みを停止し、ファイルシステムを同期させる必要があり、これにより一貫したコミットポイントが作成されます。そして、コミットポイントまでのジャーナルエントリーを解放することができます。同期を頻繁に行うことで、同期に必要な時間が短縮され、ジャーナルに残す必要のあるデータ量も減少する傾向にあります。同期の頻度が低いと、下位ファイル・システムは小さな書き込みやメタデータの更新をより最適にまとめることができ、より効率的な同期が可能になりますが、テール・レイテンシが増加する可能性があります。

**filestore\_max\_sync\_interval**

Description

ファイルストアを同期させる最大間隔（秒）

Type

Double

Required

No

Default

5

**filestore\_min\_sync\_interval**

Description

ファイルストアを同期させる最小間隔（秒）

Type

Double

Required

No

Default

.01

## Flusher[¶](https://docs.ceph.com/en/pacific/rados/configuration/filestore-config-ref/ "Permalink to this headline")

Filestore flusher は、大きな書き込みのデータを同期前に sync\_file\_range を使って強制的に書き出すことで、最終的な同期のコストを削減します（期待）。実際には 'filestore\_flusher' を無効にすると性能が向上する場合もあるようです。

**filestore\_flusher**

Description

ファイルストア・フラッシャを有効にする

Type

Boolean

Required

No

Default

false

Deprecated since version v.65.

**filestore\_flusher\_max\_fds**

Description

フラッシャのファイルディスクリプタの最大数

Type

Integer

Required

No

Default

512

Deprecated since version v.65.

**filestore\_sync\_flush**

Description

同期フラッシャを有効にする

Type

Boolean

Required

No

Default

false

Deprecated since version v.65.

**filestore\_fsync\_flushes\_journal\_data**

Description

ファイルシステムの同期中にジャーナルデータをフラッシュする

Type

Boolean

Required

No

Default

false

## Queue[¶](https://docs.ceph.com/en/pacific/rados/configuration/filestore-config-ref/ "Permalink to this headline")

以下の設定は、Filestoreキューのサイズに関する制限を提供します。

**filestore\_queue\_max\_ops**

Description

新しい操作をブロックする前に、ファイルストアが受け入れる進行中の操作の最大数

Type

Integer

Required

No. Minimal impact on performance.

Default

50

**filestore\_queue\_max\_bytes**

Description

操作の最大バイト数

Type

Integer

Required

No

Default

100\<\<20

## Timeouts[¶](https://docs.ceph.com/en/pacific/rados/configuration/filestore-config-ref/ "Permalink to this headline")

**filestore\_op\_threads**

Description

並列に実行されるファイルシステム操作スレッドの数

Type

Integer

Required

No

Default

2

**filestore\_op\_thread\_timeout**

Description

ファイルシステム操作スレッドのタイムアウト（秒）

Type

Integer

Required

No

Default

60

**filestore\_op\_thread\_suicide\_timeout**

Description

コミット操作をキャンセルするまでのタイムアウト時間（秒）

Type

Integer

Required

No

Default

180

## B\-Tree Filesystem[¶](https://docs.ceph.com/en/pacific/rados/configuration/filestore-config-ref/ "Permalink to this headline")

**filestore\_btrfs\_snap**

Description

btrfs ファイルストアでスナップショットを有効にする

Type

Boolean

Required

No. Only used for btrfs.

Default

true

**filestore\_btrfs\_clone\_range**

Description

btrfs ファイルストアのクローン・レンジを有効にする

Type

Boolean

Required

No. Only used for btrfs.

Default

true

## Journal[¶](https://docs.ceph.com/en/pacific/rados/configuration/filestore-config-ref/ "Permalink to this headline")

**filestore\_journal\_parallel**

Description

パラレルジャーナリングを有効にする。Btrfs のデフォルト

Type

Boolean

Required

No

Default

false

**filestore\_journal\_writeahead**

Description

ライトヘッド・ジャーナリングを有効にする。XFS のデフォルト

Type

Boolean

Required

No

Default

false

**filestore\_journal\_trailing**

Description

非推奨。絶対に使用しないこと

Type

Boolean

Required

No

Default

false

## Misc[¶](https://docs.ceph.com/en/pacific/rados/configuration/filestore-config-ref/ "Permalink to this headline")

**filestore\_merge\_threshold**

Description

親ディレクトリにマージする前の、サブディレクトリ内の最小ファイル数 

注：負の値は、サブディレクトリのマージを無効にすることを意味する

Type

Integer

Required

No

Default

\-10

**filestore\_split\_multiple**

Description

_\(filestore\_split\_multiple\*abs\(filestore\_merge\_threshold\)\+\(rand\(\)%filestore\_split\_rand\_factor\)\)\*16_ は、子ディレクトリに分割する前のサブディレクトリの最大ファイル数

Type

Integer

Required

No

Default

2

**filestore\_split\_rand\_factor**

Description

一度に多くの\(高価な\)Filestoreの分割が発生するのを避けるために、分割のしきい値に加えられるランダムな係数。詳細は filestore\_split\_multiple を参照。これは既存のOSDに対して、_ceph\-objectstore\-toolapply\-layout\-settings_コマンドを使用してオフラインでのみ変更可能

Type

Unsigned 32\-bit Integer

Required

No

Default

20

**filestore\_update\_to**

Description

ファイルストアの自動アップグレードを指定したバージョンに制限する

Type

Integer

Required

No

Default

1000

**filestore\_blackhole**

Description

新しいトランザクションを床に落とす

Type

Boolean

Required

No

Default

false

**filestore\_dump\_file**

Description

トランザクション・ダンプを保存するファイル

Type

Boolean

Required

No

Default

false

**filestore\_kill\_at**

Description

N番目の機会でフォルトを注入する

Type

String

Required

No

Default

false

**filestore\_fail\_eio**

Description

EIOでフェイル/クラッシュする

Type

Boolean

Required

No

Default

true
