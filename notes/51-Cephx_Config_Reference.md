# 51: Cephx Config Reference

**クリップソース:** [51: Cephx Config Reference — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/configuration/auth-config-ref/)

# Cephx Config Reference[¶](https://docs.ceph.com/en/pacific/rados/configuration/auth-config-ref/ "Permalink to this headline")

cephxプロトコルは、デフォルトで有効になっています。暗号化された認証にはいくつかの計算コストがかかりますが、一般的には非常に低いはずです。 クライアントホストとサーバーホストを結ぶネットワーク環境が非常に安全で、認証を行う余裕がない場合は、認証をオフにすることができます。これは一般的にはお勧めできません。

注意：認証を無効にすると、中間者攻撃によってクライアント／サーバーのメッセージが改ざんされる危険性があり、セキュリティ上、致命的な影響を与える可能性があります。

ユーザの作成については、「[User Management](https://docs.ceph.com/en/pacific/rados/configuration/auth-config-ref/)」を参照してください。Cephxのアーキテクチャの詳細については、「[Architecture \- High Availability Authentication](https://docs.ceph.com/en/pacific/rados/configuration/auth-config-ref/)」を参照してください。

## Deployment Scenarios[¶](https://docs.ceph.com/en/pacific/rados/configuration/auth-config-ref/ "Permalink to this headline")

Cephクラスタの展開には主に2つのシナリオがあり、Cephxの初期設定方法に影響を与えます。Cephを初めてお使いになる方の多くは、cephadmを使用してクラスタを作成します\(最も簡単です\)。他の展開ツール\(Chef、Juju、Puppetなど\)を使用するクラスタの場合は、手動の手順を使用するか、展開ツールを設定してモニタをブートストラップさせる必要があります。

### Manual Deployment[¶](https://docs.ceph.com/en/pacific/rados/configuration/auth-config-ref/ "Permalink to this headline")

クラスタを手動で展開する際には、モニターを手動でブートストラップさせ、client.adminユーザーとキーリングを作成する必要があります。モニターをブートストラップさせるには、「[Monitor Bootstrapping](https://docs.ceph.com/en/pacific/rados/configuration/auth-config-ref/)」の手順に従います。モニタのブートストラップの手順は、Chef、Puppet、Jujuなどのサードパーティのデプロイメントツールを使用する際に実行しなければならない論理的な手順です。

## Enabling/Disabling Cephx[¶](https://docs.ceph.com/en/pacific/rados/configuration/auth-config-ref/ "Permalink to this headline")

Cephxを有効にするには、モニタ、OSD、およびメタデータサーバにキーを配備する必要があります。Cephxのオン/オフを切り替えるだけであれば、ブートストラップ手順を繰り返す必要はありません。

### Enabling Cephx[¶](https://docs.ceph.com/en/pacific/rados/configuration/auth-config-ref/ "Permalink to this headline")

cephxを有効にすると、Cephはデフォルトの検索パスでキーリングを探し、その中には/etc/ceph/$cluster.$name.keyringが含まれます。[Ceph configuration](https://docs.ceph.com/en/pacific/rados/configuration/auth-config-ref/)ファイルの\[global\]セクションにkeyringオプションを追加することで、この場所をオーバーライドすることができますが、これは推奨されません。  

以下の手順を実行して、認証を無効にしたクラスタでcephxを有効にします。お客様（または展開ユーティリティ）がすでに鍵を生成している場合は、鍵の生成に関する手順を省略できます。

client.adminキーを作成し、クライアントホスト用にキーのコピーを保存します。

```
$ ceph auth get-or-create client.admin mon 'allow *' mds 'allow *' mgr 'allow *' osd 'allow *' -o /etc/ceph/ceph.client.admin.keyring
```

**Warning:** これにより、既存の/etc/ceph/client.admin.keyringファイルが削除されます。デプロイメントツールがすでにこの作業を行っている場合は、この手順を実行しないでください。注意が必要です。

モニタークラスターのキーリングを作成し、モニターシークレットキーを生成します。

```
$ ceph-authtool --create-keyring /tmp/ceph.mon.keyring --gen-key -n mon. --cap mon 'allow *'
```

モニタのキーリングを、すべてのモニタのmondataディレクトリにあるceph.mon.keyringファイルにコピーします。たとえば、cluster cephのmon.aにコピーするには、次のようにします。

```
$ cp /tmp/ceph.mon.keyring /var/lib/ceph/mon/ceph-a/keyring
```

MGR毎に秘密鍵を生成します。{$id}はMGRの文字です。

```
$ ceph auth get-or-create mgr.{$id} mon 'allow profile mgr' mds 'allow *' osd 'allow *' -o /var/lib/ceph/mgr/ceph-{$id}/keyring
```

すべてのOSDに対して秘密鍵を生成（{$id}はOSD番号

```
$ ceph auth get-or-create osd.{$id} mon 'allow rwx' osd 'allow *' -o /var/lib/ceph/osd/ceph-{$id}/keyring
```

各データシートの秘密鍵を生成します。{$id}はデータシートの文字です。

```
$ ceph auth get-or-create mds.{$id} mon 'allow rwx' osd 'allow *' mds 'allow *' mgr 'allow profile mds' -o /var/lib/ceph/mds/ceph-{$id}/keyring
```

Cephの設定ファイルの\[global\]セクションで次のオプションを設定して、cephx認証を有効にします。

```
auth_cluster_required = cephx
auth_service_required = cephx
auth_client_required = cephx
```

Cephクラスタを起動または再起動します。詳細については、「 [Operating a Cluster](https://docs.ceph.com/en/pacific/rados/configuration/auth-config-ref/) 」を参照してください。

手動でモニターを起動する方法については、「 [Manual Deployment](https://docs.ceph.com/en/pacific/rados/configuration/auth-config-ref/)」を参照してください。

### Disabling Cephx[¶](https://docs.ceph.com/en/pacific/rados/configuration/auth-config-ref/ "Permalink to this headline")

以下の手順では、Cephxを無効にする方法を説明します。クラスタ環境が比較的安全な場合は、認証を実行するための計算コストを相殺することができます。推奨はしません。ただし、セットアップやトラブルシューティングの際には、一時的に認証を無効にした方が簡単な場合があります。

Cephの設定ファイルの\[global\]セクションで次のオプションを設定して、cephx認証を無効にします。

```
auth_cluster_required = none
auth_service_required = none
auth_client_required = none
```

Cephクラスタを起動または再起動します。詳細については、「[Operating a Cluster](https://docs.ceph.com/en/pacific/rados/configuration/auth-config-ref/)」を参照してください。

## Configuration Settings[¶](https://docs.ceph.com/en/pacific/rados/configuration/auth-config-ref/ "Permalink to this headline")

### Enablement[¶](https://docs.ceph.com/en/pacific/rados/configuration/auth-config-ref/ "Permalink to this headline")

**auth\_cluster\_required**

Description

有効にすると、Ceph Storage Clusterデーモン\(ceph\-mon、ceph\-osd、ceph\-mds、ceph\-mgrなど\)が相互に認証する必要があります。有効な設定は、cephxまたはnoneです。

Type

_String_

Required

_No_

Default

_cephx._

**auth\_service\_required**

Description

有効にすると、Ceph Storage Clusterデーモンは、Cephサービスにアクセスするために、CephクライアントがCeph Storage Clusterで認証することを要求します。有効な設定は、cephxまたはnoneです。

Type

_String_

Required

_No_

Default

_cephx._

**auth\_client\_required**

Description

有効にすると、Ceph ClientはCeph Storage ClusterにCeph Clientでの認証を要求します。有効な設定は、cephxまたはnoneです。

Type

_String_

Required

_No_

Default

_cephx._

### Keys[¶](https://docs.ceph.com/en/pacific/rados/configuration/auth-config-ref/ "Permalink to this headline")

認証を有効にしてCephを実行すると、cephの管理コマンドとCephクライアントは、Ceph Storage Clusterにアクセスするための認証キーを必要とします。

これらの鍵をcephの管理コマンドとクライアントに提供する最も一般的な方法は、/etc/cephディレクトリの下にCephキーリングを含めることです。cephadmを使用するOctopus以降のリリースでは、ファイル名は通常ceph.client.admin.keyring\(または$cluster.client.admin.keyring\)です。キーリングを/etc/cephディレクトリの下に含めると、Cephの設定ファイルでキーリングエントリを指定する必要がありません。

Ceph Storage Clusterのキーリングファイルにはclient.adminキーが含まれているため、管理コマンドを実行するノードにコピーすることをお勧めします。

この手順を手動で実行するには、次のように実行します。

```
sudo scp {user}@{ceph-cluster-host}:/etc/ceph/ceph.client.admin.keyring /etc/ceph/ceph.client.admin.keyring
```

ヒント:クライアントマシンで、ceph.keyringファイルに適切なパーミッションが設定されていることを確認します\(例: chmod644\)。

キー設定を使用してCeph構成ファイルでキー自体を指定することもできますし\(推奨されません\)、キーファイル設定を使用してキーファイルへのパスを指定することもできます。

**keyring**

Description

キーリングファイルのパスです。

Type

_String_

Required

_No_

Default

_/etc/ceph/$cluster.$name.keyring,/etc/ceph/$cluster.keyring,/etc/ceph/keyring,/etc/ceph/keyring.bin_

**keyfile**

Description

キーファイル（キーのみを含むファイル）へのパスです。

Type

_String_

Required

_No_

Default

_None_

**key**

Description

キー（つまり、キーそのもののテキスト文字列）です。推奨しません。

Type

_String_

Required

_No_

Default

_None_

### Daemon Keyrings[¶](https://docs.ceph.com/en/pacific/rados/configuration/auth-config-ref/ "Permalink to this headline")

管理ユーザまたは展開ツール\(cephadmなど\)は、ユーザキーリングの生成と同じ方法でデーモンキーリングを生成できます。 デフォルトでは、Cephはデーモンのキーリングをそのデータディレクトリ内に保存します。デフォルトのキーリングの場所と、デーモンが機能するために必要な機能を以下に示します。

**ceph\-mon**

Location

_$mon\_data/keyring_

Capabilities

_mon' allow \*'_

**ceph\-osd**

Location

_$osd\_data/keyring_

Capabilities

_mgr' allow profile osd' mon' allow profile osd'osd'allow \* '_

**ceph\-mds**

Location

_$mds\_data/keyring_

Capabilities

_mds 'allow' mgr' allow profile mds' mon 'allow profile mds' osd 'allow rwx'_

**ceph\-mgr**

Location

_$mgr\_data/keyring_

Capabilities

_mon 'allow profile mgr' mds' allow \*' osd 'allow \*'_

**radosgw**

Location

_$rgw\_data/keyring_

Capabilities

_mon 'allow rwx' osd 'allow rwx'_

モニターキーリング\(mon.\)にはキーは含まれていますが、機能は含まれておらず、クラスター認証データベースの一部ではありません。

デーモン・データ・ディレクトリの位置は、デフォルトでは次のような形式のディレクトリになります。

```
/var/lib/ceph/$type/$cluster-$id
```

For example, osd.12 would be:

```
/var/lib/ceph/osd/ceph-12
```

これらの場所を上書きすることもできますが、お勧めできません。

### Signatures[¶](https://docs.ceph.com/en/pacific/rados/configuration/auth-config-ref/ "Permalink to this headline")

Cephは署名チェックを行い、飛行中にメッセージが改ざんされることに対する限定的な保護を提供します\(例: 「man in the middle」攻撃による\)。

Ceph認証の他の部分と同様に、Cephにはきめ細かな制御機能があり、クライアントとCephの間のサービスメッセージに対する署名を有効化/無効化したり、Cephデーモン間のメッセージに対する署名を有効化/無効化したりできます。

署名を有効にしても、データは飛行中には暗号化されないことに注意してください。

**cephx\_require\_signatures**

Description

trueに設定すると、Cephは、CephクライアントとCeph Storage Clusterの間、およびCeph Storage Clusterを構成するデーモンの間のすべてのメッセージトラフィックに署名を要求します。

Ceph Argonautおよび3.19より前のLinuxカーネルバージョンは署名をサポートしていません。このようなクライアントを使用している場合は、このオプションをオフにして接続できるようにすることができます。

Type

_Boolean_

Required

_No_

Default

_false_

**cephx\_cluster\_require\_signatures**

Description

trueに設定すると、Ceph Storage Clusterを構成するCephデーモン間のすべてのメッセージトラフィックにCephが署名を要求します。

Type

_Boolean_

Required

_No_

Default

_false_

**cephx\_service\_require\_signatures**

Description

trueに設定すると、CephはCephクライアントとCeph Storage Clusterの間のすべてのメッセージトラフィックに署名を要求します。

Type

_Boolean_

Required

_No_

Default

_false_

**cephx\_sign\_messages**

Description

Cephのバージョンがメッセージ署名をサポートしている場合、Cephはすべてのメッセージに署名するので、メッセージの詐称が難しくなります。

Type

_Boolean_

Default

_true_

### Time to Live[¶](https://docs.ceph.com/en/pacific/rados/configuration/auth-config-ref/ "Permalink to this headline")

**auth\_service\_ticket\_ttl**

Description

Ceph Storage ClusterがCeph Clientに認証用のチケットを送信すると、Ceph Storage Clusterはそのチケットに生存時間を割り当てます。

Type

_Double_

Default

_60\*60_
