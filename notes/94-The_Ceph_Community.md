# 94: The Ceph Community

 # The Ceph Community[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/community/ "Permalink to this headline")

The Ceph community is an excellent source of information and help. For operational issues with Ceph releases we recommend you [subscribe to the ceph\-users email list](https://docs.ceph.com/en/pacific/rados/troubleshooting/community/). When you no longer want to receive emails, you can[unsubscribe from the ceph\-users email list](https://docs.ceph.com/en/pacific/rados/troubleshooting/community/).

You may also [subscribe to the ceph\-devel email list](https://docs.ceph.com/en/pacific/rados/troubleshooting/community/). You should do so if your issue is:

* Likely related to a bug
* Related to a development release package
* Related to a development testing package
* Related to your own builds

If you no longer want to receive emails from the `ceph-devel` email list, you may [unsubscribe from the ceph\-devel email list](https://docs.ceph.com/en/pacific/rados/troubleshooting/community/).

Tip:

The Ceph community is growing rapidly, and community members can help you if you provide them with detailed information about your problem. You can attach the output of the `ceph report` command to help people understand your issues.
