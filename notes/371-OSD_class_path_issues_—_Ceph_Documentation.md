# 371: OSD class path issues — Ceph Documentation

 # OSD class path issues[¶](https://docs.ceph.com/en/pacific/dev/osd-class-path/ "Permalink to this headline")

```
2011-12-05 17:41:00.994075 7ffe8b5c3760 librbd: failed to assign a block name for image
create error: error 5: Input/output error
```

This usually happens because your OSDs can’t find `cls_rbd.so`. They search for it in `osd_class_dir`, which may not be set correctly by default \([http://tracker.ceph.com/issues/1722](https://docs.ceph.com/en/pacific/dev/osd-class-path/)\).

Most likely it’s looking in `/usr/lib/rados-classes` instead of`/usr/lib64/rados-classes` \- change `osd_class_dir` in your`ceph.conf` and restart the OSDs to fix it.
