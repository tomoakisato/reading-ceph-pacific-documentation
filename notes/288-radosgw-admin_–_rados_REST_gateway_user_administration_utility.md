# 288: radosgw-admin – rados REST gateway user administration utility

 # radosgw\-admin – rados REST gateway user administration utility[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this headline")

## Synopsis[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this headline")

**radosgw\-admin** _command_ \[ _options_ _…_ \]

## Description[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this headline")

**radosgw\-admin** is a RADOS gateway user administration utility. It allows creating and modifying users.

## Commands[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this headline")

**radosgw\-admin** utility uses many commands for administration purpose which are as follows:

**user create**Create a new user.

**user modify**Modify a user.

**user info**Display information of a user, and any potentially available subusers and keys.

**user rename**Renames a user.

**user rm**Remove a user.

**user suspend**Suspend a user.

**user enable**Re\-enable user after suspension.

**user check**Check user info.

**user stats**Show user stats as accounted by quota subsystem.

**user list**List all users.

**caps add**Add user capabilities.

**caps rm**Remove user capabilities.

**subuser create**Create a new subuser \(primarily useful for clients using the Swift API\).

**subuser modify**Modify a subuser.

**subuser rm**Remove a subuser.

**key create**Create access key.

**key rm**Remove access key.

**bucket list**List buckets, or, if bucket specified with –bucket=\<bucket\>, list its objects. If bucket specified adding –allow\-unordered removes ordering requirement, possibly generating results more quickly in buckets with large number of objects.

**bucket limit check**Show bucket sharding stats.

**bucket link**Link bucket to specified user.

**bucket unlink**Unlink bucket from specified user.

**bucket chown**Link bucket to specified user and update object ACLs. Use –marker to resume if command gets interrupted.

**bucket stats**Returns bucket statistics.

**bucket rm**Remove a bucket.

**bucket check**Check bucket index.

**bucket rewrite**Rewrite all objects in the specified bucket.

**bucket radoslist**List the rados objects that contain the data for all objects is the designated bucket, if –bucket=\<bucket\> is specified, or otherwise all buckets.

**bucket reshard**Reshard a bucket.

**bucket sync disable**Disable bucket sync.

**bucket sync enable**Enable bucket sync.

**bi get**Retrieve bucket index object entries.

**bi put**Store bucket index object entries.

**bi list**List raw bucket index entries.

**bi purge**Purge bucket index entries.

**object rm**Remove an object.

**object stat**Stat an object for its metadata.

**object unlink**Unlink object from bucket index.

**object rewrite**Rewrite the specified object.

**objects expire**Run expired objects cleanup.

**period rm**Remove a period.

**period get**Get the period info.

**period get\-current**Get the current period info.

**period pull**Pull a period.

**period push**Push a period.

**period list**List all periods.

**period update**Update the staging period.

**period commit**Commit the staging period.

**quota set**Set quota params.

**quota enable**Enable quota.

**quota disable**Disable quota.

**global quota get**View global quota parameters.

**global quota set**Set global quota parameters.

**global quota enable**Enable a global quota.

**global quota disable**Disable a global quota.

**realm create**Create a new realm.

**realm rm**Remove a realm.

**realm get**Show the realm info.

**realm get\-default**Get the default realm name.

**realm list**List all realms.

**realm list\-periods**List all realm periods.

**realm rename**Rename a realm.

**realm set**Set the realm info \(requires infile\).

**realm default**Set the realm as default.

**realm pull**Pull a realm and its current period.

**zonegroup add**Add a zone to a zonegroup.

**zonegroup create**Create a new zone group info.

**zonegroup default**Set the default zone group.

**zonegroup rm**Remove a zone group info.

**zonegroup get**Show the zone group info.

**zonegroup modify**Modify an existing zonegroup.

**zonegroup set**Set the zone group info \(requires infile\).

**zonegroup remove**Remove a zone from a zonegroup.

**zonegroup rename**Rename a zone group.

**zonegroup list**List all zone groups set on this cluster.

**zonegroup placement list**List zonegroup’s placement targets.

**zonegroup placement add**Add a placement target id to a zonegroup.

**zonegroup placement modify**Modify a placement target of a specific zonegroup.

**zonegroup placement rm**Remove a placement target from a zonegroup.

**zonegroup placement default**Set a zonegroup’s default placement target.

**zone create**Create a new zone.

**zone rm**Remove a zone.

**zone get**Show zone cluster params.

**zone set**Set zone cluster params \(requires infile\).

**zone modify**Modify an existing zone.

**zone list**List all zones set on this cluster.

**metadata sync status**Get metadata sync status.

**metadata sync init**Init metadata sync.

**metadata sync run**Run metadata sync.

**data sync status**Get data sync status of the specified source zone.

**data sync init**Init data sync for the specified source zone.

**data sync run**Run data sync for the specified source zone.

**sync error list**list sync error.

**sync error trim**trim sync error.

**zone rename**Rename a zone.

**zone placement list**List zone’s placement targets.

**zone placement add**Add a zone placement target.

**zone placement modify**Modify a zone placement target.

**zone placement rm**Remove a zone placement target.

**pool add**Add an existing pool for data placement.

**pool rm**Remove an existing pool from data placement set.

**pools list**List placement active set.

**policy**Display bucket/object policy.

**log list**List log objects.

**log show**Dump a log from specific object or \(bucket \+ date \+ bucket\-id\). \(NOTE: required to specify formatting of date to “YYYY\-MM\-DD\-hh”\)

**log rm**Remove log object.

**usage show**Show the usage information \(with optional user and date range\).

**usage trim**Trim usage information \(with optional user and date range\).

**gc list**Dump expired garbage collection objects \(specify –include\-all to list all entries, including unexpired\).

**gc process**Manually process garbage.

**lc list**List all bucket lifecycle progress.

**lc process**Manually process lifecycle.

**metadata get**Get metadata info.

**metadata put**Put metadata info.

**metadata rm**Remove metadata info.

**metadata list**List metadata info.

**mdlog list**List metadata log.

**mdlog trim**Trim metadata log.

**mdlog status**Read metadata log status.

**bilog list**List bucket index log.

**bilog trim**Trim bucket index log \(use start\-marker, end\-marker\).

**datalog list**List data log.

**datalog trim**Trim data log.

**datalog status**Read data log status.

**orphans find**Init and run search for leaked rados objects. DEPRECATED. See the “rgw\-orphan\-list” tool.

**orphans finish**Clean up search for leaked rados objects. DEPRECATED. See the “rgw\-orphan\-list” tool.

**orphans list\-jobs**List the current job\-ids for the orphans search. DEPRECATED. See the “rgw\-orphan\-list” tool.

**role create**create a new AWS role for use with STS.

**role rm**Remove a role.

**role get**Get a role.

**role list**List the roles with specified path prefix.

**role modify**Modify the assume role policy of an existing role.

**role\-policy put**Add/update permission policy to role.

**role\-policy list**List the policies attached to a role.

**role\-policy get**Get the specified inline policy document embedded with the given role.

**role\-policy rm**Remove the policy attached to a role

**reshard add**Schedule a resharding of a bucket

**reshard list**List all bucket resharding or scheduled to be resharded

**reshard process**Process of scheduled reshard jobs

**reshard status**Resharding status of a bucket

**reshard cancel**Cancel resharding a bucket

**topic list**List bucket notifications/pubsub topics

**topic get**Get a bucket notifications/pubsub topic

**topic rm**Remove a bucket notifications/pubsub topic

**subscription get**Get a pubsub subscription definition

**subscription rm**Remove a pubsub subscription

**subscription pull**Show events in a pubsub subscription

**subscription ack**Ack \(remove\) an events in a pubsub subscription

## Options[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this headline")

`-c`` ceph.conf``, ``--conf``=ceph.conf`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Use `ceph.conf` configuration file instead of the default`/etc/ceph/ceph.conf` to determine monitor addresses during startup.

`-m`` monaddress[:port]`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Connect to specified monitor \(instead of looking through ceph.conf\).

`--tenant``=<tenant>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Name of the tenant.

`--uid``=uid`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The radosgw user ID.

`--new-uid``=uid`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")ID of the new user. Used with ‘user rename’ command.

`--subuser``=<name>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Name of the subuser.

`--access-key``=<key>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")S3 access key.

`--email``=email`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The e\-mail address of the user.

`--secret/--secret-key``=<key>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The secret key.

`--gen-access-key```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Generate random access key \(for S3\).

`--gen-secret```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Generate random secret key.

`--key-type``=<type>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")key type, options are: swift, s3.

`--temp-url-key[-2]``=<key>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Temporary url key.

`--max-buckets```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")max number of buckets for a user \(0 for no limit, negative value to disable bucket creation\). Default is 1000.

`--access``=<access>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Set the access permissions for the sub\-user. Available access permissions are read, write, readwrite and full.

`--display-name``=<name>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The display name of the user.

`--admin```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Set the admin flag on the user.

`--system```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Set the system flag on the user.

`--bucket``=[tenant-id/]bucket`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Specify the bucket name. If tenant\-id is not specified, the tenant\-id of the user \(–uid\) is used.

`--pool``=<pool>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Specify the pool name. Also used with orphans find as data pool to scan for leaked rados objects.

`--object``=object`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Specify the object name.

`--date``=yyyy-mm-dd`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The date in the format yyyy\-mm\-dd.

`--start-date``=yyyy-mm-dd`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The start date in the format yyyy\-mm\-dd.

`--end-date``=yyyy-mm-dd`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The end date in the format yyyy\-mm\-dd.

`--bucket-id``=<bucket-id>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Specify the bucket id.

`--bucket-new-name``=[tenant-id/]<bucket>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Optional for bucket link; use to rename a bucket.While tenant\-id/ can be specified, this is never necessary for normal operation.

`--shard-id``=<shard-id>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Optional for mdlog list, bi list, data sync status. Required for `mdlog trim`.

`--max-entries``=<entries>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Optional for listing operations to specify the max entires

`--purge-data```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")When specified, user removal will also purge all the user data.

`--purge-keys```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")When specified, subuser removal will also purge all the subuser keys.

`--purge-objects```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")When specified, the bucket removal will also purge all objects in it.

`--metadata-key``=<key>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Key to retrieve metadata from with `metadata get`.

`--remote``=<remote>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Zone or zonegroup id of remote gateway.

`--period``=<id>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Period id.

`--url``=<url>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")url for pushing/pulling period or realm.

`--epoch``=<number>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Period epoch.

`--commit```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Commit the period during ‘period update’.

`--staging```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Get the staging period info.

`--master```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Set as master.

`--master-zone``=<id>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Master zone id.

`--rgw-realm``=<name>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The realm name.

`--realm-id``=<id>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The realm id.

`--realm-new-name``=<name>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")New name of realm.

`--rgw-zonegroup``=<name>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The zonegroup name.

`--zonegroup-id``=<id>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The zonegroup id.

`--zonegroup-new-name``=<name>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The new name of the zonegroup.

`--rgw-zone``=<zone>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Zone in which radosgw is running.

`--zone-id``=<id>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The zone id.

`--zone-new-name``=<name>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The new name of the zone.

`--source-zone```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The source zone for data sync.

`--default```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Set the entity \(realm, zonegroup, zone\) as default.

`--read-only```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Set the zone as read\-only when adding to the zonegroup.

`--placement-id```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Placement id for the zonegroup placement commands.

`--tags``=<list>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The list of tags for zonegroup placement add and modify commands.

`--tags-add``=<list>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The list of tags to add for zonegroup placement modify command.

`--tags-rm``=<list>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The list of tags to remove for zonegroup placement modify command.

`--endpoints``=<list>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The zone endpoints.

`--index-pool``=<pool>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The placement target index pool.

`--data-pool``=<pool>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The placement target data pool.

`--data-extra-pool``=<pool>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The placement target data extra \(non\-ec\) pool.

`--placement-index-type``=<type>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The placement target index type \(normal, indexless, or \#id\).

`--tier-type``=<type>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The zone tier type.

`--tier-config``=<k>=<v>[,...]`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Set zone tier config keys, values.

`--tier-config-rm``=<k>[,...]`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Unset zone tier config keys.

`--sync-from-all``[=false]`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Set/reset whether zone syncs from all zonegroup peers.

`--sync-from``=[zone-name][,...]`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Set the list of zones to sync from.

`--sync-from-rm``=[zone-name][,...]`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Remove the zones from list of zones to sync from.

`--bucket-index-max-shards```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Override a zone’s or zonegroup’s default number of bucket index shards. This option is accepted by the ‘zone create’, ‘zone modify’, ‘zonegroup add’, and ‘zonegroup modify’ commands, and applies to buckets that are created after the zone/zonegroup changes take effect.

`--fix```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Besides checking bucket index, will also fix it.

`--check-objects```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")bucket check: Rebuilds bucket index according to actual objects state.

`--format``=<format>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Specify output format for certain operations. Supported formats: xml, json.

`--sync-stats```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Option for ‘user stats’ command. When specified, it will update user stats with the current stats reported by user’s buckets indexes.

`--show-log-entries``=<flag>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Enable/disable dump of log entries on log show.

`--show-log-sum``=<flag>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Enable/disable dump of log summation on log show.

`--skip-zero-entries```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Log show only dumps entries that don’t have zero value in one of the numeric field.

`--infile```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Specify a file to read in when setting data.

`--categories``=<list>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Comma separated list of categories, used in usage show.

`--caps``=<caps>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")List of caps \(e.g., “usage=read, write; user=read”.

`--compression``=<compression-algorithm>`[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Placement target compression algorithm \(lz4|snappy|zlib|zstd\)

`--yes-i-really-mean-it```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Required for certain operations.

`--min-rewrite-size```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Specify the min object size for bucket rewrite \(default 4M\).

`--max-rewrite-size```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Specify the max object size for bucket rewrite \(default ULLONG\_MAX\).

`--min-rewrite-stripe-size```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Specify the min stripe size for object rewrite \(default 0\). If the value is set to 0, then the specified object will always be rewritten for restriping.

`--warnings-only```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")When specified with bucket limit check, list only buckets nearing or over the current max objects per shard value.

`--bypass-gc```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")When specified with bucket deletion, triggers object deletions by not involving GC.

`--inconsistent-index```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")When specified with bucket deletion and bypass\-gc set to true, ignores bucket index consistency.

`--max-concurrent-ios```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Maximum concurrent ios for bucket operations. Affects operations that scan the bucket index, e.g., listing, deletion, and all scan/search operations such as finding orphans or checking the bucket index. Default is 32.

## Quota Options[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this headline")

`--max-objects```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Specify max objects \(negative value to disable\).

`--max-size```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Specify max size \(in B/K/M/G/T, negative value to disable\).

`--quota-scope```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The scope of quota \(bucket, user\).

## Orphans Search Options[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this headline")

`--num-shards```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Number of shards to use for keeping the temporary scan info

`--orphan-stale-secs```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Number of seconds to wait before declaring an object to be an orphan. Default is 86400 \(24 hours\).

`--job-id```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Set the job id \(for orphans find\)

## Orphans list\-jobs options[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this headline")

`--extra-info```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")Provide extra info in the job list.

## Role Options[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this headline")

`--role-name```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The name of the role to create.

`--path```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The path to the role.

`--assume-role-policy-doc```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The trust relationship policy document that grants an entity permission to assume the role.

`--policy-name```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The name of the policy document.

`--policy-doc```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The permission policy document.

`--path-prefix```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The path prefix for filtering the roles.

## Bucket Notifications/PubSub Options[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this headline")

`--topic```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The bucket notifications/pubsub topic name.

`--subscription```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The pubsub subscription name.

`--event-id```[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this definition")The event id in a pubsub subscription.

## Examples[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this headline")

Generate a new user:

```
$ radosgw-admin user create --display-name="johnny rotten" --uid=johnny
{ "user_id": "johnny",
  "rados_uid": 0,
  "display_name": "johnny rotten",
  "email": "",
  "suspended": 0,
  "subusers": [],
  "keys": [
        { "user": "johnny",
          "access_key": "TCICW53D9BQ2VGC46I44",
          "secret_key": "tfm9aHMI8X76L3UdgE+ZQaJag1vJQmE6HDb5Lbrz"}],
  "swift_keys": []}
```

Remove a user:

```
$ radosgw-admin user rm --uid=johnny
```

Rename a user:

```
$ radosgw-admin user rename --uid=johny --new-uid=joe
```

Remove a user and all associated buckets with their contents:

```
$ radosgw-admin user rm --uid=johnny --purge-data
```

Remove a bucket:

```
$ radosgw-admin bucket rm --bucket=foo
```

Link bucket to specified user:

```
$ radosgw-admin bucket link --bucket=foo --bucket_id=<bucket id> --uid=johnny
```

Unlink bucket from specified user:

```
$ radosgw-admin bucket unlink --bucket=foo --uid=johnny
```

Rename a bucket:

```
$ radosgw-admin bucket link --bucket=foo --bucket-new-name=bar --uid=johnny
```

Move a bucket from the old global tenant space to a specified tenant:

```
$ radosgw-admin bucket link --bucket=/foo --uid=12345678$12345678'
```

Link bucket to specified user and change object ACLs:

```
$ radosgw-admin bucket chown --bucket=/foo --uid=12345678$12345678'
```

Show the logs of a bucket from April 1st, 2012:

```
$ radosgw-admin log show --bucket=foo --date=2012-04-01-01 --bucket-id=default.14193.1
```

Show usage information for user from March 1st to \(but not including\) April 1st, 2012:

```
$ radosgw-admin usage show --uid=johnny 
                --start-date=2012-03-01 --end-date=2012-04-01
```

Show only summary of usage information for all users:

```
$ radosgw-admin usage show --show-log-entries=false
```

Trim usage information for user until March 1st, 2012:

```
$ radosgw-admin usage trim --uid=johnny --end-date=2012-04-01
```

## Availability[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this headline")

**radosgw\-admin** is part of Ceph, a massively scalable, open\-source, distributed storage system. Please refer to the Ceph documentation at[http://ceph.com/docs](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/) for more information.

## See also[¶](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/ "Permalink to this headline")

[ceph](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/)\(8\)[radosgw](https://docs.ceph.com/en/pacific/man/8/radosgw-admin/)\(8\)
