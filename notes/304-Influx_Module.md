# 304: Influx Module

 # Influx Module[¶](https://docs.ceph.com/en/pacific/mgr/influx/ "Permalink to this headline")

The influx module continuously collects and sends time series data to an influxdb database.

The influx module was introduced in the 13.x _Mimic_ release.

## Enabling[¶](https://docs.ceph.com/en/pacific/mgr/influx/ "Permalink to this headline")

To enable the module, use the following command:

```
ceph mgr module enable influx
```

If you wish to subsequently disable the module, you can use the equivalent_disable_ command:

```
ceph mgr module disable influx
```

## Configuration[¶](https://docs.ceph.com/en/pacific/mgr/influx/ "Permalink to this headline")

For the influx module to send statistics to an InfluxDB server, it is necessary to configure the servers address and some authentication credentials.

Set configuration values using the following command:

```
ceph config set mgr mgr/influx/<key> <value>
```

The most important settings are `hostname`, `username` and `password`. For example, a typical configuration might look like this:

```
ceph config set mgr mgr/influx/hostname influx.mydomain.com
ceph config set mgr mgr/influx/username admin123
ceph config set mgr mgr/influx/password p4ssw0rd
```

Additional optional configuration settings are:

intervalTime between reports to InfluxDB. Default 30 seconds.

databaseInfluxDB database name. Default “ceph”. You will need to create this database and grant write privileges to the configured username or the username must have admin privileges to create it.

portInfluxDB server port. Default 8086

sslUse https connection for InfluxDB server. Use “true” or “false”. Default false

verify\_sslVerify https cert for InfluxDB server. Use “true” or “false”. Default true

threadsHow many worker threads should be spawned for sending data to InfluxDB. Default is 5

batch\_sizeHow big batches of data points should be when sending to InfluxDB. Default is 5000

## Debugging[¶](https://docs.ceph.com/en/pacific/mgr/influx/ "Permalink to this headline")

By default, a few debugging statements as well as error statements have been set to print in the log files. Users can add more if necessary. To make use of the debugging option in the module:

* Add this to the ceph.conf file.:
    ```
    [mgr]
        debug_mgr = 20
    ```
* Use this command `ceph influx self-test`.
* Check the log files. Users may find it easier to filter the log files using _mgr\[influx\]_.

## Interesting counters[¶](https://docs.ceph.com/en/pacific/mgr/influx/ "Permalink to this headline")

The following tables describe a subset of the values output by this module.

### Pools[¶](https://docs.ceph.com/en/pacific/mgr/influx/ "Permalink to this headline")

|Counter |Description |
|-----------|---------------------------------------------|
|stored |Bytes stored in the pool not including copies |
|max\_avail |Max available number of bytes in the pool |
|objects |Number of objects in the pool |
|wr\_bytes |Number of bytes written in the pool |
|dirty |Number of bytes dirty in the pool |
|rd\_bytes |Number of bytes read in the pool |
|stored\_raw |Bytes used in pool including copies made |

### OSDs[¶](https://docs.ceph.com/en/pacific/mgr/influx/ "Permalink to this headline")

|Counter |Description |
|--------------|----------------------------------|
|op\_w |Client write operations |
|op\_in\_bytes |Client operations total write size |
|op\_r |Client read operations |
|op\_out\_bytes |Client operations total read size |

|Counter |Description |
|----------------------------|--------------------------------------------------------------------------------------------------------|
|op\_wip |Replication operations currently being processed \(primary\) |
|op\_latency |Latency of client operations \(including queue time\) |
|op\_process\_latency |Latency of client operations \(excluding queue time\) |
|op\_prepare\_latency |Latency of client operations \(excluding queue time and wait for finished\) |
|op\_r\_latency |Latency of read operation \(including queue time\) |
|op\_r\_process\_latency |Latency of read operation \(excluding queue time\) |
|op\_w\_in\_bytes |Client data written |
|op\_w\_latency |Latency of write operation \(including queue time\) |
|op\_w\_process\_latency |Latency of write operation \(excluding queue time\) |
|op\_w\_prepare\_latency |Latency of write operations \(excluding queue time and wait for finished\) |
|op\_rw |Client read\-modify\-write operations |
|op\_rw\_in\_bytes |Client read\-modify\-write operations write in |
|op\_rw\_out\_bytes |Client read\-modify\-write operations read out |
|op\_rw\_latency |Latency of read\-modify\-write operation \(including queue time\) |
|op\_rw\_process\_latency |Latency of read\-modify\-write operation \(excluding queue time\) |
|op\_rw\_prepare\_latency |Latency of read\-modify\-write operations \(excluding queue time and wait for finished\) |
|op\_before\_queue\_op\_lat |Latency of IO before calling queue \(before really queue into ShardedOpWq\) op\_before\_dequeue\_op\_lat |
|op\_before\_dequeue\_op\_lat |Latency of IO before calling dequeue\_op\(already dequeued and get PG lock\) |

Latency counters are measured in microseconds unless otherwise specified in the description.
