# 46: Storage Devices

**クリップソース:** [46: Storage Devices — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/configuration/storage-devices/)

# Storage Devices[¶](https://docs.ceph.com/en/pacific/rados/configuration/storage-devices/ "Permalink to this headline")

デバイスにデータを保存するCephのデーモンは2つあります。

* **Ceph OSDs**
* **Ceph Monitor**

## OSD Backends[¶](https://docs.ceph.com/en/pacific/rados/configuration/storage-devices/ "Permalink to this headline")

OSDが保存するデータを管理する方法は2つあります。 Luminous 12.2.zのリリースから、新しいデフォルト（および推奨）のバックエンドはBlueStoreとなりました。 Luminous以前のデフォルト（および唯一の選択肢）はFilestoreでした。

### BlueStore[¶](https://docs.ceph.com/en/pacific/rados/configuration/storage-devices/ "Permalink to this headline")

BlueStoreは、Ceph OSDワークロードのディスク上のデータを管理するために特別に設計された、特殊目的のストレージバックエンドです。 このバックエンドは、過去10年間にFileStoreを使用してOSDをサポートおよび管理してきた経験に基づいています。 BlueStoreの主な機能は以下の通りです。

* ストレージデバイスの直接管理。 BlueStoreは生のブロックデバイスまたはパーティションを使用します。 これにより、パフォーマンスを制限したり、複雑さを増す可能性のある抽象化された層（XFSのようなローカルファイルシステムなど）が介在することはありません。
* RocksDBによるメタデータ管理。 オブジェクト名とディスク上のブロック位置のマッピングなど、内部のメタデータを管理するために、RocksDBのkey/valueデータベースを組み込んでいます。
* データとメタデータの完全なチェックサム。 デフォルトでは、BlueStoreに書き込まれたすべてのデータとメタデータは、1つ以上のチェックサムによって保護されます。 データやメタデータが検証されずにディスクから読み込まれたり、ユーザーに返されることはありません。
* インラインでの圧縮。 書き込まれたデータは、ディスクに書き込まれる前にオプションで圧縮されることがあります。
* マルチデバイスでのメタデータ階層化。 BlueStoreでは、内部ジャーナル（ライトアヘッドログ）を別の高速デバイス（SSD、NVMe、NVDIMMなど）に書き込んでパフォーマンスを向上させることができます。 また、大量の高速ストレージが利用可能な場合、内部メタデータを高速デバイスに保存することも可能です。
* 効率的なコピーオンライト。 RBDとCephFSのスナップショットは、BlueStoreで効率的に実装されているコピーオンライトのクローンメカニズムに依存しています。 これにより、通常のスナップショットでも、EC化されたプール\(クローンに依存して効率的な2フェーズコミットを実装している\)でも、効率的なIOが実現します。

詳しくは、[BlueStore Config Reference](https://docs.ceph.com/en/pacific/rados/configuration/storage-devices/) と [BlueStore Migration](https://docs.ceph.com/en/pacific/rados/configuration/storage-devices/) をご覧ください。

### FileStore[¶](https://docs.ceph.com/en/pacific/rados/configuration/storage-devices/ "Permalink to this headline")

FileStoreは、Cephでオブジェクトを格納するためのレガシーなアプローチです。 標準のファイルシステム\(通常はXFS\)と、一部のメタデータ用のキー/バリューデータベース\(従来はLevelDB、現在はRocksDB\)の組み合わせに依存しています。

FileStoreは十分にテストされており、本番環境で広く使用されていますが、全体的な設計と、オブジェクトデータの保存に従来のファイルシステムに依存していることから、多くのパフォーマンス上の欠陥があります。

FileStoreは、一般的にほとんどのPOSIX互換のファイルシステム（btrfsやext4を含む）で機能することができますが、当社ではXFSの使用のみを推奨しています。 btrfsとext4には既知のバグや欠陥があり、これらを使用するとデータの損失につながる可能性があります。 デフォルトでは、すべてのCephプロビジョニングツールはXFSを使用します。

詳細は、「[Filestore Config Reference](https://docs.ceph.com/en/pacific/rados/configuration/storage-devices/)」を参照してください。 
