# 17: Install Virtualization for Block Device

**クリップソース:** [17: Install Virtualization for Block Device — Ceph Documentation](https://docs.ceph.com/en/pacific/install/install-vm-cloud/)

# Install Virtualization for Block Device¶

Ceph Block DevicesとCeph Storage Clusterを仮想マシン\(VM\)や[クラウドプラットフォー�](https://docs.ceph.com/en/pacific/glossary/#term-Cloud-Platforms)�のバックエンドとして使用する場合、QEMU/KVMとlibvirtパッケージは、VMとクラウドプラットフォームを有効にするために重要です。VMの例を以下に示します。QEMU/KVM、XEN、VMWare、LXC、VirtualBoxなど。クラウドプラットフォームの例としては、OpenStack、CloudStack、OpenNebulaなどがあります。

![695c571bee8be98d46d62725013224a2902df18dc52481d7b7572c4a8a91119d.png](image/695c571bee8be98d46d62725013224a2902df18dc52481d7b7572c4a8a91119d.png)

## **Install QEMU[¶](https://docs.ceph.com/en/pacific/install/install-vm-cloud/#install-qemu "Permalink to this headline")**

QEMU KVMは、 librbdを介してCeph Block Devicesと対話できます。これは、Cephをクラウドプラットフォームで使用するための重要な機能です。QEMUをインストールしたら、使い方については「[QEMU and Block Devices](https://docs.ceph.com/en/pacific/rbd/qemu-rbd)」を参照してください。

[195: QEMU and Block Devices](195-QEMU_and_Block_Devices.md)

### Debian Packages[¶](https://docs.ceph.com/en/pacific/install/install-vm-cloud/#debian-packages "Permalink to this headline")

QEMUのパッケージは、Ubuntu 12.04 Precise Pangolin以降のバージョンに組み込まれています。QEMUをインストールするには、次のように実行します。

```
sudo apt-get install qemu
```

### RPM Packages[¶](https://docs.ceph.com/en/pacific/install/install-vm-cloud/#rpm-packages "Permalink to this headline")

QEMUをインストールするには、次のように実行します。

1. リポジトリを更新する。

```
sudo yum update
```

2. QEMU for Cephをインストールします。

```
sudo yum install qemu-kvm qemu-kvm-tools qemu-img
```

3. 追加のQEMUパッケージをインストールします（オプション）。

```
sudo yum install qemu-guest-agent qemu-guest-agent-win32
```

### Building QEMU[¶](https://docs.ceph.com/en/pacific/install/install-vm-cloud/#building-qemu "Permalink to this headline")

QEMUをソースからビルドするには、以下の手順で行います。

```
cd {your-development-directory}
git clone git://git.qemu.org/qemu.git
cd qemu
./configure --enable-rbd
make; make install
```

## **Install libvirt[¶](https://docs.ceph.com/en/pacific/install/install-vm-cloud/#install-libvirt "Permalink to this headline")**

Cephでlibvirtを使用するには、実行中のCeph Storage Clusterがあり、QEMUをインストールして設定している必要があります。使用方法については、「[Using libvirt with Ceph Block Device](https://docs.ceph.com/en/pacific/rbd/libvirt)」を参照してください。

[196: Using libvirt with Ceph RBD](196-Using_libvirt_with_Ceph_RBD.md)

### Debian Packages[¶](https://docs.ceph.com/en/pacific/install/install-vm-cloud/#id1 "Permalink to this headline")

libvirtのパッケージは、Ubuntu 12.04 Precise Pangolin以降のバージョンに組み込まれています。これらのディストリビューションにlibvirtをインストールするには、次のように実行します。

```
sudo apt-get update && sudo apt-get install libvirt-bin
```

### RPM Packages[¶](https://docs.ceph.com/en/pacific/install/install-vm-cloud/#id2 "Permalink to this headline")

Ceph Storage Clusterでlibvirtを使用するには、Ceph Storage Clusterが稼働していて、rbd形式をサポートするQEMUのバージョンをインストールしている必要があります。詳細については、「[Install QEMU](https://docs.ceph.com/en/pacific/install/install-vm-cloud/#install-qemu)」を参照してください。

[17.Install Virtualization for Block Device](17-Install_Virtualization_for_Block_Device.md)

libvirt パッケージは、最近の CentOS/RHEL ディストリビューションに組み込まれています。libvirtをインストールするには、次のように実行します。

```
sudo yum install libvirt
```

### Building libvirt[¶](https://docs.ceph.com/en/pacific/install/install-vm-cloud/#building-libvirt "Permalink to this headline")

libvirt をソースからビルドするには、libvirt リポジトリをクローンし、[AutoGen](http://www.gnu.org/software/autogen/) を使用してビルドを生成します。その後、make と make install を実行してインストールを完了します。例えば、以下のようになります。

```
git clone git://libvirt.org/libvirt.git
cd libvirt
./autogen.sh
make
sudo make install
```

詳細は、「[libvirt Installation](http://www.libvirt.org/compiling.html)」を参照してください。
