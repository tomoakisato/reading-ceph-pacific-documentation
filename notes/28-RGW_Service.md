# 28: RGW Service

**クリップソース:** [28: RGW Service — Ceph Documentation](https://docs.ceph.com/en/pacific/cephadm/services/rgw/)

# RGW Service[¶](https://docs.ceph.com/en/pacific/cephadm/services/rgw/ "Permalink to this headline")

## Deploy RGWs[¶](https://docs.ceph.com/en/pacific/cephadm/services/rgw/ "Permalink to this headline")

Cephadmは、単一クラスタの展開、またはマルチサイトの展開における特定のレルムとゾーンを管理するデーモンのコレクションとして、radosgwを展開します。 \(レルムとゾーンの詳細については、「[Multi\-Site](https://docs.ceph.com/en/pacific/cephadm/services/rgw/)」を参照してください\)。

cephadmでは、ceph.confやコマンドラインからではなく、モニター構成データベースを介してradosgwデーモンが設定されることに注意してください。 その設定がまだ行われていない場合\(通常はclient.rgw.\<something\>セクション\)、radosgwデーモンはデフォルトの設定\(たとえば、ポート80へのバインド\)で起動します。

任意のサービス名名を持つradosgwデーモンのセットを展開するには、以下のコマンドを実行します。

```
# ceph orch apply rgw *<name>* [--realm=*<realm-name>*] [--zone=*<zone-name>*] --placement="*<num-daemons>* [*<host1>* ...]"
```

### Trivial setup[¶](https://docs.ceph.com/en/pacific/cephadm/services/rgw/ "Permalink to this headline")

例えば、任意のサービスID「foo」のもと、シングルクラスターのRGW展開に2つのRGWデーモン（デフォルト）を配置する場合。

```
# ceph orch apply rgw foo
```

### Designated gateways[¶](https://docs.ceph.com/en/pacific/cephadm/services/rgw/ "Permalink to this headline")

一般的なシナリオとしては、ゲートウェイとして動作するホストのラベルが貼られており、複数のradosgwインスタンスが連続したポート8000と8001で実行されています。

```
# ceph orch host label add gwhost1 rgw  # the 'rgw' label can be anything
# ceph orch host label add gwhost2 rgw
# ceph orch apply rgw foo '--placement=label:rgw count-per-host:2' --port=8000
```

### Specifying Networks[¶](https://docs.ceph.com/en/pacific/cephadm/services/rgw/ "Permalink to this headline")

RGWサービスは、バインドするネットワークをyamlサービス仕様で構成することができます。

スペックファイルの例

```
service_type: rgw
service_name: foo
placement:
  label: rgw
  count-per-host: 2
networks:
- 192.169.142.0/24
spec:
  port: 8000
```

### Multisite zones[¶](https://docs.ceph.com/en/pacific/cephadm/services/rgw/ "Permalink to this headline")

マルチサイトのmyorgレルムとus\-east\-1ゾーンを提供するRGWをmyhost1とmyhost2に配備する。

```
# ceph orch apply rgw east --realm=myorg --zone=us-east-1 --placement="2 myhost1 myhost2"

```

マルチサイトの状況では、cephadmはデーモンを展開するだけであることに注意してください。 レルムやゾーンの設定の作成や更新は行いません。 新しいレルムとゾーンを作成するには、次のようにする必要があります。

```
# radosgw-admin realm create --rgw-realm=<realm-name> --default
```

```
# radosgw-admin zonegroup create --rgw-zonegroup=<zonegroup-name>  --master --default
```

```
# radosgw-admin zone create --rgw-zonegroup=<zonegroup-name> --rgw-zone=<zone-name> --master --default
```

```
# radosgw-admin period update --rgw-realm=<realm-name> --commit
```

配置指定の詳細については「[Daemon Placement](https://docs.ceph.com/en/pacific/cephadm/services/rgw/)」をご覧ください。 マルチサイトRGWの設定については「[Multi\-Site](https://docs.ceph.com/en/pacific/cephadm/services/rgw/)」をご覧ください。[228: Multi\-Site](228-Multi-Site.md)

### Setting up HTTPS[¶](https://docs.ceph.com/en/pacific/cephadm/services/rgw/ "Permalink to this headline")

RGWのサービスでHTTPSを有効にするには、以下のスキームでspecファイルを適用します。

```
service_type: rgw
service_id: myrgw
spec:
  rgw_frontend_ssl_certificate: |
    -----BEGIN PRIVATE KEY-----
    V2VyIGRhcyBsaWVzdCBpc3QgZG9vZi4gTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFt
    ZXQsIGNvbnNldGV0dXIgc2FkaXBzY2luZyBlbGl0ciwgc2VkIGRpYW0gbm9udW15
    IGVpcm1vZCB0ZW1wb3IgaW52aWR1bnQgdXQgbGFib3JlIGV0IGRvbG9yZSBtYWdu
    YSBhbGlxdXlhbSBlcmF0LCBzZWQgZGlhbSB2b2x1cHR1YS4gQXQgdmVybyBlb3Mg
    ZXQgYWNjdXNhbSBldCBqdXN0byBkdW8=
    -----END PRIVATE KEY-----
    -----BEGIN CERTIFICATE-----
    V2VyIGRhcyBsaWVzdCBpc3QgZG9vZi4gTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFt
    ZXQsIGNvbnNldGV0dXIgc2FkaXBzY2luZyBlbGl0ciwgc2VkIGRpYW0gbm9udW15
    IGVpcm1vZCB0ZW1wb3IgaW52aWR1bnQgdXQgbGFib3JlIGV0IGRvbG9yZSBtYWdu
    YSBhbGlxdXlhbSBlcmF0LCBzZWQgZGlhbSB2b2x1cHR1YS4gQXQgdmVybyBlb3Mg
    ZXQgYWNjdXNhbSBldCBqdXN0byBkdW8=
    -----END CERTIFICATE-----
  ssl: true
```

そして、このyamlドキュメントを適用します。

```
# ceph orch apply -i myrgw.yaml
```

rgw\_frontend\_ssl\_certificateの値は、改行文字を含まない｜文字で示されるリテラル文字列であることに注意してください。

### Service specification[¶](https://docs.ceph.com/en/pacific/cephadm/services/rgw/ "Permalink to this headline")

```
classceph.deployment.service_spec.RGWSpec(service_type='rgw', service_id=None, placement=None, rgw_realm=None, rgw_zone=None, rgw_frontend_port=None, rgw_frontend_ssl_certificate=None, rgw_frontend_type=None, unmanaged=False, ssl=False, preview_only=False, config=None, networks=None, subcluster=None)¶
```

\(マルチサイト\)Ceph RGWを構成するための設定

```
service_type: rgw
service_id: myrealm.myzone
spec:
    rgw_realm: myrealm
    rgw_zone: myzone
    ssl: true
    rgw_frontend_port: 1234
    rgw_frontend_type: beast
    rgw_frontend_ssl_certificate: ...
```

参照: [Service Specification](https://docs.ceph.com/en/pacific/cephadm/services/rgw/) [24: Service Management](24-Service_Management.md)

```
rgw_frontend_port:Optional[int]¶
```

RGWデーモンのポート番号

```
rgw_frontend_ssl_certificate:Optional[List[str]]¶
```

SSL証明書のリスト

```
rgw_frontend_type:Optional[str]¶
```

civetwebまたはbeast（デフォルト：beast）。参照 [HTTP Frontends](https://docs.ceph.com/en/pacific/cephadm/services/rgw/) [226: HTTP Frontends](226-HTTP_Frontends.md)

```
rgw_realm:Optional[str]¶
```

このサービスに関連するRGWレルム。手動で作成する必要があります。

```
rgw_zone:Optional[str]¶
```

このサービスに関連するRGWゾーン。手動で作成する必要があります。

```
ssl¶
```

SSLを有効にする

## High availability service for RGW[¶](https://docs.ceph.com/en/pacific/cephadm/services/rgw/ "Permalink to this headline")

ingressサービスでは、最小限の構成オプションでRGWの高可用性エンドポイントを作成することができます。 オーケストレーターは、haproxyとkeepalivedの組み合わせを展開・管理し、フローティング仮想IP上でロードバランシングを行います。

SSL を使用する場合、SSL は RGW 自体ではなく、ingress サービスで構成および終了させる必要があります。
![image.png](image/image.png)

イングレスサービスが導入されているホストはN台あります。 各ホストには haproxy デーモンと keepalived デーモンがあります。 これらのホストのうち、一度に1つのホストだけにバーチャルIPが自動的に設定されます。

各keepalivedデーモンは、同じホスト上のhaproxyデーモンが応答しているかどうかを数秒ごとにチェックします。 keepalivedは、マスターのkeepalivedデーモンが問題なく動作しているかどうかもチェックします。 マスター」となるkeepalivedデーモンやアクティブなハプロキシーが応答しない場合、バックアップモードで動作している残りのkeepalivedデーモンの1つがマスターに選出され、仮想IPはそのノードに移動されます。

active haproxyはロードバランサーのような役割を果たし、すべてのRGWリクエストを利用可能なすべてのRGWデーモンに分配します。

### Prerequisites[¶](https://docs.ceph.com/en/pacific/cephadm/services/rgw/ "Permalink to this headline")

* SSLを使用していない既存のRGWサービスです。 \(SSLサービスが必要な場合、証明書はRGWサービスではなく、ingressサービスで構成する必要があります）。

### Deploying[¶](https://docs.ceph.com/en/pacific/cephadm/services/rgw/ "Permalink to this headline")

以下のコマンドを使用します：

```
ceph orch apply -i <ingress_spec_file>
```

### Service specification[¶](https://docs.ceph.com/en/pacific/cephadm/services/rgw/ "Permalink to this headline")

yaml形式のファイルで、以下のプロパティを持ちます。

```
service_type: ingress
service_id: rgw.something    # adjust to match your existing RGW service
placement:
  hosts:
    - host1
    - host2
    - host3
spec:
  backend_service: rgw.something      # adjust to match your existing RGW service
  virtual_ip: <string>/<string>       # ex: 192.168.20.1/24
  frontend_port: <integer>            # ex: 8080
  monitor_port: <integer>             # ex: 1967, used by haproxy for load balancer status
  virtual_interface_networks: [ ... ] # optional: list of CIDR networks
  ssl_cert: |                         # optional: SSL certificate and key
    -----BEGIN CERTIFICATE-----
    ...
    -----END CERTIFICATE-----
    -----BEGIN PRIVATE KEY-----
    ...
    -----END PRIVATE KEY-----

```

where the properties of this service specification are:

* 必須、かつ "ingress "に設定
    service\_type
* サービスの名前です。 ingressを制御するサービスにちなんだ名前にすることをお勧めします（例：rgw.foo）。
    service\_id
* HA デーモンを動作させたいホストです。これらのホストには、haproxyとkeepalivedコンテナが配置されます。 これらのホストは、RGWが導入されているノードと一致する必要はありません。
    placementhosts
* イングレスサービスが利用可能となるCIDR形式の仮想IP（およびネットワーク）。
    virtual\_ip
* バーチャルIPに使用するイーサーネットインターフェースを特定するためのネットワークのリストです。
    virtual\_interface\_networks
* イングレスサービスへのアクセスに使用されるポートです。
    frontend\_port
* SSLを有効にする場合は、SSL証明書が必要です。この証明書には、証明書と秘密鍵の両方のブロックが.pem形式で含まれている必要があります。
    ssl\_cert:

### Selecting ethernet interfaces for the virtual IP[¶](https://docs.ceph.com/en/pacific/cephadm/services/rgw/ "Permalink to this headline")

インターフェイス名はホスト間\(またはリブート\)で異なる傾向があるため、仮想IPを設定するネットワークインターフェイスの名前を単純に指定することはできません。 代わりに、cephadmは、すでに設定されている他の既存のIPアドレスに基づいてインタフェースを選択します。

通常、仮想IPは、同じサブネット内に既存のIPを持つ最初のネットワークインタフェースに設定されます。 たとえば、仮想IPが192.168.0.80/24で、eth2が192.168.0.40/24の固定IPを持つ場合、cephadmはeth2を使用します。

場合によっては、仮想IPが既存の静的IPと同じサブネットに属していないことがあります。 このような場合、既存のIPと照合するサブネットのリストを指定すると、cephadmは最初に一致したネットワークインタフェースに仮想IPを配置します。 たとえば、仮想IPが192.168.0.80/24で、マシンの静的IPである10.10.0.0/16と同じインターフェイスにしたい場合は、次のような仕様になります。

```
service_type: ingress
service_id: rgw.something
spec:
  virtual_ip: 192.168.0.80/24
  virtual_interface_networks:
    - 10.10.0.0/16
  ...
```

この方法では、既存のIPアドレスを持たないインターフェイスに仮想IPを設定することができません。 このような場合には、正しいインターフェイスに「ダミー」のIPアドレスをルーティング不能なネットワークとして設定し、そのダミーのネットワークをネットワークリストで参照することをお勧めします（上記参照）。

### Useful hints for ingress[¶](https://docs.ceph.com/en/pacific/cephadm/services/rgw/ "Permalink to this headline")

* RGWデーモンは最低でも3台あると良いでしょう。
* イングレスサービスには最低3台のホストを推奨します。

## Further Reading[¶](https://docs.ceph.com/en/pacific/cephadm/services/rgw/ "Permalink to this headline")

* [Ceph Object Gateway](https://docs.ceph.com/en/pacific/cephadm/services/rgw/)
