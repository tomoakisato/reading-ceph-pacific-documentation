# 211: Monitoring Ceph iSCSI gateways

**クリップソース:** [211: Monitoring Ceph iSCSI gateways — Ceph Documentation](https://docs.ceph.com/en/pacific/rbd/iscsi-monitoring/)

# Monitoring Ceph iSCSI gateways[¶](https://docs.ceph.com/en/pacific/rbd/iscsi-monitoring/ "Permalink to this headline")

Cephは、iSCSIゲートウェイ環境向けに、エクスポートされたRADOS Block Device \(RBD\) イメージのパフォーマンスを監視するツールを提供します。

gwtopツールは、iSCSIでクライアントにエクスポートされるRBDイメージのパフォーマンスメトリクスを集計して表示するtopのようなツールです。メトリクスは、パフォーマンスメトリクスドメインエージェント\(PMDA\)から供給されます。Linux\-IO target \(LIO\) PMDAからの情報は、エクスポートされた各RBDイメージ、接続されたクライアント、関連するI/Oメトリクスをリストアップするために使用されます。

**Requirements:**

* 稼働中のCeph iSCSIゲートウェイ

**Installing:**

1. rootとして、各iSCSIゲートウェイノード上にceph\-iscsi\-toolsパッケージをインストールします：

```
# yum install ceph-iscsi-tools
```

1. rootとして、各iSCSIゲートウェイノードにperformance co\-pilot パッケージをインストールします：

```
# yum install pc
```

1. rootとして、各iSCSIゲートウェイノードにLIO PMDAパッケージをインストールします：

```
# yum install pcp-pmda-lio
```

1. rootとして、各iSCSIゲートウェイノードでperformance co\-pilot サービスを有効化し、開始します：

```
# systemctl enable pmcd
# systemctl start pmcd
```

1. rootで、pcp\-pmda\-lioエージェントを登録します：

```
cd /var/lib/pcp/pmdas/lio
./Install
```

デフォルトでは、gwtop は iSCSI ゲートウェイの設定オブジェクトが rbd プールの gateway.conf という RADOS オブジェクトに格納されていると仮定しています。この構成は、性能統計を収集するためにコンタクトする iSCSI ゲートウェイを定義します。これは、\-g または \-c フラグのいずれかを使用して上書きすることができます。詳細については、gwtop \-\-help を参照してください。

LIOの構成は、パフォーマンスco\-pilotから抽出するパフォーマンス統計の種類を決定します。gwtop は起動時に LIO 構成を調べ、ユーザースペースのディスクが見つかれば、自動的に LIO コレクターを選択します。

**Example \`\`gwtop\`\` Outputs**

```
gwtop  2/2 Gateways   CPU% MIN:  4 MAX:  5    Network Total In:    2M  Out:    3M   10:20:00
Capacity:   8G    Disks:   8   IOPS:  503   Clients:  1   Ceph: HEALTH_OK          OSDs:   3
Pool.Image       Src    Size     iops     rMB/s     wMB/s   Client
iscsi.t1703             500M        0      0.00      0.00
iscsi.testme1           500M        0      0.00      0.00
iscsi.testme2           500M        0      0.00      0.00
iscsi.testme3           500M        0      0.00      0.00
iscsi.testme5           500M        0      0.00      0.00
rbd.myhost_1      T       4G      504      1.95      0.00   rh460p(CON)
rbd.test_2                1G        0      0.00      0.00
rbd.testme              500M        0      0.00      0.00

```

Client列の\(CON\)は、iSCSIイニシエータ（クライアント）が現在iSCSIゲートウェイにログインしていることを意味します。\-multi\- が表示されている場合、複数のクライアントが1つのRBDイメージにマップされています。
