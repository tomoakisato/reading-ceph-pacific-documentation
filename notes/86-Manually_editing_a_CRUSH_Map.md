# 86: Manually editing a CRUSH Map

**クリップソース:** [86: Manually editing a CRUSH Map — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/operations/crush-map-edits/)

# Manually editing a CRUSH Map[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map-edits/ "Permalink to this headline")

CRUSHマップの手動編集は、上級管理者向けの操作です。 圧倒的多数のインストールに必要なCRUSHの変更はすべて標準のceph CLIで可能であり、CRUSHマップを手動で編集する必要はありません。 最近のCephリリースで手動編集が必要なユースケースを確認した場合は、Ceph開発者に連絡して、今後のCephバージョンでそのコーナーケースを回避できるようにすることを検討してください。

既存のCRUSHマップを編集する場合

1. [Get the CRUSH map](https://docs.ceph.com/en/pacific/rados/operations/crush-map-edits/)
2. [Decompile](https://docs.ceph.com/en/pacific/rados/operations/crush-map-edits/)
3. Edit at least one of 
4. [Recompile](https://docs.ceph.com/en/pacific/rados/operations/crush-map-edits/)
5. [Set the CRUSH map](https://docs.ceph.com/en/pacific/rados/operations/crush-map-edits/)

特定のプールに対するCRUSHマップルールの設定の詳細については、「[Set Pool Values](https://docs.ceph.com/en/pacific/rados/operations/crush-map-edits/)」を参照してください。

## Get a CRUSH Map[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map-edits/ "Permalink to this headline")

クラスタのCRUSHマップを取得するには：

```
ceph osd getcrushmap -o {compiled-crushmap-filename}
```

Cephは、コンパイルされたCRUSHマップを指定したファイル名に出力\(\-o\)します。CRUSHマップはコンパイルされた形式であるため、編集する前にまずデコンパイルする必要があります。

## Decompile a CRUSH Map[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map-edits/ "Permalink to this headline")

CRUSHマップをデコンパイルするには：

```
crushtool -d {compiled-crushmap-filename} -o {decompiled-crushmap-filename}
```

## Recompile a CRUSH Map[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map-edits/ "Permalink to this headline")

CRUSHマップをコンパイルするには：

```
crushtool -c {decompiled-crushmap-filename} -o {compiled-crushmap-filename}
```

## Set the CRUSH Map[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map-edits/ "Permalink to this headline")

クラスタのCRUSHマップを設定するには：

```
ceph osd setcrushmap -i {compiled-crushmap-filename}
```

Cephは指定されたファイル名からコンパイルされたCRUSHマップをロード\(\-i\)します。

## Sections[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map-edits/ "Permalink to this headline")

CRUSHマップには、大きく分けて6つのセクションがあります。

1. **tunables:**
2. **devices:**
3. **types**
4. **buckets:**
5. **rules:**
6. **choose\_args:**

## CRUSH Map Devices[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map-edits/ "Permalink to this headline")

デバイスとは、データを保存する個々のOSDのことです。 通常、クラスタ内の各 OSD デーモンに対してここで 1 つ定義されます。 デバイスは id \(非負の整数\) と名前、通常は osd.N \(N はデバイス ID\) で識別されます。

デバイスは、デバイスクラス（例えば、hddやssd）を関連付け、クラッシュルールの対象にすることができます。

```
# devices
device {num} {osd.name} [class {class}]
```

For example:

```
# devices
device 0 osd.0 class ssd
device 1 osd.1 class hdd
device 2 osd.2
device 3 osd.3
```

ほとんどの場合、各デバイスは1つのceph\-osdデーモンにマップされます。 これは通常、単一のストレージデバイス、デバイスのペア\(例えば、1つはデータ用、もう1つはジャーナルやメタデータ用\)、場合によっては小さなRAIDデバイスです。

## CRUSH Map Bucket Types[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map-edits/ "Permalink to this headline")

CRUSHマップの2番目のリストは、「バケット」のタイプを定義しています。バケットは、ノードとリーフの階層を容易にする。ノード（あるいはリーフでない）バケットは、一般に階層内の物理的な位置を表します。ノードは他のノードやリーフを集約します。リーフバケットはceph\-osdデーモンとそれに対応するストレージメディアを表します。

補足：CRUSHの文脈で使われる "バケット "という用語は、階層のノード、すなわち場所や物理的なハードウェアの一部という意味です。RADOS Gateway APIの文脈で使用される "バケット "とは異なる概念です。

バケットタイプをCRUSHマップに追加するには、バケットタイプ一覧の下に新しい行を作成します。type の後に、ユニークな数値の ID とバケツ名を入力します。慣習上、リーフバケットは1つで、それはtype0です。しかし、あなたはそれに好きな名前（例えば、osd、disk、drive、storageなど）をつけることができます。

```
#types
type {num} {bucket-name}
```

For example:

```
# types
type 0 osd
type 1 host
type 2 chassis
type 3 rack
type 4 row
type 5 pdu
type 6 pod
type 7 room
type 8 datacenter
type 9 zone
type 10 region
type 11 root
```

## CRUSH Map Bucket Hierarchy[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map-edits/ "Permalink to this headline")

CRUSHアルゴリズムは、一様確率分布に近似したデバイスごとの重み値に従って、データオブジェクトをストレージデバイスに分散させます。CRUSHは、定義した階層的なクラスタマップに従ってオブジェクトとそのレプリカを分散させます。CRUSHマップは、利用可能なストレージデバイスとそれを含む論理的要素を表します。

障害ドメインをまたがるPGを OSD にマッピングするために、CRUSH マップはバケットタイプの階層的なリスト（生成された CRUSH マップの \#types の下にある）を定義します。バケット階層を作成する目的は、ホスト、シャーシ、ラック、配電ユニット、ポッド、列、部屋、データセンターなどの障害ドメインごとにリーフノードを分離することです。OSD を表すリーフノード以外の階層は任意であり、ニーズに応じて定義することができます。

CRUSHマップをファームのハードウェア命名規則に合わせて作成し、物理的なハードウェアを反映したインスタンス名を使用することをお勧めします。この命名規則により、クラスタの管理が容易になり、OSDやその他のハードウェアが故障して管理者が物理的なハードウェアにアクセスする必要がある場合のトラブルシューティングが容易になります。

以下の例では、バケット階層にosdというリーフバケットと、それぞれhostとrackという2つのノードバケットを設定しています。

上位のrackバケットタイプは下位のhostバケットタイプを集約します。

リーフノードには、CRUSH マップ冒頭の \#devices リストで宣言されたストレージデバイスが反映されるため、バケットインスタンスとして宣言する必要はありません。階層で2番目に低いバケットタイプは、通常、デバイスを集約します（つまり、通常はストレージメディアを含むコンピュータであり、「ノード」「コンピュータ」「サーバー」「ホスト」「マシン」など、お好きな言葉で表現してください）。高密度環境では、シャーシごとに複数のホスト／ノードを見ることが一般的になっています。シャーシの障害も考慮する必要があります。たとえば、ノードに障害が発生した場合にシャーシを引き抜く必要があるため、多数のホスト／ノードとその OSD が停止する可能性があります。

Bucket のインスタンスを宣言する際には、その型、一意な名前（文字列）、負の整数で表される一意な ID（オプション）、アイテムの総容量/能力に対する重みの指定、バケットアルゴリズム（通常 straw2）、ハッシュ（通常 0、ハッシュアルゴリズム rjenkins1 を反映）の指定が必要です。バケットは1つ以上のアイテムを持つことができます。アイテムはノードバケットやリーフから構成されることもあります。アイテムは、その相対的な重さを反映した重みを持つことができます。

ノードバケットの宣言は、以下の構文で行うことができます。

```
[bucket-type] [bucket-name] {
        id [a unique negative numeric ID]
        weight [the relative capacity/capability of the item(s)]
        alg [the bucket type: uniform | list | tree | straw | straw2 ]
        hash [the hash type: 0 by default]
        item [item-name] weight [weight]
}
```

例えば、上図の例では、2つのhostバケットと1つのrackバケットを定義することになります。OSD は、hostバケット内のアイテムとして宣言されています。

```
host node1 {
        id -1
        alg straw2
        hash 0
        item osd.0 weight 1.00
        item osd.1 weight 1.00
}

host node2 {
        id -2
        alg straw2
        hash 0
        item osd.2 weight 1.00
        item osd.3 weight 1.00
}

rack rack1 {
        id -3
        alg straw2
        hash 0
        item node1 weight 2.00
        item node2 weight 2.00
}
```

注意：上記の例では、rackバケットにOSDが含まれていないことに注意してください。むしろ、下位のhostバケットを含み、それらのウエイトの合計をアイテムエントリーに含んでいることに注意してください。

**Bucket Types**

Cephは5つのバケットタイプをサポートしており、それぞれがパフォーマンスと再編成効率のトレードオフを表しています。どのバケットタイプを使用するか迷った場合は、straw2バケットを使用することをお勧めします。 バケットタイプの詳細については、CRUSH \- Controlled, Scalable, Decentralized Placement of Replicated Dataを参照し、より具体的にはセクション3.4を参照してください。バケットの種類は：

1. **uniform**
2. **list**
3. **tree**
4. **straw**
5. 例えば、アイテムAのウエイトを新たに追加したり、完全に削除したりする場合、アイテムAとの間でのみデータの移動が発生します。
    **straw2**

**Hash**

各バケットには、ハッシュアルゴリズムが使用されます。現在、Cephはrjenkins1をサポートしています。ハッシュの設定に0を入力すると、rjenkins1が選択されます。

**Weighting Bucket Items**

Cephはバケットの重みをdoubleで表現するため、細かい重み付けが可能です。ウェイトとは、デバイスの容量間の相対的な差のことです。1TBのストレージデバイスの相対的な重みとして、1.00を使用することをお勧めします。この場合、0.5の重みは約500GBを表し、3.00の重みは約3TBを表します。上位のバケットには、そのバケットに集約されたリーフアイテムの合計が重みとして設定されています。

バケットのアイテム重量は一次元的なものですが、ストレージ・ドライブの性能を反映してアイテム重量を計算することもできます。例えば、多数の1TBドライブがあり、一部のドライブはデータ転送速度が比較的低く、他のドライブはデータ転送速度が比較的高い場合、同じ容量であっても異なる重みを付けることができます（例えば、総スループットが低い最初のドライブのセットには0.80、総スループットが高い2番目のドライブのセットには1.20という重みがあります）。

## CRUSH Map Rules[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map-edits/ "Permalink to this headline")

CRUSHマップは、プールのデータ配置を決定するルールである'CRUSHルール'という概念をサポートしています。デフォルトのCRUSHマップは、各プールに対してルールを持ちます。大規模なクラスタでは、おそらく多くのプールを作成し、各プールはそれ自身のデフォルトでないCRUSHルールを持つかもしれません。

注意：ほとんどの場合、デフォルトのルールを変更する必要はありません。新しいプールを作成すると、デフォルトでルールが0に設定されます。

CRUSHルールとは、CRUSHがオブジェクトレプリカをどのように配置するかを正確に指定するための配置・複製ストラテジー、またはディストリビューションポリシーを定義するものです。例えば、2つのターゲットを選択して2ウェイミラーリングを行うルール、2つの異なるデータセンターにある3つのターゲットを選択して3ウェイミラーリングを行うルール、さらに6つのストレージデバイス上で消去符号化を行うルールなどを作成することができます。CRUSHルールの詳細については、[CRUSH \- Controlled, Scalable, Decentralized Placement of Replicated Data](https://docs.ceph.com/en/pacific/rados/operations/crush-map-edits/)特にセクション3.2をご参照ください。

ルールは次のような形式をとります。

```
rule <rulename> {

        id [a unique whole numeric ID]
        type [ replicated | erasure ]
        min_size <min-size>
        max_size <max-size>
        step take <bucket-name> [class <device-class>]
        step [choose|chooseleaf] [firstn|indep] <N> type <bucket-type>
        step emit
}
```

**id**

Description

ルールを識別するための一意の整数値

Purpose

ルールマスクの構成要素

Type

Integer

Required

Yes

Default

0

**type**

Description

ストレージドライブ（replicated）またはRAIDのいずれかのルールを記述します

Purpose

ルールマスクの構成要素

Type

String

Required

Yes

Default

replicated

Valid Values

現状ではreplicatedとerasureのみ

**min\_size**

Description

プールが作成するレプリカの数がこの数より少ない場合、CRUSHはこのルールを選択しません。

Type

Integer

Purpose

ルールマスクの構成要素

Required

Yes

Default

1

**max\_size**

Description

プールがこの数より多くのレプリカを作成する場合、CRUSHはこのルールを選択しません。

Type

Integer

Purpose

ルールマスクの構成要素

Required

Yes

Default

10

**step take \<bucket\-name\> \[class \<device\-class\>\]**

Description

バケット名を受け取り、ツリーの下への反復を開始します。デバイスクラスが指定された場合、それはデバイスを定義する際に以前に使用されたクラスと一致する必要があります。そのクラスに属さないデバイスはすべて除外されます。

Purpose

ルールの構成要素

Required

Yes

Example

step take data

**step choose firstn {num} type {bucket\-type}**

Description

現在のバケットの中から、指定された種類のバケットの数を選択します。この数は通常、プール内のレプリカの数（つまりプールサイズ）です。

* {num}=0の場合、pool\-num\-replicasのバケットを選択（すべて利用可能）
* {num}\>0&&\<pool\-num\-replicas の場合、その数だけバケットを選択する
* {num}\<0の場合、pool\-num\-replicas\-{num}を意味する

Purpose

ルールの構成要素

Prerequisite

step takeまたはstep chooseに準ずる。

Example

step choose firstn 1 type row

**step choose leaf firstn {num} type {bucket\-type}**

Description

{bucket\-type}のバケット集合を選択し、バケット集合の各バケットのサブツリーからリーフノード（つまりOSD）を選択します。セット内のバケット数は、通常、プール内のレプリカ数（＝プールサイズ）です。

* {num}=0の場合、pool\-num\-replicasのバケットを選択（すべて利用可能）
* {num}\>0&&\<pool\-num\-replicas ならば、その数だけバケットを選択する
* {num}\<0の場合、pool\-num\-replicas\-{num}を意味する

Purpose

ルールの構成要素。使用法では、2つのステップを使用してデバイスを選択する必要性を排除します。

Prerequisite

step takeまたはstep chooseに準ずる。

Example

step choose leaf firstn0 type row

**step emit**

Description

現在値を出力し、スタックを空にします。通常、ルールの最後に使用されますが、同じルール内の異なるツリーから選択するために使用されることもあります。

Purpose

ルールの構成要素

Prerequisite

step chooseに準ずる。

Example

step emit

重要：1つのCRUSHルールを複数のプールに割り当てることは可能ですが、1つのプールに複数のCRUSHルールを割り当てることはできません。

**firstn versus indep**

Description

CRUSH マップでアイテム \(OSD\) がマークdownされたときに CRUSH が使用する置換戦略を制御します。このルールがreplicatedプールで使用される場合は firstn に、ECプールで使用される場合は indep にする必要があります。

その理由は、以前に選択したデバイスが故障したときの動作に関係しています。例えば、OSD1, 2, 3, 4, 5にPGを保存しているとします。そして、3が故障したとします。

firstn "モードでは、CRUSHは単に1と2を選択するように計算を調整し、次に3を選択しますが、それがダウンしていることを発見し、再試行して4と5を選択し、さらに新しいOSD 6を選択します。 つまり、CRUSHの最後のマッピング変更は1、2、3、4、5 → 1、2、4、5、6です。

ただし、ECプールを保存している場合は、OSD 4、5、および6にマップされたデータを変更しただけです。 したがって、「indep」モードはそれを行わないようにします。 代わりに、失敗したOSD 3を選択したときに、6を再試行して選択し、最終的な変換が1、2、3、4、5\-\> 1、2、6、4、5になることを期待できます。

## Migrating from a legacy SSD rule to device classes[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map-edits/ "Permalink to this headline")

従来は、特殊なデバイス（SSDなど）に適用するルールを記述するために、CRUSHマップを手動で編集し、デバイスの種類ごとに並列階層を維持する必要がありました。 Luminousのリリース以降、デバイスクラス機能により透過的にこれを実現することができるようになりました。

しかし、既存の手動でカスタマイズしたデバイス単位のマップから、trivialな方法で新しいデバイスクラスのルールに移行すると、システム内のすべてのデータが入れ替わることになります。

crushtoolには、レガシールールや階層を変換して、新しいクラスベースのルールを使い始めることができるようにするコマンドがいくつか用意されています。可能な変換は3種類あります：

1. \-\-reclassify\-root \<root\-name\> \<device\-class\>

これは、root\-name以下の階層にあるすべてのものを取り込み、take\<root\-name\>を介してそのルートを参照しているルールを、代わりにtake\<root\-name\>class\<device\-class\>に調整するものです。これは、古い ID が指定されたクラスの "シャドウツリー" に使われるようにバケットの番号を変更し、データの移動が起きないようにします。

例えば、次のような既存のルールがあるとします。

```
rule replicated_ruleset {
   id 0
   type replicated
   min_size 1
   max_size 10
   step take default
   step chooseleaf firstn 0 type rack
   step emit
}
```

ルートデフォルトをクラスhddに再分類すると、ルールは次のようになります。

```
rule replicated_ruleset {
   id 0
   type replicated
   min_size 1
   max_size 10
   step take default class hdd
   step chooseleaf firstn 0 type rack
   step emit
}
```

1. \-\-set\-subtree\-class \<bucket\-name\> \<device\-class\>

Bucket\-name をルートとするサブツリー内のすべてのデバイスに、指定されたデバイスクラスでマークを付けます。

これは通常、\-\-reclassify\-rootオプションと一緒に使用し、そのルート内のすべてのデバイスが正しいクラスでラベル付けされるようにします。 しかし、状況によっては、それらのデバイスのいくつかが \(正しく\) 異なるクラスを持っており、それらのラベルを付け直したくないことがあります。 このような場合、\-\-set\-subtree\-class オプションを除外することができます。 これは、以前のルールは複数のクラスのデバイスに分散していたが、調整後のルールは指定されたデバイスクラスのデバイスにのみマッピングされるため、再マッピング処理が完全ではないことを意味しますが、異常値デバイスの数が少ない場合は、しばしば受け入れられるレベルのデータ移動です。

1. \-\-reclassify\-bucket \<match\-pattern\> \<device\-class\> \<default\-parent\>

これにより、通常の階層に並列型固有の階層をマージすることができるようになります。 例えば、多くのユーザーは次のようなマップを持っています。

```
host node1 {
   id -2           # do not change unnecessarily
   # weight 109.152
   alg straw2
   hash 0  # rjenkins1
   item osd.0 weight 9.096
   item osd.1 weight 9.096
   item osd.2 weight 9.096
   item osd.3 weight 9.096
   item osd.4 weight 9.096
   item osd.5 weight 9.096
   ...
}

host node1-ssd {
   id -10          # do not change unnecessarily
   # weight 2.000
   alg straw2
   hash 0  # rjenkins1
   item osd.80 weight 2.000
   ...
}

root default {
   id -1           # do not change unnecessarily
   alg straw2
   hash 0  # rjenkins1
   item node1 weight 110.967
   ...
}

root ssd {
   id -18          # do not change unnecessarily
   # weight 16.000
   alg straw2
   hash 0  # rjenkins1
   item node1-ssd weight 2.000
   ...
}
```

この関数は、あるパターンにマッチする各バケットを再分類します。 パターンは、%suffix や prefix% のようなものです。たとえば、上記の例では、%\-ssd というパターンを使用します。 マッチした各バケットについて、名前の残りの部分（% ワイルドカードにマッチする部分）は、ベースバケットを指定します。マッチしたバケット内のすべてのデバイスは、指定されたデバイスクラスでラベル付けされ、ベースバケットに移動されます。 ベースバケットが存在しない場合（例えば、node12\-ssd は存在するが node12 は存在しない）、指定されたデフォルト親バケットの下に作成されリンクされます。 いずれの場合も、データの移動を防ぐために、新しいシャドウバケットに対して古いバケットIDを保持するように注意しています。 古いバケットを参照するテイクステップを持つルールは、すべて調整されます。

1. \-\-reclassify\-bucket \<bucket\-name\> \<device\-class\> \<base\-bucket\>

同じコマンドは、ワイルドカードを使用せずに、単一のバケットをマッピングすることもできます。 例えば、先ほどの例では、ssd バケットをデフォルトバケットにマッピングしたいとします。

上記の断片からなるマップを変換する最終的なコマンドは次のようなものです。

```
$ ceph osd getcrushmap -o original
$ crushtool -i original --reclassify 
    --set-subtree-class default hdd 
    --reclassify-root default hdd 
    --reclassify-bucket %-ssd ssd default 
    --reclassify-bucket ssd ssd default 
    -o adjusted
```

変換が正しいことを確認するために、CRUSH マップへの入力の大きなサンプルをテストし、同じ結果が戻ってくることを確認する \-\-compare コマンドが用意されています。 これらの入力は、\-testコマンドに適用されるのと同じオプションで制御されます。 上記の例の場合：

```
$ crushtool -i original --compare adjusted
rule 0 had 0/10240 mismatched mappings (0)
rule 1 had 0/10240 mismatched mappings (0)
maps appear equivalent
```

もし違いがあれば、括弧の中にどのような比率で入力がリマップされているかがわかるはずです。

調整したマップに問題がなければ、次のような方法でクラスタに適用できます。

```
ceph osd setcrushmap -i adjusted
```

## Tuning CRUSH, the hard way[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map-edits/ "Permalink to this headline")

すべてのクライアントが最近のコードを実行していることが確認できれば、CRUSHマップを抽出して値を変更し、クラスタに再投入することで調整することができます。

* 最新のCRUSHマップを抽出します。

```
ceph osd getcrushmap -o /tmp/crush
```

* チューナブルを調整します。 これらの値は、私たちがテストした大規模クラスタと小規模クラスタの両方で最良の動作を提供するようです。 これが動作するためには、さらにcrushtoolに\-\-enable\-unsafe\-tunables引数を指定する必要があります。 このオプションは細心の注意を払って使用してください。

```
crushtool -i /tmp/crush --set-choose-local-tries 0 --set-choose-local-fallback-tries 0 --set-choose-total-tries 50 -o /tmp/crush.new
```

* 変更したマップを再投入します。

```
ceph osd setcrushmap -i /tmp/crush.new
```

## Legacy values[¶](https://docs.ceph.com/en/pacific/rados/operations/crush-map-edits/ "Permalink to this headline")

参考までに、CRUSHチューナブルのレガシーな値は、次のように設定することができます。

```
crushtool -i /tmp/crush --set-choose-local-tries 2 --set-choose-local-fallback-tries 5 --set-choose-total-tries 19 --set-chooseleaf-descend-once 0 --set-chooseleaf-vary-r 0 -o /tmp/crush.legacy
```

この場合も、特別な \-\-enable\-unsafe\-tunables オプションが必要です。さらに、上述のように、レガシーな値に戻した後に古いバージョンの ceph\-osd デーモンを実行するのは、機能ビットが完全に強制されないので、注意が必要です。
