# 88: Configure Monitor Election Strategies

**クリップソース:** [88: Configure Monitor Election Strategies — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/operations/change-mon-elections/)

# Configure Monitor Election Strategies[¶](https://docs.ceph.com/en/pacific/rados/operations/change-mon-elections/ "Permalink to this headline")

デフォルトでは、モニタはクラシックモードを使用します。よほど特殊な理由がない限り、このモードのままにしておくことをお勧めします。

クラスタを構築する前にモードを切り替えたい場合は、mon election default strategyオプションを変更します。このオプションは整数値です。

* 1 for “classic”
* 2 for “disallow”
* 3 for “connectivity”

クラスタが稼動したら、次のコマンドを実行してストラテジを変更できます。

```
$ ceph mon set election_strategy {classic|disallow|connectivity}
```

## Choosing a mode[¶](https://docs.ceph.com/en/pacific/rados/operations/change-mon-elections/ "Permalink to this headline")

クラシック以外のモードは、異なる機能を提供します。クラシックモードは最もシンプルなモードなので、余計な機能が必要ない場合は、クラシックモードのまま使用することをお勧めします。

## The disallow Mode[¶](https://docs.ceph.com/en/pacific/rados/operations/change-mon-elections/ "Permalink to this headline")

この場合、モニタはクォーラムに参加し、クライアントにサービスを提供しますが、リーダーに選出されることはありません。このモードは、クライアントから遠くにいることが分かっているモニターがある場合に使用するとよいでしょう。リーダーを不許可にするには、次のコマンドを実行します。

```
$ ceph mon add disallowed_leader {name}
```

モニタを不許可リストから削除し、再びリーダーになることを許可するには、次のように実行します。

```
$ ceph mon rm disallowed_leader {name}
```

実行すると、disallowed\_leadersのリストが含まれます。

```
$ ceph mon dump
```

## The connectivity Mode[¶](https://docs.ceph.com/en/pacific/rados/operations/change-mon-elections/ "Permalink to this headline")

このモードでは、各モニタが提供するピアへのconectionスコアを評価し、最も高いスコアを持つモニタを選択します。このモードは、クラスタが複数のデータセンタにまたがっている場合、またはネットワークトポロジーが不均衡な場合に発生する可能性のある、ネットワークパーティショニングやネットスプリットを処理するように設計されています。

このモードは、上記のdisallowと同じコマンドを使用して、モニタをリーダーにすることを禁止することもサポートしています。

## Examining connectivity scores[¶](https://docs.ceph.com/en/pacific/rados/operations/change-mon-elections/ "Permalink to this headline")

モニタは、connectivity選出モードでない場合でも、connectionスコアを保持しています。モニタが持っているスコアを調べるには、以下を実行します。

```
ceph daemon mon.{name} connection scores dump
```

個々のconnectionスコアは0～1まであり、接続が生きているか死んでいるか（タイムアウト時間内に最新のPingを返したかどうかで判断）も含まれます。

これは想定外の出来事ですが、何らかの理由でトラブルが発生し、トラブルシューティングによってスコアが無効になったと思われる場合、履歴をリセットすることができます。

```
ceph daemon mon.{name} connection scores reset
```

スコアのリセットはリスクが低いですが（モニタは接続が生きているか死んでいるかを素早く判断し、正確であれば以前のスコアにトレンドバックします！）、サポートチームや開発者から要求されない限り、必要ないはずですし、推奨もされません。
