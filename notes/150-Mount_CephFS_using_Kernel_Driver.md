# 150: Mount CephFS using Kernel Driver

**クリップソース:** [150: Mount CephFS using Kernel Driver — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/mount-using-kernel-driver/)

# Mount CephFS using Kernel Driver[¶](https://docs.ceph.com/en/pacific/cephfs/mount-using-kernel-driver/ "Permalink to this headline")

CephFSカーネルドライバは、Linuxカーネルに含まれています。CephFSを通常のファイルシステムとして、ネイティブなカーネル性能でマウントすることができます。ほとんどのユースケースで選ばれているクライアントです。

## Prerequisites[¶](https://docs.ceph.com/en/pacific/cephfs/mount-using-kernel-driver/ "Permalink to this headline")

### Complete General Prerequisites[¶](https://docs.ceph.com/en/pacific/cephfs/mount-using-kernel-driver/ "Permalink to this headline")

カーネルマウントとFUSEマウントの両方で必要な前提条件は、[Mount CephFS: Prerequisites](https://docs.ceph.com/en/pacific/cephfs/mount-prerequisites)のページで確認してください。

### Is mount helper is present?[¶](https://docs.ceph.com/en/pacific/cephfs/mount-using-kernel-driver/ "Permalink to this headline")

mount.cephヘルパーはCephパッケージによってインストールされます。このヘルパーはモニタアドレスとCephXユーザキーリングを自動的に渡すため、Ceph管理者はCephFSをマウントする際にこれらの情報を明示的に渡す手間が省けます。クライアントマシンにヘルパーが存在しない場合でも、カーネルを使用してCephFSをマウントすることはできますが、マウントコマンドにこれらの詳細を明示的に渡すことになります。システム上にヘルパーが存在するかどうかを確認するには、以下を実行します：

```
stat /sbin/mount.ceph
```

### Which Kernel Version?[¶](https://docs.ceph.com/en/pacific/cephfs/mount-using-kernel-driver/ "Permalink to this headline")

カーネルクライアントは（cephのパッケージリリースの一部ではなく）Linuxカーネルの一部として配布されるため、クライアントノードで使用するカーネルバージョンを検討する必要があります。古いカーネルにはバグのあるcephクライアントが含まれていることが知られており、最近のCephクラスタがサポートする機能をサポートしていない可能性があります。

安定したLinuxディストリビューションの「最新」カーネルは、Cephの開発が行われる（バグフィックスを含む）最新のアップストリームLinuxカーネルよりも何年も遅れている可能性があることを覚えておいてください。

目安として、Ceph 10.x（Jewel）時点では、少なくとも4.xカーネルを使用する必要があります。どうしても古いカーネルを使用する必要がある場合は、カーネルクライアントではなく、FUSEクライアントを使用する必要があります。

CephFSサポートを含むLinuxディストリビューションを使用している場合、このアドバイスは適用されません。この場合、ディストリビューターが安定したカーネルへの修正をバックポートする責任があります。ベンダーに確認してください。

## Synopsis[¶](https://docs.ceph.com/en/pacific/cephfs/mount-using-kernel-driver/ "Permalink to this headline")

一般に、カーネルドライバ経由でCephFSをマウントするコマンドは次のようになります：

```
mount -t ceph {device-string}:{path-to-mounted} {mount-point} -o {key-value-args} {other-args}
```

## Mounting CephFS[¶](https://docs.ceph.com/en/pacific/cephfs/mount-using-kernel-driver/ "Permalink to this headline")

Cephクラスタでは、デフォルトでCephXが有効になっています。カーネルドライバでCephFSをマウントするには、mountコマンドを使用します。

```
mkdir /mnt/mycephfs
mount -t ceph :/ /mnt/mycephfs -o name=foo
```

オプション\-oの直後のkey\-value引数はCephX credentialで、nameはCephFSをマウントするために使用するCephXユーザのユーザ名です。nameのデフォルト値はguestです。

カーネルドライバは、MONのソケットとCephXユーザの秘密鍵も必要とします。上記のコマンドの場合、mount.cephヘルパーは、Ceph confファイルとkeyringを見つけて読み取ることで、これらの詳細を自動的に把握します。mountコマンドを実行しているホストにこれらのファイルがない場合、これらの情報を自分で渡すこともできます：

```
mount -t ceph 192.168.0.1:6789,192.168.0.2:6789:/ /mnt/mycephfs -o name=foo,secret=AQATSKdNGBnwLhAAnNDKnH65FmVKpXZJVasUeQ==
```

上記のコマンドでMONソケットを1つだけ渡すことも可能です。上記のコマンドの潜在的な問題は、シェルのコマンド履歴に秘密鍵が残ってしまうことです。これを防ぐには、秘密鍵をファイル内にコピーし、secretの代わりにsecretfileオプションを使用してそのファイルを渡すことができます：

```
mount -t ceph :/ /mnt/mycephfs -o name=foo,secretfile=/etc/ceph/foo.secret
```

秘密鍵ファイルのパーミッションが適切（600が良い）であることを確認します。

CephXが無効の場合、\-oとそれに続くkey\-value引数のリストを省略することができます。

```
mount -t ceph :/ /mnt/mycephfs
```

CephFSルートのサブツリーをマウントするには、デバイス文字列にそのパスを追加します：

```
mount -t ceph :/subvolume/dir1/dir2 /mnt/mycephfs -o name=fs
```

Cephクラスタに複数のファイルシステムがある場合、デフォルトでないFSを次のようにマウントします：

```
mount -t ceph :/ /mnt/mycephfs2 -o name=fs,fs=mycephfs2
```

## Unmounting CephFS[¶](https://docs.ceph.com/en/pacific/cephfs/mount-using-kernel-driver/ "Permalink to this headline")

Cephファイルシステムをアンマウントするには、通常通りumountコマンドを使用します：

```
umount /mnt/mycephfs
```

ヒント：このコマンドを実行する前に、ファイルシステムのディレクトリ内にいないことを確認してください。

## Persistent Mounts[¶](https://docs.ceph.com/en/pacific/cephfs/mount-using-kernel-driver/ "Permalink to this headline")

ファイルシステムテーブルにあるCephFSをカーネルドライバでマウントするには、/etc/fstabに以下を追加します：

```
[{ipaddress}:{port}]:/ {mount}/{mountpoint} ceph [name=username,secret=secretkey|secretfile=/path/to/secretfile],[{mount.options}]
```

For example:

```
:/     /mnt/ceph    ceph    name=admin,noatime,_netdev    0       2
```

name= パラメータのデフォルトは guest です。secret または secretfile オプションが指定されていない場合、 マウントヘルパーは設定されているキーリングの中から指定された名前の秘密を見つけようとします。

CephXのユーザ管理の詳細については「[ユーザ管理](https://docs.ceph.com/en/pacific/rados/operations/user-management/)」を、取得できるオプションについては「[mount.cephマニュアル](https://docs.ceph.com/en/pacific/man/8/mount.ceph/)」を参照してください。トラブルシューティングについては、[カーネルマウントデバッグ](https://docs.ceph.com/en/pacific/cephfs/troubleshooting/#kernel-mount-debugging)を参照してください。
