# 63: Cluster Operations

**クリップソース:** [63: Cluster Operations — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/operations/)

# Cluster Operations[¶](https://docs.ceph.com/en/pacific/rados/operations/ "Permalink to this headline")

### High\-level Operations

高レベルのクラスタ操作は、主にcephサービスによるクラスタの起動、停止、再起動、クラスタの健全性のチェック、および運用中のクラスタの監視で構成されています。

* [Operating a Cluster](https://docs.ceph.com/en/pacific/rados/operations/)
* [Health checks](https://docs.ceph.com/en/pacific/rados/operations/)
* [Monitoring a Cluster](https://docs.ceph.com/en/pacific/rados/operations/)
* [Monitoring OSDs and PGs](https://docs.ceph.com/en/pacific/rados/operations/)
* [User Management](https://docs.ceph.com/en/pacific/rados/operations/)
* [Repairing PG inconsistencies](https://docs.ceph.com/en/pacific/rados/operations/)

### Data Placement

クラスタが稼動したら、データの配置に取り掛かることができます。Cephはペタバイト規模のデータストレージクラスタをサポートしており、ストレージプールと配置グループがCephのCRUSHアルゴリズムを使ってクラスタ全体にデータを分散させます。

* [Data Placement Overview](https://docs.ceph.com/en/pacific/rados/operations/)
* [Pools](https://docs.ceph.com/en/pacific/rados/operations/)
* [Erasure code](https://docs.ceph.com/en/pacific/rados/operations/)
* [Cache Tiering](https://docs.ceph.com/en/pacific/rados/operations/)
* [Placement Groups](https://docs.ceph.com/en/pacific/rados/operations/)
* [Balancer](https://docs.ceph.com/en/pacific/rados/operations/)
* [Using the pg\-upmap](https://docs.ceph.com/en/pacific/rados/operations/)
* [CRUSH Maps](https://docs.ceph.com/en/pacific/rados/operations/)
* [Manually editing a CRUSH Map](https://docs.ceph.com/en/pacific/rados/operations/)
* [Stretch Clusters](https://docs.ceph.com/en/pacific/rados/operations/)
* [Configure Monitor Election Strategies](https://docs.ceph.com/en/pacific/rados/operations/)


### Low\-level Operations

低レベルのクラスタ操作は、クラスタ内の特定のデーモンの起動、停止、再起動、特定のデーモンまたはサブシステムの設定変更、クラスタへのデーモンの追加、クラスタからのデーモンの削除で構成されます。低レベル操作の最も一般的なユースケースは、Cephクラスタの拡大または縮小、レガシーまたは故障したハードウェアを新しいハードウェアに置き換えることです。

* [Adding/Removing OSDs](https://docs.ceph.com/en/pacific/rados/operations/)
* [Adding/Removing Monitors](https://docs.ceph.com/en/pacific/rados/operations/)
* [Device Management](https://docs.ceph.com/en/pacific/rados/operations/)
* [BlueStore Migration](https://docs.ceph.com/en/pacific/rados/operations/)
* [Command Reference](https://docs.ceph.com/en/pacific/rados/operations/)

### Troubleshooting

Cephはまだ最先端であるため、Ceph構成を評価し、ログおよびデバッグ設定を変更して、クラスタで発生している問題を特定および改善する必要がある状況に遭遇する可能性があります。

* [The Ceph Community](https://docs.ceph.com/en/pacific/rados/operations/)
* [Troubleshooting Monitors](https://docs.ceph.com/en/pacific/rados/operations/)
* [Troubleshooting OSDs](https://docs.ceph.com/en/pacific/rados/operations/)
* [Troubleshooting PGs](https://docs.ceph.com/en/pacific/rados/operations/)
* [Logging and Debugging](https://docs.ceph.com/en/pacific/rados/operations/)
* [CPU Profiling](https://docs.ceph.com/en/pacific/rados/operations/)
* [Memory Profiling](https://docs.ceph.com/en/pacific/rados/operations/)
