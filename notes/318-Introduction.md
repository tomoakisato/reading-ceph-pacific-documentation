# 318: Introduction

 # Introduction[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/intro/ "Permalink to this headline")

This guide has two aims. First, it should lower the barrier to entry for software developers who wish to get involved in the Ceph project. Second, it should serve as a reference for Ceph developers.

We assume that readers are already familiar with Ceph \(the distributed object store and file system designed to provide excellent performance, reliability and scalability\). If not, please refer to the [project website](https://docs.ceph.com/en/pacific/dev/developer_guide/intro/)and especially the [publications list](https://docs.ceph.com/en/pacific/dev/developer_guide/intro/). Another way to learn about what’s happening in Ceph is to check out our [youtube channel](https://docs.ceph.com/en/pacific/dev/developer_guide/intro/) , where we post Tech Talks, Code walk\-throughs and Ceph Developer Monthly recordings.

Since this document is to be consumed by developers, who are assumed to have Internet access, topics covered elsewhere, either within the Ceph documentation or elsewhere on the web, are treated by linking. If you notice that a link is broken or if you know of a better link, please[report it as a bug](https://docs.ceph.com/en/pacific/dev/developer_guide/intro/).
