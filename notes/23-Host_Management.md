# 23: Host Management

**クリップソース:** [23: Host Management — Ceph Documentation](https://docs.ceph.com/en/pacific/cephadm/host-management/)

# Host Management[¶](https://docs.ceph.com/en/pacific/cephadm/host-management/#host-management "Permalink to this headline")

クラスターに関連するホストを一覧表示するには、次のようにします。

```
# ceph orch host ls [--format yaml]
```

## Adding Hosts[¶](https://docs.ceph.com/en/pacific/cephadm/host-management/#adding-hosts "Permalink to this headline")

ホストは、これらの[要件](https://docs.ceph.com/en/pacific/cephadm/install/#cephadm-host-requirements)がインストールされている必要があります。必要な要件がすべて揃っていないホストは、クラスターへの追加に失敗します。

それぞれの新しいホストをクラスターに追加するには、2つのステップを実行します。

1. 新しいホストのルートユーザーのauthorized\_keysファイルに、クラスタのパブリックSSHキーをインストールします。

```
# ssh-copy-id -f -i /etc/ceph/ceph.pub root@*<new-host>*
```

For example:

```
# ssh-copy-id -f -i /etc/ceph/ceph.pub root@host2
# ssh-copy-id -f -i /etc/ceph/ceph.pub root@host3
```

2. 新しいノードがクラスタの一部であることをCephに伝えます。

```
# ceph orch host add *<newhost>* [*<ip>*] [*<label1> ...*]
```

For example:

```
# ceph orch host add host2 10.10.0.102
# ceph orch host add host3 10.10.0.103
```

ホストのIPアドレスを明示的に提供するのがベストです。 IPが提供されていない場合は、ホスト名がDNSで直ちに解決され、そのIPが使用されます。

1つまたは複数のラベルを含めて、新しいホストにすぐにラベルを付けることもできます。 たとえば、デフォルトでは、\_adminラベルにより、cephadmは/etc/cephにceph.confファイルのコピーとclient.adminキーリングファイルを保持します。

```
# ceph orch host add host4 10.10.0.104 --labels _admin
```

## Removing Hosts[¶](https://docs.ceph.com/en/pacific/cephadm/host-management/#removing-hosts "Permalink to this headline")

ホストからすべてのデーモンが削除されると、クラスタからホストを安全に削除することができます。

ホストからすべてのデーモンを排出するには、次のようにします。

```
# ceph orch host drain *<host>*
```

ホストに「\_no\_schedule」ラベルが適用されます。関連項目： [Special host labels](https://docs.ceph.com/en/pacific/cephadm/host-management/#cephadm-special-host-labels) 

ホスト上のすべてのosdが削除されるようにスケジュールされます。osd の削除の進捗状況は、以下の方法で確認できます。

```
# ceph orch osd rm status
```

osdの取り外しについての詳細は、「[Remove an OSD](https://docs.ceph.com/en/pacific/cephadm/services/osd/#cephadm-osd-removal)」を参照してください。[27: OSD Service](27-OSD_Service.md)

ホスト上にデーモンが残っていないかどうかは、次のようにして確認できます。

```
# ceph orch ps <host>
```

すべてのデーモンが削除されたら、次のようにしてホストを削除できます。

```
# ceph orch host rm <host>
```

### Offline host removal[¶](https://docs.ceph.com/en/pacific/cephadm/host-management/#offline-host-removal "Permalink to this headline")

ホストがオフラインになって復旧できない場合でも、以下の方法でクラスタから削除することができます。

```
# ceph orch host rm <host> --offline --force
```

これは、各osdに対してosdpurge\-actualを呼び出すことにより、osdがクラスタから強制的にパージされるため、データ損失を引き起こす可能性があります。このホストが含まれているサービス仕様は、手動で更新する必要があります。

## Host labels[¶](https://docs.ceph.com/en/pacific/cephadm/host-management/#host-labels "Permalink to this headline")

オーケストレーターは、ホストへのラベルの割り当てをサポートしています。ラベルは自由形式で、それ自体には特に意味はなく、各ホストは複数のラベルを持つことができます。これらのラベルは、デーモンの配置を指定するために使用できます。[Placement by labels](https://docs.ceph.com/en/pacific/cephadm/services/#orch-placement-by-labels) を参照してください。 [24: Service Management](24-Service_Management.md)

ラベルは \-\-labels フラグでホストを追加するときに追加できます。

```
ceph orch host add my_hostname --labels=my_label1
ceph orch host add my_hostname --labels=my_label1,my_label2
```

既存のホストにラベルを追加するには、次のように実行します。

```
ceph orch host label add my_hostname my_label
```

ラベルを削除するには、実行します。

```
ceph orch host label rm my_hostname my_label
```

### Special host labels[¶](https://docs.ceph.com/en/pacific/cephadm/host-management/#special-host-labels "Permalink to this headline")

以下のホストラベルは、cephadmにとって特別な意味を持っています。 すべて\_で始まります。

* このラベルは、cephadmがこのホストにデーモンを配備するのを防ぎます。 すでにCephのデーモンが含まれている既存のホストにこのラベルを追加すると、cephadmはそれらのデーモンを別の場所に移動させます\(自動的に削除されないOSDを除く\)。
    \_no\_schedule: このホストでデーモンのスケジュール設定やデプロイを行わない。
* このラベルは、そのホスト上の 1 つ以上のデーモンに対して osd\_memory\_target\_autotune などのオプションが有効になっていても、デーモンのメモリが調整されないようにします。
    \_no\_autotune\_memory: このホストでメモリをオートチューンしないでください。
* デフォルトでは、\_adminラベルがクラスタ内の最初のホスト\(bootstrapが最初に実行された場所\)に適用され、client.adminキーがcephorchclient\-keyring...関数を介してそのホストに配布されるように設定されています。 追加のホストにこのラベルを追加すると、通常、cephadmは/etc/cephに設定ファイルとキーリングファイルを展開します。
    \_admin: このホストにclient.adminとceph.confを配布します。

## Maintenance Mode[¶](https://docs.ceph.com/en/pacific/cephadm/host-management/#maintenance-mode "Permalink to this headline")

ホストをメンテナンスモードにしたり、メンテナンスモードから外したりします\(ホスト上のすべてのCephデーモンを停止します\)。

```
ceph orch host maintenance enter <hostname> [--force]
ceph orch host maintenace exit <hostname>
```

メンテナンスに入るときに強制フラグを立てると、警告（警告ではない）を回避することができます。

[Fully qualified domain names vs bare host names](https://docs.ceph.com/en/pacific/cephadm/host-management/#cephadm-fqdn) も参照してください。

## Creating many hosts at once[¶](https://docs.ceph.com/en/pacific/cephadm/host-management/#creating-many-hosts-at-once "Permalink to this headline")

ceph orch apply \-iでは、複数のドキュメントを持つYAMLファイルを提出することで、一度に多くのホストを追加することができます。

```
service_type: host
hostname: node-00
addr: 192.168.0.10
labels:
- example1
- example2
---
service_type: host
hostname: node-01
addr: 192.168.0.11
labels:
- grafana
---
service_type: host
hostname: node-02
addr: 192.168.0.12
```

これは、サービス仕様\(以下\)と組み合わせてクラスタ仕様ファイルを作成し、1つのコマンドでクラスタ全体を展開することができます。 cephadm bootstrap \-\-apply\-specも参照して、ブートストラップ中にこれを実行します。クラスタSSHキーは、ホストを追加する前にホストにコピーする必要があります。

## Setting the initial CRUSH location of host[¶](https://docs.ceph.com/en/pacific/cephadm/host-management/#setting-the-initial-crush-location-of-host "Permalink to this headline")

ホストにはロケーション識別子を含めることができ、これによりcephadmは指定された階層にある新しいCRUSHホストを作成するよう指示します。

```
service_type: host
hostname: node-00
addr: 192.168.0.10
location:
  rack: rack1
```

Note: location属性は、CRUSHの初期位置にのみ影響します。それ以降のlocation属性の変更は無視されます。また、ホストを削除しても、CRUSHバケットは削除されません。

## SSH Configuration[¶](https://docs.ceph.com/en/pacific/cephadm/host-management/#ssh-configuration "Permalink to this headline")

Cephadmはリモートホストへの接続にSSHを使用します。 SSHは鍵を使って安全にホストとの認証を行います。

### Default behavior[¶](https://docs.ceph.com/en/pacific/cephadm/host-management/#default-behavior "Permalink to this headline")

Cephadmは、リモートホストへの接続に使用するSSHキーをモニターに保存します。 クラスタのブートストラップ時には、このSSHキーが自動的に生成され、追加の設定は必要ありません。

新しいSSHキーを生成：

```
ceph cephadm generate-key
```

SSHキーの公開部分の取得：

```
ceph cephadm get-pub-key
```

現在保存されているSSHキーを削除：

```
ceph cephadm clear-key
```

既存のキーを直接インポートすることで利用することができます：

```
ceph config-key set mgr/cephadm/ssh_identity_key -i <key>
ceph config-key set mgr/cephadm/ssh_identity_pub -i <pub>
```

その後、mgrデーモンを再起動して、コンフィギュレーションを再読み込みする必要があります：

```
ceph mgr fail
```

### Configuring a different SSH user[¶](https://docs.ceph.com/en/pacific/cephadm/host-management/#configuring-a-different-ssh-user "Permalink to this headline")

Cephadmは、コンテナイメージのダウンロード、コンテナの起動、パスワードの入力を求められないコマンドの実行に十分な権限を持つユーザとして、すべてのCephクラスタノードにログインできる必要があります。

「root」ユーザ\(cephadmのデフォルトオプション\)を使用したくない場合は、cephadmのすべての操作を実行するために使用するユーザ名をcephadmに提供する必要があります。

以下のコマンドを使用します：

```
ceph cephadm set-user <user>
```

これを実行する前に、このユーザのauthorized\_keysファイルにクラスタのssh鍵を追加し、非rootユーザにパスワードなしのsudoアクセス権を与える必要があります。

### Customizing the SSH configuration[¶](https://docs.ceph.com/en/pacific/cephadm/host-management/#customizing-the-ssh-configuration "Permalink to this headline")

Cephadmは、リモートホストへの接続に使用される適切なssh\_configファイルを生成します。 

これは次のようなものです：

```
Host *
User root
StrictHostKeyChecking no
UserKnownHostsFile /dev/null
```

この設定をお客様の環境に合わせてカスタマイズするには2つの方法があります：

1. カスタマイズされた設定ファイルをインポートして、モニターに保存する：

```
ceph cephadm set-ssh-config -i <ssh_config_file>
```

カスタマイズしたSSH設定を削除して、デフォルトの動作に戻す：

```
ceph cephadm clear-ssh-config
```

2. SSH設定ファイルのファイルロケーションを設定する：

```
ceph config set mgr mgr/cephadm/ssh_config_file <path>
```

この方法はお勧めできません。 パス名はどのmgrデーモンからも見えるようにする必要があり、cephadmはすべてのデーモンをコンテナとして実行します。つまり、ファイルは、デプロイメント用にカスタマイズされたコンテナイメージ内に配置するか、手動でmgrデータディレクトリ\(ホストでは/var/lib/ceph/\<cluster\-fsid\>/mgr.\<id\>、コンテナ内では/var/lib/ceph/mgr/ceph\-\<id\>\)に配布する必要があります。

## Fully qualified domain names vs bare host names[¶](https://docs.ceph.com/en/pacific/cephadm/host-management/#fully-qualified-domain-names-vs-bare-host-names "Permalink to this headline")

Note: cephadmは、ceph orch host add を介して与えられたホストの名前が、リモートホストのhostnameの出力と等しいことを要求します。

 そうしないと、cephadmは、ceph\*metadataによって返された名前がcephadmに知られているホストと一致することを確認できません。これにより、[CEPHADM\_STRAY\_HOST](https://docs.ceph.com/en/pacific/cephadm/operations/#cephadm-stray-host)警告が発生する場合があります。

 新しいホストを設定する際に、ホスト名を設定する有効な方法は2つあります：

1. 素\(bare\)のホスト名を使用する。 この場合：

* hostnameは、素\(bare\)のホスト名を返します。
* hostname\-fは、FQDNを返します。

2. 完全修飾ドメイン名をホスト名として使用する。この場合：

* hostname はFQDNを返す
* hostname\-s は素\(bare\)のホスト名を返す

なお、man hostnameでは、素\(bare\)のホスト名を返すhostnameを推奨しています。

システムのFQDN（Fully Qualified Domain Name）は、ursula.example.comのようなホスト名に対してリゾルバ\(3\)が返す名前です。通常、ホスト名の後にDNSドメイン名（最初のドットの後の部分）を加えたものです。FQDNはhostname\-\-fqdnで、ドメイン名はdnsdomainnameで確認できます。

```
You cannot change the FQDN with hostname or dnsdomainname.

The recommended method of setting the FQDN is to make the hostname
be an alias for the fully qualified name using /etc/hosts, DNS, or
NIS. For example, if the hostname was "ursula", one might have
a line in /etc/hosts which reads

       127.0.1.1    ursula.example.com ursula
```

つまり、man hostnameは、素のホスト名を返すようにhostnameを推奨しています。これは、ceph\*metadataを実行するときにCephが素\(bare\)のホスト名を返すことを意味します。これは、cephadmがクラスタにホストを追加するときにも素\(bare\)のホスト名を要求することを意味します: ceph orch host add\<bare\-name\>
