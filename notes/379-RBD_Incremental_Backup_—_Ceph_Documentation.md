# 379: RBD Incremental Backup — Ceph Documentation

 # RBD Incremental Backup[¶](https://docs.ceph.com/en/pacific/dev/rbd-diff/ "Permalink to this headline")

This is a simple streaming file format for representing a diff between two snapshots \(or a snapshot and the head\) of an RBD image.

## Header[¶](https://docs.ceph.com/en/pacific/dev/rbd-diff/ "Permalink to this headline")

“rbd diff v1n”

## Metadata records[¶](https://docs.ceph.com/en/pacific/dev/rbd-diff/ "Permalink to this headline")

Every record has a one byte “tag” that identifies the record type, followed by some other data.

Metadata records come in the first part of the image. Order is not important, as long as all the metadata records come before the data records.

### From snap[¶](https://docs.ceph.com/en/pacific/dev/rbd-diff/ "Permalink to this headline")

* u8: ‘f’
* le32: snap name length
* snap name

### To snap[¶](https://docs.ceph.com/en/pacific/dev/rbd-diff/ "Permalink to this headline")

* u8: ‘t’
* le32: snap name length
* snap name

### Size[¶](https://docs.ceph.com/en/pacific/dev/rbd-diff/ "Permalink to this headline")

* u8: ‘s’
* le64: \(ending\) image size

## Data Records[¶](https://docs.ceph.com/en/pacific/dev/rbd-diff/ "Permalink to this headline")

These records come in the second part of the sequence.

### Updated data[¶](https://docs.ceph.com/en/pacific/dev/rbd-diff/ "Permalink to this headline")

* u8: ‘w’
* le64: offset
* le64: length
* length bytes of actual data

### Zero data[¶](https://docs.ceph.com/en/pacific/dev/rbd-diff/ "Permalink to this headline")

* u8: ‘z’
* le64: offset
* le64: length

## Final Record[¶](https://docs.ceph.com/en/pacific/dev/rbd-diff/ "Permalink to this headline")

### End[¶](https://docs.ceph.com/en/pacific/dev/rbd-diff/ "Permalink to this headline")

* u8: ‘e’

## Header[¶](https://docs.ceph.com/en/pacific/dev/rbd-diff/ "Permalink to this headline")

“rbd diff v2n”

## Metadata records[¶](https://docs.ceph.com/en/pacific/dev/rbd-diff/ "Permalink to this headline")

Every record has a one byte “tag” that identifies the record type, followed by length of data, and then some other data.

Metadata records come in the first part of the image. Order is not important, as long as all the metadata records come before the data records.

In v2, we have the following metadata in each section: \(1 Bytes\) tag. \(8 Bytes\) length. \(n Bytes\) data.

In this way, we can skip the unrecognized tag.

### From snap[¶](https://docs.ceph.com/en/pacific/dev/rbd-diff/ "Permalink to this headline")

* u8: ‘f’
* le64: length of appending data \(4 \+ length\)
* le32: snap name length
* snap name

### To snap[¶](https://docs.ceph.com/en/pacific/dev/rbd-diff/ "Permalink to this headline")

* u8: ‘t’
* le64: length of appending data \(4 \+ length\)
* le32: snap name length
* snap name

### Size[¶](https://docs.ceph.com/en/pacific/dev/rbd-diff/ "Permalink to this headline")

* u8: ‘s’
* le64: length of appending data \(8\)
* le64: \(ending\) image size

## Data Records[¶](https://docs.ceph.com/en/pacific/dev/rbd-diff/ "Permalink to this headline")

These records come in the second part of the sequence.

### Updated data[¶](https://docs.ceph.com/en/pacific/dev/rbd-diff/ "Permalink to this headline")

* u8: ‘w’
* le64: length of appending data \(8 \+ 8 \+ length\)
* le64: offset
* le64: length
* length bytes of actual data

### Zero data[¶](https://docs.ceph.com/en/pacific/dev/rbd-diff/ "Permalink to this headline")

* u8: ‘z’
* le64: length of appending data \(8 \+ 8\)
* le64: offset
* le64: length

## Final Record[¶](https://docs.ceph.com/en/pacific/dev/rbd-diff/ "Permalink to this headline")

### End[¶](https://docs.ceph.com/en/pacific/dev/rbd-diff/ "Permalink to this headline")

* u8: ‘e’
