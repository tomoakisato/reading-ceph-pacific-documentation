# 476: Ceph Glossary — Ceph Documentation

 # Ceph Glossary[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this headline")

Ceph is growing rapidly. As firms deploy Ceph, the technical terms such as “RADOS”, “RBD,” “RGW” and so forth require corresponding marketing terms that explain what each component does. The terms in this glossary are intended to complement the existing technical terminology.

Sometimes more than one term applies to a definition. Generally, the first term reflects a term consistent with Ceph’s marketing, and secondary terms reflect either technical terms or legacy ways of referring to Ceph systems.

Ceph Project[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")The aggregate term for the people, software, mission and infrastructure of Ceph.

cephx[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")The Ceph authentication protocol. Cephx operates like Kerberos, but it has no single point of failure.

Ceph[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")Ceph Platform[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")All Ceph software, which includes any piece of code hosted at[https://github.com/ceph](https://docs.ceph.com/en/pacific/glossary/).

Ceph System[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")Ceph Stack[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")A collection of two or more components of Ceph.

Ceph Node[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")Node[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")Host[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")Any single machine or server in a Ceph System.

Ceph Storage Cluster[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")Ceph Object Store[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")RADOS[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")RADOS Cluster[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")Reliable Autonomic Distributed Object Store[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")The core set of storage software which stores the user’s data \(MON\+OSD\).

Ceph Cluster Map[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")Cluster Map[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")The set of maps comprising the monitor map, OSD map, PG map, MDS map and CRUSH map. See [Cluster Map](https://docs.ceph.com/en/pacific/glossary/) for details.

Ceph Object Storage[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")The object storage “product”, service or capabilities, which consists essentially of a Ceph Storage Cluster and a Ceph Object Gateway.

Ceph Object Gateway[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")RADOS Gateway[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")RGW[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")The S3/Swift gateway component of Ceph.

Ceph Block Device[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")RBD[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")The block storage component of Ceph.

Ceph Block Storage[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")The block storage “product,” service or capabilities when used in conjunction with `librbd`, a hypervisor such as QEMU or Xen, and a hypervisor abstraction layer such as `libvirt`.

Ceph File System[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")CephFS[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")Ceph FS[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")The POSIX filesystem components of Ceph. Refer[CephFS Architecture](https://docs.ceph.com/en/pacific/glossary/) and [Ceph File System](https://docs.ceph.com/en/pacific/glossary/) for more details.

Cloud Platforms[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")Cloud Stacks[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")Third party cloud provisioning platforms such as OpenStack, CloudStack, OpenNebula, ProxMox, etc.

Object Storage Device[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")OSD[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")A physical or logical storage unit \(_e.g._, LUN\). Sometimes, Ceph users use the term “OSD” to refer to [Ceph OSD Daemon](https://docs.ceph.com/en/pacific/glossary/), though the proper term is “Ceph OSD”.

Ceph OSD Daemon[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")Ceph OSD Daemons[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")Ceph OSD[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")The Ceph OSD software, which interacts with a logical disk \([OSD](https://docs.ceph.com/en/pacific/glossary/)\). Sometimes, Ceph users use the term “OSD” to refer to “Ceph OSD Daemon”, though the proper term is “Ceph OSD”.

OSD id[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")The integer that defines an OSD. It is generated by the monitors as part of the creation of a new OSD.

OSD fsid[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")This is a unique identifier used to further improve the uniqueness of an OSD and it is found in the OSD path in a file called `osd_fsid`. This`fsid` term is used interchangeably with `uuid`

OSD uuid[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")Just like the OSD fsid, this is the OSD unique identifier and is used interchangeably with `fsid`

bluestore[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")OSD BlueStore is a new back end for OSD daemons \(kraken and newer versions\). Unlike [filestore](https://docs.ceph.com/en/pacific/glossary/) it stores objects directly on the Ceph block devices without any file system interface.

filestore[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")A back end for OSD daemons, where a Journal is needed and files are written to the filesystem.

Ceph Monitor[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")MON[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")The Ceph monitor software.

Ceph Manager[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")MGR[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")The Ceph manager software, which collects all the state from the whole cluster in one place.

Ceph Manager Dashboard[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")Ceph Dashboard[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")Dashboard Module[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")Dashboard Plugin[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")Dashboard[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")A built\-in web\-based Ceph management and monitoring application to administer various aspects and objects of the cluster. The dashboard is implemented as a Ceph Manager module. See [Ceph Dashboard](https://docs.ceph.com/en/pacific/glossary/) for more details.

Ceph Metadata Server[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")MDS[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")The Ceph metadata software.

Ceph Clients[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")Ceph Client[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")The collection of Ceph components which can access a Ceph Storage Cluster. These include the Ceph Object Gateway, the Ceph Block Device, the Ceph File System, and their corresponding libraries, kernel modules, and FUSEs.

Ceph Kernel Modules[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")The collection of kernel modules which can be used to interact with the Ceph System \(e.g., `ceph.ko`, `rbd.ko`\).

Ceph Client Libraries[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")The collection of libraries that can be used to interact with components of the Ceph System.

Ceph Release[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")Any distinct numbered version of Ceph.

Ceph Point Release[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")Any ad\-hoc release that includes only bug or security fixes.

Ceph Interim Release[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")Versions of Ceph that have not yet been put through quality assurance testing, but may contain new features.

Ceph Release Candidate[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")A major version of Ceph that has undergone initial quality assurance testing and is ready for beta testers.

Ceph Stable Release[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")A major version of Ceph where all features from the preceding interim releases have been put through quality assurance testing successfully.

Ceph Test Framework[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")Teuthology[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")The collection of software that performs scripted tests on Ceph.

CRUSH[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")Controlled Replication Under Scalable Hashing. It is the algorithm Ceph uses to compute object storage locations.

CRUSH rule[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")The CRUSH data placement rule that applies to a particular pool\(s\).

Pool[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")Pools[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")Pools are logical partitions for storing objects.

systemd oneshot[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")A systemd `type` where a command is defined in `ExecStart` which will exit upon completion \(it is not intended to daemonize\)

LVM tags[¶](https://docs.ceph.com/en/pacific/glossary/ "Permalink to this term")Extensible metadata for LVM volumes and groups. It is used to store Ceph\-specific information about devices and its relationship with OSDs.
