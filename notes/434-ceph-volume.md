# 434: ceph-volume

**クリップソース:** [434: ceph\-volume — Ceph Documentation](https://docs.ceph.com/en/pacific/ceph-volume/)

# ceph\-volume[¶](https://docs.ceph.com/en/pacific/ceph-volume/ "Permalink to this headline")

lvmや物理ディスクのような異なるデバイスで、プラグイン可能なツールを使用してOSDをデプロイし、予測可能で堅牢な方法でOSDの準備、起動、開始を試みます。

[Overview](https://docs.ceph.com/en/pacific/ceph-volume/intro/#ceph-volume-overview) [435: Overview](e435-Overview.md)| [Plugin Guide](https://docs.ceph.com/en/pacific/dev/ceph-volume/plugins/#ceph-volume-plugins) [424: Plugins](424-Plugins.md)|

**Command Line Subcommands**

現在、lvmと、ceph\-diskでデプロイしたプレーンディスク（GPTパーティション）がサポートされています。

zfsサポートは、FreeBSDクラスターを実行するために利用できます。

* [lvm](https://docs.ceph.com/en/pacific/ceph-volume/lvm/#ceph-volume-lvm)
* [simple](https://docs.ceph.com/en/pacific/ceph-volume/simple/#ceph-volume-simple)
* [zfs](https://docs.ceph.com/en/pacific/ceph-volume/zfs/#ceph-volume-zfs)

**Node inventory**

[inventory](https://docs.ceph.com/en/pacific/ceph-volume/inventory/#ceph-volume-inventory) サブコマンドは、ノードの物理ディスクのインベントリに関する情報およびメタデータを提供します。[424: Plugins](424-Plugins.md)

## Migrating[¶](https://docs.ceph.com/en/pacific/ceph-volume/ "Permalink to this headline")

Cephバージョン13.0.0から、ceph\-diskは非推奨になりました。非推奨の警告が表示され、このページへのリンクが表示されます。ユーザはceph\-volumeの使用を開始することが強く推奨されます。移行には2つのパスがあります：

1.  ceph\-diskでデプロイしたOSDを維持する。
2. ceph\-volumeで既存のOSDを再デプロイする。これは、

ceph\-diskが削除された理由の詳細については、「[ceph\-diskはなぜ交換されたのですか？](https://docs.ceph.com/en/pacific/ceph-volume/intro/#ceph-disk-replaced)」 のセクションを参照してください。[435: Overview](435-Overview.md)

### New deployments[¶](https://docs.ceph.com/en/pacific/ceph-volume/ "Permalink to this headline")

[lvm](https://docs.ceph.com/en/pacific/ceph-volume/lvm/#ceph-volume-lvm)は、データOSDの入力として任意の論理ボリュームを使用することができ、またデバイスから最小の論理ボリュームをセットアップすることができます。[439: lvm](439-lvm.md)

### Existing OSDs[¶](https://docs.ceph.com/en/pacific/ceph-volume/ "Permalink to this headline")

クラスタにceph\-diskでプロビジョニングされたOSDがある場合、ceph\-volumeは[simple](https://docs.ceph.com/en/pacific/ceph-volume/simple/#ceph-volume-simple)でこれらの管理を引き継ぐことができます。データデバイスまたはOSDディレクトリでスキャンが実行され、ceph\-diskは完全に無効になります。暗号化も完全にサポートされています。[452: simple](452-simple.md)
