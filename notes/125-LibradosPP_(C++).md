# 125: LibradosPP (C++)

 
      [Report a Documentation Bug](https://docs.ceph.com/en/pacific/rados/api/libradospp/) 
 # LibradosPP \(C\+\+\)[¶](https://docs.ceph.com/en/pacific/rados/api/libradospp/ "Permalink to this headline")

Note:

The librados C\+\+ API is not guaranteed to be API\+ABI stable between major releases. All applications using the librados C\+\+ API must be recompiled and relinked against a specific Ceph release.

Todo:

write me\!

