# 90: Adding/Removing Monitors

**クリップソース:** [90: Adding/Removing Monitors — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-mons/)

# Adding/Removing Monitors[¶](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-mons/ "Permalink to this headline")

クラスタが稼働している場合、実行時にクラスタにモニタを追加または削除することができます。モニタをブートストラップするには、「[Manual Deployment](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-mons/)」または「[Monitor Bootstrap](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-mons/)」を参照してください。

## Adding Monitors[¶](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-mons/ "Permalink to this headline")

Cephモニタは、クラスタマップの単一の真実の源となる軽量なプロセスです。1つのモニタでクラスタを実行できますが、実運用クラスタでは少なくとも3つのモニタを推奨します。Cephモニタは、Paxosアルゴリズムのバリエーションを使用して、クラスタ全体のマップおよびその他の重要な情報についてのコンセンサスを確立します。Paxosの性質上、Cephでは、クォーラムを確立するためにモニタの過半数がアクティブである\(コンセンサスが確立される\)必要があります。

奇数のモニタを実行することをお勧めします。 奇数のモニタは、偶数のモニタよりも回復力があります。 たとえば、2台の場合、障害を許容することはできません。 3台の場合、1台の障害に耐えることができます。 4台の場合、1台の障害を許容できます。 5台の場合、2台の障害に耐えることができます。 これにより、スプリットブレイン現象が回避され、奇数が最適な理由になります。 要するに、Cephはモニタの過半数がアクティブである（そして相互に通信できる）必要がありますが、その過半数は単一のモニター、または2台のうち2台、3台のうち2台、4台のうち3台 等で達成できます。

マルチノードCephクラスタの小規模またはクリティカルでないデプロイでは、3台のモニタが推奨され、大規模クラスタまたは二重障害に耐えるためにモニタ数を5つに増やすことが推奨されます。 7台以上にする正当な理由はほとんどありません。

モニタは軽量であるため、OSDと同じホストで動作させることも可能ですが、カーネルとのfsyncの問題で性能が低下する可能性があるため、別々のホストで動作させることを推奨します。モニタ専用ノードは、ノードのクラッシュやメンテナンスのための停止時に、モニタとOSDデーモンが同時に非アクティブにならないため、混乱も最小限に抑えることができます。

また、専用モニタノードを使用することで、ノードの再起動や停止、クラッシュ時にOSDとモニタが停止することを回避でき、メンテナンスがしやすくなります。

注：クォーラムを確立するには、クラスタ内のモニタの過半数が相互に到達できる必要があります。

### Deploy your Hardware[¶](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-mons/ "Permalink to this headline")

新しいモニタを追加する際に新しいホストを追加する場合、モニタハードウェアの最小推奨値の詳細については、「 [Hardware Recommendations](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-mons/)」を参照してください。クラスタにモニタホストを追加するには、まず、最新バージョンのLinuxがインストールされていることを確認します（通常、Ubuntu 16.04 または RHEL 7）。

モニタホストをクラスタ内のラックに追加し、ネットワークに接続し、ネットワーク接続が可能であることを確認します。

### Install the Required Software[¶](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-mons/ "Permalink to this headline")

手動で展開されたクラスタの場合、Cephパッケージを手動でインストールする必要があります。詳細については、「[Installing Packages](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-mons/)」を参照してください。SSHは、パスワードなしの認証とroot権限を持つユーザに設定する必要があります。

### Adding a Monitor \(Manual\)[¶](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-mons/ "Permalink to this headline")

この手順では、ceph\-monデータディレクトリを作成し、モニタマップとモニタキーリングを取得し、ceph\-monデーモンをクラスタに追加します。 この結果、モニタデーモンが2つしかない場合は、クォーラムを達成するのに十分な数のceph\-monデーモンができるまでこの手順を繰り返して、さらにモニタを追加することができます。

この時点で、モニタのIDを定義する必要があります。 伝統的に、モニタには1文字（a, b, c, ...）の名前が付けられていますが、idは自由に定義してください。 このドキュメントでは、{mon\-id}はあなたが選んだidからmon.の接頭辞を除いたものであることを考慮してください（つまり、{mon\-id}は mon.a の a であるべきです）。

1. 新しいモニタをホストするマシンに、デフォルトのディレクトリを作成します。

```
ssh {new-mon-host}
sudo mkdir /var/lib/ceph/mon/ceph-{mon-id}
```

1. この処理中に必要なファイルを保存するために、一時ディレクトリ{tmp}を作成します。このディレクトリは、前のステップで作成したモニタのデフォルトディレクトリとは異なるもので、すべてのステップの実行後に削除することができます。

```
mkdir {tmp}
```

1. モニタのキーリングを取得します。{tmp}は取得したキーリングへのパス、{key\-filename}は取得したモニタキーを含むファイル名です。

```
ceph auth get mon. -o {tmp}/{key-filename}
```

1. モニタマップを取得します。{tmp}は取得したモニターマップへのパスで、{map\-filename}は取得したモニターマップを含むファイル名です。

```
ceph mon getmap -o {tmp}/{map-filename}
```

1. 最初のステップで作成したモニタのデータディレクトリを準備します。モニタのクォーラムとそのfsidに関する情報を取得できるように、モニタマップのパスを指定する必要があります。また、モニタキーリングへのパスも指定する必要があります。

```
sudo ceph-mon -i {mon-id} --mkfs --monmap {tmp}/{map-filename} --keyring {tmp}/{key-filename}
```

1. 新しいモニタを起動すると、自動的にクラスタに参加します。デーモンは、\-public\-addr {ip} または \-\-public\-network {network} 引数によって、どのアドレスにバインドするかを知っておく必要があります。例えば：

```
ceph-mon -i {mon-id} --public-addr {ip:port}
```

## Removing Monitors[¶](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-mons/ "Permalink to this headline")

クラスタからモニタを削除する場合、CephモニタはPaxosを使用してマスタ・クラスタ・マップに関するコンセンサスを確立することを考慮する必要があります。クラスタマップに関するコンセンサスのクォーラムを確立するには、十分な数のモニタを用意する必要があります。

### Removing a Monitor \(Manual\)[¶](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-mons/ "Permalink to this headline")

この手順では、クラスタからceph\-monデーモンを削除します。  この手順の結果、モニタデーモンが2つしかない場合は、クォーラムを達成できる数のceph\-monデーモンができるまで、別のモニタを追加または削除してください。

1. モニタを停止します

```
service ceph -a stop mon.{mon-id}
```

1. クラスタからモニタを取り外します

```
ceph mon remove {mon-id}
```

1. ceph.confからmonitorエントリを削除します

### Removing Monitors from an Unhealthy Cluster[¶](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-mons/ "Permalink to this headline")

この手順では、モニタがクォーラムを形成できないクラスタなど、不健全なクラスタからceph\-monデーモンを削除します。

1. すべてのモニタホスト上のすべてのceph\-monデーモンを停止します

```
ssh {mon-host}
systemctl stop ceph-mon.target
# and repeat for all mons
```

1. 生存しているモニタを特定し、そのホストにログインします

```
ssh {mon-host}
```

1. monmapファイルのコピーを抽出します

```
ceph-mon -i {mon-id} --extract-monmap {map-path}
# in most cases, that's
ceph-mon -i `hostname` --extract-monmap /tmp/monmap
```

1. 生存していないモニタや問題のあるモニタを削除します。 例えば、mon.a, mon.b, mon.cの3つのモニタがあり、mon.aだけが生き残る場合、以下の例のようにします

```
monmaptool {map-path} --rm {mon-id}
# for example,
monmaptool /tmp/monmap --rm b
monmaptool /tmp/monmap --rm c
```

1. 削除されたモニタを含む生存しているマップを、生存しているモニタに注入します。 例えば、あるマップをモニタmon.aに注入する場合、以下の例のようにします。

```
ceph-mon -i {mon-id} --inject-monmap {map-path}
# for example,
ceph-mon -i a --inject-monmap /tmp/monmap
```

1. 生き残ったモニタだけを起動します
2. モニタがクォーラムを形成していることを確認します（ceph \-s）
3. 削除したモニタのデータディレクトリを/var/lib/ceph/monの安全な場所にアーカイブするか、残りのモニタが健全で十分な冗長性があると確信できるのであれば削除するのもよいでしょう。

## Changing a Monitor’s IP Address[¶](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-mons/ "Permalink to this headline")

重要：既存モニタのIPアドレスは変更しないことになっています。

モニタはCephクラスタの重要なコンポーネントであり、システム全体が正常に動作するためにはクォーラムを維持する必要があります。クォーラムを確立するために、モニタはお互いを検出する必要があります。Cephには、モニターを検出するための厳格な要件があります。

Cephクライアントおよび他のCephデーモンは、ceph.confを使用してモニタを検出します。しかし、モニタはceph.confではなく、モニタマップを使用してお互いを検出します。たとえば、「 [Adding a Monitor \(Manual\)](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-mons/) 」を参照すると、新しいモニタを作成する際にクラスタの現在のmonmapを取得する必要があることがわかります。これは、ceph\-mon \-i{mon\-id} \-\-mkfsの必須引数の1つであるためです。次のセクションでは、Cephモニタの一貫性要件と、モニタのIPアドレスを変更するいくつかの安全な方法について説明します。

### Consistency Requirements[¶](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-mons/ "Permalink to this headline")

モニタは、クラスタ内の他のモニタを検出する際に、常に monmap のローカルコピーを参照します。 ceph.confの代わりにmonmapを使用すると、クラスタを破壊する可能性のあるエラー\(たとえば、モニターのアドレスまたはポートを指定する際のceph.confのタイプミス\)を回避できます。モニタは検出にmonmapを使用し、クライアントや他のCephデーモンとmonmapを共有するため、monmapはモニタに、そのコンセンサスが有効であることを厳密に保証します。

厳密な一貫性は、monmapの更新にも適用されます。モニタ上の他の更新と同様に、monmapへの変更は常に Paxos と呼ばれる分散型コンセンサス アルゴリズムを通過します。モニタは、クォーラム内の各モニタが同じバージョンのmonmapを持つように、モニタの追加や削除など、monmapの各更新について合意する必要があります。monmapの更新はインクリメンタルに行われ、モニタは合意された最新のバージョンと、過去のバージョンのセットを持つことになり、古いバージョンのmonmapを持つモニタがクラスタの現在の状態に追いつくことができるようになります。

モニタがmonmap経由ではなく、Ceph設定ファイル経由でお互いを発見した場合、Ceph設定ファイルは自動的に更新・配布されないため、さらなるリスクが発生することになります。モニタがうっかり古いceph.confファイルを使ったり、モニタを認識できなかったり、クォーラムから外れたり、Paxosがシステムの現在の状態を正確に判断できない事態に発展する可能性があるのです。そのため、既存のモニタのIPアドレスに変更を加える場合は、十分に注意して行う必要があります。

### Changing a Monitor’s IP address \(The Right Way\)[¶](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-mons/ "Permalink to this headline")

 ceph.confでモニタのIPアドレスを変更するだけでは、クラスタ内の他のモニタが確実に更新を受信することはできません。 モニタのIPアドレスを変更するには、使用するIPアドレスを持つ新しいモニタを追加し\([Adding a Monitor \(Manual\)](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-mons/)\)で説明\)、新しいモニタがクォーラムに正常に参加することを確認し、古いIPアドレスを使用するモニタを削除する必要があります。次に、ceph.confファイルを更新して、クライアントと他のデーモンが新しいモニタのIPアドレスを知っていることを確認します。

例えば、次のような3つのモニタがあるとします。

```
[mon.a]
        host = host01
        addr = 10.0.0.1:6789
[mon.b]
        host = host02
        addr = 10.0.0.2:6789
[mon.c]
        host = host03
        addr = 10.0.0.3:6789
```

 mon.cをIPアドレス10.0.0.4のhost04に変更するには、「[Adding a Monitor \(Manual\)](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-mons/)」の手順に従って、新しいモニタmon.dを追加します。mon.dが動作していることを確認してから mon.c を削除しないと、クォーラムが壊れてしまいます。「[Removing a Monitor \(Manual\)](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-mons/)」の説明に従って mon.c を削除します。このように、3つのモニタをすべて移動するには、このプロセスを必要なだけ繰り返す必要があります。

### Changing a Monitor’s IP address \(The Messy Way\)[¶](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-mons/ "Permalink to this headline")

モニタを別のネットワーク、データセンターの別の場所、あるいはまったく別のデータセンターに移動しなければならないときが来るかもしれません。移動は可能ですが、その作業は少々危険です。

このような場合、クラスタ内のすべてのモニタのIPアドレスを更新した新しいmonmapを生成し、個々のモニタに新しいマップをインジェクトすることで解決します。 これは最も使いやすい方法とは言えませんが、隔週で行う必要があるようなこととは考えていません。 このセクションのトップに明記されているように、モニタはIPアドレスを変更することは想定されていません。

前回のモニター設定を例にして、すべてのモニターを10.0.0.xの範囲から10.1.0.xに移動したいが、これらのネットワークが通信できない場合を想定します。 次の手順で行います。

1. モニタマップを取得します。{tmp}は取得したモニタマップへのパスで、{filename}は取得したモニタマップを含むファイルの名前です

```
ceph mon getmap -o {tmp}/{filename}
```

1. 次の例は、monmapの内容を示しています

```
$ monmaptool --print {tmp}/{filename}

monmaptool: monmap file {tmp}/{filename}
epoch 1
fsid 224e376d-c5fe-4504-96bb-ea6332a19e61
last_changed 2012-12-17 02:46:41.591248
created 2012-12-17 02:46:41.591248
0: 10.0.0.1:6789/0 mon.a
1: 10.0.0.2:6789/0 mon.b
2: 10.0.0.3:6789/0 mon.c
```

1. 既存のモニタを取り除きます

```
$ monmaptool --rm a --rm b --rm c {tmp}/{filename}

monmaptool: monmap file {tmp}/{filename}
monmaptool: removing a
monmaptool: removing b
monmaptool: removing c
monmaptool: writing epoch 1 to {tmp}/{filename} (0 monitors)
```

1. 新しいモニタの場所を追加します

```
$ monmaptool --add a 10.1.0.1:6789 --add b 10.1.0.2:6789 --add c 10.1.0.3:6789 {tmp}/{filename}

monmaptool: monmap file {tmp}/{filename}
monmaptool: writing epoch 1 to {tmp}/{filename} (3 monitors)
```

1. 新しいコンテンツを確認します

```
$ monmaptool --print {tmp}/{filename}

monmaptool: monmap file {tmp}/{filename}
epoch 1
fsid 224e376d-c5fe-4504-96bb-ea6332a19e61
last_changed 2012-12-17 02:46:41.591248
created 2012-12-17 02:46:41.591248
0: 10.1.0.1:6789/0 mon.a
1: 10.1.0.2:6789/0 mon.b
2: 10.1.0.3:6789/0 mon.c
```

この時点で、モニタ（およびストア）は新しい場所にインストールされているものとします。次のステップは、変更したmonmapを新しいモニタに伝搬させ、変更したmonmpaをそれぞれの新しいモニタにインジェクトすることです。

1. まず、すべてのモニターを停止してください。 インジェクションは、デーモンが動作していない状態で行う必要があります
2. monmapを注入します

```
ceph-mon -i {mon-id} --inject-monmap {tmp}/{filename}
```

1. モニタを再起動します

この手順の後、新しい場所への移行が完了し、モニタは正常に動作するはずです。
