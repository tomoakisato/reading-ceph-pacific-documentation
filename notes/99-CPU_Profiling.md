# 99: CPU Profiling

**クリップソース:** [99: CPU Profiling — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/troubleshooting/cpu-profiling/)

# CPU Profiling[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/cpu-profiling/ "Permalink to this headline")

Cephをソースからビルドし、[oprofile](https://docs.ceph.com/en/pacific/rados/troubleshooting/cpu-profiling/)で使用するためにCephをコンパイルした場合、CephのCPU使用率をプロファイリングすることができます。詳細については、「[Installing Oprofile](https://docs.ceph.com/en/pacific/rados/troubleshooting/cpu-profiling/)」を参照してください。

## Initializing oprofile[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/cpu-profiling/ "Permalink to this headline")

oprofileを初めて使うときは、初期化する必要があります。現在実行中のカーネルに対応する vmlinux イメージを探します。

```
ls /boot
sudo opcontrol --init
sudo opcontrol --setup --vmlinux={path-to-image} --separate=library --callgraph=6
```

## Starting oprofile[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/cpu-profiling/ "Permalink to this headline")

oprofileを起動するには、次のコマンドを実行します：

```
opcontrol --start
```

oprofileを起動したら、Cephでいくつかのテストを実行することができます。

## Stopping oprofile[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/cpu-profiling/ "Permalink to this headline")

oprofileを停止するには、次のコマンドを実行します：

```
opcontrol --stop
```

## Retrieving oprofile Results[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/cpu-profiling/ "Permalink to this headline")

cmonのトップ結果を取得するには、次のコマンドを実行します：

```
opreport -gal ./cmon | less
```

コールグラフが添付されたcmonのトップ結果を取得するには、次のコマンドを実行します：

```
opreport -cal ./cmon | less
```

重要：結果を確認した後、再度実行する前にoprofileをリセットする必要があります。oprofile をリセットすると、セッション・ディレクトリからデータが削除されます。

## Resetting oprofile[¶](https://docs.ceph.com/en/pacific/rados/troubleshooting/cpu-profiling/ "Permalink to this headline")

oprofileをリセットするには、以下のコマンドを実行します：

```
sudo opcontrol --reset
```

重要：データを分析した後、oprofileをリセットして、異なるテストの結果を混同しないようにする必要があります。
