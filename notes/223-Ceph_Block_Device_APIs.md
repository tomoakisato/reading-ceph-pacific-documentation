# 223: Ceph Block Device APIs

**クリップソース:** [223: Ceph Block Device APIs — Ceph Documentation](https://docs.ceph.com/en/pacific/rbd/api/)

# Ceph Block Device APIs[¶](https://docs.ceph.com/en/pacific/rbd/api/ "Permalink to this headline")

* [librbd \(Python\)](https://docs.ceph.com/en/pacific/rbd/api/)
    * [Example: Creating and writing to an image](https://docs.ceph.com/en/pacific/rbd/api/)
    * [API Reference](https://docs.ceph.com/en/pacific/rbd/api/)
