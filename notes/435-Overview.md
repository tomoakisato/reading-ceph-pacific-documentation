# 435: Overview

**クリップソース:** [435: Overview — Ceph Documentation](https://docs.ceph.com/en/pacific/ceph-volume/intro/)

**[Report a Documentation Bug](https://docs.ceph.com/en/pacific/ceph-volume/intro/)**

# Overview[¶](https://docs.ceph.com/en/pacific/ceph-volume/intro/ "Permalink to this headline")

ceph\-volumeツールは、論理ボリュームをOSDとしてデプロイする単一目的のコマンドラインツールを目指しており、OSDの準備、有効化、作成時にceph\-diskと同様のAPIを維持しようとします。

Ceph\-diskとは異なり、Cephにインストールされているudevルールと対話したり依存したりすることはありません。これらのルールにより、以前にセットアップされたデバイスを自動的に検出し、それをceph\-diskに送り込んで有効にすることができます。

# Replacing ceph\-disk[¶](https://docs.ceph.com/en/pacific/ceph-volume/intro/ "Permalink to this headline")

ceph\-diskツールは、プロジェクトが異なるタイプのinitシステム\(upstart, sysvinitなど\)をサポートしながらデバイスを検出できることが要求された時に作成されました。そのため、このツールは当初 \(その後も\) GPT パーティションに集中することになりました。特に、GPT GUIDは、次のような質問に答えるために、ユニークにデバイスをラベル付けするために使用されました：

* このデバイスはジャーナルですか？
* 暗号化されたデータパーティションですか？
* デバイスは部分的に準備された状態でしょうか？

これを解決するために、UDEVルールを使ってGUIDを照合し、ceph\-diskを呼び出して、ceph\-disk systemdユニットとceph\-disk executableの間で行ったり来たりすることになります。このプロセスは非常に不安定で時間がかかり\(OSDごとに3時間近いタイムアウトが必要でした\)、ノードのブートプロセス中にOSDが全く立ち上がらないという事態を引き起こしました。

UDEVの非同期動作のため、デバッグはおろか、問題を再現することさえ困難でした。

ceph\-diskの世界観はGPTパーティションに限定されていたため、LVMのような他の技術や、同様のデバイスマッパーデバイスでは動作しないことを意味します。最終的には、LVMのサポートから始めて、必要に応じて他の技術に拡張できるようなモジュール式のものを作ることにしました。

# GPT partitions are simple?[¶](https://docs.ceph.com/en/pacific/ceph-volume/intro/ "Permalink to this headline")

一般にパーティションは理屈が簡単ですが、ceph\-diskのパーティションは決して単純なものではありませんでした。デバイスディスカバリワークフローで正しく動作させるために、とてつもなく多くの特別なフラグが必要だったのです。以下は、データパーティションを作成するための呼び出しの例です：

```
/sbin/sgdisk --largest-new=1 --change-name=1:ceph data --partition-guid=1:f0fc39fd-eeb2-49f1-b922-a11939cf8a0f --typecode=1:89c57f98-2fe5-4dc0-89c1-f3ad0ceff2be --mbrtogpt -- /dev/sdb
```

これを作成するのが大変なだけでなく、これらのパーティションはデバイスをCephが独占的に所有する必要がありました。例えば、デバイスが暗号化されている場合、暗号化されていないキーを格納する特別なパーティションが作成されるケースがありました。これはceph\-diskのドメイン知識であり、「GPTパーティションは単純だ」という理解にはならないでしょう。その特殊なパーティションが作成される例を紹介します：

```
/sbin/sgdisk --new=5:0:+10M --change-name=5:ceph lockbox --partition-guid=5:None --typecode=5:fb3aabf9-d25f-47cc-bf5e-721d181642be --mbrtogpt -- /dev/sdad
```

# Modularity[¶](https://docs.ceph.com/en/pacific/ceph-volume/intro/ "Permalink to this headline")

ceph\-volumeはモジュール式のツールとして設計されています。なぜなら、私たちが考慮しなければならないハードウェアデバイスをプロビジョニングする方法がたくさんあることを想定しているからです。GPTパーティションを持つレガシーなceph\-diskデバイス\([simple](https://docs.ceph.com/en/pacific/ceph-volume/simple/#ceph-volume-simple)で処理\)と、lvmの2つが既に存在します。ユーザー空間から直接NVMeデバイスを管理するSPDKデバイスが当面の課題ですが、カーネルが全く関与しないため、LVMはそこで機能しないでしょう。

# ceph\-volume lvm[¶](https://docs.ceph.com/en/pacific/ceph-volume/intro/ "Permalink to this headline")

[LVMタグ](https://docs.ceph.com/en/pacific/glossary/#term-LVM-tags)を利用することにより、[lvm](https://docs.ceph.com/en/pacific/ceph-volume/lvm/#ceph-volume-lvm)サブコマンドは、OSDに関連付けられたデバイスを保存し、後で再検出してクエリを実行できるため、後でアクティブ化できます。

# LVM performance penalty[¶](https://docs.ceph.com/en/pacific/ceph-volume/intro/ "Permalink to this headline")

つまり、LVMへの変更に関連する重大なパフォーマンスの低下に気付くことはできませんでした。 LVMと緊密に連携できることで、他のデバイスマッパーテクノロジーと連携できるようになりました。論理ボリュームの下に配置できるものを操作するのに技術的な問題はありません。
