# 219: ceph-rbdnamer – udev helper to name RBD devices

 # ceph\-rbdnamer – udev helper to name RBD devices[¶](https://docs.ceph.com/en/pacific/man/8/ceph-rbdnamer/ "Permalink to this headline")

## Synopsis[¶](https://docs.ceph.com/en/pacific/man/8/ceph-rbdnamer/ "Permalink to this headline")

**ceph\-rbdnamer** _num_

## Description[¶](https://docs.ceph.com/en/pacific/man/8/ceph-rbdnamer/ "Permalink to this headline")

**ceph\-rbdnamer** prints the pool and image name for the given RBD devices to stdout. It is used by udev \(using a rule like the one below\) to set up a device symlink.

```
KERNEL=="rbd[0-9]*", PROGRAM="/usr/bin/ceph-rbdnamer %n", SYMLINK+="rbd/%c{1}/%c{2}"
```

## Availability[¶](https://docs.ceph.com/en/pacific/man/8/ceph-rbdnamer/ "Permalink to this headline")

**ceph\-rbdnamer** is part of Ceph, a massively scalable, open\-source, distributed storage system. Please refer to the Ceph documentation at [http://ceph.com/docs](https://docs.ceph.com/en/pacific/man/8/ceph-rbdnamer/) for more information.

## See also[¶](https://docs.ceph.com/en/pacific/man/8/ceph-rbdnamer/ "Permalink to this headline")

[rbd](https://docs.ceph.com/en/pacific/man/8/ceph-rbdnamer/)\(8\),[ceph](https://docs.ceph.com/en/pacific/man/8/ceph-rbdnamer/)\(8\)
