# 228: Multi-Site

**クリップソース:** [228: Multi\-Site — Ceph Documentation](https://docs.ceph.com/en/pacific/radosgw/multisite/)

# Multi\-Site[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

Jewelバージョンの新機能です。

シングルゾーン構成は、通常、1つのゾーンと1つ以上のceph\-radosgwインスタンスを含む 1つのゾーングループで構成され、インスタンス間でゲートウェイクライアント要求の負荷分散を 行うことができます。シングルゾーン構成では、通常、複数のゲートウェイインスタンスが1つのCephストレージクラスタを指します。ただし、KrakenはCeph Object Gatewayのマルチサイト構成オプションをいくつかサポートしています：

* **Multi\-zone:**
* **Multi\-zone\-group:**
* **Multiple Realms:**

ゾーングループ内のゾーン間のオブジェクトデータの複製は、以下のようになります：
![zone-sync2.png](image/zone-sync2.png)

クラスタの設定の詳細については、「[Ceph Object Gateway for Production](https://access.redhat.com/documentation/en-us/red_hat_ceph_storage/3/html/ceph_object_gateway_for_production/index/)」を参照してください。

## Functional Changes from Infernalis[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

Krakenでは、各Ceph Object Gatewayがアクティブ\-アクティブゾーン構成で動作するように設定し、非マスターゾーンへの書き込みを可能にすることができます。

マルチサイト構成は "realm" と呼ばれるコンテナ内に格納されます。レルムはゾーングループ、ゾーン、設定の変更を追跡するための複数のエポックを持つ時間 "period" を格納します。Krakenでは、ceph\-radosgwデーモンが同期を処理するため、別の同期エージェントは必要ありません。また、同期に対する新しいアプローチにより、Ceph Object Gatewayは「アクティブ\-パッシブ」ではなく「アクティブ\-アクティブ」構成で動作するようになりました。

## Requirements and Assumptions[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

マルチサイト構成では、少なくとも2つのCephストレージクラスタが必要で、個別のクラスタ名が付けられていることが望ましく、各Cephストレージクラスタに少なくとも1つのCephオブジェクトゲートウェイインスタンスが必要です。

このガイドでは、少なくとも2つのCephストレージクラスタが地理的に離れた場所にあることを前提としていますが、同じサイト内で構成することもできます。また、このガイドでは、rgw1およびrgw2という2つのCephオブジェクトゲートウェイサーバを想定しています。

重要：低遅延のWAN接続がない限り、シングルCephストレージクラスタ構成は推奨されません。

マルチサイト構成では、マスターゾーングループとマスターゾーンが必要です。さらに、各ゾーングループにはマスターゾーンが必要です。ゾーングループには、1つ以上のセカンダリーゾーン（非マスターゾーン）を持つことができます。

本ガイドでは、rgw1ホストがマスターゾーングループのマスターゾーンとして機能し、 rgw2ホストがマスターゾーングループのセカンダリーゾーンとして機能します。

Ceph Object Storageのプールの作成とチューニングの手順については、「[プール](https://docs.ceph.com/en/pacific/radosgw/pools)」を参照してください。

バケット同期ルールの設定方法については、「[同期ポリシー設定](https://docs.ceph.com/en/pacific/radosgw/multisite-sync-policy)」を参照してください。

## Configuring a Master Zone[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

マルチサイト構成のすべてのゲートウェイは、マスターゾーングループおよびマスターゾーン内のホスト上のceph\-radosgwデーモンから構成を取得します。 マルチサイト構成でゲートウェイを構成するには、ceph\-radosgwインスタンスを選択して、マスターゾーングループとマスターゾーンを構成します。

### Create a Realm[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

1つのレルムはゾーングループとゾーンのマルチサイト構成を含み、レルム内でグローバル名前空間を強制する役割も果たします。

マスターゾーングループおよびゾーンでサービスを提供することにしたホストのCLIを開き、マルチサイト構成の新規レルムを作成します：

```
# radosgw-admin realm create --rgw-realm={realm-name} [--default]
```

For example:

```
# radosgw-admin realm create --rgw-realm=movies --default
```

クラスタのレルムが1つの場合、\-\-defaultフラグを指定します。\-\-default を指定した場合、radosgw\-admin はデフォルトでこのレルムを使用します。\-\-default を指定しない場合、ゾーングループとゾーンを追加する際に、レルムを指定するために \-\-rgw\-realm フラグか \-\-realm\-id フラグを指定する必要があります。

レルムを作成すると、radosgw\-admin はレルムの設定をエコーバックします。例えば、以下のようになります：

```
{
    "id": "0956b174-fe14-4f97-8b50-bb7ec5e1cf62",
    "name": "movies",
    "current_period": "1950b710-3e63-4c41-a19e-46a715000980",
    "epoch": 1
}

```

注意: Cephはレルムに対して一意のIDを生成するため、必要に応じてレルムの名前を変更することができます。

### Create a Master Zone Group[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

レルムは少なくとも1つのゾーングループを持つ必要があり、これがレルムのマスターゾーングループとなります。

マスターゾーングループとゾーンでサービスを提供することにしたホスト上のCLIを開き、マルチサイト構成用の新規マスターゾーングループを作成します：

```
# radosgw-admin zonegroup create --rgw-zonegroup={name} --endpoints={url} [--rgw-realm={realm-name}|--realm-id={realm-id}] --master --default
```

For example:

```
# radosgw-admin zonegroup create --rgw-zonegroup=us --endpoints=http://rgw1:80 --rgw-realm=movies --master --default
```

レルムがシングルゾーングループのみを持つ場合、\-\-defaultフラグを指定します。\-\-default を指定した場合、radosgw\-admin は新規ゾーンを追加するとき、デフォルトでこのゾーングループを使用します。\-\-defaultを指定しない場合、ゾーンを追加したり変更したりするときに、ゾーングループを指定するために\-\-rgw\-zonegroupフラグか\-\-zonegroup\-idフラグを指定する必要があります。

マスターゾーングループの作成後、radosgw\-adminはゾーングループの設定をエコーバックします。例えば、以下のようになります：

```
{
    "id": "f1a233f5-c354-4107-b36c-df66126475a6",
    "name": "us",
    "api_name": "us",
    "is_master": "true",
    "endpoints": [
        "http://rgw1:80"
    ],
    "hostnames": [],
    "hostnames_s3webzone": [],
    "master_zone": "",
    "zones": [],
    "placement_targets": [],
    "default_placement": "",
    "realm_id": "0956b174-fe14-4f97-8b50-bb7ec5e1cf62"
}
```

### Create a Master Zone[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

重要: ゾーンは、ゾーン内のCeph Object Gatewayノードで作成する必要があります。

マスターゾーングループとゾーンでサービスを提供することにしたホスト上のCLIを開き、マルチサイト構成用の新規マスターゾーンを作成します：

```
# radosgw-admin zone create --rgw-zonegroup={zone-group-name} 
                            --rgw-zone={zone-name} 
                            --master --default 
                            --endpoints={http://fqdn}[,{http://fqdn}]
```

For example:

```
# radosgw-admin zone create --rgw-zonegroup=us --rgw-zone=us-east 
                            --master --default 
                            --endpoints={http://fqdn}[,{http://fqdn}]
```

注：\-\-access\-keyと\-\-secretは指定しません。これらの設定は、次のセクションでユーザーを作成するとゾーンに追加されます。

重要：以下の手順は、新規インストールしたシステムを使用するマルチサイト構成を想定しています。すでにデータを保存するために使用している場合は、defaultゾーンとそのプールを削除しないでください。

### Delete Default Zone Group and Zone[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

defaultゾーンが存在する場合は削除します。 最初にdefaultゾーングループから削除してください。

```
# radosgw-admin zonegroup remove --rgw-zonegroup=default --rgw-zone=default
# radosgw-admin period update --commit
# radosgw-admin zone rm --rgw-zone=default
# radosgw-admin period update --commit
# radosgw-admin zonegroup delete --rgw-zonegroup=default
# radosgw-admin period update --commit
```

最後に、Cephストレージクラスタのdefaultプールが存在する場合は削除します。

重要：次のステップは、新規インストールしたシステムを使用するマルチサイト構成を想定しています。すでにデータを保存するために使用している場合は、defaultプールを削除しないでください。

```
# ceph osd pool rm default.rgw.control default.rgw.control --yes-i-really-really-mean-it
# ceph osd pool rm default.rgw.data.root default.rgw.data.root --yes-i-really-really-mean-it
# ceph osd pool rm default.rgw.gc default.rgw.gc --yes-i-really-really-mean-it
# ceph osd pool rm default.rgw.log default.rgw.log --yes-i-really-really-mean-it
# ceph osd pool rm default.rgw.users.uid default.rgw.users.uid --yes-i-really-really-mean-it
```

### Create a System User[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

ceph\-radosgwデーモンは、レルムとperiodの情報を引き出す前に認証する必要があります。マスターゾーンで、デーモン間の認証を容易にするためにsystemユーザーを作成します。

```
# radosgw-admin user create --uid="{user-name}" --display-name="{Display Name}" --system
```

For example:

```
# radosgw-admin user create --uid="synchronization-user" --display-name="Synchronization User" --system
```

セカンダリゾーンはマスターゾーンで認証するために、access\_keyとsecret\_keyを必要としますので、メモしておいてください。

最後に、systemユーザーをマスターゾーンに追加します。

```
# radosgw-admin zone modify --rgw-zone=us-east --access-key={access-key} --secret={secret}
# radosgw-admin period update --commit
```

### Update the Period[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

マスターゾーンの設定を更新した後、periodを更新してください。

```
# radosgw-admin period update --commit
```

注：periodを更新するとエポックが変更され、更新された設定を他のゾーンが確実に受け取れるようになります。

### Update the Ceph Configuration File[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

マスターゾーンのホストのCeph設定ファイルを更新してインスタンスエントリにrgw\_zone設定オプションとマスターゾーンの名前を追加します。

```
[client.rgw.{instance-name}]
...
rgw_zone={zone-name}
```

For example:

```
[client.rgw.rgw1]
host = rgw1
rgw frontends = "civetweb port=80"
rgw_zone=us-east
```

### Start the Gateway[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

オブジェクトゲートウェイホストで、Ceph Object Gatewayサービスを起動し、有効にします：

```
# systemctl start ceph-radosgw@rgw.`hostname -s`
# systemctl enable ceph-radosgw@rgw.`hostname -s`
```

## Configure Secondary Zones[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

ゾーングループ内のゾーンは、すべてのデータをレプリケートし、各ゾーンが同じデータを持つことを保証します。セカンダリーゾーンを作成する場合、セカンダリーゾーンを提供することにしたホスト上で次のすべての操作を実行します。

注：3つ目のゾーンを追加する場合は、2つ目のゾーンの追加と同じ手順で行ってください。異なるゾーン名を使用します。

重要：ユーザー作成などのメタデータ操作は、マスターゾーン内のホストで実行する必要があります。マスターゾーンとセカンダリーゾーンはバケット操作を受け取ることができますが、セカンダリーゾーンはバケット操作をマスターゾーンにリダイレクトします。マスターゾーンがダウンしている場合、バケット操作は失敗します。

### Pull the Realm[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

マスターゾーングループ内のマスターゾーンのURLパス、アクセスキー、シークレットを使用して、レルム構成をホストに取り込みます。デフォルト以外のレルムを取得するには、\-\-rgw\-realmフラグか\-\-realm\-id フラグでレルムを指定します。

```
# radosgw-admin realm pull --url={url-to-master-zone-gateway} --access-key={access-key} --secret={secret}
```

注意: レルムを取り込むと、リモートのカレントperiod設定も取得され、このホストのカレントperiodになります。

このレルムがdefaultレルムか唯一のレルムである場合、レルムをdefaultレルムにします。

```
# radosgw-admin realm default --rgw-realm={realm-name}
```

### Create a Secondary Zone[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

重要: ゾーンは、ゾーン内のCeph Object Gatewayノードで作成する必要があります。

セカンダリーゾーンを提供することにしたホスト上のCLIを開き、マルチサイト構成のセカンダリーゾーンを作成します。ゾーングループID、新しいゾーン名、ゾーンのエンドポイントを指定します。\-\-masterフラグと\-\-defaultフラグは使用しません。Krakenでは、すべてのゾーンがデフォルトでアクティブ\-アクティブ構成で走行します。つまり、ゲートウェイクライアントは任意のゾーンにデータを書き込むことができ、そのゾーンはゾーングループ内の他のすべてのゾーンにデータをレプリケートします。セカンダリゾーンが書き込み操作を受け入れるべきではない場合、マスターゾーンと セカンダリゾーンをアクティブ\-パッシブ構成にするために \-\-read\-onlyフラグを指定します。さらに、マスターゾーングループのマスターゾーンに格納されている、生成されたsysytemユーザーの access\_key と secret\_key を提供します：

```
# radosgw-admin zone create --rgw-zonegroup={zone-group-name}
                            --rgw-zone={zone-name} --endpoints={url} 
                            --access-key={system-key} --secret={secret}
                            --endpoints=http://{fqdn}:80 
                            [--read-only]
```

For example:

```
# radosgw-admin zone create --rgw-zonegroup=us --rgw-zone=us-west 
                            --access-key={system-key} --secret={secret} 
                            --endpoints=http://rgw2:80
```

重要：以下の手順は、新規インストールされたシステムを使用するマルチサイト構成を想定しています。すでにデータを保存するために使用している場合は、defaultゾーンとそのプールを削除しないでください。

必要に応じて、defaultゾーンを削除してください。

```
# radosgw-admin zone rm --rgw-zone=default
```

最後に、必要に応じて、Cephストレージクラスタのdefaultプールを削除します。

```
# ceph osd pool rm default.rgw.control default.rgw.control --yes-i-really-really-mean-it
# ceph osd pool rm default.rgw.data.root default.rgw.data.root --yes-i-really-really-mean-it
# ceph osd pool rm default.rgw.gc default.rgw.gc --yes-i-really-really-mean-it
# ceph osd pool rm default.rgw.log default.rgw.log --yes-i-really-really-mean-it
# ceph osd pool rm default.rgw.users.uid default.rgw.users.uid --yes-i-really-really-mean-it
```

### Update the Ceph Configuration File[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

セカンダリゾーンホストのCeph設定ファイルを更新してインスタンスエントリにrgw\_zone設定オプションとセカンダリゾーンの名前を追加します。

```
[client.rgw.{instance-name}]
...
rgw_zone={zone-name}
```

For example:

```
[client.rgw.rgw2]
host = rgw2
rgw frontends = "civetweb port=80"
rgw_zone=us-west
```

### Update the Period[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

マスターゾーンの設定を更新した後、periodを更新してください。

```
# radosgw-admin period update --commit
```

注：periodを更新するとエポックが変更され、更新された設定を他のゾーンが確実に受け取れるようになります。

### Start the Gateway[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

オブジェクトゲートウェイホストで、Ceph Object Gatewayサービスを起動し、有効にします：

```
# systemctl start ceph-radosgw@rgw.`hostname -s`
# systemctl enable ceph-radosgw@rgw.`hostname -s`
```

### Check Synchronization Status[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

セカンダリーゾーンが立ち上がったら、同期の状況を確認します。同期により、マスターゾーンで作成されたユーザーとバケットがセカンダリーゾーンにコピーされます。

```
# radosgw-admin sync status
```

出力は、同期操作のステータスを提供します。例：

```
realm f3239bc5-e1a8-4206-a81d-e1576480804d (earth)
    zonegroup c50dbb7e-d9ce-47cc-a8bb-97d9b399d388 (us)
         zone 4c453b70-4a16-4ce8-8185-1893b05d346e (us-west)
metadata sync syncing
              full sync: 0/64 shards
              metadata is caught up with master
              incremental sync: 64/64 shards
    data sync source: 1ee9da3e-114d-4ae3-a8a4-056e8a17f532 (us-east)
                      syncing
                      full sync: 0/128 shards
                      incremental sync: 128/128 shards
                      data is caught up with source
```

注：セカンダリゾーンはバケット操作を受け入れます。 ただし、セカンダリゾーンはバケット操作をマスターゾーンにリダイレクトしてからマスターゾーンと同期して、バケット操作の結果を受け取ります。 マスターゾーンがダウンしている場合、セカンダリゾーンで実行されたバケット操作は失敗しますが、オブジェクト操作は成功するはずです。

## Maintenance[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

### Checking the Sync Status[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

ゾーンのレプリケーション状況についての情報は、以下の方法で照会できます：

```
$ radosgw-admin sync status
        realm b3bc1c37-9c44-4b89-a03b-04c269bea5da (earth)
    zonegroup f54f9b22-b4b6-4a0e-9211-fa6ac1693f49 (us)
         zone adce11c9-b8ed-4a90-8bc5-3fc029ff0816 (us-2)
        metadata sync syncing
              full sync: 0/64 shards
              incremental sync: 64/64 shards
              metadata is behind on 1 shards
              oldest incremental change not applied: 2017-03-22 10:20:00.0.881361s
    data sync source: 341c2d81-4574-4d08-ab0f-5a2a7b168028 (us-1)
                      syncing
                      full sync: 0/128 shards
                      incremental sync: 128/128 shards
                      data is caught up with source
              source: 3b5d1a3f-3f27-4e4a-8f34-6072d4bb1275 (us-3)
                      syncing
                      full sync: 0/128 shards
                      incremental sync: 128/128 shards
                      data is caught up with source
```

### Changing the Metadata Master Zone[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

重要：どのゾーンがメタデータマスターであるかを変更するときは注意が必要です。 ゾーンが現在のマスターゾーンからのメタデータの同期を完了していない場合、マスターに昇格したときに残りのエントリを提供できず、それらの変更は失われます。 このため、ゾーンのradosgw\-admin sync statusがメタデータの同期に追いつくのを待ってから、マスターにプロモートすることを推奨します。

同様に、メタデータへの変更が現在のマスターゾーンによって処理されているときに、別のゾーンがマスターにプロモートされている場合、それらの変更は失われる可能性があります。 これを回避するには、前のマスターゾーンのradosgwインスタンスをシャットダウンすることを推奨します。 別のゾーンをプロモートした後、その新しいperiodをradosgw\-admin period pullでフェッチして、ゲートウェイを再起動できます。

あるゾーン（たとえばzonegroup usのzone us\-2）をメタデータマスターに昇格させるには、そのゾーンで次のコマンドを実行します：

```
$ radosgw-admin zone modify --rgw-zone=us-2 --master
$ radosgw-admin zonegroup modify --rgw-zonegroup=us --master
$ radosgw-admin period update --commit
```

これは新しいperiodを生成し、ゾーンus\-2のradosgwインスタンスはこのperiodを他のゾーンに送信します。

## Failover and Disaster Recovery[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

マスターゾーンに一障害が発生した場合は、セカンダリーゾーンにフェイルオーバーしてディザスターリカバリーを行います。

1. セカンダリーゾーンをマスターゾーンおよびdefaultゾーンにします：

```
# radosgw-admin zone modify --rgw-zone={zone-name} --master --default
```

デフォルトでは、Ceph Object Gatewayはアクティブ\-アクティブ構成で走行します。クラスタがアクティブ\-パッシブ構成に設定されている場合、セカンダリゾーンはread\-onlyゾーンになります。ゾーンが書き込み操作を受け取れるようにするには、\-\-read\-onlyのステータスを削除します：

```
# radosgw-admin zone modify --rgw-zone={zone-name} --master --default \
                            --read-only=false
```

1. periodを更新して、変更を有効にします。

```
# radosgw-admin period update --commit
```

1. 最後に、Ceph Object Gatewayを再起動します。

```
# systemctl restart ceph-radosgw@rgw.`hostname -s`
```

旧マスターゾーンが回復した場合は、操作を元に戻します。

1. 現在のマスターゾーンから最新のレルム構成を取り出します。

```
# radosgw-admin realm pull --url={url-to-master-zone-gateway} \
                           --access-key={access-key} --secret={secret}
```

1. 復旧したゾーンをマスターゾーンとdefaultゾーンにします。

```
# radosgw-admin zone modify --rgw-zone={zone-name} --master --default
```

1. periodを更新して、変更を有効にします。

```
# radosgw-admin period update --commit
```

1. 復旧したゾーンでCeph Object Gatewayを再起動します。

```
# systemctl restart ceph-radosgw@rgw.`hostname -s`
```

1. セカンダリーゾーンをread\-onlyに設定する必要がある場合は、セカンダリーゾーンを更新します。

```
# radosgw-admin zone modify --rgw-zone={zone-name} --read-only
```

1. periodを更新して、変更を有効にします。

```
# radosgw-admin period update --commit
```

1. 最後に、セカンダリゾーンのCeph Object Gatewayを再起動します。

```
# systemctl restart ceph-radosgw@rgw.`hostname -s`
```

## Migrating a Single Site System to Multi\-Site[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

defaultゾーングループとゾーンを持つシングルサイトシステムからマルチサイトシステムに移行するには、次の手順を使用します：

1. レルムを作成します。\<name\>をレルム名に置き換えてください。

```
# radosgw-admin realm create --rgw-realm=<name> --default
```

1. defaultゾーンとゾーングループの名前を変更します。\<name\>をゾーングループ名またはゾーン名に置き換えてください。

```
# radosgw-admin zonegroup rename --rgw-zonegroup default --zonegroup-new-name=<name>
# radosgw-admin zone rename --rgw-zone default --zone-new-name us-east-1 --rgw-zonegroup=<name>
```

1. マスターゾーングループを設定します。\<name\>をレルム名またはゾーングループ名に置き換えてください。\<fqdn\>をゾーングループ内の完全修飾ドメイン名に置き換えます。

```
# radosgw-admin zonegroup modify --rgw-realm=<name> --rgw-zonegroup=<name> --endpoints http://<fqdn>:80 --master --default
```

1. マスターゾーンを設定します。\<name\>をレルム、ゾーングループ、またはゾーン名に置き換えてください。\<fqdn\>をゾーングループ内の完全修飾ドメイン名に置き換えます。

```
# radosgw-admin zone modify --rgw-realm=<name> --rgw-zonegroup=<name> \
                            --rgw-zone=<name> --endpoints http://<fqdn>:80 \
                            --access-key=<access-key> --secret=<secret-key> \
                            --master --default
```

1. systemユーザーを作成します。\<user\-id\>をユーザー名に置き換えてください。\<display\-name\>を表示名に置き換えてください。スペースが含まれていてもかまいません。

```
# radosgw-admin user create --uid=<user-id> --display-name="<display-name>"\
                            --access-key=<access-key> --secret=<secret-key> --system
```

1. 更新された設定をコミットします。

```
# radosgw-admin period update --commit
```

1. 最後に、Ceph Object Gatewayを再起動します。

```
# systemctl restart ceph-radosgw@rgw.`hostname -s`
```

この手順が完了したら、[Configure a Secondary Zone](https://docs.ceph.com/en/pacific/radosgw/multisite/#configure-secondary-zones)に進み、マスターゾーングループにセカンダリーゾーンを作成します。

## Multi\-Site Configuration Reference[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

以下のセクションでは、レルム、period、ゾーングループ、ゾーンの詳細とコマンドラインの使用方法について説明します。

### Realms[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

レルムは、1つ以上のゾーンを含む1つ以上のゾーングループと、オブジェクトを含むバケットを含むゾーンで構成される、グローバル名前空間を表します。 レルムを使用すると、Ceph ObjectGatewayは同じハードウェア上で複数の名前空間をサポートできます。

レルムはperiodという概念を含んでいます。各periodは、ゾーングループとゾーン設定の時間的な状態を表します。ゾーングループやゾーンに変更を加えるたびに、periodを更新し、コミットします。

デフォルトでは、Ceph Object Gatewayは、Infernalis以前のリリースとの後方互換性のため、レルムを作成しません。ただし、ベストプラクティスとして、新しいクラスタにはレルムを作成することをお勧めします。

#### Create a Realm[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

レルムを作成するには、realm createを実行し、レルム名を指定します。レルムがdefaultである場合は、\-\-defaultを指定します。

```
# radosgw-admin realm create --rgw-realm={realm-name} [--default]
```

For example:

```
# radosgw-admin realm create --rgw-realm=movies --default
```

\-\-default を指定すると、\-\-rgw\-realm とレルム名が明示的に提供されない限り、各 radosgw\-admin の呼び出しで当該レルムが暗黙的に呼び出されます。

#### Make a Realm the Default[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

レルム一覧の中の1つのレルムをdefaultのレルムとする必要があります。defaultのレルムは唯一のレルムかもしれません。レルムがひとつしかなく、作成時にdefaultレルムとして指定していない場合は、それをdefaultレルムとします。あるいは、どのレルムをdefaultとするかを変更するには、次のコマンドを実行します：

```
# radosgw-admin realm default --rgw-realm=movies
```

注意：レルムがdefaultの場合、コマンドラインは\-\-rgw\-realm=\<realm\-name\>を引数として想定しています。

#### Delete a Realm[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

レルムを削除するには、realm deleteを実行し、レルム名を指定します。

```
# radosgw-admin realm delete --rgw-realm={realm-name}
```

For example:

```
# radosgw-admin realm delete --rgw-realm=movies
```

#### Get a Realm[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

レルムを取得する場合は、realm getを実行し、レルム名を指定します。

```
#radosgw-admin realm get --rgw-realm=<name>
```

For example:

```
# radosgw-admin realm get --rgw-realm=movies [> filename.json]
```

CLIは、レルムプロパティを含むJSONオブジェクトをエコーします。

```
{
    "id": "0a68d52e-a19c-4e8e-b012-a8f831cb3ebc",
    "name": "movies",
    "current_period": "b0c5bbef-4337-4edd-8184-5aeab2ec413b",
    "epoch": 1
}
```

JSONオブジェクトをファイルに出力する場合は、\>と出力ファイル名を使用します。

#### Set a Realm[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

レルムを設定するには，realm setを実行し，レルム名を指定し，\-\-infile=で入力ファイル名を指定します。

```
#radosgw-admin realm set --rgw-realm=<name> --infile=<infilename>
```

For example:

```
# radosgw-admin realm set --rgw-realm=movies --infile=filename.json
```

#### List Realms[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

レルムの一覧を表示するには、realm listを実行します。

```
# radosgw-admin realm list
```

#### List Realm Periods[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

レルムperiodを一覧表示するには、realm list\-periods を実行します。

```
# radosgw-admin realm list-periods
```

#### Pull a Realm[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

マスターゾーングループとマスターゾーンを含むノードからセカンダリゾーングループまたはゾーンを含むノードにレルムを取り出すには、レルム構成を受け取るノードでrealm pullを実行します。

```
# radosgw-admin realm pull --url={url-to-master-zone-gateway} --access-key={access-key} --secret={secret}
```

#### Rename a Realm[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

レルムはperiodには含まれません。その結果、レルムの名前の変更はローカルにのみ適用され、 realm pull で取り出されません。複数のゾーンを持つレルムの名前を変更する場合は、各ゾーンに対してコマンドを実行します。レルムの名前を変更するには、次のように実行します：

```
# radosgw-admin realm rename --rgw-realm=<current-name> --realm-new-name=<new-realm-name>
```

注意: name パラメータを変更するために realm set を使用しないでください。これは内部的な名前のみを変更します。rgw\-realm を指定すると、古いレルム名を使用することになります。

### Zone Groups[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

Ceph Object Gatewayは、ゾーングループの概念を使用することで、マルチサイトのデプロイメントとグローバル名前空間をサポートしています。以前はInfernalisでリージョンと呼ばれていたゾーングループは、1つまたは複数のゾーン内の1つまたは複数のCeph Object Gatewayインスタンスの地理的な位置を定義しています。

ゾーングループの設定は、すべての設定がCeph設定ファイルに収まるわけではないため、通常の設定手順とは異なります。ゾーングループの一覧表示、ゾーングループ設定の取得（get）、ゾーングループ設定の設定（set）を行うことができます。

#### Create a Zone Group[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

ゾーングループの作成は、ゾーングループ名を指定することで行います。ゾーンの作成は、\-\-rgw\-realm=\<realm\-name\> が指定されない限り、 defaultのレルムを仮定しています。ゾーングループがdefaultのゾーングループである場合、\-\-defaultフラグを指定します。ゾーングループがマスターゾーングループの場合は、\-\-masterフラグを指定します。たとえば、以下のようになります：

```
# radosgw-admin zonegroup create --rgw-zonegroup=<name> [--rgw-realm=<name>][--master] [--default]
```

注：既存のゾーングループの設定を変更する場合は、zonegroup modify \-\-rgw\-zonegroup=\<zonegroup\-name\>を使用します。

#### Make a Zone Group the Default[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

ゾーングループのリスト内の1つのゾーングループをdefaultのゾーングループとする必要があります。defaultのゾーングループが唯一のゾーングループであってもかまいません。ゾーングループが1つしかなく、作成時にdefaultゾーングループとして指定していない場合は、それをdefaultゾーングループにします。または、どのzonegroupをデフォルトにするかを変更するには、以下を実行します：

```
# radosgw-admin zonegroup default --rgw-zonegroup=comedy
```

注：ゾーングループがdefaultの場合、コマンドラインは \-\-rgw\-zonegroup=\<zonegroup\-name\> を引数として想定しています。

periodを更新します：

```
# radosgw-admin period update --commit
```

#### Add a Zone to a Zone Group[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

ゾーンをゾーングループに追加するには、次のように実行します：

```
# radosgw-admin zonegroup add --rgw-zonegroup=<name> --rgw-zone=<name>
```

periodを更新します：

```
# radosgw-admin period update --commit
```

#### Remove a Zone from a Zone Group[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

ゾーングループからゾーンを削除するには、次のように実行します：

```
# radosgw-admin zonegroup remove --rgw-zonegroup=<name> --rgw-zone=<name>
```

periodを更新します：

```
# radosgw-admin period update --commit
```

#### Rename a Zone Group[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

ゾーングループの名前を変更するには、次のように実行します：

```
# radosgw-admin zonegroup rename --rgw-zonegroup=<name> --zonegroup-new-name=<name>
```

periodを更新します：

```
# radosgw-admin period update --commit
```

#### Delete a Zone Group[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

ゾーングループを削除するには、次のように実行します：

```
# radosgw-admin zonegroup delete --rgw-zonegroup=<name>
```

periodを更新します：

```
# radosgw-admin period update --commit
```

#### List Zone Groups[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

Cephクラスタには、ゾーングループのリストが含まれています。ゾーングループの一覧を表示するには、以下を実行します：

```
# radosgw-admin zonegroup list
```

radosgw\-admin は、JSON 形式のゾーングループ一覧を返します。

```
{
    "default_info": "90b28698-e7c3-462c-a42d-4aa780d24eda",
    "zonegroups": [
        "us"
    ]
}
```

#### Get a Zone Group Map[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

各ゾーングループの詳細を一覧表示する場合は：

```
# radosgw-admin zonegroup-map get
```

注：failed to read zonegroup map エラーが発生した場合、まず root で radosgw\-admin zonegroup\-map update を実行します。

#### Get a Zone Group[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

ゾーングループの設定を表示するには：

```
radosgw-admin zonegroup get [--rgw-zonegroup=<zonegroup>]
```

ゾーングループの構成は次のようになります：

```
{
    "id": "90b28698-e7c3-462c-a42d-4aa780d24eda",
    "name": "us",
    "api_name": "us",
    "is_master": "true",
    "endpoints": [
        "http://rgw1:80"
    ],
    "hostnames": [],
    "hostnames_s3website": [],
    "master_zone": "9248cab2-afe7-43d8-a661-a40bf316665e",
    "zones": [
        {
            "id": "9248cab2-afe7-43d8-a661-a40bf316665e",
            "name": "us-east",
            "endpoints": [
                "http://rgw1"
            ],
            "log_meta": "true",
            "log_data": "true",
            "bucket_index_max_shards": 0,
            "read_only": "false"
        },
        {
            "id": "d1024e59-7d28-49d1-8222-af101965a939",
            "name": "us-west",
            "endpoints": [
                "http://rgw2:80"
            ],
            "log_meta": "false",
            "log_data": "true",
            "bucket_index_max_shards": 0,
            "read_only": "false"
        }
    ],
    "placement_targets": [
        {
            "name": "default-placement",
            "tags": []
        }
    ],
    "default_placement": "default-placement",
    "realm_id": "ae031368-8715-4e27-9a99-0c9468852cfe"
}
```

#### Set a Zone Group[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

ゾーングループの定義は、必要な設定を指定して、JSONオブジェクトを作成することから始まります：

1. name: ゾーングループの名前。必須
2. api\_name: ゾーングループのAPI名。オプション
3. is\_master: ゾーングループがマスターゾーングループであるかどうか。必須。注：マスターゾーングループは1つしか持てない
4. endpoints: ゾーングループ内のすべてのエンドポイントのリスト。例えば、同じゾーングループを参照するために、複数のドメイン名を使用することができる。フォワードスラッシュ\(/\)を忘れずにエスケープすること。また、各エンドポイントにポート（fqdn:port）を指定することができる。オプション
5. hostnames: ゾーングループに含まれるすべてのホストネームのリスト。たとえば、同じゾーングループを参照するために、複数のドメイン名を使用することができる。オプション。rgw dns nameの設定は、自動的にこのリストに含まれるようになる。この設定を変更した後、ゲートウェイデーモンを再起動する必要がある
6. master\_zone: ゾーングループのマスターゾーン。オプション。指定しない場合はdefaultのゾーンを使用する。注：マスターゾーンは1つのゾーングループに1つしか設定できない
7. zones: ゾーングループ内の全ゾーンのリスト。各ゾーンは、名前（必須）、エンドポイントのリスト（オプション）、ゲートウェイがメタデータとデータ操作のログを記録するかどうか（デフォルトではfalse）を持っている
8. placement\_targets: プレイスメントターゲットのリスト（オプション）。各プレイスメントターゲットは、名前（必須）とタグのリスト（任意）を含み、（ユーザー情報のplacement\_tagsフィールドに）そのタグを持つユーザーのみがプレイスメントターゲットを使用できるようにする
9. default\_placement: オブジェクトインデックスとオブジェクトデータのデフォルトの配置先。デフォルトでdefault\-placementに設定されています。また、各ユーザーのユーザー情報にユーザーごとのデフォルト配置先を設定することも可能

ゾーングループを設定するには、必要なフィールドからなるJSONオブジェクトを作成し、ファイル（例：zonegroup.json）に保存した後、以下のコマンドを実行してください：

```
# radosgw-admin zonegroup set --infile zonegroup.json
```

ここで、zonegroup.jsonは作成したJSONファイルです。

重要：defaultゾーングループのis\_master設定は、デフォルトでtrueです。新しいゾーングループを作成し、それをマスターゾーングループにしたい場合、defaultゾーングループのis\_master設定をfalseに設定するか、defaultゾーングループを削除する必要があります。

periodを更新します：

```
# radosgw-admin period update --commit
```

#### Set a Zone Group Map[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

ゾーングループマップの設定は、1つ以上のゾーングループからなるJSONオブジェクトを作成し、クラスタのmaster\_zonegroupを設定することで行われます。ゾーングループマップの各ゾーングループは、キーと値のペアで構成され、キー設定は個々のゾーングループ設定の名前設定に相当し、値は個々のゾーングループ設定からなるJSONオブジェクトです。

is\_masterがtrueのゾーングループは1つだけ持つことができ、 それはゾーングループマップの最後にmaster\_zonegroupとして指定する必要があります。以下のJSONオブジェクトは、defaultゾーングループマップの例です：

```
{
    "zonegroups": [
        {
            "key": "90b28698-e7c3-462c-a42d-4aa780d24eda",
            "val": {
                "id": "90b28698-e7c3-462c-a42d-4aa780d24eda",
                "name": "us",
                "api_name": "us",
                "is_master": "true",
                "endpoints": [
                    "http://rgw1:80"
                ],
                "hostnames": [],
                "hostnames_s3website": [],
                "master_zone": "9248cab2-afe7-43d8-a661-a40bf316665e",
                "zones": [
                    {
                        "id": "9248cab2-afe7-43d8-a661-a40bf316665e",
                        "name": "us-east",
                        "endpoints": [
                            "http://rgw1"
                        ],
                        "log_meta": "true",
                        "log_data": "true",
                        "bucket_index_max_shards": 0,
                        "read_only": "false"
                    },
                    {
                        "id": "d1024e59-7d28-49d1-8222-af101965a939",
                        "name": "us-west",
                        "endpoints": [
                            "http://rgw2:80"
                        ],
                        "log_meta": "false",
                        "log_data": "true",
                        "bucket_index_max_shards": 0,
                        "read_only": "false"
                    }
                ],
                "placement_targets": [
                    {
                        "name": "default-placement",
                        "tags": []
                    }
                ],
                "default_placement": "default-placement",
                "realm_id": "ae031368-8715-4e27-9a99-0c9468852cfe"
            }
        }
    ],
    "master_zonegroup": "90b28698-e7c3-462c-a42d-4aa780d24eda",
    "bucket_quota": {
        "enabled": false,
        "max_size_kb": -1,
        "max_objects": -1
    },
    "user_quota": {
        "enabled": false,
        "max_size_kb": -1,
        "max_objects": -1
    }
}
```

ゾーングループマップを設定するには、次のように実行します：

```
# radosgw-admin zonegroup-map set --infile zonegroupmap.json
```

ここで、zonegroupmap.json は作成した JSON ファイルです。ゾーングループマップで指定したものに対して、ゾーンが作成されていることを確認します。最後に、periodを更新します。

```
# radosgw-admin period update --commit
```

### Zones[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

Ceph Object Gatewayは、ゾーンの概念をサポートしています。ゾーンは、1つまたは複数のCeph Object Gatewayインスタンスで構成される論理グループを定義します。

ゾーンの設定は、すべての設定がCeph設定ファイルに収まるわけではないので、一般的な設定手順とは異なります。ゾーンの一覧表示、ゾーン設定の取得（get）、ゾーン設定の設定（set）を行うことができます。

#### Create a Zone[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

ゾーンを作成するには、ゾーン名を指定します。マスターゾーンの場合は、\-\-masterオプションを指定します。ゾーングループ内の1つのゾーンだけがマスターゾーンになることができます。ゾーンをゾーングループに追加するには、\-\-rgw\-zonegroupオプションにゾーングループ名を指定します。

```
# radosgw-admin zone create --rgw-zone=<name> 
                [--zonegroup=<zonegroup-name]
                [--endpoints=<endpoint>[,<endpoint>] 
                [--master] [--default] 
                --access-key $SYSTEM_ACCESS_KEY --secret $SYSTEM_SECRET_KEY
```

periodを更新します：

```
# radosgw-admin period update --commit
```

#### Delete a Zone[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

ゾーンを削除するには、まずゾーングループから削除します。

```
# radosgw-admin zonegroup remove --zonegroup=<name>
                                 --zone=<name>
```

periodを更新します：

```
# radosgw-admin period update --commit
```

次に、ゾーンを削除します。以下を実行します：

```
# radosgw-admin zone rm --rgw-zone<name>
```

最後に、periodを更新します：

```
# radosgw-admin period update --commit
```

重要：最初にゾーングループからゾーンを削除せずにゾーンを削除しないでください。 そうしないと、periodの更新は失敗します。

削除されたゾーンのプールが他の場所で使用されない場合、プールの削除を検討してください。以下の例の\<del\-zone\>を削除したゾーンの名前に置き換えてください。

重要：ゾーン名が付加されたプールのみを削除してください。 .rgw.rootなどのルートプールを削除すると、システムのすべての構成が削除されます。

重要：プールが削除されると、その中のデータはすべて復元不可能な状態で削除されます。プールの削除は、プールの内容が不要になった場合のみ行ってください。

```
# ceph osd pool rm <del-zone>.rgw.control <del-zone>.rgw.control --yes-i-really-really-mean-it
# ceph osd pool rm <del-zone>.rgw.data.root <del-zone>.rgw.data.root --yes-i-really-really-mean-it
# ceph osd pool rm <del-zone>.rgw.gc <del-zone>.rgw.gc --yes-i-really-really-mean-it
# ceph osd pool rm <del-zone>.rgw.log <del-zone>.rgw.log --yes-i-really-really-mean-it
# ceph osd pool rm <del-zone>.rgw.users.uid <del-zone>.rgw.users.uid --yes-i-really-really-mean-it
```

#### Modify a Zone[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

ゾーンを変更するには、ゾーン名と変更したいパラメータを指定します。

```
# radosgw-admin zone modify [options]
```

Where \[options\]:

* \-\-access\-key=\<key\>
* \-\-secret/\-\-secret\-key=\<key\>
* \-\-master
* \-\-default
* \-\-endpoints=\<list\>

periodを更新します：

```
# radosgw-admin period update --commit
```

#### List Zones[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

rootで、クラスタ内のゾーンを一覧表示するには：

```
# radosgw-admin zone list
```

#### Get a Zone[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

rootで、ゾーンの設定を取得するには：

```
# radosgw-admin zone get [--rgw-zone=<zone>]
```

defaultのゾーンはこのようになっています：

```
{ "domain_root": ".rgw",
  "control_pool": ".rgw.control",
  "gc_pool": ".rgw.gc",
  "log_pool": ".log",
  "intent_log_pool": ".intent-log",
  "usage_log_pool": ".usage",
  "user_keys_pool": ".users",
  "user_email_pool": ".users.email",
  "user_swift_pool": ".users.swift",
  "user_uid_pool": ".users.uid",
  "system_key": { "access_key": "", "secret_key": ""},
  "placement_pools": [
      {  "key": "default-placement",
         "val": { "index_pool": ".rgw.buckets.index",
                  "data_pool": ".rgw.buckets"}
      }
    ]
  }
```

#### Set a Zone[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

ゾーンの構成には、一連のCeph Object Gatewayプールの指定が含まれます。一貫性を保つため、ゾーン名と同じプールプレフィックスを使用することをお勧めします。プールの設定の詳細については、「[プール](http://docs.ceph.com/en/latest/rados/operations/pools/#pools)」を参照してください。

ゾーンを設定するには、プールからなるJSONオブジェクトを作成しファイル（例：zone.json）に保存し、{zone\-name}をゾーン名に置き換えて、以下のコマンドを実行します：

```
# radosgw-admin zone set --rgw-zone={zone-name} --infile zone.json
```

ここで、zone.jsonは作成したJSONファイルです。

そして、rootで、periodを更新します：

```
# radosgw-admin period update --commit
```

#### Rename a Zone[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

ゾーン名を変更するには、ゾーン名と新しいゾーン名を指定します。

```
# radosgw-admin zone rename --rgw-zone=<name> --zone-new-name=<name>
```

periodを更新します：

```
# radosgw-admin period update --commit
```

### Zone Group and Zone Settings[¶](https://docs.ceph.com/en/pacific/radosgw/multisite/ "Permalink to this headline")

defaultのゾーングループとゾーンを構成する場合、プール名にはゾーン名が含まれます。例えば：

* default.rgw.control

defaultを変更するには、Ceph設定ファイルの各\[client.radosgw.{instance\-name}\]インスタンスの下に次の設定を含めます。

|Name                                |Description                                                                 |Type  |Default          |
|------------------------------------|----------------------------------------------------------------------------|------|-----------------|
|rgw\_zone                           |ゲートウェイインスタンスのゾーン名                                          |String|None             |
|rgw\_zonegroup                      |ゲートウェイインスタンスのゾーングループ名                                  |String|None             |
|rgw\_zonegroup\_root\_pool          |ゾーングループのルートプール                                                |String|.rgw.root        |
|rgw\_zone\_root\_pool               |ゾーンのルートプール                                                        |String|.rgw.root        |
|rgw\_default\_zone\_group\_info\_oid|defaultゾーングループを保存するためのOID。この設定を変更することは推奨しない|String|default.zonegroup|
