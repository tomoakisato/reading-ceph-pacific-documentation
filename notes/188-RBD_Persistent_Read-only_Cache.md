# 188: RBD Persistent Read-only Cache

**クリップソース:** [188: RBD Persistent Read\-only Cache — Ceph Documentation](https://docs.ceph.com/en/pacific/rbd/rbd-persistent-read-only-cache/)

# RBD Persistent Read\-only Cache[¶](https://docs.ceph.com/en/pacific/rbd/rbd-persistent-read-only-cache/ "Permalink to this headline")

## Shared, Read\-only Parent Image Cache[¶](https://docs.ceph.com/en/pacific/rbd/rbd-persistent-read-only-cache/ "Permalink to this headline")

クローンされたRBDイメージは、通常、parentイメージのごく一部しか変更しません。例えば、VDIのユースケースでは、VMは同じベースイメージからクローンされ、最初はホスト名とIPアドレスだけが異なっています。起動中、これらすべてのVMは同じparentイメージのデータの一部を読み込みます。parentイメージのローカルキャッシュがあれば、キャッシュする側のホストでの読み込みが高速化されます。 また、クライアントからクラスタへのネットワーク・トラフィックを削減することもできます。RBDキャッシュは、ceph.confで明示的に有効にする必要があります。ceph\-immutable\-object\-cacheデーモンはローカルディスク上のparentコンテンツのキャッシュを担当し、そのデータに対する将来の読み取りはローカルキャッシュからサービスされます。

注: RBD共有読み取り専用parentイメージキャッシュには、Ceph Nautilusリリースまたはそれ以降が必要です。
![606105b562e21db4756e42c68566e71117b5bbbe8f3c0332360742d08a125eba.png](image/606105b562e21db4756e42c68566e71117b5bbbe8f3c0332360742d08a125eba.png)

### Enable RBD Shared Read\-only Parent Image Cache[¶](https://docs.ceph.com/en/pacific/rbd/rbd-persistent-read-only-cache/ "Permalink to this headline")

RBD共有読み取り専用parentイメージキャッシュを有効にするには、ceph.confファイルの\[client\]セクションに次のCeph設定を追加する必要があります：

```
rbd parent cache enabled = true
rbd plugins = parent_cache
```

## Immutable Object Cache Daemon[¶](https://docs.ceph.com/en/pacific/rbd/rbd-persistent-read-only-cache/ "Permalink to this headline")

### Introduction and Generic Settings[¶](https://docs.ceph.com/en/pacific/rbd/rbd-persistent-read-only-cache/ "Permalink to this headline")

ceph\-immutable\-object\-cache デーモンは、ローカルキャッシュディレクトリ内にparentイメージコンテンツをキャッシュする役割を担っています。パフォーマンスを向上させるために、ストレージとしてSSDを使用することをお勧めします。

デーモンの主要な構成要素は以下の通りです：

1. **Domain socket based IPC:**
2. **LRU based promotion/demotion policy:**
3. **File\-based caching store:**

クローンされた各rbdイメージを開くと、librbdはUnixドメインソケットを通してキャッシュデーモンに接続しようとします。接続に成功すると、librbdはその後の読み込みでデーモンと協調します。キャッシュされていない読み込みがあった場合、デーモンはRADOSオブジェクトをローカルキャッシュディレクトリに昇格させ、そのオブジェクトの次の読み込みはキャッシュから処理されるようにします。また、デーモンは単純な LRU 統計を維持し、容量不足の際には必要に応じてコールドキャッシュ・ファイルを退避させることができます。

ここでは、重要なキャッシュのコンフィギュレーション設定について説明します：

**immutable\_object\_cache\_sock**

Description

librbdクライアントとceph\-immutable\-object\-cacheデーモン間の通信に使用されるドメインソケットへのパス

Type

String

Required

No

Default

/var/run/ceph/immutable\_object\_cache\_sock

**immutable\_object\_cache\_path**

Description

immutable object cache data ディレクトリ

Type

String

Required

No

Default

/tmp/ceph\_immutable\_object\_cache

**immutable\_object\_cache\_max\_size**

Description

immutable cacheの最大サイズ

Type

Size

Required

No

Default

1G

**immutable\_object\_cache\_watermark**

Description

キャッシュのハイウォーターマーク。この値は \(0, 1\) の間。キャッシュサイズがこの閾値に達した場合、デーモンは LRU 統計に基づいてコールドキャッシュの削除を開始する

Type

Float

Required

No

Default

0.9

ceph\-immutable\-object\-cache デーモンは、オプションの ceph\-immutable\-object\-cache 配布パッケージで利用可能です。

重要: ceph\-immutable\-object\-cache デーモンは、RADOS クラスタに接続する機能を必要とします。

### Running the Immutable Object Cache Daemon[¶](https://docs.ceph.com/en/pacific/rbd/rbd-persistent-read-only-cache/ "Permalink to this headline")

ceph\-immutable\-object\-cacheデーモンは、一意のCephユーザIDを使用する必要があります。Cephユーザを作成するには、cephでauthget\-or\-createコマンド、ユーザ名、モニタCAPS、OSD CAPSを指定します：

```
ceph auth get-or-create client.ceph-immutable-object-cache.{unique id} mon 'allow r' osd 'profile rbd-read-only'
```

ceph\-immutable\-object\-cache デーモンは、デーモンインスタンスとしてユーザ ID を指定することで systemd で管理することができます：

```
systemctl enable ceph-immutable-object-cache@ceph-immutable-object-cache.{unique id}
```

ceph\-immutable\-object\-cacheは、ceph\-immutable\-object\-cacheコマンドでフォアグラウンド実行することもできます。

```
ceph-immutable-object-cache -f --log-file={log_path}
```

### QOS Settings[¶](https://docs.ceph.com/en/pacific/rbd/rbd-persistent-read-only-cache/ "Permalink to this headline")

immutable object cache は throttling をサポートし、以下の設定により制御されます：

**immutable\_object\_cache\_qos\_schedule\_tick\_min**

Description

immutable object cacheの最小スケジュールtick

Type

Milliseconds

Required

No

Default

50

**immutable\_object\_cache\_qos\_iops\_limit**

Description

immutable object cache I/Oの IOPS 制限値

Type

Unsigned Integer

Required

No

Default

0

**immutable\_object\_cache\_qos\_iops\_burst**

Description

immutable object cache I/Oの IOPS バースト限界値

Type

Unsigned Integer

Required

No

Default

0

**immutable\_object\_cache\_qos\_iops\_burst\_seconds**

Description

immutable object cache  I/Oの IOPS バースト時間（秒）

Type

Seconds

Required

No

Default

1

**immutable\_object\_cache\_qos\_bps\_limit**

Description

immutable object cache I/Oの BPS 制限値

Type

Unsigned Integer

Required

No

Default

0

**immutable\_object\_cache\_qos\_bps\_burst**

Description

immutable object cache I/O のBPS バースト限界値

Type

Unsigned Integer

Required

No

Default

0

**immutable\_object\_cache\_qos\_bps\_burst\_seconds**

Description

immutable object cache  I/O の BPS バースト時間（秒）

Type

Seconds

Required

No

Default

1
