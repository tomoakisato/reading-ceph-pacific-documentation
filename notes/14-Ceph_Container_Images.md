# 14: Ceph Container Images

**クリップソース:** [14: Ceph Container Images — Ceph Documentation](https://docs.ceph.com/en/pacific/install/containers/)

# Ceph Container Images¶

Important

Using the :latest tag is discouraged. If you use the :latest tag, there is no guarantee that the same image will be on each of your hosts. Under these conditions, upgrades might not work properly. Remember that :latest is a relative tag, and a moving target.  

latestタグの使用は推奨されません。latestタグを使用した場合、それぞれのホストに同じ画像が存在するという保証はありません。このような状況では、アップグレードが正しく行われない可能性があります。latestは相対的なタグであり、移動するターゲットであることを忘れないでください。

Instead of the :latest tag, use explicit tags or image IDs. For example:  

latestタグの代わりに、明示的なタグや画像IDを使用してください。例えば、以下のようになります。

podman pull ceph/ceph:v15.2.0

## **Official Releases[¶](https://docs.ceph.com/en/pacific/install/containers/#official-releases "Permalink to this headline")**

Ceph Container images are available from both Quay and Docker Hub:  

Cephのコンテナイメージは、QuayとDocker Hubの両方から入手できます。

```
https://quay.io/repository/ceph/ceph
https://hub.docker.com/r/ceph

```

### ceph/ceph[¶](https://docs.ceph.com/en/pacific/install/containers/#ceph-ceph "Permalink to this headline")

* 必要なデーモンと依存関係がすべてインストールされた汎用Cephコンテナ。

|Tag                  |Meaning                                                        |
|---------------------|---------------------------------------------------------------|
|vRELNUM              |Latest release in this series \(e.g., _v14_ = Nautilus\)       |
|vRELNUM.2            |Latest _stable_ release in this stable series \(e.g., _v14.2_\)|
|vRELNUM.Y.Z          |A specific release \(e.g., _v14.2.4_\)                         |
|vRELNUM.Y.Z\-YYYYMMDD|A specific build \(e.g., _v14.2.4\-20191203_\)                 |

## **Legacy container images[¶](https://docs.ceph.com/en/pacific/install/containers/#legacy-container-images "Permalink to this headline")**

レガシーコンテナイメージは、Docker Hubから入手可能です。

```
https://hub.docker.com/r/ceph

```

### ceph/daemon\-base[¶](https://docs.ceph.com/en/pacific/install/containers/#ceph-daemon-base "Permalink to this headline")

* 必要なデーモンと依存関係がすべてインストールされた汎用Cephコンテナ。
* 基本的にceph/cephと同じですが、タグが異なります。
* なお、すべての\-develタグ（およびlatest\-masterタグ）は、https://shaman.ceph.com の未リリースで一般的にテストされていないパッケージに基づいています。

note

このイメージはすぐにceph/cephのエイリアスになります。

|Tag                   |Meaning                                                 |
|----------------------|--------------------------------------------------------|
|latest\-master        |Build of master branch a last ceph\-container.git update|
|latest\-master\-devel |Daily build of the master branch                        |
|latest\-RELEASE\-devel|Daily build of the _RELEASE_ \(e.g., nautilus\) branch  |

### ceph/daemon[¶](https://docs.ceph.com/en/pacific/install/containers/#ceph-daemon "Permalink to this headline")

* _ceph/daemon\-baseは、ceph\-nanoとceph\-ansibleがCephクラスタを管理するために使用するBASHスクリプトのコレクションです。_

|Tag                   |Meaning                                                 |
|----------------------|--------------------------------------------------------|
|latest\-master        |Build of master branch a last ceph\-container.git update|
|latest\-master\-devel |Daily build of the master branch                        |
|latest\-RELEASE\-devel|Daily build of the _RELEASE_ \(e.g., nautilus\) branch  |

## **Development builds[¶](https://docs.ceph.com/en/pacific/install/containers/#development-builds "Permalink to this headline")**

ceph\-ci.gitリポジトリの開発用wip\-\*ブランチのコンテナイメージを自動的にビルドし、Quay atにプッシュします。

```
https://quay.io/organization/ceph-ci

```

### ceph\-ci/ceph[¶](https://docs.ceph.com/en/pacific/install/containers/#ceph-ci-ceph "Permalink to this headline")

* これは、上記のceph/cephのイメージに似ています。
* TODO: wip\-\*の制限を取り除き、ceph.gitブランチも構築すること。

|Tag                                   |Meaning                                                |
|--------------------------------------|-------------------------------------------------------|
|BRANCH                                |Latest build of a given GIT branch \(e.g., _wip\-foo_\)|
|BRANCH\-SHORTSHA1\-BASEOS\-ARCH\-devel|A specific build of a branch                           |
|SHA1                                  |A specific build                                       |
