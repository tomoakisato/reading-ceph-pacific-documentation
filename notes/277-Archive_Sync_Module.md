# 277: Archive Sync Module

**クリップソース:** [277: Archive Sync Module — Ceph Documentation](https://docs.ceph.com/en/pacific/radosgw/archive-sync-module/)

# Archive Sync Module[¶](https://docs.ceph.com/en/pacific/radosgw/archive-sync-module/ "Permalink to this headline")

Nautilus バージョンの新機能です。

この同期モジュールは、RGWのS3オブジェクトのバージョン管理機能を活用し、他のゾーンで時間経過とともに発生する異なるバージョンのS3オブジェクトをキャプチャするアーカイブゾーンを持ちます。

アーカイブゾーンでは、アーカイブゾーンに関連付けたゲートウェイを介してのみ削除できるS3オブジェクトのバージョン履歴を保持できます。

この機能は、バージョン管理されていない複数のゾーンが、ゾーンゲートウェイを通じてデータやメタデータを複製する構成（ミラー構成）をとる場合に有効です。エンドユーザーに高い可用性を提供することができる一方で、アーカイブゾーンは、すべてのデータの更新とメタデータを取得し、S3オブジェクトのバージョンとして統合します。

マルチゾーン構成にアーカイブゾーンを含めることで、1つゾーンのみのにS3オブジェクトの履歴を持たせることができます。同時に、バージョン管理されたS3オブジェクトのレプリカが、他のゾーンで消費するスペースを節約することができます。

## Archive Sync Tier Type Configuration[¶](https://docs.ceph.com/en/pacific/radosgw/archive-sync-module/ "Permalink to this headline")

### How to Configure[¶](https://docs.ceph.com/en/pacific/radosgw/archive-sync-module/ "Permalink to this headline")

マルチサイトの設定方法については、「[マルチサイト設定](https://docs.ceph.com/en/pacific/radosgw/multisite)」を参照してください。アーカイブ同期モジュールは、新しいゾーンの作成が必要です。ゾーンのtire\-typeはarchiveとして定義する必要があります：

```
# radosgw-admin zone create --rgw-zonegroup={zone-group-name} 
                            --rgw-zone={zone-name} 
                            --endpoints={http://fqdn}[,{http://fqdn}]
                            --tier-type=archive
```
