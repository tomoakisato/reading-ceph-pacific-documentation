# 133: Deploying Metadata Servers

**クリップソース:** [133: Deploying Metadata Servers — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/add-remove-mds/)

# Deploying Metadata Servers[¶](https://docs.ceph.com/en/pacific/cephfs/add-remove-mds/ "Permalink to this headline")

各CephFSファイルシステムには、少なくとも1つのMDSが必要です。クラスタオペレータは通常、自動デプロイツールを使用して、必要なMDSサーバを必要に応じて起動します。 Rookおよびansible \(ceph\-ansible playbooks経由\)は、これを行うための推奨ツールです。また、ここでは分かりやすくするために、ベアメタルで実行された場合にデプロイ技術によって実行される可能性のあるsystemdコマンドも示しています。

メタデータサーバの設定の詳細については、「[MDS Config Reference](https://docs.ceph.com/en/pacific/cephfs/mds-config-ref)」を参照してください。

## Provisioning Hardware for an MDS[¶](https://docs.ceph.com/en/pacific/cephfs/add-remove-mds/ "Permalink to this headline")

MDSの現在のバージョンは、クライアント要求への応答を含むほとんどのアクティビティに対してシングルスレッドでCPUバウンドです。 最もアグレッシブなクライアント負荷の下でのMDSは、約2〜3個のCPUコアを使用します。 これは、他のその他の維持スレッドが連携して動作しているためです。

それでも、MDSサーバには十分なコアを持つ高度なCPUをプロビジョニングすることが推奨されます。MDSで利用可能なCPUコアをより有効に活用するための開発が進行中です。Cephの将来のバージョンでは、より多くのコアを活用することでMDSサーバのパフォーマンスが向上することが期待されます。

MDSのパフォーマンスに対するもう一つの側面は、キャッシュに利用可能なRAMです。MDSは必然的に、全てのクライアントと他のアクティブなMDSの間で分散・協調してメタデータ キャッシュを管理することになります。したがって、より高速なメタデータアクセスと変異を可能にするために、MDSに十分なRAMを提供することが 不可欠です。デフォルトのMDSキャッシュサイズは4GBです（「[MDSのキャッシュ構成](https://docs.ceph.com/en/pacific/cephfs/cache-configuration/)」を参照）。このキャッシュサイズに対応するため、MDSには少なくとも8GBのRAMを搭載することを推奨します。

一般に、クライアントの大規模なクラスター（1000以上）にサービスを提供するMDSは、少なくとも64GBのキャッシュを使用します。 より大きなキャッシュを備えたMDSは、既知の最大のコミュニティクラスターでは十分に検討されていません。 このような大規模なキャッシュの管理がパフォーマンスに悪影響を与える場合、収穫逓減が発生する可能性があります。 より多くのRAMをプロビジョニングする価値があるかどうかを判断するために、予想されるワークロードで分析を行うのが最善です。

ベアメタルクラスタでは、ベストプラクティスは、MDSサーバのハードウェアをオーバープロビジョニングすることです。 単一のMDSデーモンがハードウェアを完全に利用できない場合でも、後で同じノードでより多くのMDSデーモンを起動して、使用可能なコアとメモリを完全に利用することが望ましい場合があります。 さらに、クラスタ上のワークロードを使用すると、単一のMDSをオーバープロビジョニングするのではなく、同じノード上の複数のMDSでパフォーマンスが向上する場合があります。

最後に、CephFSは高速フェイルオーバーのためのスタンバイMDS（[用語](https://docs.ceph.com/en/pacific/cephfs/standby/#mds-standby)も参照）をサポートすることで、高可用性ファイルシステムであることを意識してください。スタンバイの導入から真のメリットを得るには、通常、クラスタ内の少なくとも2つのノードにMDSデーモンを分散させる必要があります。そうしないと、1つのノードでハードウェア障害が発生した場合、ファイルシステムが使用できなくなる可能性があります。

すべてのデーモンが利用可能なハードウェアを一定の範囲内で使用するように設定されている限り、MDSを他のCephデーモンと同居させること（ハイパーコンバージド）は、これを実現する効果的で推奨される方法です。 MDSの場合、これは一般的にキャッシュサイズを制限することを意味します。

## Adding an MDS[¶](https://docs.ceph.com/en/pacific/cephfs/add-remove-mds/ "Permalink to this headline")

1. mdsデータポイント/var/lib/ceph/mds/ceph\-${id}を作成します。デーモンはこのディレクトリをキーリングを保存するためにのみ使用します。
2. CephXを使用する場合、認証キーを作成します：

```
$ sudo ceph auth get-or-create mds.${id} mon 'profile mds' mgr 'profile mds' mds 'allow *' osd 'allow *' > /var/lib/ceph/mds/ceph-${id}/keyring
```

1. サービスを開始します：

```
$ sudo systemctl start ceph-mds@${id}
```

1. クラスタのステータスが表示されるはずです：

```
mds: ${id}:1 {0=${id}=up:active} 2 up:standby
```

1. オプションとして、MDSが参加すべきファイルシステムを設定します（

```
$ ceph config set mds.${id} mds_join_fs ${fs}
```

## Removing an MDS[¶](https://docs.ceph.com/en/pacific/cephfs/add-remove-mds/ "Permalink to this headline")

クラスタ内に削除したいメタデータサーバがある場合、以下の方法で削除することができます。

1. \(オプション：）新しい代替メタデータサーバーを作成します。MDSを削除した後に引き継ぐ代替MDSがない場合、ファイルシステムはクライアントから利用できなくなります。 それが望ましくない場合は、オフラインにしたいメタデータサーバを撤去する前に、メタデータサーバを追加することを検討してください。
2. 削除するMDSを停止します。

```
$ sudo systemctl stop ceph-mds@${id}
```

MDSは自動的にCephモニタにダウンしていることを通知します。これにより、利用可能なスタンバイが存在する場合、モニタは瞬時にフェイルオーバを実行することができます。ceph mds fail mds.${id}などの管理コマンドを使用して、このフェイルオーバを実行する必要はありません。

1. MDS上の/var/lib/ceph/mds/ceph\-${id}ディレクトリを削除してください。

```
$ sudo rm -rf /var/lib/ceph/mds/ceph-${id}
```
