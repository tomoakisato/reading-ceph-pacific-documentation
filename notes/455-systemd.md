# 455: systemd

**クリップソース:** [455: systemd — Ceph Documentation](https://docs.ceph.com/en/pacific/ceph-volume/simple/systemd/)

# systemd[¶](https://docs.ceph.com/en/pacific/ceph-volume/simple/systemd/ "Permalink to this headline")

起動時に、systemd 本体のインスタンス名に対応する /etc/ceph/osd/{id}\-{uuid}.json の JSON ファイルを読み込んで、論理ボリュームを識別します。

正しいボリュームを特定した後、以下のOSDの宛先規則を使用してマウントを行います：

```
/var/lib/ceph/osd/{cluster name}-{osd id}
```

IDが0のOSDの例では、識別されたデバイスが以下にマウントされることを意味します：

```
/var/lib/ceph/osd/ceph-0
```

その処理が完了すると、OSDを起動するための呼び出しが行われます：

```
systemctl start ceph-osd@0
```

このプロセスのsystemdの部分は、ceph\-volume simple triggerサブコマンドによって処理され、systemdとstartupから来るメタデータを解析し、activationを進めるceph\-volume simple activateにディスパッチすることだけを担当します。
