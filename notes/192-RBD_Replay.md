# 192: RBD Replay

**クリップソース:** [192: RBD Replay — Ceph Documentation](https://docs.ceph.com/en/pacific/rbd/rbd-replay/)

# RBD Replay[¶](https://docs.ceph.com/en/pacific/rbd/rbd-replay/ "Permalink to this headline")

RBD Replayは、RBD（RADOS Block Device）ワークロードをキャプチャして再生するためのツール群です。RBDワークロードをキャプチャするには、クライアントにlttng\-toolsをインストールし、クライアントのlibbdはv0.87（Giant）リリース以降である必要があります。RBDワークロードを再生するには、クライアントのlibrbdがGiantリリース以降である必要があります。

キャプチャーとリプレイは3つのステップで行われます：

1. トレースをキャプチャします。 キャプチャするpthread\_id のコンテキストを確認します：

```
mkdir -p traces
lttng create -o traces librbd
lttng enable-event -u 'librbd:*'
lttng add-context -u -t pthread_id
lttng start
# run RBD workload here
lttng stop
```

1. [rbd\-replay\-prep](https://docs.ceph.com/en/pacific/man/8/rbd-replay-prep)

```
rbd-replay-prep traces/ust/uid/*/* replay.bin
```

1. [rbd\-replay](https://docs.ceph.com/en/pacific/man/8/rbd-replay)

```
rbd-replay --read-only replay.bin
```

重要: rbd\-replay はデフォルトでデータを破壊します。 \-\-read\-onlyオプションを使わない限り、キープしたいイメージに対して使わないでください。

再生されるワークロードは、キャプチャしたワークロードと同じRBDイメージや同じクラスタに対してである必要はありません。違いを考慮するために、rbd\-replayの\-\-poolおよび\-\-map\-imageオプションを使用する必要がある場合があります。
