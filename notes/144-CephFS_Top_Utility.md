# 144: CephFS Top Utility

**クリップソース:** [144: CephFS Top Utility — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/cephfs-top/)

# CephFS Top Utility[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-top/ "Permalink to this headline")

CephFSは、さまざまなCephファイルシステムメトリックをリアルタイムで表示するためのtop\(1\)のようなユーティリティを提供します。 cephfs\-topは、Ceph Managerの統計プラグインを使用してメトリックをフェッチ、表示するcursesベースのPythonスクリプトです。

## Manager Plugin[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-top/ "Permalink to this headline")

Ceph ファイルシステムクライアントは定期的にCeph Metadata Servers \(MDS\)にさまざまなメトリクスを転送し、MDSランク0によってCeph Managerに転送されます。アクティブな各MDSは、それぞれのメトリクスセットをMDSランク0に転送します。メトリクスは集約され、Ceph Managerに転送されます。

メトリクスは、グローバルとMDS単位の2つのカテゴリーに分けられます。グローバルのメトリクスはファイルシステム全体に対するメトリクスのセット（例：クライアントリードレイテンシ）を表し、一方、MDS単位のメトリクスは特定のMDSランクに対するもの（例：MDSが処理するサブツリーの数）です。

注：現在は、グローバルのメトリクスのみを追跡しています。

統計プラグインはデフォルトで無効になっているため、次の方法で有効にする必要があります：

```
$ ceph mgr module enable stats
```

有効にすると、Ceph Filesystemのメトリクスは、以下の方法で取得できるようになります：

```
$ ceph fs perf stats
{"version": 1, "global_counters": ["cap_hit", "read_latency", "write_latency", "metadata_latency", "dentry_lease"], "counters": [], "client_metadata": {"client.614146": {"IP": "10.1.1.100", "hostname"  : "ceph-host1", "root": "/", "mount_point": "/mnt/cephfs", "valid_metrics": ["cap_hit", "read_latency", "write_latency", "metadata_latency", "dentry_lease"]}}, "global_metrics": {"client.614146": [[0,  0], [0, 0], [0, 0], [0, 0], [0, 0]]}, "metrics": {"delayed_ranks": [], "mds.0": {"client.614146": []}}}
```

JSONコマンド出力の詳細は以下のとおりです：

* version: 統計情報出力のバージョン
* global\_counters: グローバルな性能メトリクスのリスト
* counters: MDS単位の性能メトリクスのリスト
* client\_metadata: Ceph Filesystemクライアントのメタデータ
* global\_metrics: グローバル性能カウンタ
* metrics: MDS単位の性能カウンタ（現在は空）と遅延ランク

注：delayed\_ranksは、古いメトリクスを報告しているアクティブなMDSのランクのセットです。これは、MDSのランク0と他のアクティブなMDSの間に（一時的な）ネットワークの問題がある場合などに発生する可能性があります。

メトリックは、特定のクライアントおよび/またはアクティブなMDSのセットに対して取得できます。 特定のクライアントのメトリックを取得するには（例：client\-id：1234）：

```
$ ceph fs perf stats --client_id=1234
```

アクティブなMDSのサブセットのみのメトリクスを取得するには（例：MDSのランク1と2）：

```
$ ceph fs perf stats --mds_rank=1,2
```

## cephfs\-top[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-top/ "Permalink to this headline")

cephfs\-top ユーティリティは stats プラグインに依存して、性能メトリクスを取得し、top\(1\) のような形式で表示します。

デフォルトでは、cephfs\-topはclient.fstopユーザを使用してCephクラスタに接続します：

```
$ ceph auth get-or-create client.fstop mon 'allow r' mds 'allow r' osd 'allow r' mgr 'allow r'
$ cephfs-top
```

デフォルト以外のユーザー（client.fstop以外）を使用する場合は、以下を使用します：

```
$ cephfs-top --id <name>
```

デフォルトでは、cephfs\-topはクラスタ名cephに接続します。デフォルト以外のクラスタ名を使用する場合：

```
$ cephfs-top --cluster <cluster>
```

cephfs\-topは、デフォルトで1秒ごとに統計情報を更新します。別の更新間隔を選択するには、以下を使用します：

```
$ cephfs-top -d <seconds>
```

インターバルは 0.5 秒以上でなければなりません。端数秒は無視されます。

2クライアントでcephfs\-topを実行したサンプル画面：
![cephfs-top.png](image/cephfs-top.png)

注: 現在のところ、cephfs\-topは複数のCephファイルシステムで確実に動作するわけではありません。
