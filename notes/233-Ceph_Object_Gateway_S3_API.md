# 233: Ceph Object Gateway S3 API

**クリップソース:** [233: Ceph Object Gateway S3 API — Ceph Documentation](https://docs.ceph.com/en/pacific/radosgw/s3/)

# Ceph Object Gateway S3 API[¶](https://docs.ceph.com/en/pacific/radosgw/s3/ "Permalink to this headline")

Cephは、[Amazon S3 API](http://docs.aws.amazon.com/AmazonS3/latest/API/APIRest.html)の基本的なデータアクセスモデルと互換性のあるRESTful APIをサポートしています。

## API[¶](https://docs.ceph.com/en/pacific/radosgw/s3/ "Permalink to this headline")

* [Common](https://docs.ceph.com/en/pacific/radosgw/s3/)
* [Authentication](https://docs.ceph.com/en/pacific/radosgw/s3/)
* [Service Ops](https://docs.ceph.com/en/pacific/radosgw/s3/)
* [Bucket Ops](https://docs.ceph.com/en/pacific/radosgw/s3/)
* [Object Ops](https://docs.ceph.com/en/pacific/radosgw/s3/)
* [C\+\+](https://docs.ceph.com/en/pacific/radosgw/s3/)
* [C\#](https://docs.ceph.com/en/pacific/radosgw/s3/)
* [Java](https://docs.ceph.com/en/pacific/radosgw/s3/)
* [Perl](https://docs.ceph.com/en/pacific/radosgw/s3/)
* [PHP](https://docs.ceph.com/en/pacific/radosgw/s3/)
* [Python](https://docs.ceph.com/en/pacific/radosgw/s3/)
* [Ruby AWS::SDK Examples \(aws\-sdk gem ~\>2\)](https://docs.ceph.com/en/pacific/radosgw/s3/)
* [Ruby AWS::S3 Examples \(aws\-s3 gem\)](https://docs.ceph.com/en/pacific/radosgw/s3/)

## Features Support[¶](https://docs.ceph.com/en/pacific/radosgw/s3/ "Permalink to this headline")

次の表は、現在のAmazon S3機能機能のサポート状況について説明しています：

|Feature                        |Status   |Remarks                             |
|-------------------------------|---------|------------------------------------|
|**List Buckets**               |Supported|                                    |
|**Delete Bucket**              |Supported|                                    |
|**Create Bucket**              |Supported|Different set of canned ACLs        |
|**Bucket Lifecycle**           |Supported|                                    |
|**Policy \(Buckets, Objects\)**|Supported|ACLs & bucket policies are supported|
|**Bucket Website**             |Supported|                                    |
|**Bucket ACLs \(Get, Put\)**   |Supported|Different set of canned ACLs        |
|**Bucket Location**            |Supported|                                    |
|**Bucket Notification**        |Supported|See                                 |
|**Bucket Object Versions**     |Supported|                                    |
|**Get Bucket Info \(HEAD\)**   |Supported|                                    |
|**Bucket Request Payment**     |Supported|                                    |
|**Put Object**                 |Supported|                                    |
|**Delete Object**              |Supported|                                    |
|**Get Object**                 |Supported|                                    |
|**Object ACLs \(Get, Put\)**   |Supported|                                    |
|**Get Object Info \(HEAD\)**   |Supported|                                    |
|**POST Object**                |Supported|                                    |
|**Copy Object**                |Supported|                                    |
|**Multipart Uploads**          |Supported|                                    |
|**Object Tagging**             |Supported|See                                 |
|**Bucket Tagging**             |Supported|                                    |
|**Storage Class**              |Supported|See                                 |

## Unsupported Header Fields[¶](https://docs.ceph.com/en/pacific/radosgw/s3/ "Permalink to this headline")

以下の共通リクエストヘッダーフィールドはサポートされていません：

|Name                      |Type    |
|--------------------------|--------|
|**Server**                |Response|
|**x\-amz\-delete\-marker**|Response|
|**x\-amz\-id\-2**         |Response|
|**x\-amz\-version\-id**   |Response|
