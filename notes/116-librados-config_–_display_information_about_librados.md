# 116: librados-config – display information about librados

 # librados\-config – display information about librados[¶](https://docs.ceph.com/en/pacific/man/8/librados-config/ "Permalink to this headline")

## Synopsis[¶](https://docs.ceph.com/en/pacific/man/8/librados-config/ "Permalink to this headline")

**librados\-config** \[ –version \] \[ –vernum \]

## Description[¶](https://docs.ceph.com/en/pacific/man/8/librados-config/ "Permalink to this headline")

**librados\-config** is a utility that displays information about theinstalled `librados`.

## Options[¶](https://docs.ceph.com/en/pacific/man/8/librados-config/ "Permalink to this headline")

`--version```[¶](https://docs.ceph.com/en/pacific/man/8/librados-config/ "Permalink to this definition")Display `librados` version

`--vernum```[¶](https://docs.ceph.com/en/pacific/man/8/librados-config/ "Permalink to this definition")Display the `librados` version code

## Availability[¶](https://docs.ceph.com/en/pacific/man/8/librados-config/ "Permalink to this headline")

**librados\-config** is part of Ceph, a massively scalable, open\-source, distributed storage system. Please refer to the Ceph documentation at [http://ceph.com/docs](https://docs.ceph.com/en/pacific/man/8/librados-config/) for more information.

## See also[¶](https://docs.ceph.com/en/pacific/man/8/librados-config/ "Permalink to this headline")

[ceph](https://docs.ceph.com/en/pacific/man/8/librados-config/)\(8\),[rados](https://docs.ceph.com/en/pacific/man/8/librados-config/)\(8\)
