# 80: Placement Groups

**クリップソース:** [80: Placement Groups — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/operations/placement-groups/)

# Placement Groups[¶](https://docs.ceph.com/en/pacific/rados/operations/placement-groups/ "Permalink to this headline")

## Autoscaling placement groups[¶](https://docs.ceph.com/en/pacific/rados/operations/placement-groups/ "Permalink to this headline")

配置グループ\(PG\)は、Cephがデータを分散させる方法の内部実装の詳細です。 pg\-autoscalingを有効にすると、クラスタの使用方法に基づいて、クラスタがPGを推奨または自動的に調整できるようになります。

システム内の各プールにはpg\_autoscale\_modeプロパティがあり、off、on、またはwarnに設定することができます。

* off: このプールのオートスケールを無効にします。 各プールの適切な PG 数を選択するのは、管理者です。 詳細については、「
* on: 指定されたプールの PG カウントの自動調整を有効にします。
* warn: PG数を調整する必要がある場合、ヘルスアラートを発生させます。

既存のプールにオートスケーリングモードを設定するには：

```
ceph osd pool set <pool-name> pg_autoscale_mode <mode>
```

たとえば、プール foo でオートスケールを有効にするには：

```
ceph osd pool set foo pg_autoscale_mode on
```

また、将来作成されるすべてのプールに適用されるデフォルトのpg\_autoscale\_modeを構成するには：

```
ceph config set global osd_pool_default_pg_autoscale_mode <mode>
```

### Viewing PG scaling recommendations[¶](https://docs.ceph.com/en/pacific/rados/operations/placement-groups/ "Permalink to this headline")

このコマンドを使用すると、各プール、相対的な使用率、PGカウントの変更案を表示することができます。

```
ceph osd pool autoscale-status
```

以下のような出力になります。

```
POOL    SIZE  TARGET SIZE  RATE  RAW CAPACITY   RATIO  TARGET RATIO  EFFECTIVE RATIO BIAS PG_NUM  NEW PG_NUM  AUTOSCALE PROFILE
a     12900M                3.0        82431M  0.4695                                          8         128  warn      scale-up
c         0                 3.0        82431M  0.0000        0.2000           0.9884  1.0      1          64  warn      scale-down
b         0        953.6M   3.0        82431M  0.0347                                          8              warn      scale-down

```

**SIZE** はプールに保存されているデータ量です。 **TARGET SIZE** （存在する場合）は、管理者がこのプールに最終的に格納されることを想定して指定したデータ量です。 システムは、2つの値のうち大きい方を計算に使用します。

**RATE** はプールの乗数で、rawストレージ容量がどれだけ消費されるかを決定します。 例えば、3レプリカプールの場合、比率は3.0となり、k=4,m=2  ECプールの場合、比率は1.5となります。

**RAW CAPACITY** は、このプールの（そしておそらく他のプールの）データを格納する責任を負うOSD上のrawストレージ容量の合計量です。  **RATIO** は、このプールが消費している総容量の比率です（すなわち、比率=サイズ\*レート/raw容量）。

**TARGET RATIO**（存在する場合）は、管理者が指定した、このプールが消費するストレージの比率で、ターゲット比率が設定されている他のプールとの相対的な比較です。ターゲットサイズバイトと比率の両方が指定された場合、比率が優先されます。

**EFFECTIVE RATIO** は、2つの方法で調整した後の目標比率です。

1. ターゲットサイズを設定したプールで使用されると予想される容量を差し引く
2. 残りのスペースをまとめてターゲットにするように、ターゲット比率が設定されたプール間のターゲット比率を正規化します。 たとえば、target\_ratio 1.0の4つのプールの実効比率は、0.25になります。

実比率と有効比率の大きい方を計算の対象としています。

**BIAS** は、特定のプールがどの程度のPGを持つと予想されるかという事前情報に基づいて、プールのPGを手動で調整するための乗数として使用されます。

**PG\_NUM** は、プールの現在のPG数（または、PG\_NUMの変更が進行中の場合、プールが目指している現在のPG数）です。 **NEW PG\_NUM**がある場合、システムはプールのpg\_numを変更すべきであると考えている値です。 これは常に2の累乗であり、「理想的な」値が現在の値から3倍以上変化する場合にのみ表示されます。

**AUTOSCALE**は、プールpg\_autoscale\_modeで、on、off、またはwarnのいずれかになります。

最後の列、**PROFILE** は、各プールが使用するオートスケールプロファイルです。

### Automated scaling[¶](https://docs.ceph.com/en/pacific/rados/operations/placement-groups/ "Permalink to this headline")

クラスタが使用状況に応じて自動的にPGをスケールするようにするのが、最もシンプルな方法です。 Cephは、システム全体の利用可能なストレージの合計とPGの目標数を調べ、各プールに格納されているデータの量を確認し、それに応じてPGを割り当てようとします。 このシステムは比較的保守的なアプローチで、現在のPG数\(pg\_num\)があるべき姿から3倍以上ずれている場合にのみプールに変更を加えます。

OSDあたりのPGの目標数は、mon\_target\_pg\_per\_osd configurable（デフォルト：100）で調整可能です。

```
ceph config set global mon_target_pg_per_osd 100
```

オートスケーラはプールを分析し、サブツリー単位で調整します。各プールは異なるCRUSHルールに対応し、各ルールは異なるデバイスにデータを分散する可能性があるため、Cephは階層の各サブツリーの利用率を個別に検討します。 たとえば、クラスssdのOSDに対応するプールとクラスhddのOSDに対応するプールはそれぞれのデバイスタイプの数に依存する最適なPG数を持つことになります。

オートスケーラはデフォルトでスケールアッププロファイルを使用し、各プールを最小限のPGで開始し、各プールで使用量が増えたときにPGをスケールアップします。しかし、スケールダウン・プロファイルも用意されており、各プールはPGをフル装備して開始し、プール間の使用比率が均等でない場合にのみスケールダウンします。

スケールダウンプロファイルのみでは、ルートが重なるとスケーリング処理に問題が生じるため、オートスケーラは重なったルートを特定し、そのようなルートを持つプールがスケーリングされないようにします。

スケールダウン・プロファイルを使用するには：

```
ceph osd pool set autoscale-profile scale-down
```

デフォルトのスケールアッププロファイルに戻すには：

```
ceph osd pool set autoscale-profile scale-up
```

既存のクラスタでは、引き続きスケールアッププロファイルが使用されます。スケールダウンプロファイルを使用するには、スケールダウン機能を提供するCephのバージョンにアップグレードした後、ユーザはautoscale\-profile scale\-downを設定する必要があります。

### Specifying expected pool size[¶](https://docs.ceph.com/en/pacific/rados/operations/placement-groups/ "Permalink to this headline")

クラスタまたはプールが最初に作成されたとき、それはクラスタ全体の容量のごく一部を消費し、システムからは少数の配置グループしか必要としないように見えるでしょう。しかし、ほとんどの場合、クラスタ管理者は、どのプールが時間の経過とともにシステム容量の大部分を消費することになるかについて、十分な見当をつけています。この情報をCephに提供することで、より適切な数のPGを最初から使用でき、その後のpg\_numの変更と、その変更に伴うデータの移動に伴うオーバーヘッドを防止することができます。

プールの目標サイズは、プールの絶対サイズ（すなわちバイト）、または target\_size\_ratio 設定による他のプールとの相対的な重み付けの 2 つの方法で指定することが可能です。

For example,:

```
ceph osd pool set mypool target_size_bytes 100T
```

上記は、mypool が 100 TiB のスペースを消費する見込みであることをシステムに伝えます。 あるいは

```
ceph osd pool set mypool target_size_ratio 1.0
```

上記は、mypool が target\_size\_ratio が設定された他のプールに対して 1.0 を消費すると予想されることをシステムに伝えます。mypoolがクラスタ内の唯一のプールである場合、これは総容量の100%を使用することを意味します。target\_size\_ratio 1.0の2番目のプールがある場合、両方のプールはクラスタ容量の50%を使用すると予想されます。

ceph osd pool createコマンドのオプションの\-\-target\-size\-bytes \<bytes\>または\-\-target\-size\-ratio \<ratio\>引数を使用して、作成時にプールのターゲットサイズを設定することも可能です。

不可能なターゲット・サイズ値が指定された場合（たとえば、クラスタの合計よりも大きな容量）、ヘルス警告（POOL\_TARGET\_SIZE\_BYTES\_OVERCOMMITTED）が発生することに注意してください。

target\_size\_ratioとtarget\_size\_bytesの両方がプールに指定された場合、比率のみが考慮され、ヘルス警告（POOL\_HAS\_TARGET\_SIZE\_BYTES\_AND\_RATIO）が発行されることに注意してください。

### Specifying bounds on a pool’s PGs[¶](https://docs.ceph.com/en/pacific/rados/operations/placement-groups/ "Permalink to this headline")

また、プールのPGの最小数を指定することも可能です。これは、プールがほとんど空の場合でも、クライアントが IO を実行する際に表示される並列度の下限を設定するのに便利です。 下限を設定すると、CephがPG数を設定した数より減らす\(または減らすことを推奨する\)のを防ぐことができます。

プールの最小PG数を設定するには：

```
ceph osd pool set <pool-name> pg_num_min <num>
```

ceph osd pool createコマンドのオプション引数\-\-pg\-num\-min \<num\>で、プール作成時に最小PG数を指定することもできます。

## A preselection of pg\_num[¶](https://docs.ceph.com/en/pacific/rados/operations/placement-groups/ "Permalink to this headline")

新規にプールを作成するには：

```
ceph osd pool create {pool-name} [pg_num]
```

pg\_numの値を選択することは任意です。 pg\_numを指定しない場合、クラスタは（デフォルトで）プールに格納されているデータ量に基づいて自動的に調整することができます（上記の[Autoscaling placement groups](https://docs.ceph.com/en/pacific/rados/operations/placement-groups/)を参照してください）。

あるいは、pg\_numを明示的に指定することもできます。 しかし、pg\_numの値を指定してもしなくても、その値が事後にクラスタによって自動的に調整されるかどうかには影響しません。 自動チューニングを有効または無効にするには、:

```
ceph osd pool set {pool-name} pg_autoscale_mode (on|off|warn)
```

OSDあたりのPGの「目安」は、従来は100でした。 バランサ（デフォルトで有効になっている）の追加で、OSDあたり50のような値がおそらく妥当です。 課題は（通常はオートスケーラがやってくれる）、以下の通りです。

* プールごとのPGがプール内のデータに比例し
* OSD全体での各PGのreplicatedまたはECファンアウトを考慮した後、OSDあたり50～100個のPGで終了します。

## How are Placement Groups used ?[¶](https://docs.ceph.com/en/pacific/rados/operations/placement-groups/ "Permalink to this headline")

オブジェクト単位でオブジェクトの配置とオブジェクトのメタデータを追跡するのは計算量が多いため、PGは、プール内のオブジェクトを集約します。

Cephクライアントは、オブジェクトがどのPGに属するべきかを計算します。これは、オブジェクトIDをハッシュ化し、定義されたプール内のPG数とプールのIDに基づく演算を適用することで行われます。詳細については、「[Mapping PGs to OSDs](https://docs.ceph.com/en/pacific/rados/operations/placement-groups/)」を参照してください。

PG内のオブジェクトの内容は、一連のOSDに格納されます。たとえば、size=2のreplicatedプールでは、以下のように、各PGは2つのOSDにオブジェクトを格納することになります。

OSD \#2が失敗した場合、別のOSDがPG \#1に割り当てられ、OSD \#1のすべてのオブジェクトのコピーで満たされます。プールサイズが2から3に変更された場合、追加のOSDがPGに割り当てられ、PG内のすべてのオブジェクトのコピーを受け取ることになります。

PGはOSDを所有せず、同じプールまたは他のプールの他のPGと共有します。OSD \#2 が故障した場合、PG \#2 は OSD \#3 を使用してオブジェクトのコピーをリストアする必要があります。

PG数が増えると、新しいPGにはOSDが割り当てられます。CRUSH機能の結果も変わり、旧PGのオブジェクトの一部が新PGにコピーされ、旧PGから削除されます。

## Placement Groups Tradeoffs[¶](https://docs.ceph.com/en/pacific/rados/operations/placement-groups/ "Permalink to this headline")

データの耐久性と全 OSD への均等分配のため、PGを増やす必要がありますが、CPU とメモリを節約するため、PGの数は最小にする必要があります。

### Data durability[¶](https://docs.ceph.com/en/pacific/rados/operations/placement-groups/ "Permalink to this headline")

OSDが故障した後、含まれていたデータが完全に回復するまで、データ損失のリスクは増加します。1つのPGで永久的なデータ損失を引き起こすシナリオを想像してみましょう。

* OSDが故障し、オブジェクトのコピーが失われます。PG内のすべてのオブジェクトについて、レプリカの数が3つから2つに突然減少します。
* Cephは、新しいOSDを選択してすべてのオブジェクトの3番目のコピーを再作成することで、このPGのリカバリを開始します。
* 同じPG内の別のOSDが、新しいOSDに3つ目のコピーが完全に配置される前に故障します。いくつかのオブジェクトは、コピーが1つしかないことになります。
* Cephはさらに別のOSDを選び、コピー数を復元するためにオブジェクトをコピーし続けます。
* 同じPG内の3番目のOSDが、復旧が完了する前に故障しました。このOSDにオブジェクトの唯一残ったコピーが含まれていた場合、永久に失われます。

3つのレプリカプールに10個のOSDと512個のPGを持つクラスタでは、CRUSHは各配置グループに3個のOSDを与えます。最終的に、各OSDは\(512 \* 3\) / 10 = ~150のPGをホストすることになります。最初のOSDに障害が発生した場合、上記のシナリオでは、150のPGすべてに対して同時にリカバリを開始することになります。

リカバリされる150のPGは、残りの9つのOSDに均質に分散していると思われます。したがって、残りの各OSDは、他のすべてのOSDにオブジェクトのコピーを送信し、また、新しいPGの一部になったので、格納するためにいくつかの新しいオブジェクトを受信すると思われます。

このリカバリが完全に完了するまでの時間は、Cephクラスタのアーキテクチャに依存します。各OSDが1台のマシン上の1TB SSDでホストされ、それらすべてが10Gb/sスイッチに接続されており、1台のOSDのリカバリがM分以内に完了するとします。SSDジャーナルがなく、1Gb/sスイッチでspinnerを使用しているマシンあたり2つのOSDがある場合、少なくとも1桁は遅くなります。

このサイズのクラスタでは、PGの数はデータの耐久性にほとんど影響を及ぼしません。128でも8192でも、リカバリーが遅くなったり速くなったりすることはありません。

しかし、同じCephクラスタを10 OSDから20 OSDに成長させると、リカバリーが高速化され、その結果、データの耐久性が大幅に向上すると思われます。各OSDは、10 OSDしかなかったときには150程度だったPGに、現在では75程度しか参加しておらず、復旧のために残りの19 OSDすべてが同じ量のオブジェクトコピーを実行する必要があることに変わりはありません。しかし、10台のOSDがそれぞれ約100GBをコピーしなければならなかったのが、今では50GBずつコピーすればよくなりました。ネットワークがボトルネックになっていた場合、復旧は2倍の速さで行われます。つまり、OSDの数が増えれば増えるほど、復旧は早くなるのです。

このクラスタが40台まで成長した場合、それぞれのOSDは〜35個のPGをホストするだけです。OSDが死んでも、他のボトルネックに阻まれない限り、復旧は高速に進みます。しかし、このクラスタが200 OSDまで成長した場合、各OSDは～7個のPGしかホストしません。OSDが死亡した場合、これらのPG内の最大〜21 \(7 \* 3\) OSDの間で回復します。回復には40 OSDのときよりも時間がかかるので、PGの数を増やす必要があります。

いくら復旧時間が短くても、その間に2台目のOSDが故障する可能性はあります。前述の10個のOSDクラスタでは、そのうちの1個に障害が発生すると、～17個のPG（つまり、～150個/回復中の9個のPG）には、生き残ったコピーが1個しかないことになります。また、残りの8つのOSDのいずれかに障害が発生した場合、2つのPGの最後のオブジェクトが失われる可能性があります（つまり、～17 / 8 PGには、残りの1つのコピーしか回復されていません）。

クラスタのサイズが20 OSDに成長すると、3つのOSDの損失によって損傷を受けるPGの数は減少します。2番目に失われたOSDは、～17ではなく、～4（すなわち、～75 / 19 PGが回復される）デグレードし、3番目に失われたOSDは、生存するコピーを含む4つのOSDの1つである場合にのみデータを失うことになります。言い換えると、リカバリ期間中に 1 つの OSD を失う確率が 0.0001% である場合、OSD が 10 個のクラスターでは 17 \* 10 \* 0.0001% ですが、OSD が 20 個のクラスターでは 4 \* 20 \* 0.0001% になります。

一言で言えば、OSDの数が多ければ多いほど復旧が早くなり、障害が連鎖してPGが永久に失われる危険性が低くなるということです。512または4096のPGを持つことは、データの耐久性に関する限り、50以下のOSDを持つクラスタとほぼ同等です。

注：クラスタに追加された新しいOSDに、割り当てられたPGが反映されるまでには、長い時間がかかる場合があります。しかし、オブジェクトのデグレードはなく、クラスタに含まれるデータの耐久性にも影響はありません。

### Object distribution within a pool[¶](https://docs.ceph.com/en/pacific/rados/operations/placement-groups/ "Permalink to this headline")

各PGにオブジェクトが均等に配置されるのが理想的です。CRUSHはオブジェクトごとにPGを計算しますが、このPG内の各OSDにどれだけのデータが格納されているかは実際には分からないため、PGの数とOSDの数の比率がデータの分布に大きく影響する場合があります。

例えば、3つのレプリカプールで10個のOSDに対して1つのPGがあった場合、CRUSHには他の選択肢がないため、3つのOSDのみが使用されることになります。より多くのPGが利用可能であれば、オブジェクトはより均等に配置される可能性が高くなります。また、CRUSHは既存のすべてのPGにOSDを均等に分散させるよう、あらゆる努力をしています。

PGの数がOSDの数より1〜2桁多ければ、分布は均等になるはずです。例えば、3つのOSDに対して256のPG、10のOSDに対して512または1024のPGなどです。

データの偏在は、OSDとPGの比率以外の要因で発生することもあります。CRUSHはオブジェクトの大きさを考慮しないため、少数の非常に大きなオブジェクトが不均衡を生むことがあります。例えば、100万個の4Kオブジェクト（合計4GB）が、10個のOSD上の1024のPGに均等に配置されたとします。この場合、各OSDで4GB / 10 = 400MBを使用することになります。400MBのオブジェクトが1つプールに追加されると、そのオブジェクトが配置されたPGをサポートする3つのOSDは400MB \+ 400MB = 800MBで満たされ、他の7つは400MBだけで占有されたままとなります。

### Memory, CPU and network usage[¶](https://docs.ceph.com/en/pacific/rados/operations/placement-groups/ "Permalink to this headline")

PGごとに、OSDとMONはメモリ、ネットワーク、CPUを常時必要とし、リカバリ時にはさらに必要となります。PG内のオブジェクトをクラスタリングすることで、このオーバーヘッドを共有することが、彼らの存在意義の1つです。

PGの数を最小限にすることで、大幅な資源節約を実現します。

## Choosing the number of Placement Groups[¶](https://docs.ceph.com/en/pacific/rados/operations/placement-groups/ "Permalink to this headline")

OSDが50台以上ある場合は、リソースの使用量やデータの耐久性、分散性などのバランスを考慮して、OSDあたり約50～100個のPGを推奨しています。OSDが50個以下の場合は、上記の事前選択から選択するのが最適です。オブジェクトの単一プールの場合、次の式で基準値を得ることができます。

Total PGs = ​𝑂𝑆𝐷𝑠×100𝑝𝑜𝑜𝑙𝑠𝑖𝑧𝑒

ここで、pool sizeは、replicatedプールのレプリカ数、またはECプールのK\+M合計\(ceph osd erasure\-code\-profile getで返される\)のいずれかです。

次に、データの耐久性、オブジェクトの分散、リソースの使用を最小限に抑えるためにCephクラスタを設計した方法と結果が一致するかどうかを確認する必要があります。

結果は常に2のべき乗に切り上げなければなりません。

PG間でオブジェクトの数が均等になるのは、2のべき乗だけです。他の値では、OSD間でデータの分布が不均一になります。その使用方法は、2の累乗から別の累乗へ段階的にステップアップする場合に限定されるべきです。

例として、200のOSDと3つのレプリカのプールサイズを持つクラスタの場合、PGの数は次のように見積もられます。

200×1003=6667

2のべき乗に近い値：8192

オブジェクトを格納するために複数のデータプールを使用する場合、プールごとのPGの数とOSDごとのPGの数のバランスをとることを確認する必要があります。これにより、システムリソースに負荷をかけたりピアリングプロセスを遅くしたりせずにOSDごとに適度に低い分散を提供するPGの妥当な合計数に到達します。

例えば、10個のOSDにそれぞれ512のPGを持つ10個のプールのクラスタは、10個のOSDに広がる合計5,120のPG、つまりOSDあたり512のPGとなります。これは、あまり多くのリソースを使用しません。しかし、もし1,000のプールがそれぞれ512のPGで作成された場合、OSDはそれぞれ〜50,000のPGを扱うことになり、ピアリングのためにかなり多くのリソースと時間を必要とすることになります。

 [PGCalc](https://docs.ceph.com/en/pacific/rados/operations/placement-groups/) というツールが役に立つかもしれません。

## Set the Number of Placement Groups[¶](https://docs.ceph.com/en/pacific/rados/operations/placement-groups/ "Permalink to this headline")

プール内のPGの数を設定するには、プールの作成時にPGの数を指定する必要があります。詳細については、「[Create a Pool](https://docs.ceph.com/en/pacific/rados/operations/placement-groups/)」を参照してください。 プールが作成された後でも、プレースメント グループの数を変更することができます。

```
ceph osd pool set {pool-name} pg_num {pg_num}
```

PGの数を増やした後、クラスタがリバランスする前に、配置のためのPGの数（pgp\_num）も増やす必要があります。pgp\_numは、CRUSHアルゴリズムで配置を考慮するPGの数になります。pg\_numを増やすとPGが分割されますが、配置用のPG、つまりpgp\_numが増えるまで、データは新しいPGに移行されません。pgp\_num は pg\_num と同じにする必要があります。 配置用のPGの数を増やすには、以下を実行します。

```
ceph osd pool set {pool-name} pgp_num {pgp_num}
```

PGの数を減らすと、pgp\_numは自動的に調整されます。

## Get the Number of Placement Groups[¶](https://docs.ceph.com/en/pacific/rados/operations/placement-groups/ "Permalink to this headline")

プール内のPG数を取得するには、以下を実行します。

```
ceph osd pool get {pool-name} pg_num
```

## Get a Cluster’s PG Statistics[¶](https://docs.ceph.com/en/pacific/rados/operations/placement-groups/ "Permalink to this headline")

クラスタ内のPGの統計情報を取得するには、以下を実行します。

```
ceph pg dump [--format {format}]
```

有効なフォーマットは plain \(デフォルト\) および json です。

## Get Statistics for Stuck PGs[¶](https://docs.ceph.com/en/pacific/rados/operations/placement-groups/ "Permalink to this headline")

指定した状態で止まっているすべてのPGの統計情報を取得する場合は、次のように実行します。

```
ceph pg dump_stuck inactive|unclean|stale|undersized|degraded [--format <format>] [-t|--threshold <seconds>]
```

**Inactive** PGは、最新のデータを持つOSDの立ち上げを待っているため、読み取りや書き込みを処理できない。

**Unclean** PGには、必要な回数だけレプリケートされていないオブジェクトが含まれています。リカバリーする必要があります。

**Stale** PGは未知の状態です \- それらをホストするOSDは、しばらくモニタークラスタに報告していません（mon\_osd\_report\_timeoutによって構成されます）。

有効な形式は plain \(デフォルト\) および json です。thresholdは、PGが統計に含まれるようになるまでの最小秒数を定義します（デフォルトは300秒）。

## Get a PG Map[¶](https://docs.ceph.com/en/pacific/rados/operations/placement-groups/ "Permalink to this headline")

特定のPGのPGマップを取得する場合は、以下を実行します。

```
ceph pg map {pg-id}
```

For example:

```
ceph pg map 1.6c
```

Cephは、PGマップ、PG、OSDステータスを返します。

```
osdmap e13 pg 1.6c (1.6c) -> up [1,0] acting [1,0]
```

## Get a PGs Statistics[¶](https://docs.ceph.com/en/pacific/rados/operations/placement-groups/ "Permalink to this headline")

特定のPGの統計情報を取得する場合は、以下を実行します。

```
ceph pg {pg-id} query
```

## Scrub a Placement Group[¶](https://docs.ceph.com/en/pacific/rados/operations/placement-groups/ "Permalink to this headline")

PGをスクラブする場合は、以下を実行します。

```
ceph pg scrub {pg-id}
```

Cephはプライマリノードと任意のレプリカノードをチェックし、PG内のすべてのオブジェクトのカタログを生成し、それらを比較して、オブジェクトの欠落や不一致がなく、内容が一貫していることを確認します。 レプリカがすべて一致すると仮定すると、最終的なセマンティックスイープにより、スナップショット関連のオブジェクトのメタデータがすべて一致することが確認されます。エラーはログで報告されます。

特定のプールからすべてのPGをスクラブするには、以下を実行します。

```
ceph osd pool scrub {pool-name}
```

## Prioritize backfill/recovery of a Placement Group\(s\)[¶](https://docs.ceph.com/en/pacific/rados/operations/placement-groups/ "Permalink to this headline")

多くのPGがリカバリや埋め戻しを必要とする状況に遭遇し、いくつかの特定のグループが他のグループよりも重要なデータを保持している場合があります（例えば、それらのPGは稼働中のマシンが使用するイメージのデータを保持し、他のPGは非稼働マシン/関連性の低いデータが使用されている可能性があります）。そのような場合、それらのグループの復旧を優先し、そのグループに保存されているデータのパフォーマンスや可用性を早期に復旧させることができます。これを行うには（バックフィルまたはリカバリの際に特定のPGを優先的にマークする）、以下を実行します。

```
ceph pg force-recovery {pg-id} [{pg-id #2}] [{pg-id #3} ...]
ceph pg force-backfill {pg-id} [{pg-id #2}] [{pg-id #3} ...]
```

これにより、Cephは他のPGより先に、指定されたPGのリカバリまたはバックフィルを先に実行するようになります。これにより、現在進行中のバックフィルまたはリカバリは中断されませんが、指定されたPGができるだけ早く処理されるようになります。気が変わったり、間違ったグループを優先する場合は、以下を使用します。

```
ceph pg cancel-force-recovery {pg-id} [{pg-id #2}] [{pg-id #3} ...]
ceph pg cancel-force-backfill {pg-id} [{pg-id #2}] [{pg-id #3} ...]
```

これにより、これらのPGから "force "フラグが削除され、デフォルトの順序で処理されるようになります。繰り返しますが、これは現在処理中のPGには影響せず、まだキューに入っているPGにのみ影響します。

forceフラグは、グループの回復または埋め戻しが行われた後、自動的にクリアされます。

同様に、以下のコマンドを使用して、指定したプールからすべてのPGに対して最初にリカバリまたはバックフィルを実行するようCephに強制することもできます。

```
ceph osd pool force-recovery {pool-name}
ceph osd pool force-backfill {pool-name}
```

or:

```
ceph osd pool cancel-force-recovery {pool-name}
ceph osd pool cancel-force-backfill {pool-name}
```

気が変わった場合は、デフォルトのリカバリまたはバックフィルの優先度に復元します。

これらのコマンドは、Ceph内部の優先度計算の順序を崩す可能性があるため、注意して使用してください\! 特に、現在同じ基礎OSDを共有している複数のプールがあり、一部の特定のプールが他のプールよりも重要なデータを保持している場合、次のコマンドを使用して、すべてのプールのリカバリ/バックフィル優先順位をより良い順序に並べ替えることをお勧めします。

```
ceph osd pool set {pool-name} recovery_priority {value}
```

例えば、10個のプールがある場合、最も重要なプールを優先度10に、その次を9に、といった具合に設定することができます。あるいは、ほとんどのプールはそのままにして、例えば3つの重要なプールをすべて優先度1、あるいはそれぞれ優先度3、2、1にすることも可能です。

## Revert Lost[¶](https://docs.ceph.com/en/pacific/rados/operations/placement-groups/ "Permalink to this headline")

クラスタが1つ以上のオブジェクトを失い、失われたデータの検索を放棄することにした場合、未発見のオブジェクトを失われたものとしてマークする必要があります。

すべての可能な場所を照会しても、オブジェクトが失われる場合は、失われたオブジェクトをあきらめなければならないかもしれません。これは、書き込みそのものが回復する前に、実行された書き込みについてクラスタが知ることができるような、異常な故障の組み合わせの場合に可能です。

現在サポートされているオプションは「revert」のみで、これはオブジェクトの以前のバージョンにロールバックするか、（それが新しいオブジェクトだった場合）完全に忘れるかのどちらかです。「unfound」のオブジェクトを「lost」としてマークするには、次のように実行します。

```
ceph pg {pg-id} mark_unfound_lost revert|delete
```

重要：この機能は、オブジェクトが存在することを期待するアプリケーションを混乱させる可能性があるため、注意して使用してください。
