# 450: new-db

**クリップソース:** [450: new\-db — Ceph Documentation](https://docs.ceph.com/en/pacific/ceph-volume/lvm/newdb/)

# new\-db[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/newdb/ "Permalink to this headline")

指定された論理ボリュームをDBとしてOSDにアタッチします。論理ボリューム名の形式はvg/lvです。OSDが既にアタッチされたDBを持っている場合は失敗します。

OSD 1にDBボリュームとしてvgname/lvnameをアタッチするには：

```
ceph-volume lvm new-db --osd-id 1 --osd-fsid 55BD4219-16A7-4037-BC20-0F158EFCC83D --target vgname/new_db
```
