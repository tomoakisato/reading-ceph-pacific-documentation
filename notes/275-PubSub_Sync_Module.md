# 275: PubSub Sync Module

**クリップソース:** [275: PubSub Sync Module — Ceph Documentation](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)

# [PubSub Sync Module](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)[¶](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/ "Permalink to this headline")

Nautilus バージョンの新機能です。

Contents

* [PubSub Zone Configuration](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)
    [PubSub Zone Configuration Parameters](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)
    [Configuring Parameters via CLI](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)
    [Topic and Subscription Management via CLI](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)
    [PubSub Performance Stats](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)
    [PubSub REST API](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)
    [Topics](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)
    [Create a Topic](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)
    [Get Topic Information](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)
    [Delete Topic](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)
    [List Topics](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)
    [S3\-Compliant Notifications](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)
    [Non S3\-Compliant Notifications](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)
    [Create a Notification](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)
    [Delete Notification Information](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)
    [List Notifications](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)
    [Subscriptions](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)
    [Create a Subscription](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)
    [Get Subscription Information](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)
    [Delete Subscription](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)
    [Events](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)
    [Pull Events](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)
    [Ack Event](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)
    [PubSub Sync Module](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)

このシンクモジュールは、オブジェクトストアの変更イベントのpublishとsubscribeのメカニズムを提供します。イベントはあらかじめ定義されたトピックにpublishされます。トピックはsubscribeすることができ、イベントはそこからpullすることができます。イベントはACKされる必要があります。また、イベントは一定期間が過ぎるとexpireして消えてしまいます。

プッシュ通知の仕組みもあり、現在はHTTP、AMQP0.9.1、Kafkaのエンドポイントをサポートしています。この場合、イベントはCephに保存された上で、エンドポイントにプッシュされます。イベントをエンドポイントにプッシュするだけで、Cephに保存する必要がない場合は、pubsub syncモジュールの代わりに、[Bucket Notification](https://docs.ceph.com/en/pacific/radosgw/notifications)メカニズムを使用する必要があります。

ユーザは、異なるトピックを作成することができます。トピックの実体は、その名前で定義され、テナントごとにあります。ユーザは、（通知設定によって）自分のトピックを自分が所有するバケットにのみ関連付けることができます。

特定のバケットでイベントをpublishするには、通知（notification）エンティティを作成する必要があります。通知エンティティは、イベントタイプのサブセット、またはすべてのイベントタイプ（デフォルト）で作成することができます。1つのトピックに複数のnotificationを設定でき、同じトピックが複数のnotificationに使用されることもあります。

トピックの購読（subscription）を定義することもできます。1つのトピックに複数のサブスクリプションを設定することができます。

pubsub機構の設定・制御インターフェースを提供するために、REST APIが定義されています。このAPIには2つのフレーバーがあり、1つはS3互換で、もう1つは非互換です。2つのフレーバーは一緒に使用することができますが、S3互換のものを使用することが推奨されます。S3互換APIは、バケット通知メカニズムで使用されているものに似ています。

イベントはRGWオブジェクトとして、特別なバケットに、特別なユーザ（pubsubコントロールユーザ）の下に保存されます。イベントには直接アクセスできません。新しい REST API を使ってプルしたりアクセプトしたりする必要があります。

* [S3 Bucket Notification Compatibility](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)

注意：Bucket Notification APIを有効にするには、rgw\_enable\_apis設定パラメータに "notifications "を指定する必要があります。

## [PubSub Zone Configuration](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)[¶](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/ "Permalink to this headline")

pubsub syncモジュールは、[マルチサイト](https://docs.ceph.com/en/pacific/radosgw/multisite/#multisite)環境で新しいゾーンを作成する必要があります。まず、マスターゾーンが存在する必要があり（参照：[マスターゾーンの構成](https://docs.ceph.com/en/pacific/radosgw/multisite/#master-zone-label)）、次にセカンダリーゾーンを作成する必要があります（参照：[セカンダリーゾーンの構成](https://docs.ceph.com/en/pacific/radosgw/multisite/#secondary-zone-label)）。セカンダリゾーンの作成では、tier\-typeにpubsubを指定します：

```
# radosgw-admin zone create --rgw-zonegroup={zone-group-name} 
                            --rgw-zone={zone-name} 
                            --endpoints={http://fqdn}[,{http://fqdn}] 
                            --sync-from-all=0 
                            --sync-from={master-zone-name} 
                            --tier-type=pubsub
```

### [PubSub Zone Configuration Parameters](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)[¶](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/ "Permalink to this headline")

```
{
    "tenant": <tenant>,             # default: <empty>
    "uid": <uid>,                   # default: "pubsub"
    "data_bucket_prefix": <prefix>  # default: "pubsub-"
    "data_oid_prefix": <prefix>     #
    "events_retention_days": <days> # default: 7
}
```

* tenant \(string\)

pubsubコントロールユーザのテナント

* uid \(string\)

pubsubコントロールユーザのuid

* data\_bucket\_prefix \(string\)

特定のトピックのイベントを格納するために作成されるバケット名のプレフィックス

* data\_oid\_prefix \(string\)

保存されたイベントの OID プレフィクス

* events\_retention\_days \(integer\)

ACKされなかったイベントの保管日数

### [Configuring Parameters via CLI](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)[¶](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/ "Permalink to this headline")

tier\-configは、以下のコマンドで設定することができます：

```
# radosgw-admin zone modify --rgw-zonegroup={zone-group-name} 
                             --rgw-zone={zone-name} 
                             --tier-config={key}={val}[,{key}={val}]
```

ここで、設定のキーは更新が必要な設定変数（上記のリストから）を指定し、valはその新しい値を指定します。例えば、pubsubコントロールユーザuidをuser\_psに設定するには：

```
# radosgw-admin zone modify --rgw-zonegroup={zone-group-name} 
                             --rgw-zone={zone-name} 
                             --tier-config=uid=pubsub
```

設定フィールドは、\-\-tier\-config\-rm={key}を使用することで削除することができます。

## [Topic and Subscription Management via CLI](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)[¶](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/ "Permalink to this headline")

あるテナントに関連するすべてのトピック設定は、次のコマンドで取得できます：

```
# radosgw-admin topic list [--tenant={tenant}]
```

特定のトピック設定を取得するには：

```
# radosgw-admin topic get --topic={topic-name} [--tenant={tenant}]
```

特定のトピック設定を削除するには：

```
# radosgw-admin topic rm --topic={topic-name} [--tenant={tenant}]
```

サブスクリプション設定は、以下を使用して取得できます：

```
# radosgw-admin subscription get --subscription={topic-name} [--tenant={tenant}]
```

特定のサブスクリプション設定を削除するには：

```
# radosgw-admin subscription rm --subscription={topic-name} [--tenant={tenant}]
```

サブスクリプションに格納されているすべてのイベントを取得するには、以下を使用します：

```
# radosgw-admin subscription pull --subscription={topic-name} [--marker={last-marker}] [--tenant={tenant}]
```

サブスクリプションからイベントをACK（および削除）するには、以下を使用します：

```
# radosgw-admin subscription ack --subscription={topic-name} --event-id={event-id} [--tenant={tenant}]
```

## [PubSub Performance Stats](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)[¶](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/ "Permalink to this headline")

pubsub syncモジュールと通知機構の間で同じカウンタが共有されます。

* pubsub\_event\_triggered: 少なくとも1つのトピックが関連付けられているイベント
* pubsub\_event\_lost: トピックとサブスクリプションが関連付けられているが、どのサブスクリプションにも保存またはプッシュされていないイベント
* pubsub\_store\_ok: イベントが保存された、サブスクリプション
* pubsub\_store\_fail: イベント保存に失敗した、サブスクリプション
* pubsub\_push\_ok: イベントがエンドポイントに正常にプッシュされた、サブスクリプション
* pubsub\_push\_fail: イベントのエンドポイントへのプッシュに失敗した、サブスクリプション
* pubsub\_push\_pending: エンドポイントにプッシュされたが、まだACK/NACKされていないイベントのゲージ値

pubsub\_event\_triggered と pubsub\_event\_lost はイベントごとにインクリメントされ、pubsub\_store\_ok, pubsub\_store\_fail, pubsub\_push\_ok, pubsub\_push\_fail はそれぞれの購読の store/push 操作ごとにインクリメントされることに注意してください。

## [PubSub REST API](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)[¶](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/ "Permalink to this headline")

ヒント: PubSub RESTコールは、PubSubゾーンに属するRGWにのみ送信する必要があります。

### [Topics](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)[¶](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/ "Permalink to this headline")

#### [Create a Topic](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)[¶](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/ "Permalink to this headline")

これにより、新しいトピックが作成されます。 トピックの作成は、APIの両方のフレーバーに必要です。 オプションで、トピックには、後でS3互換の通知が作成されるときに使用されるプッシュエンドポイントパラメーターを提供できます。 リクエストが成功すると、応答にはトピックARNが含まれ、後でS3互換の通知リクエストでこのトピックを参照するために使用できます。 トピックを更新するには、トピックの作成に使用したのと同じコマンドを使用し、既存のトピックのトピック名と異なるエンドポイント値を使用します。

ヒント：トピックの更新を有効にするには、トピックにすでに関連付けられているS3互換の通知を再作成する必要があります

```
PUT /topics/<topic-name>[?OpaqueData=<opaque data>][&push-endpoint=<endpoint>[&amqp-exchange=<exchange>][&amqp-ack-level=none|broker|routable][&verify-ssl=true|false][&kafka-ack-level=none|broker][&use-ssl=true|false][&ca-location=<file path>]]
```

リクエストパラメータ：

* push\-endpoint: プッシュ通知の送信先となるエンドポイントURI
* OpaqueData: opaque データはトピックに設定され、トリガするすべての通知に追加される

エンドポイント URI には、エンドポイントの種類に応じたパラメータを含めることができます：

* HTTP endpoint
    * URI: http\[s\]://\<fqdn\>\[:\<port\]
    * port defaults to: 80/443 for HTTP/S accordingly
    * verify\-ssl: サーバ証明書をクライアントが検証するかどうか（デフォルトは "true"）
* AMQP0.9.1 endpoint
    * URI: amqp\[s\]://\[\<user\>:\<password\>@\]\<fqdn\>\[:\<port\>\]\[/\<vhost\>\]
    * user/password defaults to: guest/guest
    * user/password は HTTPS でのみ提供可能です。そうでない場合、トピック作成リクエストは拒否されます
    * port defaults to: 5672/5671 for unencrypted/SSL\-encrypted connections
    * vhost defaults to: “/”
    * verify\-ssl: サーバ証明書をクライアントで検証するかどうか（デフォルトは "true"）
    * ca\-location が提供され、セキュアな接続が使用される場合、ブローカーを認証するために、デフォルトの CA ではなく、指定された CA が使用されます
    * amqp\-exchange: エクスチェンジが存在し、トピックに基づいてメッセージをルーティングできること（AMQP0.9.1では必須パラメータ）。同じエンドポイントを指す異なるトピックは、同じエクスチェンジを使用しなければならない
    * amqp\-ack\-level: メッセージは最終的な宛先に配送される前にブローカーに留まる可能性があるため、エンドツーエンドのACKは必要ありません。3 つのACKメソッドがあります：
        * “none”: メッセージはブローカーに送信された場合、「配送された」とみなされる
        * “broker”: メッセージはブローカーからACKされた場合、「配送された」とみなされる \(デフォルト\)
        * “routable”: メッセージは、ブローカーがコンシューマーにルーティングできる場合、「配信された」とみなされる

ヒント：topic\-name（「[トピックの作成](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/#radosgw-create-a-topic)」を参照）は、AMQPトピック（トピック交換の「ルーティング・キー」）に使用されます

* Kafka endpoint
    * URI: kafka://\[\<user\>:\<password\>@\]\<fqdn\>\[:\<port\]
    * use\-ssl を "true" に設定すると、ブローカーとの接続にセキュアな接続が使用されます \(デフォルトは "false" \)
    * ca\-location が提供され、セキュアな接続が使用される場合、ブローカーを認証するために、デフォルトの CA ではなく、指定された CA が使用されます
    * user/password は HTTPS でのみ提供可能です。そうでない場合、トピック作成リクエストは拒否されます
    * user/password は use\-ssl と共に提供する必要があり、そうでない場合はブローカーへの接続に失敗します
    * port defaults to: 9092
    * kafka\-ack\-level: メッセージは最終的な宛先に配送される前にブローカーに留まる可能性があるため、エンドツーエンドのACKは必要ではありません。2 つのACKメソッドがあります：
        * “none”: メッセージはブローカーに送信された場合、「配送された」とみなされる
        * “broker”: メッセージはブローカーからACKされた場合、「配送された」とみなされる \(デフォルト\)

レスポンスに含まれるトピックARNは、以下のような形式となります：

```
arn:aws:sns:<zone-group>:<tenant>:<topic>
```

#### [Get Topic Information](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)[¶](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/ "Permalink to this headline")

特定のトピックに関する情報を返します。これには、そのトピックへのサブスクリプションと、提供されている場合はプッシュエンドポイント情報が含まれます。

```
GET /topics/<topic-name>
```

Response will have the following format \(JSON\):

```
{
    "topic":{
        "user":"",
        "name":"",
        "dest":{
            "bucket_name":"",
            "oid_prefix":"",
            "push_endpoint":"",
            "push_endpoint_args":"",
            "push_endpoint_topic":"",
            "stored_secret":"",
            "persistent":""
        },
        "arn":""
        "opaqueData":""
    },
    "subs":[]
}
```

* topic.user: トピックを作成したユーザ名
* name: トピック名
* dest.bucket\_name: not used
* dest.oid\_prefix: not used
* dest.push\_endpoint:S3準拠の通知の場合、この値はプッシュエンドポイントURLとして使用されます
* push\-endpointのURLにユーザー/パスワード情報が含まれている場合、リクエストはHTTPSで行う必要があります。そうでない場合、トピックのGETリクエストは拒否されます
* dest.push\_endpoint\_args: S3準拠の通知の場合、この値はプッシュエンドポイントの引数として使用されます。
* dest.push\_endpoint\_topic:S3準拠の通知の場合、この値はエンドポイントに送信されたトピック名を保持します\(内部トピック名と異なる場合があります\)
* topic.arn: topic ARN
* subs: このトピックに関連するサブスクリプションのリスト

#### [Delete Topic](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)[¶](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/ "Permalink to this headline")

```
DELETE /topics/<topic-name>
```

指定されたトピックを削除します。

#### [List Topics](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)[¶](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/ "Permalink to this headline")

テナントに関連するすべてのトピックを一覧表示します。

```
GET /topics
```

* push\-endpointのURLにユーザー/パスワード情報が含まれている場合、HTTPSでリクエストを行う必要があります。そうでない場合、トピック一覧のリクエストは拒否されます。

### [S3\-Compliant Notifications](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)[¶](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/ "Permalink to this headline")

詳細はこちら：[バケット操作](https://docs.ceph.com/en/pacific/radosgw/s3/bucketops)

Note:

* 通知の作成は、イベントのPush/Pullのためのサブスクリプションも作成します
* 生成されたサブスクリプションの名前は通知IDと同じになり、後でサブスクリプションAPIでイベントをフェッチして確認するために使用できます
* 通知削除は、生成されたすべてのサブスクリプションを削除します
* バケット削除が暗黙のうちに通知を削除する場合、関連するサブスクリプションは自動的に削除されず（削除されたバケットのイベントはまだアクセスできます）、サブスクリプション削除APIで明示的に削除する必要があります
* メタデータに基づくフィルタリング（S3 の拡張機能）はサポートされておらず、そのようなルールは無視されます
* タグに基づくフィルタリング（S3 の拡張機能）はサポートされておらず、そのようなルールは無視されます

### [Non S3\-Compliant Notifications](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)[¶](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/ "Permalink to this headline")

#### [Create a Notification](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)[¶](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/ "Permalink to this headline")

これにより、バケットのpublisherがトピックに作成されます。

```
PUT /notifications/bucket/<bucket>?topic=<topic-name>[&events=<event>[,<event>]]
```

リクエストパラメータ：

* topic\-name: トピック名
* event: イベントタイプ \(文字列\)、次のいずれか：OBJECT\_CREATE, OBJECT\_DELETE, DELETE\_MARKER\_CREATE

#### [Delete Notification Information](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)[¶](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/ "Permalink to this headline")

バケットのパブリッシャーをトピックから削除します。

```
DELETE /notifications/bucket/<bucket>?topic=<topic-name>
```

リクエストパラメータ：

* topic\-name: トピック名

注意：バケットを削除すると、バケットに定義されているすべての通知も削除されます。

#### [List Notifications](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)[¶](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/ "Permalink to this headline")

Bucket に定義されたすべてのトピックと関連するイベントの一覧を表示します。

```
GET /notifications/bucket/<bucket>
```

Response will have the following format \(JSON\):

```
{"topics":[
   {
      "topic":{
         "user":"",
         "name":"",
         "dest":{
            "bucket_name":"",
            "oid_prefix":"",
            "push_endpoint":"",
            "push_endpoint_args":"",
            "push_endpoint_topic":""
         }
         "arn":""
      },
      "events":[]
   }
 ]}
```

### [Subscriptions](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)[¶](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/ "Permalink to this headline")

#### [Create a Subscription](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)[¶](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/ "Permalink to this headline")

新しいサブスクリプションを作成します。

```
PUT /subscriptions/<sub-name>?topic=<topic-name>[?push-endpoint=<endpoint>[&amqp-exchange=<exchange>][&amqp-ack-level=none|broker|routable][&verify-ssl=true|false][&kafka-ack-level=none|broker][&ca-location=<file path>]]
```

リクエストパラメータ：

* topic\-name: トピック名
* push\-endpoint: プッシュ通知の送信先となるエンドポイントURI

エンドポイント URI には、エンドポイントの種類に応じたパラメータを含めることができます：

* HTTP endpoint
    * URI: http\[s\]://\<fqdn\>\[:\<port\]
    * port defaults to: 80/443 for HTTP/S accordingly
    * verify\-ssl:サーバ証明書をクライアントが検証するかどうか（デフォルトは "true"）
* AMQP0.9.1 endpoint
    * URI: amqp://\[\<user\>:\<password\>@\]\<fqdn\>\[:\<port\>\]\[/\<vhost\>\]
    * user/password defaults to : guest/guest
    * port defaults to: 5672
    * vhost defaults to: “/”
    * amqp\-exchange: エクスチェンジが存在し、トピックに基づいてメッセージをルーティングできること \(AMQP0.9.1 の必須パラメータ\)
    * amqp\-ack\-level: メッセージは最終的な宛先に配送される前にブローカーに留まる可能性があるため、エンドツーエンドのACKは必要ありません。3 つのACKメソッドがあります：
        * “none”: メッセージはブローカーに送信された場合、「配送された」とみなされます
        * “broker”: メッセージはブローカーからACKされた場合、「配送された」とみなされます \(デフォルト\)
        * “routable”: メッセージは、ブローカーがコンシューマーにルーティングできる場合、「配信された」とみなされます
* Kafka endpoint
    * URI: kafka://\[\<user\>:\<password\>@\]\<fqdn\>\[:\<port\]
    * ca\-locationが提供された場合、ブローカーとの接続にセキュアな接続が使用されます
    * user/password は HTTPS でのみ提供可能です。そうでない場合、サブスクリプション作成リクエストは拒否されます
    * user/password は ca\-location と共にのみ提供することができます。そうでない場合、サブスクリプション作成リクエストは拒否されます。
    * port defaults to: 9092
    * kafka\-ack\-level: メッセージは最終的な宛先に配送される前にブローカーに留まる可能性があるため、エンドツーエンドのACKは必要ではありません。2 つのACKメソッドがあります：
        * “none”: メッセージはブローカーに送信された場合、「配送された」とみなされます
        * “broker”: メッセージはブローカーからアクセプトされた場合、「配送された」とみなされます \(デフォルト\)

#### [Get Subscription Information](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)[¶](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/ "Permalink to this headline")

特定のサブスクリプションに関する情報を返します。

```
GET /subscriptions/<sub-name>
```

レスポンスは以下のフォーマット（JSON）になります：

```
{
    "user":"",
    "name":"",
    "topic":"",
    "dest":{
        "bucket_name":"",
        "oid_prefix":"",
        "push_endpoint":"",
        "push_endpoint_args":"",
        "push_endpoint_topic":""
    }
    "s3_id":""
}
```

* user: サブスクリプションを作成したユーザ名
* name: サブスクリプション名
* topic: サブスクリプションが関連付けられているトピック名
* dest.bucket\_name: イベントを保存しているバケット名
* dest.oid\_prefix: バケットに格納されているイベントの OID プレフィックス
* dest.push\_endpoint:S3準拠の通知の場合、この値はプッシュエンドポイントURLとして使用されます
* push\-endpointのURLにユーザー/パスワード情報が含まれている場合、リクエストはHTTPSで行う必要があります。そうでない場合、サブスクリプションの取得リクエストは拒否されます
* dest.push\_endpoint\_args: S3準拠の通知の場合、この値はプッシュエンドポイントの引数として使用されます
* dest.push\_endpoint\_topic: S3準拠の通知の場合、この値はエンドポイントに送信されたトピック名を保持します\(内部トピック名と異なる場合があります\)
* s3\_id: S3準拠の通知の場合、これはサブスクリプションを作成した通知名を保持します

#### [Delete Subscription](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)[¶](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/ "Permalink to this headline")

サブスクリプションを削除します。

```
DELETE /subscriptions/<sub-name>
```

### [Events](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)[¶](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/ "Permalink to this headline")

#### [Pull Events](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)[¶](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/ "Permalink to this headline")

特定のサブスクリプションに送信されたイベントをPULLします。

```
GET /subscriptions/<sub-name>?events[&max-entries=<max-entries>][&marker=<marker>]
```

リクエストパラメータ：

* marker: イベントのリストのページネーションマーカーは、指定されていない場合、最も古いものから始まります
* max\-entries: 返送するイベントの最大数

この応答には、現在のマーカーに関する情報と、まだ取得されていないイベントがあるかどうかの情報が含まれます：

```
{"next_marker":"","is_truncated":"",...}
```

レスポンスの実際の内容は、サブスクリプションがどのように作成されたかに依存します。S3互換の通知によってサブスクリプションが作成された場合、イベントはS3互換のレコードフォーマット（JSON）を持っています：

```
{"Records":[
    {
        "eventVersion":"2.1"
        "eventSource":"aws:s3",
        "awsRegion":"",
        "eventTime":"",
        "eventName":"",
        "userIdentity":{
            "principalId":""
        },
        "requestParameters":{
            "sourceIPAddress":""
        },
        "responseElements":{
            "x-amz-request-id":"",
            "x-amz-id-2":""
        },
        "s3":{
            "s3SchemaVersion":"1.0",
            "configurationId":"",
            "bucket":{
                "name":"",
                "ownerIdentity":{
                    "principalId":""
                },
                "arn":"",
                "id":""
            },
            "object":{
                "key":"",
                "size":"0",
                "eTag":"",
                "versionId":"",
                "sequencer":"",
                "metadata":[],
                "tags":[]
            }
        },
        "eventId":"",
        "opaqueData":"",
    }
]}
```

* awsRegion: ゾーングループ
* eventTime: イベントが発生した時刻を示すタイムスタンプ
* eventName: s3:ObjectCreated: または s3:ObjectRemoved: のいずれかです
* userIdentity: not supported
* requestParameters: not supported
* responseElements: not supported
* s3.configurationId: イベントに対するサブスクリプションを作成した通知ID
* s3.bucket.name: バケット名
* s3.bucket.ownerIdentity.principalId: バケットの所有者
* s3.bucket.arn: バケットのARN
* s3.bucket.id: バケットID \(S3 notification APIの拡張\)
* s3.object.key: オブジェクトキー
* s3.object.size: not supported
* s3.object.eTag: オブジェクトetag
* s3.object.version: バージョン管理されたバケットの場合、オブジェクトのバージョン
* s3.object.sequencer: オブジェクトごとに単調増加する変更の識別子（16進数）
* s3.object.metadata: サポートされていない（S3通知APIの拡張機能）
* s3.object.tags: サポートされていない（S3通知APIの拡張機能）
* s3.eventId: ACKに使用できるイベントID（S3通知APIの拡張）
* s3.opaqueData: opaqueデータは、トピックに設定され、トリガーされたすべての通知に追加されます（S3通知APIの拡張機能）

S3互換性のない通知によってサブスクリプションが作成されなかった場合、イベントは以下のイベントフォーマット（JSON）になります：

```
 {"events":[
    {
        "id":"",
        "event":"",
        "timestamp":"",
        "info":{
            "attrs":{
                "mtime":""
            },
            "bucket":{
                "bucket_id":"",
                "name":"",
                "tenant":""
            },
            "key":{
                "instance":"",
                "name":""
            }
        }
    }
]}
```

* id: イベントのユニークなID
* event: 次のいずれかになります：OBJECT\_CREATE, OBJECT\_DELETE, DELETE\_MARKER\_CREATE
* timestamp: イベントが送信された時刻を示すタイムスタンプ
* info.attrs.mtime: イベントが発生した時刻を示すタイムスタンプ
* info.bucket.bucket\_id: バケットID
* info.bucket.name: バケット名
* info.bucket.tenant: バケットの属するテナント
* info.key.instance: バージョン管理されたバケットの場合、オブジェクトのバージョン
* info.key.name: オブジェクトキー

#### [Ack Event](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/)[¶](https://docs.ceph.com/en/pacific/radosgw/pubsub-module/ "Permalink to this headline")

サブスクリプション履歴から削除できるようにイベントをACKします。

```
POST /subscriptions/<sub-name>?ack&event-id=<event-id>
```

リクエストパラメータ：

* event\-id: 受信するイベントID
