# 169: Handling a full Ceph file system

**クリップソース:** [169: Handling a full Ceph file system — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/full/)

# Handling a full Ceph file system[¶](https://docs.ceph.com/en/pacific/cephfs/full/ "Permalink to this headline")

RADOSクラスタがmon\_osd\_full\_ratio（デフォルト95%）の容量に達すると、OSD fullフラグが表示されます。 このフラグにより、ほとんどの通常のRADOSクライアントは、このフラグが解決される（たとえば、クラスタに容量を追加する）まですべてのオペレーションを一時停止します。

ファイルシステムは、以下に説明するように、fullフラグに対していくつかの特別な処理をしています。

## Hammer and later[¶](https://docs.ceph.com/en/pacific/cephfs/full/ "Permalink to this headline")

Hammerリリース以来、fullファイルシステムでは以下がENOSPCのになります：

* クライアントでのデータ書き込み
* 削除とトランケート以外のメタデータ操作

データがディスクにフラッシュされるまで、full状態にならないことがあるため、アプリケーションが（書き込みコールがすでに0を返した後）ファイルハンドル上でfsyncまたはfclose（相当）を呼び出すまで、ENOSPCエラーが現れないことがあります。

fsyncを呼び出すと、データがディスクに到達したかどうかが確実に示され、そうでない場合はエラーが返されます。 fcloseは、最後の書き込み以降にバッファリングされたデータがフラッシュされた場合にのみエラーを返します。 成功したfcloseは、データがディスクに到達したことを保証するものではありません。fullスペースの状況では、fcloseの後に、永続化するスペースがない場合、バッファーに入れられたデータが破棄される可能性があります。

警告：fullファイルシステム上でアプリケーションが誤動作しているように見える場合、処理を進める前に、データがディスク上にあることを確認するために、必要に応じてfsync\(\)コールを実行していることを確認してください。

データの書き込みは、OSD fullフラグが送信された時点で実行中であれば、クライアントによってキャンセルされる可能性があります。 クライアントは、キャンセルされた操作がMDSや他のクライアントによるデータオブジェクトへのその後のアクセスに影響を与えないようにするため、キャンセルされた操作によって影響を受けたファイルのCAPSをリリースする際にosd\_epoch\_barrierを更新します。 エポックバリアメカニズムの詳細については、「[背景：ブロックリストとOSDエポックバリア](https://docs.ceph.com/en/pacific/cephfs/eviction/#background-blocklisting-and-osd-epoch-barrier)」を参照してください。

## Legacy \(pre\-hammer\) behavior[¶](https://docs.ceph.com/en/pacific/cephfs/full/ "Permalink to this headline")

CephのHammer以前のバージョンでは、MDSがRADOSクラスタのfull状態を無視し、クライアントからのデータ書き込みはクラスタがfullでなくなるまで停止していました。

この振る舞いで気をつけなければならない危険な状態が2つあります：

* クライアントがファイルへの書き込みを保留している場合、MDSのファイル削除のためにクライアントがファイルをリリースできないため、ファイルシステムが一杯になった場合に容量確保が困難になります。
* クライアントが大量の空のファイルを作成し続けた場合、MDSからのメタデータ書き込みの結果、OSD上の容量が完全に枯渇し、それ以上削除ができなくなる可能性があります。
