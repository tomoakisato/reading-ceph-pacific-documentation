# 45: Configuration

**クリップソース:** [45: Configuration — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/configuration/)

# Configuration[¶](https://docs.ceph.com/en/pacific/rados/configuration/ "Permalink to this headline")

Cephの各プロセス、デーモン、ユーティリティは、起動時に、ローカル設定、モニタ、コマンドライン、環境変数など、複数のソースから設定を引き出します。 設定オプションは、すべてのデーモンに適用するようにグローバルに設定したり、特定の種類のデーモンやサービスに適用したり、特定のデーモン、プロセス、クライアントにのみ適用したりできます。

### Configuring the Object Store

一般的なオブジェクトストアの設定については、以下を参照してください。

* [Storage devices](https://docs.ceph.com/en/pacific/rados/configuration/)
* [Configuring Ceph](https://docs.ceph.com/en/pacific/rados/configuration/)

### Reference

クラスターのパフォーマンスを最適化するには、以下を参照してください。

* [Common Settings](https://docs.ceph.com/en/pacific/rados/configuration/)
* [Networks](https://docs.ceph.com/en/pacific/rados/configuration/)
* [Monitors](https://docs.ceph.com/en/pacific/rados/configuration/)
* [Authentication](https://docs.ceph.com/en/pacific/rados/configuration/)
* [OSDs](https://docs.ceph.com/en/pacific/rados/configuration/)
* [Heartbeats](https://docs.ceph.com/en/pacific/rados/configuration/)
* [Logs / Debugging](https://docs.ceph.com/en/pacific/rados/configuration/)
* [Example ceph.conf](https://docs.ceph.com/en/pacific/rados/configuration/)
* [Running Multiple Clusters \(DEPRECATED\)](https://docs.ceph.com/en/pacific/rados/configuration/)
* [Network Settings](https://docs.ceph.com/en/pacific/rados/configuration/)
* [Messenger v2 protocol](https://docs.ceph.com/en/pacific/rados/configuration/)
* [Auth Settings](https://docs.ceph.com/en/pacific/rados/configuration/)
* [Monitor Settings](https://docs.ceph.com/en/pacific/rados/configuration/)
* [Looking up Monitors through DNS](https://docs.ceph.com/en/pacific/rados/configuration/)
* [Heartbeat Settings](https://docs.ceph.com/en/pacific/rados/configuration/)
* [OSD Settings](https://docs.ceph.com/en/pacific/rados/configuration/)
* [DmClock Settings](https://docs.ceph.com/en/pacific/rados/configuration/)
* [BlueStore Settings](https://docs.ceph.com/en/pacific/rados/configuration/)
* [FileStore Settings](https://docs.ceph.com/en/pacific/rados/configuration/)
* [Journal Settings](https://docs.ceph.com/en/pacific/rados/configuration/)
* [Pool, PG & CRUSH Settings](https://docs.ceph.com/en/pacific/rados/configuration/)
* [Messaging Settings](https://docs.ceph.com/en/pacific/rados/configuration/)
* [General Settings](https://docs.ceph.com/en/pacific/rados/configuration/)
