# 18: Manual Deployment

**クリップソース:** [18: Manual Deployment — Ceph Documentation](https://docs.ceph.com/en/pacific/install/manual-deployment/)

# Manual Deployment¶

すべてのCephクラスタには、少なくとも1つのモニタと、クラスタに保存されているオブジェクトのコピーと同数のOSDが必要です。 初期のモニタをブートストラップさせることが、Ceph Storage Clusterを展開する最初のステップです。モニタの導入では、プールのレプリカ数、OSDごとの配置グループ数、ハートビート間隔、認証が必要かどうかなど、クラスタ全体の重要な基準も設定します。これらの値のほとんどはデフォルトで設定されているので、本番用のクラスタを設定する際に知っておくと便利です。

We will set up a cluster with node1 as the monitor node, and node2 and node3 for OSD nodes. 

ここでは、node1をモニターノードとし、node2とnode3をOSDノードとするクラスタを構築します。

![6dcef33553a09f32a137e61285de056fe1ac59fb0a3becfc6ddb75dbcc65e1c7.png](image/6dcef33553a09f32a137e61285de056fe1ac59fb0a3becfc6ddb75dbcc65e1c7.png)

## **Monitor Bootstrapping[¶](https://docs.ceph.com/en/pacific/install/manual-deployment/#monitor-bootstrapping "Permalink to this headline")**

モニター\(理論上はCeph Storage Cluster\)をブートストラップするには、いくつかのことが必要です。

* **Unique Identifier:**
* **Cluster Name:**
* **Monitor Name:**
* **Monitor Map:**
* **Monitor Keyring**
* **Administrator Keyring**

前述の要件は、Ceph構成ファイルの作成を意味するものではありません。ただし、ベストプラクティスとして、Ceph構成ファイルを作成して、fsid、mon初期メンバー、monホスト設定を入力することをお勧めします。

ランタイムでも、すべてのモニタ設定を取得して設定することができます。ただし、Ceph構成ファイルには、デフォルト値をオーバーライドする設定のみが含まれている場合があります。Ceph構成ファイルに設定を追加すると、これらの設定がデフォルトの設定を上書きします。Ceph構成ファイルでそれらの設定を維持すると、クラスタの保守が容易になります。

その手順は以下の通りです。

1. 初期のモニターノードにログインします。

```
ssh {hostname}
```

例えば、以下のように。

```
ssh node1
```

2. Cephの設定ファイルを置くディレクトリがあることを確認します。デフォルトでは、Cephは/etc/cephを使用します。cephをインストールすると、インストーラが/etc/cephディレクトリを自動的に作成します。

```
ls /etc/ceph
```

3. Cephの設定ファイルを作成します。デフォルトでは、Cephはceph.confを使用し、cephにはクラスタ名が反映されます。

```
sudo vim /etc/ceph/ceph.conf
```

4. クラスタに固有のID（fsid）を生成します。

```
uuidgen
```

5.ユニークなIDをCephの構成ファイルに追加します。

```
fsid = {UUID}
```

例えば、以下のように。

```
fsid = a7f64266-0894-4f1e-a635-d0aeaca0e993
```

6. Cephの構成ファイルに初期モニターを追加します。

```
mon initial members = {hostname}[,{hostname}]
```

例えば、以下のように。

```
mon initial members = node1
```

7. 初期モニターのIPアドレスをCeph構成ファイルに追加して、ファイルを保存します。

```
mon host = {ip-address}[,{ip-address}]
```

例えば、以下のように。

```
mon host = 192.168.0.1
```

**Note:** IPv4アドレスの代わりにIPv6アドレスを使用することもできますが、ms bind ipv6をtrueに設定する必要があります。ネットワーク構成の詳細については、「[Network Configuration Reference](https://docs.ceph.com/en/pacific/rados/configuration/network-config-ref)」を参照してください。

[49: Network Configuration Reference](49-Network_Configuration_Reference.md)

8. クラスタのキーリングを作成し、モニターシークレットキーを生成します。

```
sudo ceph-authtool --create-keyring /tmp/ceph.mon.keyring --gen-key -n mon. --cap mon 'allow *'
```

9. 管理者用のキーリングを生成し、client.adminユーザーを生成して、そのユーザーをキーリングに追加します。

```
sudo ceph-authtool --create-keyring /etc/ceph/ceph.client.admin.keyring --gen-key -n client.admin --cap mon 'allow *' --cap osd 'allow *' --cap mds 'allow *' --cap mgr 'allow *'
```

10. bootstrap\-osdのキーリングを生成し、client.bootstrap\-osdのユーザーを生成し、そのユーザーをキーリングに追加します。

```
sudo ceph-authtool --create-keyring /var/lib/ceph/bootstrap-osd/ceph.keyring --gen-key -n client.bootstrap-osd --cap mon 'profile bootstrap-osd' --cap mgr 'allow r'
```

11. 生成された鍵をceph.mon.keyringに追加します。

```
sudo ceph-authtool /tmp/ceph.mon.keyring --import-keyring /etc/ceph/ceph.client.admin.keyring
sudo ceph-authtool /tmp/ceph.mon.keyring --import-keyring /var/lib/ceph/bootstrap-osd/ceph.keyring
```

12. ceph.mon.keyringの所有者を変更します。

```
sudo chown ceph:ceph /tmp/ceph.mon.keyring
```

13. ホスト名、ホストのIPアドレス、FSIDを使ってモニターマップを作成する。これを/tmp/monmapとして保存します。

```
monmaptool --create --add {hostname} {ip-address} --fsid {uuid} /tmp/monmap
```

例えば、以下のように。

```
monmaptool --create --add node1 192.168.0.1 --fsid a7f64266-0894-4f1e-a635-d0aeaca0e993 /tmp/monmap
```

14. モニターホスト上にデフォルトのデータディレクトリ（複数可）を作成します。

```
sudo mkdir /var/lib/ceph/mon/{cluster-name}-{hostname}
```

例えば、以下のように。

```
sudo -u ceph mkdir /var/lib/ceph/mon/ceph-node1
```

詳細は「[Monitor Config Reference \- Data](https://docs.ceph.com/en/pacific/rados/configuration/mon-config-ref#data)」を参照してください。[52: Monitor Config Reference](52-Monitor_Config_Reference.md)

15. モニターデーモンにモニターマップとキーリングを設定する。

```
sudo -u ceph ceph-mon [--cluster {cluster-name}] --mkfs -i {hostname} --monmap /tmp/monmap --keyring /tmp/ceph.mon.keyring
```

例えば、以下のように。

```
sudo -u ceph ceph-mon --mkfs -i node1 --monmap /tmp/monmap --keyring /tmp/ceph.mon.keyring
```

16. Cephの構成ファイルの設定を検討します。一般的な設定には以下のものがあります。

```
[global]
fsid = {cluster-id}
mon initial members = {hostname}[, {hostname}]
mon host = {ip-address}[, {ip-address}]
public network = {network}[, {network}]
cluster network = {network}[, {network}]
auth cluster required = cephx
auth service required = cephx
auth client required = cephx
osd journal size = {n}
osd pool default size = {n}  # Write an object n times.
osd pool default min size = {n} # Allow writing n copies in a degraded state.
osd pool default pg num = {n}
osd pool default pgp num = {n}
osd crush chooseleaf type = {n}
```

前述の例では、コンフィギュレーションの\[global\]セクションは以下のようになります。

```
[global]
fsid = a7f64266-0894-4f1e-a635-d0aeaca0e993
mon initial members = node1
mon host = 192.168.0.1
public network = 192.168.0.0/24
auth cluster required = cephx
auth service required = cephx
auth client required = cephx
osd journal size = 1024
osd pool default size = 3
osd pool default min size = 2
osd pool default pg num = 333
osd pool default pgp num = 333
osd crush chooseleaf type = 1
```

17. Start the monitor\(s\).  

モニターを起動します。

Start the service with systemd:  

systemdでサービスを開始します。

```
sudo systemctl start ceph-mon@node1
```

18 Verify that the monitor is running.  

モニターが起動していることを確認します。

```
sudo ceph -s
```

起動したモニターが稼働していることを示す出力が表示され、配置グループが非アクティブで止まっていることを示すヘルスエラーが表示されるはずです。それは次のようなものになるはずです。

```
cluster:
  id:     a7f64266-0894-4f1e-a635-d0aeaca0e993
  health: HEALTH_OK

services:
  mon: 1 daemons, quorum node1
  mgr: node1(active)
  osd: 0 osds: 0 up, 0 in

data:
  pools:   0 pools, 0 pgs
  objects: 0 objects, 0 bytes
  usage:   0 kB used, 0 kB / 0 kB avail
  pgs:
```

**Note:** OSDを追加して起動すると、配置グループの健全性に関するエラーはなくなるはずです。詳細は、「[OSDの追�](https://docs.ceph.com/en/pacific/install/manual-deployment/#adding-osds)�」を参照してください。

## Manager daemon configuration[¶](https://docs.ceph.com/en/pacific/install/manual-deployment/#manager-daemon-configuration "Permalink to this headline")

ceph\-monデーモンを実行する各ノードでは、ceph\-mgrデーモンもセットアップする必要があります。

See [ceph\-mgr administrator’s guide](https://docs.ceph.com/en/pacific/mgr/administrator/#mgr-administrator-guide)

[293: ceph\-mgr administrator’s guide](293-ceph-mgr_administrator’s_guide.md)

## Adding OSDs[¶](https://docs.ceph.com/en/pacific/install/manual-deployment/#adding-osds "Permalink to this headline")

初期のモニターが稼働したら、OSDを追加する必要があります。オブジェクトのコピー数を処理するのに十分なOSDがないと、クラスタはアクティブ\+クリーンな状態になりません\(たとえば、osdpooldefaultsize=2の場合、少なくとも2つのOSDが必要です\)。モニターをブートストラップすると、クラスタにはデフォルトのCRUSHマップがありますが、CRUSHマップにはCeph NodeにマッピングされたCeph OSDデーモンがありません。

### Short Form[¶](https://docs.ceph.com/en/pacific/install/manual-deployment/#short-form "Permalink to this headline")

Cephには、Cephで使用するための論理ボリューム、ディスク、パーティションを準備できるceph\-volumeユーティリティが用意されています。ceph\-volumeユーティリティは、インデックスをインクリメントしてOSD IDを作成します。さらに、ceph\-volumeは新しいOSDをホスト下のCRUSHマップに追加してくれます。CLIの詳細については、ceph\-volume\-hを実行してください。ceph\-volumeユーティリティは、以下の[Long Form](https://docs.ceph.com/en/pacific/install/manual-deployment/#long-form) のステップを自動化します。ショートフォームの手順で最初の2つのOSDを作成するには、node2とnode3で次のように実行します。

#### bluestore[¶](https://docs.ceph.com/en/pacific/install/manual-deployment/#bluestore "Permalink to this headline")

1. OSDの作成。

```
ssh {node-name}
sudo ceph-volume lvm create --data {data-path}
```

For example:

```
ssh node1
sudo ceph-volume lvm create --data /dev/hdd1
```

また、作成プロセスを2つのフェーズ（準備、起動）に分けて考えることもできます。

1. OSDの準備

```
ssh {node-name}
sudo ceph-volume lvm prepare --data {data-path} {data-path}
```

For example:

```
ssh node1
sudo ceph-volume lvm prepare --data /dev/hdd1
```

用意されたOSDのIDとFSIDは、起動のために必要です。これらは、現在のサーバーのOSDをリストアップすることで入手できます。

```
sudo ceph-volume lvm list
```

1. Activate the OSD:

OSDを起動する

```
sudo ceph-volume lvm activate {ID} {FSID}
```

For example:

```
sudo ceph-volume lvm activate 0 a7f64266-0894-4f1e-a635-d0aeaca0e993
```

#### filestore[¶](https://docs.ceph.com/en/pacific/install/manual-deployment/#filestore "Permalink to this headline")

1. OSDの作成。

```
ssh {node-name}
sudo ceph-volume lvm create --filestore --data {data-path} --journal {journal-path}
```

For example:

```
ssh node1
sudo ceph-volume lvm create --filestore --data /dev/hdd1 --journal /dev/hdd2
```

また、作成プロセスを2つのフェーズ（準備、起動）に分けて考えることもできます。

1. OSDの準備

```
ssh {node-name}
sudo ceph-volume lvm prepare --filestore --data {data-path} --journal {journal-path}
```

For example:

```
ssh node1
sudo ceph-volume lvm prepare --filestore --data /dev/hdd1 --journal /dev/hdd2
```

用意されたOSDのIDとFSIDは、起動のために必要です。これらは、現在のサーバーのOSDをリストアップすることで入手できます。

```
sudo ceph-volume lvm list
```

1. OSDを起動する

```
sudo ceph-volume lvm activate --filestore {ID} {FSID}
```

For example:

```
sudo ceph-volume lvm activate --filestore 0 a7f64266-0894-4f1e-a635-d0aeaca0e993
```

### Long Form[¶](https://docs.ceph.com/en/pacific/install/manual-deployment/#long-form "Permalink to this headline")

ヘルパーユーティリティーを使用せずに、以下の手順でOSDを作成し、クラスタとCRUSHマップに追加します。長い形式の手順で最初の2つのOSDを作成するには、各OSDに対して以下の手順を実行する。

Note: 本手順では、dm\-cryptの「ロックボックス」を利用したdm\-crypt上の展開については説明しません。

OSDホストに接続し、rootになる。

```
ssh {node-name}
sudo bash
```

OSDのUUIDを生成します。

```
UUID=$(uuidgen)
```

OSD用のcephxキーを生成します。

```
OSD_SECRET=$(ceph-authtool --gen-print-key)
```

OSDを作成します。なお、以前に破壊したOSD IDを再利用する必要がある場合は、cephosdnewの追加引数としてOSD IDを指定できます。ここでは、client.bootstrap\-osdキーがマシン上に存在すると仮定します。 代わりに、このキーが存在する別のホストでclient.adminとしてこのコマンドを実行することもできます。

```
ID=$(echo "{\"cephx_secret\": \"$OSD_SECRET\"}" | \
   ceph osd new $UUID -i - \
   -n client.bootstrap-osd -k /var/lib/ceph/bootstrap-osd/ceph.keyring)
```

また、JSONにcrush\_device\_classプロパティを含めることで、デフォルト以外の初期クラス（自動検出されたデバイスタイプに基づくssdまたはhdd）を設定することも可能です。

新しいOSDにデフォルトのディレクトリを作成します。

```
mkdir /var/lib/ceph/osd/ceph-$ID
```

OSDがOSドライブ以外のドライブにある場合は、Cephで使用するために準備し、先ほど作成したディレクトリにマウントします。

```
mkfs.xfs /dev/{DEV}
mount /dev/{DEV} /var/lib/ceph/osd/ceph-$ID
```

OSDキーリングファイルにシークレットを書き込みます。

```
ceph-authtool --create-keyring /var/lib/ceph/osd/ceph-$ID/keyring \
     --name osd.$ID --add-key $OSD_SECRET
```

OSDデータディレクトリを初期化する。

```
ceph-osd -i $ID --mkfs --osd-uuid $UUID
```

所有権を修正する。

```
chown -R ceph:ceph /var/lib/ceph/osd/ceph-$ID
```

CephにOSDを追加すると、OSDが構成に入ります。ただし、まだ実行中ではありません。新しいOSDがデータの受信を開始するには、OSDを起動する必要があります。

最近のsystemdディストリビューションの場合：

```
systemctl enable ceph-osd@$ID
systemctl start ceph-osd@$ID
```

For example:

```
systemctl enable ceph-osd@12
systemctl start ceph-osd@12
```

## Adding MDS[¶](https://docs.ceph.com/en/pacific/install/manual-deployment/#adding-mds "Permalink to this headline")

以下の説明では、{id}はマシンのホスト名などの任意の名前です。

mdsデータディレクトリの作成：

```
mkdir -p /var/lib/ceph/mds/{cluster-name}-{id}
```

キーリングを作る：

```
ceph-authtool --create-keyring /var/lib/ceph/mds/{cluster-name}-{id}/keyring --gen-key -n mds.{id}
```

キーリングとキャップ・セットを取り込む：

```
ceph auth add mds.{id} osd "allow rwx" mds "allow *" mon "allow profile mds" -i /var/lib/ceph/mds/{cluster}-{id}/keyring
```

ceph.confに追加：

```
[mds.{id}]
host = {id}
```

デーモンを手動で起動する：

```
ceph-mds --cluster {cluster-name} -i {id} -m {mon-hostname}:{mon-port} [-f]
```

正しい方法でデーモンを起動（ceph.confエントリを使用）：

```
service ceph start
```

このエラーでデーモンの起動に失敗した場合：

```
mds.-1.0 ERROR: failed to authenticate: (22) Invalid argument
```

次に、ceph.confのglobalセクションにkeyringの設定がないことを確認し、clientセクションに移動するか、このmdsデーモンに固有のkeyringの設定を追加します。そして、mdsデータディレクトリとcephauthgetmds.{id}出力に同じキーが表示されていることを確認します。

これで、Cephファイルシステムを作成する準備が整いました。

[create a Ceph file system](https://docs.ceph.com/en/pacific/cephfs/createfs).

[130: Create a Ceph file system](130-Create_a_Ceph_file_system.md)

## Summary[¶](https://docs.ceph.com/en/pacific/install/manual-deployment/#summary "Permalink to this headline")

モニターと2つのOSDが起動したら、次のように実行して、配置グループのピアを見ることができます。

```
ceph -w

```

ツリーを表示するには、次のように実行します。

```
ceph osd tree

```

以下のような出力が表示されるはずです。

```
# id    weight  type name       up/down reweight
-1      2       root default
-2      2               host node1
0       1                       osd.0   up      1
-3      1               host node2
1       1                       osd.1   up      1

```

追加のモニタを追加\(または削除\)するには、「[Add/Remove Monitors](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-mons).」を参照してください。[90: Adding/Removing Monitors](90-Adding-Removing_Monitors.md)

Ceph OSDデーモンを追加\(または削除\)するには、「[Add/Remove OSDs](https://docs.ceph.com/en/pacific/rados/operations/add-or-rm-osds)」を参照してください。[89: Adding/Removing OSDs](89-Adding-Removing_OSDs.md)
