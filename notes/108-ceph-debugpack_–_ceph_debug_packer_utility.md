# 108: ceph-debugpack – ceph debug packer utility

 # ceph\-debugpack – ceph debug packer utility[¶](https://docs.ceph.com/en/pacific/man/8/ceph-debugpack/ "Permalink to this headline")

## Synopsis[¶](https://docs.ceph.com/en/pacific/man/8/ceph-debugpack/ "Permalink to this headline")

**ceph\-debugpack** \[ _options_ \] _filename.tar.gz_

## Description[¶](https://docs.ceph.com/en/pacific/man/8/ceph-debugpack/ "Permalink to this headline")

**ceph\-debugpack** will build a tarball containing various items that are useful for debugging crashes. The resulting tarball can be shared with Ceph developers when debugging a problem.

The tarball will include the binaries for ceph\-mds, ceph\-osd, and ceph\-mon, radosgw, any log files, the ceph.conf configuration file, any core files we can find, and \(if the system is running\) dumps of the current cluster state as reported by ‘ceph report’.

## Options[¶](https://docs.ceph.com/en/pacific/man/8/ceph-debugpack/ "Permalink to this headline")

`-c`` ceph.conf``, ``--conf``=ceph.conf`[¶](https://docs.ceph.com/en/pacific/man/8/ceph-debugpack/ "Permalink to this definition")Use _ceph.conf_ configuration file instead of the default`/etc/ceph/ceph.conf` to determine monitor addresses during startup.

## Availability[¶](https://docs.ceph.com/en/pacific/man/8/ceph-debugpack/ "Permalink to this headline")

**ceph\-debugpack** is part of Ceph, a massively scalable, open\-source, distributed storage system. Please refer to the Ceph documentation at [http://ceph.com/docs](https://docs.ceph.com/en/pacific/man/8/ceph-debugpack/) for more information.

## See also[¶](https://docs.ceph.com/en/pacific/man/8/ceph-debugpack/ "Permalink to this headline")

[ceph](https://docs.ceph.com/en/pacific/man/8/ceph-debugpack/)\(8\)[ceph\-post\-file](https://docs.ceph.com/en/pacific/man/8/ceph-debugpack/)\(8\)
