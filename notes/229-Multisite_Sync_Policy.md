# 229: Multisite Sync Policy

**クリップソース:** [229: Multisite Sync Policy — Ceph Documentation](https://docs.ceph.com/en/pacific/radosgw/multisite-sync-policy/)

# Multisite Sync Policy[¶](https://docs.ceph.com/en/pacific/radosgw/multisite-sync-policy/ "Permalink to this headline")

Octopusバージョンの新機能です。

マルチサイトバケット粒度同期ポリシーは、異なるゾーンのバケット間のデータ移動をきめ細かく制御します。 ゾーン同期メカニズムを拡張します。 以前は、バケットは対称的に扱われていました。つまり、各（データ）ゾーンは、他のすべてのゾーンと同じである必要があるそのバケットのミラーを保持します。 一方、バケット粒度同期ポリシーを利用すると、バケットが分岐する可能性があり、バケットは、別のゾーンにある他のバケット（名前やIDを共有しないバケット）からデータをプルできます。 したがって、同期プロセスでは、バケット同期の送信元とバケット同期の宛先が常に同じバケットを参照していると想定していましたが、現在はそうではありません。

同期ポリシーは、古いゾーングループの粗い設定（sync\_from \*）に優先します。 同期ポリシーは、ゾーングループレベルで設定できます（設定されている場合は、古いスタイルの設定を置き換えます）が、バケットレベルで設定することもできます。

同期ポリシーでは、データフロー設定のリストと、パイプ設定のリストを含む複数のグループを定義することができます。データフローは、異なるゾーン間のデータフローを定義します。複数のゾーンが互いにデータを同期する対称的（symmetrical）なデータフローを定義することも、データがあるゾーンから別のゾーンに一方的に移動する方向性のある（directional）データフローを定義することもできます。

パイプは、これらのデータフローを使用できる実際のバケットと、それに関連するプロパティ（たとえば、ソースオブジェクトのプレフィックス）を定義します。

同期ポリシーグループは、3つの状態になることができます：

|Value    |Description                                                                               |
|---------|------------------------------------------------------------------------------------------|
|enabled  |同期が許可され、有効になっている                                                          |
|allowed  |同期が許可されている                                                                      |
|forbidden|（このグループで定義されている）同期は許可されておらず、他のグループをオーバーライドできる|

ポリシーはバケットレベルで定義できます。 バケットレベルの同期ポリシーは、ゾーングループポリシーのデータフローを継承し、ゾーングループが許可するもののサブセットのみを定義できます。

ポリシー内のワイルドカードゾーンとワイルドカードバケットパラメータは、 関連するすべてのゾーンと関連するすべてのバケットを定義します。バケットポリシーの文脈では、それは現在のバケットインスタンスを意味します。 ゾーン全体をミラーリングする災害復旧構成では、バケットに何も設定する必要はありません。しかし、きめ細かいバケット同期を行うには、ゾーングループレベルでは（例えばワイルドカードを使用して）同期するパイプを許可（status=allowed）し、バケットレベルでは特定の同期のみを有効（status=enabled）とすることを推奨します。必要に応じて、バケットレベルのポリシーで、データの移動を特定の関連ゾーンに制限することができます。

重要：ゾーングループポリシーの変更は、ゾーングループのマスターゾーンに適用する必要があり、period更新とコミットが必要です。バケットポリシーの変更は、ゾーングループのマスターゾーンに適用する必要があります。この変更は、rgw によって動的に処理されます。

## S3 Replication API[¶](https://docs.ceph.com/en/pacific/radosgw/multisite-sync-policy/ "Permalink to this headline")

また、S3バケットのレプリケーションapiも実装され、異なるバケット間のレプリケーションルールを作成することができるようになりました。ただし、AWSのレプリケーション機能では、同一ゾーン内でのバケットレプリケーションが可能ですが、rgwでは現時点ではできませんのでご注意ください。 しかし、rgw api には新しい 'Zone' 配列が追加されており、特定のバケットをどのゾーンに同期させるかを選択することができます。

### Sync Policy Control Reference[¶](https://docs.ceph.com/en/pacific/radosgw/multisite-sync-policy/ "Permalink to this headline")

## Get Sync Policy[¶](https://docs.ceph.com/en/pacific/radosgw/multisite-sync-policy/ "Permalink to this headline")

現在のゾーングループ同期ポリシー、または特定のバケットポリシーを取得するには：

```
# radosgw-admin sync policy get [--bucket=<bucket>]
```

## Create Sync Policy Group[¶](https://docs.ceph.com/en/pacific/radosgw/multisite-sync-policy/ "Permalink to this headline")

同期ポリシーグループを作成するには：

```
# radosgw-admin sync group create [--bucket=<bucket>]                      
                                  --group-id=<group-id>                    
                                  --status=<enabled | allowed | forbidden> 
```

## Modify Sync Policy Group[¶](https://docs.ceph.com/en/pacific/radosgw/multisite-sync-policy/ "Permalink to this headline")

同期ポリシーグループを変更するには：

```
# radosgw-admin sync group modify [--bucket=<bucket>]                      
                                  --group-id=<group-id>                    
                                  --status=<enabled | allowed | forbidden> 
```

## Show Sync Policy Group[¶](https://docs.ceph.com/en/pacific/radosgw/multisite-sync-policy/ "Permalink to this headline")

同期ポリシーグループを表示するには：

```
# radosgw-admin sync group get [--bucket=<bucket>]       
                                  --group-id=<group-id>
```

## Remove Sync Policy Group[¶](https://docs.ceph.com/en/pacific/radosgw/multisite-sync-policy/ "Permalink to this headline")

同期ポリシーグループを削除するには：

```
# radosgw-admin sync group remove [--bucket=<bucket>]    
                                  --group-id=<group-id>
```

## Create Sync Flow[¶](https://docs.ceph.com/en/pacific/radosgw/multisite-sync-policy/ "Permalink to this headline")

* 方向性のある（directional）同期フローを作成または更新するには：

```
# radosgw-admin sync group flow create [--bucket=<bucket>]          
                                       --group-id=<group-id>        
                                       --flow-id=<flow-id>          
                                       --flow-type=directional      
                                       --source-zone=<source_zone>  
                                       --dest-zone=<dest_zone>
```

* 対称的な（symmetrical）同期フローを作成または更新するには：

```
# radosgw-admin sync group flow create [--bucket=<bucket>]          
                                       --group-id=<group-id>        
                                       --flow-id=<flow-id>          
                                       --flow-type=symmetrical      
                                       --zones=<zones>
```

zonesは、フローに追加する必要があるすべてのゾーンをカンマで区切ったリストです。

## Remove Sync Flow Zones[¶](https://docs.ceph.com/en/pacific/radosgw/multisite-sync-policy/ "Permalink to this headline")

* 方向性のある同期フローを削除するには：

```
# radosgw-admin sync group flow remove [--bucket=<bucket>]          
                                       --group-id=<group-id>        
                                       --flow-id=<flow-id>          
                                       --flow-type=directional      
                                       --source-zone=<source_zone>  
                                       --dest-zone=<dest_zone>
```

* 特定のゾーンを対称的な同期フローの対象から外すには：

```
# radosgw-admin sync group flow remove [--bucket=<bucket>]          
                                       --group-id=<group-id>        
                                       --flow-id=<flow-id>          
                                       --flow-type=symmetrical      
                                       --zones=<zones>
```

zonesは、フローから削除するすべてのゾーンをカンマで区切ったリストです。

* 対称的な同期フローを削除するには：

```
# radosgw-admin sync group flow remove [--bucket=<bucket>]          
                                       --group-id=<group-id>        
                                       --flow-id=<flow-id>          
                                       --flow-type=symmetrical
```

## Create Sync Pipe[¶](https://docs.ceph.com/en/pacific/radosgw/multisite-sync-policy/ "Permalink to this headline")

同期グループ・パイプを作成したり、そのパラメーターを更新したりするには：

```
# radosgw-admin sync group pipe create [--bucket=<bucket>]                      
                                       --group-id=<group-id>                    
                                       --pipe-id=<pipe-id>                      
                                       --source-zones=<source_zones>            
                                       [--source-bucket=<source_buckets>]       
                                       [--source-bucket-id=<source_bucket_id>]  
                                       --dest-zones=<dest_zones>                
                                       [--dest-bucket=<dest_buckets>]           
                                       [--dest-bucket-id=<dest_bucket_id>]      
                                       [--prefix=<source_prefix>]               
                                       [--prefix-rm]                            
                                       [--tags-add=<tags>]                      
                                       [--tags-rm=<tags>]                       
                                       [--dest-owner=<owner>]                   
                                       [--storage-class=<storage_class>]        
                                       [--mode=<system | user>]                 
                                       [--uid=<user_id>]
```

ゾーンは、ゾーンのリストまたは「\*」（ワイルドカード）のいずれかです。 ワイルドカードゾーンとは、同期フロールールに一致するゾーンを意味します。 バケットは、バケット名または「\*」（ワイルドカード）のいずれかです。 ワイルドカードバケットは、カレントバケットプレフィックスを定義してソースオブジェクトをフィルタリングできることを意味します。 タグは、「key = value」のコンマ区切りリストによって渡されます。 宛先所有者は、オブジェクトの宛先所有者を強制するように設定できます。 userモードが選択されている場合、宛先バケットの所有者のみを設定できます。 宛先ストレージクラスも構成できます。 user idは、userモードに設定でき、同期操作が実行されるユーザーになります（権限の検証用）。

## Remove Sync Pipe[¶](https://docs.ceph.com/en/pacific/radosgw/multisite-sync-policy/ "Permalink to this headline")

特定の同期グループパイプパラメータ、またはパイプ全体を削除するには：

```
# radosgw-admin sync group pipe remove [--bucket=<bucket>]                     
                                       --group-id=<group-id>                   
                                       --pipe-id=<pipe-id>                     
                                       [--source-zones=<source_zones>]         
                                       [--source-bucket=<source_buckets>]      
                                       [--source-bucket-id=<source_bucket_id>] 
                                       [--dest-zones=<dest_zones>]             
                                       [--dest-bucket=<dest_buckets>]          
                                       [--dest-bucket-id=<dest_bucket_id>]
```

## Sync Info[¶](https://docs.ceph.com/en/pacific/radosgw/multisite-sync-policy/ "Permalink to this headline")

（同期ポリシーで定義した）同期ソースとターゲットに関する情報を取得するには：

```
# radosgw-admin sync info [--bucket=<bucket>]             
                          [--effective-zone-name=<zone>]
```

バケットは、バケットから別のゾーンの別のバケットへのデータ移動を定義するポリシーを定義できるため、ポリシーが作成されると、特定のバケットの同期が発生したときにヒントとして使用されるバケットの依存関係のリストも生成されます。 バケットが別のバケットを参照していても、データフローで許可されていない可能性があるため、実際に当該バケットと同期していることを意味するわけではありません。

### Examples[¶](https://docs.ceph.com/en/pacific/radosgw/multisite-sync-policy/ "Permalink to this headline")

この例では、us\-east（マスターゾーン）、us\-west、us\-west\-2の3つのゾーンを含んでいます。

## Example 1: Two Zones, Complete Mirror[¶](https://docs.ceph.com/en/pacific/radosgw/multisite-sync-policy/ "Permalink to this headline")

これは、旧来の（Octopus以前の）同期機能に似ていますが、新しい同期ポリシーエンジンを介して行われます。ゾーングループ同期ポリシーの変更には、period更新とコミットが必要であることに注意してください。

```
[us-east] $ radosgw-admin sync group create --group-id=group1 --status=allowed
[us-east] $ radosgw-admin sync group flow create --group-id=group1 
                          --flow-id=flow-mirror --flow-type=symmetrical 
                          --zones=us-east,us-west
[us-east] $ radosgw-admin sync group pipe create --group-id=group1 
                          --pipe-id=pipe1 --source-zones='*' 
                          --source-bucket='*' --dest-zones='*' 
                          --dest-bucket='*'
[us-east] $ radosgw-admin sync group modify --group-id=group1 --status=enabled
[us-east] $ radosgw-admin period update --commit

$ radosgw-admin sync info --bucket=buck
{
    "sources": [
        {
            "id": "pipe1",
            "source": {
                "zone": "us-west",
                "bucket": "buck:115b12b3-....4409.1"
            },
            "dest": {
                "zone": "us-east",
                "bucket": "buck:115b12b3-....4409.1"
            },
            "params": {
...
            }
        }
    ],
    "dests": [
        {
            "id": "pipe1",
            "source": {
                "zone": "us-east",
                "bucket": "buck:115b12b3-....4409.1"
            },
            "dest": {
                "zone": "us-west",
                "bucket": "buck:115b12b3-....4409.1"
            },
           ...
        }
    ],
    ...
    }
}
```

上の出力の "id "フィールドは、そのエントリーを生成したパイプルールを反映していることに注意してください。この例に見られるように、1つのルールが複数の同期エントリーを生成することもあります。

```
[us-west] $ radosgw-admin sync info --bucket=buck
{
    "sources": [
        {
            "id": "pipe1",
            "source": {
                "zone": "us-east",
                "bucket": "buck:115b12b3-....4409.1"
            },
            "dest": {
                "zone": "us-west",
                "bucket": "buck:115b12b3-....4409.1"
            },
            ...
        }
    ],
    "dests": [
        {
            "id": "pipe1",
            "source": {
                "zone": "us-west",
                "bucket": "buck:115b12b3-....4409.1"
            },
            "dest": {
                "zone": "us-east",
                "bucket": "buck:115b12b3-....4409.1"
            },
           ...
        }
    ],
    ...
}
```

## Example 2: Directional, Entire Zone Backup[¶](https://docs.ceph.com/en/pacific/radosgw/multisite-sync-policy/ "Permalink to this headline")

これも、旧来の同期機能に似ています。ここでは、us\-west\-2という第3のゾーンを追加しており、これはus\-westのレプリカになりますが、そこからデータがレプリケートバックされることはありません。

```
[us-east] $ radosgw-admin sync group flow create --group-id=group1 
                          --flow-id=us-west-backup --flow-type=directional 
                          --source-zone=us-west --dest-zone=us-west-2
[us-east] $ radosgw-admin period update --commit
```

なお、us\-westには2つのdestがあります：

```
[us-west] $ radosgw-admin sync info --bucket=buck
{
    "sources": [
        {
            "id": "pipe1",
            "source": {
                "zone": "us-east",
                "bucket": "buck:115b12b3-....4409.1"
            },
            "dest": {
                "zone": "us-west",
                "bucket": "buck:115b12b3-....4409.1"
            },
           ...
        }
    ],
    "dests": [
        {
            "id": "pipe1",
            "source": {
                "zone": "us-west",
                "bucket": "buck:115b12b3-....4409.1"
            },
            "dest": {
                "zone": "us-east",
                "bucket": "buck:115b12b3-....4409.1"
            },
           ...
        },
        {
            "id": "pipe1",
            "source": {
                "zone": "us-west",
                "bucket": "buck:115b12b3-....4409.1"
            },
            "dest": {
                "zone": "us-west-2",
                "bucket": "buck:115b12b3-....4409.1"
            },
           ...
        }
    ],
    ...
}
```

一方、us\-west\-2は送信元のみで、送信先がありません。

```
[us-west-2] $ radosgw-admin sync info --bucket=buck
{
    "sources": [
        {
            "id": "pipe1",
            "source": {
                "zone": "us-west",
                "bucket": "buck:115b12b3-....4409.1"
            },
            "dest": {
                "zone": "us-west-2",
                "bucket": "buck:115b12b3-....4409.1"
            },
           ...
        }
    ],
    "dests": [],
    ...
}

```

## Example 3: Mirror a Specific Bucket[¶](https://docs.ceph.com/en/pacific/radosgw/multisite-sync-policy/ "Permalink to this headline")

同じグループ構成を使用し、今回はallowed状態に切り替えます。これは、同期が許可（allowed）されているが、有効（enabled）にはなっていないことを意味します。

```
[us-east] $ radosgw-admin sync group modify --group-id=group1 --status=allowed
[us-east] $ radosgw-admin period update --commit
```

そして、既存のバケットbuck2に対して、バケットレベルのポリシールールを作成することにします。なお、このポリシーを設定する前にバケットが存在する必要があり、バケットポリシーを変更する管理コマンドはマスターゾーン上で実行する必要がありますが、period更新は必要ありません。 データフローはゾーングループポリシーから継承されるため、変更する必要はありません。バケットポリシーフローは、ゾーングループポリシーで定義されたフローのサブセットとなるだけです。パイプについても同様ですが、バケットポリシーは、ゾーングループポリシーで有効になっていない（禁止されていないとはいえ）パイプを有効にすることができます。

```
[us-east] $ radosgw-admin sync group create --bucket=buck2 
                          --group-id=buck2-default --status=enabled

[us-east] $ radosgw-admin sync group pipe create --bucket=buck2 
                          --group-id=buck2-default --pipe-id=pipe1 
                          --source-zones='*' --dest-zones='*'
```

## Example 4: Limit Bucket Sync To Specific Zones[¶](https://docs.ceph.com/en/pacific/radosgw/multisite-sync-policy/ "Permalink to this headline")

これは、buck3 を（フローが us\-east への同期を許可しているゾーンから） us\-east にのみ同期させます。

```
[us-east] $ radosgw-admin sync group create --bucket=buck3 
                          --group-id=buck3-default --status=enabled

[us-east] $ radosgw-admin sync group pipe create --bucket=buck3 
                          --group-id=buck3-default --pipe-id=pipe1 
                          --source-zones='*' --dest-zones=us-east
```

## Example 5: Sync From a Different Bucket[¶](https://docs.ceph.com/en/pacific/radosgw/multisite-sync-policy/ "Permalink to this headline")

なお、バケット同期は（現在のところ）ゾーン間のみで動作し、同じゾーン内では動作しません。

Buck4 が Buck5 からデータを取り込むように設定します：

```
[us-east] $ radosgw-admin sync group create --bucket=buck4 '
                          --group-id=buck4-default --status=enabled

[us-east] $ radosgw-admin sync group pipe create --bucket=buck4 
                          --group-id=buck4-default --pipe-id=pipe1 
                          --source-zones='*' --source-bucket=buck5 
                          --dest-zones='*'
```

特定のゾーンに制限することもできます。例えば、次のようにすると、us\-westで発生したデータのみが同期されます：

```
[us-east] $ radosgw-admin sync group pipe modify --bucket=buck4 
                          --group-id=buck4-default --pipe-id=pipe1 
                          --source-zones=us-west --source-bucket=buck5 
                          --dest-zones='*'
```

us\-westでbuck5の同期情報を確認します：

```
[us-west] $ radosgw-admin sync info --bucket=buck5
{
    "sources": [],
    "dests": [],
    "hints": {
        "sources": [],
        "dests": [
            "buck4:115b12b3-....14433.2"
        ]
    },
    "resolved-hints-1": {
        "sources": [],
        "dests": [
            {
                "id": "pipe1",
                "source": {
                    "zone": "us-west",
                    "bucket": "buck5"
                },
                "dest": {
                    "zone": "us-east",
                    "bucket": "buck4:115b12b3-....14433.2"
                },
                ...
            },
            {
                "id": "pipe1",
                "source": {
                    "zone": "us-west",
                    "bucket": "buck5"
                },
                "dest": {
                    "zone": "us-west-2",
                    "bucket": "buck4:115b12b3-....14433.2"
                },
                ...
            }
        ]
    },
    "resolved-hints": {
        "sources": [],
        "dests": []
    }
}
```

解決済みのヒント（resolved hints）があることに注意してください。つまり、バケット buck5 が、自身のポリシー（buck5 のポリシーは空）からではなく、間接的に buck4 の同期について知ったことを意味します。

## Example 6: Sync To Different Bucket[¶](https://docs.ceph.com/en/pacific/radosgw/multisite-sync-policy/ "Permalink to this headline")

同じメカニズムで、（前の例のように同期元ではなく）同期先のデータを設定することができます。内部的には、依然として宛先ゾーンにあるソースからデータが引き出されることに注意してください。

Buck6 が Buck5 にデータを "Push "するように設定します：

```
[us-east] $ radosgw-admin sync group create --bucket=buck6 
                          --group-id=buck6-default --status=enabled

[us-east] $ radosgw-admin sync group pipe create --bucket=buck6 
                          --group-id=buck6-default --pipe-id=pipe1 
                          --source-zones='*' --source-bucket='*' 
                          --dest-zones='*' --dest-bucket=buck5
```

ワイルドカードのバケット名は、バケット同期ポリシーの文脈ではカレントバケットを意味します。

例5の設定と合わせると、us\-eastのbuck6にデータを書き込むと、us\-westのbuck5にデータが同期し、そこからus\-eastのbuck4とus\-west\-2にデータが分散されるようになります。

## Example 7: Source Filters[¶](https://docs.ceph.com/en/pacific/radosgw/multisite-sync-policy/ "Permalink to this headline")

buck8 から buck9 に同期させますが、foo/ で始まるオブジェクトのみ同期させます：

```
[us-east] $ radosgw-admin sync group create --bucket=buck8 
                          --group-id=buck8-default --status=enabled

[us-east] $ radosgw-admin sync group pipe create --bucket=buck8 
                          --group-id=buck8-default --pipe-id=pipe-prefix 
                          --prefix=foo/ --source-zones='*' --dest-zones='*' 
                          --dest-bucket=buck9
```

また、color=blue または color=red のタグを持つオブジェクトを buck8 から buck9 へ同期させます：

```
[us-east] $ radosgw-admin sync group pipe create --bucket=buck8 
                          --group-id=buck8-default --pipe-id=pipe-tags 
                          --tags-add=color=blue,color=red --source-zones='*' 
                          --dest-zones='*' --dest-bucket=buck9
```

そして、（例えば）us\-eastで予想される同期を確認することができます：

```
[us-east] $ radosgw-admin sync info --bucket=buck8
{
    "sources": [],
    "dests": [
        {
            "id": "pipe-prefix",
            "source": {
                "zone": "us-east",
                "bucket": "buck8:115b12b3-....14433.5"
            },
            "dest": {
                "zone": "us-west",
                "bucket": "buck9"
            },
            "params": {
                "source": {
                    "filter": {
                        "prefix": "foo/",
                        "tags": []
                    }
                },
                ...
            }
        },
        {
            "id": "pipe-tags",
            "source": {
                "zone": "us-east",
                "bucket": "buck8:115b12b3-....14433.5"
            },
            "dest": {
                "zone": "us-west",
                "bucket": "buck9"
            },
            "params": {
                "source": {
                    "filter": {
                        "tags": [
                            {
                                "key": "color",
                                "value": "blue"
                            },
                            {
                                "key": "color",
                                "value": "red"
                            }
                        ]
                    }
                },
                ...
            }
        }
    ],
    ...
}
```

ソースはなく、（各構成に1つずつ）2つの異なるデスティネーションだけであることに注意してください。同期処理は、同期する各オブジェクトに関連するルールを選択します。

prefixとtagsは組み合わせることができ、オブジェクトは同期するために両方を持つ必要があります。priorityパラメータも渡すことができ、（ソースと宛先が同じで）複数の異なるルールにマッチする場合、どのルールを使用するかの決定に使用することができます。

## Example 8: Destination Params: Storage Class[¶](https://docs.ceph.com/en/pacific/radosgw/multisite-sync-policy/ "Permalink to this headline")

宛先オブジェクトのストレージクラスを設定することができます：

```
[us-east] $ radosgw-admin sync group create --bucket=buck10 
                          --group-id=buck10-default --status=enabled

[us-east] $ radosgw-admin sync group pipe create --bucket=buck10 
                          --group-id=buck10-default 
                          --pipe-id=pipe-storage-class 
                          --source-zones='*' --dest-zones=us-west-2 
                          --storage-class=CHEAP_AND_SLOW
```

## Example 9: Destination Params: Destination Owner Translation[¶](https://docs.ceph.com/en/pacific/radosgw/multisite-sync-policy/ "Permalink to this headline")

宛先バケットの所有者に、宛先オブジェクトの所有者を設定します。これには、宛先バケットのuidを指定する必要があります：

```
[us-east] $ radosgw-admin sync group create --bucket=buck11 
                          --group-id=buck11-default --status=enabled

[us-east] $ radosgw-admin sync group pipe create --bucket=buck11 
                          --group-id=buck11-default --pipe-id=pipe-dest-owner 
                          --source-zones='*' --dest-zones='*' 
                          --dest-bucket=buck12 --dest-owner=joe
```

## Example 10: Destination Params: User Mode[¶](https://docs.ceph.com/en/pacific/radosgw/multisite-sync-policy/ "Permalink to this headline")

userモードでは、オブジェクトの読み込みと、宛先バケットへの書き込みの両方のパーミッションが当該ユーザにあることを確認します。そのためには、\(操作が実行されるコンテクスト内の\)当該ユーザの uid を指定する必要があります。

```
[us-east] $ radosgw-admin sync group pipe modify --bucket=buck11 
                          --group-id=buck11-default --pipe-id=pipe-dest-owner 
                          --mode=user --uid=jenny
```
