# 348: Deploying a development cluster — Ceph Documentation

   [Report a Documentation Bug](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/) 
 # Deploying a development cluster[¶](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/ "Permalink to this headline")

In order to develop on ceph, a Ceph utility,_vstart.sh_, allows you to deploy fake local cluster for development purpose.

## Usage[¶](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/ "Permalink to this headline")

It allows to deploy a fake local cluster on your machine for development purpose. It starts rgw, mon, osd and/or mds, or all of them if not specified.

To start your development cluster, type the following:

```
vstart.sh [OPTIONS]...
```

In order to stop the cluster, you can type:

```
./stop.sh
```

## Options[¶](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/ "Permalink to this headline")

`-b````, ``--bluestore```[¶](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/ "Permalink to this definition")Use bluestore as the objectstore backend for osds.

`--cache`` <pool>`[¶](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/ "Permalink to this definition")Set a cache\-tier for the specified pool.

`-d````, ``--debug```[¶](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/ "Permalink to this definition")Launch in debug mode.

`-e```[¶](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/ "Permalink to this definition")Create an erasure pool.

`-f````, ``--filestore```[¶](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/ "Permalink to this definition")Use filestore as the osd objectstore backend.

`--hitset`` <pool> <hit_set_type>`[¶](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/ "Permalink to this definition")Enable hitset tracking.

`-i`` ip_address`[¶](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/ "Permalink to this definition")Bind to the specified _ip\_address_ instead of guessing and resolve from hostname.

`-k```[¶](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/ "Permalink to this definition")Keep old configuration files instead of overwritting theses.

`-K````, ``--kstore```[¶](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/ "Permalink to this definition")Use kstore as the osd objectstore backend.

`-l````, ``--localhost```[¶](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/ "Permalink to this definition")Use localhost instead of hostname.

`-m`` ip[:port]`[¶](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/ "Permalink to this definition")Specifies monitor _ip_ address and _port_.

`--memstore```[¶](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/ "Permalink to this definition")Use memstore as the objectstore backend for osds

`--multimds`` <count>`[¶](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/ "Permalink to this definition")Allow multimds with maximum active count.

`-n````, ``--new```[¶](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/ "Permalink to this definition")Create a new cluster.

`-N````, ``--not-new```[¶](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/ "Permalink to this definition")Reuse existing cluster config \(default\).

`--nodaemon```[¶](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/ "Permalink to this definition")Use ceph\-run as wrapper for mon/osd/mds.

`--nolockdep```[¶](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/ "Permalink to this definition")Disable lockdep

`-o`` <config>`[¶](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/ "Permalink to this definition")Add _config_ to all sections in the ceph configuration.

`--rgw_port`` <port>`[¶](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/ "Permalink to this definition")Specify ceph rgw http listen port.

`--rgw_frontend`` <frontend>`[¶](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/ "Permalink to this definition")Specify the rgw frontend configuration \(default is civetweb\).

`--rgw_compression`` <compression_type>`[¶](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/ "Permalink to this definition")Specify the rgw compression plugin \(default is disabled\).

`--smallmds```[¶](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/ "Permalink to this definition")Configure mds with small limit cache size.

`--short```[¶](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/ "Permalink to this definition")Short object names only; necessary for ext4 dev

`--valgrind[_{osd,mds,mon}]`` 'valgrind_toolname [args...]'`[¶](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/ "Permalink to this definition")Launch the osd/mds/mon/all the ceph binaries using valgrind with the specified tool and arguments.

`--without-dashboard```[¶](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/ "Permalink to this definition")Do not run using mgr dashboard.

`-x```[¶](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/ "Permalink to this definition")Enable cephx \(on by default\).

`-X```[¶](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/ "Permalink to this definition")Disable cephx.

## Environment variables[¶](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/ "Permalink to this headline")

{OSD,MDS,MON,RGW}

Theses environment variables will contains the number of instances of the desired ceph process you want to start.

Example:

```
OSD=3 MON=3 RGW=1 vstart.sh
```

# Deploying multiple development clusters on the same machine[¶](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/ "Permalink to this headline")

In order to bring up multiple ceph clusters on the same machine, _mstart.sh_ a small wrapper around the above _vstart_ can help.

## Usage[¶](https://docs.ceph.com/en/pacific/dev/dev_cluster_deployement/ "Permalink to this headline")

To start multiple clusters, you would run mstart for each cluster you would want to deploy, and it will start monitors, rgws for each cluster on different ports allowing you to run multiple mons, rgws etc. on the same cluster. Invoke it in the following way:

```
mstart.sh <cluster-name> <vstart options>
```

For eg:

```
./mstart.sh cluster1 -n
```

For stopping the cluster, you do:

```
./mstop.sh <cluster-name>
```

 
 
