# 146: CephFS Snapshot Mirroring

**クリップソース:** [146: CephFS Snapshot Mirroring — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/cephfs-mirroring/)

# CephFS Snapshot Mirroring[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-mirroring/ "Permalink to this headline")

CephFSは、cephfs\-mirrorツールを介して、リモートCephFSファイルシステムへのスナップショットの非同期レプリケーションをサポートしています。スナップショットは、スナップショットデータをミラーリングした後、（リモートファイルシステム上の指定されたディレクトリに）同期元スナップショットと同じ名前のスナップショットを作成することで同期されます。

## Requirements[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-mirroring/ "Permalink to this headline")

The primary \(local\) and secondary \(remote\) Ceph clusters version should be Pacific or later.

## Creating Users[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-mirroring/ "Permalink to this headline")

まず、プライマリ/ローカルクラスタ上にミラーデーモン用のユーザを作成します。このユーザは、watch/notify操作のためのRADOSオブジェクト（indexオブジェクト）を作成するため、メタデータプールに対する書き込みCAPSと、データプールに対する読み取りCAPSを必要とします。

```
$ ceph auth get-or-create client.mirror mon 'profile cephfs-mirror' mds 'allow r' osd 'allow rw tag cephfs metadata=*, allow r tag cephfs data=*' mgr 'allow r'
```

セカンダリ/リモート・クラスタ上の各ファイルシステム・ピアにユーザを作成します。このユーザーは、スナップショットを取得するため、MDSとOSD上でフルCAPSを持つ必要があります。

```
$ ceph fs authorize <fs_name> client.mirror_remote / rwps
```

このユーザは、ピアを追加する際のピア指定に必要です。

## Starting Mirror Daemon[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-mirroring/ "Permalink to this headline")

ミラーデーモンは、systemctl\(1\) のユニットファイルを使用して起動する必要があります。

```
$ systemctl enable cephfs-mirror@mirror
$ systemctl start cephfs-mirror@mirror
```

cephfs\-mirror デーモンをフォアグラウンドで実行することができます：

```
$ cephfs-mirror --id mirror --cluster site-a -f
```

注：ここで使用するユーザは、上記で作成したmirrorです。

## Interface[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-mirroring/ "Permalink to this headline")

ミラーリングモジュール（マネージャプラグイン）は、ディレクトリスナップショットのミラーリングを管理するためのインターフェースを提供します。Manager インターフェイスは、ファイルシステムのミラーリングを管理するためのモニタコマンドのラッパーであり、推奨される制御インターフェイスです。

## Mirroring Module[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-mirroring/ "Permalink to this headline")

ミラーリングモジュールは、同期をとるためにミラーデーモンにディレクトリを割り当てる役割を担っています。ディレクトリスナップショットの同期を並列に行うために、複数のミラーデーモンを起動することができます。ミラーデーモンが生成（または終了）すると、ミラーリングモジュールは変更されたミラーデーモンセットを検出し、セット間のディレクトリ割り当てをリバランスし、HAを提供します。

注：複数ミラーデーモンは現在未検証です。単一ミラーデーモンのみを推奨します。

ミラーリングモジュールはデフォルトで無効になっています。ミラーリングを有効にするには：

```
$ ceph mgr module enable mirroring
```

Mirroringモジュールは、ディレクトリスナップショットのミラーリングを制御するためのコマンド群を提供します。ディレクトリを追加または削除するには、指定されたファイルシステムでミラーリングを有効にする必要があります。ミラーリングを有効にするには：

```
$ ceph fs snapshot mirror enable <fs_name>
```

注：ミラーリングモジュールコマンドは、fs snapshot mirror prefix を使用しますが、モニターコマンドは、fs mirror prefix を使用します。必ずモジュールコマンドを使用してください。

ミラーリングを無効にするには：

```
$ ceph fs snapshot mirror disable <fs_name>
```

ミラーリングを有効にしたら、ディレクトリスナップショットのミラーリング先となるピアを追加します。ピアは\<client\>@\<cluster\>の指定に従い、追加時に一意なID（UUID）が割り当てられます。ミラーリング用のCephユーザを作成する方法については、「Creating Users」セクションを参照してください。

ピアを追加するには：

```
$ ceph fs snapshot mirror peer_add <fs_name> <remote_cluster_spec> [<remote_fs_name>] [<remote_mon_host>] [<cephx_key>]
```

\<remote\_fs\_name\> はオプションで、デフォルトは（リモートクラスタ上の） \<fs\_name\> です。

このため、リモートクラスタのceph設定とユーザキーリングがプライマリクラスタで利用可能であることが必要です。これを回避するには、「Bootstrap Peers」セクションを参照してください。peer\_addでリモートクラスタのモニターアドレスとユーザーキーの受け渡しをサポートしますが、bootstrapping a peerが、ピアを追加する推奨方法です。

注：現在、1つのピアのみサポートされています。

ピアを削除するには：

```
$ ceph fs snapshot mirror peer_remove <fs_name> <peer_uuid>
```

ファイルシステムのミラーピアの一覧を表示するには：

```
$ ceph fs snapshot mirror peer_list <fs_name>
```

ミラーリングのためのディレクトリを設定するには：

```
$ ceph fs snapshot mirror add <fs_name> <path>
```

To stop a mirroring directory snapshots use:

```
$ ceph fs snapshot mirror remove <fs_name> <path>
```

ディレクトリpathは絶対パスのみ許可されます。また、pathはミラーリングモジュールによって正規化されるため、/a/b/.../bは/a/bと等価です。

```
$ mkdir -p /d0/d1/d2 
$ ceph fs snapshot mirror add cephfs /d0/d1/d2 {} 
$ ceph fs snapshot mirror add cephfs /d0/d1/../d1/d2 
Error EEXIST: directory /d0/d1/d2 is already tracked
```

一度ミラーリングに追加したディレクトリは、サブディレクトリや先祖ディレクトリをミラーリングに追加できません。

```
$ ceph fs snapshot mirror add cephfs /d0/d1
Error EINVAL: /d0/d1 is a ancestor of tracked path /d0/d1/d2
$ ceph fs snapshot mirror add cephfs /d0/d1/d2/d3
Error EINVAL: /d0/d1/d2/d3 is a subtree of tracked path /d0/d1/d2
```

ミラーデーモンへのディレクトリのマッピングとディレクトリの配布を確認するコマンドは、「Mirroring Status」セクションで詳しく説明します。

## Bootstrap Peers[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-mirroring/ "Permalink to this headline")

\(peer\_add を使って\) ピアを追加するには、ピアクラスタの設定とユーザーキーリングがプライマリクラスタ \(マネージャホストとミラーデーモンを実行しているホスト\) で利用可能であることが必要です。これは、ブートストラップとピア・トークンのインポートで回避することができます。ピアブートストラップは、ピアクラスタ上でブートストラップトークンを作成します：

```
$ ceph fs snapshot mirror peer_bootstrap create <fs_name> <client_entity> <site-name>
```

例：

```
$ ceph fs snapshot mirror peer_bootstrap create backup_fs client.mirror_remote site-remote
{"token": "eyJmc2lkIjogIjBkZjE3MjE3LWRmY2QtNDAzMC05MDc5LTM2Nzk4NTVkNDJlZiIsICJmaWxlc3lzdGVtIjogImJhY2t1cF9mcyIsICJ1c2VyIjogImNsaWVudC5taXJyb3JfcGVlcl9ib290c3RyYXAiLCAic2l0ZV9uYW1lIjogInNpdGUtcmVtb3RlIiwgImtleSI6ICJBUUFhcDBCZ0xtRmpOeEFBVnNyZXozai9YYUV0T2UrbUJEZlJDZz09IiwgIm1vbl9ob3N0IjogIlt2MjoxOTIuMTY4LjAuNTo0MDkxOCx2MToxOTIuMTY4LjAuNTo0MDkxOV0ifQ=="}
```

site\-nameは、リモートファイルシステムを識別するためのユーザ定義文字列を指します。peer\_add インターフェイスのコンテキストでは、site\-name は remote\_cluster\_spec から渡されたクラスタ名です。

プライマリクラスタにブートストラップトークンをインポートします：

```
$ ceph fs snapshot mirror peer_bootstrap import <fs_name> <token>
```

例：

```
$ ceph fs snapshot mirror peer_bootstrap import cephfs eyJmc2lkIjogIjBkZjE3MjE3LWRmY2QtNDAzMC05MDc5LTM2Nzk4NTVkNDJlZiIsICJmaWxlc3lzdGVtIjogImJhY2t1cF9mcyIsICJ1c2VyIjogImNsaWVudC5taXJyb3JfcGVlcl9ib290c3RyYXAiLCAic2l0ZV9uYW1lIjogInNpdGUtcmVtb3RlIiwgImtleSI6ICJBUUFhcDBCZ0xtRmpOeEFBVnNyZXozai9YYUV0T2UrbUJEZlJDZz09IiwgIm1vbl9ob3N0IjogIlt2MjoxOTIuMTY4LjAuNTo0MDkxOCx2MToxOTIuMTY4LjAuNTo0MDkxOV0ifQ==
```

## Mirroring Status[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-mirroring/ "Permalink to this headline")

CephFS mirroringモジュールは、ミラーデーモンの状態を確認するためのmirror daemon statusインターフェイスを提供します：

```
$ ceph fs snapshot mirror daemon status
[
  {
    "daemon_id": 284167,
    "filesystems": [
      {
        "filesystem_id": 1,
        "name": "a",
        "directory_count": 1,
        "peers": [
          {
            "uuid": "02117353-8cd1-44db-976b-eb20609aa160",
            "remote": {
              "client_name": "client.mirror_remote",
              "cluster_name": "ceph",
              "fs_name": "backup_fs"
            },
            "stats": {
              "failure_count": 1,
              "recovery_count": 0
            }
          }
        ]
      }
    ]
  }
]
```

ミラーデーモンインスタンスごとのエントリが、設定されているピアや基本的な統計情報などとともに表示されます。より詳細な統計情報を得るには、以下に示すように管理ソケットインタフェースを使用します。

CephFSミラーデーモンは、ミラー状態を問い合わせるための管理者ソケットコマンドを提供します。ミラー状態を問い合わせるために利用可能なコマンドを確認するには：

```
$ ceph --admin-daemon /path/to/mirror/daemon/admin/socket help
{
    ....
    ....
    "fs mirror status cephfs@360": "get filesystem mirror status",
    ....
    ....
}
```

fs mirror status の接頭辞を持つコマンドは、ミラーリングが有効なファイルシステムのミラー状態を提供します。cephfs@360はfilesystem\-name@filesystem\-idの形式であることに注意してください。ミラーデーモンはファイルシステムのミラー状態について非同期で通知を受けるため（ファイルシステムは削除後、同じ名前で再作成できるため）、この形式が必要です。

現在、このコマンドはミラー状態に関する最小限の情報を提供します：

```
$ ceph --admin-daemon /var/run/ceph/cephfs-mirror.asok fs mirror status cephfs@360
{
  "rados_inst": "192.168.0.5:0/1476644347",
  "peers": {
      "a2dc7784-e7a1-4723-b103-03ee8d8768f8": {
          "remote": {
              "client_name": "client.mirror_remote",
              "cluster_name": "site-a",
              "fs_name": "backup_fs"
          }
      }
  },
  "snap_dirs": {
      "dir_count": 1
  }
}
```

上記のコマンド出力の「Peers」セクションには、一意のピアID（UUID）や仕様などのピア情報が表示されます。ピアIDは、「Mirror Module and Interface」セクションで説明したように、既存のピアを削除するために必要です。

fs mirror peer status の接頭辞を持つコマンドは、ピア同期状態を提供します。このコマンドは filesystem\-name@filesystem\-id peer\-uuid の形式をとります。

```
$ ceph --admin-daemon /var/run/ceph/cephfs-mirror.asok fs mirror peer status cephfs@360 a2dc7784-e7a1-4723-b103-03ee8d8768f8
{
  "/d0": {
      "state": "idle",
      "last_synced_snap": {
          "id": 120,
          "name": "snap1",
          "sync_duration": 0.079997898999999997,
          "sync_time_stamp": "274900.558797s"
      },
      "snaps_synced": 2,
      "snaps_deleted": 0,
      "snaps_renamed": 0
  }
}
```

snaps\_synced、snaps\_deleted、snaps\_renamedなどの同期統計は、デーモンの再起動時やディレクトリが別のミラーデーモンに割り当てられた時（複数のミラーデーモンがデプロイされた時）にリセットされます。

ディレクトリは、以下のいずれかの状態になります：

```
- `idle`: 現在、ディレクトリは同期されていない
- `syncing`: ディレクトリは現在同期中
- `failed`: ディレクトリは連続失敗回数の上限に達している
```

ディレクトリが設定された数の連続した同期失敗をした場合、ミラーデーモンはそれをfailedとしてマークします。これらのディレクトリの同期が再試行されます。デフォルトでは、ディレクトリにfailedとマークされるまでの連続失敗回数はcephfs\_mirror\_max\_consecutive\_failures\_per\_directory構成オプション \(デフォルト: 10\) で、失敗したディレクトリの再試行間隔は cephfs\_mirror\_retry\_failed\_directories\_interval 構成オプションで制御します \(デフォルト: 60s\)。

例：同期対象に一般ファイルを追加すると、失敗状態になる。

```
$ ceph fs snapshot mirror add cephfs /f0
$ ceph --admin-daemon /var/run/ceph/cephfs-mirror.asok fs mirror peer status cephfs@360 a2dc7784-e7a1-4723-b103-03ee8d8768f8
{
  "/d0": {
      "state": "idle",
      "last_synced_snap": {
          "id": 120,
          "name": "snap1",
          "sync_duration": 0.079997898999999997,
          "sync_time_stamp": "274900.558797s"
      },
      "snaps_synced": 2,
      "snaps_deleted": 0,
      "snaps_renamed": 0
  },
  "/f0": {
      "state": "failed",
      "snaps_synced": 0,
      "snaps_deleted": 0,
      "snaps_renamed": 0
  }
}
```

これにより、ユーザは存在しないディレクトリを同期のために追加することができます。ミラーデーモンはそのディレクトリをfailedとしてマークし、再試行します（頻度は低くなります）。ディレクトリが存在するようになると、ミラーデーモンはスナップショットの同期が成功したときにfailedの印を外します。

ミラーリングがdisabledの場合、ファイルシステムのそれぞれのfs mirror statusコマンドは、コマンドヘルプに表示されません。

## Configuration Options[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-mirroring/ "Permalink to this headline")

**cephfs\_mirror\_max\_concurrent\_directory\_syncs**

Description

cephfs\-mirrorデーモンが同時に同期できるディレクトリスナップショットの最大数。同期スレッドの数を制御する

Type

64\-bit Integer Unsigned

Default

3

**cephfs\_mirror\_action\_update\_interval**

Description

保留中のミラー更新アクションを処理する間隔（秒）

Type

Float

Default

2

**cephfs\_mirror\_restart\_mirror\_on\_blocklist\_interval**

Description

ブロックリストされたミラーインスタンスを再起動する間隔（秒）。0に設定するとブロックリストされたインスタンスの再起動が無効になる

Type

Float

Default

30

**cephfs\_mirror\_max\_snapshot\_sync\_per\_cycle**

Description

ワーカスレッドがディレクトリをミラーリング対象にピックアップしたときに、ミラーリングするスナップショットの最大数

Type

64\-bit Integer Unsigned

Default

3

**cephfs\_mirror\_directory\_scan\_interval**

Description

スナップショットミラーリングに設定したディレクトリをスキャンする間隔（秒）

Type

64\-bit Integer Unsigned

Default

10

**cephfs\_mirror\_max\_consecutive\_failures\_per\_directory**

Description

ディレクトリを "failed "とマークするスナップショット同期の連続失敗回数。failedディレクトリは、同期の再試行頻度が低くなる

Type

64\-bit Integer Unsigned

Default

10

**cephfs\_mirror\_retry\_failed\_directories\_interval**

Description

失敗したディレクトリ同期を再試行する間隔（秒）

Type

64\-bit Integer Unsigned

Default

60

**cephfs\_mirror\_restart\_mirror\_on\_failure\_interval**

Description

失敗したミラーインスタンスを再起動する間隔（秒）。0に設定すると、失敗したミラーインスタンスの再起動が無効になる

Type

Float

Default

20

**cephfs\_mirror\_mount\_timeout**

Description

cephfs\-mirrorデーモンがプライマリまたはセカンダリ\(リモート\)cephファイルシステムをマウントするためのタイムアウト（秒）。この値を大きくすると、クラスタに到達できない場合、ファイルシステムをマウントするときにミラーデーモンが停止することがある。このオプションは、通常のclient\_mount\_timeoutをオーバーライドするために使用される

Type

Float

Default

10

## Re\-adding Peers[¶](https://docs.ceph.com/en/pacific/cephfs/cephfs-mirroring/ "Permalink to this headline")

別のクラスタ内のファイルシステムにピアを再追加（再割り当て）する場合、すべてのミラーデーモンが当該ピアへの同期を停止していることを確認します。これは、fs mirror status admin socket コマンドで確認できます（ピア UUID がコマンド出力に表示されないはずです）。また、別のファイルシステムに再追加する前に、当該ピアから同期したディレクトリ（特に、新しいプライマリファイルシステムに存在する可能性があるディレクトリ）をパージすることをお勧めします。以前に同期した同じプライマリ・ファイル・システムにピアを再追加する場合は、この必要はありません。
