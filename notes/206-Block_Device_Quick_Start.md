# 206: Block Device Quick Start

**クリップソース:** [206: Block Device Quick Start — Ceph Documentation](https://docs.ceph.com/en/pacific/start/quick-rbd/)

# Block Device Quick Start[¶](https://docs.ceph.com/en/pacific/start/quick-rbd/ "Permalink to this headline")

[Ceph Block Device](https://docs.ceph.com/en/pacific/glossary/#term-Ceph-Block-Device)を操作する前に、[Ceph Storage Cluster](https://docs.ceph.com/en/pacific/glossary/#term-Ceph-Storage-Cluster)がactive\+cleanの状態であることを確認します。

注：Ceph Block Deviceは、[RBD](https://docs.ceph.com/en/pacific/glossary/#term-RBD)または[RADOS](https://docs.ceph.com/en/pacific/glossary/#term-RADOS) Block Deviceとも呼ばれます。
![9620869c9c38737ce208fc534cbc389a7ed16f5823672e722239a828f1075bf2.png](image/9620869c9c38737ce208fc534cbc389a7ed16f5823672e722239a828f1075bf2.png)

ceph\-clientノードには仮想マシンを使用できますが、Ceph Storage Clusterノードと同じ物理ノードで以下の手順を実行しないでください\(VMを使用する場合を除く\)。詳細については、[FAQ](http://wiki.ceph.com/How_Can_I_Give_Ceph_a_Try)を参照してください。

## Create a Block Device Pool[¶](https://docs.ceph.com/en/pacific/start/quick-rbd/ "Permalink to this headline")

1. 管理ノードで、ceph ツールを使用してプールを作成します \(名前は 'rbd' を推奨します\)。
2. 管理ノードで、rbdツールを使用して、RBDが使用するプールを初期化します：

```
rbd pool init <pool-name>
```

## Configure a Block Device[¶](https://docs.ceph.com/en/pacific/start/quick-rbd/ "Permalink to this headline")

1. ceph\-clientノードで、ブロックデバイスイメージを作成します。

```
rbd create foo --size 4096 --image-feature layering [-m {mon-IP}] [-k /path/to/ceph.client.admin.keyring] [-p {pool-name}]
```

1. ceph\-clientノードで、イメージをブロックデバイスにマッピングします。

```
sudo rbd map foo --name client.admin [-m {mon-IP}] [-k /path/to/ceph.client.admin.keyring] [-p {pool-name}]
```

1. ceph\-clientノードにファイルシステムを作成し、ブロックデバイスを使用します。

```
sudo mkfs.ext4 -m0 /dev/rbd/{pool-name}/foo

This may take a few moments.
```

1. ceph\-clientノードにファイルシステムをマウントします。

```
sudo mkdir /mnt/ceph-block-device
sudo mount /dev/rbd/{pool-name}/foo /mnt/ceph-block-device
cd /mnt/ceph-block-device
```

1. オプションとして、起動時にブロックデバイスを自動的にマッピングしてマウントするように設定します（シャットダウン時にはアンマウント/アンマップ） \- 

詳しくは[ブロックデバイス](https://docs.ceph.com/en/pacific/rbd)をご覧ください。
