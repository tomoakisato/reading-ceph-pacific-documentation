# 440: activate

**クリップソース:** [440: activate — Ceph Documentation](https://docs.ceph.com/en/pacific/ceph-volume/lvm/activate/)

# activate[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/activate/ "Permalink to this headline")

[prepare](https://docs.ceph.com/en/pacific/ceph-volume/lvm/prepare/#ceph-volume-lvm-prepare)が完了し、ボリュームは「activated」できる状態になります。

このアクティベーションプロセスにより、OSD IDとそのUUID \(Ceph CLIツールではfsidとも呼ばれる\)を持続するsystemdユニットが有効になり、起動時に、どのOSDが有効でマウントが必要かを理解できるようになります。

注：この呼び出しの実行は完全にべき等であり、複数回実行しても副作用はありません

cephadmによって展開されたOSDについては、代わりに[既存のOSDを有効化する](https://docs.ceph.com/en/pacific/cephadm/services/osd/#activate-existing-osds)を参照してください。

## New OSDs[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/activate/ "Permalink to this headline")

新しく用意されたOSDを有効にするには、[OSD id](https://docs.ceph.com/en/pacific/glossary/#term-OSD-id)と[OSD uuid](https://docs.ceph.com/en/pacific/glossary/#term-OSD-uuid)の両方を提供する必要があります。例えば：

```
ceph-volume lvm activate --bluestore 0 0263644D-0BF1-4D6D-BC34-28BD98AE3BC8
```

注：UUIDは[prepare](https://docs.ceph.com/en/pacific/ceph-volume/lvm/prepare/#ceph-volume-lvm-prepare)使用時に生成されるOSDパス内のfsidファイルに格納されます。

## Activating all OSDs[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/activate/ "Permalink to this headline")

注：cephadmによって展開されたOSDについては、代わりに[既存のOSDを有効化する](https://docs.ceph.com/en/pacific/cephadm/services/osd/#activate-existing-osds)を参照してください。

allフラグを使用することにより、既存のすべてのOSDを一度に起動することが可能です。例えば：

```
ceph-volume lvm activate --all
```

この呼び出しは、ceph\-volumeによって作成されたすべての非アクティブなOSDを検査し、それらを1つずつアクティブにします。OSDのいずれかがすでに実行されている場合、それらをスキップして、再実行しても安全なように（べき等に）します。

### requiring uuids[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/activate/ "Permalink to this headline")

[OSD uuid](https://docs.ceph.com/en/pacific/glossary/#term-OSD-uuid)は、正しいOSDが起動されていることを確認するための追加ステップとして要求されています。同じ ID を持つ以前の OSD が存在し、間違ったものを起動することになる可能性は十分にあります。

### dmcrypt[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/activate/ "Permalink to this headline")

ceph\-volumeでdmcryptを使用してOSDを準備した場合、コマンドラインで再度\-\-dmcryptを指定する必要はありません（このフラグはactivateサブコマンドでは使用できません）。暗号化されたOSDは自動的に検出されます。

## Discovery[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/activate/ "Permalink to this headline")

以前にceph\-volumeで作成されたOSDでは、[LVMタグ](https://docs.ceph.com/en/pacific/glossary/#term-LVM-tags)を用いた発見処理が行われ、systemdユニットが有効化されます。

systemd ユニットが [OSD id](https://docs.ceph.com/en/pacific/glossary/#term-OSD-id) と [OSD uuid](https://docs.ceph.com/en/pacific/glossary/#term-OSD-uuid) を取得し、それを永続化します。内部的には、アクティベーションによって、このように有効になります：

```
systemctl enable ceph-volume@lvm-$id-$uuid
```

For example:

```
systemctl enable ceph-volume@lvm-0-8715BEB4-15C5-49DE-BA6F-401086EC7B41
```

上記は、0のIDと8715BEB4\-15C5\-49DE\-BA6F\-401086EC7B41のUUIDを持つOSDのディスカバープロセスを開始することになります。

注：systemdワークフローの詳細については、[systemd](https://docs.ceph.com/en/pacific/ceph-volume/lvm/systemd/#ceph-volume-lvm-systemd)を参照してください。

systemdユニットは、一致するOSDデバイスを探し、そのLVMタグを見ることによって、次のように進みます：

\# 対応する場所にデバイスをマウントします \(慣習として、これは 

/var/lib/ceph/osd/\<clustername\>\-\<osdid\>/ となります\)

\# そのOSDに必要なデバイスがすべて準備されていることを確認します。ジャーナルの場合 \(\-\-filestore が選択された場合\)、デバイスは \(パーティションの場合は blkid、論理ボリュームの場合は lvm で\) 問い合わせられ、正しいデバイスがリンクされていることが確認されます。シンボリックリンクは常に再実行され、正しいデバイスがリンクされていることを確認します。

\# ceph\-osd@0 systemd ユニットを起動します。

注：システムは、OSDデバイスに適用されたLVMタグを検査することによって、オブジェクトストアのタイプ（filestoreストアまたはbluestore）を推測します。

## Existing OSDs[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/activate/ "Permalink to this headline")

ceph\-diskで展開された既存のOSDについては、[simpleサブコマンドを使用](https://docs.ceph.com/en/pacific/ceph-volume/simple/#ceph-volume-simple)してスキャンし、アクティブ化する必要があります。別のツールが使用された場合、新しいメカニズムにそれらを移植する唯一の方法は、それらを再度準備することです\(データを失います\)。処理方法の詳細については、[既存のOSD](https://docs.ceph.com/en/pacific/ceph-volume/lvm/prepare/#ceph-volume-lvm-existing-osds)を参照してください。

## Summary[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/activate/ "Permalink to this headline")

[bluestore](https://docs.ceph.com/en/pacific/glossary/#term-bluestore)のactivateプロセス：

1. [OSD id](https://docs.ceph.com/en/pacific/glossary/#term-OSD-id)
2. IDとUUIDが一致するsystemdユニットを有効にする
3. var/lib/ceph/osd/$cluster\-$id/ にあるOSDディレクトリにtmpfsマウントを作成
4. ceph\-bluestore\-toolprime\-osd\-dirをOSDブロックデバイスに指定して、必要なファイルをすべて再作成
5. systemd ユニットは、デバイスが準備され、リンクされていることを確認する
6. マッチする ceph\-osd systemd ユニットが起動する

[filestore](https://docs.ceph.com/en/pacific/glossary/#term-filestore)のactivateプロセス：

1. [OSD id](https://docs.ceph.com/en/pacific/glossary/#term-OSD-id)
2. IDとUUIDが一致するsystemdユニットを有効にする
3. systemd ユニットは、デバイスが準備されマウントされていることを確認する \(必要な場合\)
4. マッチする ceph\-osd systemd ユニットが起動する
