# 9: Get Packages

**クリップソース:** [9: Get Packages — Ceph Documentation](https://docs.ceph.com/en/pacific/install/get-packages/)

# Get Packages¶

Cephやその他の有効なソフトウェアをインストールするには、Cephのリポジトリからパッケージを取得する必要があります。

パッケージの入手方法は3つあります：

* **Cephadm:**
* **Configure Repositories Manually:**
* **Download Packages Manually:**

## **Install packages with cephadm[¶](https://docs.ceph.com/en/pacific/install/get-packages/#install-packages-with-cephadm "Permalink to this headline")**

cephadmスクリプトのダウンロード

```
curl --silent --remote-name --location https://github.com/ceph/ceph/raw/octopus/src/cephadm/cephadm
chmod +x cephadm
```

リリース名に基づいてCephリポジトリを構成：

```
./cephadm add-repo --release nautilus
```

Octopus（15.2.0）以降のリリースでは、特定のバージョンを指定することもできます：

```
./cephadm add-repo --version 15.2.1
```

開発用パッケージでは、特定のブランチ名を指定することができます：

```
./cephadm add-repo --dev my-branch
```

適切なパッケージをインストールします。 パッケージ管理ツール\(APT、Yumなど\)を使って直接インストールすることもできますが、cephadmのラッパーを使うこともできます。 たとえば、以下のようになります。

```
./cephadm install ceph-common
```

## **Configure Repositories Manually[¶](https://docs.ceph.com/en/pacific/install/get-packages/#configure-repositories-manually "Permalink to this headline")**

\(開発を除いて\)CephのデプロイメントにはCephパッケージが必要です。また、キーと推奨パッケージを追加する必要があります。

* **Keys: \(Recommended\)**
* **Ceph: \(Required\)**
* **Ceph Development: \(Optional\)**

### Add Keys[¶](https://docs.ceph.com/en/pacific/install/get-packages/#add-keys "Permalink to this headline")

セキュリティ警告を回避するために、システムの信頼できる鍵のリストに鍵を追加します。メジャーリリース（luminous、mimic、nautilusなど）や開発リリース（release\-name\-rc1、release\-name\-rc2）では、release.ascキーを使用します。

#### **APT[¶](https://docs.ceph.com/en/pacific/install/get-packages/#apt "Permalink to this headline")**

release.ascキーをインストールするには、次のように実行します：

```
wget -q -O- 'https://download.ceph.com/keys/release.asc' | sudo apt-key add -
```

#### **RPM[¶](https://docs.ceph.com/en/pacific/install/get-packages/#rpm "Permalink to this headline")**

release.ascキーをインストールするには、次のように実行します：

```
sudo rpm --import 'https://download.ceph.com/keys/release.asc'
```

### Ceph Release Packages[¶](https://docs.ceph.com/en/pacific/install/get-packages/#ceph-release-packages "Permalink to this headline")

リリースリポジトリでは、パッケージの検証にrelease.ascキーを使用します。Advanced Package Tool \(APT\)またはYellowdog Updater, Modified \(YUM\)でCephのパッケージをインストールするには、Cephのリポジトリを追加する必要があります。

APTでインストールされたDebian/Ubuntu用のリリースは以下のサイトでご覧いただけます：

```
https://download.ceph.com/debian-{release-name}
```

CentOS/RHELなどのリリース（YUMでインストール）は以下のサイトでご覧いただけます：

```
https://download.ceph.com/rpm-{release-name}
```

Octopus以降のリリースでは、特定のバージョンx.y.z用のリポジトリを設定することもできます。 Debian/Ubuntuパッケージの場合：

```
https://download.ceph.com/debian-{version}
```

For RPMs:

```
https://download.ceph.com/rpm-{version}
```

Cephの主要なリリースについては、[Ceph Releases \(general\)](https://docs.ceph.com/en/pacific/releases/general/#ceph-releases-general) でまとめています。[458: Ceph Releases \(general\)](458-Ceph_Releases_\(general\)_—_Ceph_Documentation.md)

Tip

米国以外のユーザーの方へ。お近くにCephをダウンロードできるミラーがあるかもしれません。詳細についてはCeph Mirrors[Ceph Mirrors](https://docs.ceph.com/en/pacific/install/mirrors)を参照してください。[13: Ceph Mirrors](13-Ceph_Mirrors_—_Ceph_Documentation.md)

#### **Debian Packages[¶](https://docs.ceph.com/en/pacific/install/get-packages/#debian-packages "Permalink to this headline")**

システムのAPTソースのリストにCephパッケージリポジトリを追加します。Debian/Ubuntuの新しいバージョンでは、コマンドラインでlsb\_release \-scを呼び出して短いコードネームを取得し、次のコマンドで{codename}を置き換えます。

```
sudo apt-add-repository 'deb https://download.ceph.com/debian-octopus/ {codename} main'
```

初期のLinuxディストリビューションでは、以下のコマンドを実行することができます。

```
echo deb https://download.ceph.com/debian-octopus/ $(lsb_release -sc) main | sudo tee /etc/apt/sources.list.d/ceph.list
```

以前のCephのリリースの場合は、{release\-name}をCephのリリース名に置き換えてください。コマンドラインでlsb\_release \-scを呼び出して短いコードネームを取得し、次のコマンドで{codename}を置き換えることができます。

```
sudo apt-add-repository 'deb https://download.ceph.com/debian-{release-name}/ {codename} main'
```

古いLinuxディストリビューションの場合は、{release\-name}をリリース名に置き換えてください。

```
echo deb https://download.ceph.com/debian-{release-name}/ $(lsb_release -sc) main | sudo tee /etc/apt/sources.list.d/ceph.list
```

開発リリースパッケージについては、お使いのシステムのAPTソースのリストに私たちのパッケージリポジトリを追加してください。 サポートされている Debian と Ubuntu のリリースの完全なリストは、[testing Debian リポジトリ](https://download.ceph.com/debian-testing/dists)をご覧ください。

```
echo deb https://download.ceph.com/debian-testing/ $(lsb_release -sc) main | sudo tee /etc/apt/sources.list.d/ceph.list
```

Tip

米国以外のユーザーの方へ。お近くにCephをダウンロードできるミラーがあるかもしれません。詳細については以下を参照してください: [Ceph Mirrors](https://docs.ceph.com/en/pacific/install/mirrors).

#### **RPM Packages[¶](https://docs.ceph.com/en/pacific/install/get-packages/#rpm-packages "Permalink to this headline")**

##### **RHEL[¶](https://docs.ceph.com/en/pacific/install/get-packages/#rhel "Permalink to this headline")**

メジャーリリースの場合は、/etc/yum.repos.dディレクトリにCephエントリを追加することができます。ceph.repoファイルを作成します。以下の例では、{ceph\-release}をCephのメジャーリリース\(例: |stable\-release|\)に、{distro}をお使いのLinuxディストリビューション\(例: el8など\)に置き換えます。Cephがどのディストリビューションをサポートしているかは、[https://download.ceph.com/rpm](https://download.ceph.com/rpm)\-{ceph\-release}/ディレクトリで確認できます。一部のCephパッケージ\(EPELなど\)は標準パッケージよりも優先される必要があるため、priority=2を確実に設定する必要があります。

```
[ceph]
name=Ceph packages for $basearch
baseurl=https://download.ceph.com/rpm-{ceph-release}/{distro}/$basearch
enabled=1
priority=2
gpgcheck=1
gpgkey=https://download.ceph.com/keys/release.asc

[ceph-noarch]
name=Ceph noarch packages
baseurl=https://download.ceph.com/rpm-{ceph-release}/{distro}/noarch
enabled=1
priority=2
gpgcheck=1
gpgkey=https://download.ceph.com/keys/release.asc

[ceph-source]
name=Ceph source packages
baseurl=https://download.ceph.com/rpm-{ceph-release}/{distro}/SRPMS
enabled=0
priority=2
gpgcheck=1
gpgkey=https://download.ceph.com/keys/release.asc
```

特定のパッケージについては、名前を付けてリリースパッケージをダウンロードすることで取得することができます。当社の開発プロセスでは、3～4週間ごとにCephの新しいリリースが生成されます。これらのパッケージは、メジャーリリースよりも動きが速いです。 開発パッケージには、新機能が迅速に統合されていますが、リリース前には数週間のQAが行われます。

リポジトリパッケージは、yumで使用するリポジトリの詳細をローカルシステムにインストールします。distro}はお使いのLinuxディストリビューションに、{release}はCephの特定のリリースに置き換えてください。

```
su -c 'rpm -Uvh https://download.ceph.com/rpms/{distro}/x86_64/ceph-{release}.el7.noarch.rpm'
```

直接RPMをダウンロードすることができます。

```
https://download.ceph.com/rpm-testing
```

Tip

米国以外のユーザーの方へ。お近くにCephをダウンロードできるミラーがあるかもしれません。詳細については以下を参照してください。: [Ceph Mirrors](https://docs.ceph.com/en/pacific/install/mirrors).

##### **openSUSE Leap 15.1[¶](https://docs.ceph.com/en/pacific/install/get-packages/#opensuse-leap-15-1 "Permalink to this headline")**

Cephパッケージリポジトリを、zypperソースのリストに追加する必要があります。これは、次のコマンドで実行できます。

```
zypper ar https://download.opensuse.org/repositories/filesystems:/ceph/openSUSE_Leap_15.1/filesystems:ceph.repo
```

##### **openSUSE Tumbleweed[¶](https://docs.ceph.com/en/pacific/install/get-packages/#opensuse-tumbleweed "Permalink to this headline")**

Cephの最新メジャーリリースは、通常のTumbleweedリポジトリからすでに入手可能です。別のパッケージリポジトリを手動で追加する必要はありません。

### Ceph Development Packages[¶](https://docs.ceph.com/en/pacific/install/get-packages/#ceph-development-packages "Permalink to this headline")

Cephを開発していて、特定のCephブランチを展開してテストする必要がある場合は、まずメジャーリリースのリポジトリエントリを確実に削除してください。

#### **DEB Packages[¶](https://docs.ceph.com/en/pacific/install/get-packages/#deb-packages "Permalink to this headline")**

Cephのソースコードリポジトリの現在の開発ブランチに対して、Ubuntuパッケージを自動的にビルドします。 これらのパッケージは、開発者とQAのみを対象としています。

パッケージリポジトリをシステムのAPTソースリストに追加しますが、{BRANCH}は使用したいブランチに置き換えてください（例：wip\-hack、master）。私たちがビルドするディストリビューションの完全なリストについては、 [the shaman page](https://shaman.ceph.com/) をご覧ください。

```
curl -L https://shaman.ceph.com/api/repos/ceph/{BRANCH}/latest/ubuntu/$(lsb_release -sc)/repo/ | sudo tee /etc/apt/sources.list.d/shaman.list
```

Note

リポジトリの準備ができていない場合は、HTTP 504 を返します。

URLにlatestを使用しているのは、最後にビルドされたコミットがどれかを把握するためです。また、特定のsha1を指定することもできます。Ubuntu XenialとCephのmasterブランチの場合は、次のようになります。

```
curl -L https://shaman.ceph.com/api/repos/ceph/master/53e772a45fdf2d211c0c383106a66e1feedec8fd/ubuntu/xenial/repo/ | sudo tee /etc/apt/sources.list.d/shaman.list
```

Warning

開発用のリポジトリは2週間で利用できなくなります。

#### **RPM Packages[¶](https://docs.ceph.com/en/pacific/install/get-packages/#id1 "Permalink to this headline")**

現在の開発ブランチについては、/etc/yum.repos.dディレクトリにCephのエントリを追加することができます。[shamanページ](https://shaman.ceph.com/)を使用すると、レポファイルの完全な詳細を取得できます。これは、次のようなHTTPリクエストで取得できます。

```
curl -L https://shaman.ceph.com/api/repos/ceph/{BRANCH}/latest/centos/7/repo/ | sudo tee /etc/yum.repos.d/shaman.repo
```

URLにlatestを使用することで、最後にビルドされたコミットを把握することができます。また、特定のsha1を指定することもできます。CentOS 7とCephのmasterブランチの場合は、次のようになります。

```
curl -L https://shaman.ceph.com/api/repos/ceph/master/53e772a45fdf2d211c0c383106a66e1feedec8fd/centos/7/repo/ | sudo tee /etc/apt/sources.list.d/shaman.list
```

Warning

開発用のリポジトリは2週間で利用できなくなります。

Note

リポジトリの準備ができていない場合は、HTTP 504が返されます。

### Download Packages Manually[¶](https://docs.ceph.com/en/pacific/install/get-packages/#download-packages-manually "Permalink to this headline")

インターネットに接続されていない環境でファイアウォールの内側にインストールしようとしている場合は、インストールを試みる前にパッケージ（必要な依存関係をすべてミラーリングしたもの）を取得する必要があります。

#### **Debian Packages[¶](https://docs.ceph.com/en/pacific/install/get-packages/#id2 "Permalink to this headline")**

Cephには追加のサードパーティライブラリが必要です。

* libaio1
* libsnappy1
* libcurl3
* curl
* libgoogle\-perftools4
* google\-perftools
* libleveldb1

リポジトリパッケージは、aptで使用するためのリポジトリの詳細をローカルシステムにインストールします。

{release}を最新のCephのリリースに置き換えてください。

{version}を最新のCephのバージョン番号に置き換えてください。

{distro}をお使いのLinuxディストリビューションのコードネームに置き換えてください。

{arch}をCPUアーキテクチャに置き換えてください。

```
wget -q https://download.ceph.com/debian-{release}/pool/main/c/ceph/ceph_{version}{distro}_{arch}.deb
```

#### **RPM Packages[¶](https://docs.ceph.com/en/pacific/install/get-packages/#id3 "Permalink to this headline")**

Cephには追加のサードパーティライブラリが必要です。EPELリポジトリを追加するには、次のように実行します。

```
sudo yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
```

Cephには以下のパッケージが必要です。

* snappy
* leveldb
* gdisk
* python\-argparse
* gperftools\-libs

パッケージは現在、RHEL/CentOS7 \(el7\) プラットフォーム用に構築されています。

リポジトリパッケージは、yum で使用するリポジトリの詳細をローカルシステムにインストールします。

{distro}をお使いのディストリビューションに置き換えてください。

```
su -c 'rpm -Uvh https://download.ceph.com/rpm-octopus/{distro}/noarch/ceph-{version}.{distro}.noarch.rpm'
```

For example, for CentOS 8  \(el8\)

```
su -c 'rpm -Uvh https://download.ceph.com/rpm-octopus/el8/noarch/ceph-release-1-0.el8.noarch.rpm'
```

直接RPMをダウンロードすることができます。

```
https://download.ceph.com/rpm-octopus
```

以前のCephのリリースの場合は、{release\-name}をCephのリリース名に置き換えてください。

コマンドラインでlsb\_release \-scを呼び出して、短いコードネームを取得することもできます。

```
su -c 'rpm -Uvh https://download.ceph.com/rpm-{release-name}/{distro}/noarch/ceph-{version}.{distro}.noarch.rpm'
```
