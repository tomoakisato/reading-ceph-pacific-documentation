# 438: drive-group

**クリップソース:** [438: drive\-group — Ceph Documentation](https://docs.ceph.com/en/pacific/ceph-volume/drive-group/)

# drive\-group[¶](https://docs.ceph.com/en/pacific/ceph-volume/drive-group/ "Permalink to this headline")

drive\-groupサブコマンドでは、:ref:'drivegroups'の仕様をそのままjsonとしてceph\-volumeに渡すことができます。ceph\-volumeはこのドライブグループをbatchサブコマンドでデプロイしようと試みます。

仕様は、ファイル、文字列引数、またはstdinを介して渡すことができます。 詳細については、サブコマンドのヘルプを参照してください。

```
# ceph-volume drive-group --help
```
