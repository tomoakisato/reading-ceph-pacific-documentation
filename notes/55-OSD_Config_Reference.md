# 55: OSD Config Reference

**クリップソース:** [55: OSD Config Reference — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/configuration/osd-config-ref/)

# OSD Config Reference[¶](https://docs.ceph.com/en/pacific/rados/configuration/osd-config-ref/ "Permalink to this headline")

Ceph OSD DaemonはCeph設定ファイル\(または最近のリリースでは集中設定ストア\)で設定できますが、Ceph OSD Daemonはデフォルト値と最小限の設定を使用できます。最小のCeph OSD Daemon構成では、osd journal size\(Filestore用\)、hostを設定し、その他のほぼすべてにデフォルト値を使用します。

Ceph OSDデーモンは、次の規則に従って、0から始まるインクリメンタルな方法で数字で識別されます。

```
osd.0
osd.1
osd.2
```

設定ファイルの\[osd\]セクションに設定を追加することで、クラスタ内のすべてのCeph OSD Daemonの設定を指定することができます。特定のCeph OSD Daemon\(ホストなど\)に直接設定を追加するには、設定ファイルのOSD固有のセクションに入力します。たとえば、以下のようになります。

```
[osd]
        osd_journal_size = 5120

[osd.0]
        host = osd-host-a

[osd.1]
        host = osd-host-b
```

## General Settings[¶](https://docs.ceph.com/en/pacific/rados/configuration/osd-config-ref/ "Permalink to this headline")

以下の設定は、Ceph OSD DaemonのIDを提供し、データとジャーナルへのパスを決定します。Cephの展開スクリプトは通常、UUIDを自動的に生成します。

警告:データやジャーナルのデフォルトパスを変更しないでください、後でCephをトラブルシューティングする際に問題が生じます。

Filestoreを使用する場合、ジャーナルサイズは、予想されるドライブ速度にfilestore\_max\_sync\_intervalを乗じた積の2倍以上にする必要があります。しかし、最も一般的な方法は、ジャーナルドライブ\(多くの場合SSD\)をパーティション化し、Cephがパーティション全体をジャーナルに使用するようにマウントすることです。

**osd\_uuid**

Description

Ceph OSD DaemonのUniversally Unique Identifier \(UUID\)。

Type

_UUID_

Default

_The UUID._

Note

osd\_uuidは、単一のCeph OSDデーモンに適用されます。fsidはクラスタ全体に適用されます。

**osd\_data**

Description

OSDsデータのパスです。Cephの導入時にこのディレクトリを作成する必要があります。このマウントポイントにOSDデータ用のドライブをマウントする必要があります。デフォルトの変更はお勧めしません。

Type

_String_

Default

_/var/lib/ceph/osd/$cluster\-$id_

**osd\_max\_write\_size**

Description

書き込みの最大サイズ（メガバイト）

Type

_32\-bit Integer_

Default

_90_

**osd\_max\_object\_size**

Description

RADOSオブジェクトの最大サイズ（バイト）

Type

_32\-bit Unsigned Integer_

Default

_128MB_

**osd\_client\_message\_size\_cap**

Description

メモリ上で許可される最大のクライアントデータメッセージ

Type

_64\-bit Unsigned Integer_

Default

_500MB default. 500\*1024L\*1024L_

**osd\_class\_dir**

Description

RADOSクラスのプラグインのクラスパス

Type

_String_

Default

_$libdir/rados\-classes_

## File System Settings[¶](https://docs.ceph.com/en/pacific/rados/configuration/osd-config-ref/ "Permalink to this headline")

Cephは、Ceph OSDに使用されるファイルシステムを構築してマウントします。

**osd\_mkfs\_options{fs\-type}**

Description

{fs\-type}タイプのCeph Filestore OSDを新規作成するときに使用するオプション

Type

_String_

Default for xfs

_\-f\-i2048_

Default for other file systems

_{empty string}_

For example::

_osd\_mkfs\_options\_xfs=\-f\-dagcount=24_

**osd\_mount\_options{fs\-type}**

Description

タイプ{fs\-type}のCeph Filestore OSDをマウントするときに使用するオプション

Type

_String_

Default for xfs

_rw,noatime,inode64_

Default for other file systems

_rw,noatime_

For example::

_osd\_mount\_options\_xfs=rw,noatime,inode64,logbufs=8_

## Journal Settings[¶](https://docs.ceph.com/en/pacific/rados/configuration/osd-config-ref/ "Permalink to this headline")

本項は、旧式のFilesore OSDバックエンドにのみ適用されます。 Luminous BlueStore以降はデフォルトで優先されます。

デフォルトでは、Cephはお客様がCeph OSDデーモンのジャーナルを次のパス\(通常はデバイスまたはパーティションへのシンボリックリンク\)にプロビジョニングすることを想定しています。

```
/var/lib/ceph/osd/$cluster-$id/journal
```

単一のデバイスタイプ（スピニングドライブなど）を使用する場合は、ジャーナルをコロケーションする必要があります。論理ボリューム（またはパーティション）は、データ論理ボリュームと同じデバイスに配置する必要があります。

高速なデバイス（SSD、NVMe）と低速なデバイス（回転ドライブなど）を混在して使用する場合、ジャーナルを高速なデバイスに配置し、データは低速なデバイスを完全に占有することが理にかなっています。

デフォルトのosd\_journal\_size値は5120\(5ギガバイト\)ですが、これより大きくすることも可能で、その場合はceph.confファイルで設定する必要があります。実際には、10ギガバイトの値が一般的です。

```
osd_journal_size = 10240
```

**osd\_journal**

Description

OSDのジャーナルへのパスです。これは、ファイルまたはブロックデバイス（SSDのパーティションなど）へのパスである場合があります。ファイルの場合は、そのファイルを格納するディレクトリを作成する必要があります。osd\_dataドライブがHDDの場合は、別の高速デバイスを使用することをお勧めします。

Type

_String_

Default

_/var/lib/ceph/osd/$cluster\-$id/journal_

**osd\_journal\_size**

Description

_ジャーナルのサイズ（メガバイト）_

Type

_32\-bit Integer_

Default

_5120_

詳細は [Journal Config Reference](https://docs.ceph.com/en/pacific/rados/configuration/osd-config-ref/)を参照してください。

## Monitor OSD Interaction[¶](https://docs.ceph.com/en/pacific/rados/configuration/osd-config-ref/ "Permalink to this headline")

Ceph OSDデーモンは、互いのハートビートをチェックし、定期的にモニタに報告します。Cephは多くの場合、デフォルト値を使用できます。ただし、ネットワークに遅延の問題がある場合は、より長い間隔を採用する必要があります。ハートビートの詳細については、「モニタ/OSDの相互作用の設定」を参照してください。

## Data Placement[¶](https://docs.ceph.com/en/pacific/rados/configuration/osd-config-ref/ "Permalink to this headline")

詳細は「 [Pool & PG Config Reference](https://docs.ceph.com/en/pacific/rados/configuration/osd-config-ref/) 」を参照してください。

## Scrubbing[¶](https://docs.ceph.com/en/pacific/rados/configuration/osd-config-ref/ "Permalink to this headline")

Cephでは、オブジェクトの複数のコピーを作成するだけでなく、配置グループをスクラブすることでデータの整合性を確保します。Cephのスクラビングは、オブジェクトストレージ層のfsckに類似しています。各配置グループについて、Cephはすべてのオブジェクトのカタログを生成し、各プライマリオブジェクトとそのレプリカを比較して、オブジェクトの欠落や不一致がないことを確認します。ライトスクラビング（毎日）では、オブジェクトのサイズと属性をチェックします。 ディープスクラビング（週1回）では、データを読み取り、チェックサムを使ってデータの整合性を確保します。

スクラビングはデータの整合性を保つために重要ですが、パフォーマンスが低下する可能性があります。以下の設定を調整することで、スクラビング操作を増加または減少させることができます。

**osd\_max\_scrubs**

Description

Ceph OSDデーモンの同時スクラブ操作の最大数

Type

_32\-bit Int_

Default

_1_

**osd\_scrub\_begin\_hour**

Description

これにより、スクラビングが1日のこの時間以降に制限されます。osd\_scrub\_begin\_hour=0およびosd\_scrub\_end\_hour=0を使用すると、1日中スクラブを行うことができます。 osd\_scrub\_end\_hourとともに、これらはスクラブが実行される時間帯を定義します。しかし、配置グループのスクラブ間隔がosd\_scrub\_max\_intervalを超えている限り、タイムウィンドウが許可されているかどうかにかかわらず、スクラブは実行される。

Type

_Integer in the range of 0 to 23_

Default

_0_

**osd\_scrub\_end\_hour**

Description

これにより、スクラブがこれより前の時間に制限されます。osd\_scrub\_begin\_hour=0およびosd\_scrub\_end\_hour=0を使用すると、1日中スクラブを実行できます。 osd\_scrub\_begin\_hourとともに、これらはスクラブが実行される時間帯を定義します。しかし、配置グループのスクラブ間隔がosd\_scrub\_max\_intervalを超えている限り、タイムウィンドウが許可されているかどうかにかかわらず、スクラブは実行される。

Type

_Integer in the range of 0 to 23_

Default

_0_

**osd\_scrub\_begin\_week\_day**

Description

これは、スクラブをこの曜日以降に制限するものです。0 = 日曜日、1 = 月曜日、など。osd\_scrub\_begin\_week\_day=0およびosd\_scrub\_end\_week\_day=0を使用して、1週間全体のスクラブを許可します。osd\_scrub\_end\_week\_dayと合わせて、スクラブを実行できる時間帯を定義します。しかし、PGのスクラブ間隔がosd\_scrub\_max\_intervalを超えると、時間窓が許すかどうかにかかわらず、スクラブが実行される。

Type

_Integer in the range of 0 to 6_

Default

_0_

**osd\_scrub\_end\_week\_day**

Description

これにより、スクラブはこれよりも早い曜日に制限されます。0 = 日曜日、1 = 月曜日、など。 osd\_scrub\_begin\_week\_day=0およびosd\_scrub\_end\_week\_day=0を使用して、1週間全体のスクラブを許可します。osd\_scrub\_begin\_week\_dayと合わせて、スクラブが実行される時間帯を定義します。しかし、配置グループのスクラブ間隔がosd\_scrub\_max\_intervalを超える限り、タイムウィンドウが許可されているかどうかにかかわらず、スクラブは実行される。

Type

_Integer in the range of 0 to 6_

Default

_0_

**osd scrub during recovery**

Description

回復中のスクラブを許可する。これをfalseに設定すると、復旧作業中は新しいスクラブ（およびディープスクラブ）のスケジューリングが無効になります。すでに実行されているスクラブは継続されます。これは、忙しいクラスターの負荷を軽減するのに便利です。

Type

_Boolean_

Default

_false_

**osd\_scrub\_thread\_timeout**

Description

スクラブ・スレッドがタイムアウトするまでの最大時間を秒単位で指定します。

Type

_32\-bit Integer_

Default

_60_

**osd\_scrub\_finalize\_thread\_timeout**

Description

scrub finalizeスレッドがタイムアウトするまでの最大時間を秒単位で指定します。

Type

_32\-bit Integer_

Default

_10\*60_

**osd\_scrub\_load\_threshold**

Description

正規化された最大負荷。システムの負荷\(getloadavg\(\)/numberofonlineCPUsで定義される\)がこの数値より高い場合、Cephはスクラブを行いません。デフォルトは0.5です。

Type

_Float_

Default

_0.5_

**osd\_scrub\_min\_interval**

Description

Ceph Storage Clusterの負荷が低いときにCeph OSD Daemonをスクラブするための最小間隔\(秒\)

Type

_Float_

Default

_Once per day. 24\*60\*60_

**osd\_scrub\_max\_interval**

Description

クラスタの負荷にかかわらず、Ceph OSDデーモンをスクラブする最大の間隔を秒単位で指定します。

Type

_Float_

Default

_Once per week. 7\*24\*60\*60_

**osd\_scrub\_chunk\_min**

Description

1回の操作でスクラブするオブジェクトストアチャンクの最小数。Cephはスクラブ中に1つのチャンクへの書き込みをブロックします。

Type

_32\-bit Integer_

Default

_5_

**osd\_scrub\_chunk\_max**

Description

1回の操作でスクラブするオブジェクトストアチャンクの最大数

Type

_32\-bit Integer_

Default

_25_

**osd\_scrub\_sleep**

Description

次のチャンクのグループをスクラブするまでのスリープ時間です。この値を大きくすると、スクラビングの全体的な速度が遅くなり、クライアントの操作への影響が少なくなります。

Type

_Float_

Default

_0_

**osd\_deep\_scrub\_interval**

Description

deep」スクラブ（すべてのデータを完全に読み込む）の間隔。osd\_scrub\_load\_thresholdは、この設定に影響しません。

Type

_Float_

Default

_Once per week.  7\*24\*60\*60_

**osd\_scrub\_interval\_randomize\_ratio**

Description

PGの次のスクラブジョブをスケジューリングする際に、osd\_scrub\_min\_intervalにランダムな遅延を追加する。遅延は、osd\_scrub\_min\_interval \* osd\_scrub\_interval\_randomized\_ratioよりも小さいランダムな値です。デフォルトの設定では、\[1,1.5\] \* osd\_scrub\_min\_intervalの許可された時間枠全体にスクラブを広げます。

Type

_Float_

Default

_0.5_

**osd\_deep\_scrub\_stride**

Description

ディープ・スクラブ時にreadするサイズ

Type

_32\-bit Integer_

Default

_512 KB. 524288_

**osd\_scrub\_auto\_repair**

Description

これをtrueに設定すると、スクラブまたはディープ・スクラブによってエラーが発見された場合、PGの自動修復が有効になる。 ただし、osd\_scrub\_auto\_repair\_num\_errors以上のエラーが見つかった場合は、修復は行われない。

Type

_Boolean_

Default

_false_

**osd\_scrub\_auto\_repair\_num\_errors**

Description

この数以上のエラーが発生した場合、自動修復は行われない。

Type

_32\-bit Integer_

Default

_5_

## Operations[¶](https://docs.ceph.com/en/pacific/rados/configuration/osd-config-ref/ "Permalink to this headline")

**osd\_op\_queue**

Description

これは、各OSD内のOPSの優先順位付けに使用するキューの種類を設定するものです。どちらのキューも、通常のキューの前にデキューされる厳格なサブキューを備えています。通常のキューは、実装によって異なります。WeightedPriorityQueue \(wpq\) は、どのキューも飢餓状態に陥らないように、優先度に応じて操作を待ち受けます。WPQは、いくつかのOSDが他のOSDよりも過負荷になっている場合に役立つはずです。新しいmClockQueue（mclock\_scheduler）は、どのクラス（リカバリ、スクラブ、スナップトリム、クライアントオペ、OSDサブオペ）に属しているかに基づいて操作の優先順位を決定します。 「[QoS Based on mClock](https://docs.ceph.com/en/pacific/rados/configuration/osd-config-ref/)」を参照してください。再起動が必要です。

Type

_String_

Valid Choices

_wpq, mclock\_scheduler_

Default

_wpq_

**osd\_op\_queue\_cut\_off**

Description

これは、どの優先度のopsが通常のキューではなくstrictキューに送られるかを選択します。lowに設定すると、すべてのレプリケーションOPSとそれ以上のOPSがstrictキューに送信され、highオプションでは、レプリケーション確認応答OPSとそれ以上のOPSのみがstrictキューに送信されます。このオプションをhighに設定すると、クラスタ内のいくつかのOSDが非常に多忙な場合、特にosd\_op\_queue設定でwpqと組み合わせた場合に役立ちます。レプリケーショントラフィックの処理に非常に忙しいOSDでは、これらの設定がないと、これらのOSD上のプライマリクライアントトラフィックが枯渇する可能性があります。再起動が必要です。

Type

_String_

Valid Choices

_low, high_

Default

_high_

**osd\_client\_op\_priority**

Description

クライアント操作に設定される優先順位。 この値は、以下のosd\_recovery\_op\_priorityの値と相対的です。 デフォルトでは、回復よりもクライアントの操作を強く優先します。

Type

_32\-bit Integer_

Default

_63_

Valid Range

_1\-63_

**osd\_recovery\_op\_priority**

Description

プールのrecovery\_op\_priorityで指定されていない場合、回復操作とクライアント操作の優先順位を指定する。 デフォルト値では、クライアント操作（上記参照）が復旧操作よりも優先されます。 クライアント操作の優先順位を上げるためにこの値を下げたり、回復を優先するためにこの値を上げたりして、クラスタの健全性を回復する時間に対するクライアントの影響のトレードオフを調整することができます。

Type

_32\-bit Integer_

Default

_3_

Valid Range

_1\-63_

**osd\_scrub\_priority**

Description

プールでscrub\_priorityの値が指定されていない場合、スケジュールされたスクラブのデフォルトの作業キューの優先順位です。 これは、スクラブがクライアントの操作をブロックしている場合、osd\_client\_op\_priorityの値にまで高めることができます。

Type

_32\-bit Integer_

Default

_5_

Valid Range

_1\-63_

**osd\_requested\_scrub\_priority**

Description

作業キュー上でユーザーが要求したスクラブに設定される優先度。 この値がosd\_client\_op\_priorityよりも小さい場合、スクラブがクライアントの操作をブロックしているときに、osd\_client\_op\_priorityの値に引き上げられます。

Type

_32\-bit Integer_

Default

_120_

**osd\_snap\_trim\_priority**

Description

スナップトリムの作業キューに設定されている優先順位

Type

_32\-bit Integer_

Default

_5_

Valid Range

_1\-63_

**osd\_snap\_trim\_sleep**

Description

次のスナップトリム操作までのスリープ時間（秒）。 この値を大きくすると、スナップトリムが遅くなる。このオプションは、バックエンドに特有のバリアントよりも優先される。

Type

_Float_

Default

_0_

**osd\_snap\_trim\_sleep\_hdd**

Description

HDDの次のスナップトリム動作までのスリープ時間（秒）

Type

_Float_

Default

_5_

**osd\_snap\_trim\_sleep\_ssd**

Description

SSD OSD（NVMeを含む）の次のsnap trim opまでのスリープ時間（秒）

Type

_Float_

Default

_0_

**osd\_snap\_trim\_sleep\_hybrid**

Description

OSDデータがHDD上にあり、OSDジャーナルまたはWAL\+DBがSSD上にある場合に、次のsnap trim opまでにスリープする時間（秒）

Type

_Float_

Default

_2_

**osd\_op\_thread\_timeout**

Description

Ceph OSD Daemonの操作スレッドのタイムアウト（秒）

Type

_32\-bit Integer_

Default

_15_

**osd\_op\_complaint\_time**

Description

指定された秒数が経過すると、クレームに値する操作になる

Type

_Float_

Default

_30_

**osd\_op\_history\_size**

Description

追跡する完了オペレーションの最大数

Type

_32\-bit Unsigned Integer_

Default

_20_

**osd\_op\_history\_duration**

Description

追跡する最も古い完了した操作

Type

_32\-bit Unsigned Integer_

Default

_600_

**osd\_op\_log\_threshold**

Description

一度に表示する操作ログの数

Type

_32\-bit Integer_

Default

_5_

### QoS Based on mClock[¶](https://docs.ceph.com/en/pacific/rados/configuration/osd-config-ref/ "Permalink to this headline")

CephではmClockの使用がより洗練され、「 [mClock Config Reference](https://docs.ceph.com/en/pacific/rados/configuration/osd-config-ref/)」に記載されているような手順で使用できるようになりました。[56: mClock Config Reference](56-mClock_Config_Reference.md)

#### Core Concepts[¶](https://docs.ceph.com/en/pacific/rados/configuration/osd-config-ref/ "Permalink to this headline")

CephのQoSサポートは、dmClockアルゴリズムに基づくキューイングスケジューラを使用して実装されています。このアルゴリズムは、CephクラスタのI/Oリソースを重みに比例して割り当て、サービスがリソースを公平に獲得できるように、最小予約と最大制限の制約を適用します。現在、mclock\_scheduler操作キューは、I/Oリソースを伴うCephサービスを次のバケットに分けています。

* client op: クライアントが発行したIOPS
* osd subop: プライマリーOSDが発行したIOPS
* snap trim: スナップトリミング関連のリクエスト
* pg recovery: リカバリー関連のリクエスト
* pg scrub: スクラブ関連のリクエスト

そして、リソースは以下の3つのタグセットを使って分割されます。言い換えれば、各サービスのシェアは3つのタグで管理されています。

1. reservation: サービスに割り当てられる最小IOPS
2. limitation: サービスに割り当てられる最大IOPS
3. weight: 余分な容量やシステムがオーバースクライブした場合の容量の比例配分

Cephでは、オペレーションは「コスト」で評価されます。そして、さまざまなサービスを提供するために割り当てられたリソースは、この「コスト」によって消費されます。例えば、予約数が多いサービスほど、必要なだけのリソースを保有できることが保証されています。例えば、リカバリーサービスとクライアントオペレーションサービスの2つのサービスがあるとします。

* recovery: \(r:1, l:5, w:1\)
* client ops: \(r:2, l:0, w:9\)

上記の設定では、リカバリーが必要な場合でも、1秒間に5つ以上のリクエストを処理することはなく（下記の「現在の実装上の注意」を参照）、他のサービスが競合することもありません。しかし、クライアントが大量のI/Oリクエストを発行するようになっても、I/Oリソースをすべて使い果たすことはありません。リカバリジョブには、そのようなリクエストがある限り、常に1秒あたり1リクエストが割り当てられます。そのため、負荷の高いクラスターでも、リカバリージョブが飢えることはありません。一方、クライアントOSは、競合他社が「1」であるのに対し、ウェイトが「9」であるため、I/Oリソースのより大きな部分を享受することができます。クライアントOpsの場合、制限設定に縛られないため、リカバリーが行われていない場合は、すべてのリソースを使用することができます。

現在の実装上の注意：現在の実装では、制限値が強制的に設定されています。そのため、サービスが強制された制限値を超えた場合、制限値が回復するまでオペは操作キューに残ります。

#### Subtleties of mClock[¶](https://docs.ceph.com/en/pacific/rados/configuration/osd-config-ref/ "Permalink to this headline")

予約値と制限値には、1秒あたりのリクエスト数という単位があります。しかし、ウェイトは技術的には単位を持たず、ウェイトは互いに相対的なものです。つまり、あるクラスのリクエストのウェイトが1で、別のクラスのリクエストのウェイトが9の場合、後者のクラスのリクエストは、最初のクラスと9対1の割合で9回実行されるべきなのです。しかし、これは予約が満たされたときにのみ起こることで、その値には予約段階で実行された操作が含まれます。

ウェイトには単位がないとはいえ、アルゴリズムがリクエストにウェイトタグを割り当てる方法のため、その値の選択には注意が必要です。重みをWとすると、あるクラスのリクエストに対して、次に来たリクエストは、1/Wに前の重みタグまたは現在の時間を加えた値のうち、大きい方の重みタグを持つことになります。つまり、Wが十分に大きく、そのために1/Wが十分に小さい場合、計算されたタグは、現在の時間の値を取得するので、決して割り当てられないかもしれません。最終的な教訓は、ウェイトの値は大きすぎてはいけないということです。1 秒間に処理されると予想されるリクエストの数以下でなければなりません。

#### Caveats[¶](https://docs.ceph.com/en/pacific/rados/configuration/osd-config-ref/ "Permalink to this headline")

Ceph内のmClock opキューの影響を軽減できる要因がいくつかあります。まず、OSDへの要求は、その配置グループ識別子によってシャード化されます。各シャードには独自のmClockキューがあり、これらのキューは相互に作用したり、キュー間で情報を共有したりしません。シャードの数は、設定オプションのosd\_op\_num\_shards、osd\_op\_num\_shards\_hdd、osd\_op\_num\_shards\_ssdで制御できます。シャードの数を少なくすると、mClockキューの影響が大きくなりますが、他にも悪影響を及ぼす可能性があります。

次に、リクエストはオペレーション・キューからオペレーション・シーケンサに転送され、そこで実行のフェーズを経ることになります。オペレーション・キューにはmClockが常駐し、mClockはオペレーション・シーケンサに転送する次のオペを決定します。オペレーション・シーケンサで許可されるオペレーションの数は複雑な問題です。一般的には、シーケンサに十分な数のオペレーションを残しておきたいので、ディスクやネットワークへのアクセスが完了するまで他のオペレーションを待っている間にも、あるオペレーションでは常に作業が行われています。一方で、あるオペレーションがオペレーションシーケンサに転送されると、mClockはそのオペレーションをコントロールできなくなります。そのため、mClockの効果を最大限に発揮させるためには、オペレートシーケンサにあるオペレーションをできるだけ少なくしたいと考えます。このように、私たちには本質的な緊張関係があります。

オペレーション・シーケンサ内のオペレーション数に影響を与える設定オプションは、bluestore\_throttle\_bytes、bluestore\_throttle\_deferred\_bytes、bluestore\_throttle\_cost\_per\_io、bluestore\_throttle\_cost\_per\_io\_hdd、bluestore\_throttle\_cost\_per\_io\_ssdです。

mClockアルゴリズムの影響に影響を与える3つ目の要因は、我々は分散型システムを使用しており、リクエストは複数のOSDに対して行われ、各OSDは複数のシャードを持っている（持つことができる）ということです。しかし、私たちは現在、分散型ではないmClockアルゴリズムを使用しています（注：dmClockはmClockの分散型バージョンです）。

現在、様々な組織や個人が、このコードベースに存在するmClockと、その修正版を使って実験を行っています。mClockとdmClockの実験の経験をceph\-develメーリングリストで共有していただけると幸いです。

**osd\_push\_per\_object\_cost**

Description

プッシュ操作を行うためのオーバーヘッド

Type

_Unsigned Integer_

Default

_1000_

**osd\_recovery\_max\_chunk**

Description

リカバリ操作が運ぶことのできるデータチャンクの最大合計サイズ

Type

_Unsigned Integer_

Default

_8 MiB_

**osd\_mclock\_scheduler\_client\_res**

Description

各クライアントに予約されているIO比率（デフォルト）

Type

_Unsigned Integer_

Default

_1_

**osd\_mclock\_scheduler\_client\_wgt**

Description

各予約超過クライアントのIOシェア\(デフォルト\)

Type

_Unsigned Integer_

Default

_1_

**osd\_mclock\_scheduler\_client\_lim**

Description

各予約超過クライアントのIO制限\(デフォルト\)

Type

_Unsigned Integer_

Default

_999999_

**osd\_mclock\_scheduler\_background\_recovery\_res**

Description

バックグラウンドリカバリ用に予約されたIO比率（デフォルト）

Type

_Unsigned Integer_

Default

_1_

**osd\_mclock\_scheduler\_background\_recovery\_wgt**

Description

予約超過バックグラウンドのリカバリのためのIOシェア

Type

_Unsigned Integer_

Default

_1_

**osd\_mclock\_scheduler\_background\_recovery\_lim**

Description

予約超過バックグラウンドリカバリのIO制限

Type

_Unsigned Integer_

Default

_999999_

**osd\_mclock\_scheduler\_background\_best\_effort\_res**

Description

バックグラウンド best\_effortに予約されたIO比率（デフォルト）

Type

_Unsigned Integer_

Default

_1_

**osd\_mclock\_scheduler\_background\_best\_effort\_wgt**

Description

予約超過バックグラウンド best\_effort のIOシェア

Type

_Unsigned Integer_

Default

_1_

**osd\_mclock\_scheduler\_background\_best\_effort\_lim**

Description

予約超過バックグラウンド best\_effort のIO制限

Type

_Unsigned Integer_

Default

_999999_

## Backfilling[¶](https://docs.ceph.com/en/pacific/rados/configuration/osd-config-ref/ "Permalink to this headline")

クラスタにCeph OSDデーモンを追加または削除すると、CRUSHは配置グループをCeph OSDに移動またはCeph OSDから移動させてクラスタのバランスを取り直し、バランスの取れた使用率を回復します。配置グループとそれに含まれるオブジェクトを移行するプロセスは、クラスタの運用パフォーマンスを大幅に低下させる可能性があります。運用パフォーマンスを維持するために、Cephはこの移行を「バックフィル」で実行します。これにより、Cephはバックフィル操作の優先度をデータの読み取りまたは書き込みの要求よりも低く設定できます。

**osd\_max\_backfills**

Description

1つのOSDに対して、または1つのOSDから許可されるバックフィルの最大数。この値は、read操作とwrite操作に別々に適用されることに注意

Type

_64\-bit Unsigned Integer_

Default

_1_

**osd\_backfill\_scan\_min**

Description

バックフィルスキャン1回あたりのオブジェクトの最小数

Type

_32\-bit Integer_

Default

_64_

**osd\_backfill\_scan\_max**

Description

バックフィルスキャン1回あたりのオブジェクトの最大数

Type

_32\-bit Integer_

Default

_512_

**osd\_backfill\_retry\_interval**

Description

バックフィル要求を再試行するまでの待ち時間（秒）

Type

_Double_

Default

_10.0_

## OSD Map[¶](https://docs.ceph.com/en/pacific/rados/configuration/osd-config-ref/ "Permalink to this headline")

OSDマップは、クラスタ内で動作しているOSDデーモンを反映しています。時間の経過とともに、マップエポックの数が増えていきます。Cephでは、OSDマップが大きくなってもCephがうまく動作するように、いくつかの設定が用意されています。

**osd\_map\_dedup**

Description

OSDマップの重複削除を有効にする

Type

_Boolean_

Default

_true_

**osd\_map\_cache\_size**

Description

キャッシュしておくOSDマップの数

Type

_32\-bit Integer_

Default

_50_

**osd\_map\_message\_max**

Description

MOSDMapメッセージごとに許可される最大マップ・エントリ数

Type

_32\-bit Integer_

Default

_40_

## Recovery[¶](https://docs.ceph.com/en/pacific/rados/configuration/osd-config-ref/ "Permalink to this headline")

クラスタが起動したとき、またはCeph OSDデーモンがクラッシュして再起動したとき、OSDは書き込みが発生する前に他のCeph OSDデーモンとのピアリングを開始します。 詳細は「OSDとPGの監視」を参照してください。[67: Monitoring OSDs and PGs](67-Monitoring_OSDs_and_PGs.md)

Ceph OSDデーモンがクラッシュしてオンラインに戻ると、通常は、配置グループのオブジェクトのより新しいバージョンを含む他のCeph OSDデーモンと同期が取れなくなります。このような状況になると、Ceph OSDデーモンはリカバリモードに入り、データの最新コピーを取得してマップを最新の状態に戻そうとします。Ceph OSDデーモンがダウンしていた時間によっては、OSDのオブジェクトと配置グループが大幅に古くなっている可能性があります。また、障害ドメイン\(ラックなど\)がダウンした場合、複数のCeph OSDデーモンが同時にオンラインになることがあります。そのため、復旧作業に時間がかかり、リソースが不足する可能性があります。

運用パフォーマンスを維持するために、Cephはリカバリ要求数、スレッド数、オブジェクトチャンクサイズを制限してリカバリを実行し、これによりCephは劣化状態でも十分なパフォーマンスを発揮します。

**osd\_recovery\_delay\_start**

Description

ピアリングが完了すると、Cephは指定した秒数だけ遅延してからRADOSオブジェクトの回復を開始する

Type

_Float_

Default

_0_

**osd\_recovery\_max\_active**

Description

一度にOSDあたりのアクティブなリカバリ要求の数。要求数が多いほどリカバリは早くなるが、その分クラスターへの負荷が大きくなる。

この値は、0でない場合にのみ使用される。通常は0で、OSDをバックアップするプライマリデバイスのタイプに応じて、hddまたはssdの値（以下）が使用される

Type

_32\-bit Integer_

Default

_0_

**osd\_recovery\_max\_active\_hdd**

Description

プライマリデバイスが回転式の場合、1回のOSDあたりのアクティブなリカバリ要求の数

Type

_32\-bit Integer_

Default

_3_

**osd\_recovery\_max\_active\_ssd**

Description

プライマリデバイスが非回転型（SSDなど）の場合、1回のOSDあたりのアクティブなリカバリ要求の数

Type

_32\-bit Integer_

Default

_10_

**osd\_recovery\_max\_chunk**

Description

復元したデータのチャンクをプッシュする際の最大サイズ

Type

_64\-bit Unsigned Integer_

Default

_8\<\<20_

**osd\_recovery\_max\_single\_start**

Description

OSDがリカバリする際に新たに開始される、OSDごとのリカバリ操作の最大数

Type

_64\-bit Unsigned Integer_

Default

_1_

**osd\_recovery\_thread\_timeout**

Description

リカバリ・スレッドがタイムアウトするまでの最大時間（秒）

Type

_32\-bit Integer_

Default

_30_

**osd\_recover\_clone\_overlap**

Description

リカバリ時にクローンのオーバーラップを維持する。常にtrueに設定する必要がある

Type

_Boolean_

Default

_true_

**osd\_recovery\_sleep**

Description

次のリカバリーまたはバックフィルの動作までのスリープ時間（秒）。この値を大きくすると、クライアントの操作への影響が少なくなる一方で、リカバリーの動作が遅くなる

Type

_Float_

Default

_0_

**osd\_recovery\_sleep\_hdd**

Description

HDDの次のリカバリまたはバックフィル操作までのスリープ時間（秒）

Type

_Float_

Default

_0.1_

**osd\_recovery\_sleep\_ssd**

Description

SSDの次のリカバリまたはバックフィル操作までのスリープ時間（秒）

Type

_Float_

Default

_0_

**osd\_recovery\_sleep\_hybrid**

Description

OSDデータがHDD上にあり、OSDジャーナル/WAL\+DBがSSD上にある場合、次のリカバリまたはバックフィル操作までにスリープする時間（秒）

Type

_Float_

Default

_0.025_

**osd\_recovery\_priority**

Description

リカバリ作業キューに設定されているデフォルトの優先順位。 プールのrecovery\_priorityとは関係ない

Type

_32\-bit Integer_

Default

_5_

## Tiering[¶](https://docs.ceph.com/en/pacific/rados/configuration/osd-config-ref/ "Permalink to this headline")

**osd\_agent\_max\_ops**

Description

高速モードでのティアリングエージェントあたりの最大同時フラッシュ操作数

Type

_32\-bit Integer_

Default

_4_

**osd\_agent\_max\_low\_ops**

Description

低速モードでのティアリングエージェントあたりの最大同時フラッシュ操作数

Type

_32\-bit Integer_

Default

_2_

高速モード内でティアリングエージェントがダーティオブジェクトをフラッシュするタイミングについては、「[cache target dirty high ratio](https://docs.ceph.com/en/pacific/rados/configuration/osd-config-ref/) 」を参照してください。[71: Pools](71-Pools.md)

## Miscellaneous[¶](https://docs.ceph.com/en/pacific/rados/configuration/osd-config-ref/ "Permalink to this headline")

**osd\_snap\_trim\_thread\_timeout**

Description

スナップ・トリム・スレッドがタイムアウトするまでの最大時間（秒）

Type

_32\-bit Integer_

Default

_1\*60\*60_

**osd\_backlog\_thread\_timeout**

Description

バックログ・スレッドがタイムアウトするまでの最大時間（秒）

Type

_32\-bit Integer_

Default

_1\*60\*60_

**osd\_default\_notify\_timeout**

Description

OSD通知タイムアウトのデフォルト（秒）

Type

_32\-bit Unsigned Integer_

Default

_30_

**osd\_check\_for\_log\_corruption**

Description

ログファイルが破損していないかチェックする。計算量が多くなる可能性がある

Type

_Boolean_

Default

_false_

**osd\_remove\_thread\_timeout**

Description

Remove OSDスレッドがタイムアウトするまでの最大時間（秒）

Type

_32\-bit Integer_

Default

_60\*60_

**osd\_command\_thread\_timeout**

Description

コマンドスレッドがタイムアウトするまでの最大時間（秒）

Type

_32\-bit Integer_

Default

_10\*60_

**osd\_delete\_sleep**

Description

次の削除処理までのスリープ時間（秒）。これは、PG削除処理をスロットルする

Type

_Float_

Default

_0_

**osd\_delete\_sleep\_hdd**

Description

HDDの次の取り外しトランザクションまでのスリープ時間（秒）

Type

_Float_

Default

_5_

**osd\_delete\_sleep\_ssd**

Description

SSDの次の取り外しトランザクションまでのスリープ時間（秒）

Type

_Float_

Default

_0_

**osd\_delete\_sleep\_hybrid**

Description

OSDデータがHDD上にあり、OSDジャーナルまたはWAL\+DBがSSD上にある場合に、次の削除トランザクションまでにスリープする時間（秒）

Type

_Float_

Default

_1_

**osd\_command\_max\_records**

Description

ロスト・オブジェクトの返却数制限

Type

_32\-bit Integer_

Default

_256_

**osd\_fast\_fail\_on\_connection\_refused**

Description

このオプションを有効にすると、クラッシュしたOSDは、接続されているピアやMONによって直ちに「down」マークされる（クラッシュしたOSDホストは生存していることを想定）。このオプションを無効にすると、以前の動作に戻るが、I/O操作の途中でOSDがクラッシュすると、I/Oが長時間停止する可能性がある

Type

_Boolean_

Default

_true_
