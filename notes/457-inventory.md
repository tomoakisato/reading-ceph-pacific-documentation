# 457: inventory

**クリップソース:** [457: inventory — Ceph Documentation](https://docs.ceph.com/en/pacific/ceph-volume/zfs/inventory/)

# inventory[¶](https://docs.ceph.com/en/pacific/ceph-volume/zfs/inventory/ "Permalink to this headline")

inventory サブコマンドは、GEOM を介してホストのディスクインベントリを照会し、各物理デバイスのハードウェア情報とメタデータを提供します。

これはFreeBSDプラットフォームでのみ動作します。

デフォルトでは、このコマンドは、すべての物理ディスクの人間が読むことのできるレポートを返します。

このレポートをプログラムで利用する場合は、\-\-formatjsonを渡すと、JSON形式のレポートが生成されます。このレポートには、ディスクのメタデータ\(モデルやサイズなど\)、論理ボリュームとそれらがcephで使用されているかどうか、ディスクがcephで使用可能かどうか、使用できない理由など、物理ドライブに関する広範な情報が含まれます。

デバイスパスを指定することで、デバイスに関する広範な情報をプレーンとjsonの両方のフォーマットで報告することができます。
