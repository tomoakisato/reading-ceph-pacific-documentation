# 10: Downloading a Ceph Release Tarball

**クリップソース:** [10: Downloading a Ceph Release Tarball — Ceph Documentation](https://docs.ceph.com/en/pacific/install/get-tarballs/)

# Downloading a Ceph Release Tarball¶

Cephの開発が進むにつれて、Cephチームはソースコードの新バージョンをリリースします。Cephリリースのソースコードtarボールはこちらからダウンロードできます。

[Ceph Release Tarballs](https://download.ceph.com/tarballs/)

Tip

海外のお客様へ。お近くにCephをダウンロードできるミラーサイトがあるかもしれません。詳細については以下を参照してください：[Cephのミラー](https://docs.ceph.com/en/pacific/install/mirrors)
