# 250: Container Operations

 # Container Operations[¶](https://docs.ceph.com/en/pacific/radosgw/swift/containerops/ "Permalink to this headline")

A container is a mechanism for storing data objects. An account may have many containers, but container names must be unique. This API enables a client to create a container, set access controls and metadata, retrieve a container’s contents, and delete a container. Since this API makes requests related to information in a particular user’s account, all requests in this API must be authenticated unless a container’s access control is deliberately made publicly accessible \(i.e., allows anonymous requests\).

Note:

The Amazon S3 API uses the term ‘bucket’ to describe a data container. When you hear someone refer to a ‘bucket’ within the Swift API, the term ‘bucket’ may be construed as the equivalent of the term ‘container.’

One facet of object storage is that it does not support hierarchical paths or directories. Instead, it supports one level consisting of one or more containers, where each container may have objects. The RADOS Gateway’s Swift\-compatible API supports the notion of ‘pseudo\-hierarchical containers,’ which is a means of using object naming to emulate a container \(or directory\) hierarchy without actually implementing one in the storage system. You may name objects with pseudo\-hierarchical names \(e.g., photos/buildings/empire\-state.jpg\), but container names cannot contain a forward slash \(`/`\) character.

## Create a Container[¶](https://docs.ceph.com/en/pacific/radosgw/swift/containerops/ "Permalink to this headline")

To create a new container, make a `PUT` request with the API version, account, and the name of the new container. The container name must be unique, must not contain a forward\-slash \(/\) character, and should be less than 256 bytes. You may include access control headers and metadata headers in the request. The operation is idempotent; that is, if you make a request to create a container that already exists, it will return with a HTTP 202 return code, but will not create another container.

### Syntax[¶](https://docs.ceph.com/en/pacific/radosgw/swift/containerops/ "Permalink to this headline")

```
PUT /{api version}/{account}/{container} HTTP/1.1
Host: {fqdn}
X-Auth-Token: {auth-token}
X-Container-Read: {comma-separated-uids}
X-Container-Write: {comma-separated-uids}
X-Container-Meta-{key}: {value}
```

### Headers[¶](https://docs.ceph.com/en/pacific/radosgw/swift/containerops/ "Permalink to this headline")

`X-Container-Read`

DescriptionThe user IDs with read permissions for the container.

TypeComma\-separated string values of user IDs.

RequiredNo

`X-Container-Write`

DescriptionThe user IDs with write permissions for the container.

TypeComma\-separated string values of user IDs.

RequiredNo

`X-Container-Meta-{key}`

DescriptionA user\-defined meta data key that takes an arbitrary string value.

TypeString

RequiredNo

### HTTP Response[¶](https://docs.ceph.com/en/pacific/radosgw/swift/containerops/ "Permalink to this headline")

If a container with the same name already exists, and the user is the container owner then the operation will succeed. Otherwise the operation will fail.

`409`

DescriptionThe container already exists under a different user’s ownership.

Status Code`BucketAlreadyExists`

## List a Container’s Objects[¶](https://docs.ceph.com/en/pacific/radosgw/swift/containerops/ "Permalink to this headline")

To list the objects within a container, make a `GET` request with the with the API version, account, and the name of the container. You can specify query parameters to filter the full list, or leave out the parameters to return a list of the first 10,000 object names stored in the container.

### Syntax[¶](https://docs.ceph.com/en/pacific/radosgw/swift/containerops/ "Permalink to this headline")

```
GET /{api version}/{container} HTTP/1.1
     Host: {fqdn}
     X-Auth-Token: {auth-token}
```

### Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/swift/containerops/ "Permalink to this headline")

`format`

DescriptionDefines the format of the result.

TypeString

Valid Values`json` | `xml`

RequiredNo

`prefix`

DescriptionLimits the result set to objects beginning with the specified prefix.

TypeString

RequiredNo

`marker`

DescriptionReturns a list of results greater than the marker value.

TypeString

RequiredNo

`limit`

DescriptionLimits the number of results to the specified value.

TypeInteger

Valid Range0 \- 10,000

RequiredNo

`delimiter`

DescriptionThe delimiter between the prefix and the rest of the object name.

TypeString

RequiredNo

`path`

DescriptionThe pseudo\-hierarchical path of the objects.

TypeString

RequiredNo

`allow_unordered`

DescriptionAllows the results to be returned unordered to reduce computation overhead. Cannot be used with `delimiter`.

TypeBoolean

RequiredNo

Non\-Standard ExtensionYes

### Response Entities[¶](https://docs.ceph.com/en/pacific/radosgw/swift/containerops/ "Permalink to this headline")

`container`

DescriptionThe container.

TypeContainer

`object`

DescriptionAn object within the container.

TypeContainer

`name`

DescriptionThe name of an object within the container.

TypeString

`hash`

DescriptionA hash code of the object’s contents.

TypeString

`last_modified`

DescriptionThe last time the object’s contents were modified.

TypeDate

`content_type`

DescriptionThe type of content within the object.

TypeString

## Update a Container’s ACLs[¶](https://docs.ceph.com/en/pacific/radosgw/swift/containerops/ "Permalink to this headline")

When a user creates a container, the user has read and write access to the container by default. To allow other users to read a container’s contents or write to a container, you must specifically enable the user. You may also specify `*` in the `X-Container-Read` or `X-Container-Write`settings, which effectively enables all users to either read from or write to the container. Setting `*` makes the container public. That is it enables anonymous users to either read from or write to the container.

Note:

If you are planning to expose public read ACL functionality for the Swift API, it is strongly recommended to include the Swift account name in the endpoint definition, so as to most closely emulate the behavior of native OpenStack Swift. To do so, set the `ceph.conf` configuration option `rgw
swift account in url = true`, and update your Keystone endpoint to the URL suffix `/v1/AUTH_%(tenant_id)s`\(instead of just `/v1`\).

### Syntax[¶](https://docs.ceph.com/en/pacific/radosgw/swift/containerops/ "Permalink to this headline")

```
POST /{api version}/{account}/{container} HTTP/1.1
Host: {fqdn}
     X-Auth-Token: {auth-token}
     X-Container-Read: *
     X-Container-Write: {uid1}, {uid2}, {uid3}
```

### Request Headers[¶](https://docs.ceph.com/en/pacific/radosgw/swift/containerops/ "Permalink to this headline")

`X-Container-Read`

DescriptionThe user IDs with read permissions for the container.

TypeComma\-separated string values of user IDs.

RequiredNo

`X-Container-Write`

DescriptionThe user IDs with write permissions for the container.

TypeComma\-separated string values of user IDs.

RequiredNo

## Add/Update Container Metadata[¶](https://docs.ceph.com/en/pacific/radosgw/swift/containerops/ "Permalink to this headline")

To add metadata to a container, make a `POST` request with the API version, account, and container name. You must have write permissions on the container to add or update metadata.

### Syntax[¶](https://docs.ceph.com/en/pacific/radosgw/swift/containerops/ "Permalink to this headline")

```
POST /{api version}/{account}/{container} HTTP/1.1
Host: {fqdn}
     X-Auth-Token: {auth-token}
     X-Container-Meta-Color: red
     X-Container-Meta-Taste: salty
```

### Request Headers[¶](https://docs.ceph.com/en/pacific/radosgw/swift/containerops/ "Permalink to this headline")

`X-Container-Meta-{key}`

DescriptionA user\-defined meta data key that takes an arbitrary string value.

TypeString

RequiredNo

## Enable Object Versioning for a Container[¶](https://docs.ceph.com/en/pacific/radosgw/swift/containerops/ "Permalink to this headline")

To enable object versioning a container, make a `POST` request with the API version, account, and container name. You must have write permissions on the container to add or update metadata.

Note:

Object versioning support is not enabled in radosgw by default; you must set `rgw swift versioning enabled =
true` in `ceph.conf` to enable this feature.

### Syntax[¶](https://docs.ceph.com/en/pacific/radosgw/swift/containerops/ "Permalink to this headline")

```
POST /{api version}/{account}/{container} HTTP/1.1
Host: {fqdn}
     X-Auth-Token: {auth-token}
     X-Versions-Location: {archive-container}
```

### Request Headers[¶](https://docs.ceph.com/en/pacific/radosgw/swift/containerops/ "Permalink to this headline")

`X-Versions-Location`

DescriptionThe name of a container \(the “archive container”\) that will be used to store versions of the objects in the container that the `POST` request is made on \(the “current container”\). The archive container need not exist at the time it is being referenced, but once`X-Versions-Location` is set on the current container, and object versioning is thus enabled, the archive container must exist before any further objects are updated or deleted in the current container.

Note:

`X-Versions-Location` is the only versioning\-related header that radosgw interprets. `X-History-Location`, supported by native OpenStack Swift, is currently not supported by radosgw.

TypeString

RequiredNo \(if this header is passed with an empty value, object versioning on the current container is disabled, but the archive container continues to exist.\)

## Delete a Container[¶](https://docs.ceph.com/en/pacific/radosgw/swift/containerops/ "Permalink to this headline")

To delete a container, make a `DELETE` request with the API version, account, and the name of the container. The container must be empty. If you’d like to check if the container is empty, execute a `HEAD` request against the container. Once you have successfully removed the container, you will be able to reuse the container name.

### Syntax[¶](https://docs.ceph.com/en/pacific/radosgw/swift/containerops/ "Permalink to this headline")

```
DELETE /{api version}/{account}/{container} HTTP/1.1
Host: {fqdn}
X-Auth-Token: {auth-token}
```

### HTTP Response[¶](https://docs.ceph.com/en/pacific/radosgw/swift/containerops/ "Permalink to this headline")

`204`

DescriptionThe container was removed.

Status Code`NoContent`
