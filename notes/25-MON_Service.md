# 25: MON Service

**クリップソース:** [25: MON Service — Ceph Documentation](https://docs.ceph.com/en/pacific/cephadm/services/mon/)

# MON Service[¶](https://docs.ceph.com/en/pacific/cephadm/services/mon/ "Permalink to this headline")

## Deploying additional monitors[¶](https://docs.ceph.com/en/pacific/cephadm/services/mon/ "Permalink to this headline")

一般的なCephクラスターでは、3～5台のモニターデーモンが異なるホストに分散しています。 クラスタに5つ以上のノードがある場合は、5つのモニタを展開することをお勧めします。

Cephは、クラスタが大きくなると自動的にモニタデーモンを展開し、クラスタが縮小すると自動的にモニタデーモンを縮小します。この自動成長と縮小をスムーズに実行するには、適切なサブネット設定が必要です。

cephadmのブートストラップ手順では、クラスタ内の最初のモニタデーモンを特定のサブネットに割り当てます。cephadmはそのサブネットをクラスタのデフォルトサブネットとして指定します。新しいモニタデーモンは、cephadmが別の方法を指示しない限り、デフォルトでそのサブネットに割り当てられます。

クラスタ内のすべてのcephモニタデーモンが同じサブネットにある場合、cephモニタデーモンを手動で管理する必要はありません。cephadmは、クラスタに新しいホストが追加されると、必要に応じて最大5つのモニタをサブネットに自動的に追加します。

デフォルトでは、cephadmは任意のホストに5つのデーモンを配置します。デーモンの配置の指定の詳細については、「 [Daemon Placement](https://docs.ceph.com/en/pacific/cephadm/services/mon/) 」を参照してください。

### Designating a Particular Subnet for Monitors[¶](https://docs.ceph.com/en/pacific/cephadm/services/mon/ "Permalink to this headline")

cephモニタデーモンが使用する特定のIPサブネットを指定するには、次の形式のコマンドを使用し、 [CIDR](https://docs.ceph.com/en/pacific/cephadm/services/mon/) 形式のサブネットのアドレスを含めます\(例: 10.1.2.0/24\)。

```
# ceph config set mon public_network *<mon-cidr-network>*
```

For example:

```
# ceph config set mon public_network 10.1.2.0/24
```

Cephadmは、指定されたサブネットにIPアドレスを持つホスト上でのみ新しいモニタデーモンを展開します。

ネットワークのリストを使って2つのパブリックネットワークを指定することもできます。

```
# ceph config set mon public_network *<mon-cidr-network1>,<mon-cidr-network2>*
```

For example:

```
# ceph config set mon public_network 10.1.2.0/24,192.168.0.1/24
```

### Deploying Monitors on a Particular Network[¶](https://docs.ceph.com/en/pacific/cephadm/services/mon/ "Permalink to this headline")

各モニターのIPアドレスやCIDRネットワークを明示的に指定し、各モニターの配置場所を制御することができます。 モニターの自動配置を無効にするには、このコマンドを実行します。

```
# ceph orch apply mon --unmanaged
```

追加のモニターをそれぞれ展開するには：

```
# ceph orch daemon add mon *<host1:ip-or-network1>
```

例えば、IPアドレス10.1.2.123のnewhost1に2台目のモニターを配置し、ネットワーク10.1.2.0/24のnewhost2に3台目のモニターを配置する場合、以下のコマンドを実行します。

```
# ceph orch apply mon --unmanaged
# ceph orch daemon add mon newhost1:10.1.2.123
# ceph orch daemon add mon newhost2:10.1.2.0/24
```

ここで、デーモンの自動配置を有効にします。

```
# ceph orch apply mon --placement="newhost1,newhost2,newhost3" --dry-run
```

デーモンの配置を指定する方法については、「[Daemon Placement](https://docs.ceph.com/en/pacific/cephadm/services/mon/)」を参照してください。

最後に、\-\-dry\-runを外して、この新しい配置を適用します。

```
# ceph orch apply mon --placement="newhost1,newhost2,newhost3"
```

### Moving Monitors to a Different Network[¶](https://docs.ceph.com/en/pacific/cephadm/services/mon/ "Permalink to this headline")

モニターを新しいネットワークに移動するには、新しいネットワークに新しいモニターを配置し、その後、古いネットワークからモニターを削除します。手動でモンマップを修正して注入\(inject\)することはお勧めできません。

まず、デーモンの自動配置を無効にします。

```
# ceph orch apply mon --unmanaged
```

追加のモニターをそれぞれ展開するために：

```
# ceph orch daemon add mon *<newhost1:ip-or-network1>*
```

例えば、IPアドレス10.1.2.123のnewhost1に2台目のモニターを配置し、ネットワーク10.1.2.0/24のnewhost2に3台目のモニターを配置する場合、以下のコマンドを実行します。

```
# ceph orch apply mon --unmanaged
# ceph orch daemon add mon newhost1:10.1.2.123
# ceph orch daemon add mon newhost2:10.1.2.0/24
```

その後、古いネットワークからモニターを削除します。

```
# ceph orch daemon rm *mon.<oldhost1>*
```

public\_networkを更新する。

```
# ceph config set mon public_network *<mon-cidr-network>*
```

For example:

```
# ceph config set mon public_network 10.1.2.0/24
```

ここで、デーモンの自動配置を有効にします。

```
# ceph orch apply mon --placement="newhost1,newhost2,newhost3" --dry-run
```

デーモンの配置を指定する詳細については、「[Daemon Placement](https://docs.ceph.com/en/pacific/cephadm/services/mon/) 」を参照してください。

最後に、\-\-dry\-runを外して、この新しい配置を適用します。

```
# ceph orch apply mon --placement="newhost1,newhost2,newhost3"
```

## Futher Reading[¶](https://docs.ceph.com/en/pacific/cephadm/services/mon/ "Permalink to this headline")

* [Cluster Operations](https://docs.ceph.com/en/pacific/cephadm/services/mon/)
* [Troubleshooting Monitors](https://docs.ceph.com/en/pacific/cephadm/services/mon/)
* [Restoring the MON quorum](https://docs.ceph.com/en/pacific/cephadm/services/mon/)
