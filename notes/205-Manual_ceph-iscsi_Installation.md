# 205: Manual ceph-iscsi Installation

**クリップソース:** [205: Manual ceph\-iscsi Installation — Ceph Documentation](https://docs.ceph.com/en/pacific/rbd/iscsi-target-cli-manual-install/)

# Manual ceph\-iscsi Installation[¶](https://docs.ceph.com/en/pacific/rbd/iscsi-target-cli-manual-install/ "Permalink to this headline")

**Requirements**

ceph\-iscsiのインストールは4つのステップがあります：

1. Linuxディストリビューションのソフトウェアリポジトリから共通パッケージをインストールする
2. Git をインストールして、残りのパッケージを Git リポジトリから取得する
3. 互換性のあるカーネルを使用していることを確認する
4. tcmu\-runner
    rtslib\-fb
    configshell\-fb
    targetcli\-fb
    ceph\-iscsi
    ceph\-iscsiのすべてのコンポーネントをインストールし、関連デーモンを起動する

## 1. Install Common Packages[¶](https://docs.ceph.com/en/pacific/rbd/iscsi-target-cli-manual-install/ "Permalink to this headline")

ceph\-iscsiとターゲットツールは、以下のパッケージを使用します。これらはiSCSIゲートウェイとなる各マシンにLinuxディストリビューションのソフトウェアリポジトリからインストールする必要があります：

* libnl3
* libkmod
* librbd1
* pyparsing
* python kmod
* python pyudev
* python gobject
* python urwid
* python pyparsing
* python rados
* python rbd
* python netifaces
* python crypto
* python requests
* python flask
* pyOpenSSL

## 2. Install Git[¶](https://docs.ceph.com/en/pacific/rbd/iscsi-target-cli-manual-install/ "Permalink to this headline")

CephでiSCSIを実行するために必要なすべてのパッケージをインストールするためには、Gitを使用してリポジトリからダウンロードする必要があります。CentOS/RHELの場合：

```
> sudo yum install git
```

Debian/Ubuntuの場合：

```
> sudo apt install git
```

Gitとその仕組みについてもっと知りたい方は、[https://git\-scm.com](https://git-scm.com/) をご覧ください。

## 3. Ensure a compatible kernel is used[¶](https://docs.ceph.com/en/pacific/rbd/iscsi-target-cli-manual-install/ "Permalink to this headline")

必要なCeph iSCSIパッチを含むサポートされているカーネルを使用していることを確認します：

* カーネル v4.16 以降のすべての Linux ディストリビューション、または
* Red Hat Enterprise Linux または CentOS 7.5 以降 \(これらのディストリビューションでは ceph\-iscsi のサポートがバックポートされています\)

すでに互換性のあるカーネルを使用している場合は、次のステップに進むことができます。しかし、互換性のあるカーネルを使用していない場合、このカーネルをどのように構築するかについて、お使いのディストロのドキュメントを確認してください。Ceph iSCSI固有の要件は、以下のビルドオプションを有効にすることのみです：

```
CONFIG_TARGET_CORE=m
CONFIG_TCM_USER2=m
CONFIG_ISCSI_TARGET=m
```

## 4. Install ceph\-iscsi[¶](https://docs.ceph.com/en/pacific/rbd/iscsi-target-cli-manual-install/ "Permalink to this headline")

最後に、残りのツールは、そのGitリポジトリと関連するサービスから直接取得することができます。

### tcmu\-runner[¶](https://docs.ceph.com/en/pacific/rbd/iscsi-target-cli-manual-install/ "Permalink to this headline")

Installation:

```
> git clone https://github.com/open-iscsi/tcmu-runner
> cd tcmu-runner
```

以下のコマンドを実行し、必要な依存関係をすべてインストールします：

```
> ./extra/install_dep.sh
```

これで、tcmu\-runnerをビルドすることができます。そのためには、以下のビルドコマンドを使用します：

```
> cmake -Dwith-glfs=false -Dwith-qcow=false -DSUPPORT_SYSTEMD=ON -DCMAKE_INSTALL_PREFIX=/usr
> make install
```

デーモンを有効にして起動します：

```
> systemctl daemon-reload
> systemctl enable tcmu-runner
> systemctl start tcmu-runner
```

### rtslib\-fb[¶](https://docs.ceph.com/en/pacific/rbd/iscsi-target-cli-manual-install/ "Permalink to this headline")

Installation:

```
> git clone https://github.com/open-iscsi/rtslib-fb.git
> cd rtslib-fb
> python setup.py install
```

### configshell\-fb[¶](https://docs.ceph.com/en/pacific/rbd/iscsi-target-cli-manual-install/ "Permalink to this headline")

Installation:

```
> git clone https://github.com/open-iscsi/configshell-fb.git
> cd configshell-fb
> python setup.py install
```

### targetcli\-fb[¶](https://docs.ceph.com/en/pacific/rbd/iscsi-target-cli-manual-install/ "Permalink to this headline")

Installation:

```
> git clone https://github.com/open-iscsi/targetcli-fb.git
> cd targetcli-fb
> python setup.py install
> mkdir /etc/target
> mkdir /var/target
```

警告： ceph\-iscsiツールは、システム上の全てのターゲットを管理していると仮定しています。ターゲットがセットアップされ、targetcli によって管理されている場合、ターゲットサービスを無効にする必要があります。

### ceph\-iscsi[¶](https://docs.ceph.com/en/pacific/rbd/iscsi-target-cli-manual-install/ "Permalink to this headline")

Installation:

```
> git clone https://github.com/ceph/ceph-iscsi.git
> cd ceph-iscsi
> python setup.py install --install-scripts=/usr/bin
> cp usr/lib/systemd/system/rbd-target-gw.service /lib/systemd/system
> cp usr/lib/systemd/system/rbd-target-api.service /lib/systemd/system
```

デーモンを有効にして起動します：

```
> systemctl daemon-reload
> systemctl enable rbd-target-gw
> systemctl start rbd-target-gw
> systemctl enable rbd-target-api
> systemctl start rbd-target-api
```

インストールが完了しました。[ceph\-iscsi CLI のメインページ](https://docs.ceph.com/en/pacific/rbd/iscsi-target-cli)のセットアップのセクションに進みます。
