# 141: Quotas

**クリップソース:** [141: Quotas — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/quota/)

# Quotas[¶](https://docs.ceph.com/en/pacific/cephfs/quota/ "Permalink to this headline")

CephFSでは、システム内の任意のディレクトリにクォータを設定することができます。 クォータは、ディレクトリ階層内のそのポイント以下に格納されるバイト数またはファイル数を制限できます。

## Limitations[¶](https://docs.ceph.com/en/pacific/cephfs/quota/ "Permalink to this headline")

1. _クオータは協調的であり、敵対的ではありません。CephFSのクォータは、ファイルシステムをマウントしているクライアントの協力に依存し、上限に達したときに書き込みを停止します。 変更されたクライアントや敵対的なクライアントは、必要なだけのデータの書き込みを阻止することはできません。クライアントが完全に信頼できない環境では、クオータを使用してシステムの容量不足を防止するべきではありません。_
2. _クォータは不正確です。ファイルシステムに書き込んでいるプロセスは、クォータ制限に達した後、しばらくして停止されます。 彼らは必然的に、設定された制限を超える量のデータを書き込むことを許可されます。 どの程度までクォータを超えることができるかは、データ量ではなく、主に時間量に依存します。 一般的に、ライターは設定された制限を越えてから10秒以内に停止されます。_
3. _クオータはカーネルクライアント4.17以降に実装されています。クオータはユーザースペースクライアント\(libcephfs、ceph\-fuse\)でサポートされます。Linuxカーネルクライアント \>= 4.17はCephFSクォータをサポートしますが、mimic\+クラスタ上でのみです。 カーネルクライアント\(最近のバージョンでも\)は、quotas拡張属性を設定できる場合でも、古いクラスタでのquotasの処理に失敗することがあります。_
4. _パスベースのマウント制限と併用する場合は、クオータを慎重に設定する必要があります。クライアントは、クォータを適用するために、クォータが設定されているディレクトリの inode にアクセスできる必要があります。 クライアントがMDSの機能に基づいて特定のパス（例：/home/user）へのアクセスを制限している場合、アクセス権のない祖先ディレクトリ（例：/home）にクォータが設定されていると、クライアントはそれを実行することができません。 パスベースのアクセス制限を使用する場合、クライアントが制限されているディレクトリ（例：/home/user）またはその下にネストされたディレクトリにクォータを設定することを確認してください。_
5. _削除または変更されたスナップショットファイルのデータは、クォータにカウントされません。参照： [http://tracker.ceph.com/issues/24284](http://tracker.ceph.com/issues/24284)_

## Configuration[¶](https://docs.ceph.com/en/pacific/cephfs/quota/ "Permalink to this headline")

CephFSの他のほとんどのものと同様に、クォータは仮想拡張属性を使用して設定されます：

* ceph.quota.max\_files – file limit
* ceph.quota.max\_bytes – byte limit

この属性がディレクトリのinodeに表示される場合、そこにクォータが設定されていることを意味します。 もし属性がなければ、そのディレクトリにはクォータは設定されません \(ただし、親ディレクトリには設定されている可能性があります\)。

クォータを設定するには：

```
setfattr -n ceph.quota.max_bytes -v 100000000 /some/dir     # 100 MB
setfattr -n ceph.quota.max_files -v 10000 /some/dir         # 10,000 files
```

クォータ設定を表示するには：

```
getfattr -n ceph.quota.max_bytes /some/dir
getfattr -n ceph.quota.max_files /some/dir
```

なお、extended属性の値が0である場合は、クォータが設定されていないことを意味します。

クォータを削除するには：

```
setfattr -n ceph.quota.max_bytes -v 0 /some/dir
setfattr -n ceph.quota.max_files -v 0 /some/dir
```
