# 66: Monitoring a Cluster

**クリップソース:** [66: Monitoring a Cluster — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/operations/monitoring/)

# Monitoring a Cluster[¶](https://docs.ceph.com/en/pacific/rados/operations/monitoring/ "Permalink to this headline")

クラスタが稼働したら、cephツールを使ってクラスタを監視することができます。

クラスタの監視では、通常、OSDステータス、モニタステータス、PGステータス、メタデータサーバステータスを確認します。

## Using the command line[¶](https://docs.ceph.com/en/pacific/rados/operations/monitoring/ "Permalink to this headline")

### Interactive mode[¶](https://docs.ceph.com/en/pacific/rados/operations/monitoring/ "Permalink to this headline")

cephツールをインタラクティブモードで実行するには、コマンドラインで引数を指定せずにcephと入力します。 たとえば、以下のようになります。

```
ceph
ceph> health
ceph> status
ceph> quorum_status
ceph> mon stat

```

### Non\-default paths[¶](https://docs.ceph.com/en/pacific/rados/operations/monitoring/ "Permalink to this headline")

コンフィグやキーリングにデフォルト以外の場所を指定した場合、それらの場所を指定することができます。

```
ceph -c /path/to/conf -k /path/to/keyring health
```

## Checking a Cluster’s Status[¶](https://docs.ceph.com/en/pacific/rados/operations/monitoring/ "Permalink to this headline")

クラスタを起動した後、データの読み取りや書き込みを開始する前に、まずクラスタの状態を確認します。

クラスタの状態を確認するには、以下を実行します。

```
ceph status
```

Or:

```
ceph -s
```

インタラクティブモードで、statusと入力し、Enterを押してください。

```
ceph> status
```

Cephは、クラスタの状態を表示します。

たとえば、各サービスを1つずつ持つ小さなCephデモクラスタは、次のように表示されます。

```
cluster:
  id:     477e46f1-ae41-4e43-9c8f-72c918ab0a20
  health: HEALTH_OK

services:
  mon: 3 daemons, quorum a,b,c
  mgr: x(active)
  mds: cephfs_a-1/1/1 up  {0=a=up:active}, 2 up:standby
  osd: 3 osds: 3 up, 3 in

data:
  pools:   2 pools, 16 pgs
  objects: 21 objects, 2.19K
  usage:   546 GB used, 384 GB / 931 GB avail
  pgs:     16 active+clean

```

How Ceph Calculates Data Usage

使用量の値は、実際に使用された生のストレージの量を反映しています。

xxxGB/xxxGB 値は、クラスタ全体のストレージ容量のうち、利用可能な量（数値の小さい方）を意味します。

想定される数値は、レプリケート、クローン、スナップショットされる前の保存データのサイズを反映しています。

したがって、Cephはデータのレプリカを作成し、クローンやスナップショットのためにストレージ容量を使用することもあるため、実際に保存されるデータの量は、通常、想定される保存量を上回ります。

## Watching a Cluster[¶](https://docs.ceph.com/en/pacific/rados/operations/monitoring/ "Permalink to this headline")

Cephクラスタは、各デーモンによるローカルログのほか、システム全体に関する高レベルのイベントを記録するクラスタログを維持します。

これはモニタサーバのディスクに記録されますが\(デフォルトでは/var/log/ceph/ceph.log\)、コマンドラインを使用して監視することもできます。

クラスタログを追うには、次のコマンドを使用します。

```
ceph -w
```

Cephは、システムの状態を表示し、その後に各ログメッセージが出力されます。 たとえば、以下のようになります。

```
cluster:
  id:     477e46f1-ae41-4e43-9c8f-72c918ab0a20
  health: HEALTH_OK

services:
  mon: 3 daemons, quorum a,b,c
  mgr: x(active)
  mds: cephfs_a-1/1/1 up  {0=a=up:active}, 2 up:standby
  osd: 3 osds: 3 up, 3 in

data:
  pools:   2 pools, 16 pgs
  objects: 21 objects, 2.19K
  usage:   546 GB used, 384 GB / 931 GB avail
  pgs:     16 active+clean

2017-07-24 08:15:11.329298 mon.a mon.0 172.21.9.34:6789/0 23 : cluster [INF] osd.0 172.21.9.34:6806/20527 boot
2017-07-24 08:15:14.258143 mon.a mon.0 172.21.9.34:6789/0 39 : cluster [INF] Activating manager daemon x
2017-07-24 08:15:15.446025 mon.a mon.0 172.21.9.34:6789/0 47 : cluster [INF] Manager daemon x is now available
```

ceph\-wを使用してログ行を出力する以外に、ceph log last \[n\]を使用してクラスタログから最新のn行を参照することも可能です。

## Monitoring Health Checks[¶](https://docs.ceph.com/en/pacific/rados/operations/monitoring/ "Permalink to this headline")

Cephは、自身の状態に対して様々なヘルスチェックを継続的に実行しています。 

ヘルスチェックに失敗すると、ceph status \(またはceph health\)の出力に反映されます。 

さらに、チェックに失敗したとき、およびクラスタが回復したときを示すメッセージがクラスタログに送信されます。

たとえば、OSDがダウンした場合、ステータス出力のhealthセクションは次のように更新される場合があります。

```
health: HEALTH_WARN
        1 osds down
        Degraded data redundancy: 21/63 objects degraded (33.333%), 16 pgs unclean, 16 pgs degraded
```

このとき、ヘルスチェックの失敗を記録するためのクラスターログメッセージも出力されます。

```
2017-07-25 10:08:58.265945 mon.a mon.0 172.21.9.34:6789/0 91 : cluster [WRN] Health check failed: 1 osds down (OSD_DOWN)
2017-07-25 10:09:01.302624 mon.a mon.0 172.21.9.34:6789/0 94 : cluster [WRN] Health check failed: Degraded data redundancy: 21/63 objects degraded (33.333%), 16 pgs unclean, 16 pgs degraded (PG_DEGRADED)
```

OSDがオンラインに戻ると、クラスタログにクラスタの健全な状態への復帰が記録されます。

```
2017-07-25 10:11:11.526841 mon.a mon.0 172.21.9.34:6789/0 109 : cluster [WRN] Health check update: Degraded data redundancy: 2 pgs unclean, 2 pgs degraded, 2 pgs undersized (PG_DEGRADED)
2017-07-25 10:11:13.535493 mon.a mon.0 172.21.9.34:6789/0 110 : cluster [INF] Health check cleared: PG_DEGRADED (was: Degraded data redundancy: 2 pgs unclean, 2 pgs degraded, 2 pgs undersized)
2017-07-25 10:11:13.535577 mon.a mon.0 172.21.9.34:6789/0 111 : cluster [INF] Cluster is now healthy
```

### Network Performance Checks[¶](https://docs.ceph.com/en/pacific/rados/operations/monitoring/ "Permalink to this headline")

Ceph OSDは、デーモンの可用性を監視するために、互いにハートビートPingメッセージを送信します。 

また、応答時間を使用して、ネットワークパフォーマンスを監視します。

ビジー状態のOSDがPingレスポンスを遅らせる可能性はありますが、ネットワークスイッチに障害が発生した場合、異なるペアのOSD間で複数の遅延が検出されると想定されます。

デフォルトでは、1秒（1000ミリ秒）を超えるPing時間について警告を発します。

```
HEALTH_WARN Slow OSD heartbeats on back (longest 1118.001ms)
```

ヘルス詳細では、OSDの組み合わせで、どの程度の遅延が発生しているかが追加されます。 詳細項目は10個までです。

```
[WRN] OSD_SLOW_PING_TIME_BACK: Slow OSD heartbeats on back (longest 1118.001ms)
    Slow OSD heartbeats on back from osd.0 [dc1,rack1] to osd.1 [dc1,rack1] 1118.001 msec possibly improving
    Slow OSD heartbeats on back from osd.0 [dc1,rack1] to osd.2 [dc1,rack2] 1030.123 msec
    Slow OSD heartbeats on back from osd.2 [dc1,rack2] to osd.1 [dc1,rack1] 1015.321 msec
    Slow OSD heartbeats on back from osd.1 [dc1,rack1] to osd.0 [dc1,rack1] 1010.456 msec
```

さらに詳しく、ネットワーク・パフォーマンス情報の完全なダンプを見るには、dump\_osd\_networkコマンドを使用します。 

通常、これは mgr に送信されますが、任意の OSD に発行することで、特定の OSD の相互作用に制限することができます。 

現在のしきい値のデフォルトは1秒（1000ミリ秒）ですが、ミリ秒単位の引数で上書きすることができます。

次のコマンドは、0 のしきい値を指定して mgr に送信することにより、収集されたすべてのネットワークパフォーマンスデータを表示します。

```
$ ceph daemon /var/run/ceph/ceph-mgr.x.asok dump_osd_network 0
{
    "threshold": 0,
    "entries": [
        {
            "last update": "Wed Sep  4 17:04:49 2019",
            "stale": false,
            "from osd": 2,
            "to osd": 0,
            "interface": "front",
            "average": {
                "1min": 1.023,
                "5min": 0.860,
                "15min": 0.883
            },
            "min": {
                "1min": 0.818,
                "5min": 0.607,
                "15min": 0.607
            },
            "max": {
                "1min": 1.164,
                "5min": 1.173,
                "15min": 1.544
            },
            "last": 0.924
        },
        {
            "last update": "Wed Sep  4 17:04:49 2019",
            "stale": false,
            "from osd": 2,
            "to osd": 0,
            "interface": "back",
            "average": {
                "1min": 0.968,
                "5min": 0.897,
                "15min": 0.830
            },
            "min": {
                "1min": 0.860,
                "5min": 0.563,
                "15min": 0.502
            },
            "max": {
                "1min": 1.171,
                "5min": 1.216,
                "15min": 1.456
            },
            "last": 0.845
        },
        {
            "last update": "Wed Sep  4 17:04:48 2019",
            "stale": false,
            "from osd": 0,
            "to osd": 1,
            "interface": "front",
            "average": {
                "1min": 0.965,
                "5min": 0.811,
                "15min": 0.850
            },
            "min": {
                "1min": 0.650,
                "5min": 0.488,
                "15min": 0.466
            },
            "max": {
                "1min": 1.252,
                "5min": 1.252,
                "15min": 1.362
            },
        "last": 0.791
    },
    ...

```

### Muting health checks[¶](https://docs.ceph.com/en/pacific/rados/operations/monitoring/ "Permalink to this headline")

ヘルスチェックは、クラスタの全体的な報告ステータスに影響を与えないようにミュートすることができます。 

アラートは、ヘルスチェック・コードを使用して指定します（[Health checks](https://docs.ceph.com/en/pacific/rados/operations/monitoring/)参照）

```
ceph health mute <code>
```

たとえば、ヘルス警告がある場合、それをミュートすると、クラスタの全体的なステータスが HEALTH\_OK と報告されるようになります。 例えば、OSD\_DOWN警告をミュートするには、:

```
ceph health mute OSD_DOWN
```

ミュートは、ceph healthコマンドのshort形式およびlong形式の一部として報告されます。

たとえば、上記のシナリオでは：

```
$ ceph health
HEALTH_OK (muted: OSD_DOWN)
$ ceph health detail
HEALTH_OK (muted: OSD_DOWN)
(MUTED) OSD_DOWN 1 osds down
    osd.1 is down
```

ミュートを明示的に解除することができます。

```
ceph health unmute <code>
```

For example,:

```
ceph health unmute OSD_DOWN
```

ヘルスチェックのミュートには、オプションで TTL \(time to live\) を設定することができ、指定した時間が経過するとミュートが自動的に終了します。 

TTLは、オプションのduration引数として指定します（例）。

```
ceph health mute OSD_DOWN 4h    # mute for 4 hours
ceph health mute MON_DOWN 15m   # mute for 15  minutes
```

通常、ミュートされたヘルスアラートが解決されると（例えば、上記の例では、OSDが復帰する）、ミュートは解消されます。 

後でアラートが戻ってきた場合は、通常の方法で報告されます。

ミュートを「スティッキー」にすることで、アラートが解除されてもミュートを維持することができます。 たとえば、次のようになります。

```
ceph health mute OSD_DOWN 1h --sticky   # ignore any/all down OSDs for next hour
```

また、ほとんどのヘルスミュートは、警告の程度が悪くなると消えます。 

例えば、1つのOSDがダウンし、警告がミュートされている場合、さらに1つ以上のOSDがダウンするとミュートは消えます。 

これは、警告やエラーのトリガーとなるものの量や数を示すカウントを含むすべてのヘルス警告に当てはまります。

## Detecting configuration issues[¶](https://docs.ceph.com/en/pacific/rados/operations/monitoring/ "Permalink to this headline")

Cephが自身の状態について継続的に実行するヘルスチェックに加えて、外部ツールによってのみ検出される可能性がある設定上の問題があります。

ceph\-medicツールを使用して、Cephクラスタの設定に関するこれらの追加チェックを実行します。

## Checking a Cluster’s Usage Stats[¶](https://docs.ceph.com/en/pacific/rados/operations/monitoring/ "Permalink to this headline")

クラスタのデータ使用量やプール間のデータ分配を確認するには、dfオプションを使用します。Linuxのdfに似ています。以下を実行します。

```
ceph df
```

ceph dfの出力はこのようになります。

```
CLASS     SIZE    AVAIL     USED  RAW USED  %RAW USED
ssd    202 GiB  200 GiB  2.0 GiB   2.0 GiB       1.00
TOTAL  202 GiB  200 GiB  2.0 GiB   2.0 GiB       1.00

--- POOLS ---
POOL                   ID  PGS   STORED   (DATA)   (OMAP)   OBJECTS     USED  (DATA)   (OMAP)   %USED  MAX AVAIL  QUOTA OBJECTS  QUOTA BYTES  DIRTY  USED COMPR  UNDER COMPR
device_health_metrics   1    1  242 KiB   15 KiB  227 KiB         4  251 KiB  24 KiB  227 KiB       0    297 GiB            N/A          N/A      4         0 B          0 B
cephfs.a.meta           2   32  6.8 KiB  6.8 KiB      0 B        22   96 KiB  96 KiB      0 B       0    297 GiB            N/A          N/A     22         0 B          0 B
cephfs.a.data           3   32      0 B      0 B      0 B         0      0 B     0 B      0 B       0     99 GiB            N/A          N/A      0         0 B          0 B
test                    4   32   22 MiB   22 MiB   50 KiB       248   19 MiB  19 MiB   50 KiB       0    297 GiB            N/A          N/A    248         0 B          0 B
```

* **CLASS:**
* **SIZE:**
* **AVAIL:**
* **USED:**
* **RAW USED:**
* **%RAW USED:**

**POOLS:**

出力のPOOLSセクションは、プールのリストと各プールの想定使用量を提供します。このセクションの出力は、レプリカ、クローン、スナップショットを反映しません。例えば、1MBのデータを持つオブジェクトを保存した場合、想定される使用量は1MBですが、実際の使用量はレプリカ、クローン、スナップショットの数によって2MB以上になる可能性があります。

* **ID:**
* **\(DATA\):** RBD \(RADOS Block Device\)、CephFSファイルデータ、RGW \(RADOS Gateway\)オブジェクトデータでの使用量
    **\(OMAP\):** キーバリューペア。主にCephFSとRGW（RADOS Gateway）でメタデータの保存に使用される。
    **STORED:**
* **OBJECTS:**
* **\(DATA\):** RBD \(RADOS Block Device\)、CephFSファイルデータ、RGW \(RADOS Gateway\)オブジェクトデータでのオブジェクトの使用量
    **\(OMAP\):** オブジェクトのキーバリューペア。主にCephFSとRGW\(RADOS Gateway\)でメタデータストレージとして使用される。
    **USED:**
* **%USED:**
* **MAX AVAIL:**
* **QUOTA OBJECTS:**
* **QUOTA BYTES:**
* **DIRTY:**
* **USED COMPR:**
* **UNDER COMPR:**

POOLSセクションの数値は想定上のものです。

レプリカ、スナップショット、クローンの数は含まれません。

そのため、USEDと%USEDの合計は、出力のRAWセクションのUSEDと%USEDに一致しません。

注：MAX AVAILの値は、使用するレプリケーション、EC、ストレージをデバイスにマッピングするCRUSHルール、それらのデバイスの使用率、構成されたmon\_osd\_full\_ratioの複雑な関数です。

## Checking OSD Status[¶](https://docs.ceph.com/en/pacific/rados/operations/monitoring/ "Permalink to this headline")

以下のコマンドを実行することで、OSDが立ち上がっていることを確認できます。

```
# ceph osd stat
```

Or:

```
# ceph osd dump
```

また、以下のコマンドを使用することで、CRUSHマップ内の位置に応じたOSDビューを確認することができます。

```
# ceph osd tree
```

Cephは、ホスト、そのOSD、それらが稼働しているかどうか、およびそのウエイトを含むCRUSHツリーを出力します。

```
#ID CLASS WEIGHT  TYPE NAME             STATUS REWEIGHT PRI-AFF
 -1       3.00000 pool default
 -3       3.00000 rack mainrack
 -2       3.00000 host osd-host
  0   ssd 1.00000         osd.0             up  1.00000 1.00000
  1   ssd 1.00000         osd.1             up  1.00000 1.00000
  2   ssd 1.00000         osd.2             up  1.00000 1.00000
```

詳細については、「[Monitoring OSDs and Placement Groups](https://docs.ceph.com/en/pacific/rados/operations/monitoring/)」を参照してください。

## Checking Monitor Status[¶](https://docs.ceph.com/en/pacific/rados/operations/monitoring/ "Permalink to this headline")

クラスタに複数のモニタがある場合（おそらく）、クラスタを起動した後、データの読み取りおよび／または書き込みを行う前に、モニタのクォーラムの状態を確認する必要があります。複数のモニターが動作している場合、クォーラムが存在する必要があります。また、モニターのステータスを定期的にチェックして、それらが実行されていることを確認する必要があります。

モニターマップの表示を確認するには、以下を実行します。

```
ceph mon stat
```

Or:

```
ceph mon dump
```

モニタークラスタのクォーラムの状態を確認するには、以下を実行します。

```
ceph quorum_status
```

Cephはクォーラムの状態を返します。たとえば、3つのモニタで構成されるCephクラスタの場合、次のように返されます。

```
{ "election_epoch": 10,
  "quorum": [
        0,
        1,
        2],
  "quorum_names": [
        "a",
        "b",
        "c"],
  "quorum_leader_name": "a",
  "monmap": { "epoch": 1,
      "fsid": "444b489c-4f16-4b75-83f0-cb8097468898",
      "modified": "2011-12-12 13:28:27.505520",
      "created": "2011-12-12 13:28:27.505520",
      "features": {"persistent": [
                        "kraken",
                        "luminous",
                        "mimic"],
        "optional": []
      },
      "mons": [
            { "rank": 0,
              "name": "a",
              "addr": "127.0.0.1:6789/0",
              "public_addr": "127.0.0.1:6789/0"},
            { "rank": 1,
              "name": "b",
              "addr": "127.0.0.1:6790/0",
              "public_addr": "127.0.0.1:6790/0"},
            { "rank": 2,
              "name": "c",
              "addr": "127.0.0.1:6791/0",
              "public_addr": "127.0.0.1:6791/0"}
           ]
  }
}

```

## Checking MDS Status[¶](https://docs.ceph.com/en/pacific/rados/operations/monitoring/ "Permalink to this headline")

メタデータサーバは、CephFSのメタデータサービスを提供します。メタデータサーバには、up/downとactive/inactiveという2つの状態があります。メタデータサーバが稼働していることを確認するには、以下を実行します。

```
ceph mds stat
```

メタデータ・クラスターの詳細を表示するには、以下を実行します。

```
ceph fs dump
```

## Checking Placement Group States[¶](https://docs.ceph.com/en/pacific/rados/operations/monitoring/ "Permalink to this headline")

PGは、オブジェクトをOSDにマッピングします。

PGを監視する場合、アクティブでクリーンであることが望まれます。

詳細については、[Monitoring OSDs and Placement Groups](https://docs.ceph.com/en/pacific/rados/operations/monitoring/)を参照してください。

## Using the Admin Socket[¶](https://docs.ceph.com/en/pacific/rados/operations/monitoring/ "Permalink to this headline")

Cephのadminソケットを使用すると、ソケットインタフェース経由でデーモンに問い合わせることができます。デフォルトでは、Cephソケットは/var/run/cephの下に存在します。adminソケットを使用してデーモンにアクセスするには、デーモンを実行しているホストにログインして、次のコマンドを使用します。

```
ceph daemon {daemon-name}
ceph daemon {path-to-socket-file}
```

例えば、次のようなものが相当します。

```
ceph daemon osd.0 foo
ceph daemon /var/run/ceph/ceph-osd.0.asok foo
```

利用可能なadminソケットコマンドを表示するには、以下のコマンドを実行します。

```
ceph daemon {daemon-name} help
```

adminソケットコマンドを使用すると、実行時にコンフィギュレーションを表示したり設定したりすることができます。詳しくは、 [Viewing a Configuration at Runtime](https://docs.ceph.com/en/pacific/rados/operations/monitoring/)を参照してください。

さらに、実行時に設定値を直接設定することができます\(つまり、モニタに依存するものの当該ホストに直接ログインする必要がないceph tell {daemon\-type}.{id} config setとは異なり、adminソケットはモニタをバイパスします\)。
