# 178: LibCephFS (Python) — Ceph Documentation

 # LibCephFS \(Python\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this headline")

The cephfs python module provides access to CephFS service.

## API calls[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this headline")

This module is a thin wrapper around libcephfs.

_class_ `cephfs.``DirEntry`\(_d\_ino_, _d\_off_, _d\_reclen_, _d\_type_, _d\_name_\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")_class_ `cephfs.``LibCephFS`[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")libcephfs python wrapper

`abort_conn`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Abort mds connections.

`chdir`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Change the current working directory.

:param path the path to the working directory to change into.

`chmod`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Change directory mode.

Parameters
* **path** – the path of the directory to create. This must be either an absolute path or a relative path off of the current working directory.
* **mode** – the permissions the directory should have once created.

`chown`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Change directory ownership

Parameters
* **path** – the path of the directory to change.
* **uid** – the uid to set
* **gid** – the gid to set
* **follow\_symlink** – perform the operation on the target file if @path is a symbolic link \(default\)

`close`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Close the open file.

Parameters**fd** – the file descriptor referring to the open file.

`closedir`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Close the open directory.

Parameters**handle** – the open directory stream handle

`conf_get`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Gets the configuration value as a string.

Parameters**option** – the config option to get

`conf_parse_argv`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Parse the command line arguments and load the configuration parameters.

Parameters**argv** – the argument list

`conf_read_file`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Load the ceph configuration from the specified config file.

Parameters**str opt** \(_conffile_\) – the path to ceph.conf to override the default settings

`conf_set`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Sets a configuration value from a string.

Parameters
* **option** – the configuration option to set
* **value** – the value of the configuration option to set

`create`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Create a mount handle for interacting with Ceph. All libcephfs functions operate on a mount info handle.

Parameters
* **dict opt** \(_conf_\) – settings overriding the default ones and conffile
* **Union****\[****int****,****str****\]****,** **optional** \(_conffile_\) – the path to ceph.conf to override the default settings

Auth\_id str optthe id used to authenticate the client entity

`debug_get_fd_caps`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Get the capabilities currently issued to the client given the fd.

Parameters**fd** – the file descriptor to get issued

`debug_get_file_caps`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Get the capabilities currently issued to the client given the path.

Parameters**path** – the path of the file/directory to get the capabilities of.

`fallocate`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Preallocate or release disk space for the file for the byte range.

Parameters
* **fd** – the file descriptor of the file to fallocate.
* **mode** – the flags determines the operation to be performed on the given range. default operation \(0\) allocate and initialize to zero the file in the byte range, and the file size will be changed if offset \+ length is greater than the file size. if the FALLOC\_FL\_KEEP\_SIZE flag is specified in the mode, the file size will not be changed. if the FALLOC\_FL\_PUNCH\_HOLE flag is specified in the mode, the operation is deallocate space and zero the byte range.
* **offset** – the byte range starting.
* **length** – the length of the range.

`fchmod`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Change file mode based on fd. :param fd: the file descriptor of the file to change file mode :param mode: the permissions to be set.

`fchown`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Change file ownership :param fd: the file descriptor of the file to change ownership :param uid: the uid to set :param gid: the gid to set

`fgetxattr`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Get an extended attribute given the fd of a file.

Parameters
* **fd** – the open file descriptor referring to the file
* **name** – the name of the extended attribute to get
* **size** – the size of the pre\-allocated buffer

`flistxattr`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")List the extended attribute keys set on a file.

Parameters
* **fd** – the open file descriptor referring to the file.
* **size** – the size of list buffer to be filled with extended attribute keys.

`flock`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Apply or remove an advisory lock.

Parameters
* **fd** – the open file descriptor to change advisory lock.
* **operation** – the advisory lock operation to be performed on the file
* **owner** – the user\-supplied owner identifier \(an arbitrary integer\)

`fremovexattr`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Remove an extended attribute of a file.

Parameters
* **fd** – the open file descriptor referring to the file.
* **name** – name of the extended attribute to remove.

`fsetattrx`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Set a file’s attributes.

Parameters
* **path** – the path to the file/directory to set the attributes of.
* **mask** – a mask of all the CEPH\_SETATTR\_\* values that have been set in the statx struct.
* **stx** – a dict of statx structure that must include attribute values to set on the file.

`fsetxattr`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Set an extended attribute on a file.

Parameters
* **fd** – the open file descriptor referring to the file.
* **name** – the name of the extended attribute to set.
* **value** – the bytes of the extended attribute value

`fstat`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Get an open file’s extended statistics and attributes.

Parameters**fd** – the file descriptor of the file to get statistics of.

`fsync`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Synchronize an open file to persistent media.

Parameters
* **fd** – the file descriptor of the file to sync.
* **syncdataonly** – a boolean whether to synchronize metadata and data \(0\) or just data \(1\).

`ftruncate`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Truncate the file to the given size. If this operation causes the file to expand, the empty bytes will be filled in with zeros.

Parameters
* **path** – the path to the file to truncate.
* **size** – the new size of the file.

`futime`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Set access and modification time for a file pointed by descriptor

Parameters
* **fd** – file descriptor of the open file
* **times** – if times is not None, it must be a tuple \(atime, mtime\)

`futimens`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Set access and modification time for a file pointer by descriptor

Parameters
* **fd** – file descriptor of the open file
* **times** – if times is not None, it must be a tuple \(atime, mtime\)

`futimes`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Set access and modification time for a file pointer by descriptor

Parameters
* **fd** – file descriptor of the open file
* **times** – if times is not None, it must be a tuple \(atime, mtime\)

`get_addrs`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Get associated client addresses with this RADOS session.

`get_cap_return_timeout`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Get the amount of time that the client has to return caps

In the event that a client does not return its caps, the MDS may blocklist it after this timeout. Applications should check this value and ensure that they set the delegation timeout to a value lower than this.

`get_default_pool`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Get the default pool name and id of cephfs. This returns dict{pool\_name, pool\_id}.

`get_file_replication`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Get the file replication information from an open file descriptor.

:param fdthe open file descriptor referring to the file to getthe replication information of.

`get_fscid`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Return the file system id for this fs client.

`get_instance_id`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Get a global id for current instance

`get_layout`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Set ceph client session timeout. Must be called before mount.

Parameters**fd** – file descriptor of the file/directory for which to get the layout

`get_path_replication`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Get the file replication information given the path.

Parameters**path** – the path of the file/directory to get the replication information of.

`get_pool_id`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Get the id of the named pool.

Parameters**pool\_name** – the name of the pool.

`get_pool_replication`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Get the pool replication factor.

Parameters**pool\_id** – the pool id to look up

`getcwd`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Get the current working directory.

:rtype the path to the current working directory

`getxattr`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Get an extended attribute.

Parameters
* **path** – the path to the file
* **name** – the name of the extended attribute to get
* **size** – the size of the pre\-allocated buffer

`init`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Initialize the filesystem client \(but do not mount the filesystem yet\)

`lazyio`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Enable/disable lazyio for the file.

Parameters
* **fd** – the file descriptor of the file for which to enable lazio.
* **enable** – a boolean to enable lazyio or disable lazyio.

`lazyio_propagate`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Flushes the write buffer for the file thereby propogating the buffered write to the file.

Parameters
* **fd** – the file descriptor of the file to sync.
* **offset** – the byte range starting.
* **count** – the number of bytes starting from offset.

`lazyio_synchronize`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Flushes the write buffer for the file and invalidate the read cache. This allows a subsequent read operation to read and cache data directly from the file and hence everyone’s propagated writes would be visible.

Parameters
* **fd** – the file descriptor of the file to sync.
* **offset** – the byte range starting.
* **count** – the number of bytes starting from offset.

`lchmod`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Change file mode. If the path is a symbolic link, it won’t be dereferenced.

Parameters
* **path** – the path of the file. This must be either an absolute path or a relative path off of the current working directory.
* **mode** – the permissions to be set .

`lchown`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Change ownership of a symbolic link :param path: the path of the symbolic link to change. :param uid: the uid to set :param gid: the gid to set

`lgetxattr`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Get an extended attribute without following symbolic links. This function is identical to ceph\_getxattr, but if the path refers to a symbolic link, we get the extended attributes of the symlink rather than the attributes of the file it points to.

Parameters
* **path** – the path to the file
* **name** – the name of the extended attribute to get
* **size** – the size of the pre\-allocated buffer

`link`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Create a link.

Parameters
* **existing** – the path to the existing file/directory to link to.
* **newname** – the path to the new file/directory to link from.

`listxattr`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")List the extended attribute keys set on a file.

Parameters
* **path** – path of the file.
* **size** – the size of list buffer to be filled with extended attribute keys.

`llistxattr`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")List the extended attribute keys set on a file but do not follow symbolic link.

Parameters
* **path** – path of the file.
* **size** – the size of list buffer to be filled with extended attribute keys.

`lremovexattr`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Remove an extended attribute of a file but do not follow symbolic link.

Parameters
* **path** – path of the file.
* **name** – name of the extended attribute to remove.

`lseek`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Set the file’s current position.

Parameters
* **fd** – the file descriptor of the open file to read from.
* **offset** – the offset in the file to read from. If this value is negative, the function reads from the current offset of the file descriptor.
* **whence** – the flag to indicate what type of seeking to performs:SEEK\_SET, SEEK\_CUR, SEEK\_END

`lsetxattr`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Set an extended attribute on a file but do not follow symbolic link.

Parameters
* **path** – the path to the file.
* **name** – the name of the extended attribute to set.
* **value** – the bytes of the extended attribute value

`lstat`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Get a file’s extended statistics and attributes. When file’s a symbolic link, return the informaion of the link itself rather than that of the file it points too.

Parameters**path** – the file or directory to get the statistics of.

`lutimes`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Set access and modification time for a file. If the file is a symbolic link do not follow to the target.

Parameters
* **path** – file path for which timestamps have to be changed
* **times** – if times is not None, it must be a tuple \(atime, mtime\)

`mds_command`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition"):return 3\-tuple of output status int, output status string, output data

`mkdir`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Create a directory.

Parameters
* **path** – the path of the directory to create. This must be either an absolute path or a relative path off of the current working directory.
* **mode** – the permissions the directory should have once created.

`mkdirs`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Create multiple directories at once.

Parameters
* **path** – the full path of directories and sub\-directories that should be created.
* **mode** – the permissions the directory should have once created

`mknod`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Make a block or character special file.

Parameters
* **path** – the path to the special file.
* **mode** – the permissions to use and the type of special file. The type can be one of stat.S\_IFREG, stat.S\_IFCHR, stat.S\_IFBLK, stat.S\_IFIFO. Both should be combined using bitwise OR.
* **rdev** – If the file type is stat.S\_IFCHR or stat.S\_IFBLK then this parameter specifies the major and minor numbers of the newly created device special file. Otherwise, it is ignored.

`mksnap`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Create a snapshot.

Parameters
* **path** – path of the directory to snapshot.
* **name** – snapshot name
* **mode** – permission of the snapshot
* **metadata** – metadata key/value to store with the snapshot

RaisesclassTypeError

RaisesclassError

Returnsint: 0 on success

`mount`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Perform a mount using the path for the root of the mount.

`open`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Create and/or open a file.

Parameters
* **path** – the path of the file to open. If the flags parameter includes O\_CREAT, the file will first be created before opening.
* **flags** – set of option masks that control how the file is created/opened.
* **mode** – the permissions to place on the file if the file does not exist and O\_CREAT is specified in the flags.

`opendir`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Open the given directory.

Parameters**path** – the path name of the directory to open. Must be either an absolute path or a path relative to the current working directory.

Rtype handlethe open directory stream handle

`preadv`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Write data to a file.

Parameters
* **fd** – the file descriptor of the open file to read from
* **buffers** – the list of byte object to read from the file
* **offset** – the offset of the file read from. If this value is negative, the function reads from the current offset of the file descriptor.

`pwritev`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Write data to a file.

Parameters
* **fd** – the file descriptor of the open file to write to
* **buffers** – the list of byte object to write to the file
* **offset** – the offset of the file write into. If this value is negative, the function writes to the current offset of the file descriptor.

`read`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Read data from the file.

Parameters
* **fd** – the file descriptor of the open file to read from.
* **offset** – the offset in the file to read from. If this value is negative, the function reads from the current offset of the file descriptor.
* **l** – the flag to indicate what type of seeking to perform

`readdir`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Get the next entry in an open directory.

Parameters**handle** – the open directory stream handle

Rtype dentrythe next directory entry or None if at the end of the directory \(or the directory is empty. This pointer should not be freed by the caller, and is only safe to access between return and the next call to readdir or closedir.

`readlink`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Read a symbolic link.

Parameters
* **path** – the path to the symlink to read
* **size** – the length of the buffer

Rtype bufbuffer to hold the path of the file that the symlink points to.

`removexattr`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Remove an extended attribute of a file.

Parameters
* **path** – path of the file.
* **name** – name of the extended attribute to remove.

`rename`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Rename a file or directory.

Parameters
* **src** – the path to the existing file or directory.
* **dst** – the new name of the file or directory.

`rewinddir`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Rewind the directory stream to the beginning of the directory.

Parameters**handle** – the open directory stream handle

`rmdir`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Remove a directory.

Parameters**path** – the path of the directory to remove.

`rmsnap`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Remove a snapshot.

Parameters
* **path** – path of the directory for removing snapshot
* **name** – snapshot name

RaisesclassError

Returnsint: 0 on success

`seekdir`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Move the directory stream to a position specified by the given offset.

Parameters
* **handle** – the open directory stream handle
* **offset** – the position to move the directory stream to. This offset should be a value returned by telldir. Note that this value does not refer to the nth entry in a directory, and can not be manipulated with plus or minus.

`set_mount_timeout`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Set mount timeout

Parameters**timeout** – mount timeout

`set_session_timeout`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Set ceph client session timeout. Must be called before mount.

Parameters**timeout** – the timeout to set

`set_uuid`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Set ceph client uuid. Must be called before mount.

Parameters**uuid** – the uuid to set

`setattrx`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Set a file’s attributes.

Parameters
* **path** – the path to the file/directory to set the attributes of.
* **mask** – a mask of all the CEPH\_SETATTR\_\* values that have been set in the statx struct.
* **stx** – a dict of statx structure that must include attribute values to set on the file.
* **flags** – mask of AT\_\* flags \(only AT\_ATTR\_NOFOLLOW is respected for now\)

`setxattr`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")> Set an extended attribute on a file.

Parameters
* **path** – the path to the file.
* **name** – the name of the extended attribute to set.
* **value** – the bytes of the extended attribute value

`shutdown`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Unmount and destroy the ceph mount handle.

`snap_info`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Fetch sapshot info

Parameters**path** – snapshot path

RaisesclassError

Returnsdict: snapshot metadata

`stat`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Get a file’s extended statistics and attributes.

Parameters**path** – the file or directory to get the statistics of.

`statfs`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Perform a statfs on the ceph file system. This call fills in file system wide statistics into the passed in buffer.

Parameters**path** – any path within the mounted filesystem

`statx`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Get a file’s extended statistics and attributes.

Parameters
* **path** – the file or directory to get the statistics of.
* **mask** – want bitfield of CEPH\_STATX\_\* flags showing designed attributes.
* **flag** – bitfield that can be used to set AT\_\* modifier flags \(only AT\_NO\_ATTR\_SYNC and AT\_SYMLINK\_NOFOLLOW\)

`symlink`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Creates a symbolic link.

Parameters
* **existing** – the path to the existing file/directory to link to.
* **newname** – the path to the new file/directory to link from.

`sync_fs`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Synchronize all filesystem data to persistent media

`telldir`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Get the current position of a directory stream.

Parameters**handle** – the open directory stream handle

Return valueThe position of the directory stream. Note that the offsets returned by ceph\_telldir do not have a particular order \(cannot be compared with inequality\).

`truncate`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Truncate the file to the given size. If this operation causes the file to expand, the empty bytes will be filled in with zeros.

Parameters
* **path** – the path to the file to truncate.
* **size** – the new size of the file.

`unlink`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Removes a file, link, or symbolic link. If the file/link has multiple links to it, the file will not disappear from the namespace until all references to it are removed.

Parameters**path** – the path of the file or link to unlink.

`unmount`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Unmount a mount handle.

`utime`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Set access and modification time for path

Parameters
* **path** – file path for which timestamps have to be changed
* **times** – if times is not None, it must be a tuple \(atime, mtime\)

`utimes`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Set access and modification time for path

Parameters
* **path** – file path for which timestamps have to be changed
* **times** – if times is not None, it must be a tuple \(atime, mtime\)
* **follow\_symlink** – perform the operation on the target file if @path is a symbolic link \(default\)

`version`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Get the version number of the `libcephfs` C library.

Returnsa tuple of `(major, minor, extra)` components of the libcephfs version

`write`\(\)[¶](https://docs.ceph.com/en/pacific/cephfs/api/libcephfs-py/ "Permalink to this definition")Write data to a file.

Parameters
* **fd** – the file descriptor of the open file to write to
* **buf** – the bytes to write to the file
* **offset** – the offset of the file write into. If this value is negative, the function writes to the current offset of the file descriptor.
