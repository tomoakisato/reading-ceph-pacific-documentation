# 299: Diskprediction Module

**クリップソース:** [299: Diskprediction Module — Ceph Documentation](https://docs.ceph.com/en/pacific/mgr/diskprediction/)

# Diskprediction Module[¶](https://docs.ceph.com/en/pacific/mgr/diskprediction/ "Permalink to this headline")

diskpredictionモジュールは、Cephデバイスヘルスチェックを利用してディスクの健全性メトリクスを収集し、内部predictorモジュールを使用してディスク障害予測を行い、Cephに返送します。データ解析や結果出力に外部サーバを必要としません。内部予測モジュールの精度は約70%です。

## Enabling[¶](https://docs.ceph.com/en/pacific/mgr/diskprediction/ "Permalink to this headline")

以下のコマンドを実行して、Ceph環境のdiskprediction\_localモジュールを有効にします：

```
ceph mgr module enable diskprediction_local
```

ローカルプレディクターを有効にするには：

```
ceph config set global device_failure_prediction_mode local
```

予測機能を無効にするには：

```
ceph config set global device_failure_prediction_mode none
```

_diskprediction\_localは、デバイスの寿命を予測するために、少なくとも6つのデータセットのデバイスヘルスメトリクスを必要とします。これらのヘルスメトリクスは、ヘルスモニタリングが有効な場合にのみ収集されます。_

次のコマンドを実行すると、指定したデバイスの寿命が表示されます。

```
ceph device predict-life-expectancy <device id>
```

## Configuration[¶](https://docs.ceph.com/en/pacific/mgr/diskprediction/ "Permalink to this headline")

このモジュールは、デフォルトで1日単位で予測を実行します。でこの間隔を調整することができます：

```
ceph config set mgr mgr/diskprediction_local/predict_interval <interval-in-seconds>
```

## Debugging[¶](https://docs.ceph.com/en/pacific/mgr/diskprediction/ "Permalink to this headline")

DiskPredictionモジュールをCephのloggingレベルにマッピングしてデバッグする場合は、次のコマンドを使用します。

```
[mgr]

    debug mgr = 20

```

ロギングがマネージャのデバッグに設定されている場合、モジュールは簡単にフィルタリングできるように、プレフィックス mgr\[diskprediction\] を持つロギングメッセージを出力します。
