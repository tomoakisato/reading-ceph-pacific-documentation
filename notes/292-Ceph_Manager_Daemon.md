# 292: Ceph Manager Daemon

**クリップソース:** [292: Ceph Manager Daemon — Ceph Documentation](https://docs.ceph.com/en/pacific/mgr/)

# Ceph Manager Daemon[¶](https://docs.ceph.com/en/pacific/mgr/ "Permalink to this headline")

Ceph Managerデーモン\(ceph\-mgr\)はモニタデーモンと同時に実行され、追加の監視と外部の監視システムと管理システムへのインタフェースを提供します。

12.x \(luminous\) Cephリリース以降、通常の操作にはceph\-mgrデーモンが必要です。 ceph\-mgrデーモンは、11.x \(kraken\) Cephリリースではオプションのコンポーネントです。

デフォルトでは、マネージャーデーモンは、それが実行されていることを確認する以上の追加設定は必要ありません。 mgrデーモンが実行されていない場合、その旨のヘルス警告が表示され、ceph statusの出力の他の情報の一部は、mgrが起動するまで見つからないか、古いままとなります。

Use your normal deployment tools, such as ceph\-ansible or cephadm, to set up ceph\-mgr daemons on each of your mon nodes.  It is not mandatory to place mgr daemons on the same nodes as mons, but it is almost always sensible.

* [Installation and Configuration](https://docs.ceph.com/en/pacific/mgr/)
* [Writing modules](https://docs.ceph.com/en/pacific/mgr/)
* [Writing orchestrator plugins](https://docs.ceph.com/en/pacific/mgr/)
* [Dashboard module](https://docs.ceph.com/en/pacific/mgr/)
* [Ceph RESTful API](https://docs.ceph.com/en/pacific/mgr/)
* [Alerts module](https://docs.ceph.com/en/pacific/mgr/)
* [DiskPrediction module](https://docs.ceph.com/en/pacific/mgr/)
* [Local pool module](https://docs.ceph.com/en/pacific/mgr/)
* [RESTful module](https://docs.ceph.com/en/pacific/mgr/)
* [Zabbix module](https://docs.ceph.com/en/pacific/mgr/)
* [Prometheus module](https://docs.ceph.com/en/pacific/mgr/)
* [Influx module](https://docs.ceph.com/en/pacific/mgr/)
* [Hello module](https://docs.ceph.com/en/pacific/mgr/)
* [Telegraf module](https://docs.ceph.com/en/pacific/mgr/)
* [Telemetry module](https://docs.ceph.com/en/pacific/mgr/)
* [Iostat module](https://docs.ceph.com/en/pacific/mgr/)
* [Crash module](https://docs.ceph.com/en/pacific/mgr/)
* [Insights module](https://docs.ceph.com/en/pacific/mgr/)
* [Orchestrator module](https://docs.ceph.com/en/pacific/mgr/)
* [Rook module](https://docs.ceph.com/en/pacific/mgr/)
* [MDS Autoscaler module](https://docs.ceph.com/en/pacific/mgr/)
* [NFS module](https://docs.ceph.com/en/pacific/mgr/)
