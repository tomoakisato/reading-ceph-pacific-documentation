# 68: User Management

**クリップソース:** [68: User Management — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/operations/user-management/)

# User Management[¶](https://docs.ceph.com/en/pacific/rados/operations/user-management/ "Permalink to this headline")

この文書では、Ceph Clientユーザ、およびCeph Storage Clusterでの認証および認可について説明します。

ユーザは、個人またはアプリケーションなどのシステムアクターで、Cephクライアントを使用してCeph Storage Clusterデーモンと対話します。

認証および認可を有効にしてCephを実行する場合\(デフォルトで有効\)、ユーザ名と、指定したユーザの秘密鍵を含むキーリングを指定する必要があります\(通常はコマンドライン経由\)。

ユーザ名を指定しない場合、Cephはclient.adminをデフォルトのユーザ名として使用します。

キーリングを指定しない場合、CephはCeph構成のキーリング設定によりキーリングを探します。

たとえば、ユーザまたはキーリングを指定せずにcephhealthコマンドを実行すると、以下のようになります。

```
ceph health
```

Cephはこのようにコマンドを解釈します。

```
ceph -n client.admin --keyring=/etc/ceph/ceph.client.admin.keyring health
```

または、ユーザー名と秘密の再入力を避けるために、CEPH\_ARGS環境変数を使用することもできます。

認証を使用するためのCeph Storage Clusterの設定の詳細については、「 [Cephx Config Reference](https://docs.ceph.com/en/pacific/rados/operations/user-management/)」を参照してください。Cephxのアーキテクチャの詳細については、「[Architecture \- High Availability Authentication](https://docs.ceph.com/en/pacific/rados/operations/user-management/).」を参照してください。

## Background[¶](https://docs.ceph.com/en/pacific/rados/operations/user-management/ "Permalink to this headline")

Cephクライアントの種類（ブロックデバイス、オブジェクトストレージ、ファイルシステム、ネイティブAPIなど）に関係なく、Cephはすべてのデータをプール内のオブジェクトとして保存します。

Cephユーザがデータを読み書きするには、プールにアクセスする必要があります。

さらに、Cephの管理コマンドを使用するには、Cephユーザに実行権限が必要です。

以下の概念は、Cephのユーザ管理について理解するのに役立ちます。

### User[¶](https://docs.ceph.com/en/pacific/rados/operations/user-management/ "Permalink to this headline")

ユーザーとは、個人またはアプリケーションなどのシステムアクターのことです。

ユーザを作成すると、Ceph Storage Cluster、そのプール、およびプール内のデータにアクセスできる人\(または何か\)を制御できます。

Cephには、ユーザーのタイプという概念があります。

ユーザ管理の目的では、タイプは常にクライアントになります。

Cephは、ユーザタイプとユーザIDからなるピリオド\(.\)区切り形式でユーザを識別します。

たとえば、TYPE.ID、client.admin、client.user1などです。

ユーザタイプを指定する理由は、Cephモニタ、OSD、およびメタデータサーバもCephxプロトコルを使用しますが、これらはクライアントではないからです。

ユーザタイプを区別することで、クライアントユーザとその他のユーザを区別し、アクセス制御、ユーザ監視、およびトレーサビリティを合理化できます。

Cephのコマンドラインでは、コマンドラインの使用方法に応じて、ユーザタイプを指定したり、指定しなかったりできるため、Cephのユーザタイプが分かりにくく感じられる場合があります。

userまたは\-\-idを指定すると、タイプを省略することができます。

したがって、client.user1は単にuser1と入力することができます。

nameまたは\-nを指定した場合は、client.user1のように型と名前を指定する必要があります。

可能な限り、型と名前を使用することをお勧めします。

Note:Ceph Storage Clusterユーザは、Ceph Object StorageユーザやCeph File Systemユーザと同じではありません。

Ceph Object Gatewayは、ゲートウェイデーモンとストレージクラスタ間の通信にCeph Storage Clusterユーザを使用しますが、ゲートウェイには、エンドユーザ向けの独自のユーザ管理機能があります。

Ceph File SystemはPOSIXセマンティクスを使用します。Ceph File Systemに関連するユーザスペースは、Ceph Storage Clusterユーザと同じではありません。

### Authorization \(Capabilities\)[¶](https://docs.ceph.com/en/pacific/rados/operations/user-management/ "Permalink to this headline")

Cephは、認証されたユーザがモニタ、OSD、およびメタデータサーバの機能を行使する権限を与えることを表すために、「capabilities」（キャップス）という用語を使用します。

また、ケイパビリティは、プール内のデータ、プール内のネームスペース、またはアプリケーションタグに基づくプールのセットへのアクセスを制限することができます。

Ceph管理ユーザは、ユーザを作成または更新する際にユーザの能力を設定します。

Capabilityのシンタックスは、以下の形式です。

```
{daemon-type} '{cap-spec}[, {cap-spec} ...]'
```

* **Monitor Caps:**

```
mon 'allow {access-spec} [network {network/prefix}]'

mon 'profile {name}'
```

{access\-spec}の構文は次のとおりです。

```
* | all | [r][w][x]
```

オプションの{network/prefix}は、CIDR表記による標準的なネットワーク名とプレフィックス長（例：10.3.0.0/16）です。 

このCAPSの使用は、このネットワークから接続するクライアントに制限されます。

* **OSD Caps:**

さらに、OSD CAPSでは、プールとネームスペースの設定も可能です。

```
osd 'allow {access-spec} [{match-spec}] [network {network/prefix}]'

osd 'profile {name} [pool={pool-name} [namespace={namespace-name}]] [network {network/prefix}]'
```

{access\-spec}の構文は、以下のいずれかです。

```
* | all | [r][w][x] [class-read] [class-write]

class {class name} [{method name}]
```

オプションの{match\-spec}構文は、以下のいずれかです。

```
pool={pool-name} [namespace={namespace-name}] [object_prefix {prefix}]

[namespace={namespace-name}] tag {application} {key}={value}
```

オプションの{network/prefix}は、CIDR表記による標準的なネットワーク名とプレフィックス長です（例：10.3.0.0/16）。 

このCAPSの使用は、このネットワークから接続するクライアントに制限されます。

* **Manager Caps:**

たとえば、以下のようなものがあります。

```
mgr 'allow {access-spec} [network {network/prefix}]'

mgr 'profile {name} [{key1} {match-type} {value1} ...] [network {network/prefix}]
```

マネージャCAPSは、特定のコマンド、組み込みマネージャサービスによってエクスポートされるすべてのコマンド、または特定のアドオンモジュールによってエクスポートされるすべてのコマンドに対して指定することも可能です。例えば

```
mgr 'allow command "{command-prefix}" [with {key1} {match-type} {value1} ...] [network {network/prefix}]'

mgr 'allow service {service-name} {access-spec} [network {network/prefix}]'

mgr 'allow module {module-name} [with {key1} {match-type} {value1} ...] {access-spec} [network {network/prefix}]'
```

{access\-spec}の構文は以下のとおりです。

```
* | all | [r][w][x]
```

{service\-name}は次のいずれかです。

```
mgr | osd | pg | py
```

{match\-type}は、以下のいずれかです。

```
= | prefix | regex
```

* **Metadata Server Caps:**

CephFSクライアントなど、他のすべてのユーザについては、「[CephFS Client Capabilities](https://docs.ceph.com/en/pacific/rados/operations/user-management/)」を参照してください。

注：Ceph Object Gatewayデーモン（radosgw）はCeph Storage Clusterのクライアントであるため、Ceph Storage Clusterデーモンタイプとして表現されていません。

各アクセスCAPSの説明は以下の通りです。

_allow_

Description

デーモンのアクセス設定より優先される。MDSのみrwを意味する。

_r_

Description

ユーザに読み取り権限を与える。CRUSHマップを取得するためのモニタで必要

_w_

Description

オブジェクトへの書き込み権限を与える

_x_

Description

クラスメソッドの呼び出し（読み取りと書き込みの両方）と、モニタに対する認証操作の機能を提供する

_class\-read_

Descriptions

クラスの読み取りメソッドを呼び出す機能を提供する。xのサブセット

_class\-write_

Description

クラスの書き込みメソッドを呼び出す機能を提供する。xのサブセット

_\*, all_

Description

特定のデーモン/プールの読み取り、書き込み、実行権限、管理者コマンドの実行権限をユーザーに付与する

以下の項目は、有効なケイパビリティ・プロファイルについて説明しています。

_profile osd \(Monitor only\)_

Description

OSDとして他のOSDやモニターに接続するための権限を与える。

OSDがレプリケーションのハートビートトラフィックとステータスレポートを処理できるようにするために、OSDに付与される

_profile mds \(Monitor only\)_

Description

MDSとして他のMDSやモニタに接続するための権限をユーザーに与える

_profile bootstrap\-osd \(Monitor only\)_

Description

OSD をブートストラップするための権限をユーザーに与える。

ceph\-volume、cephadmなどのデプロイツールに付与され、OSDをブートストラップするときにキーなどを追加するパーミッションを持つようにする

_profile bootstrap\-mds \(Monitor only\)_

Description

MDSを起動するための権限をユーザに付与する。cephadmなどのデプロイツールに付与され、MDSをブートストラップする際にキーなどを追加するパーミッションが与えられる

_profile bootstrap\-rbd \(Monitor only\)_

Description

RBDユーザーをブートストラップするための権限をユーザーに付与する。

cephadmなどのデプロイメントツールに付与され、RBDユーザーをブートストラップする際にキーなどを追加するパーミッションが与えらる

_profile bootstrap\-rbd\-mirror \(Monitor only\)_

Description

rbd\-mirror デーモンユーザーをブートストラップするためのパーミッションをユーザーに与える。

cephadm などのデプロイツールに付与され、rbd\-mirror デーモンをブートストラップする際にキーなどを追加するパーミッションが与えられる

_profile rbd \(Manager, Monitor, and OSD\)_

Description

RBD イメージを操作するための権限をユーザーに付与する。

モニタCAPSとして使用する場合、RBDクライアントアプリケーションに必要な最小限の権限を提供する。

OSD CAPSとして使用される場合、RBDクライアントアプリケーションに指定されたプールへの読み取り/書き込みアクセス権を提供する。

Manager CAPSは、オプションのpoolおよびnamespaceキーワード引数をサポートする

_profile rbd\-mirror \(Monitor only\)_

Description

RBD イメージを操作し、RBD ミラーリングの設定キー秘密を取得するための権限をユーザーに付与する。

rbd\-mirror デーモンに必要な最小限の特権を提供する

_profile rbd\-read\-only \(Manager and OSD\)_

Description

RBDイメージに対する読み取り専用権限をユーザーに与える。

Manager CAPSは、オプションのpoolおよびnamespaceキーワード引数をサポートしている

_profile simple\-rados\-client \(Monitor only\)_

Description

モニタ、OSD、PGデータの読み取り専用権限をユーザーに与える。

ダイレクトlibradosクライアント・アプリケーションでの使用を意図している

_profile simple\-rados\-client\-with\-blocklist \(Monitor only\)_

Description

モニタ、OSD、PGデータの読み取り専用権限をユーザーに与える。

ダイレクトlibradosクライアント・アプリケーションでの使用を意図している。

また、HAアプリケーションを構築するためのブロック・リスト・エントリを追加する権限も含まれる

_profile fs\-client \(Monitor only\)_

Description

モニタ、OSD、PG、MDS データに対する読み取り専用のパーミッションをユーザーに与える。

 CephFSクライアントを対象としている

_profile role\-definer \(Monitor and Auth\)_

Description

認証サブシステムに対するすべての権限、モニタに対する読み取り専用アクセス権、その他の権限をユーザーに与える。

自動化ツールに便利。 セキュリティに大きな影響を与えるので、本当に何をやっているのか分かっていない限り、これを割り当てないこと

_profile crash \(Monitor only\)_

Description

デーモンのクラッシュダンプを収集し、後で解析する際に、マネージャのクラッシュモジュールと一緒に使用される

### Pool[¶](https://docs.ceph.com/en/pacific/rados/operations/user-management/ "Permalink to this headline")

プールとは、ユーザーがデータを保存するための論理パーティションです。

Cephのデプロイメントでは、類似した種類のデータの論理パーティションとしてプールを作成するのが一般的です。

たとえば、OpenStackのバックエンドとしてCephを展開する場合、一般的な展開では、ボリューム、イメージ、バックアップ、仮想マシン用のプールと、client.glass、client.cinderなどのユーザが用意されることになります。

### Application Tags[¶](https://docs.ceph.com/en/pacific/rados/operations/user-management/ "Permalink to this headline")

アプリケーションのメタデータで定義された特定のプールにアクセスを制限することができます。

ワイルドカードは、key引数、value引数、その両方に使用することができます。

### Namespace[¶](https://docs.ceph.com/en/pacific/rados/operations/user-management/ "Permalink to this headline")

プール内のオブジェクトは、プール内のオブジェクトの論理的なグループであるネームスペースに関連付けることができます。

プールへのユーザのアクセスは、ユーザによる読み取りと書き込みがネームスペース内でのみ行われるように、ネームスペースに関連付けることができます。

プール内のネームスペースに書き込まれたオブジェクトは、そのネームスペースへのアクセス権を持つユーザのみがアクセスできます。

注：名前空間は、主にlibradosの上に書かれたアプリケーションで、論理的なグループ化により、異なるプールを作成する必要性を軽減するために役立ちます。Ceph Object Gateway \(luminous以降\)は、さまざまなメタデータオブジェクトに名前空間を使用します。

名前空間の根拠は、プールは、別々のユーザー・セットを認可する目的でデータ・セットを分離するための計算コストの高い方法であることです。

たとえば、プールはOSDごとに100の配置グループを持つ必要があります。

したがって、1000のOSDを持つ例示的なクラスタは、1つのプールのために100,000の配置グループを持つことになります。

各プールは、模範的なクラスタに別の 100,000 の配置グループを作成します。

これに対して、オブジェクトをネームスペースに書き込むと、ネームスペースがオブジェクト名に関連付けられるだけで、個別のプールの計算オーバーヘッドが発生しません。

ユーザまたはユーザ・セットに対して個別のプールを作成する代わりに、ネームスペースを使用することができます。

注：現時点では、libradosを使用してのみ利用可能です。

ネームスペース機能により、特定のRADOSネームスペースにアクセスを制限することができます。

指定された名前空間の最後の文字が \* である場合、与えられた引数で始まるすべての名前空間へのアクセスが許可されます。

## Managing Users[¶](https://docs.ceph.com/en/pacific/rados/operations/user-management/ "Permalink to this headline")

ユーザ管理機能は、Ceph Storage Clusterの管理者に、Ceph Storage Clusterで直接ユーザーを作成、更新、削除する機能を提供します。

Ceph Storage Clusterでユーザーを作成または削除するときに、キーをクライアントに配布して、キーリングに追加できるようにする必要がある場合があります。

詳細については、「 [Keyring Management](https://docs.ceph.com/en/pacific/rados/operations/user-management/)」を参照してください。

### List Users[¶](https://docs.ceph.com/en/pacific/rados/operations/user-management/ "Permalink to this headline")

クラスタ内のユーザーを一覧表示するには、以下を実行します。

```
ceph auth ls
```

Cephは、クラスタ内のすべてのユーザをリストアップします。たとえば、2ノードの模範的なクラスタでは、ceph auth lsは次のようなものを出力します。

```
installed auth entries:

osd.0
        key: AQCvCbtToC6MDhAATtuT70Sl+DymPCfDSsyV4w==
        caps: [mon] allow profile osd
        caps: [osd] allow *
osd.1
        key: AQC4CbtTCFJBChAAVq5spj0ff4eHZICxIOVZeA==
        caps: [mon] allow profile osd
        caps: [osd] allow *
client.admin
        key: AQBHCbtT6APDHhAA5W00cBchwkQjh3dkKsyPjw==
        caps: [mds] allow
        caps: [mon] allow *
        caps: [osd] allow *
client.bootstrap-mds
        key: AQBICbtTOK9uGBAAdbe5zcIGHZL3T/u2g6EBww==
        caps: [mon] allow profile bootstrap-mds
client.bootstrap-osd
        key: AQBHCbtT4GxqORAADE5u7RkpCN/oo4e5W0uBtw==
        caps: [mon] allow profile bootstrap-osd

```

osd.0はosdタイプのユーザーでIDは0、client.adminはclientタイプのユーザーでIDはadmin（つまりデフォルトのclient.adminユーザー）というように、ユーザーのTYPE.ID表記が適用されることに注意してください。

また、各エントリには key:\<value\> エントリと、1つ以上の caps: エントリがあることに注意してください。

ceph auth lsで\-o{filename}オプションを使用すると、出力をファイルに保存することができます。

### Get a User[¶](https://docs.ceph.com/en/pacific/rados/operations/user-management/ "Permalink to this headline")

特定のユーザ、キー、CAPSを取得するには、以下を実行します。

```
ceph auth get {TYPE.ID}
```

For example:

```
ceph auth get client.admin
```

ceph auth getで\-o{filename}オプションを使用すると、出力をファイルに保存することができます。開発者は以下を実行することもできます。

```
ceph auth export {TYPE.ID}
```

auth exportコマンドは、auth getと同じです。

### Add a User[¶](https://docs.ceph.com/en/pacific/rados/operations/user-management/ "Permalink to this headline")

ユーザを追加すると、ユーザ名（すなわちTYPE.ID）、秘密鍵、ユーザを作成するために使用するコマンドに含まれる任意のCAPSが作成されます。

ユーザのキーは、ユーザがCeph Storage Clusterで認証することを可能にします。

ユーザのCAPSは、ユーザがCephモニタ\(mon\)、Ceph OSD\(osd\)、Cephメタデータサーバ\(mds\)で読み取り、書き込み、または実行することを許可するものです。

ユーザを追加するには、いくつかの方法があります。

* ceph auth add: このコマンドは、ユーザを追加するための標準的な方法です。ユーザを作成し、キーを生成し、指定されたCAPSを追加します。
* ceph auth get\-or\-create: このコマンドは、ユーザ名（括弧内）とキーを含むキーファイル形式を返すので、ユーザを作成する最も便利な方法です。ユーザがすでに存在する場合、このコマンドは単にユーザ名とキーをキーファイル形式で返します。また，\-o{filename} オプションを使用すると，出力をファイルに保存することができます．
* ceph auth get\-or\-create\-key: このコマンドは、ユーザを作成し、そのユーザのキーだけを返す便利な方法です。これは、キーだけを必要とするクライアント \(例: libvirt\) に便利です。ユーザがすでに存在する場合、このコマンドは単にキーを返します。出力をファイルに保存するには、\-o{filename} オプションを使用します。

clinetユーザを作成する際に、機能を持たないユーザを作成することができます。

クライアントがモニタからクラスタマップを取得できないため、CAPSのないユーザは単なる認証以上の意味がありません。

ただし、ceph auth capsコマンドを使用して機能を後で追加することを延期したい場合は、機能なしのユーザを作成することができます。

一般的なユーザは、少なくともCephモニタの読み取り能力と、Ceph OSDの読み取りと書き込み能力を持っています。

さらに、ユーザのOSD権限は、多くの場合、特定のプールへのアクセスに制限されています。

```
ceph auth add client.john mon 'allow r' osd 'allow rw pool=liverpool'
ceph auth get-or-create client.paul mon 'allow r' osd 'allow rw pool=liverpool'
ceph auth get-or-create client.george mon 'allow r' osd 'allow rw pool=liverpool' -o george.keyring
ceph auth get-or-create-key client.ringo mon 'allow r' osd 'allow rw pool=liverpool' -o ringo.key
```

重要：ユーザにOSDの機能を提供し、特定のプールへのアクセスを制限しない場合、ユーザはクラスタ内のすべてのプールにアクセスできるようになります。

### Modify User Capabilities[¶](https://docs.ceph.com/en/pacific/rados/operations/user-management/ "Permalink to this headline")

ceph auth capsコマンドでは、ユーザを指定し、そのユーザのCAPSを変更することができます。

新しいCAPSを設定すると、現在のCAPSが上書きされます。

現在のCAPSを表示するには、ceph auth get USERTYPE.USERIDを実行します。 

CAPSを追加するには、フォームを使用するときに既存のCAPSも指定する必要があります。

```
ceph auth caps USERTYPE.USERID {daemon} 'allow [r|w|x|*|...] [pool={pool-name}] [namespace={namespace-name}]' [{daemon} 'allow [r|w|x|*|...] [pool={pool-name}] [namespace={namespace-name}]']
```

For example:

```
ceph auth get client.john
ceph auth caps client.john mon 'allow r' osd 'allow rw pool=liverpool'
ceph auth caps client.paul mon 'allow rw' osd 'allow rwx pool=liverpool'
ceph auth caps client.brian-manager mon 'allow *' osd 'allow *'
```

 CAPSの詳細については、「[Authorization \(Capabilities\)](https://docs.ceph.com/en/pacific/rados/operations/user-management/)」を参照してください。

### Delete a User[¶](https://docs.ceph.com/en/pacific/rados/operations/user-management/ "Permalink to this headline")

ユーザを削除するには、ceph auth delを使用します。

```
ceph auth del {TYPE}.{ID}
```

ここで、{TYPE}はclient、osd、mon、mdsのいずれかであり、{ID}はデーモンのユーザ名またはIDです。

### Print a User’s Key[¶](https://docs.ceph.com/en/pacific/rados/operations/user-management/ "Permalink to this headline")

ユーザの認証キーを標準出力に出力するには、次のように実行します。

```
ceph auth print-key {TYPE}.{ID}
```

ここで、{TYPE}はclient, osd, mon, mdsのいずれか、{ID}はデーモンのユーザ名またはIDです。

ユーザのキーを印刷することは、クライアントソフトウェアにユーザのキーを入力する必要がある場合に便利です \(例: libvirt\)。

```
mount -t ceph serverhost:/ mountpoint -o name=client.user,secret=`ceph auth print-key client.user`
```

### Import a User\(s\)[¶](https://docs.ceph.com/en/pacific/rados/operations/user-management/ "Permalink to this headline")

1人または複数のユーザをインポートするには、ceph auth importを使用してキーリングを指定します。

```
ceph auth import -i /path/to/keyring
```

For example:

```
sudo ceph auth import -i /etc/ceph/ceph.keyring
```

注：Cephストレージクラスタは、新しいユーザ、そのキー、そのCAPSを追加し、既存のユーザ、そのキー、そのCAPSを更新します。

## Keyring Management[¶](https://docs.ceph.com/en/pacific/rados/operations/user-management/ "Permalink to this headline")

Cephクライアント経由でCephにアクセスすると、Cephクライアントはローカルのキーリングを探します。

Cephは、デフォルトで以下の4つのキーリング名でキーリング設定をプリセットするので、デフォルトを上書きする場合以外は、Ceph設定ファイルで設定する必要はありません\(推奨されません\)。

* /etc/ceph/$cluster.$name.keyring
* /etc/ceph/$cluster.keyring
* /etc/ceph/keyring
* /etc/ceph/keyring.bin

$clusterメタ変数は、Ceph構成ファイル名で定義されるCephクラスタ名です\(つまり、ceph.confはクラスタ名がcephであることを意味します。したがって、ceph.keyringとなります\)。

$nameメタ変数は、ユーザタイプおよびユーザIDです\(例: client.admin。したがって、ceph.client.admin.keyring\)。

/etc/cephを読み書きするコマンドを実行する場合、sudoを使用してrootでコマンドを実行する必要がある場合があります。

ユーザ\(client.ringoなど\)を作成したら、キーを取得してCephクライアント上のキーリングに追加し、そのユーザがCeph Storage Clusterにアクセスできるようにする必要があります。

[User Management](https://docs.ceph.com/en/pacific/rados/operations/user-management/)のセクションでは、Ceph Storage Clusterで直接ユーザをリストアップ、取得、追加、変更、および削除する方法について詳しく説明します。

ただし、Cephにはceph\-authtoolユーティリティも用意されており、Cephクライアントからキーリングを管理することも可能です。

### Create a Keyring[¶](https://docs.ceph.com/en/pacific/rados/operations/user-management/ "Permalink to this headline")

[Managing Users](https://docs.ceph.com/en/pacific/rados/operations/user-management/) セクションの手順を使用してユーザーを作成する場合、Cephクライアントが指定したユーザーのキーを取得して、Ceph Storage Clusterで認証できるように、ユーザーキーをCephクライアントに提供する必要があります。

Cephクライアントはキーリングにアクセスしてユーザ名を検索し、そのユーザのキーを取得します。

ceph\-authtoolユーティリティでは、キーリングを作成することができます。

空のキーリングを作成するには、\-\-create\-keyringまたは\-Cを使用します。

例えば、以下のようになります。

```
ceph-authtool --create-keyring /path/to/keyring
```

複数のユーザでキーリングを作成する場合、キーリングのファイル名にクラスタ名\(例: $cluster.keyring\)を使用して/etc/cephディレクトリに保存すると、Ceph構成ファイルのローカルコピーにファイル名を指定しなくてもキーリング構成のデフォルト設定がファイル名を拾ってくれますので、お勧めです。たとえば、以下を実行してceph.keyringを作成します。

```
sudo ceph-authtool -C /etc/ceph/ceph.keyring
```

単一ユーザーでキーリングを作成する場合、クラスタ名、ユーザータイプ、ユーザー名を使用し、/etc/cephディレクトリに保存することをお勧めします。

例えば、client.adminユーザには、ceph.client.admin.keyringとします。

/etc/ceph にキーリングを作成するには、root で作成する必要があります。

これは、ファイルがrootユーザーのみに対してrwパーミッションを持つことを意味し、キーリングに管理者キーが含まれる場合に適切です。

ただし、キーリングを特定のユーザまたはユーザーグループに使用する場合は、chownまたはchmodを実行して、適切なキーリングの所有権とアクセス権を確立するようにしてください。

### Add a User to a Keyring[¶](https://docs.ceph.com/en/pacific/rados/operations/user-management/ "Permalink to this headline")

Ceph Storage Clusterにユーザを追加すると、Get a User手順を使用して、ユーザ、キー、CAPSを取得し、ユーザをキーリングに保存することができます。

1つのキーリングにつき1人のユーザのみを使用したい場合、\-oオプションを付けてGet a Userプロシージャを実行すると、出力がキーリング・ファイル形式で保存されます。

たとえば、client.admin ユーザーのキーリングを作成するには、以下を実行します。

```
sudo ceph auth get client.admin -o /etc/ceph/ceph.client.admin.keyring
```

個々のユーザに対して推奨されるファイル形式を使用していることに注意してください。

ユーザをキーリングにインポートしたい場合、ceph\-authoolを使用して、宛先キーリングとソースキーリングを指定することができます。例えば、以下のようになります。

```
sudo ceph-authtool /etc/ceph/ceph.keyring --import-keyring /etc/ceph/ceph.client.admin.keyring
```

### Create a User[¶](https://docs.ceph.com/en/pacific/rados/operations/user-management/ "Permalink to this headline")

Cephは、Ceph Storage Clusterで直接ユーザを作成するためのAdd a User機能を提供します。ただし、Cephクライアントのキーリングに直接、ユーザ、キー、CAPSを作成することもできます。その後、ユーザをCeph Storage Clusterにインポートできます。たとえば、以下のようになります。

```
sudo ceph-authtool -n client.ringo --cap osd 'allow rwx' --cap mon 'allow rwx' /etc/ceph/ceph.keyring
```

CAPSの詳細については、「[Authorization \(Capabilities\)](https://docs.ceph.com/en/pacific/rados/operations/user-management/) 」を参照してください。

また、キーリングの作成と新規ユーザの追加を同時に行うことも可能です。例えば

```
sudo ceph-authtool -C /etc/ceph/ceph.keyring -n client.ringo --cap osd 'allow rwx' --cap mon 'allow rwx' --gen-key
```

前述のシナリオでは、新しいユーザーclient.ringoはキーリングにのみ存在します。新しいユーザーをCeph Storage Clusterに追加するには、やはり、新しいユーザーをCeph Storage Clusterに追加する必要があります。

```
sudo ceph auth add client.ringo -i /etc/ceph/ceph.keyring
```

### Modify a User[¶](https://docs.ceph.com/en/pacific/rados/operations/user-management/ "Permalink to this headline")

キーリング内のユーザーレコードのCAPSを変更するには、キーリング、ユーザ、CAPSの順に指定します。たとえば、以下のようになります。

```
sudo ceph-authtool /etc/ceph/ceph.keyring -n client.ringo --cap osd 'allow rwx' --cap mon 'allow rwx'
```

ユーザをCeph Storage Clusterに更新するには、キーリング内のユーザをCeph Storage Clusterのユーザーエントリに更新する必要があります。

```
sudo ceph auth import -i /etc/ceph/ceph.keyring
```

キーリングからCeph Storage Clusterユーザを更新する方法の詳細は、[Import a User\(s\)](https://docs.ceph.com/en/pacific/rados/operations/user-management/)を参照してください。

また、クラスタで直接ユーザCAPSを変更し、その結果をキーリングファイルに保存し、キーリングをメインのceph.keyringファイルにインポートすることもできます。

## Command Line Usage[¶](https://docs.ceph.com/en/pacific/rados/operations/user-management/ "Permalink to this headline")

Cephは、ユーザ名とsecretについて、以下の使い方をサポートしています。

\-\-id | \-\-user

Description

CephはタイプとIDでユーザを識別します\(例: TYPE.IDまたはclient.admin、client.user1\)。id、name、および\-nオプションを使用すると、ユーザ名のID部分を指定できます \(例: admin、user1、fooなど\)。また、\-\-idでユーザーを指定し、typeを省略することも可能です。たとえば、ユーザーclient.fooを指定するには、次のように入力します。

```
ceph --id foo --keyring /path/to/keyring health
ceph --user foo --keyring /path/to/keyring health
```

\-\-name | \-n

Description

CephはタイプとIDでユーザを識別します\(例: TYPE.IDまたはclient.admin、client.user1\)。nameおよび\-nオプションを使用すると、完全修飾ユーザ名を指定できます。ユーザIDと一緒にユーザ・タイプ \(通常はクライアント\) を指定する必要があります。たとえば、以下のようになります。

```
ceph --name client.foo --keyring /path/to/keyring health
ceph -n client.foo --keyring /path/to/keyring health
```

\-\-keyring

Description

1つまたは複数のユーザー名とsecretを含むキーリングへのパスです。secretオプションは同じ機能を提供しますが、別の目的で\-\-secretを使用するCeph RADOS Gatewayでは動作しません。ceph auth get\-or\-createでキーリングを取得し、それをローカルに保存することができます。これは、キーリングのパスを切り替えることなくユーザ名を切り替えることができるため、推奨される方法です。例えば

```
sudo rbd map --id foo --keyring /path/to/keyring mypool/myimage
```

## Limitations[¶](https://docs.ceph.com/en/pacific/rados/operations/user-management/ "Permalink to this headline")

cephxプロトコルは、Cephクライアントとサーバを相互に認証する。 これは、人間のユーザまたはその代理で実行されるアプリケーションプログラムの認証を処理することを意図していない。 アクセス制御のニーズに対応するためにその効果が必要な場合は、別のメカニズムを用意する必要があります。このメカニズムは、Cephオブジェクトストアへのアクセスに使用するフロントエンドに固有のものである可能性が高いです。 この別のメカニズムは、Cephがオブジェクトストアへのアクセスを許可するマシン上で、許容されるユーザとプログラムのみが実行できるようにする役割を持つ。

Cephクライアントおよびサーバの認証に使用されるキーは、通常、信頼できるホスト内の適切なパーミッションを持つプレーンテキストファイルに格納されます。

重要:鍵をプレーンテキストファイルに保存することはセキュリティ上の欠点がありますが、Cephがバックグラウンドで使用する基本的な認証方法を考慮すると、これを避けることは困難です。Cephシステムをセットアップする人は、これらの欠点に注意する必要があります。

特に、任意のユーザマシン、特にポータブルマシンは、Cephと直接対話するように設定すべきではありません。そのような使い方をすると、安全でないマシンに平文の認証キーを保存する必要があるからです。そのマシンを盗んだり、こっそりアクセスしたりすると、自分のマシンをCephで認証するための鍵を手に入れることができる。

安全でない可能性のあるマシンがCephオブジェクトストアに直接アクセスすることを許可するのではなく、ユーザは、目的に応じた十分なセキュリティを提供する方法を使用して、環境内の信頼できるマシンにサインインするよう要求されるべきです。 その信頼できるマシンは、人間のユーザ用の平文のCeph鍵を保存します。 Cephの将来のバージョンでは、このような特定の認証の問題がより完全に解決される可能性があります。

現時点では、Ceph認証プロトコルはいずれも転送中のメッセージの機密性を提供しない。したがって、通信中の盗聴者は、Cephのクライアントとサーバ間で送信されたすべてのデータを、作成または変更できないとしても、聞き取り、理解することができます。さらに、Cephにはオブジェクトストアのユーザデータを暗号化するオプションがありません。もちろん、ユーザは自分のデータを手作業で暗号化してCephオブジェクトストアに保存できますが、Cephにはオブジェクトの暗号化自体を実行する機能はありません。Cephに機密データを保存する場合は、Cephシステムにデータを提供する前に、データの暗号化を検討する必要があります。
