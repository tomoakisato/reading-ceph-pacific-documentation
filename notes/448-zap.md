# 448: zap

**クリップソース:** [448: zap — Ceph Documentation](https://docs.ceph.com/en/pacific/ceph-volume/lvm/zap/)

# zap[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/zap/ "Permalink to this headline")

このサブコマンドは、ceph OSDによって使用されたLV、パーティション、RAWデバイスを再利用できるようにzapするために使用されます。論理ボリュームのパスを指定する場合は、vg/lvの形式である必要があります。指定されたlvまたはパーティションに存在するファイルシステムはすべて削除され、すべてのデータがパージされます。

注：LVやパーティションはそのまま残ります。

注：論理ボリューム、Rawデバイス、パーティションが、ceph関連のマウントポイントに使用されている場合、それらはアンマウントされます。

論理ボリュームをザッピングするには：

```
ceph-volume lvm zap {vg name/lv name}
```

パーティションをザッピングするには：

```
ceph-volume lvm zap /dev/sdc1
```

## Removing Devices[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/zap/ "Permalink to this headline")

ザッピングの際、デバイス \(lv、vg、パーティション\) の完全な削除を希望する場合は、\-\-destroy フラグを使用します。一般的な使用例としては、単にrawデバイス全体を使用して OSD をデプロイすることです。そうして、そのデバイスを別のOSDに再利用したい場合、rawデバイス上でceph\-volumeが作成したvgsとlvを削除するために、zapping時に\-\-destroyフラグを使用する必要があります。

注：一度に複数のデバイスを受け付け、それらをすべてザッピングするることが可能

rawデバイスをザッピングし、存在するVGやLVを破壊するには：

```
ceph-volume lvm zap /dev/sdc --destroy
```

この操作は、パーティションや論理ボリュームに対しても行うことができます：

```
ceph-volume lvm zap /dev/sdc1 --destroy
ceph-volume lvm zap osd-vg/data-lv --destroy
```

OSD IDとOSD FSIDでフィルタリングすれば、複数のデバイスを検出することができます。どちらかの識別子を使用することも、両方を同時に使用することも可能です。これは、特定のIDに関連する複数のデバイスをパージする必要があるような状況で有用です。FSIDを使用する場合、フィルタリングはより厳しくなり、あるIDに関連する他の（おそらく無効な）デバイスにマッチしなくなります。

IDのみの場合：

```
ceph-volume lvm zap --destroy --osd-id 1
```

FSIDのみの場合：

```
ceph-volume lvm zap --destroy --osd-fsid 2E8FBE58-0328-4E3B-BFB7-3CACE4E9A6CE
```

両方での場合：

```
ceph-volume lvm zap --destroy --osd-fsid 2E8FBE58-0328-4E3B-BFB7-3CACE4E9A6CE --osd-id 1
```

警告：ザッピングされる OSD ID に関連する systemd ユニットがrunningであると検出された場合、そのデーモンが停止されるまで、ツールはザッピングを拒否します。
