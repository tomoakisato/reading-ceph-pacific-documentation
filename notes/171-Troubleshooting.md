# 171: Troubleshooting

**クリップソース:** [171: Troubleshooting — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/troubleshooting/)

# Troubleshooting[¶](https://docs.ceph.com/en/pacific/cephfs/troubleshooting/ "Permalink to this headline")

## Slow/stuck operations[¶](https://docs.ceph.com/en/pacific/cephfs/troubleshooting/ "Permalink to this headline")

明らかなハングオペレーションが発生した場合、まず、クライアント、MDS、それらを接続するネットワークのどこで問題が発生しているかを特定する必要があります。まず、どちらかで動作が止まっていないかどうか（後述の「[遅い要求（MDS）](https://docs.ceph.com/en/pacific/cephfs/troubleshooting/#slow-requests)」）を確認し、そこから問題を絞り込みます。

MDSのキャッシュをダンプすることで、何が起こっているのかヒントを得ることができます

```
ceph daemon mds.<name> dump cache /tmp/dump.txt
```

注：ファイル dump.txt はMDSを実行しているマシン上にあり、systemd で制御されたMDSサービスの場合、これはMDSコンテナ内の tmpfs 内にあります。dump.txt の場所を特定するには nsenter\(1\) を使用するか、他のシステム全体のパスを指定してください。

MDSに高いログレベルが設定されている場合、問題の診断と解決に必要な情報はほぼ間違いなくそのMDSに記録されます。

## RADOS Health[¶](https://docs.ceph.com/en/pacific/cephfs/troubleshooting/ "Permalink to this headline")

CephFSのメタデータやデータプールの一部が利用できず、CephFSが応答しない場合は、RADOS自体が不健全である可能性があります。まずはそれらの問題を解決してください（[トラブルシューティング](https://docs.ceph.com/en/pacific/rados/troubleshooting/)）。

## The MDS[¶](https://docs.ceph.com/en/pacific/cephfs/troubleshooting/ "Permalink to this headline")

MDS内で操作がハングアップした場合、最終的にceph healthに表示され、「遅いリクエストがブロックされている（slow requests are blocked）」ことが特定されます。また、クライアントが「応答しない（failing to respond）」またはその他の方法で誤動作していると特定される場合もあります。MDSが特定のクライアントの挙動不審を特定した場合、その原因を調査する必要があります。

一般的には、次のような結果になります。

1. システムの過負荷（RAMに余裕がある場合は、「mds cache memory limit」の設定をデフォルトの1GiBから増やしてください。MDSのキャッシュよりも大きなアクティブファイルセットを持つことが、この問題の第一の原因です）
2. 古い（動作不良の）クライアントが動作している
3. 根底にあるRADOSの問題

そうでない場合は、おそらく新しいバグを発見したので、開発者に報告してください。

### Slow requests \(MDS\)[¶](https://docs.ceph.com/en/pacific/cephfs/troubleshooting/ "Permalink to this headline")

以下をMDSホストで実行することで、管理ソケットから現在の操作内容を一覧表示することができます：

```
ceph daemon mds.<name> dump_ops_in_flight
```

スタックしているコマンドを特定し、なぜスタックしているのかを調べます。通常、最後の「event」はロックの収集、または操作のMDSログへの送信であったはずです。もしOSDで待機しているのであれば、それを修正します。特定のinodeで動作が止まっている場合、クライアントがダーティデータをフラッシュしようとしているため、または、CephFSの分散ファイルロックコード（file capabilities system「CAPS」）のバグが原因で、他のユーザーがそのinodeを使用できないようにするCAPSを保持しているクライアントがいる可能性があります。

もし、CAPSコードのバグの結果であれば、MDSを再起動することで問題が解決する可能性があります。

MDS上で遅いリクエストが報告されず、クライアントの誤動作も報告されない場合、クライアントに問題があるか、そのリクエストがMDSに届いていないかのどちらかです。

## ceph\-fuse debugging[¶](https://docs.ceph.com/en/pacific/cephfs/troubleshooting/ "Permalink to this headline")

ceph\-fuseはdump\_ops\_in\_flightもサポートしています。スタックしているかどうか、どこでスタックしているかを確認します。

### Debug output[¶](https://docs.ceph.com/en/pacific/cephfs/troubleshooting/ "Permalink to this headline")

ceph\-fuseからより多くのデバッグ情報を取得するには、コンソールへのログ記録\(\-d\)とクライアントデバッグ\(\-\-debug\-client=20\)、メッセージ送信ごとの印刷\(\-\-debug\-ms=1\)を有効にしてフォアグラウンドで実行してみてください。

モニタに問題があると思われる場合は、モニタデバッグも有効にしてください（\-\-debug\-monc=20）。

## Kernel mount debugging[¶](https://docs.ceph.com/en/pacific/cephfs/troubleshooting/ "Permalink to this headline")

カーネルクライアントに問題がある場合、最も重要なことは、その問題がカーネルクライアントとMDSのどちらにあるのかを見極めることです。一般的に、これは簡単に解決することができます。カーネルクライアントが直接壊れた場合、dmesgに出力があります。dmesgと不適切なカーネルの状態をすべて収集します。

### Slow requests[¶](https://docs.ceph.com/en/pacific/cephfs/troubleshooting/ "Permalink to this headline")

カーネルクライアントは管理ソケットをサポートしていませんが、カーネルでdebugfsが有効になっていれば、似たようなインターフェイスがあります。sys/kernel/debug/ceph/ にフォルダがあり、そのフォルダ \(28f7427e\-5558\-4ffd\-ae1a\-51ec3042759a.client25386880 のような名前\) には参考となる情報を cat 可能なさまざまなファイルが含まれています。遅いリクエストをデバッグするときに最も参考となるのは、mdsc と osdc ファイルでしょう。

* bdi: Cephシステムに関するBDI情報\(ブロックがダーティである、writeされているなど\)
* caps: メモリ内のファイル "caps "構造体の数と使用数
* client\_options: CephFSマウントに指定されたオプションをダンプする
* dentry\_lru: 現在メモリ内にあるCephFSのdentryをダンプする
* mdsc: MDSへの現在のリクエストをダンプする
* mdsmap: 現在のMDSMapのエポックとMDSをダンプする
* mds\_sessions: MDSへの現在のセッションをダンプする
* monc: モニタから現在のマップと、保持されている "サブスクリプション "をダンプする
* monmap: 現在のモニタマップのエポックとモニタをダンプする
* osdc: 現在OSDで実行中のOPS（つまり、ファイルデータI/O）をダンプする
* osdmap: 現在のOSDMapエポック、プール、OSDをダンプする

スタックしているリクエストはないが、ファイルI/Oが進行していない場合は、以下の可能性があります。

## Disconnected\+Remounted FS[¶](https://docs.ceph.com/en/pacific/cephfs/troubleshooting/ "Permalink to this headline")

CephFSは「一貫したキャッシュ」を持っているため、ネットワーク接続が十分な時間中断されると、クライアントはシステムから強制的に切り離されることになります。このとき、カーネルクライアントは窮地に立たされます：ダーティなデータを安全に書き戻すことができず、多くのアプリケーションはclose\(\)時のI/Oエラーを正しく処理しません。カーネルクライアントはFSを再マウントしますが、未処理のファイルシステムI/Oが満たされ場合と満たされない場合があります。このような場合、クライアントシステムをリブートする必要があるかもしれません。

dmesg/kern.logに次のようなレポートがある場合、上記の状況にあることを確認できます：

```
Jul 20 08:14:38 teuthology kernel: [3677601.123718] ceph: mds0 closed our session
Jul 20 08:14:38 teuthology kernel: [3677601.128019] ceph: mds0 reconnect start
Jul 20 08:14:39 teuthology kernel: [3677602.093378] ceph: mds0 reconnect denied
Jul 20 08:14:39 teuthology kernel: [3677602.098525] ceph:  dropping dirty+flushing Fw state for ffff8802dc150518 1099935956631
Jul 20 08:14:39 teuthology kernel: [3677602.107145] ceph:  dropping dirty+flushing Fw state for ffff8801008e8518 1099935946707
Jul 20 08:14:39 teuthology kernel: [3677602.196747] libceph: mds0 172.21.5.114:6812 socket closed (con state OPEN)
Jul 20 08:14:40 teuthology kernel: [3677603.126214] libceph: mds0 172.21.5.114:6812 connection reset
Jul 20 08:14:40 teuthology kernel: [3677603.132176] libceph: reset on mds0
```

これは、動作の改善が現在進行中の分野です。カーネルはまもなく進行中のI/Oに対して確実にエラーコードを発行するようになるでしょうが、 アプリケーションはそれをうまく処理できないかもしれません。長期的には、POSIXのセマンティクスに反しない場合には、（他のクライアントからアクセスされたり変更されていない）データの再接続と再請求を可能にしたいと考えています。

## Mounting[¶](https://docs.ceph.com/en/pacific/cephfs/troubleshooting/ "Permalink to this headline")

### Mount 5 Error[¶](https://docs.ceph.com/en/pacific/cephfs/troubleshooting/ "Permalink to this headline")

マウント時のエラー5は通常、MDSサーバーが遅延しているか、クラッシュしている場合に発生します。少なくとも1つのMDSが稼働しており、クラスタがアクティブかつ健全であることを確認してください。

### Mount 12 Error[¶](https://docs.ceph.com/en/pacific/cephfs/troubleshooting/ "Permalink to this headline")

マウント時エラー12（cannot allocate memory）は、通常、[Ceph Client](https://docs.ceph.com/en/pacific/glossary/#term-Ceph-Client)バージョンと[Ceph Storage Cluster](https://docs.ceph.com/en/pacific/glossary/#term-Ceph-Client)バージョンの間に不一致がある場合に発生します。以下を使用してバージョンを確認します：

```
ceph -v
```

CephクライアントがCephクラスタのバージョンから遅れている場合、アップグレードを試みます：

```
sudo apt-get update && sudo apt-get install ceph-common
```

ceph\-commoの最新版を入手（uninstall、autoclean、autoremoveしてから再install）する必要があるかもしれません。

## Dynamic Debugging[¶](https://docs.ceph.com/en/pacific/cephfs/troubleshooting/ "Permalink to this headline")

CephFSモジュールに対するダイナミックデバッグを有効にすることができます。

参考： [https://github.com/ceph/ceph/blob/master/src/script/kcon\_all.sh](https://github.com/ceph/ceph/blob/master/src/script/kcon_all.sh)

## Reporting Issues[¶](https://docs.ceph.com/en/pacific/cephfs/troubleshooting/ "Permalink to this headline")

具体的な問題を発見された方は、できるだけ多くの情報を添えてご報告ください。以下は特に重要な情報です：

* クライアントとサーバーにインストールされているCephのバージョン
* カーネルまたはFUSEクライアントのどちらを使用しているか
* カーネルクライアントを使用している場合、カーネルバージョン
* クライアント数とワークロードの種類
* システムが「stuck」した場合、その影響が全クライアントに及んでいるか、それとも1つのクライアントだけか
* cephのヘルスメッセージ
* クラッシュした際のcephログに含まれるバックトレース

バグを発見した場合、バグトラッカーに報告してください。一般的な質問については、 ceph\-users メーリングリストに書き込んでください。
