# 285: OpenID Connect Provider in RGW

**クリップソース:** [285: OpenID Connect Provider in RGW — Ceph Documentation](https://docs.ceph.com/en/pacific/radosgw/oidc/)

# OpenID Connect Provider in RGW[¶](https://docs.ceph.com/en/pacific/radosgw/oidc/ "Permalink to this headline")

両者の信頼関係を確立するために、OpenID Connect Providerを記述したエンティティをRGWに作成する必要があります。

## REST APIs for Manipulating an OpenID Connect Provider[¶](https://docs.ceph.com/en/pacific/radosgw/oidc/ "Permalink to this headline")

RGW上にOpenID Connect Providerエンティティを作成・管理するには、以下のREST APIを使用します。

REST admin API を呼び出すには、admin の権限を持つユーザーを作成する必要があります。

```
radosgw-admin --uid TESTER --display-name "TestUser" --access_key TESTER --secret test123 user create
radosgw-admin caps add --uid="TESTER" --caps="oidc-provider=*"
```

### Create OpenID Connect Provider[¶](https://docs.ceph.com/en/pacific/radosgw/oidc/ "Permalink to this headline")

RGWにOpenID Connect Providerエンティティを作成します。

#### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/oidc/ "Permalink to this headline")

**ClientIDList.member.N**

Description

S3リソースにアクセスする必要があるクライアントIDのリスト

Type

Array of Strings

**ThumbprintList.member.N**

Description

OpenID Connect IDPのサーバ証明書のサムプリントのリスト。最大5つまで登録可能

Type

Array of Strings

**Url**

Description

IDPのURL

Type

String

Example::

POST “\<hostname\>?Action=Action=CreateOpenIDConnectProvider

&ThumbprintList.list.1=F7D7B3515DD0D319DD219A43A9EA727AD6065287 &ClientIDList.list.1=app\-profile\-jsp &Url=http://localhost:8080/auth/realms/quickstart

### DeleteOpenIDConnectProvider[¶](https://docs.ceph.com/en/pacific/radosgw/oidc/ "Permalink to this headline")

RGW上のOpenID Connect Providerエンティティを削除します。

#### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/oidc/ "Permalink to this headline")

**OpenIDConnectProviderArn**

Description

Create APIが返したIDPのARN

Type

String

Example::

POST “\<hostname\>?Action=Action=DeleteOpenIDConnectProvider

&OpenIDConnectProviderArn=arn:aws:iam:::oidc\-provider/localhost:8080/auth/realms/quickstart

### GetOpenIDConnectProvider[¶](https://docs.ceph.com/en/pacific/radosgw/oidc/ "Permalink to this headline")

IDPに関する情報を取得します。

#### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/oidc/ "Permalink to this headline")

**OpenIDConnectProviderArn**

Description

Create APIが返したIDPのARN

Type

String

Example::

POST “\<hostname\>?Action=Action=GetOpenIDConnectProvider

&OpenIDConnectProviderArn=arn:aws:iam:::oidc\-provider/localhost:8080/auth/realms/quickstart

### ListOpenIDConnectProviders[¶](https://docs.ceph.com/en/pacific/radosgw/oidc/ "Permalink to this headline")

IDPに関する情報をリストアップします。

#### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/oidc/ "Permalink to this headline")

None

Example::

POST “\<hostname\>?Action=Action=ListOpenIDConnectProviders
