# 309: Crash Module

|# 309: Crash Module — Ceph Documentation

**ソース URL:** [https://docs.ceph.com/en/pacific/mgr/crash/](https://docs.ceph.com/en/pacific/mgr/crash/)

# Crash Module[¶](https://docs.ceph.com/en/pacific/mgr/crash/ "Permalink to this headline")

クラッシュモジュールは、デーモンのクラッシュダンプに関する情報を収集し、後の解析のためにCephクラスタに保存します。

デーモンのクラッシュダンプは、デフォルトでは /var/lib/ceph/crash にダンプされます。これは、オプション 'crash dir' で設定することができます。 クラッシュディレクトリは、日時とランダムに生成される UUID によって名付けられ、同じ "crash\_id" を持つメタデータファイル 'meta' と最近のログファイルを含んでいます。このモジュールにより、それらのダンプに関するメタデータをモニタのストレージに永続化することができます。

## Enabling[¶](https://docs.ceph.com/en/pacific/mgr/crash/ "Permalink to this headline")

クラッシュモジュールを有効にします：

```
ceph mgr module enable crash
```

## Commands[¶](https://docs.ceph.com/en/pacific/mgr/crash/ "Permalink to this headline")

```
ceph crash post -i <metafile>
```

クラッシュダンプを保存します。 metadataファイルは、crash dirにmetaとして格納されているJSON blobです。通常通り、cephコマンドは\-i \-で起動でき、stdinから読み込みます。

```
ceph crash rm <crashid>
```

指定したクラッシュダンプを削除します。

```
ceph crash ls
```

すべての（新規およびアーカイブされた）クラッシュ情報のタイムスタンプ/UUID crashid をリストアップします。

```
ceph crash ls-new
```

新規のクラッシュ情報のタイムスタンプ/UUID crashids をリストアップします。

```
ceph crash stat
```

保存済みクラッシュ情報の概要を年齢別に表示します。

```
ceph crash info <crashid>
```

保存済みクラッシュの詳細をすべて表示します。

```
ceph crash prune <keep>
```

keep日より古い保存済みクラッシュを削除します。 \<keep\>は整数でなければなりません。

```
ceph crash archive <crashid>
```

クラッシュレポートをアーカイブして、RECENT\_CRASHヘルスチェックの対象外とし、crash ls\-new出力に表示されないようにします（crash ls出力には引き続き表示されます）。

```
ceph crash archive-all
```

すべての新しいクラッシュレポートをアーカイブします。

## Options[¶](https://docs.ceph.com/en/pacific/mgr/crash/ "Permalink to this headline")

* mgr/crack/warn\_recent\_interval \[デフォルト：2週間\]は、RECENT\_CRASHヘルス警告を発生させる目的で「最近」を構成するものを制御します。
* mgr/crash/retain\_interval \[デフォルト：1年\]は、クラッシュレポートが自動的に削除されるまでにクラスターによって保持される期間を制御します。

|# 309: Insights Module — Ceph Documentation

**ソース URL:** [https://docs.ceph.com/en/pacific/mgr/insights/](https://docs.ceph.com/en/pacific/mgr/insights/)

# Insights Module[¶](https://docs.ceph.com/en/pacific/mgr/insights/ "Permalink to this headline")

The insights module collects and exposes system information to the Insights Core data analysis framework. It is intended to replace explicit interrogation of Ceph CLIs and daemon admin sockets, reducing the API surface that Insights depends on. The insights reports contains the following:

* **Health reports**
* **Crash reports**
* Software version, storage utilization, cluster maps, placement group summary, monitor status, cluster configuration, and OSD metadata.

## Enabling[¶](https://docs.ceph.com/en/pacific/mgr/insights/ "Permalink to this headline")

The _insights_ module is enabled with:

```
ceph mgr module enable insights

```

## Commands[¶](https://docs.ceph.com/en/pacific/mgr/insights/ "Permalink to this headline")

```
ceph insights

```

Generate the full report.

```
ceph insights prune-health <hours>

```

Remove historical health data older than \<hours\>. Passing 0 for \<hours\> will clear all health data.

This command is useful for cleaning the health history before automated nightly reports are generated, which may contain spurious health checks accumulated while performing system maintenance, or other health checks that have been resolved. There is no need to prune health data to reclaim storage space; garbage collection is performed regularly to remove old health data from persistent storage.
