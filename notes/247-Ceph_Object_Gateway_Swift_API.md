# 247: Ceph Object Gateway Swift API

**クリップソース:** [247: Ceph Object Gateway Swift API — Ceph Documentation](https://docs.ceph.com/en/pacific/radosgw/swift/)

# Ceph Object Gateway Swift API[¶](https://docs.ceph.com/en/pacific/radosgw/swift/ "Permalink to this headline")

Cephは、[Swift API](https://developer.openstack.org/api-ref/object-store/index.html)の基本的なデータアクセスモデルと互換性のあるRESTful APIをサポートしています。

## API[¶](https://docs.ceph.com/en/pacific/radosgw/swift/ "Permalink to this headline")

* [Authentication](https://docs.ceph.com/en/pacific/radosgw/swift/)
* [Service Ops](https://docs.ceph.com/en/pacific/radosgw/swift/)
* [Container Ops](https://docs.ceph.com/en/pacific/radosgw/swift/)
* [Object Ops](https://docs.ceph.com/en/pacific/radosgw/swift/)
* [Temp URL Ops](https://docs.ceph.com/en/pacific/radosgw/swift/)
* [Tutorial](https://docs.ceph.com/en/pacific/radosgw/swift/)
* [Java](https://docs.ceph.com/en/pacific/radosgw/swift/)
* [Python](https://docs.ceph.com/en/pacific/radosgw/swift/)
* [Ruby](https://docs.ceph.com/en/pacific/radosgw/swift/)

## Features Support[¶](https://docs.ceph.com/en/pacific/radosgw/swift/ "Permalink to this headline")

以下の表は、現在のSwift機能のサポート状況について説明したものです：

|Feature                      |Status         |Remarks                             |
|-----------------------------|---------------|------------------------------------|
|**Authentication**           |Supported      |                                    |
|**Get Account Metadata**     |Supported      |                                    |
|**Swift ACLs**               |Supported      |Supports a subset of Swift ACLs     |
|**List Containers**          |Supported      |                                    |
|**Delete Container**         |Supported      |                                    |
|**Create Container**         |Supported      |                                    |
|**Get Container Metadata**   |Supported      |                                    |
|**Update Container Metadata**|Supported      |                                    |
|**Delete Container Metadata**|Supported      |                                    |
|**List Objects**             |Supported      |                                    |
|**Static Website**           |Supported      |                                    |
|**Create Object**            |Supported      |                                    |
|**Create Large Object**      |Supported      |                                    |
|**Delete Object**            |Supported      |                                    |
|**Get Object**               |Supported      |                                    |
|**Copy Object**              |Supported      |                                    |
|**Get Object Metadata**      |Supported      |                                    |
|**Update Object Metadata**   |Supported      |                                    |
|**Expiring Objects**         |Supported      |                                    |
|**Temporary URLs**           |Partial Support|No support for container\-level keys|
|**Object Versioning**        |Partial Support|No support for X\-History\-Location |
|**CORS**                     |Not Supported  |                                    |
