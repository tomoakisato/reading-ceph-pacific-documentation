# 310: Orchestrator CLI

**クリップソース:** [310: Orchestrator CLI — Ceph Documentation](https://docs.ceph.com/en/pacific/mgr/orchestrator/)

# Orchestrator CLI[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator/ "Permalink to this headline")

このモジュールは、オーケストレーターモジュール（外部オーケストレーションサービスとインターフェイスするceph\-mgrモジュール）へのコマンドラインインターフェイス（CLI）を提供します。

オーケストレーターCLIは複数の外部オーケストレーターを統合するため、オーケストレーターモジュールに共通の命名法が必要です。

|_host_        |物理ホストのホスト名（DNS名ではない）。ポッド名、コンテナ名、コンテナ内のホスト名ではない。                                                                                         |
|--------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|_service type_|サービスのタイプ。 例：nfs, mds, osd, mon, rgw, mgr, iscsi                                                                                                                          |
|_service_     |* mdsの場合はfs\_name
* rgwの場合はrgw\_zone
* nfsの場合はganesha\_cluster\_id

論理的なサービス。通常、複数のホスト上の複数のサービスインスタンスで構成され、HAを実現する。            |
|_daemon_      |この識別子は、インスタンスを一意に識別する必要がある。
あるサービスの単一のインスタンス。通常はデーモンだが、そうでない場合もある \(たとえば、LIO や knfsd などのカーネルサービス\)。|

名前の関係は以下の通り：

* _service_
* _daemon_

注：Orchestratorモジュールは、以下に示すコマンドのサブセットしか実装していない場合があります。また、コマンドの実装はモジュールによって異なる場合があります。

## Status[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator/ "Permalink to this headline")

```
ceph orch status [--detail]
```

現在のオーケストレーターモードと高レベルの状態（オーケストレータープラグインが利用可能かどうか、動作可能かどうか）を表示します。

## Stateless services \(MDS/RGW/NFS/rbd\-mirror/iSCSI\)[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator/ "Permalink to this headline")

\(注意: オーケストレーターはサービスの設定を行いません。サービスの設定の詳細については、対応するドキュメントを参照してください\)。

nameパラメータは、インスタンスのグループの識別子です：

* MDSデーモンのグループの場合はCephFSファイルシステム
* RGWのグループの場合はゾーン名

サービスを作る／増やす／縮小する／削除する：

```
ceph orch apply mds <fs_name> [--placement=<placement>] [--dry-run]
ceph orch apply rgw <name> [--realm=<realm>] [--zone=<zone>] [--port=<port>] [--ssl] [--placement=<placement>] [--dry-run]
ceph orch apply nfs <name> <pool> [--namespace=<namespace>] [--placement=<placement>] [--dry-run]
ceph orch rm <service_name> [--force]
```

placement は [Daemon Placement](https://docs.ceph.com/en/pacific/mgr/orchestrator/) です。

e.g., ceph orch apply mds myfs \-\-placement="3 host1 host2 host3"

サービスコマンド：

```
ceph orch <start|stop|restart|redeploy|reconfig> <service_name>
```

## Configuring the Orchestrator CLI[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator/ "Permalink to this headline")

オーケストレータを有効にするには、set backendコマンドで使用するオーケストレーターモジュールを選択します：

```
ceph orch set backend <module>
```

例えば、Rookオーケストレータモジュールを有効にして、CLIで使用する場合：

```
ceph mgr module enable rook
ceph orch set backend rook
```

バックエンドが正しく設定されているか確認するには：

```
ceph orch status
```

### Disable the Orchestrator[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator/ "Permalink to this headline")

オーケストレータを無効にする場合は、空文字列 "" を使用します：

```
ceph orch set backend ""
ceph mgr module disable rook
```

## Current Implementation Status[¶](https://docs.ceph.com/en/pacific/mgr/orchestrator/ "Permalink to this headline")

現在のオーケストレータの実装状況について説明します。

|Command                        |Rook|Cephadm|
|-------------------------------|----|-------|
|apply iscsi                    |⚪  |✔      |
|apply mds                      |✔   |✔      |
|apply mgr                      |⚪  |✔      |
|apply mon                      |✔   |✔      |
|apply nfs                      |✔   |✔      |
|apply osd                      |✔   |✔      |
|apply rbd\-mirror              |✔   |✔      |
|apply rgw                      |✔   |✔      |
|apply container                |⚪  |✔      |
|host add                       |⚪  |✔      |
|host ls                        |✔   |✔      |
|host rm                        |⚪  |✔      |
|daemon status                  |⚪  |✔      |
|daemon {stop,start,…}         |⚪  |✔      |
|device {ident,fault}\-\(on,off}|⚪  |✔      |
|device ls                      |✔   |✔      |
|iscsi add                      |⚪  |✔      |
|mds add                        |⚪  |✔      |
|nfs add                        |⚪  |✔      |
|rbd\-mirror add                |⚪  |✔      |
|rgw add                        |⚪  |✔      |
|ps                             |✔   |✔      |

where

* ⚪ = not yet implemented
* ❌ = not applicable
* ✔ = implemented
