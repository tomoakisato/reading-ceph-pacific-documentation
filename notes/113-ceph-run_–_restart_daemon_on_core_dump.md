# 113: ceph-run – restart daemon on core dump

 # ceph\-run – restart daemon on core dump[¶](https://docs.ceph.com/en/pacific/man/8/ceph-run/ "Permalink to this headline")

## Synopsis[¶](https://docs.ceph.com/en/pacific/man/8/ceph-run/ "Permalink to this headline")

**ceph\-run** _command_ …

## Description[¶](https://docs.ceph.com/en/pacific/man/8/ceph-run/ "Permalink to this headline")

**ceph\-run** is a simple wrapper that will restart a daemon if it exits with a signal indicating it crashed and possibly core dumped \(that is, signals 3, 4, 5, 6, 8, or 11\).

The command should run the daemon in the foreground. For Ceph daemons, that means the `-f` option.

## Options[¶](https://docs.ceph.com/en/pacific/man/8/ceph-run/ "Permalink to this headline")

None

## Availability[¶](https://docs.ceph.com/en/pacific/man/8/ceph-run/ "Permalink to this headline")

**ceph\-run** is part of Ceph, a massively scalable, open\-source, distributed storage system. Please refer to the Ceph documentation at [http://ceph.com/docs](https://docs.ceph.com/en/pacific/man/8/ceph-run/) for more information.

## See also[¶](https://docs.ceph.com/en/pacific/man/8/ceph-run/ "Permalink to this headline")

[ceph](https://docs.ceph.com/en/pacific/man/8/ceph-run/)\(8\),[ceph\-mon](https://docs.ceph.com/en/pacific/man/8/ceph-run/)\(8\),[ceph\-mds](https://docs.ceph.com/en/pacific/man/8/ceph-run/)\(8\),[ceph\-osd](https://docs.ceph.com/en/pacific/man/8/ceph-run/)\(8\)
