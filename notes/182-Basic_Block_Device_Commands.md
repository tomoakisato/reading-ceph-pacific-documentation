# 182: Basic Block Device Commands

**クリップソース:** [182: Basic Block Device Commands — Ceph Documentation](https://docs.ceph.com/en/pacific/rbd/rados-rbd-cmds/)

# Basic Block Device Commands[¶](https://docs.ceph.com/en/pacific/rbd/rados-rbd-cmds/ "Permalink to this headline")

rbdコマンドは、ブロックデバイスのイメージの作成、リストアップ、イントロスペクション、削除を可能にします。また、イメージのクローン、スナップショットの作成、イメージをスナップショットにロールバックする、スナップショットを表示する、などにも使用できます。rbdコマンドの使い方については、[RBD \- RADOS Block Device \(RBD\) イメージの管理](https://docs.ceph.com/en/pacific/man/8/rbd/) を参照してください。

重要: Ceph Block Deviceコマンドを使用するには、実行中のCephクラスタにアクセスできる必要があります。

## Create a Block Device Pool[¶](https://docs.ceph.com/en/pacific/rbd/rados-rbd-cmds/ "Permalink to this headline")

1. 管理ノードで、ceph ツールを使用して
2. 管理ノードで、rbdツールを使用して、RBDが使用するプールを初期化します。

注意：rbdツールは、指定がない場合、デフォルトのプール名'rbd'を使用します。

## Create a Block Device User[¶](https://docs.ceph.com/en/pacific/rbd/rados-rbd-cmds/ "Permalink to this headline")

指定しない限り、rbdコマンドはadminというIDを使用してCephクラスタにアクセスします。このIDは、クラスタへの完全な管理アクセスを許可します。可能な限り、より制限されたユーザを利用することをお勧めします。

[Cephユーザを作成する](https://docs.ceph.com/en/pacific/rados/operations/user-management#add-a-user)には、cephでauthget\-or\-createコマンド、ユーザ名、モニタCAPS、OSD CAPSを指定します。

```
ceph auth get-or-create client.{ID} mon 'profile rbd' osd 'profile {profile name} [pool={pool-name}][, profile ...]' mgr 'profile rbd [pool={pool-name}]'
```

例えば、qemu というユーザ ID を作成し、プール vms への読み書き権限とプールイメージへの 読み取り専用権限を与えるには、次のように実行します：

```
ceph auth get-or-create client.qemu mon 'profile rbd' osd 'profile rbd pool=vms, profile rbd-read-only pool=images' mgr 'profile rbd pool=images'
```

ceph auth get\-or\-createコマンドの出力は、指定したユーザのキーリングになり、/etc/ceph/ceph.client.{ID}.keyringに書き込むことができます。

注：rbdコマンド使用時にオプション引数\-\-id {id}でユーザIDを指定することができます。

## Creating a Block Device Image[¶](https://docs.ceph.com/en/pacific/rbd/rados-rbd-cmds/ "Permalink to this headline")

ノードにブロックデバイスを追加する前に、まず[Ceph Storage Cluster](https://docs.ceph.com/en/pacific/glossary/#term-Ceph-Storage-Cluster)でそのデバイスのイメージを作成する必要があります。ブロックデバイスのイメージを作成するには、以下を実行します：

```
rbd create --size {megabytes} {pool-name}/{image-name}
```

例えば、swimmingpool という名前のプールに情報を格納する bar という名前の 1GB イメージを作成する場合、次のように実行します：

```
rbd create --size 1024 swimmingpool/bar
```

イメージ作成時にpoolを指定しない場合、デフォルトのプールrbdに格納されます。たとえば、foo という名前の 1GB イメージをデフォルトのプール rbd に格納する場合、次のように実行します：

```
rbd create --size 1024 foo
```

注：プールをソースとして指定する前に、まずプールを作成する必要があります。詳細については、「[ストレージプール](https://docs.ceph.com/en/pacific/rados/operations/pools)」を参照してください。

## Listing Block Device Images[¶](https://docs.ceph.com/en/pacific/rbd/rados-rbd-cmds/ "Permalink to this headline")

rbd プールのブロックデバイスを一覧表示するには、以下を実行します \(例: rbd はデフォルトのプール名\)：

```
rbd ls
```

特定のプール内のブロックデバイスを一覧表示するには、以下を実行します。 {poolname} はプール名に置き換えてください：

```
rbd ls {poolname}
```

For example:

```
rbd ls swimmingpool
```

rbdプール内の遅延削除ブロックデバイスを一覧表示するには、次のコマンドを実行します。：

```
rbd trash ls
```

特定のプール内の遅延削除ブロックデバイスを一覧表示するには、次のコマンドを実行します。{poolname}はプールの名前に置き換えてください：

```
rbd trash ls {poolname}
```

For example:

```
rbd trash ls swimmingpool
```

## Retrieving Image Information[¶](https://docs.ceph.com/en/pacific/rbd/rados-rbd-cmds/ "Permalink to this headline")

特定のイメージから情報を取得する場合は、{image\-name}をイメージの名前に置き換えて、次のように実行します：

```
rbd info {image-name}
```

For example:

```
rbd info foo
```

プール内のイメージから情報を取得するには、次のコマンドを実行します。{image\-name}をイメージの名前に置き換え、{pool\-name}をプールの名前に置き換えてください：

```
rbd info {pool-name}/{image-name}
```

For example:

```
rbd info swimmingpool/bar
```

## Resizing a Block Device Image[¶](https://docs.ceph.com/en/pacific/rbd/rados-rbd-cmds/ "Permalink to this headline")

Ceph Block Deviceのイメージはシンプロビジョニングされています。データの保存を開始するまで、実際に物理ストレージを使用することはありません。ただし、\-\-sizeオプションで設定した最大容量があります。Ceph Block Deviceイメージの最大サイズを増加\(または減少\)させたい場合は、以下を実行します：

```
rbd resize --size 2048 foo (to increase)
rbd resize --size 2048 foo --allow-shrink (to decrease)
```

## Removing a Block Device Image[¶](https://docs.ceph.com/en/pacific/rbd/rados-rbd-cmds/ "Permalink to this headline")

ブロックデバイスを削除するには、以下を実行します。{image\-name}は削除したいイメージの名前に置き換えてください：

```
rbd rm {image-name}
```

For example:

```
rbd rm foo
```

ブロックデバイスをプールから削除するには、次の手順を実行します。{image\-name}を削除するイメージの名前に置き換え、{pool\-name}をプールの名前に置き換えてください：

```
rbd rm {pool-name}/{image-name}
```

For example:

```
rbd rm swimmingpool/bar
```

プールからブロックデバイスの削除を延期するには、以下を実行します。 {image\-name} は移動するイメージの名前に、{pool\-name} はプールの名前に置き換えてください：

```
rbd trash mv {pool-name}/{image-name}
```

For example:

```
rbd trash mv swimmingpool/bar
```

遅延ブロックデバイスをプールから削除するには、次の手順を実行します。{image\-id}をイメージのIDに置き換えて削除し、{pool\-name}をプールの名前に置き換えてください：

```
rbd trash rm {pool-name}/{image-id}
```

For example:

```
rbd trash rm swimmingpool/2bf4474b0dc51
```

Note:

* スナップショットやクローンが使用中のイメージもゴミ箱に移動できますが、ゴミ箱から削除することはできません。
* \-\-expires\-atで猶予時間を設定でき（デフォルトは現在）、その猶予時間が経過していない場合は、\-\-forceを使用しない限り削除することはできません。

## Restoring a Block Device Image[¶](https://docs.ceph.com/en/pacific/rbd/rados-rbd-cmds/ "Permalink to this headline")

rbdプール内の遅延削除ブロックデバイスを復元するには、以下を実行します。{image\-id}をイメージのIDに置き換えてください：

```
rbd trash restore {image-id}
```

For example:

```
rbd trash restore 2bf4474b0dc51
```

特定のプールで遅延削除ブロックデバイスを復元するには、次の手順を実行します。{image\-id}をイメージのIDに置き換え、{pool\-name}をプールの名前に置き換えてください：

```
rbd trash restore {pool-name}/{image-id}
```

For example:

```
rbd trash restore swimmingpool/2bf4474b0dc51
```

また、\-\-imageを使用すると、復元中にイメージの名前を変更することができます。

For example:

```
rbd trash restore swimmingpool/2bf4474b0dc51 --image new-name
```
