# 71: Pools

**クリップソース:** [71: Pools — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/operations/pools/)

# Pools[¶](https://docs.ceph.com/en/pacific/rados/operations/pools/ "Permalink to this headline")

プールは、オブジェクトを格納するための論理的なパーティションです。

プールを作成せずに初めてクラスタをデプロイした場合、Cephはデータの保存にデフォルトのプールを使用します。プールは以下を提供します。

* **Resilience**
* **Placement Groups**
* **CRUSH Rules**
* **Snapshots**

データをプールに整理するために、プールの一覧表示、作成、削除を行うことができます。また、各プールの利用統計も表示できます。

## List Pools[¶](https://docs.ceph.com/en/pacific/rados/operations/pools/ "Permalink to this headline")

クラスタのプールを一覧表示するには：

```
ceph osd lspools
```

## Create a Pool[¶](https://docs.ceph.com/en/pacific/rados/operations/pools/ "Permalink to this headline")

プールを作成する前に、[Pool, PG and CRUSH Config Reference](https://docs.ceph.com/en/pacific/rados/operations/pools/)を参照してください。Ceph設定ファイルのPG数のデフォルト値は理想的な値ではないため、オーバーライドするのが理想的です。PG数の詳細については、「[setting the number of placement groups](https://docs.ceph.com/en/pacific/rados/operations/pools/)」を参照してください。[60: Pool, PG and CRUSH Config Reference](60-Pool,_PG_and_CRUSH_Config_Reference.md)

注意：Luminousから、すべてのプールは、プールを使用するアプリケーションに関連付ける必要があります。詳しくは下記の「[Associate Pool to Application](https://docs.ceph.com/en/pacific/rados/operations/pools/)」をご覧ください。

For example:

```
osd pool default pg num = 100
osd pool default pgp num = 100
```

To create a pool, execute:

```
ceph osd pool create {pool-name} [{pg-num} [{pgp-num}]] [replicated] 
     [crush-rule-name] [expected-num-objects]
ceph osd pool create {pool-name} [{pg-num} [{pgp-num}]]   erasure 
     [erasure-code-profile] [crush-rule-name] [expected_num_objects] [--autoscale-mode=<on,off,warn>]
```

Where:

**{pool\-name}**

Description

プールの名前。一意でなければならない

Type

String

Required

Yes.

**{pg\-num}**

Description

プールのPGの総数。適切な数の計算の詳細については、Placement Groupsを参照。デフォルト値の8は、ほとんどのシステムで適切ではない

Type

Integer

Required

Yes.

Default

8

**{pgp\-num}**

Description

配置を目的としたPGの総数。PG分割のシナリオを除いて、PGの総数と同じにする必要がある

Type

Integer

Required

Yes. Picks up default or Ceph configuration value if not specified.

Default

8

**{replicated|erasure}**

Description

プールのタイプは、オブジェクトの複数のコピーを保持することによって失われたOSDから回復するための「replicated」、または一種の汎用RAID5機能を得るための「erasure」のいずれか。replicatedプールはより多くのrawストレージを必要とするが、すべてのCeph操作を実装する。ECプールは、より少ないrawストレージしか必要としないが、利用可能な操作のサブセットのみを実装している。

Type

String

Required

No.

Default

replicated

**\[crush\-rule\-name\]**

Description

このプールに使用するCRUSHルールの名前。 指定されたルールは存在しなければならない

Type

String

Required

No.

Default

replicatedプールの場合は、osd pool default crush rule構成変数で指定されたルール。 このルールは存在しなければならない。ECプールの場合、default erasure codeプロファイルが使用されている場合はerasure\-code、そうでない場合は{pool\-name}。 このルールは、まだ存在しない場合、作成される

**\[erasure\-code\-profile=profile\]**

Description

ECプールのみ対象。ECプロファイルを使用する。osd erasure\-code\-profile setで定義された既存のプロファイルである必要がある

Type

String

Required

No.

**\-\-autoscale\-mode=\<on,off,warn\>**

Description

オートスケールモード

Type

String

Required

No.

Default

デフォルトの動作は、osd pool default pg autoscale modeオプションによって制御される。

autoscaleモードをonまたはwarnに設定すると、実際の使用に基づいてプール内のPG数の変更をシステムに自動調整させたり、推奨させたりすることができる。 オフのままにした場合は、「[Placement Groups](https://docs.ceph.com/en/pacific/rados/operations/pools/)」を参照

**\[expected\-num\-objects\]**

Description

このプールのオブジェクトの予想数。（負のfilestore merge thresholdと共に）この値を設定することにより、PGフォルダ分割はプール作成時に行われ、実行時のフォルダ分割によるレイテンシの影響を回避することができる。

Type

Integer

Required

No.

Default

0、プール作成時に分割を行わない

## Associate Pool to Application[¶](https://docs.ceph.com/en/pacific/rados/operations/pools/ "Permalink to this headline")

プールを使用する前に、アプリケーションと関連付ける必要があります。CephFSで使用するプールやRGWで自動的に作成されるプールは、自動的に関連付けられます。RBDで使用する予定のプールは、rbdツールを使用して初期化する必要があります\(詳細については、「[Block Device Commands](https://docs.ceph.com/en/pacific/rados/operations/pools/)」を参照してください\)。

その他の場合、フリーフォームのアプリケーション名を手動でプールに関連付けることができます。

```
ceph osd pool application enable {pool-name} {application-name}
```

CephFSはアプリケーション名cephfs、RBDはアプリケーション名rbd、RGWはアプリケーション名rgwを使用します。

## Set Pool Quotas[¶](https://docs.ceph.com/en/pacific/rados/operations/pools/ "Permalink to this headline")

プールごとに最大バイト数および/または最大オブジェクト数のプールクォータを設定することができます。

```
ceph osd pool set-quota {pool-name} [max_objects {obj-count}] [max_bytes {bytes}]
```

For example:

```
ceph osd pool set-quota data max_objects 10000
```

クォータを削除するには、その値を0にします。

## Delete a Pool[¶](https://docs.ceph.com/en/pacific/rados/operations/pools/ "Permalink to this headline")

プールを削除するには：

```
ceph osd pool delete {pool-name} [{pool-name} --yes-i-really-really-mean-it]
```

プールを削除するには、Monitor の設定で mon\_allow\_pool\_delete フラグが true に設定されている必要があります。そうでなければ、プールの削除は拒否されます。

詳しくは、「 [Monitor Configuration](https://docs.ceph.com/en/pacific/rados/operations/pools/) 」を参照してください

作成したプールに独自のルールを作成した場合、そのプールが不要になった時点で削除することを検討する必要があります。

```
ceph osd pool get {pool-name} crush_rule
```

例えばルールが「123」だった場合、次のように他のプールを確認することができます。

```
ceph osd dump | grep "^pool" | grep "crush_rule 123"
```

他のプールでそのカスタムルールが使用されていない場合は、そのルールをクラスタから削除しても安全です。

もう存在しないプールに対してのみ権限を持つユーザーを作成した場合、それらのユーザーも削除することを検討する必要があります。

```
ceph auth ls | grep -C 5 {pool-name}
ceph auth del {user}
```

## Rename a Pool[¶](https://docs.ceph.com/en/pacific/rados/operations/pools/ "Permalink to this headline")

プールの名前を変更するには：

```
ceph osd pool rename {current-pool-name} {new-pool-name}
```

プール名を変更し、認証されたユーザにプールごとの能力がある場合、ユーザの能力（CAPS）を新しいプール名で更新する必要があります。

## Show Pool Statistics[¶](https://docs.ceph.com/en/pacific/rados/operations/pools/ "Permalink to this headline")

プールの利用統計情報を表示するには：

```
rados df
```

さらに、特定のプールまたはすべてのプールのI/O情報を取得するには：

```
ceph osd pool stats [{pool-name}]
```

## Make a Snapshot of a Pool[¶](https://docs.ceph.com/en/pacific/rados/operations/pools/ "Permalink to this headline")

プールのスナップショットを作成するには：

```
ceph osd pool mksnap {pool-name} {snap-name}
```

## Remove a Snapshot of a Pool[¶](https://docs.ceph.com/en/pacific/rados/operations/pools/ "Permalink to this headline")

プールのスナップショットを削除するには：

```
ceph osd pool rmsnap {pool-name} {snap-name}
```

## Set Pool Values[¶](https://docs.ceph.com/en/pacific/rados/operations/pools/ "Permalink to this headline")

プールに値を設定するには：

```
ceph osd pool set {pool-name} {key} {value}
```

You may set values for the following keys:

**compression\_algorithm**

Description

BlueStoreに使用するインライン圧縮アルゴリズムを設定する。この設定は、グローバルな設定である bluestore compression algorithm をオーバーライドする

Type

String

Valid Settings

lz4, snappy, zlib, zstd

**compression\_mode**

Description

BlueStoreのインライン圧縮アルゴリズムのポリシーを設定する。この設定は、グローバル設定である bluestore compression mode をオーバーライドする

Type

String

Valid Settings

none, passive, aggressive, force

**compression\_min\_blob\_size**

Description

これより小さいチャンクは決して圧縮されない。この設定は、グローバルな設定である bluestore compression min blob\* をオーバーライドする

Type

Unsigned Integer

**compression\_max\_blob\_size**

Description

これより大きなチャンクは、圧縮される前に compression\_max\_blob\_size の大きさの小さな blob に分割される

Type

Unsigned Integer

**size**

Description

プール内のオブジェクトのレプリカの数を設定する。詳細は、「[Set the Number of Object Replicas](https://docs.ceph.com/en/pacific/rados/operations/pools/)」を参照。replicatedプールのみ

Type

Integer

**min\_size**

Description

I/Oに必要なレプリカの最小数を設定する。詳細は、「 [Set the Number of Object Replicas](https://docs.ceph.com/en/pacific/rados/operations/pools/) 」を参照。ECプールの場合、これは 'k' より大きい値に設定されるべきである。なぜなら、値 'k' で IO を許可すると、冗長性がなくなり、OSD に永久的な障害が発生した場合にデータが失われるからである。詳細については、[Erasure Code](https://docs.ceph.com/en/pacific/rados/operations/pools/)を参照

Type

Integer

Version

0.54 and above

**pg\_num**

Description

データ配置を計算する際に使用するPGの有効数

Type

Integer

Valid Range

pg\_num の現在値より優先

**pgp\_num**

Description

データ配置を計算する際に使用する有効なPG数

Type

Integer

Valid Range

pg\_num以下

**crush\_rule**

Description

クラスタ内のオブジェクト配置のマッピングに使用するルール

Type

String

**allow\_ec\_overwrites**

Description

EC付きプールへの書き込みがオブジェクトの一部を更新できるかどうかで、cephfsとrbdが使用できるようになる。詳細については、「[Erasure Coding with Overwrites](https://docs.ceph.com/en/pacific/rados/operations/pools/)」を参照 for more details.

Type

Boolean

Version

12.2.0 and above

**hashpspool**

Description

指定されたプールにHASHPSPOOLフラグを設定/解除する

Type

Integer

Valid Range

1 sets flag, 0 unsets flag

**nodelete**

Description

指定されたプールにNODELETEフラグを設定/解除する

Type

Integer

Valid Range

1 sets flag, 0 unsets flag

Version

Version FIXME

**nopgchange**

Description

指定されたプールにNOPGCHANGEフラグを設定/解除する

Type

Integer

Valid Range

1 sets flag, 0 unsets flag

Version

Version FIXME

**nosizechange**

Description

指定されたプールにNOSIZECHANGEフラグを設定/解除する

Type

Integer

Valid Range

1 sets flag, 0 unsets flag

Version

Version FIXME

**write\_fadvise\_dontneed**

Description

指定されたプールに WRITE\_FADVISE\_DONTNEED フラグを設定/解除するType

Integer

Valid Range

1 sets flag, 0 unsets flag

**noscrub**

Description

指定されたプールに NOSCRUB フラグを設定/解除する

Type

Integer

Valid Range

1 sets flag, 0 unsets flag

**nodeep\-scrub**

Description

指定されたプールに NODEEP\_SCRUB フラグを設定/解除する

Type

Integer

Valid Range

1 sets flag, 0 unsets flag

**hit\_set\_type**

Description

キャッシュプールのヒットセット追跡を有効にします。詳しくは[Bloom Filter](https://docs.ceph.com/en/pacific/rados/operations/pools/)を参照

Type

String

Valid Settings

bloom, explicit\_hash, explicit\_object

Default

bloom. Other values are for testing.

**hit\_set\_count**

Description

キャッシュプールのために保存するヒットセットの数。この数値が高いほど、ceph\-osdデーモンが消費するRAMが多くなる

Type

Integer

Valid Range

1. エージェントはまだ \> 1 を処理しない

**hit\_set\_period**

Description

キャッシュプールのヒットセット期間（秒）を指定する。この数値が高いほど、ceph\-osdデーモンが消費するRAMが多くなる

Type

Integer

Example

3600 1hr

**hit\_set\_fpp**

Description

ブルームヒットセットタイプの誤検出確率。詳細については、[Bloom Filter](https://docs.ceph.com/en/pacific/rados/operations/pools/) を参照

Type

Double

Valid Range

0.0 \- 1.0

Default

0.05

**cache\_target\_dirty\_ratio**

Description

キャッシュ階層化エージェントがバッキングストレージプールにフラッシュする前に、修正された（ダーティな）オブジェクトを含むキャッシュプールのパーセンテージ

Type

Double

Default

.4

**cache\_target\_dirty\_high\_ratio**

Description

キャッシュ階層化エージェントがより高速にバッキングストレージプールにフラッシュする前に、修正された（ダーティ）オブジェクトを含むキャッシュプールのパーセンテージ

Type

Double

Default

.6

**cache\_target\_full\_ratio**

Description

キャッシュ階層化エージェントがキャッシュプールからオブジェクトを削除するまでの、未修正の（クリーンな）オブジェクトを含むキャッシュプールのパーセンテージ

Type

Double

Default

.8

**target\_max\_bytes**

Description

Cephは、max\_bytes閾値がトリガーされると、オブジェクトのフラッシュまたは退避を開始する

Type

Integer

Example

1000000000000  \#1\-TB

**target\_max\_objects**

Description

Cephは、max\_objects閾値がトリガーされると、オブジェクトのフラッシュまたは退避を開始する

Type

Integer

Example

1000000 \#1M objects

**hit\_set\_grade\_decay\_rate**

Description

連続する2つのhit\_sets間の温度減衰率

Type

Integer

Valid Range

0 \- 100

Default

20

**hit\_set\_search\_last\_n**

Description

温度計算のためにhit\_setsに最大N個出現することを数える

Type

Integer

Valid Range

0 \- hit\_set\_count

Default

1

**cache\_min\_flush\_age**

Description

キャッシュ階層化エージェントが、キャッシュプールからストレージプールにオブジェクトをフラッシュするまでの時間（秒）

Type

Integer

Example

600 10min

**cache\_min\_evict\_age**

Description

キャッシュ階層化エージェントがキャッシュプールからオブジェクトを退避させるまでの時間\(秒\)

Type

Integer

Example

1800 30min

**fast\_read**

Description

ECプールでは、このフラグをオンにすると、読み取り要求はすべてのシャードに対してサブ読み取りを発行し、クライアントにサービスを提供するためにデコードするのに十分なシャードを受け取るまで待つ。jerasureおよびisa消去プラグインの場合、最初のK個の応答が返されると、クライアントのリクエストはこれらの応答からデコードされたデータを使用して直ちに処理される。これにより、一部のリソースをトレードオフしてパフォーマンスを向上させることができる。現在のところ、このフラグは ECプールでのみサポートされている

Type

Boolean

Defaults

0

**scrub\_min\_interval**

Description

負荷が低いときにプールスクラビングを行う最小間隔を秒単位で指定する。0 の場合、config の osd\_scrub\_min\_interval の値が使用される

Type

Double

Default

0

**scrub\_max\_interval**

Description

クラスタの負荷に関係なく、プールスクラビングを行う最大間隔を秒単位で指定する。0 の場合、config の osd\_scrub\_max\_interval の値が使用される

Type

Double

Default

0

**deep\_scrub\_interval**

Description

プールの「深い」スクラビングを行う間隔を秒単位で指定する。0 の場合、config の osd\_deep\_scrub\_interval の値が使用される

Type

Double

Default

0

**recovery\_priority**

Description

値を設定すると、計算された予約の優先度が増減する。この値は\-10～10の範囲である必要がある。 重要度の低いプールには負の優先度を使用し、新しいプールよりも低い優先度を持つようにする

Type

Integer

Default

0

**recovery\_op\_priority**

Description

osd\_recovery\_op\_priority の代わりに、このプールのリカバリ操作の優先順位を指定する

Type

Integer

Default

0

## Get Pool Values[¶](https://docs.ceph.com/en/pacific/rados/operations/pools/ "Permalink to this headline")

プールから値を取得するには：

```
ceph osd pool get {pool-name} {key}
```

以下のキーの値を取得することができます。

**size**

Description

see [size](https://docs.ceph.com/en/pacific/rados/operations/pools/)

Type

Integer

**min\_size**

Description

see [min\_size](https://docs.ceph.com/en/pacific/rados/operations/pools/)

Type

Integer

Version

0.54 and above

**pg\_num**

Description

see [pg\_num](https://docs.ceph.com/en/pacific/rados/operations/pools/)

Type

Integer

**pgp\_num**

Description

see [pgp\_num](https://docs.ceph.com/en/pacific/rados/operations/pools/)

Type

Integer

Valid Range

Equal to or less than pg\_num.

**crush\_rule**

Description

see [crush\_rule](https://docs.ceph.com/en/pacific/rados/operations/pools/)

**hit\_set\_type**

Description

see [hit\_set\_type](https://docs.ceph.com/en/pacific/rados/operations/pools/)

Type

String

Valid Settings

bloom, explicit\_hash, explicit\_object

**hit\_set\_count**

Description

see [hit\_set\_count](https://docs.ceph.com/en/pacific/rados/operations/pools/)

Type

Integer

**hit\_set\_period**

Description

see [hit\_set\_period](https://docs.ceph.com/en/pacific/rados/operations/pools/)

Type

Integer

**hit\_set\_fpp**

Description

see [hit\_set\_fpp](https://docs.ceph.com/en/pacific/rados/operations/pools/)

Type

Double

**cache\_target\_dirty\_ratio**

Description

see [cache\_target\_dirty\_ratio](https://docs.ceph.com/en/pacific/rados/operations/pools/)

Type

Double

**cache\_target\_dirty\_high\_ratio**

Description

see [cache\_target\_dirty\_high\_ratio](https://docs.ceph.com/en/pacific/rados/operations/pools/)

Type

Double

**cache\_target\_full\_ratio**

Description

see [cache\_target\_full\_ratio](https://docs.ceph.com/en/pacific/rados/operations/pools/)

Type

Double

**target\_max\_bytes**

Description

see [target\_max\_bytes](https://docs.ceph.com/en/pacific/rados/operations/pools/)

Type

Integer

**target\_max\_objects**

Description

see [target\_max\_objects](https://docs.ceph.com/en/pacific/rados/operations/pools/)

Type

Integer

**cache\_min\_flush\_age**

Description

see [cache\_min\_flush\_age](https://docs.ceph.com/en/pacific/rados/operations/pools/)

Type

Integer

**cache\_min\_evict\_age**

Description

see [cache\_min\_evict\_age](https://docs.ceph.com/en/pacific/rados/operations/pools/)

Type

Integer

**fast\_read**

Description

see [fast\_read](https://docs.ceph.com/en/pacific/rados/operations/pools/)

Type

Boolean

**scrub\_min\_interval**

Description

see [scrub\_min\_interval](https://docs.ceph.com/en/pacific/rados/operations/pools/)

Type

Double

**scrub\_max\_interval**

Description

see [scrub\_max\_interval](https://docs.ceph.com/en/pacific/rados/operations/pools/)

Type

Double

**deep\_scrub\_interval**

Description

see [deep\_scrub\_interval](https://docs.ceph.com/en/pacific/rados/operations/pools/)

Type

Double

**allow\_ec\_overwrites**

Description

see [allow\_ec\_overwrites](https://docs.ceph.com/en/pacific/rados/operations/pools/)

Type

Boolean

**recovery\_priority**

Description

see [recovery\_priority](https://docs.ceph.com/en/pacific/rados/operations/pools/)

Type

Integer

**recovery\_op\_priority**

Description

see [recovery\_op\_priority](https://docs.ceph.com/en/pacific/rados/operations/pools/)

Type

Integer

## Set the Number of Object Replicas[¶](https://docs.ceph.com/en/pacific/rados/operations/pools/ "Permalink to this headline")

レプリケートプールのオブジェクトレプリカ数を設定するには：

```
ceph osd pool set {poolname} size {num-replicas}
```

重要：{num\-replicas}は、オブジェクト自体を含みます。オブジェクトとオブジェクトのコピー2つ、合計3つのインスタンスが必要な場合は、3を指定します。

For example:

```
ceph osd pool set data size 3
```

このコマンドはプールごとに実行することができます。

注意: オブジェクトは、プールサイズより少ないレプリカでデグレード・モードのI/Oを受け入れる場合があります。 I/Oに必要なレプリカの最小数を設定するには、min\_size設定を使用する必要があります。たとえば、以下のようになります。

```
ceph osd pool set data min_size 2
```

これにより、データプール内のどのオブジェクトも、min\_sizeより少ないレプリカでI/Oを受け取ることがないようにすることができます。

## Get the Number of Object Replicas[¶](https://docs.ceph.com/en/pacific/rados/operations/pools/ "Permalink to this headline")

オブジェクトレプリカの数を取得するには、以下を実行します。

```
ceph osd dump | grep 'replicated size'
```

Cephは、replicated size属性を強調表示したプールをリストアップします。デフォルトでは、cephはオブジェクトのレプリカを2つ作成します\(合計3つのコピー、またはsize 3\)。
