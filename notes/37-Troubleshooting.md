# 37: Troubleshooting

**クリップソース:** [37: Troubleshooting — Ceph Documentation](https://docs.ceph.com/en/pacific/cephadm/troubleshooting/)

# Troubleshooting[¶](https://docs.ceph.com/en/pacific/cephadm/troubleshooting/ "Permalink to this headline")

cephadmコマンドが失敗した理由や、特定のサービスが正常に動作しなくなった理由を調査する必要があるかもしれません。

Cephadmはデーモンをコンテナとして展開します。これは、コンテナ化されたデーモンのトラブルシューティングが予想とは異なる動作をする可能性があることを意味します\(このトラブルシューティングが、関係するデーモンがコンテナ化されていない場合のトラブルシューティングと同様に動作することを期待している場合は、確かにその通りです\)。

ここでは、Ceph環境のトラブルシューティングに役立つツールとコマンドをいくつか紹介します。

## Pausing or disabling cephadm[¶](https://docs.ceph.com/en/pacific/cephadm/troubleshooting/ "Permalink to this headline")

何か問題が発生してcephadmの動作がおかしくなった場合は、次のコマンドを実行することで、Cephクラスタのバックグラウンドアクティビティのほとんどを一時停止することができます。

```
# ceph orch pause
```

これにより、Cephクラスタ内のすべての変更が停止されますが、cephadmは引き続き定期的にホストをチェックして、デーモンとデバイスのインベントリを更新します。 

以下のコマンドを実行すると、cephadmを完全に無効にすることができます。

```
# ceph orch set backend ''
# ceph mgr module disable cephadm
```

これらのコマンドは、すべてのcephorch...CLIコマンドを無効にします。CLIコマンドを無効にします。以前にデプロイされたすべてのデーモン・コンテナは引き続き存在し、これらのコマンドを実行する前の状態で起動します。

個々のサービスを無効にする方法については、「[Disabling automatic deployment of daemons](https://docs.ceph.com/en/pacific/cephadm/troubleshooting/) 」を参照してください。

## Per\-service and per\-daemon events[¶](https://docs.ceph.com/en/pacific/cephadm/troubleshooting/ "Permalink to this headline")

失敗したデーモンの展開をデバッグするプロセスを支援するために、cephadmはサービスおよびデーモンごとにイベントを保存します。これらのイベントには、Cephクラスタのトラブルシューティングに関連する情報が含まれていることがよくあります。

### Listing service events[¶](https://docs.ceph.com/en/pacific/cephadm/troubleshooting/ "Permalink to this headline")

特定のサービスに関連するイベントを表示するには、次のような形式のコマンドを実行します。

```
# ceph orch ls --service_name=<service-name> --format yaml
```

これにより、以下のような形で返ってきます。

```
service_type: alertmanager
service_name: alertmanager
placement:
  hosts:
  - unknown_host
status:
  ...
  running: 1
  size: 1
events:
- 2021-02-01T08:58:02.741162 service:alertmanager [INFO] "service was created"
- '2021-02-01T12:09:25.264584 service:alertmanager [ERROR] "Failed to apply: Cannot
  place <AlertManagerSpec for service_name=alertmanager> on unknown_host: Unknown hosts"'
```

### Listing daemon events[¶](https://docs.ceph.com/en/pacific/cephadm/troubleshooting/ "Permalink to this headline")

特定のデーモンに関連するイベントを表示するには、次のような形式のコマンドを実行します。

```
# ceph orch ps --service-name <service-name> --daemon-id <daemon-id> --format yaml
```

これにより、以下のような形で返ってきます。

```
daemon_type: mds
daemon_id: cephfs.hostname.ppdhsz
hostname: hostname
status_desc: running
...
events:
- 2021-02-01T08:59:43.845866 daemon:mds.cephfs.hostname.ppdhsz [INFO] "Reconfigured
  mds.cephfs.hostname.ppdhsz on host 'hostname'"
```

## Checking cephadm logs[¶](https://docs.ceph.com/en/pacific/cephadm/troubleshooting/ "Permalink to this headline")

生成されるcephadmログを監視する方法については、「[Watching cephadm log messages](https://docs.ceph.com/en/pacific/cephadm/troubleshooting/)」を参照してください。

イベントをファイルに記録するようにCephクラスタが設定されている場合は、すべてのモニタホストにceph.cephadm.logというcephadmログファイルが存在します\(これについての詳しい説明は、「 [Ceph daemon logs](https://docs.ceph.com/en/pacific/cephadm/troubleshooting/) 」を参照してください\)。

[35: Cephadm Operations](35-Cephadm_Operations.md)

## Gathering log files[¶](https://docs.ceph.com/en/pacific/cephadm/troubleshooting/ "Permalink to this headline")

journalctlを使用して、すべてのデーモンのログファイルを収集します。

注意:デフォルトでは、cephadmはログをjournaldに保存します。これは、/var/log/ceph/にデーモンのログが見られなくなることを意味します。

特定のデーモンのログファイルを読むには、次のように実行します。

```
cephadm logs --name <name-of-daemon>
```

注意：これは、デーモンが動作しているのと同じホスト上で実行した場合にのみ機能します。別のホストで動作しているデーモンのログを取得するには、\-\-fsidオプションを与えます。

```
cephadm logs --fsid <fsid> --name <name-of-daemon>
```

ここで、\<fsid\>は、_ceph status_によって表示されるクラスタIDに対応します。

特定のホスト上のすべてのデーモンのすべてのログファイルを取得するには、次を実行します。

```
for name in $(cephadm ls | jq -r '.[].name') ; do
  cephadm logs --fsid <fsid> --name "$name" > $name;
done
```

## Collecting systemd status[¶](https://docs.ceph.com/en/pacific/cephadm/troubleshooting/ "Permalink to this headline")

systemdユニットの状態を表示するには、次のように実行します。

```
systemctl status "ceph-$(cephadm shell ceph fsid)@<service name>.service";
```

特定のホストのすべてのデーモンの状態を取得するには、次のように実行します。

```
fsid="$(cephadm shell ceph fsid)"
for name in $(cephadm ls | jq -r '.[].name') ; do
  systemctl status "ceph-$fsid@$name.service" > $name;
done
```

## List all downloaded container images[¶](https://docs.ceph.com/en/pacific/cephadm/troubleshooting/ "Permalink to this headline")

ホスト上でダウンロードされたすべてのコンテナイメージを一覧表示する。

注：ImageはImageIDと呼ばれることもあります。

```
podman ps -a --format json | jq '.[].Image'
"docker.io/library/centos:8"
"registry.opensuse.org/opensuse/leap:15.2"
```

## Manually running containers[¶](https://docs.ceph.com/en/pacific/cephadm/troubleshooting/ "Permalink to this headline")

Cephadmはコンテナを実行する小さなラッパーを書きます。コンテナ実行コマンドについては、/var/lib/ceph/\<cluster\-fsid\>/\<service\-name\>/unit.runを参照してください。

## ssh errors[¶](https://docs.ceph.com/en/pacific/cephadm/troubleshooting/ "Permalink to this headline")

Error message:

```
execnet.gateway_bootstrap.HostNotFound: -F /tmp/cephadm-conf-73z09u6g -i /tmp/cephadm-identity-ky7ahp_5 root@10.10.1.2
...
raise OrchestratorError(msg) from e
orchestrator._interface.OrchestratorError: Failed to connect to 10.10.1.2 (10.10.1.2).
Please make sure that the host is reachable and accepts connections using the cephadm SSH key
...
```

ユーザーができること

cephadmにSSHのIDキーがあることを確認します。

```
[root@mon1~]# cephadm shell -- ceph config-key get mgr/cephadm/ssh_identity_key > ~/cephadm_private_key
INFO:cephadm:Inferring fsid f8edc08a-7f17-11ea-8707-000c2915dd98
INFO:cephadm:Using recent ceph image docker.io/ceph/ceph:v15 obtained 'mgr/cephadm/ssh_identity_key'
[root@mon1 ~] # chmod 0600 ~/cephadm_private_key
```

これが失敗した場合、cephadmにはキーがありません。次のコマンドを実行してこれを修正します。

```
[root@mon1 ~]# cephadm shell -- ceph cephadm generate-ssh-key
```

or:

```
[root@mon1 ~]# cat ~/cephadm_private_key | cephadm shell -- ceph cephadm set-ssk-key -i -
```

sshの設定が正しいことを確認します。

```
[root@mon1 ~]# cephadm shell -- ceph cephadm get-ssh-config > config
```

ホストに接続できることを確認します。

```
[root@mon1 ~]# ssh -F config -i ~/cephadm_private_key root@mon1
```

### Verifying that the Public Key is Listed in the authorized\_keys file[¶](https://docs.ceph.com/en/pacific/cephadm/troubleshooting/ "Permalink to this headline")

公開鍵がauthorized\_keysファイルにあることを確認するために、以下のコマンドを実行します。

```
[root@mon1 ~]# cephadm shell -- ceph cephadm get-pub-key > ~/ceph.pub
[root@mon1 ~]# grep "`cat ~/ceph.pub`"  /root/.ssh/authorized_keys
```

## Failed to infer CIDR network error[¶](https://docs.ceph.com/en/pacific/cephadm/troubleshooting/ "Permalink to this headline")

このエラーが表示された場合

```
ERROR: Failed to infer CIDR network for mon ip ***; pass --skip-mon-network to configure it later
```

Or this error:

```
Must set public_network config option or specify a CIDR network, ceph addrvec, or plain IP
```

つまり、この形式のコマンドを実行する必要があります。

```
ceph config set mon public_network <mon_network>
```

この種の操作の詳細については、「[Deploying additional monitors](https://docs.ceph.com/en/pacific/cephadm/troubleshooting/)」を参照してください。

## Accessing the admin socket[¶](https://docs.ceph.com/en/pacific/cephadm/troubleshooting/ "Permalink to this headline")

Cephの各デーモンには、MONをバイパスする管理者ソケットが用意されています\(「[Using the Admin Socket](https://docs.ceph.com/en/pacific/cephadm/troubleshooting/)」を参照\)。

管理者ソケットにアクセスするには、まず、ホスト上のデーモンコンテナに入ります。 [66: Monitoring a Cluster](66-Monitoring_a_Cluster.md)

```
[root@mon1 ~]# cephadm enter --name <daemon-name>
[ceph: root@mon1 /]# ceph --admin-daemon /var/run/ceph/ceph-<daemon-name>.asok config show
```

## Calling miscellaneous ceph tools[¶](https://docs.ceph.com/en/pacific/cephadm/troubleshooting/ "Permalink to this headline")

ceph\-objectstore\-toolやceph\-monstore\-toolなどの雑多なものを呼び出すには、次のように_cephadm shell \-\-name \<daemon\-name\>_を呼び出して実行します。

```
root@myhostname # cephadm unit --name mon.myhostname stop
root@myhostname # cephadm shell --name mon.myhostname
[ceph: root@myhostname /]# ceph-monstore-tool /var/lib/ceph/mon/ceph-myhostname get monmap > monmap
[ceph: root@myhostname /]# monmaptool --print monmap
monmaptool: monmap file monmap
epoch 1
fsid 28596f44-3b56-11ec-9034-482ae35a5fbb
last_changed 2021-11-01T20:57:19.755111+0000
created 2021-11-01T20:57:19.755111+0000
min_mon_release 17 (quincy)
election_strategy: 1
0: [v2:127.0.0.1:3300/0,v1:127.0.0.1:6789/0] mon.myhostname

```

このコマンドは、デーモンのメンテナンスを拡張したり、デーモンを対話的に実行するのに適した方法で環境を設定します。

## Restoring the MON quorum[¶](https://docs.ceph.com/en/pacific/cephadm/troubleshooting/ "Permalink to this headline")

CephのMONがクォーラムを形成できない場合、クォーラムが復元されるまで、cephadmはクラスタを管理できません。

MONのクォーラムを復元するには、以下の手順で不健全なMONをmonmapから削除します。

すべての MON を停止します。各MONのホストに対して

```
ssh {mon-host}
cephadm unit --name mon.`hostname` stop
```

生き残っているモニターを特定し、そのホストにログインします。

```
ssh {mon-host}
cephadm enter --name mon.`hostname`
```

[Removing Monitors from an Unhealthy Cluster](https://docs.ceph.com/en/pacific/cephadm/troubleshooting/)の手順に従ってください。[90: Adding/Removing Monitors](90-Adding-Removing_Monitors.md)

## Manually deploying a MGR daemon[¶](https://docs.ceph.com/en/pacific/cephadm/troubleshooting/ "Permalink to this headline")

cephadmはクラスタを管理するためにMGRデーモンを必要とします。クラスタの最後のMGRが削除された場合は、以下の手順に従って、クラスタのランダムなホストにMGR mgr.hostname.smfvfdを手動でデプロイしてください。

cephadmが新しいMGRを削除するのを防ぐために、cephadmスケジューラを無効にします。 [Enable Ceph CLI](https://docs.ceph.com/en/pacific/cephadm/troubleshooting/)を参照してください。

```
ceph config-key set mgr/cephadm/pause true
```

その後、新しいMGRの認証エントリを取得または作成します。

```
ceph auth get-or-create mgr.hostname.smfvfd mon "profile mgr" osd "allow *" mds "allow *"
```

ceph.confを取得します。

```
ceph config generate-minimal-conf
```

コンテナイメージを取得します。

```
ceph config get "mgr.hostname.smfvfd" container_image
```

デーモンのデプロイに必要な情報を含むconfig\-json.jsonファイルを作成します。

```
{
  "config": "# minimal ceph.conf for 8255263a-a97e-4934-822c-00bfe029b28fn[global]ntfsid = 8255263a-a97e-4934-822c-00bfe029b28fntmon_host = [v2:192.168.0.1:40483/0,v1:192.168.0.1:40484/0]n",
  "keyring": "[mgr.hostname.smfvfd]ntkey = V2VyIGRhcyBsaWVzdCBpc3QgZG9vZi4=n"
}
```

デーモンをデプロイする。

```
cephadm --image <container-image> deploy --fsid <fsid> --name mgr.hostname.smfvfd --config-json config-json.json
```
