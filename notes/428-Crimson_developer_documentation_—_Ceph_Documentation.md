# 428: Crimson developer documentation — Ceph Documentation

      [Report a Documentation Bug](https://docs.ceph.com/en/pacific/dev/crimson/) 
 # Crimson developer documentation[¶](https://docs.ceph.com/en/pacific/dev/crimson/ "Permalink to this headline")

Contents

* [crimson](https://docs.ceph.com/en/pacific/dev/crimson/)
    * [Building Crimson](https://docs.ceph.com/en/pacific/dev/crimson/)
    * [Running Crimson](https://docs.ceph.com/en/pacific/dev/crimson/)
        * [object store backend](https://docs.ceph.com/en/pacific/dev/crimson/)
        * [daemonize](https://docs.ceph.com/en/pacific/dev/crimson/)
        * [logging](https://docs.ceph.com/en/pacific/dev/crimson/)
        * [vstart.sh](https://docs.ceph.com/en/pacific/dev/crimson/)
    * [CBT Based Testing](https://docs.ceph.com/en/pacific/dev/crimson/)
    * [Hacking Crimson](https://docs.ceph.com/en/pacific/dev/crimson/)
        * [Seastar Documents](https://docs.ceph.com/en/pacific/dev/crimson/)
    * [Debugging Crimson](https://docs.ceph.com/en/pacific/dev/crimson/)
        * [Debugging with GDB](https://docs.ceph.com/en/pacific/dev/crimson/)
        * [Human\-readable backtraces with addr2line](https://docs.ceph.com/en/pacific/dev/crimson/)
* [error handling](https://docs.ceph.com/en/pacific/dev/crimson/)
* [PoseidonStore](https://docs.ceph.com/en/pacific/dev/crimson/)
    * [Key concepts and goals](https://docs.ceph.com/en/pacific/dev/crimson/)
        * [Background](https://docs.ceph.com/en/pacific/dev/crimson/)
        * [Motivation and Key idea](https://docs.ceph.com/en/pacific/dev/crimson/)
        * [Observation](https://docs.ceph.com/en/pacific/dev/crimson/)
    * [Design](https://docs.ceph.com/en/pacific/dev/crimson/)
        * [I/O procedure](https://docs.ceph.com/en/pacific/dev/crimson/)
        * [Crash consistency](https://docs.ceph.com/en/pacific/dev/crimson/)
        * [Comparison](https://docs.ceph.com/en/pacific/dev/crimson/)
    * [Detailed Design](https://docs.ceph.com/en/pacific/dev/crimson/)
        * [WAL](https://docs.ceph.com/en/pacific/dev/crimson/)
        * [Partition and Reactor thread](https://docs.ceph.com/en/pacific/dev/crimson/)
        * [Cache](https://docs.ceph.com/en/pacific/dev/crimson/)
        * [Sharded partitions \(with cross\-SP transaction\)](https://docs.ceph.com/en/pacific/dev/crimson/)
        * [CoW/Clone](https://docs.ceph.com/en/pacific/dev/crimson/)
    * [Plans](https://docs.ceph.com/en/pacific/dev/crimson/)
