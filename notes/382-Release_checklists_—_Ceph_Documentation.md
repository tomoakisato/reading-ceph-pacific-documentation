# 382: Release checklists — Ceph Documentation

 # Release checklists[¶](https://docs.ceph.com/en/pacific/dev/release-checklists/ "Permalink to this headline")

## Dev Kickoff[¶](https://docs.ceph.com/en/pacific/dev/release-checklists/ "Permalink to this headline")

These steps should be taken when starting a new major release, just after the previous release has been tagged \(vX.2.0\) and that tag has been merged back into master.

X is the release we are just starting development on. X\-1 is the one that was just released \(X\-1\).2.0.

### Versions and tags[¶](https://docs.ceph.com/en/pacific/dev/release-checklists/ "Permalink to this headline")

* \[x\] Update CMakeLists.txt VERSION \(right at the top to X.0.0\)
* \[x\] Update src/ceph\_release with the new release name, number, and type \(‘dev’\)
* \[ \] Initial tag vX.0.0 \(so that we can distinguish from \(and sort after\) the backported \(X\-1\).2.Z versions.

### Define release names and constants[¶](https://docs.ceph.com/en/pacific/dev/release-checklists/ "Permalink to this headline")

Make sure X \(and, ideally, X\+1\) is defined:

* \[x\] src/common/ceph\_releases.h \(ceph\_release\_t\)
* \[x\] src/common/ceph\_strings.cc \(ceph\_release\_name\(\)\)
* \[x\] src/include/rados.h \(CEPH\_RELEASE\_\* and MAX\)
* \[x\] src/mon/mon\_types.h \(ceph::features::mon::FEATURE\_\* and related structs and helpers; note that monmaptool CLI test output will need adjustment\)
* \[x\] src/mds/cephfs\_features.h \(CEPHFS\_CURRENT\_RELEASE\)

#### Scripts[¶](https://docs.ceph.com/en/pacific/dev/release-checklists/ "Permalink to this headline")

* \[x\] src/script/backport\-resolve\-issue \(releases\(\), ver\_to\_release\(\)… but for X\-1\)
* \[x\] src/script/ceph\-release\-notes \(X\-1\)
* \[x\] ceph\-build.git scripts/build\_utils.sh release\_from\_version\(\)

#### Misc[¶](https://docs.ceph.com/en/pacific/dev/release-checklists/ "Permalink to this headline")

* \[x\] update src/ceph\-volume/ceph\_volume/\_\_init\_\_.py \(\_\_release\_\_\)

### Feature bits[¶](https://docs.ceph.com/en/pacific/dev/release-checklists/ "Permalink to this headline")

* \[x\] ensure that SERVER\_X is defined
* \[x\] change any features DEPRECATED in release X\-3 are now marked RETIRED.
* \[ \] look for features that \(1\) were present in X\-2 and \(2\) have no client dependency and mark them DEPRECATED as of X.

### Compatsets[¶](https://docs.ceph.com/en/pacific/dev/release-checklists/ "Permalink to this headline")

* \[x\] mon/Monitor.h \(CEPH\_MON\_FEATURE\_INCOMPAT\_X\)
* \[x\] mon/Monitor.cc \(include in get\_supported\_features\(\)\)
* \[x\] mon/Monitor.cc \(apply\_monmap\_to\_compatset\_features\(\)\)
* \[x\] mon/Monitor.cc \(calc\_quorum\_requirements\(\)\)

### Mon[¶](https://docs.ceph.com/en/pacific/dev/release-checklists/ "Permalink to this headline")

* \[x\] qa/standalone/mon/misc adjust TEST\_mon\_features \(add X cases and adjust –mon\-debug\-no\-require\-X\)
* \[x\] mon/MgrMonitor.cc adjust always\_on\_modules
* \[x\] common/options.cc define mon\_debug\_no\_require\_X
* \[x\] common/options.cc remove mon\_debug\_no\_require\_X\-2
* \[x\] mon/OSDMonitor.cc create\_initial: adjust new require\_osd\_release, and add associated mon\_debug\_no\_require\_X
* \[x\] mon/OSDMonitor.cc preprocess\_boot: adjust “disallow boot of ” condition to disallow X if require\_osd\_release \< X\-2.
* \[x\] mon/OSDMonitor.cc: adjust “osd require\-osd\-release” to \(1\) allow setting X, and \(2\) check that all mons _and_ OSDs have X
* \[x\] mon/MonCommands.h: adjust “osd require\-osd\-release” allows options to include X
* \[x\] qa/workunits/cephtool/test.sh: adjust require\-osd\-release test

### Code cleanup[¶](https://docs.ceph.com/en/pacific/dev/release-checklists/ "Permalink to this headline")

* \[ \] search code for “after X\-1” or “X” for conditional checks
* \[ \] search code for X\-2 and X\-3 \(CEPH\_FEATURE\_SERVER\_\* andceph\_release\_t::\*\)
* \[ \] search code for require\_osd\_release
* \[ \] search code for min\_mon\_release

### QA suite[¶](https://docs.ceph.com/en/pacific/dev/release-checklists/ "Permalink to this headline")

* \[ \] create qa/suites/upgrade/\(X\-1\)\-x
* \[x\] remove qa/suites/upgrade/\(X\-3\)\-x\-\*
* \[x\] remove qa/suites/rados/upgrade/\(X\-3\)\-x\-singleton symlink
* \[x\] create qa/releases/X.yaml
* \[x\] create qa/suites/rados/thrash\-old\-clients/1\-install/\(X\-1\).yaml

## First release candidate[¶](https://docs.ceph.com/en/pacific/dev/release-checklists/ "Permalink to this headline")

* \[ \] src/ceph\_release: change type to rc

## First stable release[¶](https://docs.ceph.com/en/pacific/dev/release-checklists/ "Permalink to this headline")

* \[ \] src/ceph\_release: change type stable
* \[ \] generate new object corpus for encoding/decoding tests \- see [Corpus structure](https://docs.ceph.com/en/pacific/dev/release-checklists/)
