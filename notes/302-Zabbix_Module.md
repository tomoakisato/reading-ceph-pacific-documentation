# 302: Zabbix Module

 # Zabbix Module[¶](https://docs.ceph.com/en/pacific/mgr/zabbix/ "Permalink to this headline")

The Zabbix module actively sends information to a Zabbix server like:

* Ceph status
* I/O operations
* I/O bandwidth
* OSD status
* Storage utilization

## Requirements[¶](https://docs.ceph.com/en/pacific/mgr/zabbix/ "Permalink to this headline")

The module requires that the _zabbix\_sender_ executable is present on _all_machines running ceph\-mgr. It can be installed on most distributions using the package manager.

### Dependencies[¶](https://docs.ceph.com/en/pacific/mgr/zabbix/ "Permalink to this headline")

Installing zabbix\_sender can be done under Ubuntu or CentOS using either apt or dnf.

On Ubuntu Xenial:

```
apt install zabbix-agent
```

On Fedora:

```
dnf install zabbix-sender
```

## Enabling[¶](https://docs.ceph.com/en/pacific/mgr/zabbix/ "Permalink to this headline")

You can enable the _zabbix_ module with:

```
ceph mgr module enable zabbix
```

## Configuration[¶](https://docs.ceph.com/en/pacific/mgr/zabbix/ "Permalink to this headline")

Two configuration keys are vital for the module to work:

* zabbix\_host
* identifier \(optional\)

The parameter _zabbix\_host_ controls the hostname of the Zabbix server to which_zabbix\_sender_ will send the items. This can be a IP\-Address if required by your installation.

The _identifier_ parameter controls the identifier/hostname to use as source when sending items to Zabbix. This should match the name of the _Host_ in your Zabbix server.

When the _identifier_ parameter is not configured the ceph\-\<fsid\> of the cluster will be used when sending data to Zabbix.

This would for example be _ceph\-c4d32a99\-9e80\-490f\-bd3a\-1d22d8a7d354_

Additional configuration keys which can be configured and their default values:

* zabbix\_port: 10051
* zabbix\_sender: /usr/bin/zabbix\_sender
* interval: 60
* discovery\_interval: 100

### Configuration keys[¶](https://docs.ceph.com/en/pacific/mgr/zabbix/ "Permalink to this headline")

Configuration keys can be set on any machine with the proper cephx credentials, these are usually Monitors where the _client.admin_ key is present.

```
ceph zabbix config-set <key> <value>
```

For example:

```
ceph zabbix config-set zabbix_host zabbix.localdomain
ceph zabbix config-set identifier ceph.eu-ams02.local
```

The current configuration of the module can also be shown:

```
ceph zabbix config-show
```

### Template[¶](https://docs.ceph.com/en/pacific/mgr/zabbix/ "Permalink to this headline")

A [template](https://docs.ceph.com/en/pacific/mgr/zabbix/). \(XML\) to be used on the Zabbix server can be found in the source directory of the module.

This template contains all items and a few triggers. You can customize the triggers afterwards to fit your needs.

### Multiple Zabbix servers[¶](https://docs.ceph.com/en/pacific/mgr/zabbix/ "Permalink to this headline")

It is possible to instruct zabbix module to send data to multiple Zabbix servers.

Parameter _zabbix\_host_ can be set with multiple hostnames separated by commas. Hosnames \(or IP adderesses\) can be followed by colon and port number. If a port number is not present module will use the port number defined in _zabbix\_port_.

For example:

```
ceph zabbix config-set zabbix_host "zabbix1,zabbix2:2222,zabbix3:3333"
```

## Manually sending data[¶](https://docs.ceph.com/en/pacific/mgr/zabbix/ "Permalink to this headline")

If needed the module can be asked to send data immediately instead of waiting for the interval.

This can be done with this command:

```
ceph zabbix send
```

The module will now send its latest data to the Zabbix server.

Items discovery is accomplished also via zabbix\_sender, and runs every discovery\_interval \* interval seconds. If you wish to launch discovery manually, this can be done with this command:

```
ceph zabbix discovery
```

## Debugging[¶](https://docs.ceph.com/en/pacific/mgr/zabbix/ "Permalink to this headline")

Should you want to debug the Zabbix module increase the logging level for ceph\-mgr and check the logs.

```
[mgr]
    debug mgr = 20
```

With logging set to debug for the manager the module will print various logging lines prefixed with _mgr\[zabbix\]_ for easy filtering.
