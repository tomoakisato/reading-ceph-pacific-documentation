# 454: scan

**クリップソース:** [454: scan — Ceph Documentation](https://docs.ceph.com/en/pacific/ceph-volume/simple/scan/)

# scan[¶](https://docs.ceph.com/en/pacific/ceph-volume/simple/scan/ "Permalink to this headline")

スキャンにより、すでに配備されたOSDから詳細を取得し、他の起動ワークフローやツール（udevやceph\-diskなど）を使わずにceph\-volumeがそれを管理できるようにします。LUKSによる暗号化またはPLAINフォーマットが完全にサポートされています。

このコマンドは、OSDデータが保存されているディレクトリを検査したり、データパーティションから実行中のOSDを検査する機能を備えています。また、パスやデバイスが提供されない場合、コマンドは実行中のすべてのOSDをスキャンすることができます。

スキャンされると、（デフォルトで）メタデータをJSONとして/etc/ceph/osdのファイルに永続化します。このJSONファイルは、{OSDID}\-{OSDFSID}.jsonという命名規則が使われます。IDが1で、FSIDが86ebd829\-1405\-43d3\-8fd6\-4cbc9b6ecf96のようなOSDの場合、ファイルの絶対パスは次のようになります：

```
/etc/ceph/osd/1-86ebd829-1405-43d3-8fd6-4cbc9b6ecf96.json
```

scanサブコマンドは、このファイルがすでに存在する場合、書き込みを拒否します。内容の上書きが必要な場合は、\-\-forceフラグを使用しなければなりません。

```
ceph-volume simple scan --force {path}
```

JSONメタデータを永続化する必要がない場合は、stdoutに内容を送信する機能があります（ファイルは書き込まれません）:

```
ceph-volume simple scan --stdout {path}
```

## Running OSDs scan[¶](https://docs.ceph.com/en/pacific/ceph-volume/simple/scan/ "Permalink to this headline")

OSD ディレクトリまたはデバイスを指定せずにこのコマンドを使用すると、現在実行中のすべての OSD のディレクトリがスキャンされます。実行中のOSDがceph\-diskによって作成されていない場合、それは無視され、スキャンされません。

実行中のすべてのceph\-disk OSDをスキャンするには、次のようなコマンドを実行します：

```
ceph-volume simple scan
```

## Directory scan[¶](https://docs.ceph.com/en/pacific/ceph-volume/simple/scan/ "Permalink to this headline")

ディレクトリスキャンは、ファイルからOSDファイルの内容をキャプチャします。スキャンを成功させるためには、存在しなければならないいくつかのファイルがあります：

* ceph\_fsid
* fsid
* keyring
* ready
* type
* whoami

OSDが暗号化されている場合は、さらに以下のキーが追加されます：

* encrypted
* encryption\_type
* lockbox\_keyring

その他のファイルの場合も、バイナリやディレクトリでない限り、キャプチャされ、JSONオブジェクトの一部として永続化されます。

JSONオブジェクトのキーに関する規約は、任意のファイル名がキーとなり、その内容が値となります。内容が1行の場合（whoamiの場合のように）、内容は切り取られ、改行が落とされます。例えば、idが1のOSDの場合、JSONのエントリーはこのようになります：

```
"whoami": "1",
```

複数行のファイルについては、そのままの内容です。ただし、キーリングを抽出するためにパースされるキーリングは特別に扱われます。例えば、次のように読み取られるキーリングがあります：

```
[osd.1]ntkey = AQBBJ/dZp57NIBAAtnuQS9WOS0hnLVe0rZnE6Q==n
```

上記は以下のように格納されることになります：

```
"keyring": "AQBBJ/dZp57NIBAAtnuQS9WOS0hnLVe0rZnE6Q==",
```

/var/lib/ceph/osd/ceph\-1 のようなディレクトリの場合、コマンドは次のようになります：

```
ceph-volume simple scan /var/lib/ceph/osd/ceph1
```

## Device scan[¶](https://docs.ceph.com/en/pacific/ceph-volume/simple/scan/ "Permalink to this headline")

OSDディレクトリが利用できない場合（OSDが実行されていない、またはデバイスがマウントされていない）、scanコマンドはデバイスを検査して必要なデータをキャプチャすることが可能です。OSDのスキャンを実行するのと同じように、いくつかのファイルが存在する必要があります。これは、スキャンされるデバイスがOSDのデータパーティションでなければならないことを意味します。

OSDのデータパーティションが引数として渡されている限り、サブコマンドはその内容をスキャンすることができます。

デバイスが既にマウントされている場合、ツールはこのシナリオを検出し、そのディレクトリからファイルの内容をキャプチャすることができます。

デバイスがマウントされていない場合、一時ディレクトリを作成し、コンテンツをスキャンするためだけにデバイスを一時的にマウントします。スキャンが完了すると、デバイスはアンマウントされます。

データパーティションが/dev/sda1のようなデバイスの場合、コマンドは次のようになります：

```
ceph-volume simple scan /dev/sda1
```

## JSON contents[¶](https://docs.ceph.com/en/pacific/ceph-volume/simple/scan/ "Permalink to this headline")

JSONオブジェクトの中身はとてもシンプルです。スキャンでは、特殊なOSDファイルとその内容から情報を持続させるだけでなく、パスとデバイスUUIDの検証も行います。ceph\-diskが{device type}\_uuidファイルに保存して行うのとは異なり、このツールはdevice typeキーの一部としてそれらを永続化させます。

例えば、block.dbデバイスは次のようなものですが：

```
"block.db": {
    "path": "/dev/disk/by-partuuid/6cc43680-4f6e-4feb-92ff-9c7ba204120e",
    "uuid": "6cc43680-4f6e-4feb-92ff-9c7ba204120e"
},
```

生成されたceph\-diskファイルを以下のように永続化します：

```
"block.db_uuid": "6cc43680-4f6e-4feb-92ff-9c7ba204120e",
```

このように重複しているのは、ツールが次のようなことを保証しようとしているからです：

\# ceph\-diskファイルを持たないOSDをサポートすること

\# LVM と blkid に対するクエリにより、デバイスの最新情報を確認できること

\# 論理ボリュームと GPT デバイスの両方をサポートすること

これは、bluestore を使用している OSD からの JSON メタデータのサンプルです：

```
{
    "active": "ok",
    "block": {
        "path": "/dev/disk/by-partuuid/40fd0a64-caa5-43a3-9717-1836ac661a12",
        "uuid": "40fd0a64-caa5-43a3-9717-1836ac661a12"
    },
    "block.db": {
        "path": "/dev/disk/by-partuuid/6cc43680-4f6e-4feb-92ff-9c7ba204120e",
        "uuid": "6cc43680-4f6e-4feb-92ff-9c7ba204120e"
    },
    "block.db_uuid": "6cc43680-4f6e-4feb-92ff-9c7ba204120e",
    "block_uuid": "40fd0a64-caa5-43a3-9717-1836ac661a12",
    "bluefs": "1",
    "ceph_fsid": "c92fc9eb-0610-4363-aafc-81ddf70aaf1b",
    "cluster_name": "ceph",
    "data": {
        "path": "/dev/sdr1",
        "uuid": "86ebd829-1405-43d3-8fd6-4cbc9b6ecf96"
    },
    "fsid": "86ebd829-1405-43d3-8fd6-4cbc9b6ecf96",
    "keyring": "AQBBJ/dZp57NIBAAtnuQS9WOS0hnLVe0rZnE6Q==",
    "kv_backend": "rocksdb",
    "magic": "ceph osd volume v026",
    "mkfs_done": "yes",
    "ready": "ready",
    "systemd": "",
    "type": "bluestore",
    "whoami": "3"
}
```
