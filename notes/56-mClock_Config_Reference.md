# 56: mClock Config Reference

**クリップソース:** [56: mClock Config Reference — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/configuration/mclock-config-ref/)

# mClock Config Reference[¶](https://docs.ceph.com/en/pacific/rados/configuration/mclock-config-ref/ "Permalink to this headline")

mclockプロファイルは、低レベルの詳細をユーザーから覆い隠し、ユーザーがmclockを簡単に設定できるようにします。

mclockプロファイルでQoS関連のパラメーターを設定するには、以下の入力パラメーターが必要です。

* 各OSDの総容量（IOPS）（自動決定）
* 有効にするmclockプロファイルタイプ

指定されたプロファイルの設定を使用して、OSDは下位レベルのmclockおよびCephのパラメータを決定して適用します。mclockプロファイルで適用されるパラメータにより、クライアントI/O、リカバリ/バックフィル操作、およびその他のバックグラウンド操作\(スクラブ、スナップトリム、PG削除など\)間のQoSを調整することができます。これらのバックグラウンドアクティビティは、Cephの内部クライアントのベストエフォートと見なされます。

## mClock Profiles \- Definition and Purpose[¶](https://docs.ceph.com/en/pacific/rados/configuration/mclock-config-ref/ "Permalink to this headline")

mclockプロファイルとは、「実行中のCephクラスタに適用すると、異なるクライアントクラス\(バックグラウンドリカバリ、スクラブ、スナップトリム、クライアントオペ、OSDサブオペ\)に属する操作\(IOPS\)のスロットルを可能にする構成設定」です。

mclockプロファイルは、容量制限と、ユーザーが選択したmclockプロファイルタイプを使用して、低レベルのmclockリソース制御パラメータを決定します。

プロファイルタイプに応じて、低レベルのmclockリソース制御パラメータと一部のCeph構成パラメータが透過的に適用されます。

低レベルmclockリソース制御パラメータは、「mClockに基づくQoS」の項で説明したように、リソースシェアの制御を提供する予約、制限、および重みです。

## mClock Profile Types[¶](https://docs.ceph.com/en/pacific/rados/configuration/mclock-config-ref/ "Permalink to this headline")

mclockプロファイルは、大きく2つのタイプに分類されます。

* **high\_client\_ops** \(_default_\): このプロファイルでは、Ceph内のバックグラウンドリカバリやその他の内部クライアントと比較して、外部クライアントの操作に多くの予約と制限が割り当てられます。このプロファイルはデフォルトで有効になっています。
    **high\_recovery\_ops**: このプロファイルは、外部クライアントやCeph内の他の内部クライアントと比較して、バックグラウンドリカバリに多くの予約を割り当てます。たとえば、管理者はこのプロファイルを一時的に有効にして、ピーク時以外の時間帯にバックグラウンドリカバリを高速化することができます。
    **balanced**: このプロファイルでは、クライアントOpsとバックグラウンドリカバリーOpsに同等の予約を割り当てます。
    **Built\-in**
* **Custom**

注：内蔵プロファイルのうち、mclockの内部クライアント（「スクラブ」、「スナップトリム」、「pg削除」など）には、予約数は若干少ないものの、ウェイトは高く、制限はありません。これは、他に競合するサービスがない場合に、これらの操作が迅速に完了するようにするためです。

## mClock Built\-in Profiles[¶](https://docs.ceph.com/en/pacific/rados/configuration/mclock-config-ref/ "Permalink to this headline")

内蔵プロファイルが有効になっている場合、mClockスケジューラは、クライアントタイプごとに有効になっているプロファイルに基づいて、低レベルのmclockパラメータ\[reservation、weight、limit\]を計算します。mclockパラメータは、あらかじめ用意されたOSDの最大容量に基づいて計算されます。そのため、内蔵プロファイルを使用する場合、以下のmclock設定パラメータは変更できません。

* osd\_mclock\_scheduler\_client\_res
* osd\_mclock\_scheduler\_client\_wgt
* osd\_mclock\_scheduler\_client\_lim
* osd\_mclock\_scheduler\_background\_recovery\_res
* osd\_mclock\_scheduler\_background\_recovery\_wgt
* osd\_mclock\_scheduler\_background\_recovery\_lim
* osd\_mclock\_scheduler\_background\_best\_effort\_res
* osd\_mclock\_scheduler\_background\_best\_effort\_wgt
* osd\_mclock\_scheduler\_background\_best\_effort\_lim

以下のCephのオプションは、ユーザが変更できません。

* osd\_max\_backfills
* osd\_recovery\_max\_active

これは、設定されたプロファイルの影響を最大化するために、上記のオプションがmclockスケジューラによって内部的に変更されるためです。

デフォルトでは、high\_client\_opsプロファイルが有効になっており、帯域幅の割り当てのうち、より多くの部分がクライアントopsに割り当てられるようになっています。バックグラウンドの復旧作業には、より低い割り当てが与えられます（そのため、完了までの時間が長くなります）。しかし、場合によっては、クライアントOpsまたはリカバリーOpsのいずれかに高い割り当てを与えなければならないことがあります。このような場合には、次のセクションで説明する手順に従って、代替の組み込みプロファイルを有効にすることができます。

いずれかのmClockプロファイル\(「custom」を含む\)がアクティブな場合、以下のCeph設定のスリープオプションは無効になります。

* osd\_recovery\_sleep
* osd\_recovery\_sleep\_hdd
* osd\_recovery\_sleep\_ssd
* osd\_recovery\_sleep\_hybrid
* osd\_scrub\_sleep
* osd\_delete\_sleep
* osd\_delete\_sleep\_hdd
* osd\_delete\_sleep\_ssd
* osd\_delete\_sleep\_hybrid
* osd\_snap\_trim\_sleep
* osd\_snap\_trim\_sleep\_hdd
* osd\_snap\_trim\_sleep\_ssd
* osd\_snap\_trim\_sleep\_hybrid

上記のスリープオプションは、mclockスケジューラがオペレーションキューから次のオペを選んでオペレーションシーケンサに転送するタイミングを判断できるようにするために無効になっています。これにより、すべてのクライアントに望ましいQoSが提供されます。

## Steps to Enable mClock Profile[¶](https://docs.ceph.com/en/pacific/rados/configuration/mclock-config-ref/ "Permalink to this headline")

すでに述べたように、デフォルトのmclockプロファイルはhigh\_client\_opsに設定されています。組み込みプロファイルの他の値としては、balancedおよびhigh\_recovery\_opsがあります。

デフォルトのプロファイルを変更する必要がある場合は、以下のコマンドを使用して、ランタイム中にオプションosd\_mclock\_profileを設定することができます。

```
# ceph config set osd.N osd_mclock_profile <value>
```

例えば、「osd.0」でより速いリカバリーができるようにプロファイルを変更するには、次のコマンドでhigh\_recovery\_opsプロファイルに切り替えます。

```
# ceph config set osd.0 osd_mclock_profile high_recovery_ops
```

注：カスタムプロファイルは、上級者以外にはお勧めできません。

これで完了です。これで、クラスター上でワークロードを実行し、QoS要件が満たされているかどうかを確認する準備が整いました。

## OSD Capacity Determination \(Automated\)[¶](https://docs.ceph.com/en/pacific/rados/configuration/mclock-config-ref/ "Permalink to this headline")

総IOPSで見たOSDの容量は、OSDの初期化時に自動的に決定されます。これは、OSDベンチツールを実行し、デバイスの種類に応じてosd\_mclock\_max\_capacity\_iops\_\[hdd,ssd\]オプションのデフォルト値を上書きすることで実現します。OSDの容量を設定するために、ユーザーが他の操作や入力をする必要はありません。クラスタを立ち上げた後、次のコマンドを使用してOSDの容量を確認することができます。

```
# ceph config show osd.N osd_mclock_max_capacity_iops_[hdd, ssd]
```

たとえば、次のコマンドは、基礎となるデバイスタイプがSSDであるCephノード上の「osd.0」の最大容量を表示します。

```
# ceph config show osd.0 osd_mclock_max_capacity_iops_ssd
```

## Steps to Manually Benchmark an OSD \(Optional\)[¶](https://docs.ceph.com/en/pacific/rados/configuration/mclock-config-ref/ "Permalink to this headline")

注：これらの手順は、OSD初期化時に自動的に決定されたOSD容量を上書きしたい場合にのみ必要です。それ以外の場合は、このセクションを完全にスキップしてください。

ヒント：すでにベンチマークデータを決定していて、手動でOSDの最大OSD容量を変更したい場合は、「最大OSD容量の指定」の項を読み飛ばしてください。

この目的には、既存のあらゆるベンチマークツールを使用できます。この場合、手順では、次のセクションで説明するCeph OSD Benchコマンドを使用します。使用するツール/コマンドにかかわらず、以下で説明する手順は同じです。

[QoS Based on mClock](https://docs.ceph.com/en/pacific/rados/configuration/mclock-config-ref/) のセクションですでに説明したように、シャードの数とBlueStoreのスロットル・パラメータはmclockのopキューに影響を与えます。そのため、mclock スケジューラの影響を最大化するためには、これらの値を慎重に設定することが重要です。

**稼働中のシャード数**

設定オプションのosd\_op\_num\_shards、osd\_op\_num\_shards\_hdd、osd\_op\_num\_shards\_ssdで定義されているデフォルトのシャード数を使用することをお勧めします。一般的に、シャード数が少ないと、mclockキューの影響が大きくなります。

**Bluestoreのスロットル・パラメータ**

bluestore\_throttle\_bytesおよびbluestore\_throttle\_deferred\_bytesで定義されているデフォルト値を使用することをお勧めします。しかし、これらのパラメータは、後述するベンチマークの段階で決定することもできます。

### OSD Bench Command Syntax[¶](https://docs.ceph.com/en/pacific/rados/configuration/mclock-config-ref/ "Permalink to this headline")

 [OSD Subsystem](https://docs.ceph.com/en/pacific/rados/configuration/mclock-config-ref/) の項では、OSDベンチコマンドについて説明しています。ベンチマークに使用する構文を以下に示します。

```
# ceph tell osd.N bench [TOTAL_BYTES] [BYTES_PER_WRITE] [OBJ_SIZE] [NUM_OBJS]
```

ここで,

* TOTAL\_BYTES: 総書き込みバイト数
* BYTES\_PER\_WRITE: 書き込み時のブロックサイズ
* OBJ\_SIZE: オブジェクトあたりのバイト数
* NUM\_OBJS: 書き込むオブジェクト数

### Benchmarking Test Steps Using OSD Bench[¶](https://docs.ceph.com/en/pacific/rados/configuration/mclock-config-ref/ "Permalink to this headline")

以下の手順では、デフォルトのシャードを使用し、正しいBlueStoreのスロットルの値を決定するための手順を詳しく説明します（オプション）。

1. Cephクラスタを起動し、ベンチマークを行うOSDをホストするCephノードにログインします。
2. 次のコマンドを使用して、OSD上で単純な4KiBランダム書き込みワークロードを実行します。

注：テストを実行する前に、正確な測定値を得るためにキャッシュをクリアする必要があることに注意してください。

例えば、osd.0でベンチマークテストを行う場合、以下のコマンドを実行します。

```
ceph tell osd.0 cache drop
```

```
ceph tell osd.0 bench 12288000 4096 4194304 100
```

1. osd benchコマンドの出力から得られた全体のスループット\(IOPS\)に注目してください。この値は、デフォルトのbluestoreのスロットルオプションが有効な場合のベースラインスループット\(IOPS\)です。
2. お使いの環境に合わせてBlueStoreのスロットル値を決定することが目的であれば、まずbluestore\_throttle\_bytesとbluestore\_throttle\_deferred\_bytesの2つのオプションをそれぞれ32KiB\(32768Bytes\)に設定してください。それ以外の場合は、次のセクションに進んでください。
3. 4KiBのランダム書き込みテストをOSDベンチを使って以前のように実行します。
4. 出力からの総合的なスループットを記録し、その値をステップ3で記録したベースライン・スループットと比較します。
5. スループットがベースラインと一致しない場合は、bluestoreのスロットルオプションを2倍に増やし、得られたスループットがベースラインの値に非常に近くなるまで、ステップ5から7を繰り返します。

例えば、NVMe SSDを搭載したマシンでのベンチマークでは、mclockの影響を最大化するために、bluestore throttleとdeferred bytesの両方で256KiBという値が決定されました。HDDの場合、対応する値は40 MiBで、全体のスループットはベースラインのスループットとほぼ同じでした。なお、一般的にHDDの場合、SSDと比較してbluestore throttleの値が高くなることが予想されます。

### Specifying  Max OSD Capacity[¶](https://docs.ceph.com/en/pacific/rados/configuration/mclock-config-ref/ "Permalink to this headline")

本項の手順は、OSDの初期化時に自動的に設定される最大OSD容量を上書きする場合にのみ実行できます。OSDのオプションosd\_mclock\_max\_capacity\_iops\_\[hdd,ssd\]は、以下のコマンドを実行して設定できます。

```
# ceph config set osd.N osd_mclock_max_capacity_iops_[hdd,ssd] <value>
```

たとえば、次のコマンドでは、基礎となるデバイスタイプがHDDである特定のOSD（「osd.0」とします）の最大容量を350 IOPSに設定します。

```
# ceph config set osd.0 osd_mclock_max_capacity_iops_hdd 350
```

または、Ceph構成ファイルの各\[osd.N\]セクションでOSDの最大容量を指定することもできます。詳細については、「[Config file section names](https://docs.ceph.com/en/pacific/rados/configuration/mclock-config-ref/) 」を参照してください。

## mClock Config Options[¶](https://docs.ceph.com/en/pacific/rados/configuration/mclock-config-ref/ "Permalink to this headline")

**osd\_mclock\_profile**

Description

これは、異なるクラス\(バックグラウンドリカバリ、スクラブ、スナップトリム、クライアントOP、OSDサブOP\)に属する操作に基づいてQoSを提供するために使用するmclockプロファイルのタイプを設定する。組み込みプロファイルを有効にすると、低レベルのmclockリソース制御パラメータ\[reservation、weight、limit\]および一部のCeph構成パラメータが透過的に設定される。上記は、カスタムプロファイルには適用されないことに注意

Type

_String_

Valid Choices

_high\_client\_ops, high\_recovery\_ops, balanced, custom_

Default

_high\_client\_ops_

**osd\_mclock\_max\_capacity\_iops\_hdd**

Description

OSDごとに考慮すべき最大IOPS容量（4KiBブロックサイズ時）（回転式メディアの場合）

Type

_Float_

Default

_315.0_

**osd\_mclock\_max\_capacity\_iops\_ssd**

Description

OSDごとに考慮すべき最大IOPS容量（4KiBブロックサイズ時）（ソリッドステートメディアの場合）

Type

_Float_

Default

_21500.0_

**osd\_mclock\_cost\_per\_io\_usec**

Description

OSDごとに考慮するIOあたりのコスト（マイクロ秒）（0でない場合は\_ssdと\_hddをオーバーライドする）

Type

**Float**

Default

_0.0_

**osd\_mclock\_cost\_per\_io\_usec\_hdd**

Description

OSDで考慮すべき1IOあたりのコスト（マイクロ秒）（回転メディアの場合）

Type

_Float_

Default

_25000.0_

**osd\_mclock\_cost\_per\_io\_usec\_ssd**

Description

OSDで考慮すべきIOあたりのコスト（マイクロ秒）（ソリッドステートメディアの場合）

Type

_Float_

Default

_50.0_

**osd\_mclock\_cost\_per\_byte\_usec**

Description

OSDで考慮すべき1バイトあたりのコスト（マイクロ秒）（0でない場合は\_ssdおよび\_hddをオーバーライドする）

Type

Float

Default

0.0

**osd\_mclock\_cost\_per\_byte\_usec\_hdd**

Description

OSDで考慮すべき1バイトあたりのコスト（マイクロ秒）（回転式メディアの場合）

Type

_Float_

Default

_5.2_

**osd\_mclock\_cost\_per\_byte\_usec\_ssd**

Description

OSDで考慮すべき1バイトあたりのコスト（マイクロ秒）（ソリッドステートメディアの場合）

Type

_Float_

Default

_0.011_
