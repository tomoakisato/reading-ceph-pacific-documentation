# 13: Ceph Mirrors — Ceph Documentation

**クリップソース:** [13: Ceph Mirrors — Ceph Documentation](https://docs.ceph.com/en/pacific/install/mirrors/)

# Ceph Mirrors¶

For improved user experience multiple mirrors for Ceph are available around the world.  ユーザーの利便性を向上させるために、Cephの複数のミラーが世界中で利用可能です。

These mirrors are kindly sponsored by various companies who want to support the Ceph project.  これらのミラーは、Cephプロジェクトを支援したいと考えている様々な企業の好意により提供されています。

## **Mirroring[¶](https://docs.ceph.com/en/pacific/install/mirrors/#mirroring "Permalink to this headline")**

You can easily mirror Ceph yourself using a Bash script and rsync. A easy to use script can be found at [Github](https://github.com/ceph/ceph/tree/master/mirroring).

When mirroring Ceph, please keep the following guidelines in mind:

* Choose a mirror close to you
* Do not sync in a interval shorter than 3 hours
* Avoid syncing at minute 0 of the hour, use something between 0 and 59

## **Becoming a mirror[¶](https://docs.ceph.com/en/pacific/install/mirrors/#becoming-a-mirror "Permalink to this headline")**

If you want to provide a public mirror for other users of Ceph you can opt to become a official mirror.

To make sure all mirrors meet the same standards some requirements have been set for all mirrors. These can be found on [Github](https://github.com/ceph/ceph/tree/master/mirroring).

If you want to apply for an official mirror, please contact the ceph\-users mailinglist.
