# 50: Messenger v2

**クリップソース:** [50: Messenger v2 — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/configuration/msgr2/)

# Messenger v2[¶](https://docs.ceph.com/en/pacific/rados/configuration/msgr2/ "Permalink to this headline")

## What is it[¶](https://docs.ceph.com/en/pacific/rados/configuration/msgr2/ "Permalink to this headline")

msgr2（messenger v2 protocol）は、Cephのオンワイヤプロトコルの2回目の大きな改訂版です。 いくつかの重要な機能が追加されています。

* ネットワーク上のすべてのデータを暗号化して送受信するセキュアモード
* 認証ペイロードのカプセル化を改善し、将来的にKerberosのような新しい認証モードの統合を可能にする。
* 先行する機能のアドバタイズとネゴシエーションを改善し、将来のプロトコル改訂に対応

Cephデーモンが複数のポートにバインドできるようになり、レガシーCephクライアントとv2対応の新しいクライアントの両方が同じクラスタに接続できるようになりました。

デフォルトでは、モニタは新しいv2プロトコル用の新しいIANA割り当てポート3300\(ce4hまたは0xce4\)にバインドし、レガシーv1プロトコル用の古いデフォルトポート6789にもバインドするようになりました。

## Address formats[¶](https://docs.ceph.com/en/pacific/rados/configuration/msgr2/ "Permalink to this headline")

Nautilus以前のネットワークアドレスは、1.2.3.4:567/89012のように、IPアドレス、ポート、ネットワーク上のクライアントやデーモンを一意に識別するためのnonceが表示されていました。Nautilusからは、3つの異なるアドレスタイプがあります。

* **v2**
* **v1**
* **TYPE\_ANY　**

デーモンが複数のポートにバインドするようになったため、単一のアドレスではなく、アドレスのベクトルで記述されるようになりました。 たとえば、Nautilus クラスタのモニタマップをダンプすると、次のような行が含まれるようになりました。

```
epoch 1
fsid 50fcf227-be32-4bcb-8b41-34ca8370bd16
last_changed 2019-02-25 11:10:46.700821
created 2019-02-25 11:10:46.700821
min_mon_release 14 (nautilus)
0: [v2:10.0.0.10:3300/0,v1:10.0.0.10:6789/0] mon.foo
1: [v2:10.0.0.11:3300/0,v1:10.0.0.11:6789/0] mon.bar
2: [v2:10.0.0.12:3300/0,v1:10.0.0.12:6789/0] mon.baz
```

アドレスの括弧付きリストまたはベクトルは、同じデーモンに複数のポート\(およびプロトコル\)で到達できることを意味します。 そのデーモンに接続するクライアントや他のデーモンは、可能であればv2プロトコル\(最初に記載\)を使用し、そうでなければレガシーのv1プロトコルに戻ります。 従来のクライアントには v1 のアドレスしか表示されず、以前のように v1 プロトコルで接続を続けます。

Nautilus では、mon\_host 設定オプションと \-m\<mon\-host\> コマンドライン・オプションが、同じ括弧付きアドレス・ベクトル構文をサポートしています。

### Bind configuration options[¶](https://docs.ceph.com/en/pacific/rados/configuration/msgr2/ "Permalink to this headline")

v1とv2のどちらのプロトコルを使用するかを制御する2つの設定オプションが追加されました。

* ms\_bind\_msgr1 \[default: true\] デーモンがv1プロトコルのポートにバインドするかどうかを制御します。
* ms\_bind\_msgr2 \[default: true\] デーモンがv2プロトコルを使用するポートにバインドするかどうかを制御します。

同様に、2つのオプションは、IPv4アドレスとIPv6アドレスのどちらを使用するかを制御します。

* ms\_bind\_ipv4 \[default: true\] デーモンがIPv4アドレスにバインドするかどうかを制御します。
* ms\_bind\_ipv6 \[default: false\] デーモンがIPv6アドレスにバインドするかどうかを制御します。

注：複数のポートにバインドできるようになったことで、IPv4とIPv6のデュアルスタックをサポートする道が開けました。 しかし、デュアルスタックのサポートは、Nautilus v14.2.0の時点ではまだテストされておらず、正しく動作するためには、いくつかの追加コードの変更が必要です。

## Connection modes[¶](https://docs.ceph.com/en/pacific/rados/configuration/msgr2/ "Permalink to this headline")

v2プロトコルでは、2つの接続モードをサポートしています。

* 接続確立時の強力な初期認証（cephxでは、中間者や盗聴者から保護された両当事者の相互認証）、および欠陥のあるハードウェアや宇宙線によるビット反転を防ぐためのcrc32cによる完全性チェック
    _CRCモードで提供されます_
* 秘匿性（ネットワーク上の盗聴者は、認証後のトラフィックをすべて見ることができる）または悪意のある中間者からの保護（crc32cの値を一致させるように注意していれば、通過するトラフィックを意図的に変更することができる）
    _crcモードでは提供されません。_
* 接続確立時の強力な初期認証（cephxでは、中間者や盗聴者から保護された両当事者の相互認証）、および認証後のすべてのトラフィックの完全な暗号化（暗号化による整合性チェックを含む）。Nautilusのセキュアモードでは、AES\-GCMストリーム暗号を使用しています。この暗号は、最新のプロセッサでは一般的に非常に高速です（例えば、SHA\-256暗号ハッシュよりも高速です）。
    _セキュアモードで提供されます_

### Connection mode configuration options[¶](https://docs.ceph.com/en/pacific/rados/configuration/msgr2/ "Permalink to this headline")

ほとんどの接続では、どのモードを使用するかを制御するオプションがあります。

* ms\_cluster\_modeは、Cephデーモン間のクラスター内通信に使用される接続モード\(または許可されたモード\)です。 複数のモードが記載されている場合は、最初に記載されているモードが優先されます。
* ms\_service\_modeは、クラスターへの接続時にクライアントが使用する許可されたモードのリストです。
* ms\_client\_modeは、Cephクラスタとの通信時にクライアントが使用する\(または許可する\)接続モードのリストで、優先順位が付けられています。

また、モニター専用のオプションも用意されており、管理者はモニターとの通信に異なる（通常はより安全な）要件を設定することができます。

* ms\_mon\_cluster\_modeは、モニター間で使用する接続モード（または許可されたモード）です。
* ms\_mon\_service\_modeは、クライアントや他のCephデーモンがモニターに接続するときに使用する許可されたモードのリストです。
* ms\_mon\_client\_modeは、クライアントや非モニターデーモンがモニターに接続する際に使用する接続モードのリストで、優先順位は以下の通りです。

## Transitioning from v1\-only to v2\-plus\-v1[¶](https://docs.ceph.com/en/pacific/rados/configuration/msgr2/ "Permalink to this headline")

デフォルトでは、Nautilus 14.2.zからms\_bind\_msgr2がtrueになっています。しかし、モニターがv2を使い始めるまでは、限られたサービスのみがv2アドレスの広告を開始します。

ほとんどのユーザーは、モニターがv1プロトコル用のデフォルトのレガシーポート6789にバインドしています。 このような場合、v2を有効にするには以下のように簡単です。

```
ceph mon enable-msgr2
```

モニターが標準外のポートにバインドされている場合は、v2用の追加ポートを明示的に指定する必要があります。 例えば、モニターmon.aが1.2.3.4:1111にバインドされていて、ポート1112にv2を追加したい場合、以下のようになります。

```
ceph mon set-addrs a [v2:1.2.3.4:1112,v1:1.2.3.4:1111]
```

モニターがv2にバインドすると、各デーモンは次回の再起動時にv2アドレスの広告を開始します。

## Updating ceph.conf and mon\_host[¶](https://docs.ceph.com/en/pacific/rados/configuration/msgr2/ "Permalink to this headline")

Nautilus以前では、CLIユーザーやデーモンは、通常、/etc/ceph/ceph.confのmon\_hostオプションを使ってモニターを検出します。 Nautilusでは、このオプションの構文が拡張され、新しいブラケット付きリスト形式に対応しました。 例えば、次のような古い行があります。

```
mon_host = 10.0.0.1:6789,10.0.0.2:6789,10.0.0.3:6789
```

以下に変更することができます。

```
mon_host = [v2:10.0.0.1:3300/0,v1:10.0.0.1:6789/0],[v2:10.0.0.2:3300/0,v1:10.0.0.2:6789/0],[v2:10.0.0.3:3300/0,v1:10.0.0.3:6789/0]
```

ただし、デフォルトのポート（3300、6789）を使用する場合は、省略可能です。

```
mon_host = 10.0.0.1,10.0.0.2,10.0.0.3
```

モニターでv2が有効になったら、ceph.confを更新して、ポートを指定しないようにするか\(通常はこれが一番簡単です\)、v2とv1の両方のアドレスを明示的に指定する必要があります。 ただし、新しいブラケット構文はNautilus以降でのみ理解できるため、cephパッケージがまだアップグレードされていないホストではこの変更を行わないでください。

ceph.confを更新するときは、新しいcephconfiggenerate\-minimal\-confコマンド\(モニタに到達するのに十分な情報を含む素の設定ファイルを生成\)とcephconfigassimilate\-conf\(設定ファイルのオプションをモニタの設定データベースに移動\)が役立つことに注意してください。 たとえば、以下のようになります。

```
# ceph config assimilate-conf < /etc/ceph/ceph.conf
# ceph config generate-minimal-config > /etc/ceph/ceph.conf.new
# cat /etc/ceph/ceph.conf.new
# minimal ceph.conf for 0e5a806b-0ce5-4bc6-b949-aa6f68f5c2a3
[global]
        fsid = 0e5a806b-0ce5-4bc6-b949-aa6f68f5c2a3
        mon_host = [v2:10.0.0.1:3300/0,v1:10.0.0.1:6789/0]
# mv /etc/ceph/ceph.conf.new /etc/ceph/ceph.conf

```

## Protocol[¶](https://docs.ceph.com/en/pacific/rados/configuration/msgr2/ "Permalink to this headline")

v2ワイヤプロトコルの詳細な説明は、「 [msgr2 protocol \(msgr2.0 and msgr2.1\)](https://docs.ceph.com/en/pacific/rados/configuration/msgr2/)」を参照してください。
