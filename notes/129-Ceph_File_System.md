# 129: Ceph File System

**クリップソース:** [129: Ceph File System — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/)

# Ceph File System[¶](https://docs.ceph.com/en/pacific/cephfs/ "Permalink to this headline")

Ceph File System（CephFS）は、Cephの分散オブジェクトストアであるRADOSの上に構築されたPOSIX準拠のファイルシステムです。CephFSは、共有ホームディレクトリ、HPCスクラッチスペース、分散ワークフロー共有ストレージといった従来のユースケースを含む様々なアプリケーションに対して、最先端の、多目的、高可用、高性能なファイルストアを提供しようと努めています。

CephFSは、いくつかの斬新なアーキテクチャの選択により、これらの目標を達成しています。 特に、ファイルのメタデータはファイルデータとは別のRADOSプールに格納され、サイズ変更可能なメタデータサーバ（MDS）のクラスタを通じて提供されます。 ファイルシステムのクライアントは、ファイルデータブロックの読み取りと書き込みのためにRADOSに直接アクセスすることができます。このため、ワークロードは、基盤となるRADOSオブジェクトストアのサイズに比例して拡張することができます。つまり、クライアントのデータI/Oを仲介するゲートウェイやブローカーは存在しません。

データへのアクセスは、クライアントとMDSが協調して維持する分散メタデータキャッシュの状態に対するオーソリティとして機能するMDSのクラスタを通じて調整されます。メタデータの変更は、各MDSによってRADOS上のジャーナルに効率的に書き込まれ、メタデータの状態はMDSによってローカルに保存されることはありません。このモデルにより、POSIXファイルシステムのコンテキスト内で、クライアント間のコヒーレントで迅速なコラボレーションが可能になります。
![image.png](image/image.png)

CephFSは、その斬新なデザインとファイルシステム研究への貢献から、数多くの学術論文の題材となっています。Cephの中で最も古いストレージインターフェイスであり、かつてはRADOSの主要なユースケースでした。 現在では、他の2つのストレージインターフェースと合わせて、最新の統合ストレージシステムを形成しています：RBD \(Ceph Block Devices\)とRGW \(Ceph Object Storage Gateway\)

## Getting Started with CephFS[¶](https://docs.ceph.com/en/pacific/cephfs/ "Permalink to this headline")

Cephのほとんどのデプロイメントでは、CephFSファイルシステムのセットアップは次のように簡単です：

```
ceph fs volume create <fs name>
```

Ceph [Orchestrator](https://docs.ceph.com/en/pacific/mgr/orchestrator)は、バックエンドのデプロイ技術がサポートする場合、ファイルシステムのMDSを自動的に作成および設定します\([Orchestratorのデプロイメント表](https://docs.ceph.com/en/pacific/mgr/orchestrator/#current-implementation-status)を参照\)。そうでない場合は、必要に応じて[MDSを手動でデプロイ](https://docs.ceph.com/en/pacific/cephfs/add-remove-mds)してください。

最後に、クライアントノードにCephFSをマウントするには、「[CephFSのマウント: 前提条件](https://docs.ceph.com/en/pacific/cephfs/mount-prerequisites)」ページを参照してください。さらに、[cephfs\-shell](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell)を使用して、対話型アクセスやスクリプトを実行するためのコマンドラインシェルユーティリティが利用できます。
