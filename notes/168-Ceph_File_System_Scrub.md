# 168: Ceph File System Scrub

**クリップソース:** [168: Ceph File System Scrub — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/scrub/)

# Ceph File System Scrub[¶](https://docs.ceph.com/en/pacific/cephfs/scrub/ "Permalink to this headline")

CephFSでは、クラスタ管理者がscrubコマンドを使用してファイルシステムの一貫性をチェックすることができます。スクラブは2つの部分に分類されます：

1. フォワードスクラブ：ファイルシステムのルート（またはサブディレクトリ）からスクラブを開始し、その階層で触れることができるすべてのものを見て、一貫性を確保する方法です。
2. バックワードスクラブ：ファイルシステムプール内のすべてのRADOSオブジェクトを調べ、ファイルシステム階層にマップバックします。

本書は、フォワードスクラブ（以降スクラブ）の開始と制御に関するコマンドを詳述します。

警告： CephFSのフォワードスクラブは、ランク0上で開始し操作します。すべてのスクラブコマンドは、ランク0に向ける必要があります。

## Initiate File System Scrub[¶](https://docs.ceph.com/en/pacific/cephfs/scrub/ "Permalink to this headline")

ディレクトリツリーに対してスクラブを開始するには、次のコマンドを使用します：

```
ceph tell mds.<fsname>:0 scrub start <path> [scrubopts] [tag]
```

scrubopts は recursive, force, repair のカンマ区切りリスト、tag はオプションのカスタム文字列タグ（デフォルトは生成された UUID）です。コマンド例：

```
ceph tell mds.cephfs:0 scrub start / recursive
{
    "return_code": 0,
    "scrub_tag": "6f0d204c-6cfd-4300-9e02-73f382fd23c1",
    "mode": "asynchronous"
}
```

Recursiveスクラブは非同期です\(上記出力のmodeにヒントがあります\)。非同期スクラブの状態を知るには scrub status を使ってポーリングしなければなりません。

scrubタグはスクラブを区別するために使用され、また、各inodeのデフォルトデータプールの最初の（バックトレース情報が格納されている）データオブジェクトにscrub\_tag拡張属性でタグの値をマークするために使用されています。RADOSユーティリティを使用して拡張属性を見ることで、inodeがスクラブされたことを確認することができます。

スクラブは複数 active MDS（複数ランク）で動作します。スクラブはランク0が管理し、適宜MDSに分散されます。

## Monitor \(ongoing\) File System Scrubs[¶](https://docs.ceph.com/en/pacific/cephfs/scrub/ "Permalink to this headline")

進行中のスクラブの状態は、scrub statusコマンドで監視、ポーリングすることができます。このコマンドは、進行中のスクラブ（タグで識別）を、スクラブ開始に使用したパスとオプションとともにリストアップします。

```
ceph tell mds.cephfs:0 scrub status
{
    "status": "scrub active (85 inodes in the stack)",
    "scrubs": {
        "6f0d204c-6cfd-4300-9e02-73f382fd23c1": {
            "path": "/",
            "options": "recursive"
        }
    }
}
```

statusは、ある時点でスクラブを行う予定の inode の数を示しており、それ以降の scrub status 呼び出しで変更されます。また、ceph statusには、スクラブ動作のサマリー（動作状態、スクラブのトリガーとなったパスなど）が表示されます。

```
ceph status
[...]

task status:
  scrub status:
      mds.0: active [paths:/]

[...]
```

このリストに表示されなくなったら、スクラブは完了です（将来のリリースでは変更される可能性があります）。ダメージがあった場合は、クラスタヘルス警告で報告されます。

## Control \(ongoing\) File System Scrubs[¶](https://docs.ceph.com/en/pacific/cephfs/scrub/ "Permalink to this headline")

* Pause: 進行中のスクラブ処理を一時停止すると、（現在スクラブ処理中のinodeに対する）実行中RADOS操作が終了した後、新規または保留中のinodeはスクラブされなくなります。

```
ceph tell mds.cephfs:0 scrub pause
{
    "return_code": 0
}
```

一時停止後のscrub statusは一時停止状態を反映しています。この時点で、\(scrub start による\)新しい scrub 操作を開始すると、inode を scrub キューに入れるだけです。

```
ceph tell mds.cephfs:0 scrub status
{
    "status": "PAUSED (66 inodes in the stack)",
    "scrubs": {
        "6f0d204c-6cfd-4300-9e02-73f382fd23c1": {
            "path": "/",
            "options": "recursive"
        }
    }
}
```

* Resume: 再開は、一時停止していたスクラブ操作を開始します：

```
ceph tell mds.cephfs:0 scrub resume
{
    "return_code": 0
}
```

* Abort: 進行中のスクラブ操作を中止すると、（現在スクラブ中の inode に対する）実行中 RADOS 操作が終了した後に、保留中の inode をスクラブキューから削除します（これにより、スクラブは中止されます）。

```
ceph tell mds.cephfs:0 scrub abort
{
    "return_code": 0
}
```
