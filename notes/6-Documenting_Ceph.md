# 6: Documenting Ceph

**クリップソース:** [6: Documenting Ceph — Ceph Documentation](https://docs.ceph.com/en/pacific/start/documenting-ceph/)

# Documenting Ceph¶

Cephプロジェクトを支援する最も簡単な方法は、ドキュメントに貢献することです。Cephのユーザベースが拡大し、開発ペースが速くなるにつれて、ドキュメントを更新して新しい情報を追加する人が増えています。スペルミスの修正や説明の明確化などの小さな貢献でも、Cephプロジェクトの大きな助けになります。

Cephのドキュメントソースは、Cephリポジトリのceph/docディレクトリに存在し、Python SphinxがソースをHTMLやmanpageにレンダリングします。[http://ceph.com/docs](http://ceph.com/docs) リンクでは現在、デフォルトでmasterブランチが表示されますが、masterを希望するブランチ名に置き換えることで、古いブランチ\(argonautなど\)や将来のブランチ\(nextなど\)、さらにはワークインプログレスブランチのドキュメントを表示することができます。

## **Making Contributions[¶](https://docs.ceph.com/en/pacific/start/documenting-ceph/#making-contributions "Permalink to this headline")**

ただし、プログラム・ソースをコンパイルする代わりに、ドキュメント・ソースをビルドする必要があることを除けば、ドキュメントへの貢献は、コードへの貢献と同じ手順で行われます。この手順には以下のようなステップがあります。

### Get the Source[¶](https://docs.ceph.com/en/pacific/start/documenting-ceph/#get-the-source "Permalink to this headline")

Cephのドキュメントは、Cephリポジトリのceph/docディレクトリにCephのソースコードと一緒に置かれています。githubとCephの詳細については、「[Cephのコミュニティに参加しよう！](https://docs.ceph.com/en/pacific/start/get-involved/#get-involved)」を参照してください。

貢献を行う最も一般的な方法は、[Fork and Pull](https://help.github.com/articles/using-pull-requests)アプローチを使用することです。必要があります。

1. ローカルにgitをインストールします。Debian/Ubuntuの場合、実行します。  
   ![image.png](image/image-0.png)

2. .gitconfigファイルにあなたの名前とメールアドレスが書かれていることを確認します。  
   ![image-1.png](image/image-1.png)  

3. [github](http://github.com/)アカウントを作成する（アカウントを持っていない場合）。
4. Cephプロジェクトをフォークします。[https://github.com/ceph/ceph](https://github.com/ceph/ceph) をご覧ください。
5. 作成したCephプロジェクトのフォークをローカルホストにクローンします。

Cephは、主に主要なコンポーネントによってドキュメントを情報アーキテクチャに整理します。

* **Ceph Storage Cluster:**
* **Ceph Block Device:**
* **Ceph Object Storage:**
* **Ceph File System:**
* **Installation \(Quick\):**
* **Installation \(Manual\):**
* **Manpage:**
* **Developer:**
* **Images:**

### Select a Branch[¶](https://docs.ceph.com/en/pacific/start/documenting-ceph/#select-a-branch "Permalink to this headline")

誤字脱字の修正や説明の明確化など、ドキュメントに小さな変更を加える場合は、master ブランチを使用します（デフォルト）。また、現在のリリースに含まれている機能に貢献する場合も、master ブランチを使用する必要があります。master は最もよく使用されるブランチです。

```
git checkout master

```

次期リリースに影響するドキュメントの変更を行う場合は、nextブランチを使用します。nextは2番目によく使われるブランチです。

```
git checkout next

```

現在のリリースに含まれていない新機能などの実質的な貢献を行う場合、貢献がトラッカーIDを持つ問題に関連する場合、またはmasterブランチにマージされる前にCeph.comのWebサイトでドキュメントの表示を確認したい場合は、ブランチを作成してください。ドキュメントの更新のみを含むブランチを区別するために、慣習的にブランチの前にwip\-docを付け、wip\-doc\-{your\-branch\-name}という形にしています。ブランチが [http://tracker.ceph.com/issues](http://tracker.ceph.com/issues) に提出された課題に関連している場合、ブランチ名には課題番号が組み込まれます。例えば、ドキュメントブランチがissue \#4000の修正である場合、ブランチ名は慣習的にwip\-doc\-4000となり、関連するトラッカーURLは[http://tracker.ceph.com/issues/4000](http://tracker.ceph.com/issues/4000)。

Note

ドキュメントへの貢献とソースコードへの貢献を1つのコミットに混在させないでください。ドキュメントへの貢献とソースコードへの貢献を別々にすることで、レビュープロセスを簡素化することができます。機能や設定オプションを追加するプルリクエストには、関連する変更やオプションを記述したドキュメントのコミットも含めることを強くお勧めします。

ブランチ名を作成する前に、そのブランチ名がローカルまたはリモートのリポジトリにすでに存在していないことを確認してください。

```
git branch -a | grep wip-doc-{your-branch-name}

```

存在していない場合は、ブランチを作成します。

```
git checkout -b wip-doc-{your-branch-name}

```

### Make a Change[¶](https://docs.ceph.com/en/pacific/start/documenting-ceph/#make-a-change "Permalink to this headline")

ドキュメントの変更には、reStructuredTextファイルを開き、その内容を変更し、変更を保存する必要があります。構文要件の詳細については、「[Documentation Style Guide](https://docs.ceph.com/en/pacific/start/documenting-ceph/#documentation-style-guide)」を参照してください。

ドキュメントの追加には、docディレクトリツリー内に新しいreStructuredTextファイル（拡張子は\*.rst）を作成します。また、ドキュメントへの参照（ハイパーリンクや目次のエントリ）を含める必要があります。トップレベルのディレクトリのindex.rstファイルには、通常TOCが含まれており、ここに新しいファイル名を追加することができます。すべてのドキュメントにはタイトルが必要です。詳しくは「[Headings](https://docs.ceph.com/en/pacific/start/documenting-ceph/#headings)」をご覧ください。

新しいドキュメントは、git によって自動的に追跡されません。ドキュメントをリポジトリに追加するには、git add {path\-to\-filename} を使わなければなりません。たとえば、リポジトリのトップレベルのディレクトリから example.rst ファイルを rados サブディレクトリに追加する場合は次のようになります。

```
git add doc/rados/example.rst

```

ドキュメントを削除するには、git rm {path\-to\-filename} でそのドキュメントをリポジトリから削除します。たとえば、以下のようになります。

```
git rm doc/rados/example.rst

```

また、削除した文書への参照を他の文書から削除する必要があります。

### Build the Source[¶](https://docs.ceph.com/en/pacific/start/documenting-ceph/#build-the-source "Permalink to this headline")

ドキュメントを構築するには、cephのリポジトリディレクトリに移動します。

```
cd ceph

```

Note

これらのコマンドが動作するためには、build\-docとserve\-docを含むディレクトリが環境変数PATHに含まれている必要があります。

Debian/Ubuntu、Fedora、CentOS/RHELでドキュメントをビルドするには、次のように実行します。

```
admin/build-doc

```

外部リンクの到達可能性をスキャンするために、実行します。

```
admin/build-doc linkcheck

```

admin/build\-docを実行すると、cephの下にbuild\-docディレクトリが作成されます。Javadocファイルを出力するために、ceph/build\-docの下にディレクトリを作成する必要があるかもしれません。

```
mkdir -p output/html/api/libcephfs-java/javadoc

```

ビルドスクリプトbuild\-docは、エラーと警告を出力します。変更をコミットする前に、変更したドキュメントのエラーを修正しなければなりません（MUST）。また、変更した構文に関連する警告を修正すべきです（SHOULD）。

Important

すべてのハイパーリンクを検証する必要があります。ハイパーリンクが壊れていると、自動的にビルドも壊れてしまいます。

ドキュメントセットを構築したら、http://localhost:8080/ で HTTP サーバーを起動して、ドキュメントを見ることができます。

```
admin/serve-doc

```

また、build\-doc/outputに移動して、ビルドされたドキュメントを確認することもできます。htmlディレクトリとmanディレクトリには、それぞれHTML形式とmanページ形式のドキュメントが含まれているはずです。

#### **Build the Source \(First Time\)[¶](https://docs.ceph.com/en/pacific/start/documenting-ceph/#build-the-source-first-time "Permalink to this headline")**

CephはPython Sphinxを使用しており、一般的にディストリビューションに依存しません。Cephのドキュメントを初めて構築するときには、doxygen XMLツリーが生成されますが、これは少し時間がかかります。

Python Sphinxには、ディストリビューションによって異なるいくつかの依存関係があります。初めてドキュメントをビルドするときに、依存関係がインストールされていない場合は、スクリプトによって通知されます。Sphinxを実行してドキュメントを正常にビルドするには、以下のパッケージが必要です。

|### Debian/Ubuntu

* gcc
* python3\-dev
* python3\-pip
* python3\-sphinx
* pytnon3\-venv
* libxml2\-dev
* libxslt1\-dev
* doxygen
* graphviz
* ant
* ditaa

|### Fedora

* gcc
* python\-devel
* python\-pip
* python\-docutils
* python\-jinja2
* python\-pygments
* python\-sphinx
* libxml2\-devel
* libxslt1\-devel
* doxygen
* graphviz
* ant
* ditaa

|### CentOS/RHEL

* gcc
* python\-devel
* python\-pip
* python\-docutils
* python\-jinja2
* python\-pygments
* python\-sphinx
* libxml2\-dev
* libxslt1\-dev
* doxygen
* graphviz
* ant

|
|----------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------|

ホストにインストールされていない各依存関係をインストールします。Debian/Ubuntu ディストリビューションの場合、以下を実行してください。

```
sudo apt-get install gcc python-dev python-pip libxml2-dev libxslt-dev doxygen graphviz ant ditaa
sudo apt-get install python-sphinx

```

Fedora ディストリビューションの場合、以下を実行してください。

```
sudo yum install gcc python-devel python-pip libxml2-devel libxslt-devel doxygen graphviz ant
sudo pip install html2text
sudo yum install python-jinja2 python-pygments python-docutils python-sphinx
sudo yum install jericho-html ditaa

```

CentOS/RHEL ディストリビューションでは、epel \(Extra Packages for Enterprise Linux\) リポジトリを使用することをお勧めします。これは、デフォルトのリポジトリでは利用できない追加パッケージを提供するためです。epelをインストールするには、次のように実行します。

```
sudo yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm

```

CentOS/RHEL ディストリビューションの場合は、以下を実行してください。

```
sudo yum install gcc python-devel python-pip libxml2-devel libxslt-devel doxygen graphviz ant
sudo pip install html2text

```

CentOS/RHEL ディストリビューションの場合、残りの python パッケージは default および epel リポジトリでは入手できません。そこで、http://rpmfind.net/ を使ってパッケージを探します。そして、ミラーからダウンロードして、インストールしてください。例えば、以下のようになります。

```
wget http://rpmfind.net/linux/centos/7/os/x86_64/Packages/python-jinja2-2.7.2-2.el7.noarch.rpm
sudo yum install python-jinja2-2.7.2-2.el7.noarch.rpm
wget http://rpmfind.net/linux/centos/7/os/x86_64/Packages/python-pygments-1.4-9.el7.noarch.rpm
sudo yum install python-pygments-1.4-9.el7.noarch.rpm
wget http://rpmfind.net/linux/centos/7/os/x86_64/Packages/python-docutils-0.11-0.2.20130715svn7687.el7.noarch.rpm
sudo yum install python-docutils-0.11-0.2.20130715svn7687.el7.noarch.rpm
wget http://rpmfind.net/linux/centos/7/os/x86_64/Packages/python-sphinx-1.1.3-11.el7.noarch.rpm
sudo yum install python-sphinx-1.1.3-11.el7.noarch.rpm

```

Cephのドキュメントではditaaが多用されていますが、現在のところCentOS/RHEL7向けにはビルドされていません。ditaa図を変更する場合は、ditaaをインストールする必要があります。これにより、新規または変更したditaa図をコミットする前に、適切にレンダリングされることを確認できます。CentOS/RHELのディストリビューションに対応した互換性のある必要パッケージを取得して、手動でインストールすることもできます。CentOS/RHEL7でditaaを動作させるには、以下の依存関係が必要です。

* jericho\-html
* jai\-imageio\-core
* batik

[http://rpmfind.net/](http://rpmfind.net/) を使って、互換性のある [ditaa](http://ditaa.sourceforge.net/) とその依存関係を探します。その後、ミラーからダウンロードしてインストールします。例えば

```
wget http://rpmfind.net/linux/fedora/linux/releases/22/Everything/x86_64/os/Packages/j/jericho-html-3.3-4.fc22.noarch.rpm
sudo yum install jericho-html-3.3-4.fc22.noarch.rpm
wget http://rpmfind.net/linux/centos/7/os/x86_64/Packages/jai-imageio-core-1.2-0.14.20100217cvs.el7.noarch.rpm
sudo yum install jai-imageio-core-1.2-0.14.20100217cvs.el7.noarch.rpm
wget http://rpmfind.net/linux/centos/7/os/x86_64/Packages/batik-1.8-0.12.svn1230816.el7.noarch.rpm
sudo yum install batik-1.8-0.12.svn1230816.el7.noarch.rpm
wget http://rpmfind.net/linux/fedora/linux/releases/22/Everything/x86_64/os/Packages/d/ditaa-0.9-13.r74.fc21.noarch.rpm
sudo yum install ditaa-0.9-13.r74.fc21.noarch.rpm

```

これらのパッケージをすべてインストールしたら、「ソースのビルド」で説明している手順に従って、ドキュメントをビルドします。

### Commit the Change[¶](https://docs.ceph.com/en/pacific/start/documenting-ceph/#commit-the-change "Permalink to this headline")

Cephのドキュメントのコミットはシンプルですが、厳格な規約に従っています：

* コミットには、コミットごとに1つのファイルが必要（ロールバックが簡素化される）。 関連する変更を加えた複数のファイルをコミットできる。 無関係な変更を同じコミットに入れてはいけない
* コミットには必ずコメントが必要
* コミットメントの先頭には（厳密に）doc:を付けなければならない
* コメントの要約は（厳密に）1行のみでなければならない
* 追加コメントは、サマリーの後に空白行を設けてもよいが、簡潔なものにすること
* コミットには、Fixes:\#{バグ番号}を含めてもよい
* コミットには、（厳密に）Signed\-off\-by: Firstname Lastname \<email\> を必ず含めなければならない

Tip

特に（厳密に）と書いてあるところは、上記の規約に従わないと、この規約に従うようにコミットを修正するよう要求されます。

以下は一般的な（好ましい）コミットコメントです：

```
doc: Fixes a spelling error and a broken hyperlink.

Signed-off-by: John Doe <john.doe@gmail.com>
```

以下のコメントには、バグへの言及が含まれています：

```
doc: Fixes a spelling error and a broken hyperlink.

Fixes: #1234

Signed-off-by: John Doe <john.doe@gmail.com>
```

以下のコメントには、コメントの要約に続く簡潔な文章が含まれています。要約行と説明文の間には、キャリッジリターンがあります：

```
doc: Added mon setting to monitor config reference

Describes 'mon setting', which is a new setting added
to config_opts.h.

Signed-off-by: John Doe <john.doe@gmail.com>
```

変更をコミットするには、以下を実行します：

```
git commit -a
```

ドキュメントのコミットを管理する簡単な方法は、git用のビジュアルツールを使用することです。例えば、gitkはリポジトリの履歴を見るためのグラフィカルなインターフェースを提供し、git\-guiは未コミットの変更を見たり、コミットのためにステージングしたり、変更をコミットしたり、フォークしたCephリポジトリにプッシュするためのグラフィカルなインターフェースを提供します。

Debian/Ubuntuの場合：

```
sudo apt-get install gitk git-gui
```

Fedora/CentOS/RHELの場合：

```
sudo yum install gitk git-gui
```

そして、実行します：

```
cd {git-ceph-repo-path}
gitk
```

最後に、File\-\>Start git gui を選択して、グラフィカル・ユーザー・インターフェースを有効にします。

### Push the Change[¶](https://docs.ceph.com/en/pacific/start/documenting-ceph/#push-the-change "Permalink to this headline")

1つ以上のコミットができたら、それをリポジトリのローカルコピーからgithubにプッシュする必要があります。git\-gui のようなグラフィカルなツールは、リポジトリにプッシュするためのユーザーインターフェイスを提供します。ブランチを作成した場合：

```
git push origin wip-doc-{your-branch-name}
```

Otherwise:

```
git push
```

### Make a Pull Request[¶](https://docs.ceph.com/en/pacific/start/documenting-ceph/#make-a-pull-request "Permalink to this headline")

先に述べたように、[Fork and Pull](https://help.github.com/articles/using-pull-requests)のアプローチでドキュメントのコントリビューションを行うことができます。

### Notify Us[¶](https://docs.ceph.com/en/pacific/start/documenting-ceph/#notify-us "Permalink to this headline")

プルリクエストを行った後、[ceph\-docs@redhat.com](http://ceph-docs%40redhat.com) にメールを送ってください。

## **Documentation Style Guide[¶](https://docs.ceph.com/en/pacific/start/documenting-ceph/#documentation-style-guide "Permalink to this headline")**

Cephドキュメントプロジェクトの目的の1つは、ネイティブのrestructuredText形式と、HTMLなどのレンダリング形式の両方でドキュメントの読みやすさを確保することです。Cephリポジトリに移動し、ネイティブフォーマットでドキュメントを表示します。ターミナルでは、レンダリングされたHTML形式と同じように読みやすいことに気付くかもしれません。さらに、ditaa形式の図もテキストモードで適切に表示されることに気づくかもしれません：

```
less doc/architecture.rst
```

この一貫性を維持するには、次のスタイルガイドを確認してください。

### Headings[¶](https://docs.ceph.com/en/pacific/start/documenting-ceph/#headings "Permalink to this headline")

1. **Document Titles:**
2. **Section Titles:**
3. **Subsection Titles:**

### Text Body[¶](https://docs.ceph.com/en/pacific/start/documenting-ceph/#text-body "Permalink to this headline")

一般的なルールとして、私たちはテキストを80列で折り返し、先頭や末尾に空白がなくコマンドラインインターフェイスで読みやすいようにすることを希望します。可能であれば、テキスト、リスト、リテラルテキスト（例外あり）、表、ディタグラフィックについて、この規則を維持することが望ましいです。

1. **Paragraphs**
2. **Literal Text:**
3. インデントされたテキストには、リテラルテキストの例が含まれることがあります。テキストのインデントはスペースで行うべきですが、リテラルテキストの例はタブでインデントされるべきです。この規約により、空白行を残し、スペースでインデントして段落を開始することで、文字列の例に続いてインデントされた段落を追加することができます。
    **Indented Text:**
4. **Numbered Lists:**
5. **Code Examples:**

### Paragraph Level Markup[¶](https://docs.ceph.com/en/pacific/start/documenting-ceph/#paragraph-level-markup "Permalink to this headline")

Cephプロジェクトは、[段落レベルのマークアップ](http://sphinx-doc.org/markup/para.html)を使用してポイントを強調表示します。

1. **Tip:**
2. **Note**
3. **Important:**
4. **Version Added:**
5. **Version Changed:**
6. **Deprecated:**
7. **Topic:**

### TOC and Hyperlinks[¶](https://docs.ceph.com/en/pacific/start/documenting-ceph/#toc-and-hyperlinks "Permalink to this headline")

すべての文書は、他の文書または目次からリンクされている必要があります。そうでない場合は、文書を構築する際に警告が表示されます。

Cephプロジェクトは、.. toctree:: 指示を使用します。 詳細については、[目次ツリー](http://sphinx-doc.org/markup/toctree.html)を参照してください。 TOCをレンダリングするときは、レンダリングされたTOCが適度に簡潔になるように:maxdepth: パラメーターを指定することを検討してください。

ドキュメント作成者は、ソースファイルが移動されたり情報アーキテクチャが変更されたりしてもリンクが機能するように、リンク先が特定の一意な識別子（e.g., .. \_unique\-target\-id: ）を含み、ターゲットへの参照がターゲットを明確に参照する（e.g., :ref:\`unique\-target\-id\`） :ref: 構文を使用することを推奨します。詳しくは、[任意の場所を相互参照する](http://www.sphinx-doc.org/en/master/usage/restructuredtext/roles.html#role-ref)を参照してください。

Cephのドキュメントでは、バッククォート（アクセント記号）文字の後にリンクテキスト、別のバッククォート、およびアンダースコアが続きます。 Sphinxを使用すると、リンク先をインラインで組み込むことができます。 ただし、コマンドラインインターフェイスでのドキュメントの読みやすさを向上させるため、ドキュメントの下部に.. \_Link Text: ../path 規則を使用することをお勧めします。
