# 32: Custom Container Service

**クリップソース:** [32: Custom Container Service — Ceph Documentation](https://docs.ceph.com/en/pacific/cephadm/services/custom-container/)

# Custom Container Service[¶](https://docs.ceph.com/en/pacific/cephadm/services/custom-container/ "Permalink to this headline")

オーケストレーターでは、YAMLファイルを使ってカスタムコンテナをデプロイすることができます。対応する [Service Specification](https://docs.ceph.com/en/pacific/cephadm/services/custom-container/) は次のようなものでなければなりません。

```
service_type: container
service_id: foo
placement:
    ...
spec:
  image: docker.io/library/foo:latest
  entrypoint: /usr/bin/foo
  uid: 1000
  gid: 1000
  args:
    - "--net=host"
    - "--cpus=2"
  ports:
    - 8080
    - 8443
  envs:
    - SECRET=mypassword
    - PORT=8080
    - PUID=1000
    - PGID=1000
  volume_mounts:
    CONFIG_DIR: /etc/foo
  bind_mounts:
    - ['type=bind', 'source=lib/modules', 'destination=/lib/modules', 'ro=true']
  dirs:
    - CONFIG_DIR
  files:
    CONFIG_DIR/foo.conf:
      - refresh=true
      - username=xyz
      - "port: 1234"

```

ここで、サービス仕様のプロパティは

* サービスのユニークな名前
    service\_id
* Dockerイメージの名前
    image
* ホストシステムでディレクトリやファイルを作成する際に使用するUID
    uid
* ホストシステムでディレクトリやファイルを作成する際に使用するGID
    gid
* イメージのデフォルトENTRYPOINTを上書きする
    entrypoint
* Podman/Dockerの追加コマンドライン引数のリスト
    args
* ホストのファイアウォールで開くTCPポートのリスト
    ports
* 環境変数のリスト
    envs
* バインドマウントを使用すると、ホストマシン上のファイルまたはディレクトリがコンテナにマウントされます。相対的なsource=...パスは、/var/lib/ceph/\<cluster\-fsid\>/\<daemon\-name\>以下に配置されます。
    bind\_mounts
* ボリュームマウントを使用すると、ホストマシン上のDockerのストレージディレクトリ内に新しいディレクトリが作成され、Dockerがそのディレクトリのコンテンツを管理します。相対的なソースパスは、/var/lib/ceph/\<cluster\-fsid\>/\<daemon\-name\>以下に配置されます。
    volume\_mounts
* /var/lib/ceph/\<cluster\-fsid\>/\<daemon\-name\>以下に作成されるディレクトリのリスト
    dirs
* ファイルの相対パスをキーとし、ファイルの内容を値とする辞書です。内容が文字列の場合は二重引用符で囲む必要があります。その場合の改行には'n'を使用する。それ以外の場合は、複数行のコンテンツを文字列のリストとして定義します。指定されたファイルは、ディレクトリ /var/lib/ceph/\<cluster\-fsid\>/\<daemon\-name\> 以下に作成されます。ファイルが作成されるディレクトリの絶対パスが存在する必要があります。必要に応じてdirsプロパティを使用して作成してください。
    files
