# 110: ceph-mon – ceph monitor daemon

 # ceph\-mon – ceph monitor daemon[¶](https://docs.ceph.com/en/pacific/man/8/ceph-mon/ "Permalink to this headline")

## Synopsis[¶](https://docs.ceph.com/en/pacific/man/8/ceph-mon/ "Permalink to this headline")

**ceph\-mon** \-i _monid_ \[ –mon\-data _mondatapath_ \]

## Description[¶](https://docs.ceph.com/en/pacific/man/8/ceph-mon/ "Permalink to this headline")

**ceph\-mon** is the cluster monitor daemon for the Ceph distributed file system. One or more instances of **ceph\-mon** form a Paxos part\-time parliament cluster that provides extremely reliable and durable storage of cluster membership, configuration, and state.

The _mondatapath_ refers to a directory on a local file system storing monitor data. It is normally specified via the `mon data` option in the configuration file.

## Options[¶](https://docs.ceph.com/en/pacific/man/8/ceph-mon/ "Permalink to this headline")

`-f````, ``--foreground```[¶](https://docs.ceph.com/en/pacific/man/8/ceph-mon/ "Permalink to this definition")Foreground: do not daemonize after startup \(run in foreground\). Do not generate a pid file. Useful when run via [ceph\-run](https://docs.ceph.com/en/pacific/man/8/ceph-mon/)\(8\).

`-d```[¶](https://docs.ceph.com/en/pacific/man/8/ceph-mon/ "Permalink to this definition")Debug mode: like `-f`, but also send all log output to stderr.

`--setuser`` userorgid`[¶](https://docs.ceph.com/en/pacific/man/8/ceph-mon/ "Permalink to this definition")Set uid after starting. If a username is specified, the user record is looked up to get a uid and a gid, and the gid is also set as well, unless –setgroup is also specified.

`--setgroup`` grouporgid`[¶](https://docs.ceph.com/en/pacific/man/8/ceph-mon/ "Permalink to this definition")Set gid after starting. If a group name is specified the group record is looked up to get a gid.

`-c`` ceph.conf``, ``--conf``=ceph.conf`[¶](https://docs.ceph.com/en/pacific/man/8/ceph-mon/ "Permalink to this definition")Use _ceph.conf_ configuration file instead of the default`/etc/ceph/ceph.conf` to determine monitor addresses during startup.

`--mkfs```[¶](https://docs.ceph.com/en/pacific/man/8/ceph-mon/ "Permalink to this definition")Initialize the `mon data` directory with seed information to form and initial ceph file system or to join an existing monitor cluster. Three pieces of information must be provided:

* The cluster fsid. This can come from a monmap \(`--monmap <path>`\) or explicitly via `--fsid <uuid>`.
* A list of monitors and their addresses. This list of monitors can come from a monmap \(`--monmap <path>`\), the `mon host`configuration value \(in _ceph.conf_ or via `-m
    host1,host2,...`\), or \(for backward compatibility\) the deprecated `mon addr` lines in _ceph.conf_. If this monitor is to be part of the initial monitor quorum for a new Ceph cluster, then it must be included in the initial list, matching either the name or address of a monitor in the list. When matching by address, either the `public addr` or `public
    subnet` options may be used.
* The monitor secret key `mon.`. This must be included in the keyring provided via `--keyring <path>`.

`--keyring```[¶](https://docs.ceph.com/en/pacific/man/8/ceph-mon/ "Permalink to this definition")Specify a keyring for use with `--mkfs`.

`--no-config-file```[¶](https://docs.ceph.com/en/pacific/man/8/ceph-mon/ "Permalink to this definition")Signal that we don’t want to rely on a _ceph.conf_, either user provided or the default, to run the daemon. This will entail providing all necessary options to the daemon as arguments.

## Availability[¶](https://docs.ceph.com/en/pacific/man/8/ceph-mon/ "Permalink to this headline")

**ceph\-mon** is part of Ceph, a massively scalable, open\-source, distributed storage system. Please refer to the Ceph documentation at [http://ceph.com/docs](https://docs.ceph.com/en/pacific/man/8/ceph-mon/) for more information.

## See also[¶](https://docs.ceph.com/en/pacific/man/8/ceph-mon/ "Permalink to this headline")

[ceph](https://docs.ceph.com/en/pacific/man/8/ceph-mon/)\(8\),[ceph\-mds](https://docs.ceph.com/en/pacific/man/8/ceph-mon/)\(8\),[ceph\-osd](https://docs.ceph.com/en/pacific/man/8/ceph-mon/)\(8\)
