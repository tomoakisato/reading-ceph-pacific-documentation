# 152: Mount CephFS on Windows

**クリップソース:** [152: Mount CephFS on Windows — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/ceph-dokan/)

# Mount CephFS on Windows[¶](https://docs.ceph.com/en/pacific/cephfs/ceph-dokan/ "Permalink to this headline")

ceph\-dokanは、Windows上でCephFSファイルシステムをマウントするために使用できます。DokanyというWindowsドライバを利用しており、FUSEのようにユーザースペースでファイルシステムを実装することが可能です。

[インストールガイド](https://docs.ceph.com/en/pacific/install/windows-install)をご確認の上、ご利用ください。

## Usage[¶](https://docs.ceph.com/en/pacific/cephfs/ceph-dokan/ "Permalink to this headline")

### Mounting filesystems[¶](https://docs.ceph.com/en/pacific/cephfs/ceph-dokan/ "Permalink to this headline")

cephファイルシステムをマウントするためには、以下のコマンドを使用することができます：

```
ceph-dokan.exe -c c:ceph.conf -l x
```

これにより、ドライブ文字xを使用してデフォルトのcephファイルシステムがマウントされます。 ceph.confがデフォルトの場所である％ProgramData％cephceph.confに配置されている場合、この引数はオプションになります。

また、\-l引数は、ドライブレターの代わりに空のフォルダーをマウントポイントとして使用することができます。

ファイルシステムのマウントに使用されるuidとgidのデフォルトは0であり、以下のceph.confオプションを使用して変更することができます：

```
[client]
# client_permissions = true
client_mount_uid = 1000
client_mount_gid = 1000
```

Cephクラスタに複数のFSがある場合、オプションの\-\-client\_fsを使用してデフォルト以外のFSをマウントします。

```
mkdir -Force C:mntmycephfs2
ceph-dokan.exe --mountpoint C:mntmycephfs2 --client_fs mycephfs2
```

CephFSサブディレクトリは、\-\-root\-pathパラメータを使用してマウントすることができます：

```
ceph-dokan -l y --root-path /a
```

\-o \-\-removableフラグが設定されている場合、マウントはGet\-Volumeの結果に表示されます：

```
PS C:> Get-Volume -FriendlyName "Ceph*" | `
        Select-Object -Property @("DriveLetter", "Filesystem", "FilesystemLabel")

DriveLetter Filesystem FilesystemLabel
----------- ---------- ---------------
          Z Ceph       Ceph
          W Ceph       Ceph - new_fs
```

引数の一覧はceph\-dokan \-\-helpをご利用ください。

### Credentials[¶](https://docs.ceph.com/en/pacific/cephfs/ceph-dokan/ "Permalink to this headline")

\-\-idオプションは、CephFSのマウントに使用する予定のキーリングを持つCephXのユーザ名を渡します。以下のコマンドは同等です：

```
ceph-dokan --id foo -l x
ceph-dokan --name client.foo -l x
```

### Unmounting filesystems[¶](https://docs.ceph.com/en/pacific/cephfs/ceph-dokan/ "Permalink to this headline")

マウントの解除は、ctrl\-cを発行するか、以下のようにunmapコマンドを使用することで可能です：

```
ceph-dokan.exe unmap -l x
```

Cephファイルシステムをアンマップする場合、マッピングを作成したときとまったく同じマウントポイント引数を使用する必要があることに注意してください。

### Limitations[¶](https://docs.ceph.com/en/pacific/cephfs/ceph-dokan/ "Permalink to this headline")

WindowsのACLは無視されますのでご注意ください。Posix ACL はサポートされていますが、現在の CLI では変更することができません。将来的には、ファイルの所有権やパーミッションを変更するためのコマンドアクションを追加する可能性があります。

もう一つ注意すべき点は、Windowsが大きく依存しているmandatoryファイルロックをcephfsがサポートしていないことです。現時点では、Dokanにファイルロックの処理を任せており、ローカルにのみ強制されます。

rbd\-wnbdと異なり、ceph\-dokanは現在サービスコマンドを提供していません。cephfsマウントがホストの再起動に耐えられるようにするために、NSSMの使用を検討してください。

## Troubleshooting[¶](https://docs.ceph.com/en/pacific/cephfs/ceph-dokan/ "Permalink to this headline")

[Windowsのトラブルシューティング](https://docs.ceph.com/en/pacific/install/windows-troubleshooting)ページをご参照ください。
