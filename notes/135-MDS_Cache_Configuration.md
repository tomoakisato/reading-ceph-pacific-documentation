# 135: MDS Cache Configuration

**クリップソース:** [135: MDS Cache Configuration — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/cache-configuration/)

# MDS Cache Configuration[¶](https://docs.ceph.com/en/pacific/cephfs/cache-configuration/ "Permalink to this headline")

メタデータサーバは、すべてのMDSおよびCephFSクライアントの間で分散キャッシュを調整します。キャッシュは、メタデータアクセスのレイテンシを改善し、クライアントが安全に（コヒーレントに）メタデータのstateを（chmodなどで）変更できるようにします。MDSは、クライアントがキャッシュできるstateと、クライアントが実行できる操作\(ファイルへの書き込みなど\)を示すために、CAPSとディレクトリエントリリースを発行します。

MDSとクライアントはどちらも、キャッシュサイズを強制しようとします。 MDSキャッシュサイズを指定するメカニズムを以下に説明します。 MDSキャッシュサイズはハード制限ではないことに注意してください。 MDSを使用すると、クライアントは常に、キャッシュにロードされている新しいメタデータを検索できます。 これは、クライアント要求のデッドロック（一部の要求は、CAPSを解放する前に保持しているCAPSに依存する場合があります）を回避するために不可欠なポリシーです。

MDSキャッシュが大きすぎると、MDSはクライアントのstateをリコールするため、キャッシュアイテムは固定解除され、ドロップできるようになります。 MDSは、ドロップするメタデータを参照するクライアントがない場合にのみ、キャッシュstateをドロップできます。 また、ワークロードのニーズに合わせてMDSリコール設定を構成する方法についても以下で説明します。 これは、MDSリコールの内部スロットルがクライアントのワークロードに追いつかない場合に必要です。

## MDS Cache Size[¶](https://docs.ceph.com/en/pacific/cephfs/cache-configuration/ "Permalink to this headline")

メタデータサーバー（MDS）のキャッシュのサイズをバイト数で制限することができます。これはmds\_cache\_memory\_limit設定によって行われます。例：

```
ceph config set mds mds_cache_memory_limit 8GB
```

また、MDS操作のmds\_cache\_reservationパラメータを使用することで、キャッシュの予約を指定することができます。キャッシュ予約はメモリに対するパーセンテージで制限され、デフォルトでは5%に設定されています。このパラメータの意図は、新しいメタデータ操作が使用するキャッシュのために、MDSが余分なメモリを確保することです。その結果、MDSは一般的にメモリ制限を下回って動作するはずです。これは、キャッ シュ内の未使用のメタデータをドロップするために、クライアントから古いstateをリコールするからです。

キャッシュを目標サイズ以下に維持できない場合、MDSはキャッシュが大きすぎることを示すヘルスアラートをモニ タに送信します。これはmds\_health\_cache\_threshold設定によって制御され、デフォルトでは最大キャッシュサイズの150%に設定されています。

キャッシュの上限はハードリミットではないため、CephFSクライアント、MDS、動作不良のアプリケーションに潜在するバグによって、MDSがキャッシュサイズを超過する可能性があります。ヘルス警告は、オペレータがこのような状況を検知し、必要な調整を行ったり、バグのあるクライアントを調査したりするためのものです。

## MDS Cache Trimming[¶](https://docs.ceph.com/en/pacific/cephfs/cache-configuration/ "Permalink to this headline")

MDSのキャッシュトリミングの速度を調整するために、2つの設定があります：

```
mds_cache_trim_threshold (default 64k)
```

and

```
mds_cache_trim_decay_rate (default 1)
```

スロットルの意図は、データシートがキャッシュのトランケートに多くの時間を費やすことを防ぐことです。このため、クライアントからのリクエスト処理やその他維持管理の能力が制限される可能性があります。

トリミング設定は、内部の減衰カウンタを制御します。キャッシュからメタデータがトリムされるたびに、カウンタがインクリメントされます。 thresholdはカウンタの最大サイズを設定し、rateはカウンタの指数関数的な半減期を示します。MDSが継続的にキャッシュからアイテムを削除している場合、1秒間に \-ln\(0.5\)/rate\*threshold 個のアイテムが削除される定常状態に達します。

注：構成設定mds\_cache\_trim\_decay\_rateの値を上げると、MDSがキャッシュをトリミングする時間が短くなります。キャッシュトリミングレートを上げるには、低い値を設定してください。

デフォルトは保守的な設定であり、キャッシュサイズが大きいMDSでは変更が必要な場合があります。

## MDS Recall[¶](https://docs.ceph.com/en/pacific/cephfs/cache-configuration/ "Permalink to this headline")

MDSは、クライアントからのリリースメッセージの処理が過負荷にならないよう、クライアントのstate（CAPS/リース）のリコールを制限しています。これは以下の設定により制御されます：

リコールイベントにおいて、1つのクライアントからリコールするCAPSの最大数：

```
mds_recall_max_caps (default: 5000)
```

セッションの減衰カウンターの閾値と減衰率：

```
mds_recall_max_decay_threshold (default: 16k)
```

and:

```
mds_recall_max_decay_rate (default: 2.5 seconds)
```

セッション減衰カウンタは、個々のセッションのリコール率を制御します。このカウンタの動作は、上記のキャッシュトリミングと同じように動作します。リコールされる各CAPSは、カウンタを増加させます。

また、すべてのセッションのリコールを調整するグローバルな減衰カウンタがあります：

```
mds_recall_global_max_decay_threshold (default: 64k)
```

その減衰率はmds\_recall\_max\_decay\_rateと同じです。どのセッションでもリコールされたCAPSは、このカウンタを増加させます。

クライアントがstateを解放するのが遅い場合、"failing to respond to cache pressure "またはMDS\_HEALTH\_CLIENT\_RECALLの警告が報告されます。各セッションの解放速度は、以下で設定された別の減衰カウンタによって監視されます：

```
mds_recall_warning_threshold (default: 32k)
```

and:

```
mds_recall_warning_decay_rate (default: 60.0 seconds)
```

CAPSが解放されるたびに、カウンタはインクリメントされます。 クライアントが十分に速くCAPSを解放せず、キャッシュ圧力がある場合、カウンタは、クライアントがstateを解放するのが遅いかどうかを示すことになります。

ワークロードやクライアントの動作によっては、CAPSの獲得に追いつくために、クライアントのstateをより速くリコールする必要がある場合があります。クラスタ健全状態における「遅いリコール警告」を解決するために、必要に応じて上記のカウンターを増加させることをお勧めします。

## MDS Cap Acquisition Throttle[¶](https://docs.ceph.com/en/pacific/cephfs/cache-configuration/ "Permalink to this headline")

大きなディレクトリ階層で些細な "find "コマンドを実行すると、クライアントは解放するよりもかなり速くCAPSを受け取ることになります。MDSはクライアントの容量をmds\_max\_caps\_per\_clientの制限値以下に抑えるようにしますが、リコールのスロットルにより取得ペースに追いつくことができません。そこで、readdirは以下のような構成でCAPS取得を制御するスロットルをかけています。

readdir CAPS取得減衰カウンタの閾値と減衰率：

```
mds_session_cap_acquisition_throttle (default: 500K)
```

and:

```
mds_session_cap_acquisition_decay_rate (default: 10 seconds)
```

CAPS獲得減衰カウンタは、readdir経由のCAPS獲得率を制御します。減衰カウンタの動作は、キャッシュトリミングやCAPSリコールの場合と同じです。readdirを呼び出すたびに、結果内のファイル数だけカウンタが増加します。

クライアントが mds\_max\_maps\_per\_client を超えると、CAPS獲得スロットルによって readdir がスロットルされる可能性があります。

```
mds_session_max_caps_throttle_ratio (default: 1.1)
```

CAPS獲得スロットルのためにクライアント要求が再試行されるまでのタイムアウト（秒）：

```
mds_cap_acquisition_throttle_retry_request_timeout (default: 0.5 seconds)
```

クライアントが1セッションで獲得したCAPS数がmds\_session\_max\_caps\_throttle\_ratioより大きく、CAPS獲得減衰カウンタがmds\_session\_cap\_acquisition\_throttleより大きい場合、readdirはスロットルされます。mds\_cap\_acquisition\_throttle\_retry\_request\_timeout秒後にreaddirの要求を再試行します。

## Session Liveness[¶](https://docs.ceph.com/en/pacific/cephfs/cache-configuration/ "Permalink to this headline")

また、MDSはセッションが静止しているかどうかも記録しています。クライアントセッションがそのCAPSを利用していない、または静止している場合、MDSはキャッシュが圧迫されていなくてもセッションからstateをリコールし始めます。これは、クラスタのワークロードが高温になり、キャッシュが圧迫されてMDSがstateをリコールせざるを得なくなったときに、MDSが将来の作業を回避するのに役立ちます。これは、クライアントがそのCAPSを利用していない場合、近い将来にそのCAPSを利用する可能性は低いと予想されるからです。

特定のセッションが静止しているかどうかの判断は、以下の設定変数によって制御されます：

```
mds_session_cache_liveness_magnitude (default: 10)
```

and:

```
mds_session_cache_liveness_decay_rate (default: 5min)
```

mds\_session\_cache\_liveness\_decay\_rate 設定は、クライアントによるCAPSの使用を追跡する減衰カウンタの半減期を示します。クライアントがCAPSを操作または取得するたびに、MDSはカウンタをインクリメントします。これは、クライアントキャッシュの使用状況を監視するための大まかな方法ですが、効果的な方法です。

mds\_session\_cache\_liveness\_magnitudeは、活性減衰カウンタとセッションのCAPS数の2を底とするの大きさの差である。したがって、クライアントが1\*2^20（1M）個のCAPSを持ち、1\*2^\(20\-mds\_session\_cache\_liveness\_magnitude\)（デフォルトでは1K）未満しか使用していない場合、MDSはクライアントを静止と見なし、リコールを開始します。

## Capability Limit[¶](https://docs.ceph.com/en/pacific/cephfs/cache-configuration/ "Permalink to this headline")

また、MDSは1つのクライアントが過剰に多くのCAPSを獲得することを防ごうとしています。これは、状況によってはリカバリに時間がかかることを防ぐのに役立ちます。 一般的に、クライアントがこれほど大きなキャッシュを持つ必要はありません。この制限は、以下の方法で設定することができます：

```
mds_max_caps_per_client (default: 1M)
```

この値を5M以上に設定することは推奨されませんが、ワークロードによっては有用な場合があります。
