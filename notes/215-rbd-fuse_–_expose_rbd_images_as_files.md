# 215: rbd-fuse – expose rbd images as files

 # rbd\-fuse – expose rbd images as files[¶](https://docs.ceph.com/en/pacific/man/8/rbd-fuse/ "Permalink to this headline")

## Synopsis[¶](https://docs.ceph.com/en/pacific/man/8/rbd-fuse/ "Permalink to this headline")

**rbd\-fuse** \[ \-p pool \] \[\-c conffile\] _mountpoint_ \[ _fuse options_ \]

## Note[¶](https://docs.ceph.com/en/pacific/man/8/rbd-fuse/ "Permalink to this headline")

**rbd\-fuse** is not recommended for any production or high performance workloads.

## Description[¶](https://docs.ceph.com/en/pacific/man/8/rbd-fuse/ "Permalink to this headline")

**rbd\-fuse** is a FUSE \(“Filesystem in USErspace”\) client for RADOS block device \(rbd\) images. Given a pool containing rbd images, it will mount a userspace file system allowing access to those images as regular files at **mountpoint**.

The file system can be unmounted with:

```
fusermount -u mountpoint
```

or by sending `SIGINT` to the `rbd-fuse` process.

## Options[¶](https://docs.ceph.com/en/pacific/man/8/rbd-fuse/ "Permalink to this headline")

Any options not recognized by rbd\-fuse will be passed on to libfuse.

`-c`` ceph.conf`[¶](https://docs.ceph.com/en/pacific/man/8/rbd-fuse/ "Permalink to this definition")Use _ceph.conf_ configuration file instead of the default`/etc/ceph/ceph.conf` to determine monitor addresses during startup.

`-p`` pool`[¶](https://docs.ceph.com/en/pacific/man/8/rbd-fuse/ "Permalink to this definition")Use _pool_ as the pool to search for rbd images. Default is `rbd`.

## Availability[¶](https://docs.ceph.com/en/pacific/man/8/rbd-fuse/ "Permalink to this headline")

**rbd\-fuse** is part of Ceph, a massively scalable, open\-source, distributed storage system. Please refer to the Ceph documentation at [http://ceph.com/docs](https://docs.ceph.com/en/pacific/man/8/rbd-fuse/) for more information.

## See also[¶](https://docs.ceph.com/en/pacific/man/8/rbd-fuse/ "Permalink to this headline")

fusermount\(8\),[rbd](https://docs.ceph.com/en/pacific/man/8/rbd-fuse/)\(8\)
