# 35: Cephadm Operations

**クリップソース:** [35: Cephadm Operations — Ceph Documentation](https://docs.ceph.com/en/pacific/cephadm/operations/)

# Cephadm Operations[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

## Watching cephadm log messages[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

Cephadmはログをcephadmクラスタログチャネルに書き込みます。満杯になったログを読むことで、Cephのアクティビティをリアルタイムで監視することができます。次のコマンドを実行すると、ログがリアルタイムで表示されます。

```
# ceph -W cephadm
```

デフォルトでは、このコマンドはinfoレベルのイベント以上を表示します。 infoレベルのイベントだけでなく、デバッグレベルのメッセージも表示するには、以下のコマンドを実行してください。

```
# ceph config set mgr mgr/cephadm/log_to_cluster_level debug
# ceph -W cephadm --watch-debug
```

警告：デバッグメッセージは非常に冗長です。

以下のコマンドを実行すると、最近のイベントを見ることができます。

```
# ceph log last cephadm
```

これらのイベントは、モニターホストのceph.cephadm.logファイルにも記録され、モニターデーモンの標準エラーにも記録されます。

## Ceph daemon logs[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

### Logging to journald[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

Cephのデーモンは、伝統的にログを/var/log/cephに書き込みます。Cephデーモンはデフォルトでjournaldにログを記録し、Cephのログはコンテナのランタイム環境で取得されます。これらはjournalctlでアクセスできます。

注:Quincyより前は、Cephデーモンはstderrにログを記録していました。

#### Example of logging to journald[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

例えば、IDが5c5a50ae\-272a\-455d\-99e9\-32c6a013e694のクラスタのデーモンmon.fooのログを表示するには、以下のようなコマンドを実行します。

```
# journalctl -u ceph-5c5a50ae-272a-455d-99e9-32c6a013e694@mon.foo
```

これは、ログレベルが低い場合の通常の操作ではうまく機能します。

### Logging to files[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

また、ログをファイルに表示したい場合は、Cephのデーモンをjournaldではなくファイルにログを記録するように設定することもできます\(Cephのcephadm以前、Octopus以前の初期のバージョンではそうなっていました\)。 Cephがファイルにログを記録すると、ログは/var/log/ceph/\<cluster\-fsid\>に表示されます。Cephがjournaldではなくファイルにログを記録するように設定する場合は、Cephがjournaldにログを記録しないように設定してください\(このためのコマンドは以下で説明します\)。

#### Enabling logging to files[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

ファイルへのロギングを有効にするには、以下のコマンドを実行します。

```
# ceph config set global log_to_file true
# ceph config set global mon_cluster_log_to_file true
```

#### Disabling logging to journald[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

ファイルに記録する場合は、journaldへの記録を無効にすることをお勧めします。そうしないと、すべてが2回記録されてしまいます。以下のコマンドを実行して、stderrへのログを無効にします。

```
# ceph config set global log_to_stderr false
# ceph config set global mon_cluster_log_to_stderr false
# ceph config set global log_to_journald false
# ceph config set global mon_cluster_log_to_journald false
```

注：新しいクラスタの起動時に\-log\-to\-fileを指定することで、デフォルトを変更することができます。

#### Modifying the log retention schedule[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

デフォルトでは、cephadmは各ホストでログローテーションを設定し、これらのファイルを回転させます。 etc/logrotate.d/ceph.\<cluster\-fsid\>を変更することで、ログの保持スケジュールを設定できます。

## Data location[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

Cephadmは、デーモンのデータとログを、cephadm以前\(Octopus以前\)の古いバージョンのcephとは異なる場所に保存します。

* /var/log/ceph/\<cluster\-fsid\>にはすべてのクラスタログが含まれます。デフォルトでは、cephadmは標準エラーとコンテナランタイムを介してログを記録します。これらのログは、cephadm\-logsで説明されているようにファイルへのロギングを有効にしない限り存在しません。
* /var/lib/ceph/\<cluster\-fsid\>には、（ログ以外の）すべてのクラスタデーモンのデータが格納されています。
* /var/lib/ceph/\<cluster\-fsid\>/\<daemon\-name\>には、個々のデーモンの全データが格納されます。
* /var/lib/ceph/\<cluster\-fsid\>/crashには、クラスタのクラッシュレポートが含まれています。
* /var/lib/ceph/\<cluster\-fsid\>/removedには、cephadmによって削除されたステートフルデーモン\(monitor、prometheusなど\)の古いデーモンデータディレクトリが含まれます。

### Disk usage[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

いくつかのCephデーモン\(特にmonitorとprometheus\)は大量のデータを/var/lib/cephに保存するため、このディレクトリを独自のディスク、パーティション、または論理ボリュームに移動して、ルートファイルシステムがいっぱいにならないようにすることをお勧めします。

## Health checks[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

cephadmモジュールは、クラスタが提供するデフォルトのヘルスチェックを補完する追加のヘルスチェックを提供します。これらの追加ヘルスチェックは2つのカテゴリに分類されます。

* **cephadm operations**
* **cluster configuration**

### CEPHADM Operations[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

#### CEPHADM\_PAUSED[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

これは、cefadmのバックグラウンド作業が_ceph orch pause_で一時停止されたことを示しています。 Cephadmは、パッシブな監視活動\(ホストやデーモンの状態の確認など\)は続けますが、変更\(デーモンの展開や削除など\)は行いません。

次のコマンドを実行して、cephadmの作業を再開します。

```
# ceph orch resume
```

#### CEPHADM\_STRAY\_HOST[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

これは、1つまたは複数のホストでCeph daemonが実行されているが、cephadmが管理するホストとして登録されていないことを示しています。 これは、それらのサービスが現在cephadmによって管理\(たとえば、再起動、アップグレード、ceph orch psに含めることが\)できないことを意味します。

* 以下のコマンドを実行することで、ホストを管理することができます。

```
# ceph orch host add *<hostname>*
```

注：この操作を行う前に、リモートホストへのSSHアクセスを設定する必要があるかもしれません。

* ホスト名とドメイン名の詳細については、「
* または、手動でホストに接続して、そのホスト上のサービスが削除されるようにしたり、cephadmで管理されているホストに移行したりすることもできます。
* この警告は、以下のコマンドを実行することで完全に無効にすることができます。

```
# ceph config set mgr mgr/cephadm/warn_on_stray_hosts false
```

#### CEPHADM\_STRAY\_DAEMON[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

1つまたは複数のCeph daemonが実行されていますが、cephadmによって管理されていません。 これは、別のツールを使用して展開されているか、手動で起動されていることが原因です。 これらのサービスは現在、cephadmで管理\(再起動、アップグレード、またはceph orch psに含めることが\)できません。

* デーモンがステートフルなもの\(monitorまたはOSD\)の場合、cephadmで採用する必要があります。「既存のクラスタをcephadmに変換する」を参照してください。 ステートレスデーモンの場合は、通常、
* cephadmで管理されていないホスト上でstrayデーモンが動作している場合は、以下のコマンドを実行してホストを管理できます。

```
# ceph orch host add *<hostname>*
```

注：この機能を使用する前に、リモートホストへのSSHアクセスを設定する必要がある場合があります。

* ホスト名とドメイン名の詳細については、「
* この警告は、以下のコマンドを実行することで完全に無効にすることができます。

```
# ceph config set mgr mgr/cephadm/warn_on_stray_daemons false
```

#### CEPHADM\_HOST\_CHECK\_FAILED[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

1つ以上のホストがcephadmの基本的なホストチェックに失敗しました。このテストでは、\(1\)ホストに到達可能であり、cephadmを実行できること、\(2\)コンテナランタイム\(podmanまたはdocker\)の動作や時刻同期の動作など、ホストが基本的な前提条件を満たしていることを確認します。このテストが失敗すると、cephadmはそのホスト上のサービスを管理できなくなります。

このチェックを手動で行うには、次のコマンドを実行します。

```
# ceph cephadm check-host *<hostname>*
```

以下のコマンドを実行することで、壊れたホストを管理対象から外すことができます。

```
# ceph orch host rm *<hostname>*
```

以下のコマンドを実行することで、この健康状態に関する警告を無効にすることができます。

```
# ceph config set mgr mgr/cephadm/warn_on_failed_host_check false
```

### Cluster Configuration Checks[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

Cephadmはクラスタ内の各ホストを定期的にスキャンして、OS、ディスク、NICなどの状態を把握します。これらの事実は、クラスタ内のホスト間の一貫性を分析し、設定の異常を特定します。

#### Enabling Cluster Configuration Checks[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

コンフィギュレーションチェックはオプションの機能で、次のコマンドを実行することで有効になります。

```
# ceph config set mgr mgr/cephadm/config_checks_enabled true
```

#### States Returned by Cluster Configuration Checks[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

設定チェックは、各ホストスキャン\(1m\)の後に実行されます。cephadmのログエントリには、設定チェックの現在の状態と結果が次のように表示されます。

Disabledな状態\(config\_checks\_enabled false\)：

```
ALL cephadm checks are disabled, use 'ceph config set mgr mgr/cephadm/config_checks_enabled true' to enable
```

Enabled な状態 \(config\_checks\_enabled true\):

```
CEPHADM 8/8 checks enabled and executed (0 bypassed, 0 disabled). No issues detected
```

#### Managing Configuration Checks \(subcommands\)[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

設定チェック自体は、いくつかのcephadmサブコマンドで管理されます。

設定チェックが有効になっているかどうかを確認するには、次のコマンドを実行します：

```
# ceph cephadm config-check status
```

このコマンドは、コンフィグレーション・チェッカーの状態を「Enabled」または「Disabled」のいずれかで返します。

すべてのコンフィグレーションチェックとその現在の状態を一覧表示するには、次のコマンドを実行します：

```
# ceph cephadm config-check ls

  NAME             HEALTHCHECK                      STATUS   DESCRIPTION
kernel_security  CEPHADM_CHECK_KERNEL_LSM         enabled  checks SELINUX/Apparmor profiles are consistent across cluster hosts
os_subscription  CEPHADM_CHECK_SUBSCRIPTION       enabled  checks subscription states are consistent for all cluster hosts
public_network   CEPHADM_CHECK_PUBLIC_MEMBERSHIP  enabled  check that all hosts have a NIC on the Ceph public_netork
osd_mtu_size     CEPHADM_CHECK_MTU                enabled  check that OSD hosts share a common MTU setting
osd_linkspeed    CEPHADM_CHECK_LINKSPEED          enabled  check that OSD hosts share a common linkspeed
network_missing  CEPHADM_CHECK_NETWORK_MISSING    enabled  checks that the cluster/public networks defined exist on the Ceph hosts
ceph_release     CEPHADM_CHECK_CEPH_RELEASE       enabled  check for Ceph version consistency - ceph daemons should be on the same release (unless upgrade is active)
kernel_version   CEPHADM_CHECK_KERNEL_VERSION     enabled  checks that the MAJ.MIN of the kernel on Ceph hosts is consistent

```

各コンフィギュレーションチェックの名前は、次のような形式のコマンドを実行することで、特定のチェックを有効または無効にすることができます：

```
# ceph cephadm config-check disable <name>
```

For example:

```
# ceph cephadm config-check disable kernel_security
```

#### CEPHADM\_CHECK\_KERNEL\_LSM[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

クラスタ内の各ホストは、同じLinuxセキュリティ・モジュール（LSM）の状態で動作することが期待されます。例えば、大多数のホストがSELINUXをenforcingモードで動作させている場合、このモードで動作していないホストは異常としてフラグが立てられ、healtcheck（WARNING）の状態が発生します。

#### CEPHADM\_CHECK\_SUBSCRIPTION[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

このチェックは、ベンダーのサブスクリプションの状態に関するものです。このチェックは、RHEL を使用しているホストに対してのみ実行されますが、すべてのホストに有効なサブスクリプションが適用されていることを確認するのに役立ち、パッチやアップデートが利用可能であることが保証されます。

#### CEPHADM\_CHECK\_PUBLIC\_MEMBERSHIP[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

クラスタのすべてのメンバーは、パブリックネットワークのサブネットの少なくとも1つにNICを設定する必要があります。パブリックネットワーク上にないホストはルーティングに頼ることになり、パフォーマンスに影響を与える可能性があります。

#### CEPHADM\_CHECK\_MTU[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

OSDのNICのMTUは、安定したパフォーマンスを得るための重要な要因となります。この検査では、OSDサービスを実行しているホストを調べて、クラスタ内でMTUが一貫して設定されていることを確認します。これは、大多数のホストが使用しているMTU設定を確認することで判断します。異常があると、Cephのヘルスチェックが行われます。

#### CEPHADM\_CHECK\_LINKSPEED[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

このチェックは、MTUのチェックと同様です。リンク速度の一貫性は、OSD上のNICのMTUと同様に、クラスタの安定したパフォーマンスの要因となります。このチェックでは、大多数のOSDホストが共有しているリンク速度を決定し、より低いリンク速度に設定されているホストに対して健全性チェックを行います。

#### CEPHADM\_CHECK\_NETWORK\_MISSING[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

public\_networkおよびcluster\_networkの設定は、IPv4およびIPv6のサブネット定義をサポートしています。これらの設定がクラスタ内のどのホストにも見当たらない場合は、ヘルスチェックが行われます。

#### CEPHADM\_CHECK\_CEPH\_RELEASE[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

通常の運用では、Cephクラスタは同じcephリリースの下でデーモンを実行します\(つまり、Cephクラスタはすべてのデーモンを\(たとえば\)Octopusの下で実行します\)。 このチェックでは、各デーモンのアクティブなリリースを判断し、異常があればヘルスチェックとして報告します。クラスタ内でアップグレードプロセスがアクティブな場合、このチェックはバイパスされます。

#### CEPHADM\_CHECK\_KERNEL\_VERSION[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

OS のカーネルバージョン（maj.min）がホスト間で一致しているかどうかを確認します。大多数のホストのカーネルバージョンを基準にして、異常を特定します。

## Client keyrings and configs[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

Cephadmは、ceph.confファイルとclient keyringファイルのコピーをホストに配布できます。通常、CLIを使用してクラスタを管理するために使用するホストに、configおよびclient.adminキーリングのコピーを保存することをお勧めします。 デフォルトでは、cephadmは\_adminラベルを持つすべてのノード\(通常はブートストラップホストを含む\)に対してこれを行います。

クライアントのキーリングが管理下に置かれると、cephadmは：

* 指定された配置仕様に基づいてターゲットホストのリストを作成する（「
* 指定したホストに/etc/ceph/ceph.confファイルのコピーを保存する
* 指定したホストにキーリングファイルのコピーを保存する
* \(クラスタモニタの変更などにより\)必要に応じてceph.confファイルを更新する
* \(ceph auth...コマンドなどにより\)エンティティのキーが変更された場合、キーリングファイルを更新する
* キーリングファイルの所有権とモードが指定されていることを確認する
* クライアントのキーリング管理が無効な場合に、キーリングファイルを削除する
* キーリング配置の仕様が更新された場合、（必要に応じて）古いホストからキーリングファイルを削除する

### Listing Client Keyrings[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

現在管理されているクライアントキーリングの一覧を見るには、次のコマンドを実行してください。

```
# ceph orch client-keyring ls
```

### Putting a Keyring Under Management[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

キーリングを管理下に置くには、次のような形式のコマンドを実行します。

```
# ceph orch client-keyring set <entity> <placement> [--mode=<mode>] [--owner=<uid>.<gid>] [--path=<path>]
```

* デフォルトでは、パスは/etc/ceph/client.{entity}.keyringで、これはCephがデフォルトで見る場所です。 既存のファイルが上書きされる可能性があるため、別の場所を指定する場合は注意してください
* \*（すべてのホスト）の配置が一般的です
* モードのデフォルトは「0600」、オーナーシップは「0:0」（ユーザーroot、グループroot）となっています

たとえば、client.rbdキーを作成してrbd\-clientラベルのついたホストに展開し、uid/gid 107（qemu）でグループ読取可能にするには、次のコマンドを実行します。

```
# ceph auth get-or-create-key client.rbd mon 'profile rbd' mgr 'profile rbd' osd 'profile rbd pool=my_rbd_pool'
# ceph orch client-keyring set client.rbd label:rbd-client --owner 107:107 --mode 640
```

出来上がったキーリングファイル：

```
-rw-r-----. 1 qemu qemu 156 Apr 21 08:47 /etc/ceph/client.client.rbd.keyring
```

### Disabling Management of a Keyring File[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

キーリングファイルの管理を無効にするには、次のような形式のコマンドを実行します。

```
# ceph orch client-keyring rm <entity>
```

注：クラスタノードに書き込まれていたこのエンティティのキーリングファイルが削除されます。

## /etc/ceph/ceph.conf[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

### Distributing ceph.conf to hosts that have no keyrings[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

ceph.confファイルを、関連するクライアントキーリングファイルのないホストに配布すると便利な場合があります。 デフォルトでは、cephadmは、クライアントキーリングも配布されているホストにceph.confファイルのみを展開します\(上記を参照\)。 クライアントキーリングのないホストに設定ファイルを書き込むには、次のコマンドを実行します。

```
# ceph config set mgr mgr/cephadm/manage_etc_ceph_ceph_conf true
```

### Using Placement Specs to specify which hosts get keyrings[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

デフォルトでは、すべてのホスト\(すなわち、_ceph orch host ls_でリストされたホスト\)に設定が書き込まれます。 ceph.confを取得するホストを指定するには、次のような形式のコマンドを実行します。

```
# ceph config set mgr mgr/cephadm/manage_etc_ceph_ceph_conf_hosts <placement spec>
```

例えば、bare\_configラベルのホストにコンフィグを配布するには、以下のコマンドを実行します。

### Distributing ceph.conf to hosts tagged with bare\_config[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

例えば、bare\_configラベルのホストにコンフィグを配布するには、以下のコマンドを実行します。

```
# ceph config set mgr mgr/cephadm/manage_etc_ceph_ceph_conf_hosts label:bare_config
```

\(配置仕様の詳細については、「[Daemon Placement](https://docs.ceph.com/en/pacific/cephadm/operations/) 」を参照\)。

## Purging a cluster[¶](https://docs.ceph.com/en/pacific/cephadm/operations/ "Permalink to this headline")

危険：この操作は、このクラスタに保存されているすべてのデータを破壊します。

クラスタを破棄して、このクラスタに保存されているすべてのデータを削除するには、新しいデーモンを展開しないようにcephadmを一時停止します。

```
# ceph orch pause
```

次に、クラスタのFSIDを確認します。

```
# ceph fsid
```

クラスタ内のすべてのホストからceph daemonをパージします。

```
# # For each host:
# cephadm rm-cluster --force --zap-osds --fsid <fsid>
```
