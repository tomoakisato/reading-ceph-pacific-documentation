# 317: Contributing to Ceph: A Guide for Developers

**クリップソース:** [317: Contributing to Ceph: A Guide for Developers — Ceph Documentation](https://docs.ceph.com/en/pacific/dev/developer_guide/)

# Contributing to Ceph: A Guide for Developers[¶](https://docs.ceph.com/en/pacific/dev/developer_guide/ "Permalink to this headline")

Author: Loic Dachary

Author: Nathan Cutler

License: Creative Commons Attribution Share Alike 3.0 \(CC\-BY\-SA\-3.0\)

Note:You may also be interested in the [Ceph Internals](https://docs.ceph.com/en/pacific/dev/developer_guide/) documentation.

* Introduction
* Essentials
* What is Merged and When
* Issue tracker
* Basic workflow
* Tests: Unit Tests
* Tests: Integration Tests
* Running Tests Locally
* Running Integration Tests using Teuthology
* Running Tests in the Cloud
* Ceph Dashboard Developer Documentation \(formerly HACKING.rst\)
* cephadm Developer Documentation
