# 31: iSCSI Service

**クリップソース:** [31: iSCSI Service — Ceph Documentation](https://docs.ceph.com/en/pacific/cephadm/services/iscsi/)

# iSCSI Service[¶](https://docs.ceph.com/en/pacific/cephadm/services/iscsi/ "Permalink to this headline")

## Deploying iSCSI[¶](https://docs.ceph.com/en/pacific/cephadm/services/iscsi/ "Permalink to this headline")

iSCSI ゲートウェイを展開するには、iscsi のサービス仕様を含む yaml ファイルを作成します。

```
service_type: iscsi
service_id: iscsi
placement:
  hosts:
    - host1
    - host2
spec:
  pool: mypool  # RADOS pool where ceph-iscsi config data is stored.
  trusted_ip_list: "IP_ADDRESS_1,IP_ADDRESS_2"
  api_port: ... # optional
  api_user: ... # optional
  api_password: ... # optional
  api_secure: true/false # optional
  ssl_cert: | # optional
    ...
  ssl_key: | # optional
    ...
```

For example:

```
service_type: iscsi
service_id: iscsi
placement:
  hosts:
  - [...]
spec:
  pool: iscsi_pool
  trusted_ip_list: "IP_ADDRESS_1,IP_ADDRESS_2,IP_ADDRESS_3,..."
  api_user: API_USERNAME
  api_password: API_PASSWORD
  ssl_cert: |
    -----BEGIN CERTIFICATE-----
    MIIDtTCCAp2gAwIBAgIYMC4xNzc1NDQxNjEzMzc2MjMyXzxvQ7EcMA0GCSqGSIb3
    DQEBCwUAMG0xCzAJBgNVBAYTAlVTMQ0wCwYDVQQIDARVdGFoMRcwFQYDVQQHDA5T
    [...]
    -----END CERTIFICATE-----
  ssl_key: |
    -----BEGIN PRIVATE KEY-----
    MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC5jdYbjtNTAKW4
    /CwQr/7wOiLGzVxChn3mmCIF3DwbL/qvTFTX2d8bDf6LjGwLYloXHscRfxszX/4h
    [...]
    -----END PRIVATE KEY-----
```

```
classceph.deployment.service_spec.IscsiServiceSpec(service_type='iscsi', service_id=None, pool=None, trusted_ip_list=None, api_port=None, api_user=None, api_password=None, api_secure=None, ssl_cert=None, ssl_key=None, placement=None, unmanaged=False, preview_only=False, config=None, networks=None)¶
```

```
api_password¶
```

iscsi\-gateway.cfgで定義されているapi\_password

```
api_port¶
```

iscsi\-gateway.cfgで定義されているapi\_port

```
api_secure¶
```

iscsi\-gateway.cfgで定義されているapi\_secure

```
api_user¶
```

iscsi\-gateway.cfgで定義されたapi\_user

```
pool¶
```

ceph\-iscsiの設定データが保存されるRADOSプール。

```
ssl_cert¶
```

SSL certificate

```
ssl_key¶
```

SSL private key

```
trusted_ip_list¶
```

信頼できるIPアドレスのリスト

この仕様は、次のようにして適用できます。

```
# ceph orch apply -i iscsi.yaml
```

 配置指定の詳細については、「[Daemon Placement](https://docs.ceph.com/en/pacific/cephadm/services/iscsi/)」を参照してください。

こちらもご覧ください。 [Service Specification](https://docs.ceph.com/en/pacific/cephadm/services/iscsi/).

## Further Reading[¶](https://docs.ceph.com/en/pacific/cephadm/services/iscsi/ "Permalink to this headline")

* RBD: 
