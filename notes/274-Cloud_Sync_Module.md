# 274: Cloud Sync Module

**クリップソース:** [274: Cloud Sync Module — Ceph Documentation](https://docs.ceph.com/en/pacific/radosgw/cloud-sync-module/)

# Cloud Sync Module[¶](https://docs.ceph.com/en/pacific/radosgw/cloud-sync-module/ "Permalink to this headline")

Mimic バージョンの新機能です。

このモジュールは、ゾーンデータをリモートクラウドサービスに同期させます。この同期は一方向であり、データはリモートゾーンからは同期されません。このモジュールの目的は、複数のクラウドプロバイダへのデータ同期を可能にすることです。現在サポートされているクラウドプロバイダは、AWS \(S3\) と互換性のあるプロバイダです。

リモートクラウドオブジェクトストアサービスのユーザ認証情報を設定する必要があります。多くのクラウドサービスでは、各ユーザが作成できるバケットの数に制限があるため、ソースオブジェクトとバケットのマッピングを設定することが可能です。異なるバケットおよびバケットプレフィックスに異なるターゲットを設定することが可能です。ただし、ソース ACL は保持されません。特定の送信元ユーザの権限を、特定の送信先ユーザにマッピングすることが可能です。

API の制限により、オリジナルのオブジェクトの修正時刻と ETag を保持する方法はありません。クラウド同期モジュールは、これらを宛先オブジェクトのメタデータ属性として保存します。

## Cloud Sync Tier Type Configuration[¶](https://docs.ceph.com/en/pacific/radosgw/cloud-sync-module/ "Permalink to this headline")

トリビアルコンフィギュレーション：

```
{
  "connection": {
    "access_key": <access>,
    "secret": <secret>,
    "endpoint": <endpoint>,
    "host_style": <path | virtual>,
  },
  "acls": [ { "type": <id | email | uri>,
              "source_id": <source_id>,
              "dest_id": <dest_id> } ... ],
  "target_path": <target_path>,
}
```

ノントリビアルコンフィギュレーション：

```
{
  "default": {
    "connection": {
        "access_key": <access>,
        "secret": <secret>,
        "endpoint": <endpoint>,
        "host_style" <path | virtual>,
    },
    "acls": [
    {
      "type" : <id | email | uri>,   #  optional, default is id
      "source_id": <id>,
      "dest_id": <id>
    } ... ]
    "target_path": <path> # optional
  },
  "connections": [
      {
        "connection_id": <id>,
        "access_key": <access>,
        "secret": <secret>,
        "endpoint": <endpoint>,
        "host_style" <path | virtual>,  # optional
      } ... ],
  "acl_profiles": [
      {
        "acls_id": <id>, # acl mappings
        "acls": [ {
            "type": <id | email | uri>,
            "source_id": <id>,
            "dest_id": <id>
          } ... ]
      }
  ],
  "profiles": [
      {
       "source_bucket": <source>,
       "connection_id": <connection_id>,
       "acls_id": <mappings_id>,
       "target_path": <dest>,          # optional
      } ... ],
}
```

注：トリビアルコンフィギュレーションはノントリビアルコンフィギュレーションと一致することがあります。

* connection \(container\)

リモートクラウドサービスへの接続を表す。conection\_id, access\_key, secret, endpoint, host\_style を含む

* access\_key \(string\)

リモートクラウドサービスのアクセスキー

* secret \(string\)

リモートクラウドサービスのシークレットキー

* endpoint \(string\)

リモートクラウドサービスのエンドポイントURL

* host\_style \(path | virtual\)

リモートクラウドエンドポイントにアクセスする際に使用するhost\_styleの種類 \(デフォルト: path\)

* acls \(array\)

acl\_mappings のリスト

* acl\_mapping \(container\)

各 acl\_mapping 構造体は、type、source\_id、dest\_id を含む。これらは、各オブジェクトに行われる ACL mutationを定義する。ACL mutationは，ソースユーザ ID をデスティネーション ID に変換することを可能にする。

* type \(id | email | uri\)

ACLタイプ：id（ユーザID）、email（emailによるユーザ定義）、uri（uri（グループ）によるユーザ定義）

* source\_id \(string\)

送信元ゾーンのユーザID

* dest\_id \(string\)

送信先のユーザID

* target\_path \(string\)

ターゲットパスの作成方法を定義する文字列。ターゲットパスは、ソースオブジェクト名を付加するプレフィックスを指定する。ターゲットパス configurable には次の変数（any）が含まれる \- sid: 同期インスタンスIDを表す一意の文字列 \- zonegroup: ゾーングループ名 \- zonegroup\_id: ゾーングループID \- zone: ゾーン名 \- zone\_id: ゾーンID \- bucket: ソースバケット名 \- owner: ソースバケットのオーナID

For example: target\_path=rgwx\-${zone}\-${sid}/${owner}/${bucket}

* acl\_profiles \(array\)

acl\_profile の配列

* acl\_profile \(container\)

各プロファイルは、プロファイルを表すacls\_id文字列と、acl\_mappingsのリストを保持するacls配列を含む

* profiles \(array\)

プロファイルの一覧。各プロファイルは次を含む \- source\_bucket: プロファイルのソースバケットを定義するバケット名、またはバケットプレフィックス（\*で終わる場合） \- target\_path: 上記で定義したとおり \- connection\_id: プロファイルで使用される接続ID \- acls\_id: プロファイルで使用される ACLs プロファイルID

### S3 Specific Configurables:[¶](https://docs.ceph.com/en/pacific/radosgw/cloud-sync-module/ "Permalink to this headline")

現在のところ、クラウド同期は AWS S3 と互換性のあるバックエンドでのみ動作します。クラウドサービスにアクセスする際の挙動を調整するために、いくつかの設定項目があります：

```
{
  "multipart_sync_threshold": {object_size},
  "multipart_min_part_size": {part_size}
}
```

* multipart\_sync\_threshold \(integer\)

このサイズ以上のオブジェクトは、マルチパートアップロードを使用してクラウドに同期されます。

* multipart\_min\_part\_size \(integer\)

マルチパートアップロードを使用してオブジェクトを同期させるときに使用する最小パーツサイズ。

### How to Configure[¶](https://docs.ceph.com/en/pacific/radosgw/cloud-sync-module/ "Permalink to this headline")

マルチサイトの設定方法は、「[マルチサイト](https://docs.ceph.com/en/pacific/radosgw/multisite/#multisite)」を参照してください。クラウド同期モジュールは、新しいゾーンの作成が必要です。ゾーンのtier\_typeは、cloudとして定義する必要があります：

```
# radosgw-admin zone create --rgw-zonegroup={zone-group-name} 
                            --rgw-zone={zone-name} 
                            --endpoints={http://fqdn}[,{http://fqdn}]
                            --tier-type=cloud
```

tier構成は、次のコマンドで行うことができます：

```
# radosgw-admin zone modify --rgw-zonegroup={zone-group-name} 
                            --rgw-zone={zone-name} 
                            --tier-config={key}={val}[,{key}={val}]
```

設定のkeyは更新が必要な設定変数を指定し、valはその新しい値を指定します。ネストされた値にはピリオドを使ってアクセスすることができます。例えば：

```
# radosgw-admin zone modify --rgw-zonegroup={zone-group-name} 
                            --rgw-zone={zone-name} 
                            --tier-config=connection.access_key={key},connection.secret={secret}
```

構成配列エントリには、参照する特定のエントリを角括弧で囲んで指定することでアクセスできます。新しい配列エントリを追加するには、\[\]を使用します。 インデックス値\-1は、配列の最後のエントリを参照します。 現時点では、新しいエントリを作成して、同じコマンドで参照することはできません。 たとえば、{prefix}で始まるバケットの新しいプロファイルを作成します：

```
# radosgw-admin zone modify --rgw-zonegroup={zone-group-name} 
                            --rgw-zone={zone-name} 
                            --tier-config=profiles[].source_bucket={prefix}'*'

# radosgw-admin zone modify --rgw-zonegroup={zone-group-name} 
                            --rgw-zone={zone-name} 
                            --tier-config=profiles[-1].connection_id={conn_id},profiles[-1].acls_id={acls_id}
```

\-\-tier\-config\-rm = {key}を使用して、エントリを削除できます。
