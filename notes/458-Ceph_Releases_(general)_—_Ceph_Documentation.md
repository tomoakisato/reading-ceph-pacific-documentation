# 458: Ceph Releases (general) — Ceph Documentation

 # Ceph Releases \(general\)[¶](https://docs.ceph.com/en/pacific/releases/general/ "Permalink to this headline")

## Active stable releases[¶](https://docs.ceph.com/en/pacific/releases/general/ "Permalink to this headline")

| Version | Initial release | Latest | End of life \(estimated\) |
|--------------------------------------------------------------|---------------|-------------------------------------------------------------|-------------------------|
|[octopus](https://docs.ceph.com/en/pacific/releases/general/) | Mar 2020 |[15.2.7](https://docs.ceph.com/en/pacific/releases/general/) |2022\-06\-01 |
|[nautilus](https://docs.ceph.com/en/pacific/releases/general/) | Mar 2019 | [14.2.14](https://docs.ceph.com/en/pacific/releases/general/) | 2021\-06\-01 |

## Understanding the release cycle[¶](https://docs.ceph.com/en/pacific/releases/general/ "Permalink to this headline")

Starting with the Nautilus release \(14.2.0\), there is a new stable release cycle every year, targeting March 1st. Each stable release series will receive a name \(e.g., ‘Mimic’\) and a major release number \(e.g., 13 for Mimic because ‘M’ is the 13th letter of the alphabet\).

Releases are named after a species of cephalopod \(usually the common name, since the latin names are harder to remember or pronounce\).

Version numbers have three components, _x.y.z_. _x_ identifies the release cycle \(e.g., 13 for Mimic\). _y_ identifies the release type:

* x.0.z \- development releases \(for early testers and the brave at heart\)
* x.1.z \- release candidates \(for test clusters, brave users\)
* x.2.z \- stable/bugfix releases \(for users\)

This versioning convention started with the 9.y.z Infernalis cycle. Prior to that, versions looked with 0.y for development releases and 0.y.z for stable series.

### Development releases \(x.0.z\)[¶](https://docs.ceph.com/en/pacific/releases/general/ "Permalink to this headline")

Each development release \(x.0.z\) freezes the master development branch and applies [integration and upgrade tests](https://docs.ceph.com/en/pacific/releases/general/) before it is released. Once released, there is no effort to backport fixes; developer focus is on the next development release which is usually only a few weeks away.

* Development release every 8 to 12 weeks
* Intended for testing, not production deployments
* Full integration testing
* Upgrade testing from the last stable release\(s\)
* Every effort is made to allow _offline_ upgrades from previous development releases \(meaning you can stop all daemons, upgrade, and restart\). No attempt is made to support online rolling upgrades between development releases. This facilitates deployment of development releases on non\-production test clusters without repopulating them with data.

### Release candidates \(x.1.z\)[¶](https://docs.ceph.com/en/pacific/releases/general/ "Permalink to this headline")

There is a feature release roughly eight \(8\) weeks prior to the planned initial stable release, after which focus shifts to stabilization and bug fixes only.

* Release candidate release every 1\-2 weeks
* Intended for final testing and validation of the upcoming stable release

### Stable releases \(x.2.z\)[¶](https://docs.ceph.com/en/pacific/releases/general/ "Permalink to this headline")

Once the initial stable release is made \(x.2.0\), there are semi\-regular bug\-fix point releases with bug fixes and \(occasionally\) small feature backports. Bug fixes are accumulated and included in the next point release.

* Stable point release every 4 to 6 weeks
* Intended for production deployments
* Bug fix backports for two full release cycles.
* Online, rolling upgrade support and testing from the last two \(2\) stable release\(s\) \(starting from Luminous\).
* Online, rolling upgrade support and testing from prior stable point releases

For each stable release:

* [Integration and upgrade tests](https://docs.ceph.com/en/pacific/releases/general/) are run on a regular basis and [their results](https://docs.ceph.com/en/pacific/releases/general/) analyzed by Ceph developers.
* [Issues](https://docs.ceph.com/en/pacific/releases/general/)fixed in the development branch \(master\) are scheduled to be backported.
* When an issue found in the stable release is [reported](https://docs.ceph.com/en/pacific/releases/general/), it is triaged by Ceph developers.
* The [stable releases and backport team](https://docs.ceph.com/en/pacific/releases/general/)publishes `point releases` including fixes that have been backported to the stable release.

## Lifetime of stable releases[¶](https://docs.ceph.com/en/pacific/releases/general/ "Permalink to this headline")

The lifetime of a stable release series is calculated to be approximately 24 months \(i.e., two 12 month release cycles\) after the month of the first release. For example, Mimic \(13.2.z\) will reach end of life \(EOL\) shortly after Octopus \(15.2.0\) is released. The lifetime of a release may vary because it depends on how quickly the stable releases are published.

In the case of Jewel and Kraken, the lifetime was slightly different than described above. Prior to Luminous, only every other stable release was an “LTS” release. Therefore,

* Upgrade scenarios “Jewel \-\> Kraken \-\> Luminous” and “Jewel \-\> Luminous” were expected to work.
* Upgrades from Jewel or Kraken must upgrade to Luminous first before proceeding further \(e.g., Kraken \-\> Luminous \-\> Mimic but not Kraken \-\> Mimic\).
* Jewel was maintained until Mimic was released \(June 2018\).
* Kraken is no longer maintained.

Detailed information on all releases, past and present, can be found at [Ceph Releases \(index\)](https://docs.ceph.com/en/pacific/releases/general/)

## Release timeline[¶](https://docs.ceph.com/en/pacific/releases/general/ "Permalink to this headline")

|Date |development |[octopus](https://docs.ceph.com/en/pacific/releases/general/) |[nautilus](https://docs.ceph.com/en/pacific/releases/general/) |[mimic](https://docs.ceph.com/en/pacific/releases/general/) |[luminous](https://docs.ceph.com/en/pacific/releases/general/) |[kraken](https://docs.ceph.com/en/pacific/releases/general/) | [jewel](https://docs.ceph.com/en/pacific/releases/general/) | [infernalis](https://docs.ceph.com/en/pacific/releases/general/) | [hammer](https://docs.ceph.com/en/pacific/releases/general/) | [giant](https://docs.ceph.com/en/pacific/releases/general/) | [firefly](https://docs.ceph.com/en/pacific/releases/general/) |
|--------|------------------------------------------------------------|-------------------------------------------------------------|--------------------------------------------------------------|-------------------------------------------------------------|--------------------------------------------------------------|------------------------------------------------------------|-------------------------------------------------------------|----------------------------------------------------------------|-------------------------------------------------------------|------------------------------------------------------------|-------------------------------------------------------------|
|Nov 2020 |– |[15.2.7](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |
|Nov 2020 |– |[15.2.6](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |
|Nov 2020 |– |– |[14.2.14](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |
|Nov 2020 |– |– |[14.2.13](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |
|Sep 2020 |– |– |[14.2.12](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |
|Sep 2020 |– |[15.2.5](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |
|Aug 2020 |– |– |[14.2.11](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |
|Jun 2020 |– |[15.2.4](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |
|Jun 2020 |– |– |[14.2.10](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |
|May 2020 |– |[15.2.3](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |
|May 2020 |– |[15.2.2](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |
|Apr 2020 |– |– |– |[13.2.10](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |
|Apr 2020 |– |– |– |[13.2.9](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |
|Apr 2020 |– |– |[14.2.9](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |
|Apr 2020 |– |[15.2.1](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |
|Mar 2020 |– |[15.2.0](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |
|Mar 2020 |15.1.1 |– |– |– |– |– |– |– |– |– |– |
|Mar 2020 |– |– |[14.2.8](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |
|Jan 2020 |– |– |[14.2.7](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |
|Jan 2020 |– |– |– |– |[12.2.13](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |
|Jan 2020 |15.1.0 |– |– |– |– |– |– |– |– |– |– |
|Jan 2020 |– |– |[14.2.6](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |
|Dec 2019 |– |– |– |[13.2.8](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |
|Dec 2019 |– |– |[14.2.5](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |
|Nov 2019 |– |– |– |[13.2.7](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |
|Sep 2019 |– |– |[14.2.4](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |
|Sep 2019 |– |– |[14.2.3](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |
|Jul 2019 |– |– |[14.2.2](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |
|Jun 2019 |– |– |– |[13.2.6](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |
|Apr 2019 |– |– |[14.2.1](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |
|Apr 2019 |– |– |– |– |[12.2.12](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |
|Apr 2019 |15.0.0 |– |– |– |– |– |– |– |– |– |– |
|Mar 2019 |– |– |[14.2.0](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |
|Mar 2019 |– |– |– |[13.2.5](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |
|Mar 2019 |14.1.1 |– |– |– |– |– |– |– |– |– |– |
|Feb 2019 |14.1.0 |– |– |– |– |– |– |– |– |– |– |
|Jan 2019 |– |– |– |– |[12.2.11](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |
|Jan 2019 |– |– |– |[13.2.4](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |
|Jan 2019 |– |– |– |[13.2.3](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |
|Nov 2018 |– |– |– |– |[12.2.10](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |
|Nov 2018 |14.0.1 |– |– |– |– |– |– |– |– |– |– |
|Nov 2018 |– |– |– |– |[12.2.9](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |
|Sep 2018 |– |– |– |[13.2.2](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |
|Sep 2018 |– |– |– |– |[12.2.8](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |
|Jul 2018 |– |– |– |– |– |– |[10.2.11](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |
|Jul 2018 |– |– |– |[13.2.1](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |
|Jul 2018 |– |– |– |– |[12.2.7](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |
|Jul 2018 |– |– |– |– |[12.2.6](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |
|Jun 2018 |– |– |– |[13.2.0](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |
|May 2018 |14.0.0 |– |– |– |– |– |– |– |– |– |– |
|May 2018 |13.1.0 |– |– |– |– |– |– |– |– |– |– |
|Apr 2018 |– |– |– |– |[12.2.5](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |
|Apr 2018 |13.0.2 |– |– |– |– |– |– |– |– |– |– |
|Feb 2018 |– |– |– |– |[12.2.4](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |
|Feb 2018 |– |– |– |– |[12.2.3](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |
|Feb 2018 |13.0.1 |– |– |– |– |– |– |– |– |– |– |
|Dec 2017 |– |– |– |– |[12.2.2](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |
|Oct 2017 |– |– |– |– |– |– |[10.2.10](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |
|Sep 2017 |– |– |– |– |[12.2.1](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |
|Aug 2017 |– |– |– |– |[12.2.0](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |
|Aug 2017 |– |– |– |– |– |[11.2.1](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |
|Aug 2017 |13.0.0 |– |– |– |– |– |– |– |– |– |– |
|Aug 2017 |12.1.4 |– |– |– |– |– |– |– |– |– |– |
|Aug 2017 |12.1.3 |– |– |– |– |– |– |– |– |– |– |
|Aug 2017 |12.1.2 |– |– |– |– |– |– |– |– |– |– |
|Jul 2017 |– |– |– |– |– |– |[10.2.9](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |
|Jul 2017 |– |– |– |– |– |– |[10.2.8](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |
|Jul 2017 |12.1.1 |– |– |– |– |– |– |– |– |– |– |
|Jun 2017 |12.1.0 |– |– |– |– |– |– |– |– |– |– |
|May 2017 |12.0.3 |– |– |– |– |– |– |– |– |– |– |
|Apr 2017 |– |– |– |– |– |– |[10.2.7](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |
|Apr 2017 |12.0.2 |– |– |– |– |– |– |– |– |– |– |
|Mar 2017 |– |– |– |– |– |– |[10.2.6](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |
|Mar 2017 |12.0.1 |– |– |– |– |– |– |– |– |– |– |
|Feb 2017 |– |– |– |– |– |– |– |– |[0.94.10](https://docs.ceph.com/en/pacific/releases/general/) |– |– |
|Feb 2017 |12.0.0 |– |– |– |– |– |– |– |– |– |– |
|Jan 2017 |– |– |– |– |– |[11.2.0](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |
|Jan 2017 |11.1.1 |– |– |– |– |– |– |– |– |– |– |
|Dec 2016 |– |– |– |– |– |– |[10.2.5](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |
|Dec 2016 |– |– |– |– |– |– |[10.2.4](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |
|Dec 2016 |11.1.0 |– |– |– |– |– |– |– |– |– |– |
|Oct 2016 |[11.0.2](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |– |
|Oct 2016 |11.0.1 |– |– |– |– |– |– |– |– |– |– |
|Sep 2016 |– |– |– |– |– |– |[10.2.3](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |
|Aug 2016 |– |– |– |– |– |– |– |– |[0.94.9](https://docs.ceph.com/en/pacific/releases/general/) |– |– |
|Aug 2016 |– |– |– |– |– |– |– |– |[0.94.8](https://docs.ceph.com/en/pacific/releases/general/) |– |– |
|Jun 2016 |– |– |– |– |– |– |[10.2.2](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |
|Jun 2016 |11.0.0 |– |– |– |– |– |– |– |– |– |– |
|May 2016 |– |– |– |– |– |– |[10.2.1](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |
|May 2016 |– |– |– |– |– |– |– |– |[0.94.7](https://docs.ceph.com/en/pacific/releases/general/) |– |– |
|Apr 2016 |– |– |– |– |– |– |[10.2.0](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |
|Apr 2016 |[10.1.2](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |– |
|Apr 2016 |[10.1.1](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |– |
|Mar 2016 |[10.1.0](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |– |
|Mar 2016 |[10.0.5](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |– |
|Mar 2016 |10.0.4 |– |– |– |– |– |– |– |– |– |– |
|Feb 2016 |– |– |– |– |– |– |– |[9.2.1](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |
|Feb 2016 |– |– |– |– |– |– |– |– |[0.94.6](https://docs.ceph.com/en/pacific/releases/general/) |– |– |
|Feb 2016 |[10.0.3](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |– |
|Jan 2016 |[10.0.2](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |– |
|Dec 2015 |[10.0.1](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |– |
|Nov 2015 |– |– |– |– |– |– |– |[9.2.0](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |
|Nov 2015 |– |– |– |– |– |– |– |– |– |– |[0.80.11](https://docs.ceph.com/en/pacific/releases/general/) |
|Nov 2015 |[10.0.0](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |– |
|Oct 2015 |– |– |– |– |– |– |– |– |[0.94.5](https://docs.ceph.com/en/pacific/releases/general/) |– |– |
|Oct 2015 |– |– |– |– |– |– |– |– |[0.94.4](https://docs.ceph.com/en/pacific/releases/general/) |– |– |
|Oct 2015 |[9.1.0](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |– |
|Aug 2015 |– |– |– |– |– |– |– |– |[0.94.3](https://docs.ceph.com/en/pacific/releases/general/) |– |– |
|Aug 2015 |[9.0.3](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |– |
|Jul 2015 |– |– |– |– |– |– |– |– |– |– |[0.80.10](https://docs.ceph.com/en/pacific/releases/general/) |
|Jul 2015 |[9.0.2](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |– |
|Jun 2015 |– |– |– |– |– |– |– |– |[0.94.2](https://docs.ceph.com/en/pacific/releases/general/) |– |– |
|Jun 2015 |[9.0.1](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |– |
|May 2015 |[9.0.0](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |– |
|Apr 2015 |– |– |– |– |– |– |– |– |[0.94.1](https://docs.ceph.com/en/pacific/releases/general/) |– |– |
|Apr 2015 |– |– |– |– |– |– |– |– |[0.94](https://docs.ceph.com/en/pacific/releases/general/) |– |– |
|Apr 2015 |– |– |– |– |– |– |– |– |– |[0.87.2](https://docs.ceph.com/en/pacific/releases/general/) |– |
|Mar 2015 |– |– |– |– |– |– |– |– |– |– |[0.80.9](https://docs.ceph.com/en/pacific/releases/general/) |
|Feb 2015 |– |– |– |– |– |– |– |– |– |[0.87.1](https://docs.ceph.com/en/pacific/releases/general/) |– |
|Feb 2015 |[0.93](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |– |
|Feb 2015 |[0.92](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |– |
|Jan 2015 |– |– |– |– |– |– |– |– |– |– |[0.80.8](https://docs.ceph.com/en/pacific/releases/general/) |
|Jan 2015 |[0.91](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |– |
|Dec 2014 |[0.90](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |– |
|Dec 2014 |[0.89](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |– |
|Nov 2014 |[0.88](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |– |
|Oct 2014 |– |– |– |– |– |– |– |– |– |[0.87](https://docs.ceph.com/en/pacific/releases/general/) |– |
|Oct 2014 |– |– |– |– |– |– |– |– |– |– |[0.80.7](https://docs.ceph.com/en/pacific/releases/general/) |
|Oct 2014 |– |– |– |– |– |– |– |– |– |– |[0.80.6](https://docs.ceph.com/en/pacific/releases/general/) |
|Oct 2014 |[0.86](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |– |
|Sep 2014 |[0.85](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |– |
|Aug 2014 |[0.84](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |– |
|Jul 2014 |– |– |– |– |– |– |– |– |– |– |[0.80.5](https://docs.ceph.com/en/pacific/releases/general/) |
|Jul 2014 |– |– |– |– |– |– |– |– |– |– |[0.80.4](https://docs.ceph.com/en/pacific/releases/general/) |
|Jul 2014 |– |– |– |– |– |– |– |– |– |– |[0.80.3](https://docs.ceph.com/en/pacific/releases/general/) |
|Jul 2014 |– |– |– |– |– |– |– |– |– |– |[0.80.2](https://docs.ceph.com/en/pacific/releases/general/) |
|Jul 2014 |[0.83](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |– |
|Jun 2014 |[0.82](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |– |
|Jun 2014 |[0.81](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |– |
|May 2014 |– |– |– |– |– |– |– |– |– |– |[0.80.1](https://docs.ceph.com/en/pacific/releases/general/) |
|May 2014 |– |– |– |– |– |– |– |– |– |– |[0.80](https://docs.ceph.com/en/pacific/releases/general/) |
|Apr 2014 |[0.79](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |– |
|Mar 2014 |[0.78](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |– |
|Feb 2014 |[0.77](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |– |
|Jan 2014 |[0.76](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |– |
|Jan 2014 |[0.75](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |– |
|Dec 2013 |[0.74](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |– |
|Dec 2013 |[0.73](https://docs.ceph.com/en/pacific/releases/general/) |– |– |– |– |– |– |– |– |– |– |
