# 87 Stretch Clusters

**クリップソース:** [87 Stretch Clusters — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/operations/stretch-mode/)

# Stretch Clusters[¶](https://docs.ceph.com/en/pacific/rados/operations/stretch-mode/ "Permalink to this headline")

## Stretch Clusters[¶](https://docs.ceph.com/en/pacific/rados/operations/stretch-mode/ "Permalink to this headline")

Cephは、ネットワーク部分とクラスタ全体が同等の信頼性を持ち、障害がCRUSHマップにランダムに分散することを期待しています。スイッチを失って多数のOSDが停止することがありますが、残りのOSDとモニタがそれを回避するようにルーティングすることが期待されます。

これは通常は適切ですが、クラスタの大部分が単一のネットワークコンポーネントの背後にある拡張クラスタ構成ではうまく機能しない場合があります。 たとえば、複数のデータセンターにある単一クラスタで、DCの損失を維持したいとします。

標準的な構成としては、2つまたは3つのデータセンター（アベイラビリティゾーン）が導入されているのを見ることがあります。2つのゾーンの場合、各サイトはデータのコピーを保持し、3番目のサイトにはタイブレーカーとなるモニタ（メインサイトと比較して高レイテンシ）を置き、ネットワーク接続に障害が発生しても両方のDCが生きていれば勝者を選択することを想定しています。3つのサイトでは、データのコピーと、各サイトに同数のモニタがあることを想定しています。

Cephの標準的な構成は、ネットワークやデータセンターの障害に耐え、データの一貫性を損なうことがないことに注意してください。 障害発生後、十分な数のCephサーバを復旧させれば、回復します。データセンターを失っても、モニタのクォーラムを形成し、すべてのデータを利用できる場合（プールのmin\_sizeを満たすために再レプリケーションするCRUSHルール）、Cephは可用性を維持します。

何が処理できないのでしょうか？

## Stretch Cluster Issues[¶](https://docs.ceph.com/en/pacific/rados/operations/stretch-mode/ "Permalink to this headline")

何が起こっても、Cephはデータの完全性と一貫性に妥協しません。ネットワークに障害が発生したり、ノードが失われたりしても、サービスを復旧できれば、Cephは自力で正常な機能に戻ります。

しかし、Cephの一貫性とサイジングの制約を満たすのに十分なサーバーがあるにもかかわらずデータの可用性を失うシナリオや、Cephの制約を満たさないことに驚くようなシナリオもあります。これらの障害の最初の重要なカテゴリは、不整合ネットワーク周りで解決します。ネットスプリットがある場合、プライマリがデータをレプリケートできないにもかかわらず、CephはOSDをマークdownできず、動作中のPGセットから追い出される可能性があります。この場合、Cephは耐久性保証を満たすことができないため、IOは許可されません。

2つ目の重要な失敗のカテゴリーは、データセンター間でデータを複製しているつもりでも、制約条件がそれを保証するのに十分でない場合です。例えば、データセンターAとBがあり、CRUSHルールが3つのコピーをターゲットとし、各データセンターにmin\_size 2でコピーを配置するとします。PGは、サイトAに2つのコピー、サイトBにコピーがない状態でアクティブになることがありますが、これはサイトAが失われると、データが失われCephで操作できないことを意味します。この状況を標準のCRUSHルールで回避するのは驚くほど困難です。

## Stretch Mode[¶](https://docs.ceph.com/en/pacific/rados/operations/stretch-mode/ "Permalink to this headline")

新しいストレッチモードは、2サイトのケースに対応するように設計されています。3サイトはネットスプリットの問題と同じように影響を受けやすいですが、2サイトクラスタよりもコンポーネントの可用性停止に対する耐性がはるかに高いです。

ストレッチモードに入るには、CRUSHマップに合わせて、各モニタの位置を設定する必要があります。例えば、mon.aを最初のデータセンターに配置する場合：

```
$ ceph mon set_location a datacenter=site1
```

次に、各データセンタに2つのコピーを配置するCRUSHルールを生成します。これは、CRUSHマップを直接編集する必要があります。

```
$ ceph osd getcrushmap > crush.map.bin
$ crushtool -d crush.map.bin -o crush.map.txt
```

ここで、crush.map.txtファイルを編集して、新しいルールを追加します。ここでは他のルールは1つしかないので、ID 1となっていますが、別のルールIDを使用する必要があるかもしれません。また、site1 と site2 という名前の2つのデータセンターバケットを用意しました。

```
rule stretch_rule {
        id 1
        type replicated
        min_size 1
        max_size 10
        step take site1
        step chooseleaf firstn 2 type host
        step emit
        step take site2
        step chooseleaf firstn 2 type host
        step emit
}
```

最後に、CRUSH マップをインジェクトして、クラスタがルールを利用できるようにします。

```
$ crushtool -c crush.map.txt -o crush2.map.bin
$ ceph osd setcrushmap -i crush2.map.bin
```

まだモニタをconnectivityモードで動作させていない場合は、「[Changing Monitor Elections](https://docs.ceph.com/en/pacific/rados/operations/stretch-mode/)」の手順でconnectivityモードにしてください。[88: Configure Monitor Election Strategies](88-Configure_Monitor_Election_Strategies.md)

そして最後に、クラスタにストレッチモードに入るように指示します。ここでは mon.e がタイブレーカーとなり、データセンタ間で分割しています。

```
$ ceph mon enable_stretch_mode e stretch_rule data center
```

ストレッチモードが有効な場合、OSDはデータセンタ（指定したCRUSHバケットタイプ）間でピアが発生したときに、両方が生きていると仮定して、PGをアクティブにするだけです。プールのサイズはデフォルトの3から4に増加し、各サイトに2つずつコピーされる予定です。OSDは、同じデータセンタ内のモニタにのみ接続することができます。新しいモニタは、場所を指定しない場合、クラスタに参加することができません。

データセンタのすべてのOSDとモニタが一度にアクセス不能になった場合、生存しているデータセンタはデグレードストレッチモードに移行します。これにより、警告が発行され、min\_sizeが1に減らされ、クラスタが残りの1つのサイトのデータでアクティブになるようにします。プール・サイズは変更しないので、プールが小さすぎるという警告も表示されますが、特別なストレッチ・モード・フラグにより、残りのデータ・センタでOSDが余分なコピーを作成することはありません（以前と同様に2つのコピーしか保持しません）。

行方不明のデータセンタが戻ってくると、クラスタはリカバリーストレッチモードになります。これにより、警告が変更され、ピアリングが可能になりますが、依然として、ずっと稼働していたデータセンタからのOSDのみが必要です。すべての PG が既知の状態にあり、デグレードでも不完全でもない場合、クラスタは通常のストレッチ・モードに移行して警告を終了し、min\_size を開始時の値 \(2\) に戻して両方のサイトにピアリングを要求し、ピアリング時に常に稼働しているサイトを要求しなくなります（必要に応じて他のサイトにフェイルオーバーできるようにするためです）。

## Stretch Mode Limitations[¶](https://docs.ceph.com/en/pacific/rados/operations/stretch-mode/ "Permalink to this headline")

設定にあるように、ストレッチモードはOSDを持つ2つのサイトしか扱えません。

強制ではありませんが、各サイトで2台＋タイブレーク1台、合計5台のモニターを稼働させる必要があります。これは、OSDがストレッチモードのとき、自サイトのモニターにしか接続できないためです。

ストレッチモードでは、ECプールを使用することはできません。試しても拒否されますし、ストレッチモードでECプールを作成することはできません。

各サイトに2つのコピーを提供する独自のCRUSHルールを作成する必要があり、各サイトに2つの合計4つのコピーを使用する必要があります。 デフォルト以外のsize / min\_sizeの既存のプールがある場合、ストレッチモードを有効にしようとするとCephは反対します。

デグレード時にはmin\_size=1で実行されるため、ストレッチモードはオールフラッシュOSDでのみ使用する必要があります。 これにより、接続性が回復した後の復旧に必要な時間を最小限に抑えることができるため、データ損失の可能性を最小限に抑えることができます。

今後の開発で、ECプールや2つ以上のフルサイトでの運用に対応できるようになればいいなと思います。

## Other commands[¶](https://docs.ceph.com/en/pacific/rados/operations/stretch-mode/ "Permalink to this headline")

Pacific v16.2.8から、タイブレークモニタが何らかの理由で故障した場合、モニタを交換することができるようになりました。新しいモニタの電源を入れ、以下を実行します。

```
$ ceph mon set_new_tiebreaker mon.<new_mon_name>
```

このコマンドは、新しいモニタが既存の非タイブレーカーモニタと同じ場所にある場合、抗議します。このコマンドは以前のタイブレーカーモニタを削除することはありません。

また、16.2.7では、Cephをデプロイするための独自のツールを作成している場合、ceph mon set\_locationの代わりに、モニタのブート時に新しい\-\-set\-crush\-locationオプションを使用できるようになりました。このオプションは、単一の「bucket=loc」ペアのみを受け付けます。たとえば、ceph\-mon \-\-set\-crush\-location 'datacenter=a'で、enable\_stretch\_modeの実行時に指定したバケットタイプに一致させる必要があります。

デグレードストレッチモードの場合、切断されたデータセンタが戻ってくると、クラスタは自動的に「recovery」モードに移行します。これがうまくいかない場合、またはrecoveryモードを早期に有効にしたい場合は、次のコマンドを実行します。

```
$ ceph osd force_recovery_stretch_mode --yes-i-realy-mean-it
```

しかし、このコマンドは本来必要ないもので、不測の事態に対処するために搭載されています。

recoveryモードの場合、PG が正常であれば、クラスタは通常のストレッチモードに戻るはずです。これが起こらない場合、またはデータセンタ間のピアリングを早期に強制したいがデータのダウンタイムを許容する場合（またはすべての PG が完全に回復していなくてもピアリングできることを個別に検証した場合）、次のコマンドを呼び出すことができます。

```
$ ceph osd force_healthy_stretch_mode --yes-i-really-mean-it
```

このコマンドは、予期せぬ事態に対処するために用意されたもので、本来は必要ないものです。しかし、recoveryモードが生成する HEALTH\_WARN 状態を取り除くために、このコマンドを呼び出したいと思うかもしれません。
