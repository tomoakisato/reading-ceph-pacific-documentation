# 186: RBD Mirroring

**クリップソース:** [186: RBD Mirroring — Ceph Documentation](https://docs.ceph.com/en/pacific/rbd/rbd-mirroring/)

# RBD Mirroring[¶](https://docs.ceph.com/en/pacific/rbd/rbd-mirroring/ "Permalink to this headline")

RBDイメージは、2つのCephクラスタ間で非同期的にミラーリングすることができます。この機能は、2つのモードで利用できます：

* **Journal\-based**
* **Snapshot\-based**

注：ジャーナルベースのミラーリングにはCeph Jewelリリース以降が、スナップショットベースのミラーリングにはCeph Octopusリリース以降が必要です。

ミラーリングはピアクラスター内のプール単位で設定され、プール内の特定のイメージのサブセットに設定することができます。 ジャーナルベースのミラーリングを使用する場合、特定のプール内のすべてのイメージをミラーリングすることもできます。ミラーリングは、rbd コマンドを使用して設定します。rbd\-mirror デーモンは、リモート・ピアクラスタからイメージの更新を取得し、ローカルクラスタ内のイメージに適用する役割を担います。

レプリケーションの必要性に応じて、RBDミラーリングは片方向または双方向のレプリケーションに設定することができます：

* **One\-way Replication**
* **Two\-way Replication**

重要: rbd\-mirrorデーモンの各インスタンスは、ローカルおよびリモートCephクラスタの両方\(つまり、すべてのモニタおよびOSDホスト\)に同時に接続できる必要があります。さらに、ミラーリングのワークロードを処理するために、2つのデータセンター間で十分な帯域幅を持つネットワークが必要です。

## Pool Configuration[¶](https://docs.ceph.com/en/pacific/rbd/rbd-mirroring/ "Permalink to this headline")

以下の手順は、rbdコマンドを使用してミラーリングを設定するための基本的な管理タスクの実行方法を示しています。ミラーリングはプールごとに構成されます。

これらのプール構成手順は、両方のピアクラスタ上で実行する必要があります。これらの手順は、わかりやすくするために、"site\-a" と "site\-b" という名前の両方のクラスタが単一のホストからアクセス可能であることを前提としています。

異なるCephクラスタに接続する方法の詳細については、[rbd](https://docs.ceph.com/en/pacific/man/8/rbd)のマニュアルページを参照してください。

注：以下の例におけるクラスタ名は、同名のCeph設定ファイル\(例：/etc/ceph/site\-b.conf\)に対応します。 複数のクラスタを設定する方法については、[ceph\-conf](https://docs.ceph.com/en/pacific/rados/configuration/ceph-conf/#running-multiple-clusters)のドキュメントを参照してください。 rbd\-mirrorでは、ソースクラスタとデスティネーションクラスタが一意の内部名を持つ必要はないことに注意してください。rbd\-mirrorがローカルおよびリモートクラスタで必要とする設定ファイルには任意の名前を付けることができ、デーモンをコンテナ化することは、混乱を避けるためにそれらを/etc/cephの外で管理する1つの戦略です。

### Enable Mirroring[¶](https://docs.ceph.com/en/pacific/rbd/rbd-mirroring/ "Permalink to this headline")

rbdを使用したプールでミラーリングを有効にするには、プール名とミラーリングモードを指定してmirror pool enableサブコマンドを発行します。

```
rbd mirror pool enable {pool-name} {mode}
```

ミラーリングのモードは、イメージまたはプールのいずれかになります：

* **image**
* **pool**

For example:

```
$ rbd --cluster site-a mirror pool enable image-pool image
$ rbd --cluster site-b mirror pool enable image-pool image
```

### Disable Mirroring[¶](https://docs.ceph.com/en/pacific/rbd/rbd-mirroring/ "Permalink to this headline")

rbd を使用しているプールでミラーリングを無効にするには、mirror pool disable コマンドとプール名を指定します。

```
rbd mirror pool disable {pool-name}
```

この方法でミラーリングがプール上で無効になると、ミラーリングが明示的に有効になっている（プール内の）イメージも無効になります。

For example:

```
$ rbd --cluster site-a mirror pool disable image-pool
$ rbd --cluster site-b mirror pool disable image-pool
```

### Bootstrap Peers[¶](https://docs.ceph.com/en/pacific/rbd/rbd-mirroring/ "Permalink to this headline")

rbd\-mirrorデーモンがピアクラスターを検出するためには、ピアを登録し、ユーザーアカウントを作成する必要があります。このプロセスは、rbdとmirror pool peer bootstrap createおよびmirror pool peer bootstrap importコマンドを使用して自動化することができます。

rbdを使用して新しいブートストラップトークンを手動で作成するには、mirror pool peer bootstrap createサブコマンド、プール名、オプションのフレンドリサイト名を発行して、ローカルクラスターを記述します：

```
rbd mirror pool peer bootstrap create [--site-name {local-site-name}] {pool-name}
```

mirror pool peer bootstrap createの出力は、mirror pool peer bootstrap importコマンドに提供されるべきトークンになります。たとえば、site\-aで：

```
$ rbd --cluster site-a mirror pool peer bootstrap create --site-name site-a image-pool
eyJmc2lkIjoiOWY1MjgyZGItYjg5OS00NTk2LTgwOTgtMzIwYzFmYzM5NmYzIiwiY2xpZW50X2lkIjoicmJkLW1pcnJvci1wZWVyIiwia2V5IjoiQVFBUnczOWQwdkhvQmhBQVlMM1I4RmR5dHNJQU50bkFTZ0lOTVE9PSIsIm1vbl9ob3N0IjoiW3YyOjE5Mi4xNjguMS4zOjY4MjAsdjE6MTkyLjE2OC4xLjM6NjgyMV0ifQ==
```

rbdを使用して別のクラスターによって作成されたブートストラップトークンを手動でインポートするには、mirror pool peer bootstrap importコマンド、プール名、作成されたトークンへのファイルパス（標準入力から読み取る場合は「\-」）、ローカルクラスタを表すオプションのフレンドリーサイト名、ミラーリング方向（双方向ミラーリングではデフォルトでrx\-txですが、単方向ミラーリングではrx\-onlyに設定可能です）を指定します：

```
rbd mirror pool peer bootstrap import [--site-name {local-site-name}] [--direction {rx-only or rx-tx}] {pool-name} {token-path}
```

For example, on site\-b:

```
$ cat <<EOF > token
eyJmc2lkIjoiOWY1MjgyZGItYjg5OS00NTk2LTgwOTgtMzIwYzFmYzM5NmYzIiwiY2xpZW50X2lkIjoicmJkLW1pcnJvci1wZWVyIiwia2V5IjoiQVFBUnczOWQwdkhvQmhBQVlMM1I4RmR5dHNJQU50bkFTZ0lOTVE9PSIsIm1vbl9ob3N0IjoiW3YyOjE5Mi4xNjguMS4zOjY4MjAsdjE6MTkyLjE2OC4xLjM6NjgyMV0ifQ==
EOF
$ rbd --cluster site-b mirror pool peer bootstrap import --site-name site-b image-pool token
```

### Add Cluster Peer Manually[¶](https://docs.ceph.com/en/pacific/rbd/rbd-mirroring/ "Permalink to this headline")

必要に応じて、または現在インストールされているCephのリリースで上記のbootstrapコマンドが使用できない場合は、クラスタピアを手動で指定できます。

リモートの rbd\-mirror デーモンは、ミラーリングを実行するためにローカルクラスタにアクセスする必要があります。リモートデーモンが使用するために、新しいローカルCephユーザを作成する必要があります。[Cephユーザを作成する](https://docs.ceph.com/en/pacific/rados/operations/user-management#add-a-user)には、cephでauthget\-or\-createコマンド、ユーザ名、モニタCAPS、OSD CAPSを指定します。

```
ceph auth get-or-create client.rbd-mirror-peer mon 'profile rbd' osd 'profile rbd'
```

以下に説明するCephモニタのconfig\-keyストアを使用しない場合、結果のキーリングを他のクラスタのrbd\-mirrorデーモンホストにコピーする必要があります。

rbdを使用してミラーリングピアCephクラスタを手動で追加するには、mirror pool peer addコマンド、プール名、クラスタ仕様を指定します：

```
rbd mirror pool peer add {pool-name} {client-name}@{cluster-name}
```

For example:

```
$ rbd --cluster site-a mirror pool peer add image-pool client.rbd-mirror-peer@site-b
$ rbd --cluster site-b mirror pool peer add image-pool client.rbd-mirror-peer@site-a
```

デフォルトでは、rbd\-mirrorデーモンは、デフォルトまたは設定されたキーリング検索パスにある{client\-name}のキーリング\(例: /etc/ceph/{cluster\-name}.keyring\)に加えて、ピアクラスタのモニタのアドレスを示す/etc/ceph/{cluster\-name}.confにあるCeph設定ファイルにアクセスする必要があります。

または、ピアクラスタのモニタやクライアントキーをローカルCephモニタ設定キーストア内に安全に格納することも可能です。ミラーリングピアの追加時にピアクラスタ接続属性を指定するには、\-\-remote\-mon\-host と \-\-remote\-key\-fileオプションを使用します。たとえば、以下のようになります：

```
$ cat <<EOF > remote-key-file
AQAeuZdbMMoBChAAcj++/XUxNOLFaWdtTREEsw==
EOF
$ rbd --cluster site-a mirror pool peer add image-pool client.rbd-mirror-peer@site-b --remote-mon-host 192.168.1.1,192.168.1.2 --remote-key-file remote-key-file
$ rbd --cluster site-a mirror pool info image-pool --all
Mode: pool
Peers:
  UUID                                 NAME   CLIENT                 MON_HOST                KEY
  587b08db-3d33-4f32-8af8-421e77abb081 site-b client.rbd-mirror-peer 192.168.1.1,192.168.1.2 AQAeuZdbMMoBChAAcj++/XUxNOLFaWdtTREEsw==
```

### Remove Cluster Peer[¶](https://docs.ceph.com/en/pacific/rbd/rbd-mirroring/ "Permalink to this headline")

rbdを使用してミラーリングピアCephクラスタを削除するには、mirror pool peer removeコマンド、プール名、ピアUUID \(rbd mirror pool infoコマンドから取得可能\)を指定します：

```
rbd mirror pool peer remove {pool-name} {peer-uuid}
```

For example:

```
$ rbd --cluster site-a mirror pool peer remove image-pool 55672766-c02b-4729-8567-f13a66893445
$ rbd --cluster site-b mirror pool peer remove image-pool 60c0e299-b38f-4234-91f6-eed0a367be08
```

### Data Pools[¶](https://docs.ceph.com/en/pacific/rbd/rbd-mirroring/ "Permalink to this headline")

デスティネーションクラスタにイメージを作成する際、rbd\-mirrorは以下のようにデータプールを選択します：

1. 宛先クラスタに（rbd\_default\_data\_pool設定オプションで）デフォルトのデータプールが設定されている場合、それが使用される
2. それ以外の場合で、ソースイメージが個別のデータプールを使用していて、デスティネーションクラスタ上に同じ名前のプールが存在する場合、そのプールが使用される
3. 上記のいずれでもない場合、データプールは設定されない

## Image Configuration[¶](https://docs.ceph.com/en/pacific/rbd/rbd-mirroring/ "Permalink to this headline")

プール構成とは異なり、イメージ構成は単一のミラーリングピアCephクラスタに対してのみ実行する必要があります。

ミラーリングされたRBDイメージは、プライマリまたは非プライマリのいずれかに指定されます。これはイメージのプロパティであり、プールのプロパティではありません。非プライマリとして指定されたイメージは、変更できません。

イメージのミラーリングが初めて有効になると、イメージは自動的にプライマリに昇格します \(プールミラーモードがプールでジャーナリングイメージ機能が有効な場合は暗黙的に、プールミラーモードがイメージの場合は rbd コマンドで明示的に有効化されます\)。

### Enable Image Mirroring[¶](https://docs.ceph.com/en/pacific/rbd/rbd-mirroring/ "Permalink to this headline")

イメージのプールに対してイメージモードでミラーリングが設定されている場合、プール内の各イメージに対して明示的にミラーリングを有効にする必要があります。rbd で特定のイメージのミラーリングを有効にするには、プール、イメージ名、モードとともに mirror image enable コマンドを指定します。

```
rbd mirror image enable {pool-name}/{image-name} {mode}
```

ミラーイメージモードは、ジャーナルまたはスナップショットのいずれかを選択できます：

* **journal**
* **snapshot**

For example:

```
$ rbd --cluster site-a mirror image enable image-pool/image-1 snapshot
$ rbd --cluster site-a mirror image enable image-pool/image-2 journal
```

### Enable Image Journaling Feature[¶](https://docs.ceph.com/en/pacific/rbd/rbd-mirroring/ "Permalink to this headline")

RBDジャーナルベースミラーリングは、RBDイメージジャーナリング機能を使用して、複製されたイメージが常にクラッシュコンシステントを維持することを保証します。イメージミラーリングモードを使用する場合、イメージのミラーリングが有効化されると、ジャーナリング機能が自動的に有効になります。プールミラーリングモードを使用する場合、イメージをピアクラスタにミラーリングする前に、RBDイメージジャーナリング機能が有効になっている必要があります。この機能は、rbd コマンドに \-\-image\-feature exclusive\-lock,journaling オプションを指定して、イメージ作成時に有効にすることができます。

また、ジャーナリング機能は既存のRBDイメージで動的に有効にすることもできます。rbd でジャーナリングを有効にするには、feature enable コマンド、プールとイメージの名前、および機能名を指定します。

```
rbd feature enable {pool-name}/{image-name} {feature-name}
```

For example:

```
$ rbd --cluster site-a feature enable image-pool/image-1 journaling
```

注：ジャーナリング機能は排他的ロック機能に依存しています。排他的ロック機能がまだ有効になっていない場合は、ジャーナリング機能を有効にする前に排他的ロック機能を有効にする必要があります。

Tip: Ceph設定ファイルにrbd default features=125を追加すると、すべての新しいイメージでジャーナリングをデフォルトで有効にすることができます。

ヒント: rbd\-mirror のチューナブルは、デフォルトでプール全体のミラーリングに適した値に設定されています。 rbd\-mirrorを使用して単一ボリュームをクラスタにマイグレートする場合、ローカルまたは集中設定の \[client\] 設定セクションでrbd\_mirror\_journal\_max\_fetch\_bytes=33554432とrbd\_journal\_max\_payload\_bytes=8388608を設定すると大幅にパフォーマンスが向上する可能性があります。 これらの設定は、rbd\-mirrorが宛先クラスタにかなりの書き込み負荷を与える可能性があることに注意してください：マイグレーション中にクラスタパフォーマンスを注意深く監視し、複数のマイグレーションを並行して実行する前に慎重にテストしてください。

### Create Image Mirror\-Snapshots[¶](https://docs.ceph.com/en/pacific/rbd/rbd-mirroring/ "Permalink to this headline")

スナップショットベースのミラーリングを使用する場合、RBDイメージの変更されたコンテンツをミラーリングする必要がある場合は、常にミラースナップショットを作成する必要があります。rbdでミラースナップショットを手動で作成するには、プールとイメージ名と一緒にmirror image snapshotコマンドを指定します。

```
rbd mirror image snapshot {pool-name}/{image-name}
```

For example:

```
$ rbd --cluster site-a mirror image snapshot image-pool/image-1
```

デフォルトでは、1つのイメージにつき3つのミラースナップショットのみが作成されます。この制限に達した場合、最新のミラースナップショットは自動的に切り捨てられます。必要に応じて、rbd\_mirroring\_max\_mirroring\_snapshots 設定オプションで制限をオーバーライドすることができます。さらに、イメージが削除されたり、ミラーリングが無効になると、 ミラースナップショットは自動的に削除されます。

また、ミラースナップショットのスケジュールを定義すれば、定期的にミラースナップショットを自動作成することも可能です。ミラースナップショットのスケジュールは、グローバル、プール単位、イメージ単位で設定できます。複数のミラースナップショットスケジュールをどのレベルでも定義できますが、個々のミラーイメージに一致する最も具体的なスナップショットスケジュールのみが実行されます。

rbdでミラースナップショットのスケジュールを作成するには、mirror snapshot schedule addコマンドを指定し、オプションでプールまたはイメージ名、間隔、開始時刻を指定してください：

```
rbd mirror snapshot schedule add [--pool {pool-name}] [--image {image-name}] {interval} [{start-time}]
```

日、時間、分単位の指定が可能で、それぞれ d、h、m という接尾辞が付きます。開始時刻は、ISO 8601の時刻フォーマットを用いて指定することができます。例えば：

```
$ rbd --cluster site-a mirror snapshot schedule add --pool image-pool 24h 14:00:00-05:00
$ rbd --cluster site-a mirror snapshot schedule add --pool image-pool --image image1 6h
```

rbdでミラースナップショットのスケジュールを削除するには、mirror snapshot schedule removeコマンドを、対応するadd scheduleコマンドと同じオプションで指定します。

rbdで特定のレベル（グローバル、プール、イメージ）のすべてのスナップショットスケジュールを一覧表示するには、mirror snapshot schedule lsコマンドと、オプションでプールまたはイメージ名を指定します。さらに、\-\-recursiveオプションを指定すると、指定されたレベル以下のすべてのスケジュールを一覧表示することができます。たとえば、以下のようになります：

```
$ rbd --cluster site-a mirror schedule ls --pool image-pool --recursive
POOL        NAMESPACE IMAGE  SCHEDULE
image-pool  -         -      every 1d starting at 14:00:00-05:00
image-pool            image1 every 6h
```

RBDイメージのrbdによるスナップショットベースのミラーリングで、次のスナップショットがいつ作成されるかのステータスを表示するには、オプションのプールまたはイメージ名と一緒にmirror snapshot schedule statusコマンドを指定します：

```
rbd mirror snapshot schedule status [--pool {pool-name}] [--image {image-name}]
```

For example:

```
$ rbd --cluster site-a mirror schedule status
SCHEDULE TIME       IMAGE
2020-02-26 18:00:00 image-pool/image1
```

### Disable Image Mirroring[¶](https://docs.ceph.com/en/pacific/rbd/rbd-mirroring/ "Permalink to this headline")

rbd で特定のイメージのミラーリングを無効にするには、プール名とイメージ名とともにmirror image disableコマンドを指定します：

```
rbd mirror image disable {pool-name}/{image-name}
```

For example:

```
$ rbd --cluster site-a mirror image disable image-pool/image-1
```

### Image Promotion and Demotion[¶](https://docs.ceph.com/en/pacific/rbd/rbd-mirroring/ "Permalink to this headline")

プライマリ指定を対向するCephクラスタのイメージに移行する必要があるフェイルオーバーシナリオでは、プライマリイメージへのアクセスを停止し\(例: VMのパワーダウンまたはVMからの関連ドライブの削除\)、現在のプライマリイメージを降格し、新しいプライマリイメージを昇格し、代替クラスタのイメージにアクセスを再開する必要があります。

注：RBDはイメージのフェイルオーバーを促進するために必要なツールのみを提供します。完全なフェイルオーバープロセスを調整するためには、外部のメカニズムが必要です（例えば、降格前にイメージを閉じるなど）。

rbd で特定のイメージを非プライマリに降格させるには、プール名とイメージ名を指定して、mirror image demoteコマンドを指定します：

```
rbd mirror image demote {pool-name}/{image-name}
```

For example:

```
$ rbd --cluster site-a mirror image demote image-pool/image-1
```

プール内のすべてのプライマリイメージをrbdで非プライマリに降格させるには、プール名と一緒にmirror pool demoteコマンドを指定します：

```
rbd mirror pool demote {pool-name}
```

For example:

```
$ rbd --cluster site-a mirror pool demote image-pool
```

rbd で特定のイメージをプライマリに昇格させるには、プール名とイメージ名とともにmirror pool promoteコマンドを指定します：

```
rbd mirror image promote [--force] {pool-name}/{image-name}
```

For example:

```
$ rbd --cluster site-b mirror image promote image-pool/image-1
```

プール内のすべての非プライマリイメージを rbd でプライマリに昇格させるには、プール名と一緒にmirror pool promoteコマンドを指定します：

```
rbd mirror pool promote [--force] {pool-name}
```

For example:

```
$ rbd --cluster site-a mirror pool promote image-pool

```

Tip:プライマリ/非プライマリのステータスはイメージ単位なので、2つのクラスタでIO負荷を分割し、フェイルオーバー/フェイルバックを実現することができます。

注：\-\-force オプションを使用すると、昇格を強制することができます。強制昇格は、降格をピアのCephクラスタに伝搬できない場合に必要です\(Cephクラスタの障害、通信停止など\)。これにより、2つのピア間でスプリットブレインのシナリオが発生し、[force resyncコマンド](https://docs.ceph.com/en/pacific/rbd/rbd-mirroring/#force-image-resync)が発行されるまで、イメージは同期されなくなります。

### Force Image Resync[¶](https://docs.ceph.com/en/pacific/rbd/rbd-mirroring/ "Permalink to this headline")

rbd\-mirror デーモンがスプリットブレインのイベントを検出した場合、修正されるまで、影響を受けるイメージのミラーリングを試みません。あるイメージのミラーリングを再開するには、まず古いと判断された[イメージを降格させ](https://docs.ceph.com/en/pacific/rbd/rbd-mirroring/#image-promotion-and-demotion)、それからプライマリイメージへの再同期を要求してください。rbd でイメージの再同期を要求するには、プール名とイメージ名とともに mirror image resync コマンドを指定します：

```
rbd mirror image resync {pool-name}/{image-name}
```

For example:

```
$ rbd mirror image resync image-pool/image-1
```

注意：rbd コマンドは、イメージに再同期が必要であるというフラグを立てるだけです。ローカルクラスタのrbd\-mirrorデーモンプロセスは、非同期で再同期を実行する責任を負います。

## Mirror Status[¶](https://docs.ceph.com/en/pacific/rbd/rbd-mirroring/ "Permalink to this headline")

ピアクラスターレプリケーションのステータスは、プライマリミラーイメージごとに保存されます。このステータスは、mirror image statusおよびmirror pool statusコマンドを使用して取得することができます。

rbdでミラーイメージの状態を要求するには、プール名とイメージ名とともにmirror image statusコマンドを指定します：

```
rbd mirror image status {pool-name}/{image-name}
```

For example:

```
$ rbd mirror image status image-pool/image-1
```

rbd でミラープールのサマリー状態を要求するには、プール名と一緒にmirror pool statusコマンドを指定します：

```
rbd mirror pool status {pool-name}
```

For example:

```
$ rbd mirror pool status image-pool
```

注: ミラープール状態コマンドに \-\-verbose オプションを追加すると、プール内の各ミラーリング イメージの状態の詳細が追加で出力されます。

## rbd\-mirror Daemon[¶](https://docs.ceph.com/en/pacific/rbd/rbd-mirroring/ "Permalink to this headline")

2つのrbd\-mirrorデーモンは、リモートのピアクラスター上のイメージジャーナルを監視し、ローカルクラスターに対してジャーナルイベントを再生する役割を担っています。RBDのイメージジャーナリング機能は、イメージへのすべての変更を発生した順に記録します。これにより、リモートイメージのクラッシュコンシステントなミラーがローカルで利用できるようになります。

rbd\-mirror デーモンは、オプションの rbd\-mirror ディストリビューションパッケージで利用可能です。

重要：各rbd\-mirrorデーモンは、両方のクラスタに同時に接続する能力が必要です。

警告： Luminous以前のリリースではCephクラスタごとに1つのrbd\-mirrorデーモンしか実行しないでください。

各rbd\-mirrorデーモンは、一意のCephユーザーIDを使用する必要があります。Cephユーザを作成するには、cephでauthget\-or\-createコマンド、ユーザ名、モニタCAPS、OSD CAPSを指定します：

```
ceph auth get-or-create client.rbd-mirror.{unique id} mon 'profile rbd-mirror' osd 'profile rbd'
```

rbd\-mirror デーモンは、デーモンインスタンスにユーザ ID を指定することで systemd で管理することができます：

```
systemctl enable ceph-rbd-mirror@rbd-mirror.{unique id}
```

rbd\-mirrorコマンドでフォアグラウンド実行することもできます：

```
rbd-mirror -f --log-file={log_path}
```
