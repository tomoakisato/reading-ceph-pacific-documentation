# 1: Welcome to Ceph

**クリップソース:** [Welcome to Ceph — Ceph Documentation](https://docs.ceph.com/en/pacific/)

# Welcome to Ceph¶

Cephは、オブジェクト、ブロック、ファイルのストレージを1つのシステムで提供する

### Ceph Object Store

* RESTful Interface
* S3\- and Swift\-compliant APIs
* S3\-style subdomains
* Unified S3/Swift namespace
* User management
* Usage tracking
* Striped objects
* Cloud solution integration
* Multi\-site deployment
* Multi\-site replication

### Ceph Block Device

* Thin\-provisioned
* Images up to 16 exabytes
* Configurable striping
* In\-memory caching
* Snapshots
* Copy\-on\-write cloning
* Kernel driver support
* KVM/libvirt support
* Back\-end for cloud solutions
* Incremental backup
* Disaster recovery \(multisite asynchronous replication\)

### Ceph File System

* POSIX\-compliant semantics
* Separates metadata from data
* Dynamic rebalancing
* Subdirectory snapshots
* Configurable striping
* Kernel driver support
* FUSE support
* NFS/CIFS deployable
* Use with Hadoop \(replace HDFS\)

See [Ceph Object Store](https://docs.ceph.com/en/pacific/radosgw) for additional details.　[225: Ceph Object Gateway — Ceph Documentation](225-Ceph_Object_Gateway.md)

See [Ceph Block Device](https://docs.ceph.com/en/pacific/rbd) for additional details.  [181: Ceph Block Device — Ceph Documentation](181-Ceph_Block_Device.md)

See [Ceph File System](https://docs.ceph.com/en/pacific/cephfs) for additional details.  [129: Ceph File System — Ceph Documentation](129-Ceph_File_System.md)

Cephは信頼性が高く、管理が簡単、無料

Cephは、ITインフラストラクチャと膨大なデータを管理する能力を一変させる

Cephを試す：[7: Installing Ceph](7-Installing_Ceph.md)

Cephの詳細：[316: Architecture](316-Architecture_—_Ceph_Documentation.md)
