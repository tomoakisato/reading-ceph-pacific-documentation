# 232: Admin Guide

**クリップソース:** [232: Admin Guide — Ceph Documentation](https://docs.ceph.com/en/pacific/radosgw/admin/)

# Admin Guide[¶](https://docs.ceph.com/en/pacific/radosgw/admin/ "Permalink to this headline")

Ceph Object Storageサービスを稼働させたら、ユーザ管理、アクセス制御、クォータ、使用状況の追跡などの機能でサービスを管理できます。

## User Management[¶](https://docs.ceph.com/en/pacific/radosgw/admin/ "Permalink to this headline")

Ceph Object Storageのユーザ管理は、Ceph Object Storageサービスのユーザを指します\(つまり、Ceph Storage ClusterユーザとしてのCeph Object Gatewayではありません\)。エンドユーザがCeph Object Gatewayのサービスとやり取りできるようにするには、ユーザ、アクセスキー、シークレットを作成する必要があります。

ユーザータイプは2つあります：

* **User:**
* **Subuser:**

ユーザとサブユーザの作成、変更、表示、一時停止、削除ができます。ユーザとサブユーザのIDの他に、ユーザの表示名とメールアドレスを追加することができます。 キーとシークレットを指定したり、キーとシークレットを自動生成したりすることができます。キーを生成または指定する場合、ユーザIDはS3キータイプに対応し、サブユーザIDはswiftキータイプに対応することに注意してください。また、swiftキーには、read、write、readwrite、fullというアクセスレベルがあります。

### Create a User[¶](https://docs.ceph.com/en/pacific/radosgw/admin/ "Permalink to this headline")

（S3インターフェース）ユーザを作成する場合は、以下を実行します：

```
radosgw-admin user create --uid={username} --display-name="{display-name}" [--email={email}]
```

For example:

```
radosgw-admin user create --uid=johndoe --display-name="John Doe" --email=john@example.com
```

```
{ "user_id": "johndoe",
  "display_name": "John Doe",
  "email": "john@example.com",
  "suspended": 0,
  "max_buckets": 1000,
  "subusers": [],
  "keys": [
        { "user": "johndoe",
          "access_key": "11BS02LGFB6AL6H1ADMW",
          "secret_key": "vzCEkuryfn060dfee4fgQPqFrncKEIkh3ZcdOANY"}],
  "swift_keys": [],
  "caps": [],
  "op_mask": "read, write, delete",
  "default_placement": "",
  "placement_tags": [],
  "bucket_quota": { "enabled": false,
      "max_size_kb": -1,
      "max_objects": -1},
  "user_quota": { "enabled": false,
      "max_size_kb": -1,
      "max_objects": -1},
  "temp_url_keys": []}
```

ユーザを作成すると、S3 API互換のクライアントで使用するためのaccess\_keyとsecret\_keyのエントリも作成されます。

重要：キーの出力を確認してください。時々、radosgw\-admin は JSON エスケープ \(\) 文字を生成しますが、一部のクライアントは JSON エスケープ文字をどのように処理すればよいのかわかりません。対処法としては、JSONエスケープ文字（）を削除する、文字列を引用符で囲む、キーを再生成してJSONエスケープ文字がないことを確認する、キーとシークレットを手動で指定する、などがあります。

### Create a Subuser[¶](https://docs.ceph.com/en/pacific/radosgw/admin/ "Permalink to this headline")

ユーザの（Swiftインターフェース）サブユーザを作成するには、ユーザID（\-\-uid={username}）、サブユーザID、サブユーザのアクセスレベルを指定する必要があります。

```
radosgw-admin subuser create --uid={uid} --subuser={uid} --access=[ read | write | readwrite | full ]
```

For example:

```
radosgw-admin subuser create --uid=johndoe --subuser=johndoe:swift --access=full
```

注：fullはアクセスコントロールポリシーも含むため、readwriteではありません。

```
{ "user_id": "johndoe",
  "display_name": "John Doe",
  "email": "john@example.com",
  "suspended": 0,
  "max_buckets": 1000,
  "subusers": [
        { "id": "johndoe:swift",
          "permissions": "full-control"}],
  "keys": [
        { "user": "johndoe",
          "access_key": "11BS02LGFB6AL6H1ADMW",
          "secret_key": "vzCEkuryfn060dfee4fgQPqFrncKEIkh3ZcdOANY"}],
  "swift_keys": [],
  "caps": [],
  "op_mask": "read, write, delete",
  "default_placement": "",
  "placement_tags": [],
  "bucket_quota": { "enabled": false,
      "max_size_kb": -1,
      "max_objects": -1},
  "user_quota": { "enabled": false,
      "max_size_kb": -1,
      "max_objects": -1},
  "temp_url_keys": []}

```

### Get User Info[¶](https://docs.ceph.com/en/pacific/radosgw/admin/ "Permalink to this headline")

ユーザに関する情報を取得するには、userinfo とユーザID \(\-\-uid={username}\) を指定する必要があります。

```
radosgw-admin user info --uid=johndoe
```

### Modify User Info[¶](https://docs.ceph.com/en/pacific/radosgw/admin/ "Permalink to this headline")

ユーザーに関する情報を変更するには、ユーザID \(\-\-uid={username}\) と変更したい属性を指定する必要があります。典型的な修正項目は、キーとシークレット、電子メールアドレス、表示名、アクセスレベルです。例：

```
radosgw-admin user modify --uid=johndoe --display-name="John E. Doe"
```

サブユーザの値を変更する場合は、subuser modify、ユーザID、サブユーザIDを指定します。例：

```
radosgw-admin subuser modify --uid=johndoe --subuser=johndoe:swift --access=full
```

### User Enable/Suspend[¶](https://docs.ceph.com/en/pacific/radosgw/admin/ "Permalink to this headline")

ユーザを作成すると、そのユーザはデフォルトで有効になります。しかし、ユーザ権限を一時停止して、後で再び有効にすることができます。ユーザを一時停止するには、user suspendとユーザIDを指定します。

```
radosgw-admin user suspend --uid=johndoe
```

停止したユーザーを再有効化するには、user enableとユーザーIDを指定します。

```
radosgw-admin user enable --uid=johndoe
```

注：ユーザを無効にすると、サブユーザも無効になります。

### Remove a User[¶](https://docs.ceph.com/en/pacific/radosgw/admin/ "Permalink to this headline")

ユーザを削除すると、そのユーザとサブユーザもシステムから削除されます。ただし、必要に応じて、サブユーザのみを削除することもできます。ユーザ（およびサブユーザ）を削除するには、user rmとユーザIDを指定します。

```
radosgw-admin user rm --uid=johndoe
```

サブユーザのみを削除する場合は、subuser rmとサブユーザIDを指定します。

```
radosgw-admin subuser rm --subuser=johndoe:swift
```

オプションは以下の通りです：

* **Purge Data:**
* **Purge Keys:**

### Remove a Subuser[¶](https://docs.ceph.com/en/pacific/radosgw/admin/ "Permalink to this headline")

サブユーザを削除すると、Swiftインターフェースへのアクセスも削除されます。ユーザはシステム内に残ります。サブユーザを削除するには、subuser rmとサブユーザIDを指定します。

```
radosgw-admin subuser rm --subuser=johndoe:swift
```

オプションは以下の通りです：

* **Purge Keys:**

### Add / Remove a Key[¶](https://docs.ceph.com/en/pacific/radosgw/admin/ "Permalink to this headline")

ユーザとサブユーザの両方が、S3またはSwiftのインターフェースにアクセスするためにキーを必要とします。S3を利用するためには、アクセスキーとシークレットキーからなるキーペアが必要です。一方、Swiftを利用するためには、通常、シークレットキー（パスワード）が必要であり、ユーザIDと共に使用します。キーを作成し、アクセスキーやシークレットキーを指定したり生成したりすることができます。また、キーを削除することもできます。オプションは以下の通りです：

* \-\-key\-type=\<type\> キーの種類を指定する。オプションは、S3、Swift
* \-\-access\-key=\<key\> S3アクセスキーを指定する
* \-\-secret\-key=\<key\> S3シークレットキーまたはSwiftシークレットキーを指定する
* \-\-gen\-access\-key ランダムなS3アクセスキーを自動生成する
* \-\-gen\-secret ランダムなS3シークレットキーまたはSwiftシークレットキーを自動生成する

あるユーザに指定したS3キーペアを追加する例：

```
radosgw-admin key create --uid=foo --key-type=s3 --access-key fooAccessKey --secret-key fooSecretKey
```

```
{ "user_id": "foo",
  "rados_uid": 0,
  "display_name": "foo",
  "email": "foo@example.com",
  "suspended": 0,
  "keys": [
    { "user": "foo",
      "access_key": "fooAccessKey",
      "secret_key": "fooSecretKey"}],
}

```

1人のユーザーに対して複数のS3キーペアを作成することができることに注意してください。

サブユーザに指定された swift シークレットキーをアタッチする例：

```
radosgw-admin key create --subuser=foo:bar --key-type=swift --secret-key barSecret
```

```
{ "user_id": "foo",
  "rados_uid": 0,
  "display_name": "foo",
  "email": "foo@example.com",
  "suspended": 0,
  "subusers": [
     { "id": "foo:bar",
       "permissions": "full-control"}],
  "swift_keys": [
    { "user": "foo:bar",
      "secret_key": "asfghjghghmgm"}]}

```

サブユーザが持つことができるのは、1つの swift シークレットキーだけであることに注意してください。

サブユーザがS3キーペアと関連付けられている場合、サブユーザはS3 APIで使用することもできます：

```
radosgw-admin key create --subuser=foo:bar --key-type=s3 --access-key barAccessKey --secret-key barSecretKey
```

```
{ "user_id": "foo",
  "rados_uid": 0,
  "display_name": "foo",
  "email": "foo@example.com",
  "suspended": 0,
  "subusers": [
     { "id": "foo:bar",
       "permissions": "full-control"}],
  "keys": [
    { "user": "foo:bar",
      "access_key": "barAccessKey",
      "secret_key": "barSecretKey"}],
}
```

S3キーペアを削除するには、アクセスキーを指定します。

```
radosgw-admin key rm --uid=foo --key-type=s3 --access-key=fooAccessKey
```

swiftシークレットキーを削除するには：

```
radosgw-admin key rm --subuser=foo:bar --key-type=swift
```

### Add / Remove Admin Capabilities[¶](https://docs.ceph.com/en/pacific/radosgw/admin/ "Permalink to this headline")

Ceph Storage Clusterは、REST APIを介して管理機能を実行できる管理APIを提供します。デフォルトでは、ユーザはこのAPIにアクセスすることはできません。ユーザが管理機能を実行できるようにするには、当該ユーザに管理CAPSを提供します。

ユーザに管理CAPSを追加するには、次のように実行します：

```
radosgw-admin caps add --uid={uid} --caps={caps}
```

ユーザ、バケット、メタデータ、使用状況（利用状況）に対して、read、write、allのCAPSを追加することができます。例：

```
--caps="[users|buckets|metadata|usage|zone]=[*|read|write|read, write]"
```

For example:

```
radosgw-admin caps add --uid=johndoe --caps="users=*;buckets=*"
```

ユーザから管理CAPSを削除するには、次のように実行します：

```
radosgw-admin caps rm --uid=johndoe --caps={caps}
```

## Quota Management[¶](https://docs.ceph.com/en/pacific/radosgw/admin/ "Permalink to this headline")

Ceph Object Gatewayでは、ユーザおよびユーザが所有するバケットにクォータを設定することができます。クォータには、バケット内のオブジェクトの最大数と、バケットが保持できる最大ストレージサイズが含まれます。

* **Bucket:**
* **Maximum Objects:**
* **Maximum Size:**
* **Quota Scope:**

### Set User Quota[¶](https://docs.ceph.com/en/pacific/radosgw/admin/ "Permalink to this headline")

クォータを有効にする前に、まずクォータパラメータを設定する必要があります。例：

```
radosgw-admin quota set --quota-scope=user --uid=<uid> [--max-objects=<num objects>] [--max-size=<max size>]
```

For example:

```
radosgw-admin quota set --quota-scope=user --uid=johndoe --max-objects=1024 --max-size=1024B
```

num objectsと/ max sizeに負の値がある場合、当該クォータ属性のチェックが無効であることを意味します。

### Enable/Disable User Quota[¶](https://docs.ceph.com/en/pacific/radosgw/admin/ "Permalink to this headline")

ユーザクォータを設定したら、それを有効にすることができます。例：

```
radosgw-admin quota enable --quota-scope=user --uid=<uid>
```

有効になっているユーザークオータを無効にすることができます。例：

```
radosgw-admin quota disable --quota-scope=user --uid=<uid>
```

### Set Bucket Quota[¶](https://docs.ceph.com/en/pacific/radosgw/admin/ "Permalink to this headline")

バケットクォータは、指定されたuidが所有するバケットに適用されます。ユーザから独立しています。

```
radosgw-admin quota set --uid=<uid> --quota-scope=bucket [--max-objects=<num objects>] [--max-size=<max size]
```

num objectsと/ max sizeに負の値がある場合、当該クォータ属性のチェックが無効であることを意味します。

### Enable/Disable Bucket Quota[¶](https://docs.ceph.com/en/pacific/radosgw/admin/ "Permalink to this headline")

バケットクォータを設定したら、それを有効にすることができます。例：

```
radosgw-admin quota enable --quota-scope=bucket --uid=<uid>
```

有効になっているバケットクォータを無効にすることができます。例：

```
radosgw-admin quota disable --quota-scope=bucket --uid=<uid>
```

### Get Quota Settings[¶](https://docs.ceph.com/en/pacific/radosgw/admin/ "Permalink to this headline")

各ユーザのクォータ設定は、ユーザ情報APIでアクセスできます。CLIインターフェイスでユーザクォータ設定情報を読み込むには、以下を実行します：

```
radosgw-admin user info --uid=<uid>
```

### Update Quota Stats[¶](https://docs.ceph.com/en/pacific/radosgw/admin/ "Permalink to this headline")

クォータ統計は非同期で更新されます。すべてのユーザとすべてのバケットのクォータ統計情報を手動で更新して、最新のクォータ統計情報を取得することができます。

```
radosgw-admin user stats --uid=<uid> --sync-stats
```

### Get User Usage Stats[¶](https://docs.ceph.com/en/pacific/radosgw/admin/ "Permalink to this headline")

ユーザがどれだけクォータを消費したかを確認するには、次のように実行します：

```
radosgw-admin user stats --uid=<uid>
```

注意：最新のデータを受け取るには、radosgw\-admin user statsを\-\-sync\-statsオプション付きで実行する必要があります。

### Default Quotas[¶](https://docs.ceph.com/en/pacific/radosgw/admin/ "Permalink to this headline")

configでデフォルトクォータを設定することができます。 これらのデフォルトは、新しいユーザを作成するときに使用され、既存のユーザーには影響を与えません。関連するデフォルトクォータが config で設定されている場合、そのクォータが新しいユーザに設定され、そのクォータが有効になります。 [Ceph Object Gateway Config Reference](https://docs.ceph.com/en/pacific/radosgw/config-ref/)のrgw bucket default quota max objects、rgw bucket default quota max size、rgw user default quota max objects、およびrgw user default quota max sizeを参照してください。

### Quota Cache[¶](https://docs.ceph.com/en/pacific/radosgw/admin/ "Permalink to this headline")

クォータ統計は、各RGWインスタンスにキャッシュされます。 複数のインスタンスがある場合、各インスタンスでクォータに関する見解が異なるため、キャッシュによってクォータが完全に実施されない可能性があります。 これを制御するオプションは、rgw bucket quota ttl、rgw user quota bucket sync interval、rgw user quota sync intervalです。 これらの値が高いほど、クォータ操作は効率的になりますが、複数のインスタンスはより同期しなくなります。 これらの値が低いほど、複数のインスタンスがより完璧に近い強制力を持つようになります。 3つとも0の場合、クォータキャッシングは事実上無効となり、複数のインスタンスでクォータが完全に執行されることになります。 [Ceph Object Gateway Config Reference](https://docs.ceph.com/en/pacific/radosgw/config-ref/)を参照してください。

### Reading / Writing Global Quotas[¶](https://docs.ceph.com/en/pacific/radosgw/admin/ "Permalink to this headline")

period設定のグローバルクォータ設定の読み書きができます。グローバルクォータ設定を表示するには：

```
radosgw-admin global quota get
```

グローバルクォータ設定は、global quotaのquota set、quota enable、quota disableコマンド対応部分で操作することができます。

```
radosgw-admin global quota set --quota-scope bucket --max-objects 1024
radosgw-admin global quota enable --quota-scope bucket
```

注：マルチサイト構成で、レルムとperiodが存在する場合、グローバルクォータへの変更はperiod update \-\-commitを使用してコミットされなければなりません。periodが存在しない場合、変更を有効にするために rados ゲートウェイを再起動する必要があります。

## Usage[¶](https://docs.ceph.com/en/pacific/radosgw/admin/ "Permalink to this headline")

Ceph Object Gatewayは、各ユーザの使用状況をログに記録します。日付の範囲内でユーザの使用状況を追跡することもできます。

* ceph.confの\[client.rgw\]セクションにrgw enable usage log=trueを追加し、radosgwサービスを再起動します。

オプションは以下の通りです：

* **Start Date:**
* **End Date:**
* **Log Entries:**

注：時刻は分・秒単位で指定できますが、1時間の分解能で保存されます。

### Show Usage[¶](https://docs.ceph.com/en/pacific/radosgw/admin/ "Permalink to this headline")

使用量の統計を表示するには、usage show を指定します。特定のユーザの使用量を表示するには、ユーザIDを指定する必要があります。また、開始日、終了日、ログを表示するかどうかも指定できます：

```
radosgw-admin usage show --uid=johndoe --start-date=2012-03-01 --end-date=2012-04-01
```

また、ユーザIDを省略し、全ユーザの利用情報のサマリーを表示することも可能です。

```
radosgw-admin usage show --show-log-entries=false
```

### Trim Usage[¶](https://docs.ceph.com/en/pacific/radosgw/admin/ "Permalink to this headline")

頻繁に使用すると、usageログがストレージスペースを占有する可能性があります。 全ユーザまたは特定ユーザーのusageログをトリミングできます。 トリム操作の日付範囲を指定することもできます。

```
radosgw-admin usage trim --start-date=2010-01-01 --end-date=2010-12-31
radosgw-admin usage trim --uid=johndoe
radosgw-admin usage trim --uid=johndoe --end-date=2013-12-31
```
