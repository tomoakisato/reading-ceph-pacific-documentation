# 462: Archived Releases — Ceph Documentation
      [Report a Documentation Bug](https://docs.ceph.com/en/pacific/releases/archived-index/) 
 # Archived Releases[¶](https://docs.ceph.com/en/pacific/releases/archived-index/ "Permalink to this headline")

* [v13.2.10 Mimic](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v13.2.9 Mimic](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v13.2.8 Mimic](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v13.2.7 Mimic](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v13.2.6 Mimic](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v13.2.5 Mimic](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v13.2.4 Mimic](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v13.2.3 Mimic](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v13.2.2 Mimic](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v13.2.1 Mimic](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v13.2.0 Mimic](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v12.2.13 Luminous](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v12.2.12 Luminous](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v12.2.11 Luminous](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v12.2.10 Luminous](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v12.2.9 Luminous](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v12.2.8 Luminous](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v12.2.7 Luminous](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v12.2.6 Luminous](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v12.2.5 Luminous](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v12.2.4 Luminous](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v12.2.3 Luminous](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v12.2.2 Luminous](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v12.2.1 Luminous](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v12.2.0 Luminous](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v11.2.1 Kraken](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v11.2.0 Kraken](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v11.0.2 Kraken](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v10.2.11 Jewel](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v10.2.10 Jewel](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v10.2.9 Jewel](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v10.2.8 Jewel](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v10.2.7 Jewel](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v10.2.6 Jewel](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v10.2.5 Jewel](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v10.2.4 Jewel](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v10.2.3 Jewel](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v10.2.2 Jewel](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v10.2.1 Jewel](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v10.2.0 Jewel](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v9.2.1 Infernalis](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v9.2.0 Infernalis](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v9.1.0 Infernalis release candidate](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v9.0.3](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v9.0.2](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v9.0.1](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v9.0.0](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.94.10 Hammer](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.94.9 Hammer](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.94.8 Hammer](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.94.7 Hammer](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.94.6 Hammer](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.94.5 Hammer](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.94.4 Hammer](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.94.3 Hammer](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.94.2 Hammer](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.94.1 Hammer](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.94 Hammer](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.93](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.92](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.91](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.90](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.89](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.88](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.87.2 Giant](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.87.1 Giant](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.87 Giant](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.86](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.85](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.84](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.83](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.82](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.81](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.80.11 Firefly](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.80.10 Firefly](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.80.9 Firefly](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.80.8 Firefly](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.80.7 Firefly](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.80.6 Firefly](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.80.5 Firefly](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.80.4 Firefly](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.80.3 Firefly](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.80.2 Firefly](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.80.1 Firefly](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.80 Firefly](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.79](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.78](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.77](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.76](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.75](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.74](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.73](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.72.3 Emperor \(pending release\)](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.72.2 Emperor](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.72.1 Emperor](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.72 Emperor](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.71](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.70](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.69](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.68](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.67.12 “Dumpling” \(draft\)](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.67.11 “Dumpling”](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.67.10 “Dumpling”](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.67.9 “Dumpling”](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.67.8 “Dumpling”](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.67.7 “Dumpling”](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.67.6 “Dumpling”](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.67.5 “Dumpling”](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.67.4 “Dumpling”](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.67.3 “Dumpling”](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.67.2 “Dumpling”](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.67.1 “Dumpling”](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.67 “Dumpling”](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.66](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.65](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.64](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.63](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.62](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.61.9 “Cuttlefish”](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.61.8 “Cuttlefish”](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.61.7 “Cuttlefish”](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.61.6 “Cuttlefish”](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.61.5 “Cuttlefish”](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.61.4 “Cuttlefish”](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.61.3 “Cuttlefish”](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.61.2 “Cuttlefish”](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.61.1 “Cuttlefish”](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.61 “Cuttlefish”](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.60](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.59](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.58](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.57](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.56.7 “bobtail”](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.56.6 “bobtail”](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.56.5 “bobtail”](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.56.4 “bobtail”](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.56.3 “bobtail”](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.56.2 “bobtail”](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.56.1 “bobtail”](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.56 “bobtail”](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.54](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.48.3 “argonaut”](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.48.2 “argonaut”](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.48.1 “argonaut”](https://docs.ceph.com/en/pacific/releases/archived-index/)
* [v0.48 “argonaut”](https://docs.ceph.com/en/pacific/releases/archived-index/)

  
