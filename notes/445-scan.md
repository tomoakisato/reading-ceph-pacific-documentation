# 445: scan

**クリップソース:** [445: scan — Ceph Documentation](https://docs.ceph.com/en/pacific/ceph-volume/lvm/scan/)

# scan[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/scan/ "Permalink to this headline")

このサブコマンドは、システムの論理ボリュームとそのタグを調べることで、ツールによって以前にセットアップされたCephボリュームを検出できるようにするものです。

[prepare](https://docs.ceph.com/en/pacific/ceph-volume/lvm/prepare/#ceph-volume-lvm-prepare)プロセスの一環として、論理ボリュームには重要な情報を含むいくつかのタグが割り当てられます。

注：このサブコマンドは未実装です。
