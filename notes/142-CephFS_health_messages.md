# 142: CephFS health messages

**クリップソース:** [142: CephFS health messages — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/health-messages/)

# CephFS health messages[¶](https://docs.ceph.com/en/pacific/cephfs/health-messages/ "Permalink to this headline")

## Cluster health checks[¶](https://docs.ceph.com/en/pacific/cephfs/health-messages/ "Permalink to this headline")

Cephモニタデーモンは、ファイルシステムマップ構造\(とエンクローズするMDSマップ\)の状態に応答してヘルスメッセージを生成します。

Message: **mds rank\(s\) _ranks_ have failed** 

Description: 1つ以上のMDSランクが現在MDSデーモンに割り当てられていません。適切な代替デーモンが起動するまで、クラスタは回復しません。

Message: **mds rank\(s\) _ranks_ are damaged**

Description: 1つまたは複数のMDSのランクで、保存されているメタデータに深刻な損傷が発生し、修復されるまで再起不能となりました。

Message: **mds cluster is degraded**

Description: 1つまたは複数のMDSのランクが現在稼働していない場合、クライアントはこの状況が解決されるまでメタデータIOを一時停止することができます。 これには、ランクが故障または損傷している場合も含まれ、さらに、MDS上で動作しているがまだアクティブな状態になっていないランク（例：現在_replay_状態にあるランク）も含まれます。

Message: **mds _names_ are laggy**

Description: 指定されたMDSデーモンは、mds\_beacon\_interval（デフォルト4秒）ごとにビーコンメッセージを送信することになっていますが、少なくともmds\_beacon\_grace（デフォルト15秒）の間モニターへのビーコンメッセージの送信に失敗しています。 デーモンがクラッシュした可能性があります。 Cephモニタは、遅延しているデーモンがあれば、自動的にstandbyデーモンに置き換えます。

Message: **insufficient standby daemons available**

Description: 1つまたは複数のファイルシステムが、ある数のstandbyデーモン（standby\-replay中のデーモンを含む）を利用できるように構成されていますが、クラスタのstandbyデーモンが十分ではありません。standby\-replayに参加していないstandbyデーモンは、どのファイルシステムに対してもカウントされます（つまり、それらは重複する可能性があります）。この警告は、ceph fs set \<fs\> standby\_count\_wanted \<count\> を設定することにより、設定することができます。 無効化する場合は、countに0を指定します。

## Daemon\-reported health checks[¶](https://docs.ceph.com/en/pacific/cephfs/health-messages/ "Permalink to this headline")

MDSデーモンはさまざまな望まない状況を識別し、ceph statusの出力でオペレータに表示することができます。これらの状況には人間が読めるメッセージがあり、さらにMDS\_で始まる一意のコードがあります。

ceph health detail は、状況の詳細を表示します。以下は、MDSに関連するパフォーマンス問題が発生しているクラスタからの典型的なヘルスレポートです：

```
$ ceph health detail
HEALTH_WARN 1 MDSs report slow metadata IOs; 1 MDSs report slow requests
MDS_SLOW_METADATA_IO 1 MDSs report slow metadata IOs
   mds.fs-01(mds.0): 3 slow metadata IOs are blocked > 30 secs, oldest blocked for 51123 secs
MDS_SLOW_REQUEST 1 MDSs report slow requests
   mds.fs-01(mds.0): 5 slow requests are blocked > 30 secs
```

例えば、MDS\_SLOW\_REQUESTは、リクエストの完了に長い時間がかかっている状況を表す一意のコードです。また、以下の説明では、その深刻度と、これらの遅いリクエストを処理しているMDSデーモンを示しています。

このページでは、MDSデーモンが発するヘルスチェックの一覧を示します。他のデーモンによるチェックについては、[ヘルスチェック](https://docs.ceph.com/en/pacific/rados/operations/health-checks/#health-checks)を参照してください。

* Message
    **“Behind on trimming…”**
    Description
    CephFSは、ログセグメントに分割されたメタデータジャーナルを維持します。 ジャーナルの長さ\(セグメント数\)はmds\_log\_max\_segmentsの設定によって制御され、セグメント数がこの設定を超えると、MDSは最も古いセグメントを削除\(トリミング\)できるようにメタデータの書き戻しを開始します。 この書き戻しが遅すぎる場合、またはソフトウェアのバグによりトリミングができない場合、このヘルスメッセージが表示されることがあります。 このメッセージが表示される閾値は、設定オプションのmds\_log\_warn\_factorで制御され、デフォルトは2.0です。
    MDS\_TRIM
* Message
    **“Client _name_ failing to respond to capability release”**
    Description
    CephFSクライアントはMDSからCAPSを発行されますが、これはロックのようなものです。 他のクライアントがアクセスを必要とする場合など、MDSはクライアントにCAPSのリリースを要求することがあります。 クライアントが応答しない場合やバグがある場合、この要求が迅速に実行されなかったり、まったく実行されなかったりすることがあります。 このメッセージは、クライアントが応答するまでにsession\_timeout（デフォルトでは60秒）を超える時間がかかった場合に表示されます。
    MDS\_HEALTH\_CLIENT\_LATE\_RELEASE, MDS\_HEALTH\_CLIENT\_LATE\_RELEASE\_MANY
* Message
    **“Client _name_ failing to respond to cache pressure”**
    Description
    クライアントはメタデータキャッシュを維持します。 クライアントキャッシュ内のアイテム（iノードなど）もMDSキャッシュに固定されるため、MDSがキャッシュを縮小する必要がある場合（mds\_cache\_memory\_limit内にとどまるため）、クライアントにメッセージを送信してキャッシュも縮小します。 クライアントが応答しないかバグがある場合、これによりMDSがキャッシュ制限内に適切に留まらず、最終的にメモリが不足してクラッシュする可能性があります。 このメッセージは、クライアントが最後のmds\_recall\_warning\_decay\_rate秒以内にmds\_recall\_warning\_threshold個（mds\_recall\_max\_decay\_rateの半減期で減衰）を超えてCAPSリリースできなかった場合に表示されます。
    MDS\_CLIENT\_RECALL, MDS\_HEALTH\_CLIENT\_RECALL\_MANY
* Message
    **“Client _name_ failing to advance its oldest client/flush tid”**
    Description
    CephFSクライアント\-MDSプロトコルは、どのクライアント要求が完全に完了し、MDSが忘れてもよいかをMDSに知らせるため、最も古いtidというフィールドを使用します。 バグを持つクライアントがこのフィールドを進めない場合、MDSがクライアント要求によって使用されたリソースを適切にクリーンアップできない可能性があります。 このメッセージは、MDS側では完了しているが、クライアントの最も古いtidの値にまだ計上されていないリクエストが、max\_completed\_requests（デフォルト100000）より多くあるように見える場合に表示されます。
    MDS\_CLIENT\_OLDEST\_TID, MDS\_CLIENT\_OLDEST\_TID\_MANY
* Message
    **“Metadata damage detected”**
    Description
    メタデータプールからの読み取り中に、メタデータの破損または欠落が発生しました。 このメッセージは、損傷したサブツリーへのクライアントアクセスはIOエラーを返しますが、損傷がMDSの動作を継続するために十分に分離されたことを示します。 損傷の詳細を取得するには、damage ls 管理ソケットコマンドを使用します。 このメッセージは、損傷が発生するとすぐに表示されます。
    MDS\_DAMAGE
* Message
    **“MDS in read\-only mode”**
    Description
    MDSはリードオンリーモードに移行し、クライアントがメタデータを変更しようとするとEROFSエラーコードを返します。 MDSは、メタデータプールへの書き込み中に書き込みエラーが発生した場合、または管理者がforce\_readonly 管理ソケットコマンドを使用して強制的にリードオンリーモードに移行します。
    MDS\_HEALTH\_READ\_ONLY
* Message
    **“_N_ slow requests are blocked”**
    Description
    1つ以上のクライアント要求が迅速に完了していません。MDSの動作が非常に遅いか、RADOSクラスタがジャーナル書き込みを迅速に認識しないか、またはバグがあることを示しています。ops 管理ソケットコマンドを使用して、未完了のメタデータ操作を一覧表示します。このメッセージは、クライアントからの要求がmds\_op\_complaint\_time（デフォルトの30秒）よりも長い時間かかった場合に表示されます。
    MDS\_SLOW\_REQUEST
* Message
    **“Too many inodes in cache”**
    Description
    MDSが、管理者によって設定された制限に準拠するようにキャッシュを切り詰めることに成功していません。 MDSのキャッシュが大きくなりすぎると、デーモンが使用可能なメモリを使い果たし、クラッシュする可能性があります。 デフォルトでは、このメッセージは実際のキャッシュサイズ（メモリ内）がmds\_cache\_memory\_limit（デフォルト1GB）より50%以上大きい場合に表示されます。mds\_health\_cache\_threshold を修正して、警告の比率を設定します。
    MDS\_CACHE\_OVERSIZED
