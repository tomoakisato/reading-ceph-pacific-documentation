# 181: Ceph Block Device

**クリップソース:** [181: Ceph Block Device — Ceph Documentation](https://docs.ceph.com/en/pacific/rbd/)

# Ceph Block Device[¶](https://docs.ceph.com/en/pacific/rbd/ "Permalink to this headline")

ブロックとは、バイトの連続（多くは512バイト）のことです。ブロックベースのストレージインターフェイスは、HDD、SSD、CD、フロッピーディスク、さらにはテープなどのメディアにデータを保存するための、一般的な方法です。ブロックデバイスインターフェイスのユビキタス性は、Cephを含む大容量データストレージとの相互作用に最適です。

Cephブロックデバイスはシンプロビジョニングされ、サイズ変更可能で、複数のOSD上にデータをストライピングして保存します。 Cephブロックデバイスは、スナップショット、レプリケーション、強力な一貫性などのRADOSの機能を活用します。Cephブロックストレージクライアントは、カーネルモジュールまたはlibrbdライブラリを通じてCephクラスタと通信します。
![274b3a8c6be03027e4cbcc949e348d05010b41856c98f7a97992ff7bacfc27da.png](image/274b3a8c6be03027e4cbcc949e348d05010b41856c98f7a97992ff7bacfc27da.png)

注：カーネルモジュールはLinuxのページキャッシングを使用することができます。librbdベースのアプリケーションの場合、Cephは[RBDキャッシング](https://docs.ceph.com/en/pacific/rbd/rbd-config-ref/)をサポートします。

Cephのブロックデバイスは、[カーネルモジュール](https://docs.ceph.com/en/pacific/rbd/rbd-ko/)、あるいは[QEMU](https://docs.ceph.com/en/pacific/rbd/qemu-rbd/)などのKVM、[OpenStack](https://docs.ceph.com/en/pacific/rbd/rbd-openstack)や[CloudStack](https://docs.ceph.com/en/pacific/rbd/rbd-cloudstack)などのクラウドベースコンピューティングシステムに膨大なスケーラビリティで高いパフォーマンスを提供し、libvirtやQEMUに依存してCephブロックデバイスと統合しています。同じクラスタを使用して、[Ceph RADOS Gateway](https://docs.ceph.com/en/pacific/radosgw/#object-gateway)、[Ceph File System](https://docs.ceph.com/en/pacific/cephfs/#ceph-file-system)、Cephブロックデバイスを同時に操作することができます。

重要: Ceph Block Devicesを使用するには、実行中のCephクラスタにアクセスする必要があります。

* [Basic Commands](https://docs.ceph.com/en/pacific/rbd/)
* [Operations](https://docs.ceph.com/en/pacific/rbd/)
    * [Snapshots](https://docs.ceph.com/en/pacific/rbd/)
    * [Exclusive Locking](https://docs.ceph.com/en/pacific/rbd/)
    * [Mirroring](https://docs.ceph.com/en/pacific/rbd/)
    * [Live\-Migration](https://docs.ceph.com/en/pacific/rbd/)
    * [Persistent Read\-only Cache](https://docs.ceph.com/en/pacific/rbd/)
    * [Persistent Write\-back Cache](https://docs.ceph.com/en/pacific/rbd/)
    * [Encryption](https://docs.ceph.com/en/pacific/rbd/)
    * [Config Settings \(librbd\)](https://docs.ceph.com/en/pacific/rbd/)
    * [RBD Replay](https://docs.ceph.com/en/pacific/rbd/)
* [Integrations](https://docs.ceph.com/en/pacific/rbd/)
    * [Kernel Modules](https://docs.ceph.com/en/pacific/rbd/)
    * [QEMU](https://docs.ceph.com/en/pacific/rbd/)
    * [libvirt](https://docs.ceph.com/en/pacific/rbd/)
    * [Kubernetes](https://docs.ceph.com/en/pacific/rbd/)
    * [OpenStack](https://docs.ceph.com/en/pacific/rbd/)
    * [CloudStack](https://docs.ceph.com/en/pacific/rbd/)
    * [LIO iSCSI Gateway](https://docs.ceph.com/en/pacific/rbd/)
    * [Windows](https://docs.ceph.com/en/pacific/rbd/)
* [Manpages](https://docs.ceph.com/en/pacific/rbd/)
    * [rbd](https://docs.ceph.com/en/pacific/rbd/)
    * [rbd\-fuse](https://docs.ceph.com/en/pacific/rbd/)
    * [rbd\-nbd](https://docs.ceph.com/en/pacific/rbd/)
    * [rbd\-ggate](https://docs.ceph.com/en/pacific/rbd/)
    * [rbd\-map](https://docs.ceph.com/en/pacific/rbd/)
    * [ceph\-rbdnamer](https://docs.ceph.com/en/pacific/rbd/)
    * [rbd\-replay\-prep](https://docs.ceph.com/en/pacific/rbd/)
    * [rbd\-replay](https://docs.ceph.com/en/pacific/rbd/)
    * [rbd\-replay\-many](https://docs.ceph.com/en/pacific/rbd/)
* [APIs](https://docs.ceph.com/en/pacific/rbd/)
    * [librbd \(Python\)](https://docs.ceph.com/en/pacific/rbd/)
