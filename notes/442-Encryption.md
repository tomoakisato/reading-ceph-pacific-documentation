# 442: Encryption

**クリップソース:** [442: Encryption — Ceph Documentation](https://docs.ceph.com/en/pacific/ceph-volume/lvm/encryption/)

# Encryption[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/encryption/ "Permalink to this headline")

OSD作成時に\-\-dmcryptフラグを指定することで、dmcryptを使用して論理ボリュームを暗号化することができます。ceph\-volumeは、論理ボリュームの暗号化をセットアップする方法について、一貫性があり堅牢であるように、多少意見を述べます。

ceph\-volume lvmは以下の制約に従います：

* LUKS（バージョン1）のみを使用
* 論理ボリュームは暗号化されるが、その下にあるPV（物理ボリューム）は暗号化されない
* パーティションなどの非LVMデバイスも、同じOSDキーで暗号化される

## LUKS[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/encryption/ "Permalink to this headline")

現在、LUKSには1と2の2つのバージョンがあります。バージョン2は実装が少し簡単ですが、Cephがサポートするすべてのディストロで広く利用できるわけではありません。LUKS 1はLUKS 2に取って代わられる予定はないため、できるだけ幅広いサポートを得るために、ceph\-volumeはLUKSバージョン1を使用します。

注：LUKSのバージョン1を「LUKS」、バージョン2を「LUKS2」と表記します。

## LUKS on LVM[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/encryption/ "Permalink to this headline")

暗号化は、既存の論理ボリュームの上で行われます（物理デバイスの暗号化とは異なります）。任意の1つの論理ボリュームを暗号化し、他のボリュームは暗号化しないままにしておくことができます。この方法では、LVが作成されると暗号化が行われるため、論理ボリュームのセットアップを柔軟に行うことも可能です。

## Workflow[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/encryption/ "Permalink to this headline")

OSDのセットアップ時に、シークレットキーが作成され、そのキーがログに取り込まれないように、stdinとしてJSONフォーマットでモニタに渡されます。

JSONのペイロードは次のようなものです：

```
{
    "cephx_secret": CEPHX_SECRET,
    "dmcrypt_key": DMCRYPT_KEY,
    "cephx_lockbox_secret": LOCKBOX_SECRET,
}
```

キーの命名規則は厳格で、ceph\-diskが使用していたハードコードされた\(レガシー\)名前のように命名されます。

* cephx\_secret : 認証に使用されるcephxキー
* dmcrypt\_key : 暗号化されたデバイスをunlockするための秘密（プライベート）キー
* cephx\_lockbox\_secret : dmcrypt\_keyを取得するために使用する認証キー。ceph\-diskにlockboxという名前の非暗号化パーティションがあり、公開鍵や他のOSDメタデータを格納するために使用されていたため、lockboxと名付けられました。

この命名規則が厳格なのは、モニタがこれらのキー名を使用していたceph\-diskによる命名規則をサポートしていたためです。互換性を維持し、ceph\-diskが壊れないようにするため、ceph\-volumeは同じ命名規則を使用しますが、新しい暗号化ワークフローでは意味を持ちません。

準備段階で、[filestore](https://docs.ceph.com/en/pacific/glossary/#term-filestore)または[bluestore](https://docs.ceph.com/en/pacific/glossary/#term-bluestore)を使用してOSDを設定する一般的な手順の後、論理ボリュームはデバイスの状態（encryptedまたはdecrypted）に関係なく、起動可能な状態で残されます。

アクティベーション時に、論理ボリュームの暗号が解除され、プロセスが正しく完了するとOSDが開始されます。

新しいOSDを作成するための暗号化ワークフローのサマリー：

1. OSDが作成され、lockboxとdmcryptの両方の鍵が作成され、JSONでモニタに送信され、暗号化されたOSDであることが示される
2. 補完デバイス（journal、db、walなど）は、同じOSDキーで作成され、暗号化される。キーはOSDのLVMメタデータに格納される
3. デバイスがマウントされていることを確認し、モニタからdmcrypt秘密鍵を取得し、OSDが開始される前に復号化することでアクティベーションが継続される
