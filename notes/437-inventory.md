# 437: inventory

**クリップソース:** [437: inventory — Ceph Documentation](https://docs.ceph.com/en/pacific/ceph-volume/inventory/)

# inventory[¶](https://docs.ceph.com/en/pacific/ceph-volume/inventory/ "Permalink to this headline")

inventory サブコマンドは、ホストのディスク・インベントリを照会し、各物理デバイスのハードウェア情報とメタデータを提供します。

このコマンドはデフォルトで、全物理ディスクの人間が読むことのできるレポートを返します。

このレポートをプログラムで利用する場合は、\-\-format jsonを渡すと、JSON形式のレポートが生成されます。このレポートには、ディスクのメタデータ\(モデルやサイズなど\)、論理ボリュームとそれらがcephで使用されているかどうか、ディスクがcephで使用可能かどうか、使用できない理由など、物理ドライブに関する広範な情報が含まれます。

デバイスパスを指定することで、デバイスに関する広範な情報をプレーンとjsonの両形式で報告することができます。
