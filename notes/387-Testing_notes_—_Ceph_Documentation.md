# 387: Testing notes — Ceph Documentation

 # Testing notes[¶](https://docs.ceph.com/en/pacific/dev/testing/ "Permalink to this headline")

## build\-integration\-branch[¶](https://docs.ceph.com/en/pacific/dev/testing/ "Permalink to this headline")

### Setup[¶](https://docs.ceph.com/en/pacific/dev/testing/ "Permalink to this headline")

1. Create a github token at [https://github.com/settings/tokens](https://docs.ceph.com/en/pacific/dev/testing/)and put it in `~/.github_token`. Note that only the`public_repo` under the `repo` section needs to be checked.
2. Create a ceph repo label wip\-yourname\-testing if you don’t already have one at [https://github.com/ceph/ceph/labels](https://docs.ceph.com/en/pacific/dev/testing/).
3. Create the `ci` remote:
    ```
    git remote add ci git@github.com:ceph/ceph-ci
    ```

### Using[¶](https://docs.ceph.com/en/pacific/dev/testing/ "Permalink to this headline")

1. Tag some subset of needs\-qa commits with your label \(usually wip\-yourname\-testing\).
2. Create the integration branch:
    ```
    git checkout master
    git pull
    ../src/script/build-integration-branch wip-yourname-testing
    ```
3. Smoke test:
    ```
    make && ctest -j12
    ```
4. Push to ceph\-ci:
    ```
    git push ci $(git rev-parse --abbrev-ref HEAD)
    ```
