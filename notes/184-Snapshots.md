# 184: Snapshots

**クリップソース:** [184: Snapshots — Ceph Documentation](https://docs.ceph.com/en/pacific/rbd/rbd-snapshot/)

# Snapshots[¶](https://docs.ceph.com/en/pacific/rbd/rbd-snapshot/ "Permalink to this headline")

スナップショットとは、特定の時点におけるイメージの読み取り専用の論理コピーのことで、チェックポイントと呼ばれます。Cephブロックデバイスの高度な機能の1つは、イメージのスナップショットを作成し、ポイントインタイムの状態履歴を保持できることです。Cephはスナップショットのレイヤリングもサポートしており、イメージ（VMイメージなど）のクローンを迅速かつ容易に作成することができます。Cephブロックデバイスのスナップショットは、rbdコマンドと、[QEMU](https://docs.ceph.com/en/pacific/rbd/qemu-rbd/)、[libvirt](https://docs.ceph.com/en/pacific/rbd/libvirt/)、[OpenStack](https://docs.ceph.com/en/pacific/rbd/rbd-openstack/)、[CloudStack](https://docs.ceph.com/en/pacific/rbd/rbd-cloudstack/)などの上位インターフェースを使用して管理されます。

重要: RBDスナップショットを使用するには、Cephクラスタが稼動している必要があります。

注：RBDはイメージ（ボリューム）内のファイルシステムについては知らないため、スナップショットはマウント（アタッチ）するオペレーティングシステム内で調整されない限り、クラッシュコンシステントではありません。そのため、スナップショットを作成する前にI/Oを一時停止または停止することをお勧めします。ボリュームにファイルシステムが含まれている場合、スナップショットを取得する前に内部的に一貫性のある状態である必要があります。 一貫性のない時点で取得されたスナップショットは、その後のマウントの前にfsck実行が必要になる場合があります。 I/Oを停止するには、fsfreezeコマンドを使用します。詳細は fsfreeze\(8\) man page を参照してください。仮想マシンでは、qemu\-guest\-agent を使用して、スナップショットを作成する際にファイルシステムを自動的にフリーズさせることができます。
![7cbd9275f3de1b06aaf936799f86e1a32d9229c6d29a9ec8e0e330369df429b4.png](image/7cbd9275f3de1b06aaf936799f86e1a32d9229c6d29a9ec8e0e330369df429b4.png)

## Cephx Notes[¶](https://docs.ceph.com/en/pacific/rbd/rbd-snapshot/ "Permalink to this headline")

[cephx](https://docs.ceph.com/en/pacific/rados/configuration/auth-config-ref/)認証が有効な場合\(デフォルトで有効\)、ユーザ名またはID、対応する鍵が含まれるキーリングへのパスを指定する必要があります。詳細については、「[ユーザ管理](https://docs.ceph.com/en/pacific/rados/operations/user-management/#user-management)」を参照してください。これらのパラメータの再入力を避けるために、CEPH\_ARGS環境変数を設定することもできます。

```
rbd --id {user-ID} --keyring=/path/to/secret [commands]
rbd --name {username} --keyring=/path/to/secret [commands]
```

For example:

```
rbd --id admin --keyring=/etc/ceph/ceph.keyring [commands]
rbd --name client.admin --keyring=/etc/ceph/ceph.keyring [commands]
```

Tip： ユーザーとシークレットをCEPH\_ARGS環境変数に追加して、毎回入力する必要がないようにします。

## Snapshot Basics[¶](https://docs.ceph.com/en/pacific/rbd/rbd-snapshot/ "Permalink to this headline")

以下の手順は、rbdコマンドを使用してスナップショットを作成、リストアップ、削除する方法を示しています。

### Create Snapshot[¶](https://docs.ceph.com/en/pacific/rbd/rbd-snapshot/ "Permalink to this headline")

rbdでスナップショットを作成するには、snap createオプション、プール名、イメージ名を指定します。

```
rbd snap create {pool-name}/{image-name}@{snap-name}
```

For example:

```
rbd snap create rbd/foo@snapname
```

### List Snapshots[¶](https://docs.ceph.com/en/pacific/rbd/rbd-snapshot/ "Permalink to this headline")

あるイメージのスナップショットを一覧表示するには、プール名とイメージ名を指定します。

```
rbd snap ls {pool-name}/{image-name}
```

For example:

```
rbd snap ls rbd/foo
```

### Rollback Snapshot[¶](https://docs.ceph.com/en/pacific/rbd/rbd-snapshot/ "Permalink to this headline")

rbdでスナップショットにロールバックするには、snap rollbackオプション、プール名、イメージ名、スナップ名を指定します。

```
rbd snap rollback {pool-name}/{image-name}@{snap-name}
```

For example:

```
rbd snap rollback rbd/foo@snapname
```

注：スナップショットへのロールバックは、スナップショットのデータで現在のバージョンのイメージを上書きすることを意味します。ロールバックの実行にかかる時間は、イメージのサイズに比例して長くなります。スナップショットからのクローン作成は、スナップショットへのロールバックよりも高速であり、既存の状態に戻す方法として推奨されます。

### Delete a Snapshot[¶](https://docs.ceph.com/en/pacific/rbd/rbd-snapshot/ "Permalink to this headline")

rbdでスナップショットを削除するには、snap rmサブコマンド、プール名、イメージ名、スナップ名を指定します。

```
rbd snap rm {pool-name}/{image-name}@{snap-name}
```

For example:

```
rbd snap rm rbd/foo@snapname
```

注：Ceph OSDは非同期にデータを削除するため、スナップショットを削除しても、基盤となるOSDの容量がすぐに解放されるわけではありません。

### Purge Snapshots[¶](https://docs.ceph.com/en/pacific/rbd/rbd-snapshot/ "Permalink to this headline")

rbdでイメージの全スナップショットを削除するには、snap purgeサブコマンドとイメージ名を指定します。

```
rbd snap purge {pool-name}/{image-name}
```

For example:

```
rbd snap purge rbd/foo
```

## Layering[¶](https://docs.ceph.com/en/pacific/rbd/rbd-snapshot/ "Permalink to this headline")

Cephは、ブロックデバイスのスナップショットから多数のコピーオンライト（COW）クローンを作成する機能をサポートしています。スナップショットのレイヤリングにより、Cephブロックデバイスクライアントは非常に迅速にイメージを作成することができます。たとえば、Linux VMが書き込まれたブロックデバイスイメージを作成し、イメージをスナップショットし、スナップショットを保護し、コピーオンライトクローンを好きなだけ作成することができます。スナップショットは読み取り専用なので、スナップショットのクローンを作成するとセマンティクスが単純化され、クローンを高速に作成することができます。
![1e283ff0c1314d9472ab5fd710c670afc5f162869e630e5a32911e7942f52a76.png](image/1e283ff0c1314d9472ab5fd710c670afc5f162869e630e5a32911e7942f52a76.png)

注：「parent」および「child」という用語は、Cephブロックデバイスのスナップショット（親）と、そのスナップショットからクローンされた対応するイメージ（子）を指します。これらの用語は、下記のコマンドラインの使用において重要です。

各クローンイメージ（childイメージ）はparentイメージへの参照を保存しており、これによりクローンイメージはparentスナップショットを開いて読み出すことができます。

スナップショットのCOWクローンは、他のCephブロックデバイスのイメージと全く同じように動作します。クローンイメージへの読み込み、書き込み、クローン作成、サイズ変更が可能です。クローンイメージには、特別な制限はありません。ただし、スナップショットのコピーオンライトクローンはスナップショットに依存するため、クローンを作成する前にスナップショットを保護する必要があります。次の図は、そのプロセスを表しています。

注: Cephは、RBDフォーマット2イメージ\(すなわち、rbd create\-\-image\-format 2で作成されたもの\)のクローニングのみをサポートします。 カーネルクライアントは、3.10リリース以降、クローンイメージをサポートしています。

### Getting Started with Layering[¶](https://docs.ceph.com/en/pacific/rbd/rbd-snapshot/ "Permalink to this headline")

Cephブロックデバイスのレイヤリングは簡単な作業です。イメージを持っている必要があります。イメージのスナップショットを作成する必要があります。スナップショットを保護する必要があります。これらの手順を実行したら、スナップショットのクローン作成を開始できます。
![5038fec5f0d1ea54596f536eb87ba81c913d488cef1a9c75944d74ff6137f143.png](image/5038fec5f0d1ea54596f536eb87ba81c913d488cef1a9c75944d74ff6137f143.png)

クローンイメージは、parentスナップショットへの参照を持ち、プールID、イメージID、スナップショットIDを含みます。プールIDを含むということは、あるプールから別のプールのイメージにスナップショットをクローンすることができることを意味します。

1. **Image Template:**
2. **Extended Template:**
3. **Template Pool:**
4. **Image Migration/Recovery:**

### Protecting a Snapshot[¶](https://docs.ceph.com/en/pacific/rbd/rbd-snapshot/ "Permalink to this headline")

クローンはparentスナップショットにアクセスします。ユーザが誤ってparentスナップショットを削除した場合、すべてのクローンが壊れます。データ損失を防ぐには、クローンを作成する前にスナップショットを保護する必要があります。

```
rbd snap protect {pool-name}/{image-name}@{snapshot-name}
```

For example:

```
rbd snap protect rbd/my-image@my-snapshot
```

注意：保護されたスナップショットを削除することはできません。

### Cloning a Snapshot[¶](https://docs.ceph.com/en/pacific/rbd/rbd-snapshot/ "Permalink to this headline")

スナップショットを複製するには、parentプール、イメージ、スナップショット、childプールとイメージの名前を指定する必要があります。スナップショットを複製する前に、スナップショットを保護する必要があります。

```
rbd clone {pool-name}/{parent-image}@{snap-name} {pool-name}/{child-image-name}
```

For example:

```
rbd clone rbd/my-image@my-snapshot rbd/new-image
```

注：あるプールから別のプールのイメージにスナップショットをクローンすることができます。たとえば、読み取り専用のイメージとスナップショットをテンプレートとしてあるプールで管理し、書き込み可能なクローンを別のプールで管理することができます。

### Unprotecting a Snapshot[¶](https://docs.ceph.com/en/pacific/rbd/rbd-snapshot/ "Permalink to this headline")

スナップショットを削除する前に、まずその保護を解除する必要があります。また、クローンからの参照を持つスナップショットを削除することはできません。スナップショットを削除する前に、スナップショットの各クローンを平坦化（flatten）する必要があります。

```
rbd snap unprotect {pool-name}/{image-name}@{snapshot-name}
```

For example:

```
rbd snap unprotect rbd/my-image@my-snapshot
```

### Listing Children of a Snapshot[¶](https://docs.ceph.com/en/pacific/rbd/rbd-snapshot/ "Permalink to this headline")

スナップショットのchildを一覧表示するには、以下を実行します。

```
rbd children {pool-name}/{image-name}@{snapshot-name}
```

For example:

```
rbd children rbd/my-image@my-snapshot
```

### Flattening a Cloned Image[¶](https://docs.ceph.com/en/pacific/rbd/rbd-snapshot/ "Permalink to this headline")

クローンイメージは、parentスナップショットへの参照を保持します。childクローンからparentスナップショットへの参照を取り除くと、スナップショットの情報がクローンにコピーされるため、効果的に画像を「平坦化」することができます。クローンを平坦化するのにかかる時間は、スナップショットのサイズによって増加します。スナップショットを削除するには、まずchildイメージを平坦化する必要があります。

```
rbd flatten {pool-name}/{image-name}
```

For example:

```
rbd flatten rbd/new-image
```

注：フラット化されたイメージにはスナップショットのすべての情報が含まれるため、フラット化されたイメージはレイヤードクローンよりも多くのストレージスペースを消費します。
