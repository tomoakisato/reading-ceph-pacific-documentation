# 151: Mount CephFS using FUSE

**クリップソース:** [151: Mount CephFS using FUSE — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/mount-using-fuse/)

# Mount CephFS using FUSE[¶](https://docs.ceph.com/en/pacific/cephfs/mount-using-fuse/ "Permalink to this headline")

[ceph\-fuse](https://docs.ceph.com/en/pacific/man/8/ceph-fuse/#options)は、CephFSをマウントする別の方法ですが、ユーザースペースにマウントします。 したがって、FUSEのパフォーマンスは比較的低くなる可能性がありますが、特にCephFSをアップグレードしている間は、FUSEクライアントをより管理しやすくすることができます。

## Prerequisites[¶](https://docs.ceph.com/en/pacific/cephfs/mount-using-fuse/ "Permalink to this headline")

### Complete General Prerequisites[¶](https://docs.ceph.com/en/pacific/cephfs/mount-using-fuse/ "Permalink to this headline")

カーネルマウントとFUSEマウントの両方で必要な前提条件は、[Mount CephFS: Prerequisites](https://docs.ceph.com/en/pacific/cephfs/mount-prerequisites)のページで確認してください。

### fuse.conf option[¶](https://docs.ceph.com/en/pacific/cephfs/mount-using-fuse/ "Permalink to this headline")

1. スーパーユーザ/ルートユーザ/システム管理者としてではなく、FUSEでCephをマウントする場合は、/etc/fuse.confにオプションuser\_allow\_otherを追加する必要があります（confのセクションなし）。

## Synopsis[¶](https://docs.ceph.com/en/pacific/cephfs/mount-using-fuse/ "Permalink to this headline")

一般的にFUSE経由でCephFSをマウントするコマンドは以下のようなものです：

```
ceph-fuse {mountpoint} {options}
```

## Mounting CephFS[¶](https://docs.ceph.com/en/pacific/cephfs/mount-using-fuse/ "Permalink to this headline")

CephファイルシステムをFUSEマウントするには、ceph\-fuseコマンドを使用します：

```
mkdir /mnt/mycephfs
ceph-fuse --id foo /mnt/mycephfs
```

オプションの\-\-idは、CephFSのマウントに使用する予定のキーリングを持つCephXユーザの名前を渡します。上記のコマンドでは、fooです。代わりに\-nを使用することもできますが、\-\-idの方が簡単であることは明らかです：

```
ceph-fuse -n client.foo /mnt/mycephfs
```

標準的な場所にキーリングが存在しない場合は、それも指定することができます：

```
ceph-fuse --id foo -k /path/to/keyring /mnt/mycephfs
```

MONのソケットも指定しても構いませんが、これは必須ではありません：

```
ceph-fuse --id foo -m 192.168.0.1:6789 /mnt/mycephfs
```

また、ローカルFSにCephFSのルートをマウントするのではなく、CephFS内の特定のディレクトリをマウントすることも可能です：

```
ceph-fuse --id foo -r /path/to/dir /mnt/mycephfs
```

Cephクラスタに複数のFSがある場合、オプションの\-\-client\_fsを使用してデフォルト以外のFSをマウントします：

```
ceph-fuse --id foo --client_fs mycephfs2 /mnt/mycephfs2
```

また、ceph.confにclient\_fsの設定を追加することもできます。

## Unmounting CephFS[¶](https://docs.ceph.com/en/pacific/cephfs/mount-using-fuse/ "Permalink to this headline")

CephFSをアンマウントするには、他のFSと同様にumountを使用します：

```
umount /mnt/mycephfs
```

ヒント：このコマンドを実行する前に、ファイルシステムのディレクトリ内にいないことを確認してください。

## Persistent Mounts[¶](https://docs.ceph.com/en/pacific/cephfs/mount-using-fuse/ "Permalink to this headline")

CephFSをユーザ空間のファイルシステムとしてマウントするには、/etc/fstabに以下を追加します：

```
#DEVICE PATH       TYPE      OPTIONS
none    /mnt/mycephfs  fuse.ceph ceph.id={user-ID}[,ceph.conf={path/to/conf.conf}],_netdev,defaults  0 0
```

For example:

```
none    /mnt/mycephfs  fuse.ceph ceph.id=myuser,_netdev,defaults  0 0
none    /mnt/mycephfs  fuse.ceph ceph.id=myuser,ceph.conf=/etc/ceph/foo.conf,_netdev,defaults  0 0
```

必ずIDを使用してください（例：client.myuserではなく、myuser）。この方法で、コマンドラインに任意の有効なceph\-fuseオプションを渡すことができます。

CephFSのサブディレクトリをマウントするには、/etc/fstabに以下を追加します。

```
none    /mnt/mycephfs  fuse.ceph ceph.id=myuser,ceph.client_mountpoint=/path/to/dir,_netdev,defaults  0 0
```

ceph\-fuse@.service と ceph\-fuse.target systemd ユニットが利用可能です。通常通り、これらのユニットファイルは、ceph\-fuseのデフォルトの依存関係と推奨される実行コンテキストを宣言します。上記のfstabエントリを作成した後、以下のコマンドを実行します：

```
systemctl start ceph-fuse@/mnt/mycephfs.service
systemctl enable ceph-fuse.target
systemctl enable ceph-fuse@-mnt-mycephfs.service
```

CephXのユーザ管理の詳細については「[ユーザ管理](https://docs.ceph.com/en/pacific/rados/operations/user-management/#user-management)」を、取り得るその他のオプションについては「[ceph\-fuseマニュアル](https://docs.ceph.com/en/pacific/man/8/ceph-fuse/#options)」を参照してください。トラブルシューティングについては、「[ceph\-fuseのデバッグ](https://docs.ceph.com/en/pacific/cephfs/troubleshooting/#ceph-fuse-debugging)」を参照してください。
