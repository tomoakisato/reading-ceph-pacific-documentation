# 78: CLAY code plugin

**クリップソース:** [78: CLAY code plugin — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-clay/)

# CLAY code plugin[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-clay/ "Permalink to this headline")

CLAY（Coupled\-Layerの略）コードは、故障したノード/OSD/ラックの修理時に、ネットワーク帯域やディスクIOを大幅に節約できるように設計されたECです。

d = 修理時に連絡するOSDの数

と、します。

jerasureがk=8、m=4で構成されている場合、1つのOSDを失うと、修復のために他のOSD d=8 個から読み出す必要があります。そして、例えば1GiBを回復するためには、8 X 1GiB = 8GiBの情報をダウンロードする必要があります。

一方、CLAYプラグインの場合、dは制限範囲内で設定可能です。

k\+1 \<= d \<= k\+m\-1

デフォルトでは、ネットワーク帯域とディスクIOを最も節約できるd=k\+m\-1が選ばれています。k=8、m=4、d=11で構成されたCLAYプラグインの場合、1つのOSDに障害が発生すると、d=11のOSDが接触し、それぞれから250MiBがダウンロードされ、11 X 250MiB = 2.75GiBの情報量がダウンロードされる結果となりました。より一般的なパラメータを以下に示します。テラバイト単位の情報を保存しているラックの修復を行う場合、その効果は絶大です。

|plugin      |total amount of disk IO                  |
|------------|-----------------------------------------|
|jerasure,isa|kS
                                       |
|clay        |dS/\(d \- k \+ 1\) = \(k \+ m \- 1\) S/ m
|

ここで、Sは修復中の1台のOSDに保存されているデータ量です。上の表では、dの値を可能な限り大きくしています。これは、OSD障害からの復旧に必要なデータダウンロードの量が最小になるためです。

## Erasure\-code profile examples[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-clay/ "Permalink to this headline")

帯域幅の使用量削減を観察するために使用できる設定例です。

```
$ ceph osd erasure-code-profile set CLAYprofile 
     plugin=clay 
     k=4 m=2 d=5 
     crush-failure-domain=host
$ ceph osd pool create claypool erasure CLAYprofile
```

## Creating a clay profile[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-clay/ "Permalink to this headline")

clay ECプロファイルを新規に作成するには：

```
ceph osd erasure-code-profile set {name} 
     plugin=clay 
     k={data-chunks} 
     m={coding-chunks} 
     [d={helper-chunks}] 
     [scalar_mds={plugin-name}] 
     [technique={technique-name}] 
     [crush-failure-domain={bucket-type}] 
     [directory={directory}] 
     [--force]
```

ここで：

**k={datachunks}**

Description

各オブジェクトはデータチャンクパーツに分割され、それぞれが異なるOSDに格納される

Type

Integer

Required

Yes.

Example

4

**m={coding\-chunks}**

Description

各オブジェクトのコーディングチャンクを計算し、異なるOSDに保存する。コーディングチャンクの数は、データを失うことなくダウンすることができるOSDの数でもある

Type

Integer

Required

Yes.

Example

2

**d={helper\-chunks}**

Description

1つのチャンクのリカバリーの間にデータを送信するよう要求されたOSDの数。d は k\+1 \<= d \<= k\+m\-1 となるように選択される必要がある。d が大きければ大きいほど、節約になる

Type

Integer

Required

No.

Default

k\+m\-1

**scalar\_mds={jerasure|isa|shec}**

Description

scalar\_mds は、レイヤ構築のビルディングブロックとして使用されるプラグインを指定する。jerasure, isa, shec のいずれか

String

Required

No.

Default

jerasure

**technique={technique}**

Description

technique は、指定した 'scalar\_mds' プラグイン内で選択されるテクニックを指定する。サポートする手法は、jerasure では 'reed\_sol\_van', 'reed\_sol\_r6\_op', 'cauchy\_orig', 'cauchy\_good', 'liber8tion'。 isa では 'reed\_sol\_van', 'cauchy'。 shec では 'single', 'multiple'

Type

String

Required

No.

Default

reed\_sol\_van \(for jerasure, isa\), single \(for shec\)

**rush\-root={root}**

Description

CRUSHルールの最初のステップに使用されるCRUSHバケットの名前

Type

String

Required

No.

Default

default

**crush\-failure\-domain={bucket\-type}**

Description

同じ障害ドメインを持つバケットに、2つのチャンクが存在しないことを確認する。例えば、障害ドメインがhostの場合、2つのチャンクが同じhostに保存されることはない。step chooseleaf hostのようなCRUSHルールのステップを作成するために使用される

Type

String

Required

No.

Default

host

**crush\-device\-class={device\-class}**

Description

CRUSHマップのcrushデバイスクラス名を使って、特定のクラスのデバイス（例：ssdやhdd）に配置を制限する

Type

String

Required

No.

Default

**directory={directory}**

Description

ECプラグインの読み込み元となるディレクトリ名

Type

String

Required

No.

Default

/usr/lib/ceph/erasure\-code

**\-\-force**

Description

同名の既存プロファイルを上書きする

Type

String

Required

No.

## Notion of sub\-chunks[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-clay/ "Permalink to this headline")

Clayコードはベクトルコードであるため、ディスクIOやネットワーク帯域を節約でき、サブチャンクと呼ばれるより細かい粒度でチャンク内のデータを表示・操作することができます。Clay コードのチャンク内のサブチャンクの数は次式で与えられます。

sub\-chunk count = ​𝑞𝑘\+𝑚𝑞qk\+mq, where ​𝑞=𝑑−𝑘\+1q=d−k\+1

OSDの修復中、利用可能なOSDから要求されるヘルパー情報は、1チャンクのほんの一部です。実際、修復中にアクセスされるチャンク内のサブチャンクの数は、次式で与えられます。

repair sub\-chunk count = ​𝑠𝑢𝑏−−−𝑐ℎ𝑢𝑛𝑘𝑐𝑜𝑢𝑛𝑡𝑞

### Examples[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-clay/ "Permalink to this headline")

1. k=4、m=2、d=5の構成では、サブチャンク数が8、修復サブチャンク数が4であるため、修復時にはチャンクの半分しか読み込まれません。
2. k=8, m=4, d=11の時、サブチャンク数は64、リペアサブチャンク数は16。故障したチャンクの修復のために、利用可能なOSDからチャンクの1/4が読み出されます。

## How to choose a configuration given a workload[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-clay/ "Permalink to this headline")

チャンク内の全サブチャンクのうち、一部のサブチャンクのみを読み出します。これらのサブチャンクは必ずしもチャンク内で連続して保存されているわけではありません。最高のディスクIO性能を得るには、連続したデータを読み取ることが有効です。このため、サブチャンクサイズが十分に大きくなるようなストライプサイズを選択することが推奨されます。

与えられたストライプサイズ（ワークロードに基づいて固定されている）に対して、以下のようにk、m、dを選択します。

sub\-chunk size = ​𝑠𝑡𝑟𝑖𝑝𝑒−𝑠𝑖𝑧𝑒𝑘𝑠𝑢𝑏−𝑐ℎ𝑢𝑛𝑘𝑐𝑜𝑢𝑛𝑡stripe−sizeksub−chunkcount ​= 4KB, 8KB, 12KB …

1. 例えば、ストライプサイズが64MBの場合、k=16、m=4、d=19を選択すると、サブチャンク数は1024、サブチャンクサイズは4KBになります。
2. 小規模なワークロードでは、k=4, m=2がネットワークとディスクIOの両方の利点をもたらす良い構成となります。

## Comparisons with LRC[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-clay/ "Permalink to this headline")

LRC（Locally Recoverable Codes）は、単一のOSDリカバリ時にネットワーク帯域幅やディスクIOを節約するために設計されています。しかし、LRCでは、修復時に接触するOSDの数（d）を最小限に抑えることに重点を置いていますが、これにはストレージのオーバーヘッドという代償が伴います。クレーコードはストレージオーバーヘッドm/kを持ちます。lrcの場合、m個のパリティに加え、\(k\+m\)/d個のパリティを保存するため、ストレージのオーバーヘッドは\(m\+\(k\+m\)/d\)/kになります。clayとlrcはどちらも任意のm個のOSDの故障から回復することができます。

|Parameters   |disk IO, storage overhead \(LRC\)|disk IO, storage overhead \(CLAY\)|
|-------------|---------------------------------|----------------------------------|
|\(k=10, m=4\)|7 \* S, 0.6 \(d=7\)              |3.25 \* S, 0.4 \(d=13\)           |
|\(k=16, m=4\)|4 \* S, 0.5625 \(d=4\)           |4.75 \* S, 0.25 \(d=19\)          |

ここで、Sは復旧するOSD1個の保存データ量です。
