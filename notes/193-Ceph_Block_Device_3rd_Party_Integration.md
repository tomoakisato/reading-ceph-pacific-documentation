# 193: Ceph Block Device 3rd Party Integration

**クリップソース:** [193: Ceph Block Device 3rd Party Integration — Ceph Documentation](https://docs.ceph.com/en/pacific/rbd/rbd-integrations/)

# Ceph Block Device 3rd Party Integration[¶](https://docs.ceph.com/en/pacific/rbd/rbd-integrations/ "Permalink to this headline")

* [Kernel Modules](https://docs.ceph.com/en/pacific/rbd/rbd-integrations/)
* [QEMU](https://docs.ceph.com/en/pacific/rbd/rbd-integrations/)
* [libvirt](https://docs.ceph.com/en/pacific/rbd/rbd-integrations/)
* [Kubernetes](https://docs.ceph.com/en/pacific/rbd/rbd-integrations/)
* [OpenStack](https://docs.ceph.com/en/pacific/rbd/rbd-integrations/)
* [CloudStack](https://docs.ceph.com/en/pacific/rbd/rbd-integrations/)
* [LIO iSCSI Gateway](https://docs.ceph.com/en/pacific/rbd/rbd-integrations/)
* [Windows](https://docs.ceph.com/en/pacific/rbd/rbd-integrations/)
