# 147: Client Configuration

**クリップソース:** [147: Client Configuration — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/client-config-ref/)

# Client Configuration[¶](https://docs.ceph.com/en/pacific/cephfs/client-config-ref/ "Permalink to this headline")

## Updating Client Configuration[¶](https://docs.ceph.com/en/pacific/cephfs/client-config-ref/ "Permalink to this headline")

特定のクライアント構成は実行時に適用できます。 実行時に構成オプションを適用できる（クライアントで有効になる）かどうかを確認するには、config helpコマンドを使用します：

```
ceph config help debug_client
 debug_client - Debug level for client
 (str, advanced)                                                                                                                      Default: 0/5
 Can update at runtime: true

 The value takes the form 'N' or 'N/M' where N and M are values between 0 and 99.  N is the debug level to log (all values below this are included), and M is the level to gather and buffer in memory.  In the event of a crash, the most recent items <= M are dumped to the log file.
```

config help は、与えられた設定が実行時に適用可能かどうかを、デフォルトと設定オプションの説明とともに表示します。

実行時に設定オプションを更新するには、config setコマンドを使用します：

```
ceph config set client debug_client 20/20
```

これは、すべてのクライアントに対して与えられたコンフィギュレーションを変更することに注意してください。

設定されているオプションを確認するには、config getコマンドを使用します：

```
ceph config get client
 WHO    MASK LEVEL    OPTION                    VALUE     RO
 client      advanced debug_client              20/20
 global      advanced osd_pool_default_min_size 1
 global      advanced osd_pool_default_size     3
```

## Client Config Reference[¶](https://docs.ceph.com/en/pacific/cephfs/client-config-ref/ "Permalink to this headline")

**client\_acl\_type**

Description

ACL タイプを設定する。現在、設定できる値は POSIX ACL を有効にするための "posix\_acl" または空文字列のみ。このオプションは、fuse\_default\_permissions が false に設定されているときのみ有効

Type

String

Default

"" \(no ACL enforcement\)

**client\_cache\_mid**

Description

クライアントキャッシュのミッドポイントを設定する。ミッドポイントは、LRUリストをホットリストとウォームリストに分割する

Type

Float

Default

0.75

**client\_cache\_size**

Description

クライアントがメタデータキャッシュに保持するinodeの数

Type

Integer

Default

16384

**client\_caps\_release\_delay**

Description

CAPSをリリースするまでの遅延時間（秒）。delayは、クライアントが、他のユーザー空間の操作に必要なCAPSがある場合に、不要になったCAPSをリリースするのを何秒待つかを設定する

Type

Integer

Default

5 \(seconds\)

**client\_debug\_force\_sync\_read**

Description

trueに設定すると、クライアントはローカルページキャッシュを使用せず、OSDから直接データを読み込む

Type

Boolean

Default

false

**client\_dirsize\_rbytes**

Description

trueに設定すると、ディレクトリの再帰的なサイズ（つまり、すべての子孫の合計）を使用する

Type

Boolean

Default

true

**client\_max\_inline\_size**

Description

RADOSの個別のデータオブジェクトではなく、ファイルのinodeに保存されるインラインデータの最大サイズ。この設定は、MDSマップにinline\_dataフラグが設定されている場合にのみ適用される

Type

Integer

Default

4096

**client\_metadata**

Description

（自動生成されたバージョン、ホスト名、その他のメタデータの他に）各MDSに送信するクライアントのメタデータをカンマ区切りで表した文字列

Type

String

Default

"" \(no additional metadata\)

**client\_mount\_gid**

Description

CephFSマウントのグループID

Type

Integer

Default

\-1

**client\_mount\_timeout**

Description

CephFSマウントのタイムアウト（秒）

Type

Float

Default

300.0

**client\_mount\_uid**

Description

CephFSマウントのユーザID

Type

Integer

Default

\-1

**client\_mountpoint**

Description

CephFSファイルシステム上にマウントするディレクトリ。ceph\-fuseコマンドの\-rオプションの代わりとなるもの

Type

String

Default

"/"

**client\_oc**

Description

オブジェクトキャッシュを有効にする

Type

Boolean

Default

true

**client\_oc\_max\_dirty**

Description

オブジェクトキャッシュのダーティバイトの最大数

Type

Integer

Default

104857600 \(100MB\)

**client\_oc\_max\_dirty\_age**

Description

オブジェクトキャッシュのダーティデータの、ライトバックまでの最大経過時間（秒）

Type

Float

Default

5.0 \(seconds\)

**client\_oc\_max\_objects**

Description

オブジェクトキャッシュの最大数

Type

Integer

Default

1000

**client\_oc\_size**

Description

クライアントが何バイトのデータをキャッシュするか

Type

Integer

Default

209715200 \(200 MB\)

**client\_oc\_target\_dirty**

Description

ダーティデータの目標サイズ。この数値は小さくしておくことを推奨する

Type

Integer

Default

8388608 \(8MB\)

**client\_permissions**

Description

すべてのI/O操作でクライアントパーミッションを確認する

Type

Boolean

Default

true

**client\_quota**

Description

trueに設定すると、クライアントのクォータチェックを有効にする

Type

Boolean

Default

true

**client\_quota\_df**

Description

statfs操作でルートディレクトリのクォータを報告する

Type

Boolean

Default

true

**client\_readahead\_max\_bytes**

Description

今後のread操作でクライアントが先読みする最大バイト数。client\_readahead\_max\_periods の設定により上書きされる

Type

Integer

Default

0 \(unlimited\)

**client\_readahead\_max\_periods**

Description

クライアントが先読みするファイルレイアウト期間（オブジェクトサイズ\*ストライプ数）の数。client\_readahead\_max\_bytes の設定を上書きする

Type

Integer

Default

4

**client\_readahead\_min**

Description

クライアントが先読みする最小バイト数

Type

Integer

Default

131072 \(128KB\)

**client\_reconnect\_stale**

Description

古くなったセッションを自動で再接続する

Type

Boolean

Default

false

**client\_snapdir**

Description

スナップショットディレクトリ名

Type

String

Default

".snap"

**client\_tick\_interval**

Description

CAPS更新やその他の維持の間隔（秒）

Type

Float

Default

1.0 \(seconds\)

**client\_use\_random\_mds**

Description

各リクエストに対してランダムなMDSを選択する

Type

Boolean

Default

false

**fuse\_default\_permissions**

Description

falseに設定すると、ceph\-fuseユーティリティチェックは、FUSEのパーミッションに依存するのではなく、独自のパーミッションチェックを行う。POSIX ACLを有効にするには、client acl type=posix\_aclオプションと一緒にfalseに設定する

Type

Boolean

Default

true

**fuse\_max\_write**

Description

1回のwrite操作における最大バイト数。0は変更なしを表し、FUSEのデフォルトである128Kバイトが有効

Type

Integer

Default

0

**fuse\_disable\_pagecache**

Description

trueに設定すると、カーネルページキャッシュはceph\-fuseマウントで無効。複数のクライアントが同時にファイルに対して読み書きする場合、リーダーはページキャッシュから古いデータを取得することがある。FUSEの制限により、ceph\-fuseはページキャッシュを動的に無効化できない。

Type

Boolean

Default

false

### Developer Options[¶](https://docs.ceph.com/en/pacific/cephfs/client-config-ref/ "Permalink to this headline")

重要：これらのオプションは内部的なものです。これらのオプションは、オプションのリストを完成させるためにのみ、ここに記載されています。

**client\_debug\_getattr\_caps**

Description

MDSからの返信に必要なCAPSが含まれているかどうかを確認する

Type

Boolean

Default

false

**client\_debug\_inject\_tick\_delay**

Description

クライアントのtick間に遅延を追加する

Type

Integer

Default

0

**client\_inject\_fixed\_oldest\_tid**

Description

Type

Boolean

Default

false

**client\_inject\_release\_failure**

Description

Type

Boolean

Default

false

**client\_trace**

Description

すべてのファイル操作のトレースファイルへのpath。出力は、Ceph syntheticクライアントで使用するように設計されている

Type

String

Default

"" \(disabled\)
