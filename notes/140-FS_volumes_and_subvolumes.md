# 140: FS volumes and subvolumes

**クリップソース:** [140: FS volumes and subvolumes — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/fs-volumes/)

# FS volumes and subvolumes[¶](https://docs.ceph.com/en/pacific/cephfs/fs-volumes/ "Permalink to this headline")

CephFSエクスポートの単一のソースは、[Ceph Manager](https://docs.ceph.com/en/pacific/glossary/#term-Ceph-Manager)デーモン（ceph\-mgr）のボリュームモジュールに実装されています。OpenStack共有ファイルシステムサービス（[manila](https://github.com/openstack/manila)）、Ceph Container Storage Interface（[CSI](https://github.com/ceph/ceph-csi)）、ストレージ管理者などは、ceph\-mgrボリュームモジュールが提供する共通のCLIを使用して、CephFSエクスポートを管理することができます。

ceph\-mgrボリュームモジュールは、次のファイルシステムエクスポート抽象化を実装しています：

* FSボリューム、CephFSファイルシステムの抽象化
* FSサブボリューム、独立したCephFSディレクトリツリーの抽象化
* FSサブボリュームグループ、FSサブボリュームより上位のディレクトリレベルの抽象化で、サブボリュームのセット全体にポリシー（

エクスポートの抽象化の可能なユースケース：

* FSサブボリュームをmanila共有またはCSIボリュームとして使用する
* manila共有グループとして使用されるFSサブボリュームグループ

## Requirements[¶](https://docs.ceph.com/en/pacific/cephfs/fs-volumes/ "Permalink to this headline")

* Nautilus \(14.2.x\) or a later version of Ceph
* 以下の最小限のCAPSを持つCephxクライアント・ユーザ（「

```
mon 'allow r'
mgr 'allow rw'
```

## FS Volumes[¶](https://docs.ceph.com/en/pacific/cephfs/fs-volumes/ "Permalink to this headline")

ボリュームを作成するには：

```
$ ceph fs volume create <vol_name> [<placement>]
```

これにより、CephFSファイルシステムとそのデータとメタデータのプールが作成されます。また、有効化されたceph\-mgrオーケストレータモジュール\([オーケストレータCLI](https://docs.ceph.com/en/pacific/mgr/orchestrator/)参照\)を使用して、ファイルシステム用のMDSを作成することもできます。（例:rook）

\<vol\_name\> はボリューム名\(任意の文字列\)、そして

\<placement\> は、NFS Ganesha デーモンコンテナを実行するホストを示すオプションの文字列で、オプションとしてクラスタの NFS Ganesha デーモンの合計数（ノードあたり複数の NFS Ganesha デーモンを実行したい場合）を指定できます。たとえば、次の配置文字列は、「ノードhost1およびhost2上にNFS Ganeshaデーモンを配置する（各ホストに1つのデーモン）」を意味します：

“host1,host2”

以下の配置指定では、ノードhost1とhost2にそれぞれ2つのNFS Ganeshaデーモンを配置します（クラスタに合計4つのNFS Ganeshaデーモンがあることになります）：

“4 host1,host2”

配置指定の詳細については、[オーケストレーターのドキュメント](https://docs.ceph.com/docs/master/mgr/orchestrator/#placement-specification)を参照してください。

ボリュームを削除するには：

```
$ ceph fs volume rm <vol_name> [--yes-i-really-mean-it]
```

これは、ファイルシステムとそのデータとメタデータプールを削除するものです。また、有効化されたceph\-mgrオーケストレータモジュールを使用して、MDSを削除しようとします。

使用しているボリュームを一覧表示するには：

```
$ ceph fs volume ls
```

## FS Subvolume groups[¶](https://docs.ceph.com/en/pacific/cephfs/fs-volumes/ "Permalink to this headline")

サブボリュームグループを作成するには：

```
$ ceph fs subvolumegroup create <vol_name> <group_name> [--pool_layout <data_pool_name>] [--uid <uid>] [--gid <gid>] [--mode <octal_mode>]
```

サブボリュームグループがすでに存在する場合でも、コマンドは成功します。

サブボリュームグループの作成時に、データプールレイアウト（ファイルレイアウトを参照）、uid、gid、ファイルモードを8進数で指定することができます。デフォルトでは、サブボリュームグループは8進数のファイルモード「755」、uid「0」、gid「0」、親ディレクトリのデータプールレイアウトで作成されます。

サブボリュームグループを削除するには：

```
$ ceph fs subvolumegroup rm <vol_name> <group_name> [--force]
```

サブボリュームグループが空でない場合、または存在しない場合は削除に失敗します。'\-\-force' フラグは存在しないサブボリュームグループの削除コマンドを成功させることができます。

サブボリュームグループの絶対パスを取得するには：

```
$ ceph fs subvolumegroup getpath <vol_name> <group_name>
```

サブボリュームグループを一覧表示するには：

```
$ ceph fs subvolumegroup ls <vol_name>
```

注意: サブボリュームグループスナップショット機能は、メインラインCephFSではサポートされなくなりました\(既存のグループスナップショットは引き続きリストおよび削除可能です\)。

サブボリュームグループのスナップショットを削除するには：

```
$ ceph fs subvolumegroup snapshot rm <vol_name> <group_name> <snap_name> [--force]
```

'\-\-force' フラグを使用すると、スナップショットが存在しない場合にコマンドを成功させることができます。

サブボリュームグループのスナップショットを一覧表示するには：

```
$ ceph fs subvolumegroup snapshot ls <vol_name> <group_name>
```

## FS Subvolumes[¶](https://docs.ceph.com/en/pacific/cephfs/fs-volumes/ "Permalink to this headline")

サブボリュームを作成するには：

```
$ ceph fs subvolume create <vol_name> <subvol_name> [--size <size_in_bytes>] [--group_name <subvol_group_name>] [--pool_layout <data_pool_name>] [--uid <uid>] [--gid <gid>] [--mode <octal_mode>] [--namespace-isolated]
```

サブボリュームがすでに存在する場合でも、コマンドは成功します。

サブボリュームの作成時には、サブボリュームグループ、データプールレイアウト、uid、gid、ファイルモード（8進数）、サイズをバイト単位で指定することができます。サブボリュームのサイズは、サブボリュームにクォータを設定することで指定します（「クォータ」参照）。サブボリュームは、\-\-namespace\-isolatedオプションを指定することにより、別のRADOSネームスペースに作成することができます。デフォルトでは、サブボリュームはデフォルトのサブボリュームグループ内に作成され、8進ファイルモード「755」、そのサブボリュームグループのuid、そのサブボリュームグループのgid、その親ディレクトリのデータプールレイアウト、サイズ制限なしとなっています。

サブボリュームを削除するには：

```
$ ceph fs subvolume rm <vol_name> <subvol_name> [--group_name <subvol_group_name>] [--force] [--retain-snapshots]
```

このコマンドは、サブボリュームとその内容を削除します。これは2つのステップで行われます。まず、サブボリュームをゴミ箱フォルダーに移動し、次に非同期的にその内容を消去します。

サブボリュームの削除は、スナップショットがある場合や、存在しない場合は失敗します。'\-\-force' フラグは、存在しないサブボリュームの削除コマンドを成功させることができます。

サブボリュームの既存のスナップショットを保持して削除するには、'\-\-retain\-snapshots' オプションを使用することができます。スナップショットが保持される場合、保持されたスナップショットを含まない全ての操作でサブボリュームは空とみなされます。

注：スナップショットで保持されたサブボリュームは、「ceph fs subvolume create」を使用して再作成することができます。

注：保持されたスナップショットは、サブボリュームの再作成や、より新しいサブボリュームへのクローンソースとして使用することができます。

サブボリュームのサイズを変更するには：

```
$ ceph fs subvolume resize <vol_name> <subvol_name> <new_size> [--group_name <subvol_group_name>] [--no_shrink]
```

このコマンドは 'new\_size' で指定されたサイズを使用してサブボリュームのクォータをリサイズします。'\-\-no\_shrink' フラグは、サブボリュームの現在の使用サイズ以下に縮小することを防ぎます。

new\_size に 'inf' または 'infinite' を渡すことにより、サブボリュームのサイズを無限に変更することができます。

cephx認証ID、fsサブボリュームへの読み取り/読み取り/書き込みアクセスを許可するには：

```
$ ceph fs subvolume authorize <vol_name> <sub_name> <auth_id> [--group_name=<group_name>] [--access_level=<access_level>]
```

access\_level' は 'r' または 'rw' を値として取ります。

cephx認証ID、fsサブボリュームへの読み取り/読み取り/書き込みアクセスの認証を解除するには：

```
$ ceph fs subvolume deauthorize <vol_name> <sub_name> <auth_id> [--group_name=<group_name>]
```

fsサブボリュームへのアクセスを許可されたcephx認証IDをリストするには：

```
$ ceph fs subvolume authorized_list <vol_name> <sub_name> [--group_name=<group_name>]
```

認証IDとマウントされたサブボリュームに基づいてfsクライアントを退出させるには：

```
$ ceph fs subvolume evict <vol_name> <sub_name> <auth_id> [--group_name=<group_name>]
```

サブボリュームの絶対パスを取得するには：

```
$ ceph fs subvolume getpath <vol_name> <subvol_name> [--group_name <subvol_group_name>]
```

サブボリュームのメタデータを取得するには：

```
$ ceph fs subvolume info <vol_name> <subvol_name> [--group_name <subvol_group_name>]
```

出力形式はjsonで、以下のようなフィールドが含まれます。

* atime: サブボリュームパスのaccess時間（フォーマット "YYYY\-MM\-DD HH:MM:SS"\)
* mtime: サブボリュームパスのmodification時刻（フォーマット "YYYY\-MM\-DD HH:MM:SS"\)
* ctime: サブボリュームパスのchange時間（フォーマット "YYYY\-MM\-DD HH:MM:SS"\)
* uid: サブボリュームパスの uid
* gid: サブボリュームパスのgid
* mode: サブボリュームパスのモード
* mon\_addrs: モニタアドレス一覧
* bytes\_pcent: quota が設定されている場合はパーセンテージ、設定されていない場合は "undefined" と表示される
* bytes\_quota: quota が設定されている場合、quota のサイズ（バイト）、そうでない場合は "infinite" と表示される
* bytes\_used: サブボリュームの現在の使用サイズ（バイト）
* created\_at:サブボリュームの作成時刻（フォーマット "YYYY\-MM\-DD HH:MM:SS"\)
* data\_pool: サブボリュームの所属するデータプール
* path: サブボリュームの絶対パス
* type: クローンかサブボリュームかを示すサブボリュームの種類
* pool\_namespace: サブボリュームのRADOS名前空間
* features: サブボリュームでサポートされる機能
* state: サブボリュームの現在の状態

サブボリュームがそのスナップショットを保持したまま削除されている場合、出力には以下のようなフィールドのみが含まれます。

* type: クローンかサブボリュームかを示すサブボリュームの種類
* features: サブボリュームがサポートする機能
* state: サブボリュームの現在の状態

サブボリュームの "features "は、サブボリュームの内部バージョンに基づき、以下の機能のサブセットを含むリストです。

* “snapshot\-clone”: サブボリュームのスナップショットをソースとしたクローン作成に対応している
* “snapshot\-autoprotect”: アクティブなクローンソースであるスナップショットを、自動的に削除から保護することができる
* “snapshot\-retention”: サブボリュームの内容を削除し、既存のスナップショットを保持することができる

サブボリュームの "state "は、現在のサブボリュームの状態に基づき、以下の値のいずれかを含みます。

* “complete”: サブボリュームは操作可能
* “snapshot\-retained”: サブボリュームは削除されたが、スナップショットは保持されている

サブボリュームを一覧表示するには：

```
$ ceph fs subvolume ls <vol_name> [--group_name <subvol_group_name>]
```

注：削除されたがスナップショットが保持されているサブボリュームもリストアップされます。

サブボリュームのスナップショットを作成するには：

```
$ ceph fs subvolume snapshot create <vol_name> <subvol_name> <snap_name> [--group_name <subvol_group_name>]
```

サブボリュームのスナップショットを削除するには：

```
$ ceph fs subvolume snapshot rm <vol_name> <subvol_name> <snap_name> [--group_name <subvol_group_name>] [--force]
```

’\-\-force' フラグを使用すると、スナップショットが存在しない場合にコマンドを成功させることができます。

注意：スナップショットが保持されているサブボリューム内の最後のスナップショットを削除した場合、サブボリュームも削除されます。

サブボリュームのスナップショットを一覧表示するには：

```
$ ceph fs subvolume snapshot ls <vol_name> <subvol_name> [--group_name <subvol_group_name>]
```

スナップショットのメタデータを取得するには：

```
$ ceph fs subvolume snapshot info <vol_name> <subvol_name> <snap_name> [--group_name <subvol_group_name>]
```

出力形式はjsonで、以下のようなフィールドが含まれます。

* created\_at: スナップショットの作成時刻（フォーマット "YYYY\-MM\-DD HH:MM:SS:ffffff" ）
* data\_pool: スナップショットが属するデータプール
* has\_pending\_clones:スナップショットクローンが進行中の場合は "yes"、そうでない場合は "no "
* size: スナップショットのサイズ（バイト）

## Cloning Snapshots[¶](https://docs.ceph.com/en/pacific/cephfs/fs-volumes/ "Permalink to this headline")

サブボリュームは、サブボリュームのスナップショットのクローンを作成することで作成することができます。クローン作成は、スナップショットからサブボリュームへのデータ・コピーを含む非同期操作です。この一括コピーの性質上、クローニングは現在、非常に巨大なデータセットに対して非効率的です。

注：スナップショット（ソースサブボリューム）の削除は、保留中のクローン操作や進行中のクローン操作がある場合は失敗します。

Nautilusリリースでは、クローン作成前にスナップショットを保護することが前提条件となっており、スナップショットの保護/保護を解除するコマンドはこの目的のために導入されました。この前提条件、つまり保護/保護解除コマンドは、メインラインのCephFSでは非推奨となっており、将来のリリースで削除される可能性があります。

非推奨となるコマンドは以下の通りです：

$ ceph fs subvolume snapshot protect \<vol\_name\> \<subvol\_name\> \<snap\_name\> \[–group\_name \<subvol\_group\_name\>\] 

$ ceph fs subvolume snapshot unprotect \<vol\_name\> \<subvol\_name\> \<snap\_name\> \[–group\_name \<subvol\_group\_name\>\]

注：上記のコマンドを使用してもエラーにはなりませんが、有用な機能はありません。

注：subvolume infoコマンドを使用して、サポートされている「features」に関するサブボリュームのメタデータを取得し、「snapshot\-autoprotect」機能の有効性に基づいて、スナップショットの保護/保護解除が必要であるかどうかを判断するのに使用します。

クローン操作を開始するには：

```
$ ceph fs subvolume snapshot clone <vol_name> <subvol_name> <snap_name> <target_subvol_name>
```

スナップショット（ソースサブボリューム）がdefault以外のグループに属している場合、以下のようにグループ名を指定する必要があります：

```
$ ceph fs subvolume snapshot clone <vol_name> <subvol_name> <snap_name> <target_subvol_name> --group_name <subvol_group_name>
```

クローンされたサブボリュームは、ソーススナップショットとは異なるグループに属することができます（デフォルトでは、クローンされたサブボリュームはdefaultグループに作成されます）。特定のグループにクローンするには、以下を使用します：

```
$ ceph fs subvolume snapshot clone <vol_name> <subvol_name> <snap_name> <target_subvol_name> --target_group_name <subvol_group_name>
```

サブボリュームの作成時にプール・レイアウトを指定するのと同様に、クローン・サブボリュームの作成時にプール・レイアウトを指定することができます。特定のプールレイアウトを持つクローンサブボリュームを作成するには、以下を使用します：

```
$ ceph fs subvolume snapshot clone <vol_name> <subvol_name> <snap_name> <target_subvol_name> --pool_layout <pool_layout>
```

最大同時クローン数を設定します。デフォルトは 4 に設定されています：

```
$ ceph config set mgr mgr/volumes/max_concurrent_clones <value>
```

クローン操作の状態を確認するには：

```
$ ceph fs clone status <vol_name> <clone_name> [--group_name <group_name>]
```

クローンは以下のいずれかの状態になります：

1. pending     : クローン作成が開始されていない
2. in\-progress : クローン操作中
3. complete    : クローン作成が正常に終了した
4. failed      : クローン操作に失敗した

クローン処理中の出力例：

```
$ ceph fs subvolume snapshot clone cephfs subvol1 snap1 clone1
$ ceph fs clone status cephfs clone1
{
  "status": {
    "state": "in-progress",
    "source": {
      "volume": "cephfs",
      "subvolume": "subvol1",
      "snapshot": "snap1"
    }
  }
}
```

\(注：subvol1がdefaultグループに属しているため、クローン状態のソースセクションにはグループ名が含まれません）。

注：クローンされたサブボリュームは、クローン操作が正常に完了した後でのみアクセス可能です。

クローン操作に成功すると、クローンの状態は次のようになります：

```
$ ceph fs clone status cephfs clone1
{
  "status": {
    "state": "complete"
  }
}
```

クローンに失敗した場合は、Failed 状態となります。

クローン操作に失敗した場合は、部分クローンを削除し、クローン操作を再度起動する必要があります。部分クローンを削除するには：

```
$ ceph fs subvolume rm <vol_name> <clone_name> [--group_name <group_name>] --force
```

注：クローン作成は、ディレクトリ、通常ファイル、シンボリックリンクのみ同期します。また、inodeのタイムスタンプ（アクセス時刻と変更時刻）は秒粒度で同期されます。

進行中のクローン操作や保留中のクローン操作をキャンセルすることができます。クローン操作をキャンセルするには、clone cancel コマンドを使用します：

```
$ ceph fs clone cancel <vol_name> <clone_name> [--group_name <group_name>]
```

キャンセルに成功すると、クローン化されたサブボリュームはcanceled状態に移行します：

```
$ ceph fs subvolume snapshot clone cephfs subvol1 snap1 clone1
$ ceph fs clone cancel cephfs clone1
$ ceph fs clone status cephfs clone1
{
  "status": {
    "state": "canceled",
    "source": {
      "volume": "cephfs",
      "subvolume": "subvol1",
      "snapshot": "snap1"
    }
  }
}
```

注意：fs subvolume rmコマンドの\-\-forceオプションで、クローン解除したものを削除することができます。

## Pinning Subvolumes and Subvolume Groups[¶](https://docs.ceph.com/en/pacific/cephfs/fs-volumes/ "Permalink to this headline")

サブボリュームやサブボリュームグループは、ポリシーに従って自動的にランクに固定することができます。これにより、予測可能で安定した方法で、MDSのランクに負荷を分散させることができます。 ピン留めの仕組みについては、「[ディレクトリツリーを特定のランクに手動でピン留めする](https://docs.ceph.com/en/pacific/cephfs/multimds/#cephfs-pinning)」と「[サブツリーのパーティショニングポリシーを設定する](https://docs.ceph.com/en/pacific/cephfs/multimds/#cephfs-ephemeral-pinning)」を参照してください。

ピン止めは、以下の方法で設定します：

```
$ ceph fs subvolumegroup pin <vol_name> <group_name> <pin_type> <pin_setting>
```

またはサブボリュームの場合：

```
$ ceph fs subvolume pin <vol_name> <group_name> <pin_type> <pin_setting>
```

一般的には、サブボリュームグループのピンを設定することになります。pin\_typeはexport、distributed、randomのいずれかになります。pin\_settingは、上記のpinningのドキュメントにあるように、拡張属性 "value "に対応します。

そのため、例えば、サブボリュームグループに分散ピンニング戦略を設定するには：

```
$ ceph fs subvolumegroup pin cephfilesystem-a csi distributed 1
```

"csi" サブボリュームグループの分散サブツリーパーティショニングポリシーを有効にします。 これにより、グループ内の各サブボリュームは、ファイルシステム上の利用可能なランクの1つに自動的に固定されます。
