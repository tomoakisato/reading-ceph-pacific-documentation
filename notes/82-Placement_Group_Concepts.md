# 82: Placement Group Concepts

**クリップソース:** [82: Placement Group Concepts — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/operations/pg-concepts/)

# Placement Group Concepts[¶](https://docs.ceph.com/en/pacific/rados/operations/pg-concepts/ "Permalink to this headline")

ceph \-w、ceph osd dumpなど、PGに関連するコマンドを実行すると、Cephは以下の用語の一部を使用して値を返す場合があります。

**_Peering_**

PGを格納するすべてのOSDが、そのPG内のすべてのオブジェクト（とそのメタデータ）の状態について一致するようにするプロセス。状態について合意しても、すべて最新の内容であることを意味しないことに注意

**_Acting Set_**

特定のPGに責任を持つ \(またはあるエポックにおいて責任を負っていた\) OSD の順序付きリスト

**_Up Set_**

CRUSH に従って、あるエポックにおける特定のPGに責任を持つ OSD の順序付きリスト。OSDマップのpg\_tempでActing Setが明示的に上書きされている場合を除き、通常これはActing Setと同じ

**_Current Interval_ or _Past Interval_**

特定のPGのActing SetとUp Setが変化しない間のOSDマップエポックのシーケンス

**_Primary_**

ピアリングの調整を担当し、PG内のオブジェクトへのクライアント書き込みを受け付ける唯一のOSDである、Acting Setのメンバ（最初に配置される）

**_Replica_**

PGのActing Setに含まれる非プライマリOSD（プライマリからそのように認識されアクティベートされている）

**_Stray_**

現在のActing Setのメンバではないが、PGのコピーを削除できることをまだ知らされていないOSD

**_Recovery_**

PG内のすべてのオブジェクトのコピーが、Acting Set内のすべてのOSDにあることを確認すること。 ピアリングが実行されると、プライマリは書き込み操作の受け付けを開始でき、リカバリはバックグラウンドで進行することができる

**_PG Info_**

PGの作成エポック、PGへの最新の書き込みのバージョン、_Last Epoch Start_、_Last Epoch Clean_、現在のインターバルの開始に関する基本的なメタデータ。 PGに関するOSD間通信には、PG Infoが含まれる。これにより、PGが存在する（または存在した）ことを知っているOSDにも、_Last Epoch Clean_または_Last Epoch Start_の下限があります。

**_PG Log_**

PG内のオブジェクトに対して最近行われた更新のリスト。ログは、Acting Set内のすべてのOSDがある時点までACKされた後に切り捨てられることに注意

**_Missing Set_**

各OSDは更新ログエントリを記録し、それがオブジェクトの内容に対する更新を意味する場合、そのオブジェクトを必要な更新のリストに追加する。 このリストは、その\<OSD,PG\>のMissing Setと呼ばれる

**_Authoritative History_**

もし実行すれば、PGのOSDのコピーを最新にする、完全で、順序付けられた操作のセット

**_Epoch_**

OSDマップの（単調増加する）バージョン番号

**_Last Epoch Start_**

特定のPGのActing Setに含まれるすべてのノードがAuthoritative Historyに合意した最後のエポック。 この時点で、Peeringは成功したとみなされる

**_up\_thru_**

プライマリがPeeringプロセスを正常に完了する前に、現在のOSDマップエポックを通じて生きているモニタに、モニタがosdマップにup\_thruを設定することによって通知しなければならない。 これは、以下の2番目のインターバルのような、失敗の連続の後にピアリングが完了しなかった以前のActing Setを、ピアリングが無視できるようにするため

* _acting set_
* _acting set_
* _acting set_
* _acting set_

**_Last Epoch Clean_**

特定のPGのActing Set内のすべてのノードが完全に最新になった最後のエポック（PG Logとオブジェクトの両方の内容）。この時点で復旧が完了したとみなされる
