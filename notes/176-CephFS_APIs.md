# 176: CephFS APIs

**クリップソース:** [176: CephFS APIs — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/api/)

# CephFS APIs[¶](https://docs.ceph.com/en/pacific/cephfs/api/ "Permalink to this headline")

* [libcephfs \(Java\)](https://docs.ceph.com/en/pacific/cephfs/api/)
* [libcephfs \(Python\)](https://docs.ceph.com/en/pacific/cephfs/api/)
    * [API calls](https://docs.ceph.com/en/pacific/cephfs/api/)
