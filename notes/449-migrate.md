# 449: migrate

**クリップソース:** [449: migrate — Ceph Documentation](https://docs.ceph.com/en/pacific/ceph-volume/lvm/migrate/)

# migrate[¶](https://docs.ceph.com/en/pacific/ceph-volume/lvm/migrate/ "Permalink to this headline")

BlueFSデータをソースボリュームからターゲットボリュームに移動します。ソースボリューム（main、すなわちdataやblockのものを除く）は成功すると削除されます。

LVMボリュームは、ターゲットに対してのみ指定できます（既にアタッチされている場合も、新規の場合も）。

後者の場合、OSDにアタッチされソースデバイスの1つを置き換えます。

以下の置換ルールを適用します（優先順で、最初のマッチで停止する）：

* ソースリストにDBボリュームがある場合、ターゲットデバイスがそれを置き換えます。
* ソースリストにWALボリュームがある場合、ターゲットデバイスがそれを置き換えます。
* ソースリストが低速ボリュームのみの場合 \- 操作は許可されず、new\-db/new\-wal コマンドによる明示的な割り当てが必要です。

BlueFSのデータをmainデバイスからDBとしてアタッチ済みのLVに移動するには：

```
ceph-volume lvm migrate --osd-id 1 --osd-fsid <uuid> --from data --target vgname/db
```

BlueFSのデータを共有mainデバイスから、新規DBとしてアタッチされるLVに移動するには：

```
ceph-volume lvm migrate --osd-id 1 --osd-fsid <uuid> --from data --target vgname/new_db
```

BlueFSのデータをDBデバイスから新しいLVに移動し、DBを交換するには：

```
ceph-volume lvm migrate --osd-id 1 --osd-fsid <uuid> --from db --target vgname/new_db
```

BlueFSのデータをmainデバイスとDBデバイスから新しいLVに移動し、DBを交換するには：

```
ceph-volume lvm migrate --osd-id 1 --osd-fsid <uuid> --from data db --target vgname/new_db
```

BlueFSのデータをmain、DB、WALの各デバイスから新しいLVに移動し、WALとDBは削除するには：

```
ceph-volume lvm migrate --osd-id 1 --osd-fsid <uuid> --from data db wal --target vgname/new_db
```

BlueFSのデータをDB、WALの各デバイスからmainデバイスに移動し、WALとDBは削除するには：

```
ceph-volume lvm migrate --osd-id 1 --osd-fsid <uuid> --from db wal --target vgname/data
```
