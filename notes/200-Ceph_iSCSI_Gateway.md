# 200: Ceph iSCSI Gateway

**クリップソース:** [200: Ceph iSCSI Gateway — Ceph Documentation](https://docs.ceph.com/en/pacific/rbd/iscsi-overview/)

# Ceph iSCSI Gateway[¶](https://docs.ceph.com/en/pacific/rbd/iscsi-overview/ "Permalink to this headline")

iSCSI Gatewayは、RBD（RADOS Block Device）イメージをSCSIディスクとしてエクスポートするHA（Highly Available）iSCSIターゲットを提供します。iSCSIプロトコルは、クライアント（イニシエータ）がTCP/IPネットワーク上でストレージデバイス（ターゲット）にSCSIコマンドを送信することを可能にし、Cephクライアントのネイティブサポートを持たないクライアントがCephブロックストレージにアクセスすることを可能にするものです。 これには、Microsoft Windowsや、BIOSも含まれます。

各 iSCSI ゲートウェイは、Linux IO target kernel subsystem \(LIO\) を利用して、iSCSI プロトコルのサポートを提供します。LIOはユーザースペースパススルー\(TCMU\)を利用してCephのlibrbdライブラリと対話し、RBDイメージをiSCSIクライアントに公開します。CephのiSCSIゲートウェイを使用すると、従来のストレージエリアネットワーク\(SAN\)のすべての機能と利点を備えた、完全に統合されたブロックストレージインフラストラクチャをプロビジョニングすることができます。

* [Requirements](https://docs.ceph.com/en/pacific/rbd/iscsi-overview/)
* [Configuring the iSCSI Target](https://docs.ceph.com/en/pacific/rbd/iscsi-overview/)
* [Configuring the iSCSI Initiators](https://docs.ceph.com/en/pacific/rbd/iscsi-overview/)
* [Monitoring the iSCSI Gateways](https://docs.ceph.com/en/pacific/rbd/iscsi-overview/)
