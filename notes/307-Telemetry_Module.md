# 307: Telemetry Module

**クリップソース:** [307: Telemetry Module — Ceph Documentation](https://docs.ceph.com/en/pacific/mgr/telemetry/)

# Telemetry Module[¶](https://docs.ceph.com/en/pacific/mgr/telemetry/ "Permalink to this headline")

telemetryモジュールは、クラスタに関する匿名のデータをCephの開発者に送り返し、Cephの使用状況やユーザが経験する可能性のある問題を把握するのに役立ちます。

このデータは公開ダッシュボードで可視化され、報告を行っているクラスタの数、総容量とOSD数、バージョン分布の傾向などの概要統計を、コミュニティがすぐに確認できるようになっています。

## Channels[¶](https://docs.ceph.com/en/pacific/mgr/telemetry/ "Permalink to this headline")

テレメトリーレポートは、いくつかの「チャンネル」に分けられ、それぞれ異なる種類の情報を持っています。 テレメトリーが有効になっている場合、個々のチャンネルをオンまたはオフにすることができます。 \(テレメトリーがオフの場合、チャンネルごとの設定は影響しません）。

* クラスタ容量
    モニタ、マネージャ、OSD、MDS、オブジェクトゲートウェイ、その他デーモンの数
    ソフトウェアバージョン
    RADOSプールとCephFSファイルシステムの数とタイプ
    デフォルトから変更された設定オプションの名前 \(値ではない\)
    **basic**
* デーモンのタイプ
    デーモンのバージョン
    オペレーティングシステム \(OSディストリビューション、カーネルバージョン\)
    Cephコードのどこでクラッシュが発生したかを特定するスタックトレース
    **crash**
* 匿名化されたSMARTメトリクス
    **device**
* クラスタ説明
    連絡先メールアドレス
    **ident**

報告されるデータには、プール名、オブジェクト名、オブジェクトの内容、ホスト名、デバイスのシリアル番号などの機密データは含まれていません。

これには、クラスタの展開方法、Cephのバージョン、ホストの分布、Cephの使用方法をプロジェクトがより深く理解するのに役立つその他のパラメータに関するカウンタと統計が含まれています。

データはセキュアにhttps://telemetry.ceph.comに送信されます。

## Sample report[¶](https://docs.ceph.com/en/pacific/mgr/telemetry/ "Permalink to this headline")

どのようなデータが報告されているかは、コマンドでいつでも見ることができます：

```
ceph telemetry show
```

プライバシー保護のため、デバイスレポートは別途作成され、ホスト名やデバイスのシリアル番号などのデータは匿名化されています。デバイスの遠隔測定は別のエンドポイントに送信され、デバイスデータを特定のクラスターと関連付けることはありません。デバイスレポートのプレビューを表示するには、コマンドを使用します：

```
ceph telemetry show-device
```

注意：デバイスレポートを生成するために、JSON出力をサポートするSmartmontoolsバージョン7.0以降を使用しています。このレポートに含まれる情報に関してプライバシーに関する懸念がある場合は、Cephの開発者にお問い合わせください。

## Channels[¶](https://docs.ceph.com/en/pacific/mgr/telemetry/ "Permalink to this headline")

個別のチャンネルを有効・無効にすることができます：

```
ceph config set mgr mgr/telemetry/channel_ident false
ceph config set mgr mgr/telemetry/channel_basic false
ceph config set mgr mgr/telemetry/channel_crash false
ceph config set mgr mgr/telemetry/channel_device false
ceph telemetry show
ceph telemetry show-device
```

## Enabling Telemetry[¶](https://docs.ceph.com/en/pacific/mgr/telemetry/ "Permalink to this headline")

テレメトリーモジュールのデータ共有開始を許可するには：

```
ceph telemetry on
```

注意：テレメトリーデータは、Community Data License Agreement \- Sharing \- Version 1.0 \(https://cdla.io/sharing\-1\-0/\) に基づいてライセンスされています。したがって、'ceph telemetry on' コマンドに '\-license sharing\-1\-0' を追加して初めて、テレメトリーモジュールを有効にすることができるようになります。

でいつでもテレメトリーを無効にすることができます：

```
ceph telemetry off
```

## Interval[¶](https://docs.ceph.com/en/pacific/mgr/telemetry/ "Permalink to this headline")

このモジュールは、デフォルトで24時間ごとに新しいレポートをコンパイルして送信します。でこの間隔を調整することができます：

```
ceph config set mgr mgr/telemetry/interval 72    # report every three days
```

## Status[¶](https://docs.ceph.com/en/pacific/mgr/telemetry/ "Permalink to this headline")

現在のコンフィギュレーションを表示します：

```
ceph telemetry status
```

## Manually sending telemetry[¶](https://docs.ceph.com/en/pacific/mgr/telemetry/ "Permalink to this headline")

テレメトリーデータをアドホックに送信するには：

```
ceph telemetry send
```

テレメトリーが有効でない場合（「ceph telemetry on」の場合）、「ceph telemetry send」コマンドに「\-license sharing\-1\-0」を追加する必要があります。

## Sending telemetry through a proxy[¶](https://docs.ceph.com/en/pacific/mgr/telemetry/ "Permalink to this headline")

クラスタが設定されたtelemetryエンドポイント\(デフォルトのtelemetry.ceph.com\)に直接接続できない場合、HTTP/HTTPSプロキシサーバを使用して設定することができます：

```
ceph config set mgr mgr/telemetry/proxy https://10.0.0.1:8080
```

また、必要に応じてuser:passを含めることができます：

```
ceph config set mgr mgr/telemetry/proxy https://ceph:telemetry@10.0.0.1:8080
```

## Contact and Description[¶](https://docs.ceph.com/en/pacific/mgr/telemetry/ "Permalink to this headline")

報告書に連絡先と説明を追加することができます。 これは完全にオプションであり、デフォルトでは無効になっています：

```
ceph config set mgr mgr/telemetry/contact 'John Doe <john.doe@example.com>'
ceph config set mgr mgr/telemetry/description 'My first Ceph cluster'
ceph config set mgr mgr/telemetry/channel_ident true
```
