# 298: Alerts module

**クリップソース:** [298: Alerts module — Ceph Documentation](https://docs.ceph.com/en/pacific/mgr/alerts/)

# Alerts module[¶](https://docs.ceph.com/en/pacific/mgr/alerts/ "Permalink to this headline")

アラートモジュールは、クラスタの状態に関する簡単なアラートメッセージを電子メールで送信することができます。 将来的には、他の通知方法もサポートする予定です。

注：このモジュールは、堅牢な監視ソリューションを意図していません。 Cephクラスタ自体の一部であるため、ceph\-mgrデーモンに障害が発生すると、アラートが送信されないという根本的な制限があります。 このモジュールは、既存の監視インフラストラクチャが存在しないスタンドアロンクラスタに有用な場合があります。

## Enabling[¶](https://docs.ceph.com/en/pacific/mgr/alerts/ "Permalink to this headline")

alertsモジュールを有効にするには：

```
ceph mgr module enable alerts
```

## Configuration[¶](https://docs.ceph.com/en/pacific/mgr/alerts/ "Permalink to this headline")

SMTPを設定するには、以下のconfigオプションが全て設定されている必要があります：

```
ceph config set mgr mgr/alerts/smtp_host *<smtp-server>*
ceph config set mgr mgr/alerts/smtp_destination *<email-address-to-send-to>*
ceph config set mgr mgr/alerts/smtp_sender *<from-email-address>*
```

デフォルトでは、このモジュールはSSLとポート465を使用します。 それを変更するには：

```
ceph config set mgr mgr/alerts/smtp_ssl false   # if not SSL
ceph config set mgr mgr/alerts/smtp_port *<port-number>*  # if not 465
```

SMTPサーバーを認証するために、ユーザーとパスワードを設定する必要があります：

```
ceph config set mgr mgr/alerts/smtp_user *<username>*
ceph config set mgr mgr/alerts/smtp_password *<password>*
```

デフォルトでは、From:行の名前は単にCephです。 これを変更するには\(例えば、クラスタを識別するには\)：

```
ceph config set mgr mgr/alerts/smtp_from_name 'Ceph Cluster Foo'
```

デフォルトでは、モジュールは1分に1回クラスタの健全性をチェックし、変化があればメッセージを送信します。 その頻度を変更するには：

```
ceph config set mgr mgr/alerts/interval *<interval>*   # e.g., "5m" for 5 minutes
```

## Commands[¶](https://docs.ceph.com/en/pacific/mgr/alerts/ "Permalink to this headline")

アラートを即座に送信するには：

```
ceph alerts send
```
