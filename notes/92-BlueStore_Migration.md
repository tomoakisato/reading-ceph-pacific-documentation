# 92: BlueStore Migration

**クリップソース:** [92: BlueStore Migration — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/operations/bluestore-migration/)

# BlueStore Migration[¶](https://docs.ceph.com/en/pacific/rados/operations/bluestore-migration/ "Permalink to this headline")

各OSDはBlueStoreまたはFileStoreのいずれかを実行でき、1つのCephクラスタに両方を混在させることができます。 以前FileStoreを導入していたユーザーは、向上したパフォーマンスと堅牢性を活用するために、BlueStoreへの移行を希望する可能性が高いです。 このような移行を行うには、いくつかの戦略があります。

しかし、個々のOSDを単独でその場に変換することはできません。BlueStoreとFileStoreはあまりにも異なるため、実用的ではありません。 「変換」は、クラスタの通常のレプリケーションとヒーリングサポート、あるいは古い（FileStore）デバイスから新しい（BlueStore）デバイスにOSDコンテンツをコピーするツールや戦略のいずれかに依存することになります。

## Deploy new OSDs with BlueStore[¶](https://docs.ceph.com/en/pacific/rados/operations/bluestore-migration/ "Permalink to this headline")

新しいOSD（クラスタの拡張時など）は、BlueStoreを使用してデプロイすることができます。 これはデフォルトの動作なので、特に変更の必要はありません。

同様に、故障したドライブを交換した後に再プロビジョニングされたOSDも、BlueStoreを使用することができます。

## Convert existing OSDs[¶](https://docs.ceph.com/en/pacific/rados/operations/bluestore-migration/ "Permalink to this headline")

### Mark out and replace[¶](https://docs.ceph.com/en/pacific/rados/operations/bluestore-migration/ "Permalink to this headline")

最もシンプルな方法は、各デバイスを順番にマークoutし、クラスタ全体でデータがレプリケートされるのを待ち、OSDを再プロビジョニングし、再びマークinする方法です。 これはシンプルで自動化しやすい方法です。 しかし、必要以上のデータ移行が必要になるため、最適とは言えません。

1. 交換するFileStore OSDを特定する

```
ID=<osd-id-number>
DEVICE=<disk-device>
```

あるOSDがFileStoreなのかBlueStoreなのかはを見分けるには：

```
ceph osd metadata $ID | grep osd_objectstore
```

filestore vs bluestoreの現在のカウントを取得するには：

```
ceph osd count-metadata osd_objectstore
```

1. filestore OSDをマークoutする

```
ceph osd out $ID
```

1. 当該OSDからデータが移行されるのを待つ

```
while ! ceph osd safe-to-destroy $ID ; do sleep 60 ; done
```

1. OSDを停止する：

```
systemctl kill ceph-osd@$ID
```

1. このOSDがどのデバイスを使用しているかをメモしておく：

```
mount | grep /var/lib/ceph/osd/ceph-$ID
```

1. OSDをアンマウントする：

```
umount /var/lib/ceph/osd/ceph-$ID
```

1. OSDデータを破棄する。デバイス上のデータが不要であること（クラスタが健全であること）を確認してから行ってください。

```
ceph-volume lvm zap $DEVICE
```

1. クラスタにOSDが破壊されたことを伝える（同じIDで新しいOSDを再プロビジョニングすることができる）

```
ceph osd destroy $ID --yes-i-really-mean-it
```

1. 同じOSD IDでBlueStore OSDをその場所に再プロビジョニングする。そのためには、上記の内容に基づいて、どのデバイスをワイプするのかを特定する必要があります。注意してください。

```
ceph-volume lvm create --bluestore --data $DEVICE --osd-id $ID
```

1. 繰り返し.

OSDを破壊する前に、クラスタが完全にクリーンであること（すべてのデータがすべてのレプリカを持つこと）を確認する限り、交換用OSDの再充填は次のOSDの排出と同時に行わせることも、複数のOSDに対して同じ手順を並行して行うことも可能です。 これを行わないと、データの冗長性が低下し、データ損失のリスクが高まります（また、原因となる可能性もあります）。

メリット：

* シンプル.
* デバイス単位で可能
* 予備のデバイスやホストは不要です

デメリット：

* データはネットワーク経由で2回コピーされます。1回目は（レプリカの数を維持するため）クラスタ内の他のOSDにコピー、2回目は再プロビジョニングしたBlueStore OSDに戻すコピー。

### Whole host replacement[¶](https://docs.ceph.com/en/pacific/rados/operations/bluestore-migration/ "Permalink to this headline")

クラスタ内に予備ホストがある場合、または予備として使用するためにホスト全体を退避させるのに十分な空き領域がある場合は、データの各保存コピーを1回だけ移行して、ホスト単位で変換を行うことができます。

まず、データのない空のホストを用意する必要があります。 これを行うには2つの方法があります: まだクラスタの一部ではない、新しい空のホストから始めるか、クラスタ内の既存のホストからデータをオフロードするか、どちらかです。

#### Use a new, empty host[¶](https://docs.ceph.com/en/pacific/rados/operations/bluestore-migration/ "Permalink to this headline")

（厳密には関係ありませんが）理想的には、変換する他のホストとほぼ同じ容量であることが望ましいです。

```
NEWHOST=<empty-host-name>
```

ホストをCRUSHの階層に追加しますが、rootにはアタッチしないでください。

```
ceph osd crush add-bucket $NEWHOST host
```

cephのパッケージがインストールされていることを確認します。

#### Use an existing host[¶](https://docs.ceph.com/en/pacific/rados/operations/bluestore-migration/ "Permalink to this headline")

すでにクラスタの一部となっている既存のホストを使用し、そのホストに十分な空き領域があり、そのデータをすべて移行できる場合は、代わりにこの方法を使用できます。

```
OLDHOST=<existing-cluster-host-to-offload>
ceph osd crush unlink $OLDHOST default
```

ここで、"default "はCRUSHマップの直系の祖先です。\(構成が変更されていない小規模なクラスタでは、これは通常「default」になりますが、ラック名である場合もあります\)。 これで、OSD ツリー出力の最上位に、親を持たないホストが表示されるはずです。

```
$ bin/ceph osd tree
ID CLASS WEIGHT  TYPE NAME     STATUS REWEIGHT PRI-AFF
-5             0 host oldhost
10   ssd 1.00000     osd.10        up  1.00000 1.00000
11   ssd 1.00000     osd.11        up  1.00000 1.00000
12   ssd 1.00000     osd.12        up  1.00000 1.00000
-1       3.00000 root default
-2       3.00000     host foo
 0   ssd 1.00000         osd.0     up  1.00000 1.00000
 1   ssd 1.00000         osd.1     up  1.00000 1.00000
 2   ssd 1.00000         osd.2     up  1.00000 1.00000
...
```

問題がないようであれば、そのまま下の「データ移行が完了するのを待ちます：」のステップに飛び、そこから古いOSDのクリーンアップに進みます。

#### Migration process[¶](https://docs.ceph.com/en/pacific/rados/operations/bluestore-migration/ "Permalink to this headline")

新しいホストを使用する場合は、手順 1 から始めてください。 既存のホストの場合は、以下のステップ\#5に進んでください。

1. すべてのデバイスに新しいBlueStore OSDをプロビジョニングします

```
ceph-volume lvm create --bluestore --data /dev/$DEVICE
```

1. OSDがクラスタに参加することを確認する：

```
ceph osd tree
```

新しいホスト $NEWHOST とその下のすべての OSD が見えるはずですが、ホストは他の階層のノード \(root default など\) の下にネストされてはいけません。 たとえば、newhost が空のホストである場合、次のように表示されるでしょう：

```
$ bin/ceph osd tree
ID CLASS WEIGHT  TYPE NAME     STATUS REWEIGHT PRI-AFF
-5             0 host newhost
10   ssd 1.00000     osd.10        up  1.00000 1.00000
11   ssd 1.00000     osd.11        up  1.00000 1.00000
12   ssd 1.00000     osd.12        up  1.00000 1.00000
-1       3.00000 root default
-2       3.00000     host oldhost1
 0   ssd 1.00000         osd.0     up  1.00000 1.00000
 1   ssd 1.00000         osd.1     up  1.00000 1.00000
 2   ssd 1.00000         osd.2     up  1.00000 1.00000
...
```

1. 最初にコンバートするターゲットホストを特定する

```
OLDHOST=<existing-cluster-host-to-convert>
```

1. 新しいホストをクラスタ内の古いホストの位置にスワップします

```
ceph osd crush swap-bucket $NEWHOST $OLDHOST
```

この時点で、$OLDHOST上のすべてのデータは$NEWHOST上のOSDに移行を開始します。 新旧のホストの総容量に差がある場合、クラスタ内の他のノードとの間でデータの移行が行われるかもしれませんが、ホストのサイズが同じであれば、これは比較的小さなデータ量になるでしょう。

1. データ移行が完了するのを待ちます：

```
while ! ceph osd safe-to-destroy $(ceph osd ls-tree $OLDHOST); do sleep 60 ; done
```

1. 今は空っぽの$OLDHOST上のすべての古いOSDを停止します：

```
ssh $OLDHOST
systemctl kill ceph-osd.target
umount /var/lib/ceph/osd/ceph-*
```

1. 古いOSDを破棄し、パージする

```
for osd in `ceph osd ls-tree $OLDHOST`; do
    ceph osd purge $osd --yes-i-really-mean-it
done
```

1. 古いOSDデバイスをワイプします。この作業では、どのデバイスをワイプするのかを手動で確認する必要があります（慎重に！）。各デバイスについて：

```
ceph-volume lvm zap $DEVICE
```

1. 現在空になっているホストを新しいホストとして使用し、これを繰り返します：

```
NEWHOST=$OLDHOST
```

メリット：

* データはネットワーク上に一度だけコピーされます
* ホスト全体のOSDを一度に変換します
* 並列化により、複数のホストを同時に変換することができます
* 各ホストに予備のデバイスは必要ありません

デメリット：

* 予備ホストが必要です
* ホスト全体のOSDが一度にデータを移行することになります。 これは、クラスタ全体の性能に影響を与える可能性が高いようです
* 移行されたデータはすべて、ネットワーク上を1回フルホップすることに変わりはありません

### Per\-OSD device copy[¶](https://docs.ceph.com/en/pacific/rados/operations/bluestore-migration/ "Permalink to this headline")

ceph\-objectstore\-toolのコピー機能を使用すると、1つの論理OSDを変換することができます。 これには、新しい空のBlueStore OSDをプロビジョニングするために、ホストに空きデバイス\(1つまたは複数\)があることが必要です。 たとえば、クラスタの各ホストに12のOSDがある場合、次のOSDを変換するために古いデバイスが回収される前に各OSDを順番に変換できるように、13番目の空きデバイスが必要になります。

注意事項：

* この方法では、新しいOSD IDを割り当てずに、空のBlueStore OSDを準備する必要がありますが、これはceph\-volumeツールがサポートしていないものです。 さらに重要なのは、dmcryptのセットアップがOSD IDと密接に結びついているため、この方法は暗号化されたOSDでは機能しないことです
* デバイスを手動でパーティション分割する必要があります
* ツールは実装されていない！？
* 文書化されていない\!

メリット：

* 変換中にネットワーク上を移動するデータはほとんどない

デメリット：

* ツールは完全に実装されていない
* プロセスが文書化されていない
* 各ホストは、予備または空のデバイスを持つ必要があります
* 変換中はOSDがオフラインになるため、新しい書き込みはOSDのサブセットのみに書き込まれることになります。 そのため、その後の障害によるデータ消失のリスクが高まります。 \(ただし、変換が完了する前に障害が発生した場合は、元のFileStore OSDを起動して、元のデータへのアクセスを提供することができます\)。
