# 4: OS Recommendations

**クリップソース:** [4: OS Recommendations — Ceph Documentation](https://docs.ceph.com/en/pacific/start/os-recommendations/)

# OS Recommendations¶

## **Ceph Dependencies[¶](https://docs.ceph.com/en/pacific/start/os-recommendations/#ceph-dependencies "Permalink to this headline")**

原則として、新しいリリースのLinuxにCephを導入することをお勧めします。また、長期サポートのあるリリースに導入することをお勧めします。

### Linux Kernel[¶](https://docs.ceph.com/en/pacific/start/os-recommendations/#linux-kernel "Permalink to this headline")

* カーネルクライアントを使用してRBDブロックデバイスをマッピングしたりCephFSをマウントしたりする場合、一般的なアドバイスとして、クライアントホストには、http://kernel.org またはお使いのLinuxディストリビューションが提供する「安定版」または「長期保守版」のカーネルシリーズを使用してください。
    RBDでは、長期のカーネルを追跡する場合、現在は4.xベースの「長期保守」カーネルシリーズ以降を推奨しています。
    **Ceph Kernel Client**
    * 4.19.z
    * 4.14.z
    * 5.x

CephFSの場合、カーネルバージョンのガイダンスについては、「[Mounting CephFS using Kernel Driver](https://docs.ceph.com/en/pacific/cephfs/mount-using-kernel-driver#which-kernel-version)」の項を参照してください。[150: Mount CephFS using Kernel Driver](150-Mount_CephFS_using_Kernel_Driver.md)

古いカーネルクライアントのバージョンでは、[CRUSH tunables](https://docs.ceph.com/en/pacific/rados/operations/crush-map#tunables)プロファイルやCephクラスタのその他の新機能がサポートされない場合があり、これらの機能を無効にしてストレージクラスタを構成する必要があります。

## **Platforms[¶](https://docs.ceph.com/en/pacific/start/os-recommendations/#platforms "Permalink to this headline")**

以下の図は、Cephの要件がさまざまなLinuxプラットフォームにどのようにマッピングされるかを示しています。 一般的に、カーネルとシステム初期化パッケージ\(sysvinit、upstart、systemdなど\)を除いて、特定のディストリビューションへの依存はほとんどありません。

### Octopus \(15.2.z\)[¶](https://docs.ceph.com/en/pacific/start/os-recommendations/#octopus-15-2-z "Permalink to this headline")

|Distro  |Release|Code Name    |Kernel       |Notes|Testing|
|--------|-------|-------------|-------------|-----|-------|
|CentOS  |8      |N/A          |linux\-4.18  |     |B, I, C|
|CentOS  |7      |N/A          |linux\-3.10.0|4, 5 |B, I   |
|Debian  |10     |Buster       |linux\-4.19  |     |B      |
|RHEL    |8      |Ootpa        |linux\-4.18  |     |B, I, C|
|RHEL    |7      |Maipo        |linux\-3.10.0|     |B, I   |
|Ubuntu  |18.04  |Bionic Beaver|linux\-4.15  |4    |B, I, C|
|openSUSE|15.2   |Leap         |linux\-5.3   |6    |       |
|openSUSE|       |Tumbleweed   |             |     |       |

### Nautilus \(14.2.z\)[¶](https://docs.ceph.com/en/pacific/start/os-recommendations/#nautilus-14-2-z "Permalink to this headline")

|Distro  |Release|Code Name    |Kernel       |Notes|Testing|
|--------|-------|-------------|-------------|-----|-------|
|CentOS  |7      |N/A          |linux\-3.10.0|3    |B, I, C|
|Debian  |8.0    |Jessie       |linux\-3.16.0|1, 2 |B, I   |
|Debian  |9.0    |Stretch      |linux\-4.9   |1, 2 |B, I   |
|RHEL    |7      |Maipo        |linux\-3.10.0|     |B, I   |
|Ubuntu  |14.04  |Trusty Tahr  |linux\-3.13.0|     |B, I, C|
|Ubuntu  |16.04  |Xenial Xerus |linux\-4.4.0 |3    |B, I, C|
|Ubuntu  |18.04  |Bionic Beaver|linux\-4.15  |3    |B, I, C|
|openSUSE|15.1   |Leap         |linux\-4.12  |6    |       |

### Luminous \(12.2.z\)[¶](https://docs.ceph.com/en/pacific/start/os-recommendations/#luminous-12-2-z "Permalink to this headline")

|Distro|Release|Code Name   |Kernel       |Notes|Testing|
|------|-------|------------|-------------|-----|-------|
|CentOS|7      |N/A         |linux\-3.10.0|3    |B, I, C|
|Debian|8.0    |Jessie      |linux\-3.16.0|1, 2 |B, I   |
|Debian|9.0    |Stretch     |linux\-4.9   |1, 2 |B, I   |
|Fedora|22     |N/A         |linux\-3.14.0|     |B, I   |
|RHEL  |7      |Maipo       |linux\-3.10.0|     |B, I   |
|Ubuntu|14.04  |Trusty Tahr |linux\-3.13.0|     |B, I, C|
|Ubuntu|16.04  |Xenial Xerus|linux\-4.4.0 |3    |B, I, C|

### Notes[¶](https://docs.ceph.com/en/pacific/start/os-recommendations/#notes "Permalink to this headline")

* **1:** デフォルトのカーネルには古いバージョンのBtrfsが含まれていますが、これはceph-osdのストレージノードにはお勧めできません。LuminousからはBlueStoreを、以前のリリースではFilestoreでXFSを使用することをお勧めします。
* **2:** デフォルトのカーネルには古いCephクライアントがあり、カーネルクライアント(カーネルRBDまたはCephファイルシステム)には推奨されません。 推奨カーネルにアップグレードしてください。
* **3:** Btrfsファイルシステムが使用されている場合、デフォルトのカーネルはQAで定期的に失敗します。Luminous」からはBlueStoreを、「Filestore」を搭載した以前のリリースではXFSを使用することをお勧めします。
* **4:** 本リリースでは、btrfsのテストは行われていません。bluestoreの使用をお勧めします。
* **5:** ダッシュボードに関連する一部の追加機能は利用できません。
* **6:** パッケージは定期的にビルドされますが、アップストリームCephでは配布されません。

### Testing[¶](https://docs.ceph.com/en/pacific/start/os-recommendations/#testing "Permalink to this headline")

* **B:** このプラットフォームのリリースパッケージを構築します。これらのプラットフォームの中には、すべてのCephブランチを継続的に構築し、基本的なユニットテストを実施することもあります。
* **I:** このプラットフォームでリリースされた製品の基本的なインストールと機能テストを行います。
* **C:** このプラットフォーム上で、包括的な機能テスト、回帰テスト、およびストレステストを継続的に実施しています。これには、開発ブランチ、プレリリース、リリースされたコードが含まれます。
