# 367: msgr2 protocol (msgr2.0 and msgr2.1) — Ceph Documentation

 # msgr2 protocol \(msgr2.0 and msgr2.1\)[¶](https://docs.ceph.com/en/pacific/dev/msgr2/ "Permalink to this headline")

This is a revision of the legacy Ceph on\-wire protocol that was implemented by the SimpleMessenger. It addresses performance and security issues.

## Goals[¶](https://docs.ceph.com/en/pacific/dev/msgr2/ "Permalink to this headline")

This protocol revision has several goals relative to the original protocol:

* _Flexible handshaking_. The original protocol did not have a sufficiently flexible protocol negotiation that allows for features that were not required.
* _Encryption_. We will incorporate encryption over the wire.
* _Performance_. We would like to provide for protocol features \(e.g., padding\) that keep computation and memory copies out of the fast path where possible.
* _Signing_. We will allow for traffic to be signed \(but not necessarily encrypted\). This is not implemented.

## Definitions[¶](https://docs.ceph.com/en/pacific/dev/msgr2/ "Permalink to this headline")

* _client_ \(C\): the party initiating a \(TCP\) connection
* _server_ \(S\): the party accepting a \(TCP\) connection
* _connection_: an instance of a \(TCP\) connection between two processes.
* _entity_: a ceph entity instantiation, e.g. ‘osd.0’. each entity has one or more unique entity\_addr\_t’s by virtue of the ‘nonce’ field, which is typically a pid or random value.
* _session_: a stateful session between two entities in which message exchange is ordered and lossless. A session might span multiple connections if there is an interruption \(TCP connection disconnect\).
* _frame_: a discrete message sent between the peers. Each frame consists of a tag \(type code\), payload, and \(if signing or encryption is enabled\) some other fields. See below for the structure.
* _tag_: a type code associated with a frame. The tag determines the structure of the payload.

## Phases[¶](https://docs.ceph.com/en/pacific/dev/msgr2/ "Permalink to this headline")

A connection has four distinct phases:

1. banner
2. authentication frame exchange
3. message flow handshake frame exchange
4. message frame exchange

## Banner[¶](https://docs.ceph.com/en/pacific/dev/msgr2/ "Permalink to this headline")

Both the client and server, upon connecting, send a banner:

```
"ceph v2n"
__le16 banner payload length
banner payload
```

A banner payload has the form:

```
__le64 peer_supported_features
__le64 peer_required_features
```

This is a new, distinct feature bit namespace \(CEPH\_MSGR2\_\*\). Currently, only CEPH\_MSGR2\_FEATURE\_REVISION\_1 is defined. It is supported but not required, so that msgr2.0 and msgr2.1 peers can talk to each other.

If the remote party advertises required features we don’t support, we can disconnect.

![b420ffeb3611a894ca699b51e980bf5035b37dd95d14871a58547c3775e513e0.png](image/b420ffeb3611a894ca699b51e980bf5035b37dd95d14871a58547c3775e513e0.png)

## Frame format[¶](https://docs.ceph.com/en/pacific/dev/msgr2/ "Permalink to this headline")

After the banners are exchanged, all further communication happens in frames. The exact format of the frame depends on the connection mode \(msgr2.0\-crc, msgr2.0\-secure, msgr2.1\-crc or msgr2.1\-secure\). All connections start in crc mode \(either msgr2.0\-crc or msgr2.1\-crc, depending on peer\_supported\_features from the banner\).

Each frame has a 32\-byte preamble:

```
__u8 tag
__u8 number of segments
{
  __le32 segment length
  __le16 segment alignment
} * 4
reserved (2 bytes)
__le32 preamble crc
```

An empty frame has one empty segment. A non\-empty frame can have between one and four segments, all segments except the last may be empty.

If there are less than four segments, unused \(trailing\) segment length and segment alignment fields are zeroed.

The reserved bytes are zeroed.

The preamble checksum is CRC32\-C. It covers everything up to itself \(28 bytes\) and is calculated and verified irrespective of the connection mode \(i.e. even if the frame is encrypted\).

\#\#\# msgr2.0\-crc mode

A msgr2.0\-crc frame has the form:

```
preamble (32 bytes)
{
  segment payload
} * number of segments
epilogue (17 bytes)
```

where epilogue is:

```
__u8 late_flags
{
  __le32 segment crc
} * 4
```

late\_flags is used for frame abortion. After transmitting the preamble and the first segment, the sender can fill the remaining segments with zeros and set a flag to indicate that the receiver must drop the frame. This allows the sender to avoid extra buffering when a frame that is being put on the wire is revoked \(i.e. yanked out of the messenger\): payload buffers can be unpinned and handed back to the user immediately, without making a copy or blocking until the whole frame is transmitted. Currently this is used only by the kernel client, see ceph\_msg\_revoke\(\).

The segment checksum is CRC32\-C. For “used” empty segments, it is set to \(\_\_le32\)\-1. For unused \(trailing\) segments, it is zeroed.

The crcs are calculated just to protect against bit errors. No authenticity guarantees are provided, unlike in msgr1 which attempted to provide some authenticity guarantee by optionally signing segment lengths and crcs with the session key.

Issues:

1. As part of introducing a structure for a generic frame with variable number of segments suitable for both control and message frames, msgr2.0 moved the crc of the first segment of the message frame \(ceph\_msg\_header2\) into the epilogue.
    As a result, ceph\_msg\_header2 can no longer be safely interpreted before the whole frame is read off the wire. This is a regression from msgr1, because in order to scatter the payload directly into user\-provided buffers and thus avoid extra buffering and copying when receiving message frames, ceph\_msg\_header2 must be available in advance – it stores the transaction id which the user buffers are keyed on. The implementation has to choose between forgoing this optimization or acting on an unverified segment.
2. late\_flags is not covered by any crc. Since it stores the abort flag, a single bit flip can result in a completed frame being dropped \(causing the sender to hang waiting for a reply\) or, worse, in an aborted frame with garbage segment payloads being dispatched.
    This was the case with msgr1 and got carried over to msgr2.0.

\#\#\# msgr2.1\-crc mode

Differences from msgr2.0\-crc:

1. The crc of the first segment is stored at the end of the first segment, not in the epilogue. The epilogue stores up to three crcs, not up to four.
    If the first segment is empty, \(\_\_le32\)\-1 crc is not generated.
2. The epilogue is generated only if the frame has more than one segment \(i.e. at least one of second to fourth segments is not empty\). Rationale: If the frame has only one segment, it cannot be aborted and there are no crcs to store in the epilogue.
3. Unchecksummed late\_flags is replaced with late\_status which builds in bit error detection by using a 4\-bit nibble per flag and two code words that are Hamming Distance = 4 apart \(and not all zeros or ones\). This comes at the expense of having only one reserved flag, of course.

Some example frames:

* A 0\+0\+0\+0 frame \(empty, no epilogue\):
    ```
    preamble (32 bytes)
    ```
* A 20\+0\+0\+0 frame \(no epilogue\):
    ```
    preamble (32 bytes)
    segment1 payload (20 bytes)
    __le32 segment1 crc
    ```
* A 0\+70\+0\+0 frame:
    ```
    preamble (32 bytes)
    segment2 payload (70 bytes)
    epilogue (13 bytes)
    ```
* A 20\+70\+0\+350 frame:
    ```
    preamble (32 bytes)
    segment1 payload (20 bytes)
    __le32 segment1 crc
    segment2 payload (70 bytes)
    segment4 payload (350 bytes)
    epilogue (13 bytes)
    ```

where epilogue is:

```
__u8 late_status
{
  __le32 segment crc
} * 3
```

## Hello[¶](https://docs.ceph.com/en/pacific/dev/msgr2/ "Permalink to this headline")

* TAG\_HELLO: client\-\>server and server\-\>client:
    ```
    __u8 entity_type
    entity_addr_t peer_socket_address
    ```
    * We immediately share our entity type and the address of the peer \(which can be useful for detecting our effective IP address, especially in the presence of NAT\).

## Authentication[¶](https://docs.ceph.com/en/pacific/dev/msgr2/ "Permalink to this headline")

* TAG\_AUTH\_REQUEST: client\-\>server:
    ```
    __le32 method;  // CEPH_AUTH_{NONE, CEPHX, ...}
    __le32 num_preferred_modes;
    list<__le32> mode  // CEPH_CON_MODE_*
    method specific payload
    ```
* TAG\_AUTH\_BAD\_METHOD server \-\> client: reject client\-selected auth method:
    ```
    __le32 method
    __le32 negative error result code
    __le32 num_methods
    list<__le32> allowed_methods // CEPH_AUTH_{NONE, CEPHX, ...}
    __le32 num_modes
    list<__le32> allowed_modes   // CEPH_CON_MODE_*
    ```
    * Returns the attempted auth method, and error code \(\-EOPNOTSUPP if the method is unsupported\), and the list of allowed authentication methods.
* TAG\_AUTH\_REPLY\_MORE: server\-\>client:
    ```
    __le32 len;
    method specific payload
    ```
* TAG\_AUTH\_REQUEST\_MORE: client\-\>server:
    ```
    __le32 len;
    method specific payload
    ```
* TAG\_AUTH\_DONE: \(server\-\>client\):
    ```
    __le64 global_id
    __le32 connection mode // CEPH_CON_MODE_*
    method specific payload
    ```
    * The server is the one to decide authentication has completed and what the final connection mode will be.

Example of authentication phase interaction when the client uses an allowed authentication method:

![3cf1b28cac2a199c528b25e601db0a1a3838343706167ee92349af6c73811481.png](image/3cf1b28cac2a199c528b25e601db0a1a3838343706167ee92349af6c73811481.png)

Example of authentication phase interaction when the client uses a forbidden authentication method as the first attempt:

![937a2c848ff67d1831ed7f4a8dc48437be5a2474bfb3551a7db25e98a8baf01b.png](image/937a2c848ff67d1831ed7f4a8dc48437be5a2474bfb3551a7db25e98a8baf01b.png)

## Post\-auth frame format[¶](https://docs.ceph.com/en/pacific/dev/msgr2/ "Permalink to this headline")

Depending on the negotiated connection mode from TAG\_AUTH\_DONE, the connection either stays in crc mode or switches to the corresponding secure mode \(msgr2.0\-secure or msgr2.1\-secure\).

\#\#\# msgr2.0\-secure mode

A msgr2.0\-secure frame has the form:

```
{
  preamble (32 bytes)
  {
    segment payload
    zero padding (out to 16 bytes)
  } * number of segments
  epilogue (16 bytes)
} ^ AES-128-GCM cipher
auth tag (16 bytes)
```

where epilogue is:

```
__u8 late_flags
zero padding (15 bytes)
```

late\_flags has the same meaning as in msgr2.0\-crc mode.

Each segment and the epilogue are zero padded out to 16 bytes. Technically, GCM doesn’t require any padding because Counter mode \(the C in GCM\) essentially turns a block cipher into a stream cipher. But, if the overall input length is not a multiple of 16 bytes, some implicit zero padding would occur internally because GHASH function used by GCM for generating auth tags only works on 16\-byte blocks.

Issues:

1. The sender encrypts the whole frame using a single nonce and generating a single auth tag. Because segment lengths are stored in the preamble, the receiver has no choice but to decrypt and interpret the preamble without verifying the auth tag – it can’t even tell how much to read off the wire to get the auth tag otherwise\! This creates a decryption oracle, which, in conjunction with Counter mode malleability, could lead to recovery of sensitive information.
    This issue extends to the first segment of the message frame as well. As in msgr2.0\-crc mode, ceph\_msg\_header2 cannot be safely interpreted before the whole frame is read off the wire.
2. Deterministic nonce construction with a 4\-byte counter field followed by an 8\-byte fixed field is used. The initial values are taken from the connection secret – a random byte string generated during the authentication phase. Because the counter field is only four bytes long, it can wrap and then repeat in under a day, leading to GCM nonce reuse and therefore a potential complete loss of both authenticity and confidentiality for the connection. This was addressed by disconnecting before the counter repeats \(CVE\-2020\-1759\).

\#\#\# msgr2.1\-secure mode

Differences from msgr2.0\-secure:

1. The preamble, the first segment and the rest of the frame are encrypted separately, using separate nonces and generating separate auth tags. This gets rid of unverified plaintext use and keeps msgr2.1\-secure mode close to msgr2.1\-crc mode, allowing the implementation to receive message frames in a similar fashion \(little to no buffering, same scatter/gather logic, etc\).
    In order to reduce the number of en/decryption operations per frame, the preamble is grown by a fixed size inline buffer \(48 bytes\) that the first segment is inlined into, either fully or partially. The preamble auth tag covers both the preamble and the inline buffer, so if the first segment is small enough to be fully inlined, it becomes available after a single decryption operation.
2. As in msgr2.1\-crc mode, the epilogue is generated only if the frame has more than one segment. The rationale is even stronger, as it would require an extra en/decryption operation.
3. For consistency with msgr2.1\-crc mode, late\_flags is replaced with late\_status \(the built\-in bit error detection isn’t really needed in secure mode\).
4. In accordance with [NIST Recommendation for GCM](https://docs.ceph.com/en/pacific/dev/msgr2/), deterministic nonce construction with a 4\-byte fixed field followed by an 8\-byte counter field is used. An 8\-byte counter field should never repeat but the nonce reuse protection put in place for msgr2.0\-secure mode is still there.
    The initial values are the same as in msgr2.0\-secure mode.

As in msgr2.0\-secure mode, each segment is zero padded out to 16 bytes. If the first segment is fully inlined, its padding goes to the inline buffer. Otherwise, the padding is on the remainder. The corollary to this is that the inline buffer is consumed in 16\-byte chunks.

The unused portion of the inline buffer is zeroed.

Some example frames:

* A 0\+0\+0\+0 frame \(empty, nothing to inline, no epilogue\):
    ```
    {
      preamble (32 bytes)
      zero padding (48 bytes)
    } ^ AES-128-GCM cipher
    auth tag (16 bytes)
    ```
* A 20\+0\+0\+0 frame \(first segment fully inlined, no epilogue\):
    ```
    {
      preamble (32 bytes)
      segment1 payload (20 bytes)
      zero padding (28 bytes)
    } ^ AES-128-GCM cipher
    auth tag (16 bytes)
    ```
* A 0\+70\+0\+0 frame \(nothing to inline\):
    ```
    {
      preamble (32 bytes)
      zero padding (48 bytes)
    } ^ AES-128-GCM cipher
    auth tag (16 bytes)
    {
      segment2 payload (70 bytes)
      zero padding (10 bytes)
      epilogue (16 bytes)
    } ^ AES-128-GCM cipher
    auth tag (16 bytes)
    ```
* A 20\+70\+0\+350 frame \(first segment fully inlined\):
    ```
    {
      preamble (32 bytes)
      segment1 payload (20 bytes)
      zero padding (28 bytes)
    } ^ AES-128-GCM cipher
    auth tag (16 bytes)
    {
      segment2 payload (70 bytes)
      zero padding (10 bytes)
      segment4 payload (350 bytes)
      zero padding (2 bytes)
      epilogue (16 bytes)
    } ^ AES-128-GCM cipher
    auth tag (16 bytes)
    ```
* A 105\+0\+0\+0 frame \(first segment partially inlined, no epilogue\):
    ```
    {
      preamble (32 bytes)
      segment1 payload (48 bytes)
    } ^ AES-128-GCM cipher
    auth tag (16 bytes)
    {
      segment1 payload remainder (57 bytes)
      zero padding (7 bytes)
    } ^ AES-128-GCM cipher
    auth tag (16 bytes)
    ```
* A 105\+70\+0\+350 frame \(first segment partially inlined\):
    ```
    {
      preamble (32 bytes)
      segment1 payload (48 bytes)
    } ^ AES-128-GCM cipher
    auth tag (16 bytes)
    {
      segment1 payload remainder (57 bytes)
      zero padding (7 bytes)
    } ^ AES-128-GCM cipher
    auth tag (16 bytes)
    {
      segment2 payload (70 bytes)
      zero padding (10 bytes)
      segment4 payload (350 bytes)
      zero padding (2 bytes)
      epilogue (16 bytes)
    } ^ AES-128-GCM cipher
    auth tag (16 bytes)
    ```

where epilogue is:

```
__u8 late_status
zero padding (15 bytes)
```

late\_status has the same meaning as in msgr2.1\-crc mode.

## Message flow handshake[¶](https://docs.ceph.com/en/pacific/dev/msgr2/ "Permalink to this headline")

In this phase the peers identify each other and \(if desired\) reconnect to an established session.

* TAG\_CLIENT\_IDENT \(client\-\>server\): identify ourselves:
    ```
    __le32 num_addrs
    entity_addrvec_t*num_addrs entity addrs
    entity_addr_t target entity addr
    __le64 gid (numeric part of osd.0, client.123456, ...)
    __le64 global_seq
    __le64 features supported (CEPH_FEATURE_* bitmask)
    __le64 features required (CEPH_FEATURE_* bitmask)
    __le64 flags (CEPH_MSG_CONNECT_* bitmask)
    __le64 cookie
    ```
    * client will send first, server will reply with same. if this is a new session, the client and server can proceed to the message exchange.
    * the target addr is who the client is trying to connect _to_, so that the server side can close the connection if the client is talking to the wrong daemon.
    * type.gid \(entity\_name\_t\) is set here, by combinging the type shared in the hello frame with the gid here. this means we don’t need it in the header of every message. it also means that we can’t send messages “from” other entity\_name\_t’s. the current implementations set this at the top of \_send\_message etc so this shouldn’t break any existing functionality. implementation will likely want to mask this against what the authenticated credential allows.
    * cookie is the client coookie used to identify a session, and can be used to reconnect to an existing session.
    * we’ve dropped the ‘protocol\_version’ field from msgr1
* TAG\_IDENT\_MISSING\_FEATURES \(server\-\>client\): complain about a TAG\_IDENT with too few features:
    ```
    __le64 features we require that the peer didn't advertise
    ```
* TAG\_SERVER\_IDENT \(server\-\>client\): accept client ident and identify server:
    ```
    __le32 num_addrs
    entity_addrvec_t*num_addrs entity addrs
    __le64 gid (numeric part of osd.0, client.123456, ...)
    __le64 global_seq
    __le64 features supported (CEPH_FEATURE_* bitmask)
    __le64 features required (CEPH_FEATURE_* bitmask)
    __le64 flags (CEPH_MSG_CONNECT_* bitmask)
    __le64 cookie
    ```
    * The server cookie can be used by the client if it is later disconnected and wants to reconnect and resume the session.
* TAG\_RECONNECT \(client\-\>server\): reconnect to an established session:
    ```
    __le32 num_addrs
    entity_addr_t * num_addrs
    __le64 client_cookie
    __le64 server_cookie
    __le64 global_seq
    __le64 connect_seq
    __le64 msg_seq (the last msg seq received)
    ```
* TAG\_RECONNECT\_OK \(server\-\>client\): acknowledge a reconnect attempt:
    ```
    __le64 msg_seq (last msg seq received)
    ```
    * once the client receives this, the client can proceed to message exchange.
    * once the server sends this, the server can proceed to message exchange.
* TAG\_RECONNECT\_RETRY\_SESSION \(server only\): fail reconnect due to stale connect\_seq
* TAG\_RECONNECT\_RETRY\_GLOBAL \(server only\): fail reconnect due to stale global\_seq
* TAG\_RECONNECT\_WAIT \(server only\): fail reconnect due to connect race.
    * Indicates that the server is already connecting to the client, and that direction should win the race. The client should wait for that connection to complete.
* TAG\_RESET\_SESSION \(server only\): ask client to reset session:
    ```
    __u8 full
    ```
    * full flag indicates whether peer should do a full reset, i.e., drop message queue.

Example of failure scenarios:

* First client’s client\_ident message is lost, and then client reconnects.

![5ee2cc8e76bbb8c89f7025ec100684fe3f12bac8089cb75305640cb42cb7cd83.png](image/5ee2cc8e76bbb8c89f7025ec100684fe3f12bac8089cb75305640cb42cb7cd83.png)

* Server’s server\_ident message is lost, and then client reconnects.

![4065a5a0f2827c071fc0b42a972941854b3c45e106f37cea26546494a8fe0ddf.png](image/4065a5a0f2827c071fc0b42a972941854b3c45e106f37cea26546494a8fe0ddf.png)

* Server’s server\_ident message is lost, and then server reconnects.

![8087a55b5f9518dfe9974ba56498df53a2b71291ea7c434f24ed2665b19dc047.png](image/8087a55b5f9518dfe9974ba56498df53a2b71291ea7c434f24ed2665b19dc047.png)

* Connection failure after session is established, and then client reconnects.

![6a3daf5fa1775c9aa1a0b4576e6030825f25033220b4a3aa873b6c11ebfa3735.png](image/6a3daf5fa1775c9aa1a0b4576e6030825f25033220b4a3aa873b6c11ebfa3735.png)

* Connection failure after session is established because server reset, and then client reconnects.

![e21fcedd0607c69c99f72b1cd3bbe8a8041e244b837d8d5e152aecac6d872c18.png](image/e21fcedd0607c69c99f72b1cd3bbe8a8041e244b837d8d5e152aecac6d872c18.png)

RC\* means that the reset session full flag depends on the policy.resetcheck of the connection.

* Connection failure after session is established because client reset, and then client reconnects.

![0a10762e19899a15cf142c01a4a469f0a8c6c71e2c6f4672d7c382c0d8f01b4d.png](image/0a10762e19899a15cf142c01a4a469f0a8c6c71e2c6f4672d7c382c0d8f01b4d.png)

## Message exchange[¶](https://docs.ceph.com/en/pacific/dev/msgr2/ "Permalink to this headline")

Once a session is established, we can exchange messages.

* TAG\_MSG: a message:
    ```
    ceph_msg_header2
    front
    middle
    data_pre_padding
    data
    ```
    * The ceph\_msg\_header2 is modified from ceph\_msg\_header:
        * include an ack\_seq. This avoids the need for a TAG\_ACK message most of the time.
        * remove the src field, which we now get from the message flow handshake \(TAG\_IDENT\).
        * specifies the data\_pre\_padding length, which can be used to adjust the alignment of the data payload. \(NOTE: is this is useful?\)
* TAG\_ACK: acknowledge receipt of message\(s\):
    ```
    __le64 seq
    ```
    * This is only used for stateful sessions.
* TAG\_KEEPALIVE2: check for connection liveness:
    ```
    ceph_timespec stamp
    ```
    * Time stamp is local to sender.
* TAG\_KEEPALIVE2\_ACK: reply to a keepalive2:
    ```
    ceph_timestamp stamp
    ```
    * Time stamp is from the TAG\_KEEPALIVE2 we are responding to.
* TAG\_CLOSE: terminate a connection
    Indicates that a connection should be terminated. This is equivalent to a hangup or reset \(i.e., should trigger ms\_handle\_reset\). It isn’t strictly necessary or useful as we could just disconnect the TCP connection.

### Example of protocol interaction \(WIP\)[¶](https://docs.ceph.com/en/pacific/dev/msgr2/ "Permalink to this headline")

![8743172dea6dc6f60895a246336fa39f522c8d8f71e7adb67cb66ccb58cdfe06.png](image/8743172dea6dc6f60895a246336fa39f522c8d8f71e7adb67cb66ccb58cdfe06.png)
