# 187: Image Live-Migration

**クリップソース:** [187: Image Live\-Migration — Ceph Documentation](https://docs.ceph.com/en/pacific/rbd/rbd-live-migration/)

# Image Live\-Migration[¶](https://docs.ceph.com/en/pacific/rbd/rbd-live-migration/ "Permalink to this headline")

RBDイメージは、同じクラスタ内の異なるプール間、異なるイメージフォーマットやレイアウト間、または外部データソースからライブマイグレーションすることができます。起動すると、ソースはデスティネーションイメージにディープコピーされ、可能な限りデータのスパースアロケーションを維持しながら、すべてのスナップショット履歴を引き出します。

デフォルトでは、同じCephクラスタ内でRBDイメージをライブマイグレーションする場合、ソースイメージは読み取り専用とマークされ、すべてのクライアントは代わりに新しいターゲットイメージにIOをリダイレクトします。さらに、このモードでは、オプションでソースイメージのparentへのリンクを保持してスパースネスを保持したり、移行中にイメージを平坦化してソースイメージのparentへの依存を取り除いたりすることができます。

ライブマイグレーションプロセスは、ソースイメージを変更せず、ターゲットイメージをバッキングファイル、HTTP\(s\)ファイル、S3オブジェクトなどの外部データソースにリンクさせるインポートオンリーモードで使用することも可能です。

ライブマイグレーションコピー処理は、新しいターゲットイメージが使用されている間、バックグラウンドで安全に実行することができます。現在、インポートオンリーの操作モードを使用していない場合、移行を準備する前にソースイメージの使用を一時的に停止する必要があります。これは、そのイメージを使用しているクライアントが新しいターゲットイメージを指すように更新されていることを確認するのに役立ちます。

注：イメージのライブマイグレーションには、Ceph Nautilusリリース以降が必要です。外部データソースのサポートには、Ceph Pacificリリース以降が必要です。krbdカーネルモジュールは、現時点ではライブマイグレーションをサポートしていません。
![f902d268f14956a24fe9ca51a06749965d2b3eb365deca3f6f888597c32fab17.png](image/f902d268f14956a24fe9ca51a06749965d2b3eb365deca3f6f888597c32fab17.png)

ライブマイグレーションは、3つのステップで構成されています：

1. [レイヤーイメージ](https://docs.ceph.com/en/pacific/rbd/rbd-snapshot/#layering)と同様に、ターゲットイメージ内の未初期化データエクステントを読み込もうとすると、内部的にソースイメージにリダイレクトされ、ターゲット内の未初期化エクステントへの書き込みは、内部的に重複するソースイメージブロックをターゲットイメージにディープコピーします。
    **Prepare Migration:**
2. **Execute Migration:**
3. **Finish Migration:**

## Prepare Migration[¶](https://docs.ceph.com/en/pacific/rbd/rbd-live-migration/ "Permalink to this headline")

同じCephクラスタ内のイメージに対するデフォルトのライブマイグレーションプロセスは、ソースイメージとターゲットイメージを指定してrbd migration prepareコマンドを実行することによって開始されます：

```
$ rbd migration prepare migration_source [migration_target]
```

rbd migration prepareコマンドは、rbd createコマンドと同じレイアウトオプションをすべて受け付け、イミュータブルイメージのオンディスクレイアウトを変更できるようにします。オリジナルのイメージ名を維持したままディスク上のレイアウトを変更することが目的であれば、migration\_targetはスキップすることができます。

ライブマイグレーションを準備する前に、ソースイメージを使用しているすべてのクライアントを停止する必要があります。イメージを読み取り/書き込みモードで開いている実行中のクライアントが見つかると、準備ステップは失敗します。準備ステップが完了したら、新しいターゲットイメージ名を使用してクライアントを再起動することができます。ソースイメージ名を使用してクライアントを再起動しようとすると、失敗します。

rbd statusコマンドは、ライブマイグレーションの現在の状態を表示します：

```
$ rbd status migration_target
Watchers: none
Migration:
            source: rbd/migration_source (5e2cba2f62e)
            destination: rbd/migration_target (5e2ed95ed806)
            state: prepared
```

なお、移行作業中の誤使用を避けるため、ソースイメージはRBDのtrashに移動されます：

```
$ rbd info migration_source
rbd: error opening image migration_source: (2) No such file or directory
$ rbd trash ls --all
5e2cba2f62e migration_source
```

## Prepare Import\-Only Migration[¶](https://docs.ceph.com/en/pacific/rbd/rbd-live-migration/ "Permalink to this headline")

インポートオンリーのライブマイグレーションは、同じrbd migration prepareコマンドを実行することで開始されますが、\-\-import\-onlyオプションを追加し、ソースイメージデータへのアクセス方法を記述するJSONエンコードされたソース仕様を提供します。このソース仕様は、\-\-source\-specオプションで直接渡すか、\-\-source\-spec\-pathオプションでファイルやSTDINを経由して渡すことができます：

```
$ rbd migration prepare --import-only --source-spec "<JSON>" migration_target
```

rbd migration prepareコマンドは、rbd createコマンドと同じレイアウトオプションをすべて受け付けます。

rbd statusコマンドは、ライブマイグレーションの現在の状態を表示します：

```
$ rbd status migration_target
Watchers: none
Migration:
        source: {"stream":{"file_path":"/mnt/image.raw","type":"file"},"type":"raw"}
        destination: rbd/migration_target (ac69113dc1d7)
        state: prepared
```

ソース仕様のJSONの一般的な書式は以下の通りです：

```
{
    "type": "<format-type>",
    <format unique parameters>
    "stream": {
        "type": "<stream-type>",
        <stream unique parameters>
    }
}
```

現在対応しているフォーマットは、Native、qcow、Rawです。現在サポートしているストリームは、file, http, s3です。

### Formats[¶](https://docs.ceph.com/en/pacific/rbd/rbd-live-migration/ "Permalink to this headline")

ネイティブ形式は、Cephクラスタ内のネイティブRBDイメージをソースイメージとして記述するために使用できます。そのソース仕様のJSONは、以下のようにエンコードされています：

```
{
    "type": "native",
    "pool_name": "<pool-name>",
    ["pool_id": <pool-id>,] (optional alternative to "pool_name")
    ["pool_namespace": "<pool-namespace",] (optional)
    "image_name": "<image-name>",
    ["image_id": "<image-id>",] (optional if image in trash)
    "snap_name": "<snap-name>",
    ["snap_id": "<snap-id>",] (optional alternative to "snap_name")
}
```

ネイティブ形式は、Cephのネイティブ操作を利用するため、ストリームオブジェクトを含まないことに注意してください。例えば、イメージrbd/ns1/image1@snap1からインポートする場合、ソース仕様は次のようにエンコードされます：

```
{
    "type": "native",
    "pool_name": "rbd",
    "pool_namespace": "ns1",
    "image_name": "image1",
    "snap_name": "snap1"
}
```

qcow形式は、QCOW（QEMUコピーオンライト）ブロックデバイスを記述するために使用することができます。現在、QCOW（v1）およびQCOW2形式の両方が、圧縮、暗号化、バッキングファイル、外部データファイルなどの高度な機能を除いてサポートされています。これらの欠落している機能のサポートは、将来のリリースで追加される可能性があります。qcow形式のデータは、以下に説明するサポートされる任意のストリームソースにリンクすることができます。例えば、そのベースとなるソース仕様のJSONは、以下のようにエンコードされています：

```
{
    "type": "qcow",
    "stream": {
        <stream unique parameters>
    }
}
```

rawフォーマットは、シックプロビジョニングされたrawブロックデバイスのエクスポートを記述するために使用することができます（すなわち、rbd export \-export\-format 1 \<snap\-spec\>）。rawフォーマットのデータは、以下に説明するサポートされる任意のストリームソースにリンクすることができます。たとえば、そのベースとなるソース仕様のJSONは、次のようにエンコードされています：

```
{
    "type": "raw",
    "stream": {
        <stream unique parameters for HEAD, non-snapshot revision>
    },
    "snapshots": [
        {
            "type": "raw",
            "name": "<snapshot-name>",
            "stream": {
                <stream unique parameters for snapshot>
            }
        },
    ] (optional oldest to newest ordering of snapshots)
}
```

スナップショットアレイの追加はオプションで、現在はシックプロビジョニングのRawスナップショットエクスポートのみをサポートしています。

RBD export\-format v2やRBD export\-diffスナップショットなどの追加フォーマットは、将来のリリースで追加される予定です。

### Streams[¶](https://docs.ceph.com/en/pacific/rbd/rbd-live-migration/ "Permalink to this headline")

ファイルストリームは、ローカルにアクセス可能なPOSIXファイルソースからインポートするために使用することができます。そのソース仕様のJSONは、以下のようにエンコードされています：

```
{
    <format unique parameters>
    "stream": {
        "type": "file",
        "file_path": "<file-path>"
    }
}
```

例えば、"/mnt/image.raw "にあるファイルからRaw形式のイメージを取り込む場合、そのソース仕様のJSONは次のようにエンコードされます：

```
{
    "type": "raw",
    "stream": {
        "type": "file",
        "file_path": "/mnt/image.raw"
    }
}
```

httpストリームは、リモートのHTTPまたはHTTPSウェブサーバーからインポートするために使用することができます。そのソース仕様のJSONは、以下のようにエンコードされています：

```
{
    <format unique parameters>
    "stream": {
        "type": "http",
        "url": "<url-path>"
    }
}
```

例えば、http://download.ceph.com/image.raw にあるファイルから raw 形式のイメージを取り込む場合、そのソース仕様の JSON は次のようにエンコードされます：

```
{
    "type": "raw",
    "stream": {
        "type": "http",
        "url": "http://download.ceph.com/image.raw"
    }
}
```

s3ストリームは、リモートのS3バケットからインポートするために使用することができます。そのソース仕様のJSONは以下のようにエンコードされています：

```
{
    <format unique parameters>
    "stream": {
        "type": "s3",
        "url": "<url-path>",
        "access_key": "<access-key>",
        "secret_key": "<secret-key>"
    }
}
```

例えば、http://s3.ceph.com/bucket/image.raw にあるファイルから raw 形式のイメージを取り込む場合、そのソース仕様の JSON は次のようにエンコードされます：

```
{
    "type": "raw",
    "stream": {
        "type": "s3",
        "url": "http://s3.ceph.com/bucket/image.raw",
        "access_key": "NX5QOQKC6BH2IDN8HC7A",
        "secret_key": "LnEsqNNqZIpkzauboDcLXLcYaWwLQ3Kop0zAnKIn"
    }
}
```

注：access\_key と secret\_key パラメータは、鍵の値の前に config:// を付け、その後に MON config\-key ストア内のパスを指定することで、MON config\-key ストアに鍵を保存することをサポートしています。値は、ceph config\-key set \<key\-path\> \<value\>でconfig\-key storeに格納できます \(例: ceph config\-key set rbd/s3/access\_key NX5QOQKC6BH2IDN8HC7A\).

## Execute Migration[¶](https://docs.ceph.com/en/pacific/rbd/rbd-live-migration/ "Permalink to this headline")

After preparing the live\-migration, the image blocks from the source image must be copied to the target image. This is accomplished by running the rbd migration execute command:

```
$ rbd migration execute migration_target
Image migration: 100% complete...done.
```

rbd statusコマンドは、マイグレーションブロックのディープコピー処理の進行状況をフィードバックすることもできます：

```
$ rbd status migration_target
Watchers:
    watcher=1.2.3.4:0/3695551461 client.123 cookie=123
Migration:
            source: rbd/migration_source (5e2cba2f62e)
            destination: rbd/migration_target (5e2ed95ed806)
            state: executing (32% complete)

```

## Commit Migration[¶](https://docs.ceph.com/en/pacific/rbd/rbd-live-migration/ "Permalink to this headline")

ライブマイグレーションでソースイメージからターゲットへの全データブロックのディープコピーが完了すると、マイグレーションをコミットすることができます。

```
$ rbd status migration_target
Watchers: none
Migration:
            source: rbd/migration_source (5e2cba2f62e)
            destination: rbd/migration_target (5e2ed95ed806)
            state: executed
$ rbd migration commit migration_target
Commit image migration: 100% complete...done.
```

migration\_source イメージが1つ以上のクローンの親である場合、すべての子孫のクローンイメージが使用中でないことを確認した後に、\-\-force オプションを指定する必要があります。

ライブマイグレーションをコミットすると、ソースイメージとターゲットイメージの間のクロスリンクが削除され、ソースイメージが削除されます：

```
$ rbd trash list --all
```

## Abort Migration[¶](https://docs.ceph.com/en/pacific/rbd/rbd-live-migration/ "Permalink to this headline")

準備または実行ステップを元に戻したい場合は、rbd migration abortコマンドを実行して、マイグレーション処理を元に戻します。

```
$ rbd migration abort migration_target
Abort image migration: 100% complete...done.
```

マイグレーションを中止すると、ターゲットイメージは削除され、元のソースイメージへのアクセスが復元されます：

```
$ rbd ls
migration_source
```
