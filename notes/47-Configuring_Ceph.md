# 47: Configuring Ceph

**クリップソース:** [47: Configuring Ceph — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/configuration/ceph-conf/)

# Configuring Ceph[¶](https://docs.ceph.com/en/pacific/rados/configuration/ceph-conf/ "Permalink to this headline")

Cephのサービスが開始されると、初期化プロセスにより、バックグラウンドで実行される一連のデーモンが起動します。Ceph Storage Clusterは、最低でも3種類のデーモンを実行します。

* [Ceph Monitor](https://docs.ceph.com/en/pacific/rados/configuration/ceph-conf/)
* [Ceph Manager](https://docs.ceph.com/en/pacific/rados/configuration/ceph-conf/)
* [Ceph OSD Daemon](https://docs.ceph.com/en/pacific/rados/configuration/ceph-conf/)

Ceph File SystemをサポートするCeph Storageクラスターは、少なくとも1つのCeph Metadata Server \(ceph\-mds\)も実行します。Ceph Object Storageをサポートするクラスタでは、Ceph RADOS Gatewayデーモン\(radosgw\)も実行されます。

各デーモンにはいくつかの設定オプションがあり、それぞれにデフォルト値があります。 これらの設定オプションを変更することで、システムの動作を調整することができます。 デフォルト値を上書きすると、クラスターのパフォーマンスや安定性が著しく低下する可能性があるため、その結果を理解するように注意してください。 また、デフォルト値はリリースごとに変更されることがあるので、お使いのCephのリリースに合わせてこのドキュメントのバージョンを確認することをお勧めします。

## Option names[¶](https://docs.ceph.com/en/pacific/rados/configuration/ceph-conf/ "Permalink to this headline")

すべてのCephの設定オプションには、小文字で構成され、アンダースコア\(\_\)文字で接続された単語からなる固有の名前があります。

オプション名をコマンドラインで指定する場合、アンダースコア\(\_\)またはダッシュ\(\-\)のいずれかの文字を入れ替えて使用できます\(たとえば、\-\-mon\-hostは\-\-mon\_hostと同等です\)。

設定ファイルにオプション名を記述する場合、アンダースコアやダッシュの代わりにスペースを使用することもできます。 しかし、わかりやすさと利便性を考慮して、このドキュメントでは一貫してアンダースコアを使用することをお勧めします。

## Config sources[¶](https://docs.ceph.com/en/pacific/rados/configuration/ceph-conf/ "Permalink to this headline")

各Cephデーモン、プロセス、およびライブラリは、以下に示す複数のソースから設定を取得します。 リストの後の方にあるソースが、リストの前の方にあるソースよりも優先されます。

* コンパイルされたデフォルト値
* モニタークラスターの集中型コンフィグレーションデータベース
* ローカルホストに保存されている設定ファイル
* 環境変数
* コマンドライン引数
* 管理者が設定するランタイムオーバーライド

Cephプロセスが起動時に最初に行うことの1つは、コマンドライン、環境、ローカル設定ファイルで提供される設定オプションの解析です。 次に、プロセスはモニタクラスタに連絡して、クラスタ全体で一元的に保存されている設定を取得します。 設定の完全な表示が可能になると、デーモンまたはプロセスの起動が進みます。

### Bootstrap options[¶](https://docs.ceph.com/en/pacific/rados/configuration/ceph-conf/ "Permalink to this headline")

設定オプションの中には、プロセスがモニターに連絡したり、認証したり、クラスターに保存されている設定を取得したりする機能に影響を与えるものがあるため、ノードにローカルに保存し、ローカルの設定ファイルに設定する必要がある場合があります。 これらのオプションには次のようなものがあります。

*  mon\_host、クラスタのモニタのリスト。
* mon\_host\_override: Cephクラスタとの通信の新しいインスタンスを開始するときに最初に連絡するクラスタのモニタのリスト。 これは、古いCephインスタンス\(libradosクラスタハンドルなど\)に送信されたMonMapアップデートから得られる既知のモニタリストをオーバーライドします。 このオプションは主にデバッグに役立つことが予想されます。
*  mon\_dns\_srv\_name \(default: ceph\-mon\)、DNS経由でクラスタモニタを識別するためにチェックするDNS SRVレコードの名前。
*  mon\_data、osd\_data、mds\_data、mgr\_data、および同様のオプションで、デーモンがデータを保存するローカルディレクトリを定義します。
*  keyring、keyfile、keyは、モニターとの認証に使用する認証クレデンシャルを指定するために使用できます。 ほとんどの場合、デフォルトのキーリングの場所は上記で指定されたデータディレクトリになります。

ほとんどの場合、これらのデフォルト値は適切ですが、クラスタのモニタのアドレスを識別するmon\_hostオプションは例外です。 DNSを使用してモニタを識別する場合、ローカルのceph設定ファイルを完全に回避することができます。

### Skipping monitor config[¶](https://docs.ceph.com/en/pacific/rados/configuration/ceph-conf/ "Permalink to this headline")

どのプロセスにもオプション\-\-no\-mon\-configを渡すことで、クラスタ・モニタから設定を取得するステップをスキップすることができます。 これは、設定がすべて設定ファイルで管理されている場合や、モニター・クラスターが現在ダウンしているが何らかのメンテナンス作業を行う必要がある場合に便利です。

## Configuration sections[¶](https://docs.ceph.com/en/pacific/rados/configuration/ceph-conf/ "Permalink to this headline")

任意のプロセスやデーモンは、各設定オプションに1つの値を持っています。 ただし、オプションの値は、同じ種類のデーモンであっても、デーモンの種類によって異なる場合があります。 モニタ設定データベースまたはローカル設定ファイルに格納されているCephオプションは、どのデーモンまたはクライアントに適用されるかを示すセクションにグループ化されています。

これらのセクションには以下のものがあります。

**global**

Description

globalの設定は、Ceph Storage Clusterのすべてのデーモンとクライアントに影響します。

Example

```
log_file=/var/log/ceph/$cluster-$type.$id.log
```

**mon**

Description

monでの設定は、Ceph Storage Clusterのすべてのceph\-monデーモンに影響し、globalでの同じ設定を上書きします。

Example

```
mon_cluster_log_to_syslog=true
```

**mgr**

Description

mgrセクションの設定は、Ceph Storage Clusterのすべてのceph\-mgrデーモンに影響し、globalでの同じ設定を上書きします。

Example

```
mgr_stats_period=10
```

**osd**

Description

osdでの設定は、Ceph Storage Clusterのすべてのceph\-osdデーモンに影響し、globalでの同じ設定を上書きします。

Example

```
osd_op_queue=wpq
```

**mds**

Description

mdsセクションの設定は、Ceph Storage Clusterのすべてのceph\-mdsデーモンに影響し、globalの同じ設定を上書きします。

Example

```
mds_cache_memory_limit=10G
```

**client**

Description

clientでの設定は、すべてのCephクライアント\(マウントされたCephファイルシステム、マウントされたCephブロックデバイスなど\)およびRados Gateway\(RGW\)デーモンに影響します。

Example

```
objecter_inflight_ops=512
```

セクションでは、個々のデーモンやクライアントの名前を指定することもできます。 たとえば、mon.foo、osd.123、client.smith はすべて有効なセクション名です。

任意のデーモンは、グローバルセクション、デーモンまたはクライアントのタイプセクション、およびその名前を持つセクションから設定を引き出します。例えば、同じソース（同じ設定ファイル）のglobal、mon、mon.fooの両方に同じオプションが指定されている場合、mon.fooの値が使用されます。

同じセクションに同じ設定オプションの値が複数指定されている場合は、最後の値が優先されます。

なお、ローカル構成ファイルの値は、どのセクションにあっても、モニター構成データベースの値よりも常に優先されます。

## Metavariables[¶](https://docs.ceph.com/en/pacific/rados/configuration/ceph-conf/ "Permalink to this headline")

MetavariableはCeph Storage Clusterの設定を劇的に簡素化します。メタ変数が設定値に設定されると、Cephは設定値が使用されるときにメタ変数を具体的な値に展開します。Cephのメタ変数は、Bashシェルの変数展開に似ています。

Cephは以下のメタ変数をサポートしています。

**$cluster**

Description

Ceph Storage Clusterの名前に展開します。同じハードウェアで複数のCeph Storage Clustersを実行する場合に便利です。

Example

```
/etc/ceph/$cluster.keyring
```

Default

_ceph_

**$type**

Description

デーモンやプロセスの種類（例：mds、osd、mon）に展開します。

Example

```
/var/lib/ceph/$type
```

**$id**

Description

デーモンまたはクライアントの識別子に展開します。osd.0であれば0、mds.aであればaとなります。

Example

```
/var/lib/ceph/$type/$cluster-$id
```

**$host**

Description

プロセスが実行されているホスト名に展開します。

**$name**

Description

$type.$idに展開します。

Example

```
/var/run/ceph/$cluster-$name.asok
```

**$pid**

Description

デーモンのpidに展開します。

Example

```
/var/run/ceph/$cluster-$name-$pid.asok
```

## The Configuration File[¶](https://docs.ceph.com/en/pacific/rados/configuration/ceph-conf/ "Permalink to this headline")

起動時に、Cephプロセスは以下の場所で設定ファイルを検索します。

1. $CEPH\_CONF \(つまり、環境変数$CEPH\_CONFに続くパスです。\)
2. \-c path/path  \(すなわち、\-cコマンドライン引数\)
3. /etc/ceph/$cluster.conf
4. ~/.ceph/$cluster.conf
5. ./$cluster.conf \(つまり、現在の作業ディレクトリ\)
6. On FreeBSD systems only, /usr/local/etc/ceph/$cluster.conf

ここで、$clusterはクラスタの名前\(デフォルトはceph\)です。

Cephの設定ファイルはini形式の構文を使用しています。ポンド記号\(\#\)またはセミコロン\(;\)の後にコメントテキストを追加できます。 たとえば、以下のようになります。

```
# <--A number (#) sign precedes a comment.
; A comment may be anything.
# Comments always follow a semi-colon (;) or a pound (#) on each line.
# The end of the line terminates a comment.
# We recommend that you provide comments in your configuration file(s).
```

### Config file section names[¶](https://docs.ceph.com/en/pacific/rados/configuration/ceph-conf/ "Permalink to this headline")

設定ファイルはセクションに分かれています。各セクションは、角括弧で囲まれた有効な設定セクション名（上記の「[Configuration sections](https://docs.ceph.com/en/pacific/rados/configuration/ceph-conf/)」を参照）で始まる必要があります。例えば、以下のようになります。 

```
[global]
debug_ms = 0

[osd]
debug_ms = 1

[osd.1]
debug_ms = 10

[osd.2]
debug_ms = 10

```

### Config file option values[¶](https://docs.ceph.com/en/pacific/rados/configuration/ceph-conf/ "Permalink to this headline")

設定オプションの値は文字列です。一行に収まらないほど長い場合は、行末にバックスラッシュ\(\)を置いて行継続マーカーとすることで、現在の行の=以降の文字列と次の行の文字列を合わせたものがオプションの値となります。

```
[global]
foo = long long ago
long ago
```

上の例では、"foo "の値は "longlongagolongago "となります。

通常、オプションの値は、次のように改行、またはコメントで終わります。

```
[global]
obscure_one = difficult to explain # I will try harder in next release
simpler_one = nothing to explain
```

上記の例では、「obscureone」の値は「difficulttoexplain」、「simplerone」の値は「nothingtoexplain」となります。

オプションの値にスペースが含まれていて、それを明示したい場合は、次のように一重または二重引用符を使って値を引用することができます。

```
[global]
line = "to be, or not to be"
```

特定の文字は、オプション値の中に直接入れることができません。これらの文字は、=、\#、 ; 、\[ です。

```
[global]
secret = "i love # and ["
```

すべての設定オプションは、以下のいずれかのタイプで表されます。

**int**

Description

64ビット符号付き整数、SI接頭辞は、"K"、"M"、"G"、"T"、"P"、"E "などがサポートされており、それぞれ103、106、109などを意味する。 そして、"B "は唯一のサポート単位です。つまり、"1K"、"1M"、"128B"、"\-1 "はすべて有効なオプション値です。しきい値や限界値のオプションでは、負の値が「無制限」を意味することもあります。

Example

42, \-1

**uint**

Description

整数とほぼ同じです。ただし、負の値は拒否されます。

Example

256, 0

**str**

Description

UTF\-8でエンコードされたフリースタイルの文字列ですが、使用できない文字があります。詳細は上記の注意事項をご参照ください。

Example

"helloworld", "ilove\#", yet\-another\-name

**boolean**

Description

2つの値のうちの1つが真か偽かを表します。しかし、整数でも構いません。"0 "は偽を意味し、0以外の値は真を意味します。

Example

true, false, 1, 0

**addr**

Description

メッセンジャーのプロトコルを表す v1、v2、または any のプレフィックスを持つ単一のアドレスです。プレフィックスが指定されていない場合は、v2プロトコルが使用されます。詳しくは「アドレスフォーマット」をご覧ください。

Example

v1:[1.2.3.4:567](http://1.2.3.4:567), v2:[1.2.3.4:567](http://1.2.3.4:567), [1.2.3.4:567](http://1.2.3.4:567), 2409:8a1e:8fb6:aa20:1260:4bff:fe92:18f5::567, \[::1\]:6789

**addrvec**

Description

", "で区切られたアドレスのセットです。アドレスはオプションで\[ と \]を使って引用することができます。

Example

\[v1:1.2.3.4:567,v2:1.2.3.4:568\], v1:1.2.3.4:567,v1:1.2.3.14:567\[2409:8a1e:8fb6:aa20:1260:4bff:fe92:18f5::567\],\[2409:8a1e:8fb6:aa20:1260:4bff:fe92:18f5::568\]

**uuid**

Description

は、RFC4122で定義されているuuidの文字列形式です。また、いくつかのバリエーションもサポートしています。詳しくはBoostドキュメントをご覧ください。

Example

f81d4fae\-7dec\-11d0\-a765\-00a0c91e6bf6

**size**

Description

は，64ビットの符号なし整数を表す。SI接頭辞とIEC接頭辞の両方に対応しています。また，"B "は唯一のサポート単位である。負の値は拒否されます。

Example

1Ki, 1K, 1KiB and 1B.

**secs**

Description

は、時間の長さを表します。指定されていない場合、デフォルトでは単位は秒となります。以下の時間単位がサポートされています。

* second: "s", "sec", "second", "seconds"
* minute "m", "min", "minute", "minutes"
* hour "hs"、"hr"、"hour"、"hours"
* day "d"、"day"、"days"
* week "w", "wk", "week", "weeks"
* month "mo", "month", "months"
* year "y", "yr", "year", "years"

Example

1m, 1m and 1week

## Monitor configuration database[¶](https://docs.ceph.com/en/pacific/rados/configuration/ceph-conf/ "Permalink to this headline")

モニタークラスターは、クラスター全体で使用可能な設定オプションのデータベースを管理し、システム全体の設定管理を効率的に行うことができます。 設定オプションの大部分は、管理のしやすさと透明性のために、このデータベースに保存することができます。

一部の設定は、モニターへの接続、認証、設定情報の取得などに影響するため、ローカルの設定ファイルに保存する必要があります。 ほとんどの場合、これは mon\_host オプションに限定されますが、DNS SRV レコードを使用することで回避することもできます。

### Sections and masks[¶](https://docs.ceph.com/en/pacific/rados/configuration/ceph-conf/ "Permalink to this headline")

モニターが保存する設定オプションは、設定ファイルのオプションと同様に、グローバルセクション、デーモンタイプセクション、特定のデーモンセクションに格納することができます。

さらに、オプションには、オプションが適用されるデーモンやクライアントをさらに制限するためのマスクが関連付けられている場合があります。マスクには2つの形式があります。

1. type:location typeはrackやhostなどのCRUSHプロパティ、locationはそのプロパティの値です。 例えば、host:fooとすると、特定のホスト上で実行されているデーモンまたはクライアントのみにオプションを制限することができます。
2. class:device\-class ここでdevice\-classは、CRUSHデバイスクラスの名前です（例：hddまたはssd）。 例えば、class:ssdとすると、SSDでバックアップされたOSDのみにオプションを制限することができます。\(このマスクは、OSD以外のデーモンまたはクライアントには効果がありません。\)

構成オプションを設定する際の who は、セクション名、マスク、またはその両方をスラッシュ \(/\) 文字で区切って指定します。 たとえば、osd/rack:fooはfooラック内のすべてのOSDデーモンを意味します。

設定オプションを表示する際には、読みやすくするために、セクション名とマスクは通常、別のフィールドまたは列に分離されます。

### Commands[¶](https://docs.ceph.com/en/pacific/rados/configuration/ceph-conf/ "Permalink to this headline")

クラスタの設定には、以下のCLIコマンドを使用します。

* _ceph config dump_
* _ceph config get \<who\>_
* _ceph config set \<who\> \<option\> \<value\>_
* _ceph config show \<who\>_
* _ceph config assimilate\-conf \-i \<inputfile\> \-o \<outputfile\>_

## Help[¶](https://docs.ceph.com/en/pacific/rados/configuration/ceph-conf/ "Permalink to this headline")

あなたは、特定のオプションのためのヘルプを得ることができます。

```
ceph config help <option>
```

この場合、実行中のモニターにコンパイルされている設定スキーマが使用されることに注意してください。 バージョンが混在しているクラスタ（アップグレード時など）では、特定の実行中のデーモンからオプションスキーマを照会したい場合もあるでしょう。

```
ceph daemon <name> config help [option]
```

For example,:

```
$ ceph config help log_file
log_file - path to log file
  (std::string, basic)
  Default (non-daemon):
  Default (daemon): /var/log/ceph/$cluster-$name.log
  Can update at runtime: false
  See also: [log_to_stderr,err_to_stderr,log_to_syslog,err_to_syslog]
```

or:

```
$ ceph config help log_file -f json-pretty
{
    "name": "log_file",
    "type": "std::string",
    "level": "basic",
    "desc": "path to log file",
    "long_desc": "",
    "default": "",
    "daemon_default": "/var/log/ceph/$cluster-$name.log",
    "tags": [],
    "services": [],
    "see_also": [
        "log_to_stderr",
        "err_to_stderr",
        "log_to_syslog",
        "err_to_syslog"
    ],
    "enum_values": [],
    "min": "",
    "max": "",
    "can_update_at_runtime": false
}

```

levelプロパティには、basic、advanced、devのいずれかを指定します。devオプションは、開発者がテスト目的で使用することを意図しており、オペレータが使用することは推奨されていません。

## Runtime Changes[¶](https://docs.ceph.com/en/pacific/rados/configuration/ceph-conf/ "Permalink to this headline")

ほとんどの場合、Cephでは実行中にデーモンの設定を変更することができます。この機能は、ログ出力の増加/減少、デバッグ設定の有効化/無効化、さらにはランタイムの最適化に非常に役立ちます。

一般に、設定オプションは、_ceph config set_コマンドを介して通常の方法で更新できます。 たとえば、特定のOSDでデバッグログレベルを有効にするには、次のようにします。

```
ceph config set osd.123 debug_ms 20
```

なお、同じオプションがローカルの設定ファイルでもカスタマイズされている場合は、モニターの設定は無視されます（ローカルの設定ファイルよりも優先度が低い）。

### Override values[¶](https://docs.ceph.com/en/pacific/rados/configuration/ceph-conf/ "Permalink to this headline")

Ceph CLIのtellまたはdaemonインタフェースを使用して、オプションを一時的に設定することもできます。 これらのオーバーライド値は、実行中のプロセスにのみ影響し、デーモンまたはプロセスが再起動すると破棄/消失されるという点で、一過性のものです。

オーバーライド値は2つの方法で設定できます。

どのホストからでも、ネットワーク経由でデーモンにメッセージを送ることができます。

```
ceph tell <name> config set <option> <value>
```

For example,:

```
ceph tell osd.123 config set debug_osd 20
```

tellコマンドでは、デーモンの識別子にワイルドカードを使用することもできます。 例えば、すべてのOSDデーモンのデバッグレベルを調整するには、:

```
ceph tell osd.* config set debug_osd 20
```

プロセスが実行されているホストから、/var/run/cephにあるソケットを介して、次のようにしてプロセスに直接接続することができます。

```
ceph daemon <name> config set <option> <value>
```

For example,:

```
ceph daemon osd.4 config set debug_osd 20
```

_ceph config show_コマンドの出力では、これらの一時的な値がオーバーライドのソースとともに表示されることに注意してください。

## Viewing runtime settings[¶](https://docs.ceph.com/en/pacific/rados/configuration/ceph-conf/ "Permalink to this headline")

実行中のデーモンに設定されている現在のオプションは、_ceph config show_コマンドで確認できます。 たとえば、以下のようになります。

```
ceph config show osd.0
```

を実行すると、そのデーモンの（デフォルト以外の）オプションが表示されます。 で特定のオプションを見ることもできます。

```
ceph config show osd.0 debug_osd
```

または、すべてのオプション（デフォルト値があるものも含む）を表示するには

```
ceph config show-with-defaults osd.0
```

また、ローカルホストからadminソケットを介してデーモンに接続することで、実行中のデーモンの設定を確認することができます。 例えば、次のようになります。

```
ceph daemon osd.0 config show
```

現在の設定をすべてダンプします。

```
ceph daemon osd.0 config diff
```

デフォルト以外の設定のみを表示します（設定ファイル、モニター、オーバーライドなど、値がどこから来たかも表示されます）。

```
ceph daemon osd.0 config get debug_osd
```

1つのオプションの値を報告します。

## Changes since Nautilus[¶](https://docs.ceph.com/en/pacific/rados/configuration/ceph-conf/ "Permalink to this headline")

Octopusのリリースに伴い、設定ファイルの解析方法を変更しました。これらの変更点は以下の通りです。

* 設定オプションの繰り返しは許可されており、警告は表示されません。最後に設定された値が使用されます。つまり、ファイル内で最後に設定されたものが有効になります。この変更前は、以下のようにオプションが重複している行があった場合に警告メッセージを表示していました。

```
warning line 42: 'foo' in section 'bar' redefined
```

* 無効なUTF\-8オプションは警告メッセージで無視されていました。しかし、Octopus以降は、致命的なエラーとして扱われます。
* バックスラッシュは，現在の行に次の行を結合するための行継続マーカとして使用される。Octopus以前では，バックスラッシュの後には，空行でないものが必要でした。しかし，Octopusでは，バックスラッシュの後に空の行があってもよいことになりました。
* 設定ファイルでは、各行で個別の設定オプションを指定します。オプション名とその値は = で区切られ、値はシングルクォートまたはダブルクォートで引用されます。無効な設定が指定された場合は、無効な設定ファイルとして扱われます

```
bad option ==== bad value
```

* Octopus以前では、設定ファイルにセクション名が指定されていない場合、すべてのオプションは、グローバルセクション内にあるかのように設定されていました。これは、現在では推奨されません。Octopus以降では、セクション名のない構成ファイルでは、単一のオプ ションのみが許可されます。
