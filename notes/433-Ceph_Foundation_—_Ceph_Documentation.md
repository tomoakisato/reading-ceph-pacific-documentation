# 433: Ceph Foundation — Ceph Documentation

      [Report a Documentation Bug](https://docs.ceph.com/en/pacific/foundation/) 
 # Ceph Foundation[¶](https://docs.ceph.com/en/pacific/foundation/ "Permalink to this headline")

The Ceph Foundation exists to enable industry members to collaborate and pool resources to support the Ceph project community. The Foundation provides an open, collaborative, and neutral home for project stakeholders to coordinate their development and community investments in the Ceph ecosystem.

The Ceph Foundation is organized as a directed fund under the Linux Foundation. Premier and General Member organizations contribute a yearly fee to become members. Associate members are educational institutions or government organizations and are invited to join at no cost.

For more information, see [https://ceph.com/foundation](https://docs.ceph.com/en/pacific/foundation/).

## Members[¶](https://docs.ceph.com/en/pacific/foundation/ "Permalink to this headline")

### Premier[¶](https://docs.ceph.com/en/pacific/foundation/ "Permalink to this headline")

* [Amihan](https://docs.ceph.com/en/pacific/foundation/)
* [Bloomberg](https://docs.ceph.com/en/pacific/foundation/)
* [Canonical](https://docs.ceph.com/en/pacific/foundation/)
* [China Mobile](https://docs.ceph.com/en/pacific/foundation/)
* [DigitalOcean](https://docs.ceph.com/en/pacific/foundation/)
* [Intel](https://docs.ceph.com/en/pacific/foundation/)
* [OVH](https://docs.ceph.com/en/pacific/foundation/)
* [Red Hat](https://docs.ceph.com/en/pacific/foundation/)
* [Samsung Electronics](https://docs.ceph.com/en/pacific/foundation/)
* [SoftIron](https://docs.ceph.com/en/pacific/foundation/)
* [SUSE](https://docs.ceph.com/en/pacific/foundation/)
* [Western Digital](https://docs.ceph.com/en/pacific/foundation/)
* [XSKY](https://docs.ceph.com/en/pacific/foundation/)
* [ZTE](https://docs.ceph.com/en/pacific/foundation/)

### General[¶](https://docs.ceph.com/en/pacific/foundation/ "Permalink to this headline")

* [ARM](https://docs.ceph.com/en/pacific/foundation/)
* [Catalyst Cloud](https://docs.ceph.com/en/pacific/foundation/)
* [Cloudbase Solutions](https://docs.ceph.com/en/pacific/foundation/)
* [Clyso](https://docs.ceph.com/en/pacific/foundation/)
* [croit](https://docs.ceph.com/en/pacific/foundation/)
* [DiDi](https://docs.ceph.com/en/pacific/foundation/)
* [EasyStack](https://docs.ceph.com/en/pacific/foundation/)
* [ISS](https://docs.ceph.com/en/pacific/foundation/)
* [QCT](https://docs.ceph.com/en/pacific/foundation/)
* [SinoRail](https://docs.ceph.com/en/pacific/foundation/)
* [Vexxhost](https://docs.ceph.com/en/pacific/foundation/)

### Associate[¶](https://docs.ceph.com/en/pacific/foundation/ "Permalink to this headline")

* [Boston University](https://docs.ceph.com/en/pacific/foundation/)
* [Center for Research in Open Source Systems \(CROSS\)](https://docs.ceph.com/en/pacific/foundation/)
* [CERN](https://docs.ceph.com/en/pacific/foundation/)
* [FASRC](https://docs.ceph.com/en/pacific/foundation/)
* [grnet](https://docs.ceph.com/en/pacific/foundation/)
* [Monash University](https://docs.ceph.com/en/pacific/foundation/)
* [NRF SARAO](https://docs.ceph.com/en/pacific/foundation/)
* [Science & Technology Facilities Councel \(STFC\)](https://docs.ceph.com/en/pacific/foundation/)
* [University of Michigan](https://docs.ceph.com/en/pacific/foundation/)
* [SWITCH](https://docs.ceph.com/en/pacific/foundation/)

## Governing Board[¶](https://docs.ceph.com/en/pacific/foundation/ "Permalink to this headline")

The Governing Board consists of all Premier members, a representative for the General members, a representative for the Associate members, and a representative from the Ceph Leadership Team \(the technical governance body\). The board is responsible for:

* Building and approving an annual budget for spending in support of the Ceph project
* Establishing ad\-hoc committees to address current needs of the project
* Coordinating outreach or marketing
* Meeting regularly to discuss Foundation activities, the status of the Ceph project, and overall project strategy
* Voting on any decisions or matters before the board

The Ceph Foundation board is not responsible for and does not have any direct control over the technical governance of Ceph. Development and engineering activities are managed through traditional open source processes and are overseen by the [Ceph Leadership Team](https://docs.ceph.com/en/pacific/foundation/). For more information see [Governance](https://docs.ceph.com/en/pacific/foundation/).

### Members[¶](https://docs.ceph.com/en/pacific/foundation/ "Permalink to this headline")

* Anjaneya “Reddy” Chagam \(Intel\)
* Dan van der Ster \(CERN\) \- Associate member representative
* Haomai Wang \(XSKY\)
* James Page \(Canonical\)
* Lenz Grimmer \(SUSE\) \- Ceph Leadership Team representative
* Lars Marowsky\-Bree \(SUSE\)
* Matias Bjorling \(Western Digital\)
* Matthew Leonard \(Bloomberg\)
* Mike Perez \(Red Hat\) \- Ceph community manager
* Myoungwon Oh \(Samsung Electronics\)
* Paul Emmerich \(croit\) \- General member representative
* Pawel Sadowski \(OVH\)
* Phil Straw \(SoftIron\)
* Robin Johnson \(DigitalOcean\)
* Sage Weil \(Red Hat\) \- Ceph project leader
* Winston Damarillio \(Amihan\)
* Xie Xingguo \(ZTE\)
* Zhang Shaowen \(China Mobile\)

## Joining[¶](https://docs.ceph.com/en/pacific/foundation/ "Permalink to this headline")

For information about joining the Ceph Foundation, please contact[membership@linuxfoundation.org](https://docs.ceph.com/en/pacific/foundation/).
