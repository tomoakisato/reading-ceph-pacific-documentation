# 54: Configuring Monitor/OSD Interaction

**クリップソース:** [54: Configuring Monitor/OSD Interaction — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/configuration/mon-osd-interaction/)

# Configuring Monitor/OSD Interaction[¶](https://docs.ceph.com/en/pacific/rados/configuration/mon-osd-interaction/ "Permalink to this headline")

Cephの初期設定が完了したら、Cephを展開して実行することができます。 _ceph health_や_ceph \-s_などのコマンドを実行すると、Ceph MonitorがCeph Storage Clusterの現在の状態をレポートします。Ceph Monitorは、各Ceph OSD Daemonからのレポートを要求したり、Ceph OSD Daemonから隣接するCeph OSD Daemonの状態についてのレポートを受け取ったりすることで、Ceph Storage Clusterについて把握しています。Ceph Monitorがレポートを受信しない場合、またはCeph Storage Clusterの変更のレポートを受信した場合、Ceph MonitorはCeph Cluster Mapのステータスを更新します。

Cephでは、Ceph MonitorとCeph OSD Daemonの相互作用について妥当なデフォルト設定が用意されています。ただし、デフォルトを上書きすることができます。次のセクションでは、Ceph Storage Clusterを監視する目的でCeph MonitorとCeph OSD Daemonがどのように相互作用するかを説明します。

## OSDs Check Heartbeats[¶](https://docs.ceph.com/en/pacific/rados/configuration/mon-osd-interaction/ "Permalink to this headline")

各Ceph OSDデーモンは、6秒未満のランダムな間隔で他のCeph OSDデーモンのハートビートを確認します。 近隣のCeph OSDデーモンが20秒の猶予期間内にハートビートを表示しない場合、Ceph OSDデーモンは近隣のCeph OSDデーモンがダウンしていると見なしてCeph Monitorに報告し、Ceph Cluster Mapを更新することがあります。Ceph構成ファイルの\[mon\]および\[osd\]または\[global\]セクションに_osd heartbeat grace_設定を追加するか、実行時に値を設定することで、この猶予期間を変更できます。

## OSDs Report Down OSDs[¶](https://docs.ceph.com/en/pacific/rados/configuration/mon-osd-interaction/ "Permalink to this headline")

デフォルトでは、異なるホストの2つのCeph OSDデーモンが、別のCeph OSDデーモンがダウンしたことをCeph Monitorに報告してから、報告されたCeph OSDデーモンがダウンしたことをCeph Monitorが認める必要があります。しかし、障害を報告しているすべてのOSDが、他のOSDとの接続に問題がある不良スイッチのあるラックでホストされている可能性があります。このような誤報を避けるために、障害を報告したピアを、同様に遅延しているクラスタ全体の潜在的な「サブクラスタ」の代理と考えます。_mon osd reporter subtree level_は、CRUSHマップの共通の祖先タイプによってピアを「サブクラスタ」にグループ化するために使用されます。デフォルトでは、別のCeph OSD Daemonのダウンを報告するには、異なるサブツリーからの2つのレポートのみが必要です。Ceph構成ファイルの\[mon\]セクションの下に_mon osd min down reporters_および_mon osd reporter subtree level_設定を追加するか、実行時に値を設定することで、Ceph OSD DaemonのダウンをCeph Monitorに報告するために必要な、固有のサブツリーからのレポーターの数と共通の祖先のタイプを変更できます。

![539d4767f6071669bd3abcc77432af9b55d9525c4a34ed11395ee05e6a33fd65.png](image/539d4767f6071669bd3abcc77432af9b55d9525c4a34ed11395ee05e6a33fd65.png)

## OSDs Report Peering Failure[¶](https://docs.ceph.com/en/pacific/rados/configuration/mon-osd-interaction/ "Permalink to this headline")

Ceph OSD Daemonが、そのCeph構成ファイルで定義されたCeph OSD Daemonのいずれとも\(またはクラスタマップと\)ピアリングできない場合、Ceph OSD Daemonは30秒ごとにクラスタマップの最新コピーを求めてCeph Monitorにpingを実行します。Ceph構成ファイルの\[ossd\]セクションの下に_osd mon heartbeat interval_設定を追加するか、実行時に値を設定することで、Ceph Monitorのハートビート間隔を変更できます。

![09c17cf28f1e86cffd075e9300e7e584186931c0bf675390878bd11d0f4fb700.png](image/09c17cf28f1e86cffd075e9300e7e584186931c0bf675390878bd11d0f4fb700.png)

## OSDs Report Their Status[¶](https://docs.ceph.com/en/pacific/rados/configuration/mon-osd-interaction/ "Permalink to this headline")

Ceph OSD DaemonがCeph Monitorにレポートしない場合、_mon osd report timeout_が経過すると、Ceph MonitorはCeph OSD Daemonがダウンしていると判断します。Ceph OSD Daemonは、障害、配置グループの統計情報の変更、up\_thruの変更などの報告可能なイベントが発生したとき、または5秒以内にブートしたときに、Ceph Monitorにレポートを送信します。Ceph OSD Daemonの最小レポート間隔を変更するには、Ceph構成ファイルの\[ossd\]セクションの下に_osd mon report interval_設定を追加するか、ランタイムに値を設定します。Ceph OSD Daemonは、注目すべき変更があったかどうかにかかわらず、120秒ごとにCeph Monitorにレポートを送信します。Ceph構成ファイルの\[osd\]セクションに_osd mon reporti nterval max_設定を追加するか、実行時にこの値を設定することで、Ceph Monitorのレポート間隔を変更できます。

![3bd973b9f631fb4d138cea852c1daef4326821a68e672f8ee6b6b4d7e98f5a94.png](image/3bd973b9f631fb4d138cea852c1daef4326821a68e672f8ee6b6b4d7e98f5a94.png)

## Configuration Settings[¶](https://docs.ceph.com/en/pacific/rados/configuration/mon-osd-interaction/ "Permalink to this headline")

ハートビート設定を変更する場合は、構成ファイルの\[global\]セクションに含める必要があります。

### Monitor Settings[¶](https://docs.ceph.com/en/pacific/rados/configuration/mon-osd-interaction/ "Permalink to this headline")

**mon osd min up ratio**

Description

CephがCeph OSDデーモンを「down」とマークするまでの、「up」のCeph OSDデーモンの最小比率

Type

_Double_

Default

_.3_

**mon osd min in ratio**

Description

CephがCeph OSDデーモンを「out」とマークするまでの、「in」 のCeph OSDデーモンの最小比率。

Type

_Double_

Default

_.75_

**mon osd laggy half life**

Description

ラギー推定値が減衰する秒数

Type

_Integer_

Default

_60\*60_

**mon osd laggy weight**

Description

ラギー推定 decayでの新規サンプルに対する重み

Type

_Double_

Default

_0.3_

**mon osd laggy max interval**

Description

ラギー推定におけるlaggy\_intervalの最大値（秒）。モニタは、特定のOSDのlaggy\_intervalを評価するために、適応的なアプローチを使用します。この値は、そのOSDの猶予時間の計算に使用されます。

Type

_Integer_

Default

_300_

**mon osd adjust heartbeat grace**

Description

trueに設定すると、Cephはラギー推定値に基づいてスケーリングします。

Type

_Boolean_

Default

_true_

**mon osd adjust down out interval**

Description

trueに設定すると、Cephはラギー推定値に基づいてスケーリングされます。

Type

_Boolean_

Default

_true_

**mon osd auto mark in**

Description

Cephは、起動したCeph OSDデーモンをCeph Storage Clusterの「in」であるとマークします。

Type

_Boolean_

Default

_false_

**mon osd auto mark auto out in**

Description

Cephは、Ceph Storage Clusterの「out」に自動マークされたCeph OSDデーモンのブートを「in」にマークします。

Type

_Boolean_

Default

_true_

**mon osd auto mark new in**

Description

Cephは、新しいCeph OSDデーモンの起動をCeph Storage Clusterの「in」としてマークします。

Type

_Boolean_

Default

_true_

**mon osd down out interval**

Description

Ceph OSDデーモンが応答しない場合に、CephがCeph OSDデーモンを「down」および「out」とマークするまでの待ち時間（秒数）

Type

_32\-bit Integer_

Default

_600_

**mon osd down out subtree limit**

Description

Cephが自動的に「out」をマークしない最小のCRUSHユニットタイプ。たとえば、ホストに設定されていて、ホストのすべてのOSDが「down」している場合、CephはこれらのOSDを自動的に「out」にしません。

Type

_String_

Default

_rack_

**mon osd report time out**

Description

応答しないCeph OSDデーモンを「down」と宣言するまでの猶予時間を秒単位で指定します。

Type

_32\-bit Integer_

Default

_900_

**mon osd min down reporters**

Description

Ceph OSDデーモンの「down」を報告するために必要なCeph OSDデーモンの最小数

Type

_32\-bit Integer_

Default

_2_

**mon\_osd\_reporter\_subtree\_level**

Description

報告者がカウントされる親バケットのレベル。OSDは、応答しない相手を見つけると、モニターに障害報告を送る。モニタは報告されたOSDを「out」とマークし、猶予期間を経て「down」とする。

Type

_String_

Default

_host_

### OSD Settings[¶](https://docs.ceph.com/en/pacific/rados/configuration/mon-osd-interaction/ "Permalink to this headline")

**osd\_heartbeat\_interval**

Description

Ceph OSDデーモンがピアにpingを実行する頻度\(秒単位\)

Type

_32\-bit Integer_

Default

_6_

**osd\_heartbeat\_grace**

Description

Ceph OSDデーモンがハートビートを表示していないときに、Ceph Storage Clusterが「down」とみなす経過時間。この設定は、モニタデーモンとOSDデーモンの両方が読めるように、\[mon\]と\[osd\]または\[global\]の両方のセクションで設定する必要があります。

Type

_32\-bit Integer_

Default

_20_

**osd\_mon\_heartbeat\_interval**

Description

Ceph OSD DaemonにCeph Monitorのピアがない場合に、Ceph OSD DaemonがCeph Monitorにpingする頻度。

Type

_32\-bit Integer_

Default

_30_

**osd\_mon\_heartbeat\_stat\_stale**

Description

この秒数だけ更新されていないハートビートping時間の報告を停止します。 ゼロに設定すると、この動作は無効になります。

Type

_32\-bit Integer_

Default

_3600_

**osd\_mon\_report\_interval**

Description

Ceph OSDデーモンが起動またはその他の報告可能なイベントからCeph Monitorに報告するまでに待機する秒数。

Type

_32\-bit Integer_

Default

_5_
