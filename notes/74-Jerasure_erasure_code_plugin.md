# 74: Jerasure erasure code plugin

**クリップソース:** [74: Jerasure erasure code plugin — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-jerasure/)

# Jerasure erasure code plugin[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-jerasure/ "Permalink to this headline")

jerasureプラグインは、最も汎用的で柔軟なプラグインで、CephのECプールのデフォルトでもあります。

jerasureプラグインは、[Jerasure](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-jerasure/)ライブラリをカプセル化したものです。パラメータをよりよく理解するために、jerasure のドキュメントを読むことをお勧めします。 

## Create a jerasure profile[¶](https://docs.ceph.com/en/pacific/rados/operations/erasure-code-jerasure/ "Permalink to this headline")

To create a new _jerasure_ erasure code profile:

```
ceph osd erasure-code-profile set {name} 
     plugin=jerasure 
     k={data-chunks} 
     m={coding-chunks} 
     technique={reed_sol_van|reed_sol_r6_op|cauchy_orig|cauchy_good|liberation|blaum_roth|liber8tion} 
     [crush-root={root}] 
     [crush-failure-domain={bucket-type}] 
     [crush-device-class={device-class}] 
     [directory={directory}] 
     [--force]
```

ここで：

**k={datachunks}**

Description

各オブジェクトはデータチャンク部分に分割され、それぞれが異なるOSDに格納される

Type

Integer

Required

Yes.

Example

4

**m={coding\-chunks}**

Description

各オブジェクトのコーディングチャンクを計算し、異なるOSDに保存する。コーディングチャンクの数は、データを失うことなくダウンすることができるOSDの数でもある

Type

Integer

Required

Yes.

Example

2

**technique={reed\_sol\_van|reed\_sol\_r6\_op|cauchy\_orig|cauchy\_good|liberation|blaum\_roth|liber8tion}**

Description

より柔軟な手法は reed\_sol\_van で、k と m を設定すれば十分である。reed\_sol\_r6\_op, liberation, blaum\_roth, liber8tion はすべて、m=2 でしか設定できないという意味で RAID6 と等価である

Type

String

Required

No.

Default

reed\_sol\_van

**packetsize={bytes}**

Description

エンコーディングは、バイトサイズのパケットを一度に処理する。正しいパケットサイズを選択することは困難である。jerasureのドキュメントには、このトピックに関する幅広い情報が記載されている

Type

Integer

Required

No.

Default

2048

**crush\-root={root}**

Description

CRUSHルールの最初のステップに使用されるCRUSHバケットの名前

Type

String

Required

No.

Default

default

**crush\-failure\-domain={bucket\-type}**

Description

同じ障害ドメインを持つバケットに、2つのチャンクが存在しないことを確認する。例えば、障害ドメインがhostの場合、2つのチャンクが同じhostに保存されることはない。step chooseleaf hostのようなCRUSHルールのステップを作成するために使用される

Type

String

Required

No.

Default

host

**crush\-device\-class={device\-class}**

Description

CRUSHマップのcrushデバイスクラス名を使って、特定のクラスのデバイス（例：ssdやhdd）に配置を制限する。

Type

String

Required

No.

**directory={directory}**

Description

ECプラグインの読み込み元となるディレクトリ名

Type

String

Required

No.

Default

/usr/lib/ceph/erasure\-code

**\-\-force**

Description

同名の既存プロファイルを上書きする

Type

String

Required

No.
