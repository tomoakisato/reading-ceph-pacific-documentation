# 148: CephFS Client Capabilities

**クリップソース:** [148: CephFS Client Capabilities — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/client-auth/)

# CephFS Client Capabilities[¶](https://docs.ceph.com/en/pacific/cephfs/client-auth/ "Permalink to this headline")

Cephの認証CAPSを使用して、ファイルシステムのクライアントを必要最小限の権限に制限します。

注：path制限とレイアウト変更制限は、CephのJewelリリースの新機能です。

注：CephFSでECプールを使用することは、BlueStore Backendでのみサポートされています。メタデータプールとして使用することはできず、データプールで上書きを有効にする必要があります。

## Path restriction[¶](https://docs.ceph.com/en/pacific/cephfs/client-auth/ "Permalink to this headline")

デフォルトでは、クライアントはマウントできるpathに制限がありません。さらに、クライアントがサブディレクトリ（例：/home/user）をマウントする場合、MDSはその後の操作がそのディレクトリ内に「ロック」されているかどうかをデフォルトでは確認しません。

クライアントが特定のディレクトリ内のみマウントして作業できるように制限するには、pathベースのMDS認証CAPSを使用します。

### Syntax[¶](https://docs.ceph.com/en/pacific/cephfs/client-auth/ "Permalink to this headline")

指定されたディレクトリのみへのrwアクセスを許可するには、次の構文を使用してクライアントのキーを作成するときに、ディレクトリを指定します：

```
ceph fs authorize <fs_name> client.<client_id> <path-in-cephfs> rw
```

たとえば、クライアントfooがファイルシステムcephfs\_aのbarディレクトリにのみ書き込みできるように制限するには、次のようにします：

```
ceph fs authorize cephfs_a client.foo / r /bar rw

results in:

client.foo
  key: *key*
  caps: [mds] allow r, allow rw path=/bar
  caps: [mon] allow r
  caps: [osd] allow rw tag cephfs data=cephfs_a
```

クライアントをbarディレクトリに完全に制限するには、ルートディレクトリを省略します

```
ceph fs authorize cephfs_a client.foo /bar rw
```

クライアントの読み取りアクセスがpathに制限されている場合、mountコマンドで読み取り可能なパスを指定したときのみ、ファイルシステムをマウントできることに注意してください（下記参照）。

ファイルシステム名として all または \* を指定すると、すべてのファイルシステムへのアクセスが許可されます。通常、シェルから保護するために \* をクオーテーションする必要があることに注意してください。

ユーザー管理の詳細については、「[ユーザー管理 \- Keyringへのユーザーの追�](https://docs.ceph.com/en/pacific/rados/operations/user-management/#add-a-user-to-a-keyring)�」を参照してください。

クライアントを指定されたサブディレクトリにのみ制限するには、マウント時に以下の構文で指定されたディレクトリを指定します：

```
ceph-fuse -n client.<client_id> <mount-path> -r *directory_to_be_mounted*
```

例えば、クライアントfooをmnt/barディレクトリに制限する場合、次のようになります：

```
ceph-fuse -n client.foo mnt -r /bar
```

### Free space reporting[¶](https://docs.ceph.com/en/pacific/cephfs/client-auth/ "Permalink to this headline")

デフォルトでは、クライアントがサブディレクトリをマウントしている場合、クラスタ上の全体的な使用量を報告するのではなく、そのサブディレクトリのクォータから使用領域（df）が計算されることになります。

もし、クライアントがマウントされたサブディレクトリのクォータ使用量だけでなく、ファイルシステム全体の使用量を報告したい場合、クライアントに以下の設定オプションを設定します：

```
client quota df = false
```

クォータが有効でない場合、またはマウントされたサブディレクトリにクォータが設定されていない場合は、この設定の値に関係なく、ファイルシステムの全体的な使用量が報告されます。

## Layout and Quota restriction \(the ‘p’ flag\)[¶](https://docs.ceph.com/en/pacific/cephfs/client-auth/ "Permalink to this headline")

レイアウトやクォータを設定するには、クライアントは 'rw' に加えて 'p' フラグを必要とします。これにより、「ceph.」プレフィックスを持つ特別な拡張属性によって設定されるすべての属性が制限され、これらのフィールドを設定する他の手段も制限されます（レイアウトを使用したopenc操作など）。

たとえば、次のスニペットでは、client.0はファイルシステムcephfs\_aのレイアウトとクォータを変更できますが、client.1は変更できません。

```
client.0
    key: AQAz7EVWygILFRAAdIcuJ12opU/JKyfFmxhuaw==
    caps: [mds] allow rwp
    caps: [mon] allow r
    caps: [osd] allow rw tag cephfs data=cephfs_a

client.1
    key: AQAz7EVWygILFRAAdIcuJ12opU/JKyfFmxhuaw==
    caps: [mds] allow rw
    caps: [mon] allow r
    caps: [osd] allow rw tag cephfs data=cephfs_a
```

## Snapshot restriction \(the ‘s’ flag\)[¶](https://docs.ceph.com/en/pacific/cephfs/client-auth/ "Permalink to this headline")

スナップショットを作成または削除するには、クライアントは 'rw' に加えて 's' フラグを必要とします。CAPS 文字列が 'p' フラグも含んでいる場合、 's' フラグはその後に記述しなければならないことに注意してください（'rw' 以外のフラグはすべてアルファベット順で指定しなければならなりません）。

たとえば、次のスニペットでは、client.0はファイルシステムcephfs\_aのbarディレクトリにスナップショットを作成または削除することができます。

```
client.0
    key: AQAz7EVWygILFRAAdIcuJ12opU/JKyfFmxhuaw==
    caps: [mds] allow rw, allow rws path=/bar
    caps: [mon] allow r
    caps: [osd] allow rw tag cephfs data=cephfs_a
```

## Network restriction[¶](https://docs.ceph.com/en/pacific/cephfs/client-auth/ "Permalink to this headline")

```
client.foo
  key: *key*
  caps: [mds] allow r network 10.0.0.0/8, allow rw path=/bar network 10.0.0.0/8
  caps: [mon] allow r network 10.0.0.0/8
  caps: [osd] allow rw tag cephfs data=cephfs_a network 10.0.0.0/8
```

オプションの{network/prefix}は、CIDR記法による標準的なネットワーク名とプレフィックス長（例：10.3.0.0/16）です。 存在する場合、このCAPSの使用は、このネットワークから接続するクライアントに制限されます。

## File system Information Restriction[¶](https://docs.ceph.com/en/pacific/cephfs/client-auth/ "Permalink to this headline")

必要であれば、モニタクラスタは利用可能なファイルシステムの限定ビューを表示することができます。この場合、モニタクラスタは、管理者が指定したファイルシステムについてのみクライアントに通知します。その他のファイルシステムは報告されず、それらに影響を与えるコマンドは、ファイルシステムが存在しないかのように失敗します。

次のような例を考えてみましょう。Cephクラスタには2つのFSがあります：

```
$ ceph fs ls
name: cephfs, metadata pool: cephfs_metadata, data pools: [cephfs_data ]
name: cephfs2, metadata pool: cephfs2_metadata, data pools: [cephfs2_data ]
```

しかし、我々はsomeuserクライアントを1つのFSに対してのみ認可します：

```
$ ceph fs authorize cephfs client.someuser / rw
[client.someuser]
    key = AQAmthpf89M+JhAAiHDYQkMiCq3x+J0n9e8REQ==
$ cat ceph.client.someuser.keyring
[client.someuser]
    key = AQAmthpf89M+JhAAiHDYQkMiCq3x+J0n9e8REQ==
    caps mds = "allow rw fsname=cephfs"
    caps mon = "allow r fsname=cephfs"
    caps osd = "allow rw tag cephfs data=cephfs"
```

そして、クライアントは認可を受けたFSしか見ることができません：

```
$ ceph fs ls -n client.someuser -k ceph.client.someuser.keyring
name: cephfs, metadata pool: cephfs_metadata, data pools: [cephfs_data ]
```

待機中のMDSデーモンは常に表示されます。制限されたMDSデーモンおよびファイルシステムに関する情報は、ceph health detailなどの他の手段で利用できるようになる可能性があることに注意してください。

## MDS communication restriction[¶](https://docs.ceph.com/en/pacific/cephfs/client-auth/ "Permalink to this headline")

デフォルトでは、ユーザーアプリケーションは、関連するファイルシステム上のデータの変更が許可されているか否かに関わらず、任意のMDSと通信することができます（上記のPath restrictionを参照）。クライアントの通信を特定のファイルシステムに関連するMDSデーモンに制限するには、そのファイルシ ステムに対するMDS CAPSを設定します。次の例では、Cephクラスタに2つのFSがある場合を考えてみましょう：

```
$ ceph fs ls
name: cephfs, metadata pool: cephfs_metadata, data pools: [cephfs_data ]
name: cephfs2, metadata pool: cephfs2_metadata, data pools: [cephfs2_data ]
```

クライアントsomeuserは、1つのFSに対してのみ権限を持ちます：

```
$ ceph fs authorize cephfs client.someuser / rw
[client.someuser]
    key = AQBPSARfg8hCJRAAEegIxjlm7VkHuiuntm6wsA==
$ ceph auth get client.someuser > ceph.client.someuser.keyring
exported keyring for client.someuser
$ cat ceph.client.someuser.keyring
[client.someuser]
    key = AQBPSARfg8hCJRAAEegIxjlm7VkHuiuntm6wsA==
    caps mds = "allow rw fsname=cephfs"
    caps mon = "allow r"
    caps osd = "allow rw tag cephfs data=cephfs"
```

someuserでcephfs1をマウントすると成功します：

```
$ sudo ceph-fuse /mnt/cephfs1 -n client.someuser -k ceph.client.someuser.keyring --client-fs=cephfs
ceph-fuse[96634]: starting ceph client
ceph-fuse[96634]: starting fuse
$ mount | grep ceph-fuse
ceph-fuse on /mnt/cephfs1 type fuse.ceph-fuse (rw,nosuid,nodev,relatime,user_id=0,group_id=0,allow_other)
```

しかし、cephfs2をマウントすると成功しません：

```
$ sudo ceph-fuse /mnt/cephfs2 -n client.someuser -k ceph.client.someuser.keyring --client-fs=cephfs2
ceph-fuse[96599]: starting ceph client
ceph-fuse[96599]: ceph mount failed with (1) Operation not permitted
```

## Root squash[¶](https://docs.ceph.com/en/pacific/cephfs/client-auth/ "Permalink to this headline")

rootsquash機能は、誤ってsudo rm \-rf /pathのようなシナリオを防ぐための安全対策として実装されています。MDS capsのroot\_squashモードを有効にすると、uid=0またはgid=0のクライアントが書き込みアクセス操作（例：rm、rmdir、rmsnap、mkdir、mksnap）を実行できないようにすることが可能です。しかし、このモードでは、他のファイルシステムとは異なり、rootクライアントの読み取り操作を許可しています。

以下は、ファイルシステム内の「/volumes」ディレクトリツリー以外での root\_squash を有効にする例：

```
$ ceph fs authorize a client.test_a / rw root_squash /volumes rw
$ ceph auth get client.test_a
[client.test_a]
    key = AQBZcDpfEbEUKxAADk14VflBXt71rL9D966mYA==
    caps mds = "allow rw fsname=a root_squash, allow rw fsname=a path=/volumes"
    caps mon = "allow r fsname=a"
    caps osd = "allow rw tag cephfs data=a"
```
