# 222: rbd-replay-many – replay a rados block device (RBD) workload on several clients

 # rbd\-replay\-many – replay a rados block device \(RBD\) workload on several clients[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay-many/ "Permalink to this headline")

## Synopsis[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay-many/ "Permalink to this headline")

**rbd\-replay\-many** \[ _options_ \] –original\-image _name_ _host1_ \[ _host2_ \[ … \] \] – _rbd\_replay\_args_

## Description[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay-many/ "Permalink to this headline")

**rbd\-replay\-many** is a utility for replaying a rados block device \(RBD\) workload on several clients. Although all clients use the same workload, they replay against separate images. This matches normal use of librbd, where each original client is a VM with its own image.

Configuration and replay files are not automatically copied to clients. Replay images must already exist.

## Options[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay-many/ "Permalink to this headline")

`--original-image`` name`[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay-many/ "Permalink to this definition")Specifies the name \(and snap\) of the originally traced image. Necessary for correct name mapping.

`--image-prefix`` prefix`[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay-many/ "Permalink to this definition")Prefix of image names to replay against. Specifying –image\-prefix=foo results in clients replaying against foo\-0, foo\-1, etc. Defaults to the original image name.

`--exec`` program`[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay-many/ "Permalink to this definition")Path to the rbd\-replay executable.

`--delay`` seconds`[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay-many/ "Permalink to this definition")Delay between starting each client. Defaults to 0.

## Examples[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay-many/ "Permalink to this headline")

Typical usage:

```
rbd-replay-many host-0 host-1 --original-image=image -- -c ceph.conf replay.bin
```

This results in the following commands being executed:

```
ssh host-0 'rbd-replay' --map-image 'image=image-0' -c ceph.conf replay.bin
ssh host-1 'rbd-replay' --map-image 'image=image-1' -c ceph.conf replay.bin
```

## Availability[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay-many/ "Permalink to this headline")

**rbd\-replay\-many** is part of Ceph, a massively scalable, open\-source, distributed storage system. Please refer to the Ceph documentation at [http://ceph.com/docs](https://docs.ceph.com/en/pacific/man/8/rbd-replay-many/) for more information.

## See also[¶](https://docs.ceph.com/en/pacific/man/8/rbd-replay-many/ "Permalink to this headline")

[rbd\-replay](https://docs.ceph.com/en/pacific/man/8/rbd-replay-many/)\(8\),[rbd](https://docs.ceph.com/en/pacific/man/8/rbd-replay-many/)\(8\)
