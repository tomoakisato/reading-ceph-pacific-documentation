# 314: API Documentation

**クリップソース:** [314: API Documentation — Ceph Documentation](https://docs.ceph.com/en/pacific/api/)

# API Documentation[¶](https://docs.ceph.com/en/pacific/api/ "Permalink to this headline")

## Ceph RESTful API[¶](https://docs.ceph.com/en/pacific/api/ "Permalink to this headline")

See [Ceph REST API](https://docs.ceph.com/en/pacific/mgr/ceph_api/).

## Ceph Storage Cluster APIs[¶](https://docs.ceph.com/en/pacific/api/ "Permalink to this headline")

See [Ceph Storage Cluster APIs](https://docs.ceph.com/en/pacific/rados/api/).

## Ceph File System APIs[¶](https://docs.ceph.com/en/pacific/api/ "Permalink to this headline")

See [libcephfs](https://docs.ceph.com/en/pacific/cephfs/api)

## Ceph Block Device APIs[¶](https://docs.ceph.com/en/pacific/api/ "Permalink to this headline")

See [librbdpy](https://docs.ceph.com/en/pacific/rbd/api/librbdpy).

## Ceph RADOS Gateway APIs[¶](https://docs.ceph.com/en/pacific/api/ "Permalink to this headline")

See [librgw\-py](https://docs.ceph.com/en/pacific/radosgw/api).

## Ceph Object Store APIs[¶](https://docs.ceph.com/en/pacific/api/ "Permalink to this headline")

* See 
* See 
* See 

## Ceph MON Command API[¶](https://docs.ceph.com/en/pacific/api/ "Permalink to this headline")

* See 
