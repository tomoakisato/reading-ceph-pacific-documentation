# 149: Mount CephFS: Prerequisites

**クリップソース:** [149: Mount CephFS: Prerequisites — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/mount-prerequisites/)

# Mount CephFS: Prerequisites[¶](https://docs.ceph.com/en/pacific/cephfs/mount-prerequisites/ "Permalink to this headline")

CephFSを使用するには、ローカルファイルシステムにマウントするか、[cephfs\-shell](https://docs.ceph.com/en/pacific/cephfs/cephfs-shell)を使用します。CephFSは、[FUSEを使う](https://docs.ceph.com/en/pacific/cephfs/mount-using-fuse)だけでなく、[カーネルを使って](https://docs.ceph.com/en/pacific/cephfs/mount-using-kernel-driver)マウントすることも可能です。どちらにもそれぞれの利点があります。次のセクションを読んで、CephFSをマウントするこれら2つの方法の詳細を理解します。

WindowsのCephFSマウントについては、[ceph\-dokan](https://docs.ceph.com/en/pacific/cephfs/ceph-dokan)のページをご確認ください。

## Which CephFS Client?[¶](https://docs.ceph.com/en/pacific/cephfs/mount-prerequisites/ "Permalink to this headline")

FUSEクライアントは最もアクセスしやすく、ストレージクラスタが使用するCephのバージョンにアップグレードするのが最も簡単ですが、カーネルクライアントは常に優れたパフォーマンスを提供します。

バグやパフォーマンスの問題に遭遇したとき、そのバグがクライアント固有のものであるかどうかを調べる（そして、開発者に知らせる）ために、他のクライアントを使用してみることはしばしば有益です。

## General Pre\-requisite for Mounting CephFS[¶](https://docs.ceph.com/en/pacific/cephfs/mount-prerequisites/ "Permalink to this headline")

CephFSをマウントする前に、\(CephFSをマウントして使用する）クライアントホストにCeph構成ファイル\(ceph.conf\)のコピーとMDSへのアクセス権限を持つCephXユーザのキーリングがあることを確認します。これらのファイルは両方とも、Ceph MONが存在するホスト上に既に存在する必要があります。

1. クライアントホスト用の最小限のconfファイルを生成し、標準的な場所に配置します：

```
# on client host
mkdir -p -m 755 /etc/ceph
ssh {user}@{mon-host} "sudo ceph config generate-minimal-conf" | sudo tee /etc/ceph/ceph.conf
```

また、confファイルをコピーすることもできます。しかし、上記の方法は最小限の詳細を含むconfを生成するので、通常はこれで十分です。詳細については、[クライアント認証](https://docs.ceph.com/en/pacific/cephfs/client-auth)と[ブートストラップオプション](https://docs.ceph.com/en/pacific/rados/configuration/ceph-conf/#bootstrap-options)を参照してください。

1. confに適切な権限があることを確認します：

```
chmod 644 /etc/ceph/ceph.conf
```

1. CephXユーザを作成し、その秘密鍵を取得します：

```
ssh {user}@{mon-host} "sudo ceph fs authorize cephfs client.foo / rw" | sudo tee /etc/ceph/ceph.client.foo.keyring
```

上記コマンドのうち、cephfsはCephFSの名前、fooはCephXのユーザー名、/はクライアントホストにアクセスを許可するCephFS内のパス、rwは読み取りと書き込みの両方のパーミッションに置き換えてください。または、/etc/cephでMONホストからクライアントホストにCephキーリングをコピーすることもできますが、クライアントホストに固有のキーリングを作成する方がよいでしょう。CephXキーリング/クライアントを作成する際、複数のマシンで同じクライアント名を使用することは全く問題ありません。

注意: 上記2つのコマンドのいずれかを実行中にパスワードのプロンプトが2回表示された場合は、これらのコマンドの直前に sudo ls \(または sudo を使用するその他の些細なコマンド\) を実行してください。

1. keyringに適切な権限があることを確認します：

```
chmod 600 /etc/ceph/ceph.client.foo.keyring
```

注意：カーネルとFUSEのマウントには、個別にいくつかの前提条件がある場合があります。
