# 283: Role

**クリップソース:** [283: Role — Ceph Documentation](https://docs.ceph.com/en/pacific/radosgw/role/)

# Role[¶](https://docs.ceph.com/en/pacific/radosgw/role/ "Permalink to this headline")

ロール（role）はユーザに似ており、ロールができること、できないことを決定する許可ポリシーがアタッチされています。ロールは、それを必要とするすべてのIDによって引き受ける（assume）ことができます。ユーザがロールを引き受けた場合、動的に作成された一時的なクレデンシャルセットがユーザに返されます。ロールは、s3リソースへのアクセス許可を持たないユーザ、アプリケーション、サービスにアクセスを委譲（delegate）するために使用できます。

以下のradosgw\-adminコマンドを使用して、ロールと関連する権限を作成/削除/更新することができます。

## Create a Role[¶](https://docs.ceph.com/en/pacific/radosgw/role/ "Permalink to this headline")

ロールを作成するには、以下を実行します：

```
radosgw-admin role create --role-name={role-name} [--path=="{path to the role}"] [--assume-role-policy-doc={trust-policy-document}]
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/role/ "Permalink to this headline")

**role\-name**

Description

ロール名

Type

String

**path**

Description

ロールへのpath。デフォルトはスラッシュ\(/\)

Type

String

**assume\-role\-policy\-doc**

Description

エンティティにロール引き受けを許可する信頼関係ポリシードキュメント

Type

String

For example:

```
radosgw-admin role create --role-name=S3Access1 --path=/application_abc/component_xyz/ --assume-role-policy-doc={"Version":"2012-10-17","Statement":[{"Effect":"Allow","Principal":{"AWS":["arn:aws:iam:::user/TESTER"]},"Action":["sts:AssumeRole"]}]}
```

```
{
  "id": "ca43045c-082c-491a-8af1-2eebca13deec",
  "name": "S3Access1",
  "path": "/application_abc/component_xyz/",
  "arn": "arn:aws:iam:::role/application_abc/component_xyz/S3Access1",
  "create_date": "2018-10-17T10:18:29.116Z",
  "max_session_duration": 3600,
  "assume_role_policy_document": "{"Version":"2012-10-17","Statement":[{"Effect":"Allow","Principal":{"AWS":["arn:aws:iam:::user/TESTER"]},"Action":["sts:AssumeRole"]}]}"
}
```

## Delete a Role[¶](https://docs.ceph.com/en/pacific/radosgw/role/ "Permalink to this headline")

ロールを削除するには、次のように実行します：

```
radosgw-admin role rm --role-name={role-name}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/role/ "Permalink to this headline")

**role\-name**

Description

ロール名

Type

String

For example:

```
radosgw-admin role rm --role-name=S3Access1
```

注：ロールは、権限ポリシがアタッチされていない場合にのみ削除できます。

## Get a Role[¶](https://docs.ceph.com/en/pacific/radosgw/role/ "Permalink to this headline")

ロールの情報を取得するには、次のように実行します：

```
radosgw-admin role get --role-name={role-name}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/role/ "Permalink to this headline")

**role\-name**

Description

ロール名

Type

String

For example:

```
radosgw-admin role get --role-name=S3Access1
```

```
{
  "id": "ca43045c-082c-491a-8af1-2eebca13deec",
  "name": "S3Access1",
  "path": "/application_abc/component_xyz/",
  "arn": "arn:aws:iam:::role/application_abc/component_xyz/S3Access1",
  "create_date": "2018-10-17T10:18:29.116Z",
  "max_session_duration": 3600,
  "assume_role_policy_document": "{"Version":"2012-10-17","Statement":[{"Effect":"Allow","Principal":{"AWS":["arn:aws:iam:::user/TESTER"]},"Action":["sts:AssumeRole"]}]}"
}
```

## List Roles[¶](https://docs.ceph.com/en/pacific/radosgw/role/ "Permalink to this headline")

特定のパスプレフィックスのロールを一覧表示するには、次のように実行します：

```
radosgw-admin role list [--path-prefix ={path prefix}]
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/role/ "Permalink to this headline")

**path\-prefix**

Description

ロールをフィルタリングするためのパスプレフィックス。指定しない場合、すべてのロールがリストアップされる

Type

String

For example:

```
radosgw-admin role list --path-prefix="/application"
```

```
[
  {
      "id": "3e1c0ff7-8f2b-456c-8fdf-20f428ba6a7f",
      "name": "S3Access1",
      "path": "/application_abc/component_xyz/",
      "arn": "arn:aws:iam:::role/application_abc/component_xyz/S3Access1",
      "create_date": "2018-10-17T10:32:01.881Z",
      "max_session_duration": 3600,
      "assume_role_policy_document": "{"Version":"2012-10-17","Statement":[{"Effect":"Allow","Principal":{"AWS":["arn:aws:iam:::user/TESTER"]},"Action":["sts:AssumeRole"]}]}"
  }
]
```

## Update Assume Role Policy Document of a role[¶](https://docs.ceph.com/en/pacific/radosgw/role/ "Permalink to this headline")

ロールの信頼関係ポリシードキュメントを変更する場合は、以下を実行します：

```
radosgw-admin role modify --role-name={role-name} --assume-role-policy-doc={trust-policy-document}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/role/ "Permalink to this headline")

**role\-name**

Description

ロール名

Type

String

**assume\-role\-policy\-doc**

Description

エンティティにロール引き受けを許可する信頼関係ポリシードキュメント

Type

String

For example:

```
radosgw-admin role modify --role-name=S3Access1 --assume-role-policy-doc={"Version":"2012-10-17","Statement":[{"Effect":"Allow","Principal":{"AWS":["arn:aws:iam:::user/TESTER2"]},"Action":["sts:AssumeRole"]}]}
```

```
{
  "id": "ca43045c-082c-491a-8af1-2eebca13deec",
  "name": "S3Access1",
  "path": "/application_abc/component_xyz/",
  "arn": "arn:aws:iam:::role/application_abc/component_xyz/S3Access1",
  "create_date": "2018-10-17T10:18:29.116Z",
  "max_session_duration": 3600,
  "assume_role_policy_document": "{"Version":"2012-10-17","Statement":[{"Effect":"Allow","Principal":{"AWS":["arn:aws:iam:::user/TESTER2"]},"Action":["sts:AssumeRole"]}]}"
}
```

上記の例では、assume\_role\_policy\_documentの Principal を TESTER から TESTER2 に変更します。

## Add/ Update a Policy attached to a Role[¶](https://docs.ceph.com/en/pacific/radosgw/role/ "Permalink to this headline")

ロールにアタッチするインラインポリシーを追加/更新するには、次のように実行します：

```
radosgw-admin role policy put --role-name={role-name} --policy-name={policy-name} --policy-doc={permission-policy-doc}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/role/ "Permalink to this headline")

**role\-name**

Description

ロール名

Type

String

**policy\-name**

Description

ポリシー名

Type

String

**policy\-doc**

Description

パーミッションポリシードキュメント

Type

String

For example:

```
radosgw-admin role-policy put --role-name=S3Access1 --policy-name=Policy1 --policy-doc={"Version":"2012-10-17","Statement":[{"Effect":"Allow","Action":["s3:*"],"Resource":"arn:aws:s3:::example_bucket"}]}
```

上記の例では、ロール「S3Access1」にポリシー「Policy1」をアタッチし、「example\_bucket」に対するすべてのs3アクションを許可しています。

## List Permission Policy Names attached to a Role[¶](https://docs.ceph.com/en/pacific/radosgw/role/ "Permalink to this headline")

ロールにアタッチされているパーミションポリシー名を一覧表示するには、次のように実行します：

```
radosgw-admin role policy get --role-name={role-name}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/role/ "Permalink to this headline")

**role\-name**

Description

ロール名Type

String

For example:

```
radosgw-admin role-policy list --role-name=S3Access1
```

```
[
  "Policy1"
]
```

## Get Permission Policy attached to a Role[¶](https://docs.ceph.com/en/pacific/radosgw/role/ "Permalink to this headline")

ロールにアタッチされている特定のパーミションポリシーを取得するには、以下を実行します：

```
radosgw-admin role policy get --role-name={role-name} --policy-name={policy-name}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/role/ "Permalink to this headline")

**role\-name**

Description

ロール名

Type

String

**policy\-name**

Description

ポリシー名

Type

String

For example:

```
radosgw-admin role-policy get --role-name=S3Access1 --policy-name=Policy1
```

```
{
  "Permission policy": "{"Version":"2012-10-17","Statement":[{"Effect":"Allow","Action":["s3:*"],"Resource":"arn:aws:s3:::example_bucket"}]}"
}
```

## Delete Policy attached to a Role[¶](https://docs.ceph.com/en/pacific/radosgw/role/ "Permalink to this headline")

ロールにアタッチされているパーミションポリシーを削除するには、次の操作を行います：

```
radosgw-admin role policy rm --role-name={role-name} --policy-name={policy-name}
```

### Request Parameters[¶](https://docs.ceph.com/en/pacific/radosgw/role/ "Permalink to this headline")

**role\-name**

Description

ロール名

Type

String

**policy\-name**

Description

ポリシー名

Type

String

For example:

```
radosgw-admin role-policy get --role-name=S3Access1 --policy-name=Policy1
```

#### REST APIs for Manipulating a Role[¶](https://docs.ceph.com/en/pacific/radosgw/role/ "Permalink to this headline")

ロールの操作には、上記のradosgw\-adminコマンドの他に、以下のREST APIを利用することが可能です。リクエストパラメータとその説明については、上記のセクションを参照してください。

REST admin API を呼び出すには、admin CAPSを持つユーザを作成する必要があります。

```
radosgw-admin --uid TESTER --display-name "TestUser" --access_key TESTER --secret test123 user create
radosgw-admin caps add --uid="TESTER" --caps="roles=*"
```

## Create a Role[¶](https://docs.ceph.com/en/pacific/radosgw/role/ "Permalink to this headline")

Example::

POST “\<hostname\>?Action=CreateRole&RoleName=S3Access&Path=/application\_abc/component\_xyz/&AssumeRolePolicyDocument={"Version":"2012\-10\-17","Statement":\[{"Effect":"Allow","Principal":{"AWS":\["arn:aws:iam:::user/TESTER"\]},"Action":\["sts:AssumeRole"\]}\]}”

```
<role>
  <id>8f41f4e0-7094-4dc0-ac20-074a881ccbc5</id>
  <name>S3Access</name>
  <path>/application_abc/component_xyz/</path>
  <arn>arn:aws:iam:::role/application_abc/component_xyz/S3Access</arn>
  <create_date>2018-10-23T07:43:42.811Z</create_date>
  <max_session_duration>3600</max_session_duration>
  <assume_role_policy_document>{"Version":"2012-10-17","Statement":[{"Effect":"Allow","Principal":{"AWS":["arn:aws:iam:::user/TESTER"]},"Action":["sts:AssumeRole"]}]}</assume_role_policy_document>
</role>
```

## Delete a Role[¶](https://docs.ceph.com/en/pacific/radosgw/role/ "Permalink to this headline")

Example::

POST “\<hostname\>?Action=DeleteRole&RoleName=S3Access”

注：ロールは、パーミションポリシーがアタッチされていない場合にのみ削除できます。

## Get a Role[¶](https://docs.ceph.com/en/pacific/radosgw/role/ "Permalink to this headline")

Example::

POST “\<hostname\>?Action=GetRole&RoleName=S3Access”

```
<role>
  <id>8f41f4e0-7094-4dc0-ac20-074a881ccbc5</id>
  <name>S3Access</name>
  <path>/application_abc/component_xyz/</path>
  <arn>arn:aws:iam:::role/application_abc/component_xyz/S3Access</arn>
  <create_date>2018-10-23T07:43:42.811Z</create_date>
  <max_session_duration>3600</max_session_duration>
  <assume_role_policy_document>{"Version":"2012-10-17","Statement":[{"Effect":"Allow","Principal":{"AWS":["arn:aws:iam:::user/TESTER"]},"Action":["sts:AssumeRole"]}]}</assume_role_policy_document>
</role>
```

## List Roles[¶](https://docs.ceph.com/en/pacific/radosgw/role/ "Permalink to this headline")

Example::

POST “\<hostname\>?Action=ListRoles&RoleName=S3Access&PathPrefix=/application”

```
<role>
  <id>8f41f4e0-7094-4dc0-ac20-074a881ccbc5</id>
  <name>S3Access</name>
  <path>/application_abc/component_xyz/</path>
  <arn>arn:aws:iam:::role/application_abc/component_xyz/S3Access</arn>
  <create_date>2018-10-23T07:43:42.811Z</create_date>
  <max_session_duration>3600</max_session_duration>
  <assume_role_policy_document>{"Version":"2012-10-17","Statement":[{"Effect":"Allow","Principal":{"AWS":["arn:aws:iam:::user/TESTER"]},"Action":["sts:AssumeRole"]}]}</assume_role_policy_document>
</role>
```

## Update Assume Role Policy Document[¶](https://docs.ceph.com/en/pacific/radosgw/role/ "Permalink to this headline")

Example::

POST “\<hostname\>?Action=UpdateAssumeRolePolicy&RoleName=S3Access&PolicyDocument={"Version":"2012\-10\-17","Statement":\[{"Effect":"Allow","Principal":{"AWS":\["arn:aws:iam:::user/TESTER2"\]},"Action":\["sts:AssumeRole"\]}\]}”

## Add/ Update a Policy attached to a Role[¶](https://docs.ceph.com/en/pacific/radosgw/role/ "Permalink to this headline")

Example::

POST “\<hostname\>?Action=PutRolePolicy&RoleName=S3Access&PolicyName=Policy1&PolicyDocument={"Version":"2012\-10\-17","Statement":\[{"Effect":"Allow","Action":\["s3:CreateBucket"\],"Resource":"arn:aws:s3:::example\_bucket"}\]}”

## List Permission Policy Names attached to a Role[¶](https://docs.ceph.com/en/pacific/radosgw/role/ "Permalink to this headline")

Example::

POST “\<hostname\>?Action=ListRolePolicies&RoleName=S3Access”

```
<PolicyNames>
  <member>Policy1</member>
</PolicyNames>
```

## Get Permission Policy attached to a Role[¶](https://docs.ceph.com/en/pacific/radosgw/role/ "Permalink to this headline")

Example::

POST “\<hostname\>?Action=GetRolePolicy&RoleName=S3Access&PolicyName=Policy1”

```
<GetRolePolicyResult>
  <PolicyName>Policy1</PolicyName>
  <RoleName>S3Access</RoleName>
  <Permission_policy>{"Version":"2012-10-17","Statement":[{"Effect":"Allow","Action":["s3:CreateBucket"],"Resource":"arn:aws:s3:::example_bucket"}]}</Permission_policy>
</GetRolePolicyResult>
```

## Delete Policy attached to a Role[¶](https://docs.ceph.com/en/pacific/radosgw/role/ "Permalink to this headline")

Example::

POST “\<hostname\>?Action=DeleteRolePolicy&RoleName=S3Access&PolicyName=Policy1”
