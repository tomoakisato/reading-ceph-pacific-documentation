# 396: Erasure Code developer notes — Ceph Documentation

 # Erasure Code developer notes[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/erasure_coding/developer_notes/ "Permalink to this headline")

## Introduction[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/erasure_coding/developer_notes/ "Permalink to this headline")

Each chapter of this document explains an aspect of the implementation of the erasure code within Ceph. It is mostly based on examples being explained to demonstrate how things work.

## Reading and writing encoded chunks from and to OSDs[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/erasure_coding/developer_notes/ "Permalink to this headline")

An erasure coded pool stores each object as K\+M chunks. It is divided into K data chunks and M coding chunks. The pool is configured to have a size of K\+M so that each chunk is stored in an OSD in the acting set. The rank of the chunk is stored as an attribute of the object.

Let’s say an erasure coded pool is created to use five OSDs \( K\+M = 5 \) and sustain the loss of two of them \( M = 2 \).

When the object _NYAN_ containing _ABCDEFGHI_ is written to it, the erasure encoding function splits the content in three data chunks, simply by dividing the content in three : the first contains _ABC_, the second _DEF_ and the last _GHI_. The content will be padded if the content length is not a multiple of K. The function also creates two coding chunks : the fourth with _YXY_ and the fifth with _GQC_. Each chunk is stored in an OSD in the acting set. The chunks are stored in objects that have the same name \( _NYAN_ \) but reside on different OSDs. The order in which the chunks were created must be preserved and is stored as an attribute of the object \( shard\_t \), in addition to its name. Chunk _1_ contains _ABC_ and is stored on _OSD5_ while chunk _4_contains _YXY_ and is stored on _OSD3_.

```
                          +-------------------+
                     name |        NYAN       |
                          +-------------------+
                  content |      ABCDEFGHI    |
                          +--------+----------+
                                   |
                                   |
                                   v
                            +------+------+
            +---------------+ encode(3,2) +-----------+
            |               +--+--+---+---+           |
            |                  |  |   |               |
            |          +-------+  |   +-----+         |
            |          |          |         |         |
         +--v---+   +--v---+   +--v---+  +--v---+  +--v---+
   name  | NYAN |   | NYAN |   | NYAN |  | NYAN |  | NYAN |
         +------+   +------+   +------+  +------+  +------+
  shard  |  1   |   |  2   |   |  3   |  |  4   |  |  5   |
         +------+   +------+   +------+  +------+  +------+
content  | ABC  |   | DEF  |   | GHI  |  | YXY  |  | QGC  |
         +--+---+   +--+---+   +--+---+  +--+---+  +--+---+
            |          |          |         |         |
            |          |          |         |         |
            |          |       +--+---+     |         |
            |          |       | OSD1 |     |         |
            |          |       +------+     |         |
            |          |       +------+     |         |
            |          +------>| OSD2 |     |         |
            |                  +------+     |         |
            |                  +------+     |         |
            |                  | OSD3 |<----+         |
            |                  +------+               |
            |                  +------+               |
            |                  | OSD4 |<--------------+
            |                  +------+
            |                  +------+
            +----------------->| OSD5 |
                               +------+
```

When the object _NYAN_ is read from the erasure coded pool, the decoding function reads three chunks : chunk _1_ containing _ABC_, chunk _3_ containing _GHI_ and chunk _4_ containing _YXY_ and rebuild the original content of the object _ABCDEFGHI_. The decoding function is informed that the chunks _2_ and _5_ are missing \( they are called_erasures_ \). The chunk _5_ could not be read because the _OSD4_ is_out_.

The decoding function could be called as soon as three chunks are read : _OSD2_ was the slowest and its chunk does not need to be taken into account. This optimization is not implemented in Firefly.

```
                          +-------------------+
                     name |        NYAN       |
                          +-------------------+
                  content |      ABCDEFGHI    |
                          +--------+----------+
                                   ^
                                   |
                                   |
                            +------+------+
                            | decode(3,2) |
                            | erasures 2,5|
            +-------------->|             |
            |               +-------------+
            |                     ^   ^
            |                     |   +-----+
            |                     |         |
         +--+---+   +------+   +--+---+  +--+---+
   name  | NYAN |   | NYAN |   | NYAN |  | NYAN |
         +------+   +------+   +------+  +------+
  shard  |  1   |   |  2   |   |  3   |  |  4   |
         +------+   +------+   +------+  +------+
content  | ABC  |   | DEF  |   | GHI  |  | YXY  |
         +--+---+   +--+---+   +--+---+  +--+---+
            ^          .          ^         ^
            |    TOO   .          |         |
            |    SLOW  .       +--+---+     |
            |          ^       | OSD1 |     |
            |          |       +------+     |
            |          |       +------+     |
            |          +-------| OSD2 |     |
            |                  +------+     |
            |                  +------+     |
            |                  | OSD3 |-----+
            |                  +------+
            |                  +------+
            |                  | OSD4 | OUT
            |                  +------+
            |                  +------+
            +------------------| OSD5 |
                               +------+
```

## Erasure code library[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/erasure_coding/developer_notes/ "Permalink to this headline")

Using [Reed\-Solomon](https://docs.ceph.com/en/pacific/dev/osd_internals/erasure_coding/developer_notes/), with parameters K\+M, object O is encoded by dividing it into chunks O1, O2, … OM and computing coding chunks P1, P2, … PK. Any K chunks out of the available K\+M chunks can be used to obtain the original object. If data chunk O2 or coding chunk P2 are lost, they can be repaired using any K chunks out of the K\+M chunks. If more than M chunks are lost, it is not possible to recover the object.

Reading the original content of object O can be a simple concatenation of O1, O2, … OM, because the plugins are using[systematic codes](https://docs.ceph.com/en/pacific/dev/osd_internals/erasure_coding/developer_notes/). Otherwise the chunks must be given to the erasure code library _decode_ method to retrieve the content of the object.

Performance depend on the parameters to the encoding functions and is also influenced by the packet sizes used when calling the encoding functions \( for Cauchy or Liberation for instance \): smaller packets means more calls and more overhead.

Although Reed\-Solomon is provided as a default, Ceph uses it via an[abstract API](https://docs.ceph.com/en/pacific/dev/osd_internals/erasure_coding/developer_notes/) designed to allow each pool to choose the plugin that implements it using key=value pairs stored in an [erasure code profile](https://docs.ceph.com/en/pacific/dev/osd_internals/erasure_coding/developer_notes/).

```
$ ceph osd erasure-code-profile set myprofile 
    crush-failure-domain=osd
$ ceph osd erasure-code-profile get myprofile
directory=/usr/lib/ceph/erasure-code
k=2
m=1
plugin=jerasure
technique=reed_sol_van
crush-failure-domain=osd
$ ceph osd pool create ecpool erasure myprofile
```

The _plugin_ is dynamically loaded from _directory_ and expected to implement the _int \_\_erasure\_code\_init\(char \*plugin\_name, char \*directory\)_ function which is responsible for registering an object derived from _ErasureCodePlugin_in the registry. The [ErasureCodePluginExample](https://docs.ceph.com/en/pacific/dev/osd_internals/erasure_coding/developer_notes/) plugin reads:

```
ErasureCodePluginRegistry &instance =
                           ErasureCodePluginRegistry::instance();
instance.add(plugin_name, new ErasureCodePluginExample());
```

The _ErasureCodePlugin_ derived object must provide a factory method from which the concrete implementation of the _ErasureCodeInterface_object can be generated. The [ErasureCodePluginExample plugin](https://docs.ceph.com/en/pacific/dev/osd_internals/erasure_coding/developer_notes/) reads:

```
virtual int factory(const map<std::string,std::string> &parameters,
                    ErasureCodeInterfaceRef *erasure_code) {
  *erasure_code = ErasureCodeInterfaceRef(new ErasureCodeExample(parameters));
  return 0;
}
```

The _parameters_ argument is the list of _key=value_ pairs that were set in the erasure code profile, before the pool was created.

```
ceph osd erasure-code-profile set myprofile 
   directory=<dir>          # mandatory
   plugin=jerasure          # mandatory
   m=10                     # optional and plugin dependent
   k=3                      # optional and plugin dependent
   technique=reed_sol_van   # optional and plugin dependent
```

## Notes[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/erasure_coding/developer_notes/ "Permalink to this headline")

If the objects are large, it may be impractical to encode and decode them in memory. However, when using _RBD_ a 1TB device is divided in many individual 4MB objects and _RGW_ does the same.

Encoding and decoding is implemented in the OSD. Although it could be implemented client side for read write, the OSD must be able to encode and decode on its own when scrubbing.
