# 395: Erasure Coded Placement Groups — Ceph Documentation

 # Erasure Coded Placement Groups[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/erasure_coding/ "Permalink to this headline")

## Glossary[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/erasure_coding/ "Permalink to this headline")

_chunk_when the encoding function is called, it returns chunks of the same size. Data chunks which can be concatenated to reconstruct the original object and coding chunks which can be used to rebuild a lost chunk.

_chunk rank_the index of a chunk when returned by the encoding function. The rank of the first chunk is 0, the rank of the second chunk is 1 etc.

_stripe_when an object is too large to be encoded with a single call, each set of chunks created by a call to the encoding function is called a stripe.

_shard|strip_an ordered sequence of chunks of the same rank from the same object. For a given placement group, each OSD contains shards of the same rank. When dealing with objects that are encoded with a single operation, _chunk_ is sometime used instead of _shard_because the shard is made of a single chunk. The _chunks_ in a_shard_ are ordered according to the rank of the stripe they belong to.

_K_the number of data _chunks_, i.e. the number of _chunks_ in which the original object is divided. For instance if _K_ = 2 a 10KB object will be divided into _K_ objects of 5KB each.

_M_the number of coding _chunks_, i.e. the number of additional _chunks_computed by the encoding functions. If there are 2 coding _chunks_, it means 2 OSDs can be out without losing data.

_N_the number of data _chunks_ plus the number of coding _chunks_, i.e. _K\+M_.

_rate_the proportion of the _chunks_ that contains useful information, i.e. _K/N_. For instance, for _K_ = 9 and _M_ = 3 \(i.e. _K\+M_ = _N_ = 12\) the rate is_K_ = 9 / _N_ = 12 = 0.75, i.e. 75% of the chunks contain useful information.

The definitions are illustrated as follows \(PG stands for placement group\):

```
                OSD 40                       OSD 33
      +-------------------------+ +-------------------------+
      |      shard 0 - PG 10    | |      shard 1 - PG 10    |
      |+------ object O -------+| |+------ object O -------+|
      ||+---------------------+|| ||+---------------------+||
stripe|||    chunk  0         ||| |||    chunk  1         ||| ...
  0   |||    stripe 0         ||| |||    stripe 0         |||
      ||+---------------------+|| ||+---------------------+||
      ||+---------------------+|| ||+---------------------+||
stripe|||    chunk  0         ||| |||    chunk  1         ||| ...
  1   |||    stripe 1         ||| |||    stripe 1         |||
      ||+---------------------+|| ||+---------------------+||
      ||+---------------------+|| ||+---------------------+||
stripe|||    chunk  0         ||| |||    chunk  1         ||| ...
  2   |||    stripe 2         ||| |||    stripe 2         |||
      ||+---------------------+|| ||+---------------------+||
      |+-----------------------+| |+-----------------------+|
      |         ...             | |         ...             |
      +-------------------------+ +-------------------------+
```

## Table of content[¶](https://docs.ceph.com/en/pacific/dev/osd_internals/erasure_coding/ "Permalink to this headline")

* [Developer notes](https://docs.ceph.com/en/pacific/dev/osd_internals/erasure_coding/)
* [Jerasure plugin](https://docs.ceph.com/en/pacific/dev/osd_internals/erasure_coding/)
* [ECBackend Implementation Strategy](https://docs.ceph.com/en/pacific/dev/osd_internals/erasure_coding/)
* [ECBackend](https://docs.ceph.com/en/pacific/dev/osd_internals/erasure_coding/)
