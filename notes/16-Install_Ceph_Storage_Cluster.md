# 16: Install Ceph Storage Cluster

**クリップソース:** [16: Install Ceph Storage Cluster — Ceph Documentation](https://docs.ceph.com/en/pacific/install/install-storage-cluster/)

# Install Ceph Storage Cluster¶

このガイドでは、Cephのパッケージを手動でインストールする方法を説明します。この手順は、cephadm、chef、jujuなどのデプロイメントツールを使用してインストールしていないユーザのみを対象としています。

## **Installing with APT[¶](https://docs.ceph.com/en/pacific/install/install-storage-cluster/#installing-with-apt "Permalink to this headline")**

リリースパッケージまたは開発パッケージをAPTに追加したら、APTのデータベースを更新してCephをインストールします。

```
sudo apt-get update && sudo apt-get install ceph ceph-mds
```

## **Installing with RPM[¶](https://docs.ceph.com/en/pacific/install/install-storage-cluster/#installing-with-rpm "Permalink to this headline")**

RPMを使用してCephをインストールするには、以下の手順を実行します。

1.   yum\-plugin\-prioritiesをインストールします。

```
sudo yum install yum-plugin-priorities
```

2. /etc/yum/pluginconf.d/priorities.confが存在することを確認してください。

3. priorities.confでプラグインが有効になっていることを確認します。

```
[main]
enabled = 1
```

4. YUM ceph.repoエントリにpriority=2が含まれていることを確認します。詳細については、「[Get Packages](https://docs.ceph.com/en/pacific/install/get-packages)」を参照してください。

```
[ceph]
name=Ceph packages for $basearch
baseurl=https://download.ceph.com/rpm-{ceph-release}/{distro}/$basearch
enabled=1
priority=2
gpgcheck=1
gpgkey=https://download.ceph.com/keys/release.asc

[ceph-noarch]
name=Ceph noarch packages
baseurl=https://download.ceph.com/rpm-{ceph-release}/{distro}/noarch
enabled=1
priority=2
gpgcheck=1
gpgkey=https://download.ceph.com/keys/release.asc

[ceph-source]
name=Ceph source packages
baseurl=https://download.ceph.com/rpm-{ceph-release}/{distro}/SRPMS
enabled=0
priority=2
gpgcheck=1
gpgkey=https://download.ceph.com/keys/release.asc
```

5. 前提条件となるパッケージをインストールします。

```
sudo yum install snappy leveldb gdisk python-argparse gperftools-libs
```

リリースパッケージまたは開発パッケージのいずれかを追加するか、/etc/yum.repos.dにceph.repoファイルを追加したら、Cephパッケージをインストールできます。

```
sudo yum install ceph
```

## **Installing a Build[¶](https://docs.ceph.com/en/pacific/install/install-storage-cluster/#installing-a-build "Permalink to this headline")**

ソースコードからCephをビルドした場合は、以下を実行することでユーザースペースにCephをインストールできます。

```
sudo make install
```

Cephをローカルにインストールした場合、makeは実行ファイルをusr/local/binに配置します。Cephの設定ファイルをusr/local/binディレクトリに追加して、1つのディレクトリからCephを実行することができます。
