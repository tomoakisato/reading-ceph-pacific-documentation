# 258: librgw (Python)

 # librgw \(Python\)[¶](https://docs.ceph.com/en/pacific/radosgw/api/ "Permalink to this headline")

The rgw python module provides file\-like access to rgw.

## API Reference[¶](https://docs.ceph.com/en/pacific/radosgw/api/ "Permalink to this headline")

This module is a thin wrapper around rgw\_file.

_class_ `rgw.``LibRGWFS`[¶](https://docs.ceph.com/en/pacific/radosgw/api/ "Permalink to this definition")librgwfs python wrapper

`shutdown`\(\)[¶](https://docs.ceph.com/en/pacific/radosgw/api/ "Permalink to this definition")Unmount and destroy the ceph mount handle.

`version`\(\)[¶](https://docs.ceph.com/en/pacific/radosgw/api/ "Permalink to this definition")Get the version number of the `librgwfile` C library.

Returnsa tuple of `(major, minor, extra)` components of the libcephfs version
