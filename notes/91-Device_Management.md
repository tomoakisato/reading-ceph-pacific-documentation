# 91: Device Management

**クリップソース:** [91: Device Management — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/operations/devices/)

# Device Management[¶](https://docs.ceph.com/en/pacific/rados/operations/devices/ "Permalink to this headline")

Cephは、どのハードウェアストレージデバイス（HDDやSSDなど）がどのデーモンによって消費されているかを追跡し、それらのデバイスに関するヘルスメトリクスを収集して、ハードウェア障害を予測したり自動的に対応するためのツールを提供します。

## Device tracking[¶](https://docs.ceph.com/en/pacific/rados/operations/devices/ "Permalink to this headline")

どのストレージデバイスが使用中であるかを照会するには：

```
ceph device ls
```

デーモン単位、ホスト単位でデバイスを一覧表示するには：

```
ceph device ls-by-daemon <daemon>
ceph device ls-by-host <host>
```

個々のデバイスについて、その位置や消費状況などの情報を照会するには：

```
ceph device info <devid>
```

## Identifying physical devices[¶](https://docs.ceph.com/en/pacific/rados/operations/devices/ "Permalink to this headline")

ハードウェアエンクロージャーのドライブLEDを点滅させることで、故障したディスクの交換を容易にし、エラーを少なくすることができます。 次のコマンドを使用します。

```
device light on|off <devid> [ident|fault] [--force]
```

\<devid\>パラメータはデバイスの識別番号です。以下のコマンドで取得できます。

```
ceph device ls
```

\[ident|fault\]パラメータで、点滅させる光の種類を設定します。デフォルトでは、 identificationランプが使用されます。

このコマンドを実行するには、CephadmまたはRookのオーケストレータモジュールが有効である必要があります。オーケストレータモジュールが有効になっていることは、以下のコマンドを実行することで確認できます。

```
ceph orch status
```

ドライブのLEDを点滅させるコマンドは、ldmcliです。このコマンドをカスタマイズする必要がある場合は、Jinja2テンプレートで設定することができます。

```
ceph config-key set mgr/cephadm/blink_device_light_cmd "<template>"
ceph config-key set mgr/cephadm/<host>/blink_device_light_cmd "lsmcli local-disk-{{ ident_fault }}-led-{{'on' if on else 'off'}} --path '{{ path or dev }}'"
```

Jinja2テンプレートは、以下の引数を用いてレンダリングされます。

* ブーリアン値
    on
* ident または fault を含む文字列
    ident\_fault
* デバイスIDを含む文字列（例：SanDisk\_X400\_M.2\_2280\_512GB\_162924424784）
    dev
* デバイスのパスを含む文字列（例：/dev/sda）
    path

## Enabling monitoring[¶](https://docs.ceph.com/en/pacific/rados/operations/devices/ "Permalink to this headline")

Cephは、デバイスに関連するヘルスメトリクスを監視することもできます。 例えば、SATAハードディスクはSMARTという規格を実装しており、電源オン時間、電源サイクル数、回復不能な読み取りエラーなど、デバイスの使用状況と健全性に関するさまざまな内部メトリクスを提供します。 SASやNVMeなどの他のデバイス・タイプも、（若干異なる規格を経由して）同様のメトリクスを実装しています。これらはすべて、Cephがsmartctlツールで収集できます。

ヘルスモニタリングの有効・無効を設定するには：

```
ceph device monitoring on
```

or:

```
ceph device monitoring off
```

## Scraping[¶](https://docs.ceph.com/en/pacific/rados/operations/devices/ "Permalink to this headline")

モニタリングが有効な場合、一定時間ごとにメトリクスが自動的にスクレイピングされます。 その間隔は以下で設定することができます。

```
ceph config set mgr mgr/devicehealth/scrape_frequency <seconds>
```

デフォルトでは、24時間に1回スクレイピングを行います。

全デバイスのスクレイプを手動で発動するには：

```
ceph device scrape-health-metrics
```

1つのデバイスでスクレイプを発動するには：

```
ceph device scrape-health-metrics <device-id>
```

単一のデーモンのデバイスでスクレイプを発動するには：

```
ceph device scrape-daemon-health-metrics <who>
```

保存されているデバイスのヘルスメトリクスを（オプションで特定のタイムスタンプについて）取得するには：

```
ceph device get-health-metrics <devid> [sample-timestamp]
```

## Failure prediction[¶](https://docs.ceph.com/en/pacific/rados/operations/devices/ "Permalink to this headline")

Cephは、収集したヘルスメトリクスをもとに、寿命や機器の故障を予測することができます。 3つのモードがあります。

* _none: デバイスの障害予測を無効にします_
* _local: ceph\-mgrデーモンから事前学習された予測モデルを使用します_

予測モードを設定するには：

```
ceph config set global device_failure_prediction_mode <mode>
```

予測は通常バックグラウンドで定期的に実行されるため、寿命の値が入力されるまでに時間がかかる場合があります。 出力される全デバイスの寿命を確認するには：

```
ceph device ls
```

特定のデバイスのメタデータを照会するには：

```
ceph device info <devid>
```

明示的にデバイスの寿命を強制的に予測するには：

```
ceph device predict-life-expectancy <devid>
```

Cephの内部デバイス故障予測を使用せず、デバイス故障に関する何らかの外部情報源を持っている場合、以下の方法でデバイスの寿命をCephに知らせることができます。

```
ceph device set-life-expectancy <devid> <from> [<to>]
```

平均寿命は時間間隔として表現されるため、不確実性を広い間隔で表現することができます。また、区間終了を不定にすることもできます。

## Health alerts[¶](https://docs.ceph.com/en/pacific/rados/operations/devices/ "Permalink to this headline")

mgr/devicehealth/warn\_threshold は、デバイスの故障が予想される場合に、どの程度でヘルスアラートを発生させるかを制御します。

すべてのデバイスの保存された寿命を確認し、適切なヘルスアラートを生成するには：

```
ceph device check-health
```

## Automatic Mitigation[¶](https://docs.ceph.com/en/pacific/rados/operations/devices/ "Permalink to this headline")

mgr/devicehealth/self\_heal オプションが有効な場合（デフォルトでは有効）、すぐに故障すると予想されるデバイスについては、モジュールがデバイスを「out」とマークすることで自動的にデータをそれらのデバイスから移行させます。

mgr/devicehealth/mark\_out\_threshold は、デバイスの故障が予想される場合に、どの程度で osd を自動的に「out」マークするかを制御します。
