# 22: Converting an existing cluster to cephadm

**クリップソース:** [22: Converting an existing cluster to cephadm — Ceph Documentation](https://docs.ceph.com/en/pacific/cephadm/adoption/)

# Converting an existing cluster to cephadm[¶](https://docs.ceph.com/en/pacific/cephadm/adoption/#converting-an-existing-cluster-to-cephadm "Permalink to this headline")

一部の既存のクラスターを変換して、cephadmで管理できるようにすることができます。このステートメントは、ceph\-deploy、ceph\-ansible、またはDeepSeaで展開された一部のクラスタに適用されます。

このドキュメントのこのセクションでは、クラスタをcephadmで管理できる状態に変換できるかどうかを判断する方法と、それらの変換を実行する方法について説明します。

## Limitations[¶](https://docs.ceph.com/en/pacific/cephadm/adoption/#limitations "Permalink to this headline")

* CephadmはBlueStore OSDでのみ動作します。クラスタ内にあるFileStore OSDはcephadmでは管理できません。

## Preparation[¶](https://docs.ceph.com/en/pacific/cephadm/adoption/#preparation "Permalink to this headline")

既存のクラスタの各ホストでcephadmコマンドラインツールが利用可能であることを確認します。 方法については、「[Install cephadm](https://docs.ceph.com/en/pacific/cephadm/install/#get-cephadm) 」を参照してください。

以下のコマンドを実行して、cephadmが使用できるように各ホストを準備します。

```
cephadm prepare-host
```

変換に使用するCephのバージョンを選択します。この手順は、Octopus\(15.2.z\)以降のCephのリリースであれば、すべて動作します。 Cephの最新の安定版リリースがデフォルトです。この変換を実行するのと同時に、Cephの以前のリリースからアップグレードすることもできます。以前のリリースからアップグレードする場合は、そのリリースのアップグレードリリースされた指示に必ず従ってください。

次のコマンドでイメージをcephadmに渡します。

```
cephadm --image $IMAGE <rest of command goes here>
```

変換が始まります。

cephadm lsを実行し、デーモンのスタイルが変更されていることを確認して、変換が進行していることを確認します。

変換プロセスを開始する前に、cephadm lsはすべての既存のデーモンにレガシーのスタイルを表示します。変換プロセスが進むと、採用されたデーモンはcephadm:v1というスタイルで表示されるようになります。

```
cephadm ls
```

## Adoption process[¶](https://docs.ceph.com/en/pacific/cephadm/adoption/#adoption-process "Permalink to this headline")

cephの設定がクラスタ設定データベースを使用するように移行されていることを確認します。 各ホストで/etc/ceph/ceph.confが同一である場合、以下のコマンドを1つのホストで実行すると、すべてのホストに影響を与えます。

```
ceph config assimilate-conf -i /etc/ceph/ceph.conf
```

ホスト間で構成に違いがある場合は、各ホストでこのコマンドを繰り返す必要があります。このAdoptionプロセスの間、以下のコマンドを実行してクラスタの構成を表示し、完了したことを確認します。

```
ceph config dump
```

各モニターをAdopt：

```
cephadm adopt --style legacy --name mon.<hostname>
```

各レガシーモニターは停止し、cephadmコンテナとしてすぐに再起動して、クォーラムに再参加します。

各マネージャーをAdopt：

```
cephadm adopt --style legacy --name mgr.<hostname>
```

cephadmを有効にする：

```
ceph mgr module enable cephadm
ceph orch set backend cephadm
```

SSHキーを生成する：

```
ceph cephadm generate-key
ceph cephadm get-pub-key > ~/ceph.pub
```

クラスタ内の各ホストにクラスタSSHキーをインストールします。

```
ssh-copy-id -f -i ~/ceph.pub root@<host>
```

注：既存のssh鍵をインポートすることも可能です。既存のsshキーをインポートする方法については、トラブルシューティングドキュメントのsshエラーを参照してください。

Note:cephadmで非rootユーザを使用してクラスタホストにssh接続することも可能です。このユーザには、パスワードなしのsudoアクセスが必要です。cephcephadmset\-user\<user\>を使用して、そのユーザにsshキーをコピーします。[Configuring a different SSH user](https://docs.ceph.com/en/pacific/cephadm/host-management/#cephadm-ssh-user)を参照してください。

cephadmに管理するホストを伝えます：

```
ceph orch host add <hostname> [ip-address]
```

これにより、追加する前に各ホストに対してcephadm check\-hostが実行され、ホストが正常に機能していることが確認されます。IPアドレスを指定することをお勧めします。指定しない場合は、DNSでホスト名が解決されます。

Adoptされたモニターとマネージャーのデーモンが表示されていることを確認します：

```
ceph orch ps
```

クラスタ内のすべてのOSDをAdoptする：

```
cephadm adopt --style legacy --name <name>
```

For example:

```
cephadm adopt --style legacy --name osd.1
cephadm adopt --style legacy --name osd.2
```

cephadmにファイルシステムごとに実行するデーモンの数を指示して、MDSデーモンを再デプロイします。cephfslsコマンドでファイルシステムを名前でリストアップします。マスターノードで次のコマンドを実行して、MDSデーモンを再デプロイします：

```
ceph orch apply mds <fs-name> [--placement=<placement>]
```

例えば、fooという単一のファイルシステムを持つクラスタでは：

```
ceph fs ls
```

```
name: foo, metadata pool: foo_metadata, data pools: [foo_data ]
```

```
ceph orch apply mds foo 2
```

新しいMDSデーモンが起動したことを確認します：

```
ceph orch ps --daemon-type mds
```

最後に、レガシーMDSデーモンを停止して削除します：

```
systemctl stop ceph-mds.target
rm -rf /var/lib/ceph/mds/ceph-*
```

RGWデーモンの再デプロイを行います。CephadmはRGWデーモンをゾーンごとに管理しています。ゾーンごとに、cephadmで新しいRGWデーモンをデプロイします：

```
ceph orch apply rgw <svc_id> [--realm=<realm>] [--zone=<zone>] [--port=<port>] [--ssl] [--placement=<placement>]
```

ここで、\<placement\>には、単純なデーモン数、または特定のホストのリスト（[Daemon Placement](https://docs.ceph.com/en/pacific/cephadm/services/#orchestrator-cli-placement-spec)を参照）を指定し、zoneとrealmの引数は、マルチサイトのセットアップでのみ必要です。

デーモンが起動し、機能していることが確認できたら、古いレガシーデーモンを停止して削除します：

```
systemctl stop ceph-rgw.target
rm -rf /var/lib/ceph/radosgw/ceph-*
```

コマンドceph health detailの出力をチェックして、迷子のクラスタデーモンや、まだcephadmによって管理されていないホストに関するcephadmの警告を確認します。
