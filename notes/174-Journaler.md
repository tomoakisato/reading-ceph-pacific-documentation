# 174: Journaler

**クリップソース:** [174: Journaler — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/journaler/)

# Journaler[¶](https://docs.ceph.com/en/pacific/cephfs/journaler/ "Permalink to this headline")

**journaler write head interval**

Description

ジャーナルヘッドオブジェクトの更新頻度

Type

Integer

Required

No

Default

15

**journaler prefetch periods**

Description

ジャーナル再生時リードアヘッドのストライプピリオド数

Type

Integer

Required

No

Default

10

**journal prezero periods**

Description

書き込み位置の前にゼロになるストライプピリオド数

Type

Integer

Required

No

Default

10

**journaler batch interval**

Description

人為的に発生させる遅延の最大値（秒）

Type

Double

Required

No

Default

.001

**journaler batch max**

Description

フラッシュを遅延させる最大バイト数

Type

64\-bit Unsigned Integer

Required

No

Default

0
