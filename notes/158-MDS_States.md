# 158: MDS States

**クリップソース:** [158: MDS States — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/mds-states/)

# MDS States[¶](https://docs.ceph.com/en/pacific/cephfs/mds-states/ "Permalink to this headline")

メタデータサーバ\(MDS\)は、CephFSの通常動作中にいくつかの状態を遷移します。例えば、ある状態は、MDSインスタンスによる以前のフェイルオーバからリカバリ中であることを示します。ここでは、全状態を文書化し、状態遷移図で視覚化します。

## State Descriptions[¶](https://docs.ceph.com/en/pacific/cephfs/mds-states/ "Permalink to this headline")

### Common states[¶](https://docs.ceph.com/en/pacific/cephfs/mds-states/ "Permalink to this headline")

```
up:active
```

これはMDSの正常動作状態です。MDSとファイルシステム内の当該ランクが利用可能であることを示します。

```
up:standby
```

MDSは故障したランクを引き継ぐことが可能です（[用語の説明](https://docs.ceph.com/en/pacific/cephfs/standby/#mds-standby)も参照）。この状態のMDSが利用可能になると、モニタは自動的に故障したランクに割り当てます。

```
up:standby_replay
```

このMDSは、他のup:activeなMDSのジャーナルをフォローしています。activeなMDSに障害が発生した場合、replayモードでstandbyしているMDSがあれば、ライブ ジャーナルを再生しているため、より迅速に処理を引き継ぐことができます。standby\-replay MDSを持つことのデメリットは、他のMDSに障害が発生した場合、フォローしているMDS以外を引き継ぐことができない点です。

### Less common or transitory states[¶](https://docs.ceph.com/en/pacific/cephfs/mds-states/ "Permalink to this headline")

```
up:boot
```

この状態は、起動時にCephモニタにブロードキャストされます。モニタは直ちにMDSを使用可能なランクに割り当てるか、MDSをstandbyとして動作させるよう命令するため、この状態は決して表示されません。この状態は、念のためここに記録されています。

```
up:creating
```

MDSは、ランクごとのメタデータ（ジャーナルなど）を構築し、MDSクラスタに入ることで、新しいランク（おそらくランク0）を作成しています。

```
up:starting
```

MDSは停止したランクを再開中です。ランクごとのメタデータを開き、MDSクラスタに入ります。

```
up:stopping
```

ランクが停止されると、モニタはアクティブなMDSに対してup:stoppingの状態になるよう命令します。この状態では、MDSは新しいクライアント接続を受け付けず、すべてのサブツリーをファイルシステム内の他のランクに移行し、メタデータジャーナルをフラッシュし、最後のランク（0）の場合はすべてのクライアントを退去させてシャットダウンします（[CephFS管理コマンド](https://docs.ceph.com/en/pacific/cephfs/administration/#cephfs-administration)も参照）。

```
up:replay
```

MDSが故障したランクを引き継いでいます。この状態は、MDSがジャーナルやその他のメタデータを回復中であることを表しています。

```
up:resolve
```

Cephファイルシステムに複数のランク（当該ランクを含む）がある場合、つまり単一アクティブMDSクラスタでない場合、MDSはup:replayからこの状態になります。MDSはコミットされていないMDS間の操作を解決中です。ファイルシステム内のすべてのランクがこの状態かそれ以降でない（つまり、どのランクもfailed/damagedまたはup:replayでない）と進行しません。。

```
up:reconnect
```

MDSはup:replayまたはup:resolveからこの状態に遷移します。この状態は、クライアントからの再接続を促すためのものです。このランクとのセッションがあったクライアントは、mds\_reconnect\_timeoutで設定可能な時間内に再接続する必要があります。

```
up:rejoin
```

MDSはup:reconnectからこの状態に遷移します。この状態では、MDSはMDSのクラスタキャッシュに再参加しています。メタデータに対する全てのMDS間ロックは再確立されます。

再生すべき既知のクライアントリクエストがない場合、MDSはこの状態から直接up:activeになります。

```
up:clientreplay
```

MDSはup:rejoinからこの状態に遷移することがあります。MDSは、返信したがdurableではない（ジャーナルされていない）クライアントリクエストを再生中です。クライアントはup:reconnectの間にこれらのリクエストを再送し、リクエストは再度再生されます。MDSは再生完了後、up:activeに遷移します。

### Failed states[¶](https://docs.ceph.com/en/pacific/cephfs/mds-states/ "Permalink to this headline")

```
down:failed
```

実際にこの状態を保持するMDSはありません。その代わり、ファイルシステム内のランクに適用されます。例えば：

```
$ ceph fs dump
...
max_mds 1
in      0
up      {}
failed  0
...
```

ランク0はfailedセットの一部です。

```
down:damaged
```

実際にこの状態を保持するMDSはありません。その代わり、ファイルシステム内のランクに適用されます。例えば：

```
$ ceph fs dump
...
max_mds 1
in      0
up      {}
failed
damaged 0
...
```

ランク0が破損し（[ディザスターリカバリー](https://docs.ceph.com/en/pacific/cephfs/disaster-recovery/#cephfs-disaster-recovery)も参照）、damagedセットに入れられました。ランク0として動作していたMDSが、自動復旧できないメタデータの破損を発見しました。オペレータの介入が必要です。

```
down:stopped
```

実際にこの状態を保持するMDSはありません。その代わり、ファイルシステム内のランクに適用されます。例えば：

```
$ ceph fs dump
...
max_mds 1
in      0
up      {}
failed
damaged
stopped 1
...
```

当該ランクはmax\_mdsを減らすことで停止しました（「[複数のアクティブなデータシート・デーモンを構成する](https://docs.ceph.com/en/pacific/cephfs/multimds/#cephfs-multimds)」も参照してください）。

## State Diagram[¶](https://docs.ceph.com/en/pacific/cephfs/mds-states/ "Permalink to this headline")

この状態図は、MDS/ランクで起こりうる状態遷移を示したものです。凡例は以下の通りです：

### Color[¶](https://docs.ceph.com/en/pacific/cephfs/mds-states/ "Permalink to this headline")

* Green: MDSはactive
* Orange: MDSはactiveになろうとしている過渡的な状態
* Red: MDSは、ランクがfailedとマークされる状態
* Purple: MDSとランクが停止中
* Black: MDSは、ランクがdamagedとマークされる状態

### Shape[¶](https://docs.ceph.com/en/pacific/cephfs/mds-states/ "Permalink to this headline")

* Circle: MDSがこの状態を保持している
* Hexagon: MDSは、この状態を保持しない（ランクに適用される）

### Lines[¶](https://docs.ceph.com/en/pacific/cephfs/mds-states/ "Permalink to this headline")

* 二重線の形状は、ランクが「in」であることを示す

![mds-state-diagram.png](image/mds-state-diagram.png)
