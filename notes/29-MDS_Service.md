# 29: MDS Service

**クリップソース:** [29: MDS Service — Ceph Documentation](https://docs.ceph.com/en/pacific/cephadm/services/mds/)

# MDS Service[¶](https://docs.ceph.com/en/pacific/cephadm/services/mds/ "Permalink to this headline")

## Deploy CephFS[¶](https://docs.ceph.com/en/pacific/cephadm/services/mds/ "Permalink to this headline")

CephFSファイルシステムを使用するには、1つまたは複数のMDSデーモンが必要です。

これらは、より新しいceph fs volumeインタフェースを使用して新しいファイルシステムを作成すると、自動的に作成されます。詳細については、「[FS volumes and subvolumes](https://docs.ceph.com/en/pacific/cephadm/services/mds/)」を参照してください。 [140: FS volumes and subvolumes](140-FS_volumes_and_subvolumes.md)

For example:

```
# ceph fs volume create <fs_name> --placement="<placement spec>"
```

ここで、fs\_nameはCephFSの名前、placementは [Daemon Placement](https://docs.ceph.com/en/pacific/cephadm/services/mds/)です。

MDSデーモンを手動で配置する場合は、本仕様書をご利用ください。

```
service_type: mds
service_id: fs_name
placement:
  count: 3
```

この仕様は、以下の方法で適用できます。

```
# ceph orch apply -i mds.yaml
```

 CLIでMDSデーモンを手動で展開する場合は、「[Stateless services \(MDS/RGW/NFS/rbd\-mirror/iSCSI\)](https://docs.ceph.com/en/pacific/cephadm/services/mds/)」を参照してください。[310: Orchestrator CLI](310-Orchestrator_CLI.md)

## Further Reading[¶](https://docs.ceph.com/en/pacific/cephadm/services/mds/ "Permalink to this headline")

* [Ceph File System](https://docs.ceph.com/en/pacific/cephadm/services/mds/)
