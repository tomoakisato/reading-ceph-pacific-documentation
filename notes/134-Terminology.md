# 134: Terminology

**クリップソース:** [134: Terminology — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/standby/)

**[Report a Documentation Bug](https://docs.ceph.com/en/pacific/cephfs/standby/)**

# Terminology[¶](https://docs.ceph.com/en/pacific/cephfs/standby/ "Permalink to this headline")

Cephクラスタは、0個以上のCephFSファイルシステムを持つことができます。 CephFSファイルシステムは、人間が読める名前\(fsnewで設定\)と整数のIDを持ちます。 このIDはファイルシステムクラスタID、またはFSCIDと呼ばれます。

各CephFSファイルシステムにはランクがあり、デフォルトでは1つで、0から始まります。 ランクは、メタデータシャードと考えることができます。ファイルシステムのランク数の制御については、「[複数のアクティブなMDSデーモンを設定する](https://docs.ceph.com/en/pacific/cephfs/multimds/)」で説明しています。

各CephFS ceph\-mdsプロセス\(デーモン\)は、最初はランクなしで起動されます。 モニタクラスタによってランクが割り当てられることがあります。デーモンは一度に1つのランクしか保持できません。 デーモンがランクを放棄するのは、ceph\-mdsプロセスが停止したときだけです。

ランクがデーモンと関連付けられていない場合、ランクはfailedとみなされます。 ランクがデーモンに割り当てられると、そのランクはupと見なされます。

デーモンは、最初に設定するときに管理者によって静的に設定される名前を持ちます。 一般的な構成では、デーモンが実行されるホスト名をデーモン名として使用します。

mds\_join\_fs設定オプションにファイルシステム名を設定することで、ceph\-mdsのデーモンを特定のファイルシステムに割り当てることができます。

デーモンが起動するたびに、デーモンのプロセス寿命に固有のGIDも割り当てられます。 GIDは整数です。

# Referring to MDS daemons[¶](https://docs.ceph.com/en/pacific/cephfs/standby/ "Permalink to this headline")

MDSデーモンを参照する管理コマンドのほとんどは、ランク、GID、名前を含む柔軟な引数形式を受け付けます。

ランクを使用する場合、オプションでファイルシステム名またはIDで修飾することができます。 デーモンがスタンバイの（つまり、現在ランクが割り当てられていない）場合、GIDまたは名前によってのみ参照されます。

例えば、「myhost」という名前のMDSデーモンがあり、GIDが5446で、FSCIDが3のファイルシステム「mysf」においてランク0が割り当てられた場合、以下のいずれかが「fail」コマンドの適切な形式となります：

```
ceph mds fail 5446     # GID
ceph mds fail myhost   # Daemon name
ceph mds fail 0        # Unqualified rank
ceph mds fail 3:0      # FSCID and rank
ceph mds fail myfs:0   # File System name and rank
```

# Managing failover[¶](https://docs.ceph.com/en/pacific/cephfs/standby/ "Permalink to this headline")

MDSデーモンがモニタとの通信を停止した場合、モニタはmds\_beacon\_grace秒（デフォルト15秒）待ってからデーモンをlaggyとしてマークします。スタンバイが利用可能な場合、モニタは直ちにlaggyデーモンを置き換えます。

各ファイルシステムは、健全とみなされるスタンバイデーモンの数を指定することができます。この数には、ランクの障害を待機しているstandby\-replayのデーモンが含まれます\(standby\-replayのデーモンは、他のランクの障害や他のCephFSファイルシステムの障害を引き継ぐために割り当てられないことを覚えておいてください\)。replay中でないスタンバイデーモンのプールは、どのファイルシステムのカウントにも含まれます。各ファイルシステムは、使用したいスタンバイデーモンの数を設定できます。

```
ceph fs set <fs name> standby_count_wanted <count>
```

countを0にするとヘルスチェックが無効になります。

# Configuring standby\-replay[¶](https://docs.ceph.com/en/pacific/cephfs/standby/ "Permalink to this headline")

各CephFSファイルシステムは、standby\-replayデーモンを追加するように設定することができます。 これらのスタンバイデーモンはアクティブなMDSのメタデータジャーナルをフォローし、アクティブなMDSが使用できなくなった場合のフェイルオーバ時間を短縮します。各アクティブなMDSには、1つのstandby\-replayデーモンしかフォローさせることができま せん。

ファイルシステムでのstandby\-replayの設定は、以下を使用して行います：

```
ceph fs set <fs name> allow_standby_replay <bool>
```

一度設定すると、モニタは利用可能なスタンバイデーモンをそのファイルシステム内のアクティブなMDSをフォローするように割り当てます。

MDSがstandby\-replay状態になると、そのMDSがフォローしているランクのスタンバイとしてのみ使用されます。他のランクに障害が発生した場合、このstandby\-replayデーモンは、他にスタンバイがない場合でも、代替とし て使用されることはありません。このため、standby\-replayを使用する場合は、すべてのアクティブなMDSにstandby\-replayデーモン を配置することをお勧めします。

# Configuring MDS file system affinity[¶](https://docs.ceph.com/en/pacific/cephfs/standby/ "Permalink to this headline")

特定のファイルシステムに使用するMDSを用意したい場合があります。あるいは、より良いハードウェア上に大きなMDSがあり、少ないハードウェアやオーバープロビジョニングされたハードウェア上のlast\-resortスタンバイよりも優先されるべきかもしれません。このような優先順位を表現するために、CephFSにはmds\_join\_fsというMDSの構成オプションがあり、このアフィニティが強制的に適用されます。

フェイルオーバーの一環として、Cephモニタは、mds\_join\_fsが故障したランクのファイルシステム名と同じであるスタンバイデーモンを優先します。 mds\_join\_fsがファイルシステム名と等しいスタンバイが存在しない場合、最後の手段として、バニラスタンバイ（mds\_join\_fsの設定なし）または他の利用可能なスタンバイを代替に選択します。これは、standby\-replayデーモンが他のスタンバイを調べる前に常に選択されるという動作を変更するものではないことに注意してください。

さらに、モニタは安定時にCephFSファイルシステムを定期的に調査し、アフィニティの低いMDSを置き換えるために、よりアフィニティの高いスタンバイが利用可能かどうかをチェックします。この処理はstandby\-replayデーモンに対しても行われ、通常のスタンバイがstandby\-replayのMDSよりも強いアフィニティを持っている場合、standby\-replayのMDSを置き換えます。

例えば、安定した健全なファイルシステムがあるとします：

```
$ ceph fs dump
dumped fsmap epoch 399
...
Filesystem 'cephfs' (27)
...
e399
max_mds 1
in      0
up      {0=20384}
failed
damaged
stopped
...
[mds.a{0:20384} state up:active seq 239 addr [v2:127.0.0.1:6854/966242805,v1:127.0.0.1:6855/966242805]]

Standby daemons:

[mds.b{-1:10420} state up:standby seq 2 addr [v2:127.0.0.1:6856/2745199145,v1:127.0.0.1:6857/2745199145]]
```

スタンバイ側でmds\_join\_fsを設定し、設定を強制することができます：

```
$ ceph config set mds.b mds_join_fs cephfs
```

自動フェイルオーバー後：

```
$ ceph fs dump
dumped fsmap epoch 405
e405
...
Filesystem 'cephfs' (27)
...
max_mds 1
in      0
up      {0=10420}
failed
damaged
stopped
...
[mds.b{0:10420} state up:active seq 274 join_fscid=27 addr [v2:127.0.0.1:6856/2745199145,v1:127.0.0.1:6857/2745199145]]

Standby daemons:

[mds.a{-1:10720} state up:standby seq 2 addr [v2:127.0.0.1:6854/1340357658,v1:127.0.0.1:6855/1340357658]]
```

上記の例では、mds.bがjoin\_fscid=27になったことに注意してください。この出力では、mds\_join\_fsのファイルシステム名がファイルシステム識別子\(27\)に変更されています。ファイルシステムが同じ名前で再作成された場合、スタンバイは期待どおり新しいファイルシステムに従います。

最後に、ファイルシステムがデグレードしているかサイズが小さい場合、mds\_join\_fsを実施するためのフェイルオーバーは発生しません。
