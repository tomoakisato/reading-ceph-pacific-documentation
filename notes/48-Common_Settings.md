# 48: Common Settings

**クリップソース:** [48: Common Settings — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/configuration/common/)

**[Report a Documentation Bug](https://docs.ceph.com/en/pacific/rados/configuration/common/)**

# Common Settings[¶](https://docs.ceph.com/en/pacific/rados/configuration/common/ "Permalink to this headline")

[Hardware Recommendations](https://docs.ceph.com/en/pacific/rados/configuration/common/) では、Ceph Storage Clusterを構成する際のハードウェアのガイドラインを説明します。1つのCephノードで複数のデーモンを実行することができます。たとえば、複数のドライブを持つ1つのノードでは、ドライブごとに1つのceph\-osdを実行できます。理想的には、特定の種類のプロセスのためにノードを用意します。たとえば、あるノードはceph\-osdデーモンを実行し、他のノードはceph\-mdsデーモンを実行し、さらに他のノードはceph\-monデーモンを実行することができます。

各ノードには、host設定で識別される名前があります。また、モニターは、ネットワークアドレスとポート（ドメイン名またはIPアドレス）をaddr設定で指定します。 基本的な設定ファイルでは、モニターデーモンの各インスタンスに対して最小限の設定のみを指定するのが一般的です。例えば、以下のようになります。

```
[global]
mon_initial_members = ceph1
mon_host = 10.0.0.1
```

重要：ホスト設定は、ノードのショートネーム（fqdnではありません）です。また、IPアドレスでもありません。 コマンドラインでhostname\-sと入力すると、ノードの名前が表示されます。Cephを手動で展開している場合を除き、初期モニタ以外にホスト設定を使用しないでください。chefやcephadmなどの展開ツールを使用する場合は、個々のデーモンにhostを指定してはいけません。

# Networks[¶](https://docs.ceph.com/en/pacific/rados/configuration/common/ "Permalink to this headline")

Cephで使用するためのネットワークの設定についての詳細は、「[Network Configuration Reference](https://docs.ceph.com/en/pacific/rados/configuration/common/) 」を参照してください。

# Monitors[¶](https://docs.ceph.com/en/pacific/rados/configuration/common/ "Permalink to this headline")

本番のCephクラスターでは通常、最低3つのCeph Monitorデーモンをプロビジョニングして、モニターインスタンスがクラッシュした場合の可用性を確保します。最低3つあれば、Paxosアルゴリズムが、定足数内の過半数のCeph MonitorからどのバージョンのCeph Cluster Mapが最新かを判断できます。

注:Cephを1つのモニタで展開することもできますが、インスタンスに障害が発生した場合、他のモニタがないとデータサービスの可用性が妨げられる可能性があります。

Cephモニターは通常、新しいv2プロトコルではポート3300で、古いv1プロトコルでは6789で待ち受けます。

デフォルトでは、Cephはモニタデータを次のパスに格納することを想定しています。

```
/var/lib/ceph/mon/$cluster-$id
```

お客様またはデプロイメントツール\(cephadmなど\)が対応するディレクトリを作成する必要があります。メタ変数が完全に表現され、クラスタの名前が「ceph」の場合、前述のディレクトリは次のように評価されます。

```
/var/lib/ceph/mon/ceph-a
```

詳細は「 [Monitor Config Reference](https://docs.ceph.com/en/pacific/rados/configuration/common/)」を参照してください。

# Authentication[¶](https://docs.ceph.com/en/pacific/rados/configuration/common/ "Permalink to this headline")

バージョンBobtailの新機能。0.56

Bobtail\(v 0.56\)以降では、Ceph設定ファイルの\[global\]セクションで認証を明示的に有効または無効にする必要があります。

```
auth_cluster_required = cephx
auth_service_required = cephx
auth_client_required = cephx
```

さらに、メッセージ署名を有効にする必要があります。詳細は「[Cephx Config Reference](https://docs.ceph.com/en/pacific/rados/configuration/common/)」を参照してください。 

# OSDs[¶](https://docs.ceph.com/en/pacific/rados/configuration/common/ "Permalink to this headline")

Cephの本番クラスタでは通常、Ceph OSDデーモンが配備され、1つのノードで1つのOSDデーモンが1つのストレージデバイス上でFilestoreを実行します。現在はBlueStoreバックエンドがデフォルトですが、Filestoreを使用する場合はジャーナルサイズを指定します。例えば、以下のようになります。

```
[osd]
osd_journal_size = 10000

[osd.0]
host = {hostname} #manual deployments only.
```

デフォルトでは、CephはCeph OSD Daemonのデータを次のパスに保存することを想定しています。

```
/var/lib/ceph/osd/$cluster-$id
```

お客様または展開ツール\(cephadmなど\)が対応するディレクトリを作成する必要があります。メタ変数が完全に表現され、クラスタの名前が「ceph」の場合、この例は次のように評価されます。

```
/var/lib/ceph/osd/ceph-0
```

osd\_dataの設定でこのパスを上書きすることができます。デフォルトの場所を変更しないことをお勧めします。OSDホスト上にデフォルトのディレクトリを作成してください。

```
$ ssh {osd-host}
$ sudo mkdir /var/lib/ceph/osd/ceph-{osd-number}
```

osd\_dataパスは、OSやデーモンが入っているデバイスとは別のデバイスのマウントポイントにつながるのが理想的です。OSDがOSデバイス以外のデバイスを使用する場合は、Cephで使用するための準備をして、先ほど作成したディレクトリにマウントしてください

```
$ ssh {new-osd-host}
$ sudo mkfs -t {fstype} /dev/{disk}
$ sudo mount -o user_xattr /dev/{hdd} /var/lib/ceph/osd/ceph-{osd-number}
```

mkfsを実行する際には、xfsファイルシステムを使用することをお勧めします。 \(btrfsおよびext4は推奨されておらず、テストも終了しています\)。

設定の詳細については、「OSD Config Reference」をご覧ください。

# Heartbeats[¶](https://docs.ceph.com/en/pacific/rados/configuration/common/ "Permalink to this headline")

実行時の操作中に、Ceph OSDデーモンは他のCeph OSDデーモンをチェックして、その結果をCeph Monitorに報告します。何らかの設定を行う必要はありません。ただし、ネットワークのレイテンシの問題がある場合は、設定を変更するとよいでしょう。

その他の詳細については、「[Configuring Monitor/OSD Interaction](https://docs.ceph.com/en/pacific/rados/configuration/common/)」を参照してください。

# Logs / Debugging[¶](https://docs.ceph.com/en/pacific/rados/configuration/common/ "Permalink to this headline")

時には、ログ出力の修正やCephのデバッグの使用が必要なCephの問題に遭遇することがあります。ログローテーションの詳細については、「デバッグとロギング」を参照してください。

# Example ceph.conf[¶](https://docs.ceph.com/en/pacific/rados/configuration/common/ "Permalink to this headline")

```
[global]
fsid = {cluster-id}
mon_initial_ members = {hostname}[, {hostname}]
mon_host = {ip-address}[, {ip-address}]

#All clusters have a front-side public network.
#If you have two network interfaces, you can configure a private / cluster 
#network for RADOS object replication, heartbeats, backfill,
#recovery, etc.
public_network = {network}[, {network}]
#cluster_network = {network}[, {network}] 

#Clusters require authentication by default.
auth_cluster_required = cephx
auth_service_required = cephx
auth_client_required = cephx

#Choose reasonable numbers for journals, number of replicas
#and placement groups.
osd_journal_size = {n}
osd_pool_default_size = {n}  # Write an object n times.
osd_pool_default_min size = {n} # Allow writing n copy in a degraded state.
osd_pool_default_pg num = {n}
osd_pool_default_pgp num = {n}

#Choose a reasonable crush leaf type.
#0 for a 1-node cluster.
#1 for a multi node cluster in a single rack
#2 for a multi node, multi chassis cluster with multiple hosts in a chassis
#3 for a multi node cluster with hosts across racks, etc.
osd_crush_chooseleaf_type = {n}

```

# Running Multiple Clusters \(DEPRECATED\)[¶](https://docs.ceph.com/en/pacific/rados/configuration/common/ "Permalink to this headline")

各Cephクラスタには、設定ファイル名、ログファイル名、ディレクトリ名、マウントポイント名の一部として使用される内部名があります。 この名前はデフォルトでは「ceph」です。 Cephの以前のリリースでは、代わりに「ceph2」などのカスタム名を指定することができました。 これは、同じ物理ハードウェア上で複数の論理クラスタを実行できるようにするためのものですが、実際にはほとんど利用されていないため、今後は利用しないようにしてください。 また、以前のドキュメントでは、rbd\-mirrorを使用するために固有のクラスタ名が必要であると誤解されていました。

カスタムクラスター名は現在非推奨とされており、一部のツールではすでに配置する機能が削除されていますが、既存のカスタム名による配置は引き続き動作します。 カスタム名でクラスタを実行および管理する機能は、今後のCephのリリースで徐々に削除される可能性があるため、すべての新しいクラスタをデフォルト名の「ceph」で展開することを強くお勧めします。

一部のCeph CLIコマンドには、オプションで\-\-cluster \(クラスタ名\)オプションがあります。このオプションは、純粋に後方互換性のために存在するもので、新しいツールやデプロイメントで対応する必要はありません。

同一ホスト上に複数のクラスタを存在させる必要がある場合は、コンテナを使用して各クラスタを完全に分離するCephadmを使用してください。
