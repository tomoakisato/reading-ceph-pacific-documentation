# 132: Multiple Ceph File Systems

**クリップソース:** [132: Multiple Ceph File Systems — Ceph Documentation](https://docs.ceph.com/en/pacific/cephfs/multifs/)

# Multiple Ceph File Systems[¶](https://docs.ceph.com/en/pacific/cephfs/multifs/ "Permalink to this headline")

Pacificリリースから、マルチファイルシステムのサポートが安定し、すぐに使用できるようになりました。この機能により、完全なデータ分離をした別々のファイルシステムを別々のプールで構成することができます。

既存のクラスタでは、複数のファイルシステムを有効にするフラグを設定する必要があります。

```
ceph fs flag set enable_multiple true
```

新しいCephクラスタでは、自動的にこれが設定されます。

## Creating a new Ceph File System[¶](https://docs.ceph.com/en/pacific/cephfs/multifs/ "Permalink to this headline")

新しいボリューム・プラグイン・インターフェース（参照：[FSボリュームとサブボリュー�](https://docs.ceph.com/en/pacific/cephfs/fs-volumes/)�）は、新しいファイルシステムを設定する作業のほとんどを自動化します。ボリューム" のコンセプトは、単に新しいファイルシステムです。以下を介して行うことができます：

```
ceph fs volume create <fs_name>
```

Cephは新しいプールを作成し、新しいファイルシステムをサポートするために新しいMDSのデプロイメントを自動化します。cephadmなどを使用すると、新しいファイルシステムを操作するために、新しいMDSデーモンのMDSアフィニティ（「[MDSファイルシステムのアフィニティの設定](https://docs.ceph.com/en/pacific/cephfs/standby/#mds-join-fs)」を参照）も構成されます。

## Securing access[¶](https://docs.ceph.com/en/pacific/cephfs/multifs/ "Permalink to this headline")

fs authorizeコマンドは、特定のファイルシステムに対するクライアントのアクセス権を設定することができます。「[ファイルシステム情報の制限](https://docs.ceph.com/en/pacific/cephfs/client-auth/#fs-authorize-multifs)」の項も参照してください。クライアントは認証されたファイルシステムのみを見ることができ、Monitor/MDSは認証されていないクライアントからのアクセスを拒否します。

## Other Notes[¶](https://docs.ceph.com/en/pacific/cephfs/multifs/ "Permalink to this headline")

複数のファイルシステムでプールを共有しません。これはスナップショットで特に重要ですが、重複するinodeを防ぐための対策がなされていないためでもあります。Cephコマンドは、この危険な構成を防止します。

各ファイルシステムには、それぞれ独自のMDSのランクが設定されています。その結果、新しいファイルシステムごとに、より多くのMDSデーモンが必要となり、運用コストが増加します。これは、アプリケーションやユーザーベースによるメタデータのスループットを向上させるためには有効ですが、ファイルシステムの作成にかかるコストも増加します。一般的に、アプリケーション間の負荷を分離するためには、サブツリーのピン留めによる単一ファイルシステムの方が適しています。
