# 124: Librados (C)

 # Librados \(C\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this headline")

librados provides low\-level access to the RADOS service. For an overview of RADOS, see [Architecture](https://docs.ceph.com/en/pacific/rados/api/librados/).

## Example: connecting and writing an object[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this headline")

To use Librados, you instantiate a [`rados_t`](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") variable \(a cluster handle\) and call [`rados_create()`](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_create") with a pointer to it:

```
int err;
rados_t cluster;

err = rados_create(&cluster, NULL);
if (err < 0) {
        fprintf(stderr, "%s: cannot create a cluster handle: %sn", argv[0], strerror(-err));
        exit(1);
}
```

Then you configure your [`rados_t`](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") to connect to your cluster, either by setting individual values \([`rados_conf_set()`](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_conf_set")\), using a configuration file \([`rados_conf_read_file()`](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_conf_read_file")\), using command line options \([`rados_conf_parse_argv()`](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_conf_parse_argv")\), or an environment variable \([`rados_conf_parse_env()`](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_conf_parse_env")\):

```
err = rados_conf_read_file(cluster, "/path/to/myceph.conf");
if (err < 0) {
        fprintf(stderr, "%s: cannot read config file: %sn", argv[0], strerror(-err));
        exit(1);
}
```

Once the cluster handle is configured, you can connect to the cluster with [`rados_connect()`](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_connect"):

```
err = rados_connect(cluster);
if (err < 0) {
        fprintf(stderr, "%s: cannot connect to cluster: %sn", argv[0], strerror(-err));
        exit(1);
}
```

Then you open an “IO context”, a [`rados_ioctx_t`](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t"), with [`rados_ioctx_create()`](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_create"):

```
rados_ioctx_t io;
char *poolname = "mypool";

err = rados_ioctx_create(cluster, poolname, &io);
if (err < 0) {
        fprintf(stderr, "%s: cannot open rados pool %s: %sn", argv[0], poolname, strerror(-err));
        rados_shutdown(cluster);
        exit(1);
}
```

Note that the pool you try to access must exist.

Then you can use the RADOS data manipulation functions, for example write into an object called `greeting` with[`rados_write_full()`](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_write_full"):

```
err = rados_write_full(io, "greeting", "hello", 5);
if (err < 0) {
        fprintf(stderr, "%s: cannot write pool %s: %sn", argv[0], poolname, strerror(-err));
        rados_ioctx_destroy(io);
        rados_shutdown(cluster);
        exit(1);
}
```

In the end, you will want to close your IO context and connection to RADOS with [`rados_ioctx_destroy()`](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_destroy") and [`rados_shutdown()`](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_shutdown"):

```
rados_ioctx_destroy(io);
rados_shutdown(cluster);
```

## Asynchronous IO[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this headline")

When doing lots of IO, you often don’t need to wait for one operation to complete before starting the next one. Librados provides asynchronous versions of several operations:

* [`rados_aio_write()`](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_aio_write")
* [`rados_aio_append()`](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_aio_append")
* [`rados_aio_write_full()`](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_aio_write_full")
* [`rados_aio_read()`](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_aio_read")

For each operation, you must first create a[`rados_completion_t`](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") that represents what to do when the operation is safe or complete by calling[`rados_aio_create_completion()`](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_aio_create_completion"). If you don’t need anything special to happen, you can pass NULL:

```
rados_completion_t comp;
err = rados_aio_create_completion(NULL, NULL, NULL, &comp);
if (err < 0) {
        fprintf(stderr, "%s: could not create aio completion: %sn", argv[0], strerror(-err));
        rados_ioctx_destroy(io);
        rados_shutdown(cluster);
        exit(1);
}
```

Now you can call any of the aio operations, and wait for it to be in memory or on disk on all replicas:

```
err = rados_aio_write(io, "foo", comp, "bar", 3, 0);
if (err < 0) {
        fprintf(stderr, "%s: could not schedule aio write: %sn", argv[0], strerror(-err));
        rados_aio_release(comp);
        rados_ioctx_destroy(io);
        rados_shutdown(cluster);
        exit(1);
}
rados_aio_wait_for_complete(comp); // in memory
rados_aio_wait_for_safe(comp); // on disk
```

Finally, we need to free the memory used by the completion with [`rados_aio_release()`](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_aio_release"):

```
rados_aio_release(comp);
```

You can use the callbacks to tell your application when writes are durable, or when read buffers are full. For example, if you wanted to measure the latency of each operation when appending to several objects, you could schedule several writes and store the ack and commit time in the corresponding callback, then wait for all of them to complete using [`rados_aio_flush()`](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_aio_flush") before analyzing the latencies:

```
typedef struct {
        struct timeval start;
        struct timeval ack_end;
        struct timeval commit_end;
} req_duration;

void ack_callback(rados_completion_t comp, void *arg) {
        req_duration *dur = (req_duration *) arg;
        gettimeofday(&dur->ack_end, NULL);
}

void commit_callback(rados_completion_t comp, void *arg) {
        req_duration *dur = (req_duration *) arg;
        gettimeofday(&dur->commit_end, NULL);
}

int output_append_latency(rados_ioctx_t io, const char *data, size_t len, size_t num_writes) {
        req_duration times[num_writes];
        rados_completion_t comps[num_writes];
        for (size_t i = 0; i < num_writes; ++i) {
                gettimeofday(&times[i].start, NULL);
                int err = rados_aio_create_completion((void*) &times[i], ack_callback, commit_callback, &comps[i]);
                if (err < 0) {
                        fprintf(stderr, "Error creating rados completion: %sn", strerror(-err));
                        return err;
                }
                char obj_name[100];
                snprintf(obj_name, sizeof(obj_name), "foo%ld", (unsigned long)i);
                err = rados_aio_append(io, obj_name, comps[i], data, len);
                if (err < 0) {
                        fprintf(stderr, "Error from rados_aio_append: %s", strerror(-err));
                        return err;
                }
        }
        // wait until all requests finish *and* the callbacks complete
        rados_aio_flush(io);
        // the latencies can now be analyzed
        printf("Request # | Ack latency (s) | Commit latency (s)n");
        for (size_t i = 0; i < num_writes; ++i) {
                // don't forget to free the completions
                rados_aio_release(comps[i]);
                struct timeval ack_lat, commit_lat;
                timersub(&times[i].ack_end, &times[i].start, &ack_lat);
                timersub(&times[i].commit_end, &times[i].start, &commit_lat);
                printf("%9ld | %8ld.%06ld | %10ld.%06ldn", (unsigned long) i, ack_lat.tv_sec, ack_lat.tv_usec, commit_lat.tv_sec, commit_lat.tv_usec);
        }
        return 0;
}
```

Note that all the [`rados_completion_t`](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") must be freed with [`rados_aio_release()`](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_aio_release") to avoid leaking memory.

## API calls[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this headline")

> Defines
> 
> `LIBRADOS_ALL_NSPACES`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Pass as nspace argument to rados\_ioctx\_set\_namespace\(\) before calling rados\_nobjects\_list\_open\(\) to return all objects in all namespaces.
> 
> 
> _struct_ `obj_watch_t`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _\#include \<rados\_types.h\>_One item from list\_watchers
> 
> Public Members
> 
> char `addr`\[256\][¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Address of the Watcher.
> 
> int64\_t `watcher_id`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Watcher ID.
> 
> uint64\_t `cookie`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Cookie.
> 
> uint32\_t `timeout_seconds`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Timeout in Seconds.
> 
> 
> _struct_ `notify_ack_t`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Public Members
> 
> uint64\_t `notifier_id`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> uint64\_t `cookie`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> char \*`payload`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> uint64\_t `payload_len`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> 
> _struct_ `notify_timeout_t`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Public Members
> 
> uint64\_t `notifier_id`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> uint64\_t `cookie`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> 
> xattr comparison operations
> 
> Operators for comparing xattrs on objects, and aborting the rados\_read\_op or rados\_write\_op transaction if the comparison fails.
> 
> _enum_ **\[anonymous\]**[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _Values:_
> 
> _enumerator_ `LIBRADOS_CMPXATTR_OP_EQ`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_CMPXATTR_OP_NE`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_CMPXATTR_OP_GT`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_CMPXATTR_OP_GTE`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_CMPXATTR_OP_LT`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_CMPXATTR_OP_LTE`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> 
> Operation Flags
> 
> Flags for rados\_read\_op\_operate\(\), rados\_write\_op\_operate\(\), rados\_aio\_read\_op\_operate\(\), and rados\_aio\_write\_op\_operate\(\). See librados.hpp for details.
> 
> _enum_ **\[anonymous\]**[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _Values:_
> 
> _enumerator_ `LIBRADOS_OPERATION_NOFLAG`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_OPERATION_BALANCE_READS`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_OPERATION_LOCALIZE_READS`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_OPERATION_ORDER_READS_WRITES`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_OPERATION_IGNORE_CACHE`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_OPERATION_SKIPRWLOCKS`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_OPERATION_IGNORE_OVERLAY`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_OPERATION_FULL_TRY`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_OPERATION_FULL_FORCE`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_OPERATION_IGNORE_REDIRECT`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_OPERATION_ORDERSNAP`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_OPERATION_RETURNVEC`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> 
> Alloc hint flags
> 
> Flags for rados\_write\_op\_alloc\_hint2\(\) and rados\_set\_alloc\_hint2\(\) indicating future IO patterns.
> 
> _enum_ **\[anonymous\]**[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _Values:_
> 
> _enumerator_ `LIBRADOS_ALLOC_HINT_FLAG_SEQUENTIAL_WRITE`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_ALLOC_HINT_FLAG_RANDOM_WRITE`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_ALLOC_HINT_FLAG_SEQUENTIAL_READ`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_ALLOC_HINT_FLAG_RANDOM_READ`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_ALLOC_HINT_FLAG_APPEND_ONLY`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_ALLOC_HINT_FLAG_IMMUTABLE`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_ALLOC_HINT_FLAG_SHORTLIVED`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_ALLOC_HINT_FLAG_LONGLIVED`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_ALLOC_HINT_FLAG_COMPRESSIBLE`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_ALLOC_HINT_FLAG_INCOMPRESSIBLE`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> 
> Asynchronous I/O
> 
> Read and write to objects without blocking.
> 
> _typedef_ void \(\*`rados_callback_t`\)\([rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") cb, void \*arg\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Callbacks for asynchrous operations take two parameters:
> 
> 
> 
> * cb the completion that has finished
> * arg application defined data made available to the callback function
> 
> 
> int `rados_aio_create_completion`\(void \*_cb\_arg_, [rados\_callback\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_callback_t") _cb\_complete_, [rados\_callback\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_callback_t") _cb\_safe_, [rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") \*_pc_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Constructs a completion to use with asynchronous operations
> 
> The complete and safe callbacks correspond to operations being acked and committed, respectively. The callbacks are called in order of receipt, so the safe callback may be triggered before the complete callback, and vice versa. This is affected by journalling on the OSDs.
> 
> TODO: more complete documentation of this elsewhere \(in the RADOS docs?\)
> 
> Note:
> 
> Read operations only get a complete callback.
> 
> 
> Note:
> 
> BUG: this should check for ENOMEM instead of throwing an exception
> 
> 
> Parameters
> * **cb\_arg** – application\-defined data passed to the callback functions
> * **cb\_complete** – the function to be called when the operation is in memory on all replicas
> * **cb\_safe** – the function to be called when the operation is on stable storage on all replicas
> * **pc** – where to store the completion
> Returns0
> 
> int `rados_aio_create_completion2`\(void \*_cb\_arg_, [rados\_callback\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_callback_t") _cb\_complete_, [rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") \*_pc_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Constructs a completion to use with asynchronous operations
> 
> The complete callback corresponds to operation being acked.
> 
> Note:
> 
> BUG: this should check for ENOMEM instead of throwing an exception
> 
> 
> Parameters
> * **cb\_arg** – application\-defined data passed to the callback functions
> * **cb\_complete** – the function to be called when the operation is committed on all replicas
> * **pc** – where to store the completion
> Returns0
> 
> int `rados_aio_wait_for_complete`\([rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _c_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Block until an operation completes
> 
> This means it is in memory on all replicas.
> 
> Note:
> 
> BUG: this should be void
> 
> 
> Parameters
> * **c** – operation to wait for
> Returns0
> 
> int `rados_aio_wait_for_safe`\([rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _c_\) \_\_attribute\_\_\(\(deprecated\)\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Block until an operation is safe
> 
> This means it is on stable storage on all replicas.
> 
> Note:
> 
> BUG: this should be void
> 
> 
> Parameters
> * **c** – operation to wait for
> Returns0
> 
> int `rados_aio_is_complete`\([rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _c_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Has an asynchronous operation completed?
> 
> Warning:
> 
> This does not imply that the complete callback has finished
> 
> 
> Parameters
> * **c** – async operation to inspect
> Returnswhether c is complete
> 
> int `rados_aio_is_safe`\([rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _c_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Is an asynchronous operation safe?
> 
> Warning:
> 
> This does not imply that the safe callback has finished
> 
> 
> Parameters
> * **c** – async operation to inspect
> Returnswhether c is safe
> 
> int `rados_aio_wait_for_complete_and_cb`\([rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _c_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Block until an operation completes and callback completes
> 
> This means it is in memory on all replicas and can be read.
> 
> Note:
> 
> BUG: this should be void
> 
> 
> Parameters
> * **c** – operation to wait for
> Returns0
> 
> int `rados_aio_wait_for_safe_and_cb`\([rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _c_\) \_\_attribute\_\_\(\(deprecated\)\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Block until an operation is safe and callback has completed
> 
> This means it is on stable storage on all replicas.
> 
> Note:
> 
> BUG: this should be void
> 
> 
> Parameters
> * **c** – operation to wait for
> Returns0
> 
> int `rados_aio_is_complete_and_cb`\([rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _c_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Has an asynchronous operation and callback completed
> 
> Parameters
> * **c** – async operation to inspect
> Returnswhether c is complete
> 
> int `rados_aio_is_safe_and_cb`\([rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _c_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Is an asynchronous operation safe and has the callback completed
> 
> Parameters
> * **c** – async operation to inspect
> Returnswhether c is safe
> 
> int `rados_aio_get_return_value`\([rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _c_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Get the return value of an asychronous operation
> 
> The return value is set when the operation is complete or safe, whichever comes first.
> 
> Note:
> 
> BUG: complete callback may never be called when the safe message is received before the complete message
> 
> 
> Parameters
> * **c** – async operation to inspect
> PreThe operation is safe or complete
> 
> Returnsreturn value of the operation
> 
> uint64\_t `rados_aio_get_version`\([rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _c_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Get the internal object version of the target of an asychronous operation
> 
> The return value is set when the operation is complete or safe, whichever comes first.
> 
> Note:
> 
> BUG: complete callback may never be called when the safe message is received before the complete message
> 
> 
> Parameters
> * **c** – async operation to inspect
> PreThe operation is safe or complete
> 
> Returnsversion number of the asychronous operation’s target
> 
> void `rados_aio_release`\([rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _c_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Release a completion
> 
> Call this when you no longer need the completion. It may not be freed immediately if the operation is not acked and committed.
> 
> Parameters
> * **c** – completion to release
> int `rados_aio_write`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_oid_, [rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _completion_, _const_ char \*_buf_, size\_t _len_, uint64\_t _off_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Write data to an object asynchronously
> 
> Queues the write and returns. The return value of the completion will be 0 on success, negative error code on failure.
> 
> Parameters
> * **io** – the context in which the write will occur
> * **oid** – name of the object
> * **completion** – what to do when the write is safe and complete
> * **buf** – data to write
> * **len** – length of the data, in bytes
> * **off** – byte offset in the object to begin writing at
> Returns0 on success, \-EROFS if the io context specifies a snap\_seq other than LIBRADOS\_SNAP\_HEAD
> 
> int `rados_aio_append`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_oid_, [rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _completion_, _const_ char \*_buf_, size\_t _len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Asynchronously append data to an object
> 
> Queues the append and returns.
> 
> The return value of the completion will be 0 on success, negative error code on failure.
> 
> Parameters
> * **io** – the context to operate in
> * **oid** – the name of the object
> * **completion** – what to do when the append is safe and complete
> * **buf** – the data to append
> * **len** – length of buf \(in bytes\)
> Returns0 on success, \-EROFS if the io context specifies a snap\_seq other than LIBRADOS\_SNAP\_HEAD
> 
> int `rados_aio_write_full`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_oid_, [rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _completion_, _const_ char \*_buf_, size\_t _len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Asynchronously write an entire object
> 
> The object is filled with the provided data. If the object exists, it is atomically truncated and then written. Queues the write\_full and returns.
> 
> The return value of the completion will be 0 on success, negative error code on failure.
> 
> Parameters
> * **io** – the io context in which the write will occur
> * **oid** – name of the object
> * **completion** – what to do when the write\_full is safe and complete
> * **buf** – data to write
> * **len** – length of the data, in bytes
> Returns0 on success, \-EROFS if the io context specifies a snap\_seq other than LIBRADOS\_SNAP\_HEAD
> 
> int `rados_aio_writesame`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_oid_, [rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _completion_, _const_ char \*_buf_, size\_t _data\_len_, size\_t _write\_len_, uint64\_t _off_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Asynchronously write the same buffer multiple times
> 
> Queues the writesame and returns.
> 
> The return value of the completion will be 0 on success, negative error code on failure.
> 
> Parameters
> * **io** – the io context in which the write will occur
> * **oid** – name of the object
> * **completion** – what to do when the writesame is safe and complete
> * **buf** – data to write
> * **data\_len** – length of the data, in bytes
> * **write\_len** – the total number of bytes to write
> * **off** – byte offset in the object to begin writing at
> Returns0 on success, \-EROFS if the io context specifies a snap\_seq other than LIBRADOS\_SNAP\_HEAD
> 
> int `rados_aio_remove`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_oid_, [rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _completion_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Asynchronously remove an object
> 
> Queues the remove and returns.
> 
> The return value of the completion will be 0 on success, negative error code on failure.
> 
> Parameters
> * **io** – the context to operate in
> * **oid** – the name of the object
> * **completion** – what to do when the remove is safe and complete
> Returns0 on success, \-EROFS if the io context specifies a snap\_seq other than LIBRADOS\_SNAP\_HEAD
> 
> int `rados_aio_read`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_oid_, [rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _completion_, char \*_buf_, size\_t _len_, uint64\_t _off_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Asynchronously read data from an object
> 
> The io context determines the snapshot to read from, if any was set by rados\_ioctx\_snap\_set\_read\(\).
> 
> The return value of the completion will be number of bytes read on success, negative error code on failure.
> 
> Note:
> 
> only the ‘complete’ callback of the completion will be called.
> 
> 
> Parameters
> * **io** – the context in which to perform the read
> * **oid** – the name of the object to read from
> * **completion** – what to do when the read is complete
> * **buf** – where to store the results
> * **len** – the number of bytes to read
> * **off** – the offset to start reading from in the object
> Returns0 on success, negative error code on failure
> 
> int `rados_aio_flush`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Block until all pending writes in an io context are safe
> 
> This is not equivalent to calling rados\_aio\_wait\_for\_safe\(\) on all write completions, since this waits for the associated callbacks to complete as well.
> 
> Note:
> 
> BUG: always returns 0, should be void or accept a timeout
> 
> 
> Parameters
> * **io** – the context to flush
> Returns0 on success, negative error code on failure
> 
> int `rados_aio_flush_async`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, [rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _completion_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Schedule a callback for when all currently pending aio writes are safe. This is a non\-blocking version of rados\_aio\_flush\(\).
> 
> Parameters
> * **io** – the context to flush
> * **completion** – what to do when the writes are safe
> Returns0 on success, negative error code on failure
> 
> int `rados_aio_stat`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_o_, [rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _completion_, uint64\_t \*_psize_, time\_t \*_pmtime_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Asynchronously get object stats \(size/mtime\)
> 
> Parameters
> * **io** – ioctx
> * **o** – object name
> * **completion** – what to do when the stat is complete
> * **psize** – where to store object size
> * **pmtime** – where to store modification time
> Returns0 on success, negative error code on failure
> 
> int `rados_aio_cmpext`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_o_, [rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _completion_, _const_ char \*_cmp\_buf_, size\_t _cmp\_len_, uint64\_t _off_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Asynchronously compare an on\-disk object range with a buffer
> 
> Parameters
> * **io** – the context in which to perform the comparison
> * **o** – the name of the object to compare with
> * **completion** – what to do when the comparison is complete
> * **cmp\_buf** – buffer containing bytes to be compared with object contents
> * **cmp\_len** – length to compare and size of `cmp_buf` in bytes
> * **off** – object byte offset at which to start the comparison
> Returns0 on success, negative error code on failure, \(\-MAX\_ERRNO \- mismatch\_off\) on mismatch
> 
> int `rados_aio_cancel`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, [rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _completion_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Cancel async operation
> 
> Parameters
> * **io** – ioctx
> * **completion** – completion handle
> Returns0 on success, negative error code on failure
> 
> int `rados_aio_exec`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_o_, [rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _completion_, _const_ char \*_cls_, _const_ char \*_method_, _const_ char \*_in\_buf_, size\_t _in\_len_, char \*_buf_, size\_t _out\_len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Asynchronously execute an OSD class method on an object
> 
> The OSD has a plugin mechanism for performing complicated operations on an object atomically. These plugins are called classes. This function allows librados users to call the custom methods. The input and output formats are defined by the class. Classes in ceph.git can be found in src/cls subdirectories
> 
> Parameters
> * **io** – the context in which to call the method
> * **o** – name of the object
> * **completion** – what to do when the exec completes
> * **cls** – the name of the class
> * **method** – the name of the method
> * **in\_buf** – where to find input
> * **in\_len** – length of in\_buf in bytes
> * **buf** – where to store output
> * **out\_len** – length of buf in bytes
> Returns0 on success, negative error code on failure
> 
> 
> Watch/Notify
> 
> Watch/notify is a protocol to help communicate among clients. It can be used to sychronize client state. All that’s needed is a well\-known object name \(for example, rbd uses the header object of an image\).
> 
> Watchers register an interest in an object, and receive all notifies on that object. A notify attempts to communicate with all clients watching an object, and blocks on the notifier until each client responds or a timeout is reached.
> 
> See rados\_watch\(\) and rados\_notify\(\) for more details.
> 
> _typedef_ void \(\*`rados_watchcb_t`\)\(uint8\_t opcode, uint64\_t ver, void \*arg\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Callback activated when a notify is received on a watched object.
> 
> Note:
> 
> BUG: opcode is an internal detail that shouldn’t be exposed
> 
> 
> Note:
> 
> BUG: ver is unused
> 
> 
> Parameters
> * **opcode** – undefined
> * **ver** – version of the watched object
> * **arg** – application\-specific data
> _typedef_ void \(\*`rados_watchcb2_t`\)\(void \*arg, uint64\_t notify\_id, uint64\_t handle, uint64\_t notifier\_id, void \*data, size\_t data\_len\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Callback activated when a notify is received on a watched object.
> 
> Parameters
> * **arg** – opaque user\-defined value provided to rados\_watch2\(\)
> * **notify\_id** – an id for this notify event
> * **handle** – the watcher handle we are notifying
> * **notifier\_id** – the unique client id for the notifier
> * **data** – payload from the notifier
> * **datalen** – length of payload buffer
> _typedef_ void \(\*`rados_watcherrcb_t`\)\(void \*pre, uint64\_t cookie, int err\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Callback activated when we encounter an error with the watch session. This can happen when the location of the objects moves within the cluster and we fail to register our watch with the new object location, or when our connection with the object OSD is otherwise interrupted and we may have missed notify events.
> 
> Parameters
> * **pre** – opaque user\-defined value provided to rados\_watch2\(\)
> * **err** – error code
> int `rados_watch`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_o_, uint64\_t _ver_, uint64\_t \*_cookie_, [rados\_watchcb\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_watchcb_t") _watchcb_, void \*_arg_\) \_\_attribute\_\_\(\(deprecated\)\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Register an interest in an object
> 
> A watch operation registers the client as being interested in notifications on an object. OSDs keep track of watches on persistent storage, so they are preserved across cluster changes by the normal recovery process. If the client loses its connection to the primary OSD for a watched object, the watch will be removed after 30 seconds. Watches are automatically reestablished when a new connection is made, or a placement group switches OSDs.
> 
> Note:
> 
> BUG: librados should provide a way for watchers to notice connection resets
> 
> 
> Note:
> 
> BUG: the ver parameter does not work, and \-ERANGE will never be returned \(See URL tracker.ceph.com/issues/2592\)
> 
> 
> Parameters
> * **io** – the pool the object is in
> * **o** – the object to watch
> * **ver** – expected version of the object
> * **cookie** – where to store the internal id assigned to this watch
> * **watchcb** – what to do when a notify is received on this object
> * **arg** – application defined data to pass when watchcb is called
> Returns0 on success, negative error code on failure
> 
> Returns\-ERANGE if the version of the object is greater than ver
> 
> int `rados_watch2`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_o_, uint64\_t \*_cookie_, [rados\_watchcb2\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_watchcb2_t") _watchcb_, [rados\_watcherrcb\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_watcherrcb_t") _watcherrcb_, void \*_arg_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Register an interest in an object
> 
> A watch operation registers the client as being interested in notifications on an object. OSDs keep track of watches on persistent storage, so they are preserved across cluster changes by the normal recovery process. If the client loses its connection to the primary OSD for a watched object, the watch will be removed after a timeout configured with osd\_client\_watch\_timeout. Watches are automatically reestablished when a new connection is made, or a placement group switches OSDs.
> 
> Parameters
> * **io** – the pool the object is in
> * **o** – the object to watch
> * **cookie** – where to store the internal id assigned to this watch
> * **watchcb** – what to do when a notify is received on this object
> * **watcherrcb** – what to do when the watch session encounters an error
> * **arg** – opaque value to pass to the callback
> Returns0 on success, negative error code on failure
> 
> int `rados_watch3`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_o_, uint64\_t \*_cookie_, [rados\_watchcb2\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_watchcb2_t") _watchcb_, [rados\_watcherrcb\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_watcherrcb_t") _watcherrcb_, uint32\_t _timeout_, void \*_arg_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Register an interest in an object
> 
> A watch operation registers the client as being interested in notifications on an object. OSDs keep track of watches on persistent storage, so they are preserved across cluster changes by the normal recovery process. Watches are automatically reestablished when a new connection is made, or a placement group switches OSDs.
> 
> Parameters
> * **io** – the pool the object is in
> * **o** – the object to watch
> * **cookie** – where to store the internal id assigned to this watch
> * **watchcb** – what to do when a notify is received on this object
> * **watcherrcb** – what to do when the watch session encounters an error
> * **timeout** – how many seconds the connection will keep after disconnection
> * **arg** – opaque value to pass to the callback
> Returns0 on success, negative error code on failure
> 
> int `rados_aio_watch`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_o_, [rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _completion_, uint64\_t \*_handle_, [rados\_watchcb2\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_watchcb2_t") _watchcb_, [rados\_watcherrcb\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_watcherrcb_t") _watcherrcb_, void \*_arg_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Asynchronous register an interest in an object
> 
> A watch operation registers the client as being interested in notifications on an object. OSDs keep track of watches on persistent storage, so they are preserved across cluster changes by the normal recovery process. If the client loses its connection to the primary OSD for a watched object, the watch will be removed after 30 seconds. Watches are automatically reestablished when a new connection is made, or a placement group switches OSDs.
> 
> Parameters
> * **io** – the pool the object is in
> * **o** – the object to watch
> * **completion** – what to do when operation has been attempted
> * **handle** – where to store the internal id assigned to this watch
> * **watchcb** – what to do when a notify is received on this object
> * **watcherrcb** – what to do when the watch session encounters an error
> * **arg** – opaque value to pass to the callback
> Returns0 on success, negative error code on failure
> 
> int `rados_aio_watch2`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_o_, [rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _completion_, uint64\_t \*_handle_, [rados\_watchcb2\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_watchcb2_t") _watchcb_, [rados\_watcherrcb\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_watcherrcb_t") _watcherrcb_, uint32\_t _timeout_, void \*_arg_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Asynchronous register an interest in an object
> 
> A watch operation registers the client as being interested in notifications on an object. OSDs keep track of watches on persistent storage, so they are preserved across cluster changes by the normal recovery process. If the client loses its connection to the primary OSD for a watched object, the watch will be removed after the number of seconds that configured in timeout parameter. Watches are automatically reestablished when a new connection is made, or a placement group switches OSDs.
> 
> Parameters
> * **io** – the pool the object is in
> * **o** – the object to watch
> * **completion** – what to do when operation has been attempted
> * **handle** – where to store the internal id assigned to this watch
> * **watchcb** – what to do when a notify is received on this object
> * **watcherrcb** – what to do when the watch session encounters an error
> * **timeout** – how many seconds the connection will keep after disconnection
> * **arg** – opaque value to pass to the callback
> Returns0 on success, negative error code on failure
> 
> int `rados_watch_check`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, uint64\_t _cookie_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Check on the status of a watch
> 
> Return the number of milliseconds since the watch was last confirmed. Or, if there has been an error, return that.
> 
> If there is an error, the watch is no longer valid, and should be destroyed with rados\_unwatch2\(\). The the user is still interested in the object, a new watch should be created with rados\_watch2\(\).
> 
> Parameters
> * **io** – the pool the object is in
> * **cookie** – the watch handle
> Returnsms since last confirmed on success, negative error code on failure
> 
> int `rados_unwatch`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_o_, uint64\_t _cookie_\) \_\_attribute\_\_\(\(deprecated\)\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Unregister an interest in an object
> 
> Once this completes, no more notifies will be sent to us for this watch. This should be called to clean up unneeded watchers.
> 
> Parameters
> * **io** – the pool the object is in
> * **o** – the name of the watched object \(ignored\)
> * **cookie** – which watch to unregister
> Returns0 on success, negative error code on failure
> 
> int `rados_unwatch2`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, uint64\_t _cookie_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Unregister an interest in an object
> 
> Once this completes, no more notifies will be sent to us for this watch. This should be called to clean up unneeded watchers.
> 
> Parameters
> * **io** – the pool the object is in
> * **cookie** – which watch to unregister
> Returns0 on success, negative error code on failure
> 
> int `rados_aio_unwatch`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, uint64\_t _cookie_, [rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _completion_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Asynchronous unregister an interest in an object
> 
> Once this completes, no more notifies will be sent to us for this watch. This should be called to clean up unneeded watchers.
> 
> Parameters
> * **io** – the pool the object is in
> * **completion** – what to do when operation has been attempted
> * **cookie** – which watch to unregister
> Returns0 on success, negative error code on failure
> 
> int `rados_notify`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_o_, uint64\_t _ver_, _const_ char \*_buf_, int _buf\_len_\) \_\_attribute\_\_\(\(deprecated\)\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Sychronously notify watchers of an object
> 
> This blocks until all watchers of the object have received and reacted to the notify, or a timeout is reached.
> 
> Note:
> 
> BUG: the timeout is not changeable via the C API
> 
> 
> Note:
> 
> BUG: the bufferlist is inaccessible in a rados\_watchcb\_t
> 
> 
> Parameters
> * **io** – the pool the object is in
> * **o** – the name of the object
> * **ver** – obsolete \- just pass zero
> * **buf** – data to send to watchers
> * **buf\_len** – length of buf in bytes
> Returns0 on success, negative error code on failure
> 
> int `rados_aio_notify`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_o_, [rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _completion_, _const_ char \*_buf_, int _buf\_len_, uint64\_t _timeout\_ms_, char \*\*_reply\_buffer_, size\_t \*_reply\_buffer\_len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Sychronously notify watchers of an object
> 
> This blocks until all watchers of the object have received and reacted to the notify, or a timeout is reached.
> 
> The reply buffer is optional. If specified, the client will get back an encoded buffer that includes the ids of the clients that acknowledged the notify as well as their notify ack payloads \(if any\). Clients that timed out are not included. Even clients that do not include a notify ack payload are included in the list but have a 0\-length payload associated with them. The format:
> 
> le32 num\_acks { le64 gid global id for the client \(for client.1234 that’s 1234\) le64 cookie cookie for the client le32 buflen length of reply message buffer u8 \* buflen payload } \* num\_acks le32 num\_timeouts { le64 gid global id for the client le64 cookie cookie for the client } \* num\_timeouts
> 
> Note: There may be multiple instances of the same gid if there are multiple watchers registered via the same client.
> 
> Note: The buffer must be released with rados\_buffer\_free\(\) when the user is done with it.
> 
> Note: Since the result buffer includes clients that time out, it will be set even when rados\_notify\(\) returns an error code \(like \-ETIMEDOUT\).
> 
> Parameters
> * **io** – the pool the object is in
> * **completion** – what to do when operation has been attempted
> * **o** – the name of the object
> * **buf** – data to send to watchers
> * **buf\_len** – length of buf in bytes
> * **timeout\_ms** – notify timeout \(in ms\)
> * **reply\_buffer** – pointer to reply buffer pointer \(free with rados\_buffer\_free\)
> * **reply\_buffer\_len** – pointer to size of reply buffer
> Returns0 on success, negative error code on failure
> 
> int `rados_notify2`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_o_, _const_ char \*_buf_, int _buf\_len_, uint64\_t _timeout\_ms_, char \*\*_reply\_buffer_, size\_t \*_reply\_buffer\_len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> int `rados_decode_notify_response`\(char \*_reply\_buffer_, size\_t _reply\_buffer\_len_, _struct_ [notify\_ack\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "notify_ack_t") \*\*_acks_, size\_t \*_nr\_acks_, _struct_ [notify\_timeout\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "notify_timeout_t") \*\*_timeouts_, size\_t \*_nr\_timeouts_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Decode a notify response
> 
> Decode a notify response \(from rados\_aio\_notify\(\) call\) into acks and timeout arrays.
> 
> Parameters
> * **reply\_buffer** – buffer from rados\_aio\_notify\(\) call
> * **reply\_buffer\_len** – reply\_buffer length
> * **acks** – pointer to struct [notify\_ack\_t](https://docs.ceph.com/en/pacific/rados/api/librados/) pointer
> * **nr\_acks** – pointer to ack count
> * **timeouts** – pointer to [notify\_timeout\_t](https://docs.ceph.com/en/pacific/rados/api/librados/) pointer
> * **nr\_timeouts** – pointer to timeout count
> Returns0 on success
> 
> void `rados_free_notify_response`\(_struct_ [notify\_ack\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "notify_ack_t") \*_acks_, size\_t _nr\_acks_, _struct_ [notify\_timeout\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "notify_timeout_t") \*_timeouts_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Free notify allocated buffer
> 
> Release memory allocated by rados\_decode\_notify\_response\(\) call
> 
> Parameters
> * **acks** – [notify\_ack\_t](https://docs.ceph.com/en/pacific/rados/api/librados/) struct \(from rados\_decode\_notify\_response\(\)\)
> * **nr\_acks** – ack count
> * **timeouts** – [notify\_timeout\_t](https://docs.ceph.com/en/pacific/rados/api/librados/) struct \(from rados\_decode\_notify\_response\(\)\)
> int `rados_notify_ack`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_o_, uint64\_t _notify\_id_, uint64\_t _cookie_, _const_ char \*_buf_, int _buf\_len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Acknolwedge receipt of a notify
> 
> Parameters
> * **io** – the pool the object is in
> * **o** – the name of the object
> * **notify\_id** – the notify\_id we got on the watchcb2\_t callback
> * **cookie** – the watcher handle
> * **buf** – payload to return to notifier \(optional\)
> * **buf\_len** – payload length
> Returns0 on success
> 
> int `rados_watch_flush`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Flush watch/notify callbacks
> 
> This call will block until all pending watch/notify callbacks have been executed and the queue is empty. It should usually be called after shutting down any watches before shutting down the ioctx or librados to ensure that any callbacks do not misuse the ioctx \(for example by calling rados\_notify\_ack after the ioctx has been destroyed\).
> 
> Parameters
> * **cluster** – the cluster handle
> int `rados_aio_watch_flush`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, [rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _completion_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Flush watch/notify callbacks
> 
> This call will be nonblock, and the completion will be called until all pending watch/notify callbacks have been executed and the queue is empty. It should usually be called after shutting down any watches before shutting down the ioctx or librados to ensure that any callbacks do not misuse the ioctx \(for example by calling rados\_notify\_ack after the ioctx has been destroyed\).
> 
> Parameters
> * **cluster** – the cluster handle
> * **completion** – what to do when operation has been attempted
> 
> Mon/OSD/PG Commands
> 
> These interfaces send commands relating to the monitor, OSD, or PGs.
> 
> _typedef_ void \(\*`rados_log_callback_t`\)\(void \*arg, _const_ char \*line, _const_ char \*who, uint64\_t sec, uint64\_t nsec, uint64\_t seq, _const_ char \*level, _const_ char \*msg\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _typedef_ void \(\*`rados_log_callback2_t`\)\(void \*arg, _const_ char \*line, _const_ char \*channel, _const_ char \*who, _const_ char \*name, uint64\_t sec, uint64\_t nsec, uint64\_t seq, _const_ char \*level, _const_ char \*msg\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> int `rados_mon_command`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, _const_ char \*\*_cmd_, size\_t _cmdlen_, _const_ char \*_inbuf_, size\_t _inbuflen_, char \*\*_outbuf_, size\_t \*_outbuflen_, char \*\*_outs_, size\_t \*_outslen_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Send monitor command.
> 
> The result buffers are allocated on the heap; the caller is expected to release that memory with rados\_buffer\_free\(\). The buffer and length pointers can all be NULL, in which case they are not filled in.
> 
> Note:
> 
> Takes command string in carefully\-formatted JSON; must match defined commands, types, etc.
> 
> 
> Parameters
> * **cluster** – cluster handle
> * **cmd** – an array of char \*’s representing the command
> * **cmdlen** – count of valid entries in cmd
> * **inbuf** – any bulk input data \(crush map, etc.\)
> * **inbuflen** – input buffer length
> * **outbuf** – double pointer to output buffer
> * **outbuflen** – pointer to output buffer length
> * **outs** – double pointer to status string
> * **outslen** – pointer to status string length
> Returns0 on success, negative error code on failure
> 
> int `rados_mgr_command`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, _const_ char \*\*_cmd_, size\_t _cmdlen_, _const_ char \*_inbuf_, size\_t _inbuflen_, char \*\*_outbuf_, size\_t \*_outbuflen_, char \*\*_outs_, size\_t \*_outslen_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Send ceph\-mgr command.
> 
> The result buffers are allocated on the heap; the caller is expected to release that memory with rados\_buffer\_free\(\). The buffer and length pointers can all be NULL, in which case they are not filled in.
> 
> Note:
> 
> Takes command string in carefully\-formatted JSON; must match defined commands, types, etc.
> 
> 
> Parameters
> * **cluster** – cluster handle
> * **cmd** – an array of char \*’s representing the command
> * **cmdlen** – count of valid entries in cmd
> * **inbuf** – any bulk input data \(crush map, etc.\)
> * **inbuflen** – input buffer length
> * **outbuf** – double pointer to output buffer
> * **outbuflen** – pointer to output buffer length
> * **outs** – double pointer to status string
> * **outslen** – pointer to status string length
> Returns0 on success, negative error code on failure
> 
> int `rados_mgr_command_target`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, _const_ char \*_name_, _const_ char \*\*_cmd_, size\_t _cmdlen_, _const_ char \*_inbuf_, size\_t _inbuflen_, char \*\*_outbuf_, size\_t \*_outbuflen_, char \*\*_outs_, size\_t \*_outslen_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Send ceph\-mgr tell command.
> 
> The result buffers are allocated on the heap; the caller is expected to release that memory with rados\_buffer\_free\(\). The buffer and length pointers can all be NULL, in which case they are not filled in.
> 
> Note:
> 
> Takes command string in carefully\-formatted JSON; must match defined commands, types, etc.
> 
> 
> Parameters
> * **cluster** – cluster handle
> * **name** – mgr name to target
> * **cmd** – an array of char \*’s representing the command
> * **cmdlen** – count of valid entries in cmd
> * **inbuf** – any bulk input data \(crush map, etc.\)
> * **inbuflen** – input buffer length
> * **outbuf** – double pointer to output buffer
> * **outbuflen** – pointer to output buffer length
> * **outs** – double pointer to status string
> * **outslen** – pointer to status string length
> Returns0 on success, negative error code on failure
> 
> int `rados_mon_command_target`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, _const_ char \*_name_, _const_ char \*\*_cmd_, size\_t _cmdlen_, _const_ char \*_inbuf_, size\_t _inbuflen_, char \*\*_outbuf_, size\_t \*_outbuflen_, char \*\*_outs_, size\_t \*_outslen_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Send monitor command to a specific monitor.
> 
> The result buffers are allocated on the heap; the caller is expected to release that memory with rados\_buffer\_free\(\). The buffer and length pointers can all be NULL, in which case they are not filled in.
> 
> Note:
> 
> Takes command string in carefully\-formatted JSON; must match defined commands, types, etc.
> 
> 
> Parameters
> * **cluster** – cluster handle
> * **name** – target monitor’s name
> * **cmd** – an array of char \*’s representing the command
> * **cmdlen** – count of valid entries in cmd
> * **inbuf** – any bulk input data \(crush map, etc.\)
> * **inbuflen** – input buffer length
> * **outbuf** – double pointer to output buffer
> * **outbuflen** – pointer to output buffer length
> * **outs** – double pointer to status string
> * **outslen** – pointer to status string length
> Returns0 on success, negative error code on failure
> 
> void `rados_buffer_free`\(char \*_buf_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> free a rados\-allocated buffer
> 
> Release memory allocated by librados calls like rados\_mon\_command\(\).
> 
> Parameters
> * **buf** – buffer pointer
> int `rados_osd_command`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, int _osdid_, _const_ char \*\*_cmd_, size\_t _cmdlen_, _const_ char \*_inbuf_, size\_t _inbuflen_, char \*\*_outbuf_, size\_t \*_outbuflen_, char \*\*_outs_, size\_t \*_outslen_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> int `rados_pg_command`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, _const_ char \*_pgstr_, _const_ char \*\*_cmd_, size\_t _cmdlen_, _const_ char \*_inbuf_, size\_t _inbuflen_, char \*\*_outbuf_, size\_t \*_outbuflen_, char \*\*_outs_, size\_t \*_outslen_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> int `rados_monitor_log`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, _const_ char \*_level_, [rados\_log\_callback\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_log_callback_t") _cb_, void \*_arg_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> int `rados_monitor_log2`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, _const_ char \*_level_, [rados\_log\_callback2\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_log_callback2_t") _cb_, void \*_arg_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> int `rados_service_register`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, _const_ char \*_service_, _const_ char \*_daemon_, _const_ char \*_metadata\_dict_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> register daemon instance for a service
> 
> Register us as a daemon providing a particular service. We identify the service \(e.g., ‘rgw’\) and our instance name \(e.g., ‘rgw.$hostname’\). The metadata is a map of keys and values with arbitrary static metdata for this instance. The encoding is a series of NULL\-terminated strings, alternating key names and values, terminating with an empty key name. For example, “foo0bar0this0that00” is the dict {foo=bar,this=that}.
> 
> For the lifetime of the librados instance, regular beacons will be sent to the cluster to maintain our registration in the service map.
> 
> Parameters
> * **cluster** – handle
> * **service** – service name
> * **daemon** – daemon instance name
> * **metadata\_dict** – static daemon metadata dict
> int `rados_service_update_status`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, _const_ char \*_status\_dict_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> update daemon status
> 
> Update our mutable status information in the service map.
> 
> The status dict is encoded the same way the daemon metadata is encoded for rados\_service\_register. For example, “foo0bar0this0that00” is {foo=bar,this=that}.
> 
> Parameters
> * **cluster** – rados cluster handle
> * **status\_dict** – status dict
> 
> Setup and Teardown
> 
> These are the first and last functions to that should be called when using librados.
> 
> int `rados_create`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") \*_cluster_, _const_ char \*_const_ _id_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Create a handle for communicating with a RADOS cluster.
> 
> Ceph environment variables are read when this is called, so if $CEPH\_ARGS specifies everything you need to connect, no further configuration is necessary.
> 
> Parameters
> * **cluster** – where to store the handle
> * **id** – the user to connect as \(i.e. admin, not client.admin\)
> Returns0 on success, negative error code on failure
> 
> int `rados_create2`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") \*_pcluster_, _const_ char \*_const_ _clustername_, _const_ char \*_const_ _name_, uint64\_t _flags_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Extended version of rados\_create.
> 
> Like rados\_create, but 1\) don’t assume ‘client.’\+id; allow full specification of name 2\) allow specification of cluster name 3\) flags for future expansion
> 
> int `rados_create_with_context`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") \*_cluster_, [rados\_config\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_config_t") _cct_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Initialize a cluster handle from an existing configuration.
> 
> Share configuration state with another rados\_t instance.
> 
> Parameters
> * **cluster** – where to store the handle
> * **cct** – the existing configuration to use
> Returns0 on success, negative error code on failure
> 
> int `rados_ping_monitor`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, _const_ char \*_mon\_id_, char \*\*_outstr_, size\_t \*_outstrlen_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Ping the monitor with ID mon\_id, storing the resulting reply in buf \(if specified\) with a maximum size of len.
> 
> The result buffer is allocated on the heap; the caller is expected to release that memory with rados\_buffer\_free\(\). The buffer and length pointers can be NULL, in which case they are not filled in.
> 
> Parameters
> * **cluster** – cluster handle
> * **mon\_id** – \[in\] ID of the monitor to ping
> * **outstr** – \[out\] double pointer with the resulting reply
> * **outstrlen** – \[out\] pointer with the size of the reply in outstr
> int `rados_connect`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Connect to the cluster.
> 
> Note:
> 
> BUG: Before calling this, calling a function that communicates with the cluster will crash.
> 
> 
> Parameters
> * **cluster** – The cluster to connect to.
> PreThe cluster handle is configured with at least a monitor address. If cephx is enabled, a client name and secret must also be set.
> 
> PostIf this succeeds, any function in librados may be used
> 
> Returns0 on success, negative error code on failure
> 
> void `rados_shutdown`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Disconnects from the cluster.
> 
> For clean up, this is only necessary after rados\_connect\(\) has succeeded.
> 
> Warning:
> 
> This does not guarantee any asynchronous writes have completed. To do that, you must call rados\_aio\_flush\(\) on all open io contexts.
> 
> 
> Warning:
> 
> We implicitly call rados\_watch\_flush\(\) on shutdown. If there are watches being used, this should be done explicitly before destroying the relevant IoCtx. We do it here as a safety measure.
> 
> 
> Parameters
> * **cluster** – the cluster to shutdown
> Postthe cluster handle cannot be used again
> 
> 
> Configuration
> 
> These functions read and update Ceph configuration for a cluster handle. Any configuration changes must be done before connecting to the cluster.
> 
> Options that librados users might want to set include:
> 
> 
> 
> * mon\_host
> * auth\_supported
> * key, keyfile, or keyring when using cephx
> * log\_file, log\_to\_stderr, err\_to\_stderr, and log\_to\_syslog
> * debug\_rados, debug\_objecter, debug\_monc, debug\_auth, or debug\_ms
> 
> 
> See docs.ceph.com for information about available configuration options\`
> 
> int `rados_conf_read_file`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, _const_ char \*_path_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Configure the cluster handle using a Ceph config file
> 
> If path is NULL, the default locations are searched, and the first found is used. The locations are:
> 
> 
> 
> * $CEPH\_CONF \(environment variable\)
> * /etc/ceph/ceph.conf
> * ~/.ceph/config
> * ceph.conf \(in the current working directory\)
> 
> 
> Parameters
> * **cluster** – cluster handle to configure
> * **path** – path to a Ceph configuration file
> Prerados\_connect\(\) has not been called on the cluster handle
> 
> Returns0 on success, negative error code on failure
> 
> int `rados_conf_parse_argv`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, int _argc_, _const_ char \*\*_argv_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Configure the cluster handle with command line arguments
> 
> argv can contain any common Ceph command line option, including any configuration parameter prefixed by ‘’ and replacing spaces with dashes or underscores. For example, the following options are equivalent:
> 
> 
> 
> * mon\-host 10.0.0.1:6789
> * mon\_host 10.0.0.1:6789
> * \-m 10.0.0.1:6789
> 
> 
> Parameters
> * **cluster** – cluster handle to configure
> * **argc** – number of arguments in argv
> * **argv** – arguments to parse
> Prerados\_connect\(\) has not been called on the cluster handle
> 
> Returns0 on success, negative error code on failure
> 
> int `rados_conf_parse_argv_remainder`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, int _argc_, _const_ char \*\*_argv_, _const_ char \*\*_remargv_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Configure the cluster handle with command line arguments, returning any remainders. Same rados\_conf\_parse\_argv, except for extra remargv argument to hold returns unrecognized arguments.
> 
> Parameters
> * **cluster** – cluster handle to configure
> * **argc** – number of arguments in argv
> * **argv** – arguments to parse
> * **remargv** – char\* array for returned unrecognized arguments
> Prerados\_connect\(\) has not been called on the cluster handle
> 
> Returns0 on success, negative error code on failure
> 
> int `rados_conf_parse_env`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, _const_ char \*_var_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Configure the cluster handle based on an environment variable
> 
> The contents of the environment variable are parsed as if they were Ceph command line options. If var is NULL, the CEPH\_ARGS environment variable is used.
> 
> Note:
> 
> BUG: this is not threadsafe \- it uses a static buffer
> 
> 
> Parameters
> * **cluster** – cluster handle to configure
> * **var** – name of the environment variable to read
> Prerados\_connect\(\) has not been called on the cluster handle
> 
> Returns0 on success, negative error code on failure
> 
> int `rados_conf_set`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, _const_ char \*_option_, _const_ char \*_value_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Set a configuration option
> 
> Parameters
> * **cluster** – cluster handle to configure
> * **option** – option to set
> * **value** – value of the option
> Prerados\_connect\(\) has not been called on the cluster handle
> 
> Returns0 on success, negative error code on failure
> 
> Returns\-ENOENT when the option is not a Ceph configuration option
> 
> int `rados_conf_get`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, _const_ char \*_option_, char \*_buf_, size\_t _len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Get the value of a configuration option
> 
> Parameters
> * **cluster** – configuration to read
> * **option** – which option to read
> * **buf** – where to write the configuration value
> * **len** – the size of buf in bytes
> Returns0 on success, negative error code on failure
> 
> Returns\-ENAMETOOLONG if the buffer is too short to contain the requested value
> 
> 
> Pools
> 
> RADOS pools are separate namespaces for objects. Pools may have different crush rules associated with them, so they could have differing replication levels or placement strategies. RADOS permissions are also tied to pools \- users can have different read, write, and execute permissions on a per\-pool basis.
> 
> int `rados_pool_list`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, char \*_buf_, size\_t _len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> List pools
> 
> Gets a list of pool names as NULL\-terminated strings. The pool names will be placed in the supplied buffer one after another. After the last pool name, there will be two 0 bytes in a row.
> 
> If len is too short to fit all the pool name entries we need, we will fill as much as we can.
> 
> Buf may be null to determine the buffer size needed to list all pools.
> 
> Parameters
> * **cluster** – cluster handle
> * **buf** – output buffer
> * **len** – output buffer length
> Returnslength of the buffer we would need to list all pools
> 
> int `rados_inconsistent_pg_list`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, int64\_t _pool_, char \*_buf_, size\_t _len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> List inconsistent placement groups of the given pool
> 
> Gets a list of inconsistent placement groups as NULL\-terminated strings. The placement group names will be placed in the supplied buffer one after another. After the last name, there will be two 0 types in a row.
> 
> If len is too short to fit all the placement group entries we need, we will fill as much as we can.
> 
> Parameters
> * **cluster** – cluster handle
> * **pool** – pool ID
> * **buf** – output buffer
> * **len** – output buffer length
> Returnslength of the buffer we would need to list all pools
> 
> [rados\_config\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_config_t") `rados_cct`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Get a configuration handle for a rados cluster handle
> 
> This handle is valid only as long as the cluster handle is valid.
> 
> Parameters
> * **cluster** – cluster handle
> Returnsconfig handle for this cluster
> 
> uint64\_t `rados_get_instance_id`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Get a global id for current instance
> 
> This id is a unique representation of current connection to the cluster
> 
> Parameters
> * **cluster** – cluster handle
> Returnsinstance global id
> 
> int `rados_get_min_compatible_osd`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, int8\_t \*_require\_osd\_release_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Gets the minimum compatible OSD version
> 
> Parameters
> * **cluster** – cluster handle
> * **require\_osd\_release** – \[out\] minimum compatible OSD version based upon the current features
> Returns0 on sucess, negative error code on failure
> 
> int `rados_get_min_compatible_client`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, int8\_t \*_min\_compat\_client_, int8\_t \*_require\_min\_compat\_client_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Gets the minimum compatible client version
> 
> Parameters
> * **cluster** – cluster handle
> * **min\_compat\_client** – \[out\] minimum compatible client version based upon the current features
> * **require\_min\_compat\_client** – \[out\] required minimum client version based upon explicit setting
> Returns0 on success, negative error code on failure
> 
> int `rados_ioctx_create`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, _const_ char \*_pool\_name_, [rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") \*_ioctx_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Create an io context
> 
> The io context allows you to perform operations within a particular pool. For more details see rados\_ioctx\_t.
> 
> Parameters
> * **cluster** – which cluster the pool is in
> * **pool\_name** – name of the pool
> * **ioctx** – where to store the io context
> Returns0 on success, negative error code on failure
> 
> int `rados_ioctx_create2`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, int64\_t _pool\_id_, [rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") \*_ioctx_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> void `rados_ioctx_destroy`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> The opposite of rados\_ioctx\_create
> 
> This just tells librados that you no longer need to use the io context. It may not be freed immediately if there are pending asynchronous requests on it, but you should not use an io context again after calling this function on it.
> 
> Warning:
> 
> This does not guarantee any asynchronous writes have completed. You must call rados\_aio\_flush\(\) on the io context before destroying it to do that.
> 
> 
> Warning:
> 
> If this ioctx is used by rados\_watch, the caller needs to be sure that all registered watches are disconnected via rados\_unwatch\(\) and that rados\_watch\_flush\(\) is called. This ensures that a racing watch callback does not make use of a destroyed ioctx.
> 
> 
> Parameters
> * **io** – the io context to dispose of
> [rados\_config\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_config_t") `rados_ioctx_cct`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Get configuration handle for a pool handle
> 
> Parameters
> * **io** – pool handle
> Returnsrados\_config\_t for this cluster
> 
> [rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") `rados_ioctx_get_cluster`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Get the cluster handle used by this rados\_ioctx\_t Note that this is a weak reference, and should not be destroyed via rados\_shutdown\(\).
> 
> Parameters
> * **io** – the io context
> Returnsthe cluster handle for this io context
> 
> int `rados_ioctx_pool_stat`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _struct_ [rados\_pool\_stat\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_pool_stat_t") \*_stats_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Get pool usage statistics
> 
> Fills in a [rados\_pool\_stat\_t](https://docs.ceph.com/en/pacific/rados/api/librados/) after querying the cluster.
> 
> Parameters
> * **io** – determines which pool to query
> * **stats** – where to store the results
> Returns0 on success, negative error code on failure
> 
> int64\_t `rados_pool_lookup`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, _const_ char \*_pool\_name_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Get the id of a pool
> 
> Parameters
> * **cluster** – which cluster the pool is in
> * **pool\_name** – which pool to look up
> Returnsid of the pool
> 
> Returns\-ENOENT if the pool is not found
> 
> int `rados_pool_reverse_lookup`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, int64\_t _id_, char \*_buf_, size\_t _maxlen_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Get the name of a pool
> 
> Parameters
> * **cluster** – which cluster the pool is in
> * **id** – the id of the pool
> * **buf** – where to store the pool name
> * **maxlen** – size of buffer where name will be stored
> Returnslength of string stored, or \-ERANGE if buffer too small
> 
> int `rados_pool_create`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, _const_ char \*_pool\_name_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Create a pool with default settings
> 
> The default crush rule is rule 0.
> 
> Parameters
> * **cluster** – the cluster in which the pool will be created
> * **pool\_name** – the name of the new pool
> Returns0 on success, negative error code on failure
> 
> int `rados_pool_create_with_auid`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, _const_ char \*_pool\_name_, uint64\_t _auid_\) \_\_attribute\_\_\(\(deprecated\)\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Create a pool owned by a specific auid.
> 
> DEPRECATED: auid support has been removed, and this call will be removed in a future release.
> 
> Parameters
> * **cluster** – the cluster in which the pool will be created
> * **pool\_name** – the name of the new pool
> * **auid** – the id of the owner of the new pool
> Returns0 on success, negative error code on failure
> 
> int `rados_pool_create_with_crush_rule`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, _const_ char \*_pool\_name_, uint8\_t _crush\_rule\_num_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Create a pool with a specific CRUSH rule
> 
> Parameters
> * **cluster** – the cluster in which the pool will be created
> * **pool\_name** – the name of the new pool
> * **crush\_rule\_num** – which rule to use for placement in the new pool1
> Returns0 on success, negative error code on failure
> 
> int `rados_pool_create_with_all`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, _const_ char \*_pool\_name_, uint64\_t _auid_, uint8\_t _crush\_rule\_num_\) \_\_attribute\_\_\(\(deprecated\)\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Create a pool with a specific CRUSH rule and auid
> 
> DEPRECATED: auid support has been removed and this call will be removed in a future release.
> 
> This is a combination of rados\_pool\_create\_with\_crush\_rule\(\) and rados\_pool\_create\_with\_auid\(\).
> 
> Parameters
> * **cluster** – the cluster in which the pool will be created
> * **pool\_name** – the name of the new pool
> * **crush\_rule\_num** – which rule to use for placement in the new pool2
> * **auid** – the id of the owner of the new pool
> Returns0 on success, negative error code on failure
> 
> int `rados_pool_get_base_tier`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, int64\_t _pool_, int64\_t \*_base\_tier_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Returns the pool that is the base tier for this pool.
> 
> The return value is the ID of the pool that should be used to read from/write to. If tiering is not set up for the pool, returns `pool`.
> 
> Parameters
> * **cluster** – the cluster the pool is in
> * **pool** – ID of the pool to query
> * **base\_tier** – \[out\] base tier, or `pool` if tiering is not configured
> Returns0 on success, negative error code on failure
> 
> int `rados_pool_delete`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, _const_ char \*_pool\_name_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Delete a pool and all data inside it
> 
> The pool is removed from the cluster immediately, but the actual data is deleted in the background.
> 
> Parameters
> * **cluster** – the cluster the pool is in
> * **pool\_name** – which pool to delete
> Returns0 on success, negative error code on failure
> 
> int `rados_ioctx_pool_set_auid`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, uint64\_t _auid_\) \_\_attribute\_\_\(\(deprecated\)\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Attempt to change an io context’s associated auid “owner”
> 
> DEPRECATED: auid support has been removed and this call has no effect.
> 
> Requires that you have write permission on both the current and new auid.
> 
> Parameters
> * **io** – reference to the pool to change.
> * **auid** – the auid you wish the io to have.
> Returns0 on success, negative error code on failure
> 
> int `rados_ioctx_pool_get_auid`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, uint64\_t \*_auid_\) \_\_attribute\_\_\(\(deprecated\)\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Get the auid of a pool
> 
> DEPRECATED: auid support has been removed and this call always reports CEPH\_AUTH\_UID\_DEFAULT \(\-1\).
> 
> Parameters
> * **io** – pool to query
> * **auid** – where to store the auid
> Returns0 on success, negative error code on failure
> 
> int `rados_ioctx_pool_requires_alignment`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_\) \_\_attribute\_\_\(\(deprecated\)\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> int `rados_ioctx_pool_requires_alignment2`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, int \*_req_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Test whether the specified pool requires alignment or not.
> 
> Parameters
> * **io** – pool to query
> * **req** – 1 if alignment is supported, 0 if not.
> Returns0 on success, negative error code on failure
> 
> uint64\_t `rados_ioctx_pool_required_alignment`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_\) \_\_attribute\_\_\(\(deprecated\)\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> int `rados_ioctx_pool_required_alignment2`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, uint64\_t \*_alignment_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Get the alignment flavor of a pool
> 
> Parameters
> * **io** – pool to query
> * **alignment** – where to store the alignment flavor
> Returns0 on success, negative error code on failure
> 
> int64\_t `rados_ioctx_get_id`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Get the pool id of the io context
> 
> Parameters
> * **io** – the io context to query
> Returnsthe id of the pool the io context uses
> 
> int `rados_ioctx_get_pool_name`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, char \*_buf_, unsigned _maxlen_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Get the pool name of the io context
> 
> Parameters
> * **io** – the io context to query
> * **buf** – pointer to buffer where name will be stored
> * **maxlen** – size of buffer where name will be stored
> Returnslength of string stored, or \-ERANGE if buffer too small
> 
> 
> Object Locators
> 
> void `rados_ioctx_locator_set_key`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_key_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Set the key for mapping objects to pgs within an io context.
> 
> The key is used instead of the object name to determine which placement groups an object is put in. This affects all subsequent operations of the io context \- until a different locator key is set, all objects in this io context will be placed in the same pg.
> 
> Parameters
> * **io** – the io context to change
> * **key** – the key to use as the object locator, or NULL to discard any previously set key
> void `rados_ioctx_set_namespace`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_nspace_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Set the namespace for objects within an io context
> 
> The namespace specification further refines a pool into different domains. The mapping of objects to pgs is also based on this value.
> 
> Parameters
> * **io** – the io context to change
> * **nspace** – the name to use as the namespace, or NULL use the default namespace
> int `rados_ioctx_get_namespace`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, char \*_buf_, unsigned _maxlen_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Get the namespace for objects within the io context
> 
> Parameters
> * **io** – the io context to query
> * **buf** – pointer to buffer where name will be stored
> * **maxlen** – size of buffer where name will be stored
> Returnslength of string stored, or \-ERANGE if buffer too small
> 
> 
> Listing Objects
> 
> int `rados_nobjects_list_open`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, [rados\_list\_ctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_list_ctx_t") \*_ctx_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Start listing objects in a pool
> 
> Parameters
> * **io** – the pool to list from
> * **ctx** – the handle to store list context in
> Returns0 on success, negative error code on failure
> 
> uint32\_t `rados_nobjects_list_get_pg_hash_position`\([rados\_list\_ctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_list_ctx_t") _ctx_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Return hash position of iterator, rounded to the current PG
> 
> Parameters
> * **ctx** – iterator marking where you are in the listing
> Returnscurrent hash position, rounded to the current pg
> 
> uint32\_t `rados_nobjects_list_seek`\([rados\_list\_ctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_list_ctx_t") _ctx_, uint32\_t _pos_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Reposition object iterator to a different hash position
> 
> Parameters
> * **ctx** – iterator marking where you are in the listing
> * **pos** – hash position to move to
> Returnsactual \(rounded\) position we moved to
> 
> uint32\_t `rados_nobjects_list_seek_cursor`\([rados\_list\_ctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_list_ctx_t") _ctx_, [rados\_object\_list\_cursor](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_object_list_cursor") _cursor_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Reposition object iterator to a different position
> 
> Parameters
> * **ctx** – iterator marking where you are in the listing
> * **cursor** – position to move to
> Returnsrounded position we moved to
> 
> int `rados_nobjects_list_get_cursor`\([rados\_list\_ctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_list_ctx_t") _ctx_, [rados\_object\_list\_cursor](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_object_list_cursor") \*_cursor_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Reposition object iterator to a different position
> 
> The returned handle must be released with rados\_object\_list\_cursor\_free\(\).
> 
> Parameters
> * **ctx** – iterator marking where you are in the listing
> * **cursor** – where to store cursor
> Returns0 on success, negative error code on failure
> 
> int `rados_nobjects_list_next`\([rados\_list\_ctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_list_ctx_t") _ctx_, _const_ char \*\*_entry_, _const_ char \*\*_key_, _const_ char \*\*_nspace_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Get the next object name and locator in the pool
> 
> _entry and \*key are valid until next call to rados\_nobjects\_list\__
> 
> Parameters
> * **ctx** – iterator marking where you are in the listing
> * **entry** – where to store the name of the entry
> * **key** – where to store the object locator \(set to NULL to ignore\)
> * **nspace** – where to store the object namespace \(set to NULL to ignore\)
> Returns0 on success, negative error code on failure
> 
> Returns\-ENOENT when there are no more objects to list
> 
> int `rados_nobjects_list_next2`\([rados\_list\_ctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_list_ctx_t") _ctx_, _const_ char \*\*_entry_, _const_ char \*\*_key_, _const_ char \*\*_nspace_, size\_t \*_entry\_size_, size\_t \*_key\_size_, size\_t \*_nspace\_size_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Get the next object name, locator and their sizes in the pool
> 
> The sizes allow to list objects with 0 \(the NUL character\) in .e.g _entry. Is is unusual see such object names but a bug in a client has risen the need to handle them as well. \*entry and \*key are valid until next call to rados\_nobjects\_list\__
> 
> Parameters
> * **ctx** – iterator marking where you are in the listing
> * **entry** – where to store the name of the entry
> * **key** – where to store the object locator \(set to NULL to ignore\)
> * **nspace** – where to store the object namespace \(set to NULL to ignore\)
> * **entry\_size** – where to store the size of name of the entry
> * **key\_size** – where to store the size of object locator \(set to NULL to ignore\)
> * **nspace\_size** – where to store the size of object namespace \(set to NULL to ignore\)
> Returns0 on success, negative error code on failure
> 
> Returns\-ENOENT when there are no more objects to list
> 
> void `rados_nobjects_list_close`\([rados\_list\_ctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_list_ctx_t") _ctx_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Close the object listing handle.
> 
> This should be called when the handle is no longer needed. The handle should not be used after it has been closed.
> 
> Parameters
> * **ctx** – the handle to close
> [rados\_object\_list\_cursor](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_object_list_cursor") `rados_object_list_begin`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Get cursor handle pointing to the _beginning_ of a pool.
> 
> This is an opaque handle pointing to the start of a pool. It must be released with rados\_object\_list\_cursor\_free\(\).
> 
> Parameters
> * **io** – ioctx for the pool
> Returnshandle for the pool, NULL on error \(pool does not exist\)
> 
> [rados\_object\_list\_cursor](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_object_list_cursor") `rados_object_list_end`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Get cursor handle pointing to the _end_ of a pool.
> 
> This is an opaque handle pointing to the start of a pool. It must be released with rados\_object\_list\_cursor\_free\(\).
> 
> Parameters
> * **io** – ioctx for the pool
> Returnshandle for the pool, NULL on error \(pool does not exist\)
> 
> int `rados_object_list_is_end`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, [rados\_object\_list\_cursor](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_object_list_cursor") _cur_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Check if a cursor has reached the end of a pool
> 
> Parameters
> * **io** – ioctx
> * **cur** – cursor
> Returns1 if the cursor has reached the end of the pool, 0 otherwise
> 
> void `rados_object_list_cursor_free`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, [rados\_object\_list\_cursor](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_object_list_cursor") _cur_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Release a cursor
> 
> Release a cursor. The handle may not be used after this point.
> 
> Parameters
> * **io** – ioctx
> * **cur** – cursor
> int `rados_object_list_cursor_cmp`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, [rados\_object\_list\_cursor](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_object_list_cursor") _lhs_, [rados\_object\_list\_cursor](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_object_list_cursor") _rhs_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Compare two cursor positions
> 
> Compare two cursors, and indicate whether the first cursor precedes, matches, or follows the second.
> 
> Parameters
> * **io** – ioctx
> * **lhs** – first cursor
> * **rhs** – second cursor
> Returns\-1, 0, or 1 for lhs \< rhs, lhs == rhs, or lhs \> rhs
> 
> int `rados_object_list`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ [rados\_object\_list\_cursor](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_object_list_cursor") _start_, _const_ [rados\_object\_list\_cursor](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_object_list_cursor") _finish_, _const_ size\_t _result\_size_, _const_ char \*_filter\_buf_, _const_ size\_t _filter\_buf\_len_, [rados\_object\_list\_item](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_object_list_item") \*_results_, [rados\_object\_list\_cursor](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_object_list_cursor") \*_next_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Returnsthe number of items set in the results array
> 
> void `rados_object_list_free`\(_const_ size\_t _result\_size_, [rados\_object\_list\_item](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_object_list_item") \*_results_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> void `rados_object_list_slice`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ [rados\_object\_list\_cursor](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_object_list_cursor") _start_, _const_ [rados\_object\_list\_cursor](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_object_list_cursor") _finish_, _const_ size\_t _n_, _const_ size\_t _m_, [rados\_object\_list\_cursor](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_object_list_cursor") \*_split\_start_, [rados\_object\_list\_cursor](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_object_list_cursor") \*_split\_finish_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Obtain cursors delineating a subset of a range. Use this when you want to split up the work of iterating over the global namespace. Expected use case is when you are iterating in parallel, with `m` workers, and each worker taking an id `n`.
> 
> Parameters
> * **io** – ioctx
> * **start** – start of the range to be sliced up \(inclusive\)
> * **finish** – end of the range to be sliced up \(exclusive\)
> * **n** – which of the m chunks you would like to get cursors for
> * **m** – how many chunks to divide start\-finish into
> * **split\_start** – cursor populated with start of the subrange \(inclusive\)
> * **split\_finish** – cursor populated with end of the subrange \(exclusive\)
> 
> Snapshots
> 
> RADOS snapshots are based upon sequence numbers that form a snapshot context. They are pool\-specific. The snapshot context consists of the current snapshot sequence number for a pool, and an array of sequence numbers at which snapshots were taken, in descending order. Whenever a snapshot is created or deleted, the snapshot sequence number for the pool is increased. To add a new snapshot, the new snapshot sequence number must be increased and added to the snapshot context.
> 
> There are two ways to manage these snapshot contexts:
> 
> 
> 
> 1. within the RADOS cluster These are called pool snapshots, and store the snapshot context in the OSDMap. These represent a snapshot of all the objects in a pool.
> 2. within the RADOS clients These are called self\-managed snapshots, and push the responsibility for keeping track of the snapshot context to the clients. For every write, the client must send the snapshot context. In librados, this is accomplished with rados\_selfmanaged\_snap\_set\_write\_ctx\(\). These are more difficult to manage, but are restricted to specific objects instead of applying to an entire pool.
> 
> 
> int `rados_ioctx_snap_create`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_snapname_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Create a pool\-wide snapshot
> 
> Parameters
> * **io** – the pool to snapshot
> * **snapname** – the name of the snapshot
> Returns0 on success, negative error code on failure
> 
> int `rados_ioctx_snap_remove`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_snapname_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Delete a pool snapshot
> 
> Parameters
> * **io** – the pool to delete the snapshot from
> * **snapname** – which snapshot to delete
> Returns0 on success, negative error code on failure
> 
> int `rados_ioctx_snap_rollback`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_oid_, _const_ char \*_snapname_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Rollback an object to a pool snapshot
> 
> The contents of the object will be the same as when the snapshot was taken.
> 
> Parameters
> * **io** – the pool in which the object is stored
> * **oid** – the name of the object to rollback
> * **snapname** – which snapshot to rollback to
> Returns0 on success, negative error code on failure
> 
> int `rados_rollback`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_oid_, _const_ char \*_snapname_\) \_\_attribute\_\_\(\(deprecated\)\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Warning:
> 
> Deprecated: Use rados\_ioctx\_snap\_rollback\(\) instead
> 
> 
> void `rados_ioctx_snap_set_read`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, [rados\_snap\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_snap_t") _snap_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Set the snapshot from which reads are performed.
> 
> Subsequent reads will return data as it was at the time of that snapshot.
> 
> Parameters
> * **io** – the io context to change
> * **snap** – the id of the snapshot to set, or LIBRADOS\_SNAP\_HEAD for no snapshot \(i.e. normal operation\)
> int `rados_ioctx_selfmanaged_snap_create`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, [rados\_snap\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_snap_t") \*_snapid_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Allocate an ID for a self\-managed snapshot
> 
> Get a unique ID to put in the snaphot context to create a snapshot. A clone of an object is not created until a write with the new snapshot context is completed.
> 
> Parameters
> * **io** – the pool in which the snapshot will exist
> * **snapid** – where to store the newly allocated snapshot ID
> Returns0 on success, negative error code on failure
> 
> void `rados_aio_ioctx_selfmanaged_snap_create`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, [rados\_snap\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_snap_t") \*_snapid_, [rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _completion_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> int `rados_ioctx_selfmanaged_snap_remove`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, [rados\_snap\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_snap_t") _snapid_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Remove a self\-managed snapshot
> 
> This increases the snapshot sequence number, which will cause snapshots to be removed lazily.
> 
> Parameters
> * **io** – the pool in which the snapshot will exist
> * **snapid** – where to store the newly allocated snapshot ID
> Returns0 on success, negative error code on failure
> 
> void `rados_aio_ioctx_selfmanaged_snap_remove`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, [rados\_snap\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_snap_t") _snapid_, [rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _completion_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> int `rados_ioctx_selfmanaged_snap_rollback`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_oid_, [rados\_snap\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_snap_t") _snapid_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Rollback an object to a self\-managed snapshot
> 
> The contents of the object will be the same as when the snapshot was taken.
> 
> Parameters
> * **io** – the pool in which the object is stored
> * **oid** – the name of the object to rollback
> * **snapid** – which snapshot to rollback to
> Returns0 on success, negative error code on failure
> 
> int `rados_ioctx_selfmanaged_snap_set_write_ctx`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, [rados\_snap\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_snap_t") _seq_, [rados\_snap\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_snap_t") \*_snaps_, int _num\_snaps_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Set the snapshot context for use when writing to objects
> 
> This is stored in the io context, and applies to all future writes.
> 
> Parameters
> * **io** – the io context to change
> * **seq** – the newest snapshot sequence number for the pool
> * **snaps** – array of snapshots in sorted by descending id
> * **num\_snaps** – how many snaphosts are in the snaps array
> Returns0 on success, negative error code on failure
> 
> Returns\-EINVAL if snaps are not in descending order
> 
> int `rados_ioctx_snap_list`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, [rados\_snap\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_snap_t") \*_snaps_, int _maxlen_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> List all the ids of pool snapshots
> 
> If the output array does not have enough space to fit all the snapshots, \-ERANGE is returned and the caller should retry with a larger array.
> 
> Parameters
> * **io** – the pool to read from
> * **snaps** – where to store the results
> * **maxlen** – the number of rados\_snap\_t that fit in the snaps array
> Returnsnumber of snapshots on success, negative error code on failure
> 
> Returns\-ERANGE is returned if the snaps array is too short
> 
> int `rados_ioctx_snap_lookup`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_name_, [rados\_snap\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_snap_t") \*_id_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Get the id of a pool snapshot
> 
> Parameters
> * **io** – the pool to read from
> * **name** – the snapshot to find
> * **id** – where to store the result
> Returns0 on success, negative error code on failure
> 
> int `rados_ioctx_snap_get_name`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, [rados\_snap\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_snap_t") _id_, char \*_name_, int _maxlen_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Get the name of a pool snapshot
> 
> Parameters
> * **io** – the pool to read from
> * **id** – the snapshot to find
> * **name** – where to store the result
> * **maxlen** – the size of the name array
> Returns0 on success, negative error code on failure
> 
> Returns\-ERANGE if the name array is too small
> 
> int `rados_ioctx_snap_get_stamp`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, [rados\_snap\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_snap_t") _id_, time\_t \*_t_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Find when a pool snapshot occurred
> 
> Parameters
> * **io** – the pool the snapshot was taken in
> * **id** – the snapshot to lookup
> * **t** – where to store the result
> Returns0 on success, negative error code on failure
> 
> 
> Synchronous I/O
> 
> Writes are replicated to a number of OSDs based on the configuration of the pool they are in. These write functions block until data is in memory on all replicas of the object they’re writing to \- they are equivalent to doing the corresponding asynchronous write, and the calling rados\_ioctx\_wait\_for\_complete\(\). For greater data safety, use the asynchronous functions and rados\_aio\_wait\_for\_safe\(\).
> 
> uint64\_t `rados_get_last_version`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Return the version of the last object read or written to.
> 
> This exposes the internal version number of the last object read or written via this io context
> 
> Parameters
> * **io** – the io context to check
> Returnslast read or written object version
> 
> int `rados_write`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_oid_, _const_ char \*_buf_, size\_t _len_, uint64\_t _off_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Write _len_ bytes from _buf_ into the _oid_ object, starting at offset _off_. The value of _len_ must be \<= UINT\_MAX/2.
> 
> Note:
> 
> This will never return a positive value not equal to len.
> 
> 
> Parameters
> * **io** – the io context in which the write will occur
> * **oid** – name of the object
> * **buf** – data to write
> * **len** – length of the data, in bytes
> * **off** – byte offset in the object to begin writing at
> Returns0 on success, negative error code on failure
> 
> int `rados_write_full`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_oid_, _const_ char \*_buf_, size\_t _len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Write _len_ bytes from _buf_ into the _oid_ object. The value of _len_ must be \<= UINT\_MAX/2.
> 
> The object is filled with the provided data. If the object exists, it is atomically truncated and then written.
> 
> Parameters
> * **io** – the io context in which the write will occur
> * **oid** – name of the object
> * **buf** – data to write
> * **len** – length of the data, in bytes
> Returns0 on success, negative error code on failure
> 
> int `rados_writesame`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_oid_, _const_ char \*_buf_, size\_t _data\_len_, size\_t _write\_len_, uint64\_t _off_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Write the same _data\_len_ bytes from _buf_ multiple times into the _oid_ object. _write\_len_ bytes are written in total, which must be a multiple of _data\_len_. The value of _write\_len_ and _data\_len_ must be \<= UINT\_MAX/2.
> 
> Parameters
> * **io** – the io context in which the write will occur
> * **oid** – name of the object
> * **buf** – data to write
> * **data\_len** – length of the data, in bytes
> * **write\_len** – the total number of bytes to write
> * **off** – byte offset in the object to begin writing at
> Returns0 on success, negative error code on failure
> 
> int `rados_append`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_oid_, _const_ char \*_buf_, size\_t _len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Append _len_ bytes from _buf_ into the _oid_ object. The value of _len_ must be \<= UINT\_MAX/2.
> 
> Parameters
> * **io** – the context to operate in
> * **oid** – the name of the object
> * **buf** – the data to append
> * **len** – length of buf \(in bytes\)
> Returns0 on success, negative error code on failure
> 
> int `rados_read`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_oid_, char \*_buf_, size\_t _len_, uint64\_t _off_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Read data from an object
> 
> The io context determines the snapshot to read from, if any was set by rados\_ioctx\_snap\_set\_read\(\).
> 
> Parameters
> * **io** – the context in which to perform the read
> * **oid** – the name of the object to read from
> * **buf** – where to store the results
> * **len** – the number of bytes to read
> * **off** – the offset to start reading from in the object
> Returnsnumber of bytes read on success, negative error code on failure
> 
> int `rados_checksum`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_oid_, [rados\_checksum\_type\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_checksum_type_t") _type_, _const_ char \*_init\_value_, size\_t _init\_value\_len_, size\_t _len_, uint64\_t _off_, size\_t _chunk\_size_, char \*_pchecksum_, size\_t _checksum\_len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Compute checksum from object data
> 
> The io context determines the snapshot to checksum, if any was set by rados\_ioctx\_snap\_set\_read\(\). The length of the init\_value and resulting checksum are dependent upon the checksum type:
> 
> XXHASH64: le64 XXHASH32: le32 CRC32C: le32
> 
> The checksum result is encoded the following manner:
> 
> le32 num\_checksum\_chunks { leXX checksum for chunk \(where XX = appropriate size for the checksum type\) } \* num\_checksum\_chunks
> 
> Parameters
> * **io** – the context in which to perform the checksum
> * **oid** – the name of the object to checksum
> * **type** – the checksum algorithm to utilize
> * **init\_value** – the init value for the algorithm
> * **init\_value\_len** – the length of the init value
> * **len** – the number of bytes to checksum
> * **off** – the offset to start checksumming in the object
> * **chunk\_size** – optional length\-aligned chunk size for checksums
> * **pchecksum** – where to store the checksum result
> * **checksum\_len** – the number of bytes available for the result
> Returnsnegative error code on failure
> 
> int `rados_remove`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_oid_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Delete an object
> 
> Note:
> 
> This does not delete any snapshots of the object.
> 
> 
> Parameters
> * **io** – the pool to delete the object from
> * **oid** – the name of the object to delete
> Returns0 on success, negative error code on failure
> 
> int `rados_trunc`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_oid_, uint64\_t _size_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Resize an object
> 
> If this enlarges the object, the new area is logically filled with zeroes. If this shrinks the object, the excess data is removed.
> 
> Parameters
> * **io** – the context in which to truncate
> * **oid** – the name of the object
> * **size** – the new size of the object in bytes
> Returns0 on success, negative error code on failure
> 
> int `rados_cmpext`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_o_, _const_ char \*_cmp\_buf_, size\_t _cmp\_len_, uint64\_t _off_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Compare an on\-disk object range with a buffer
> 
> Parameters
> * **io** – the context in which to perform the comparison
> * **o** – name of the object
> * **cmp\_buf** – buffer containing bytes to be compared with object contents
> * **cmp\_len** – length to compare and size of `cmp_buf` in bytes
> * **off** – object byte offset at which to start the comparison
> Returns0 on success, negative error code on failure, \(\-MAX\_ERRNO \- mismatch\_off\) on mismatch
> 
> 
> Xattrs
> 
> Extended attributes are stored as extended attributes on the files representing an object on the OSDs. Thus, they have the same limitations as the underlying filesystem. On ext4, this means that the total data stored in xattrs cannot exceed 4KB.
> 
> int `rados_getxattr`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_o_, _const_ char \*_name_, char \*_buf_, size\_t _len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Get the value of an extended attribute on an object.
> 
> Parameters
> * **io** – the context in which the attribute is read
> * **o** – name of the object
> * **name** – which extended attribute to read
> * **buf** – where to store the result
> * **len** – size of buf in bytes
> Returnslength of xattr value on success, negative error code on failure
> 
> int `rados_setxattr`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_o_, _const_ char \*_name_, _const_ char \*_buf_, size\_t _len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Set an extended attribute on an object.
> 
> Parameters
> * **io** – the context in which xattr is set
> * **o** – name of the object
> * **name** – which extended attribute to set
> * **buf** – what to store in the xattr
> * **len** – the number of bytes in buf
> Returns0 on success, negative error code on failure
> 
> int `rados_rmxattr`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_o_, _const_ char \*_name_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Delete an extended attribute from an object.
> 
> Parameters
> * **io** – the context in which to delete the xattr
> * **o** – the name of the object
> * **name** – which xattr to delete
> Returns0 on success, negative error code on failure
> 
> int `rados_getxattrs`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_oid_, [rados\_xattrs\_iter\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_xattrs_iter_t") \*_iter_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Start iterating over xattrs on an object.
> 
> Parameters
> * **io** – the context in which to list xattrs
> * **oid** – name of the object
> * **iter** – where to store the iterator
> Postiter is a valid iterator
> 
> Returns0 on success, negative error code on failure
> 
> int `rados_getxattrs_next`\([rados\_xattrs\_iter\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_xattrs_iter_t") _iter_, _const_ char \*\*_name_, _const_ char \*\*_val_, size\_t \*_len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Get the next xattr on the object
> 
> Parameters
> * **iter** – iterator to advance
> * **name** – where to store the name of the next xattr
> * **val** – where to store the value of the next xattr
> * **len** – the number of bytes in val
> Preiter is a valid iterator
> 
> Postname is the NULL\-terminated name of the next xattr, and val contains the value of the xattr, which is of length len. If the end of the list has been reached, name and val are NULL, and len is 0.
> 
> Returns0 on success, negative error code on failure
> 
> void `rados_getxattrs_end`\([rados\_xattrs\_iter\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_xattrs_iter_t") _iter_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Close the xattr iterator.
> 
> iter should not be used after this is called.
> 
> Parameters
> * **iter** – the iterator to close
> 
> Asynchronous Xattrs
> 
> Extended attributes are stored as extended attributes on the files representing an object on the OSDs. Thus, they have the same limitations as the underlying filesystem. On ext4, this means that the total data stored in xattrs cannot exceed 4KB.
> 
> int `rados_aio_getxattr`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_o_, [rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _completion_, _const_ char \*_name_, char \*_buf_, size\_t _len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Asynchronously get the value of an extended attribute on an object.
> 
> Parameters
> * **io** – the context in which the attribute is read
> * **o** – name of the object
> * **completion** – what to do when the getxattr completes
> * **name** – which extended attribute to read
> * **buf** – where to store the result
> * **len** – size of buf in bytes
> Returnslength of xattr value on success, negative error code on failure
> 
> int `rados_aio_setxattr`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_o_, [rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _completion_, _const_ char \*_name_, _const_ char \*_buf_, size\_t _len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Asynchronously set an extended attribute on an object.
> 
> Parameters
> * **io** – the context in which xattr is set
> * **o** – name of the object
> * **completion** – what to do when the setxattr completes
> * **name** – which extended attribute to set
> * **buf** – what to store in the xattr
> * **len** – the number of bytes in buf
> Returns0 on success, negative error code on failure
> 
> int `rados_aio_rmxattr`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_o_, [rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _completion_, _const_ char \*_name_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Asynchronously delete an extended attribute from an object.
> 
> Parameters
> * **io** – the context in which to delete the xattr
> * **o** – the name of the object
> * **completion** – what to do when the rmxattr completes
> * **name** – which xattr to delete
> Returns0 on success, negative error code on failure
> 
> int `rados_aio_getxattrs`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_oid_, [rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _completion_, [rados\_xattrs\_iter\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_xattrs_iter_t") \*_iter_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Asynchronously start iterating over xattrs on an object.
> 
> Parameters
> * **io** – the context in which to list xattrs
> * **oid** – name of the object
> * **completion** – what to do when the getxattrs completes
> * **iter** – where to store the iterator
> Postiter is a valid iterator
> 
> Returns0 on success, negative error code on failure
> 
> 
> Hints
> 
> int `rados_set_alloc_hint`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_o_, uint64\_t _expected\_object\_size_, uint64\_t _expected\_write\_size_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Set allocation hint for an object
> 
> This is an advisory operation, it will always succeed \(as if it was submitted with a LIBRADOS\_OP\_FLAG\_FAILOK flag set\) and is not guaranteed to do anything on the backend.
> 
> Parameters
> * **io** – the pool the object is in
> * **o** – the name of the object
> * **expected\_object\_size** – expected size of the object, in bytes
> * **expected\_write\_size** – expected size of writes to the object, in bytes
> Returns0 on success, negative error code on failure
> 
> int `rados_set_alloc_hint2`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_o_, uint64\_t _expected\_object\_size_, uint64\_t _expected\_write\_size_, uint32\_t _flags_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Set allocation hint for an object
> 
> This is an advisory operation, it will always succeed \(as if it was submitted with a LIBRADOS\_OP\_FLAG\_FAILOK flag set\) and is not guaranteed to do anything on the backend.
> 
> Parameters
> * **io** – the pool the object is in
> * **o** – the name of the object
> * **expected\_object\_size** – expected size of the object, in bytes
> * **expected\_write\_size** – expected size of writes to the object, in bytes
> * **flags** – hints about future IO patterns
> Returns0 on success, negative error code on failure
> 
> 
> Object Operations
> 
> A single rados operation can do multiple operations on one object atomically. The whole operation will succeed or fail, and no partial results will be visible.
> 
> Operations may be either reads, which can return data, or writes, which cannot. The effects of writes are applied and visible all at once, so an operation that sets an xattr and then checks its value will not see the updated value.
> 
> [rados\_write\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_write_op_t") `rados_create_write_op`\(void\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Create a new rados\_write\_op\_t write operation. This will store all actions to be performed atomically. You must call rados\_release\_write\_op when you are finished with it.
> 
> Note:
> 
> the ownership of a write operartion is passed to the function performing the operation, so the same instance of `rados_write_op_t` cannot be used again after being performed.
> 
> 
> Returnsnon\-NULL on success, NULL on memory allocation error.
> 
> void `rados_release_write_op`\([rados\_write\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_write_op_t") _write\_op_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Free a rados\_write\_op\_t, must be called when you’re done with it.
> 
> Parameters
> * **write\_op** – operation to deallocate, created with rados\_create\_write\_op
> void `rados_write_op_set_flags`\([rados\_write\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_write_op_t") _write\_op_, int _flags_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Set flags for the last operation added to this write\_op. At least one op must have been added to the write\_op.
> 
> Parameters
> * **write\_op** – operation to add this action to
> * **flags** – see librados.h constants beginning with LIBRADOS\_OP\_FLAG
> void `rados_write_op_assert_exists`\([rados\_write\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_write_op_t") _write\_op_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Ensure that the object exists before writing
> 
> Parameters
> * **write\_op** – operation to add this action to
> void `rados_write_op_assert_version`\([rados\_write\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_write_op_t") _write\_op_, uint64\_t _ver_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Ensure that the object exists and that its internal version number is equal to “ver” before writing. “ver” should be a version number previously obtained with rados\_get\_last\_version\(\).
> 
> 
> 
> * If the object’s version is greater than the asserted version then rados\_write\_op\_operate will return \-ERANGE instead of executing the op.
> * If the object’s version is less than the asserted version then rados\_write\_op\_operate will return \-EOVERFLOW instead of executing the op.
> 
> 
> Parameters
> * **write\_op** – operation to add this action to
> * **ver** – object version number
> void `rados_write_op_cmpext`\([rados\_write\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_write_op_t") _write\_op_, _const_ char \*_cmp\_buf_, size\_t _cmp\_len_, uint64\_t _off_, int \*_prval_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Ensure that given object range \(extent\) satisfies comparison.
> 
> Parameters
> * **write\_op** – operation to add this action to
> * **cmp\_buf** – buffer containing bytes to be compared with object contents
> * **cmp\_len** – length to compare and size of `cmp_buf` in bytes
> * **off** – object byte offset at which to start the comparison
> * **prval** – returned result of comparison, 0 on success, negative error code on failure, \(\-MAX\_ERRNO \- mismatch\_off\) on mismatch
> void `rados_write_op_cmpxattr`\([rados\_write\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_write_op_t") _write\_op_, _const_ char \*_name_, uint8\_t _comparison\_operator_, _const_ char \*_value_, size\_t _value\_len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Ensure that given xattr satisfies comparison. If the comparison is not satisfied, the return code of the operation will be \-ECANCELED
> 
> Parameters
> * **write\_op** – operation to add this action to
> * **name** – name of the xattr to look up
> * **comparison\_operator** – currently undocumented, look for LIBRADOS\_CMPXATTR\_OP\_EQ in librados.h
> * **value** – buffer to compare actual xattr value to
> * **value\_len** – length of buffer to compare actual xattr value to
> void `rados_write_op_omap_cmp`\([rados\_write\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_write_op_t") _write\_op_, _const_ char \*_key_, uint8\_t _comparison\_operator_, _const_ char \*_val_, size\_t _val\_len_, int \*_prval_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Ensure that the an omap value satisfies a comparison, with the supplied value on the right hand side \(i.e. for OP\_LT, the comparison is actual\_value \< value.
> 
> Parameters
> * **write\_op** – operation to add this action to
> * **key** – which omap value to compare
> * **comparison\_operator** – one of LIBRADOS\_CMPXATTR\_OP\_EQ, LIBRADOS\_CMPXATTR\_OP\_LT, or LIBRADOS\_CMPXATTR\_OP\_GT
> * **val** – value to compare with
> * **val\_len** – length of value in bytes
> * **prval** – where to store the return value from this action
> void `rados_write_op_omap_cmp2`\([rados\_write\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_write_op_t") _write\_op_, _const_ char \*_key_, uint8\_t _comparison\_operator_, _const_ char \*_val_, size\_t _key\_len_, size\_t _val\_len_, int \*_prval_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Ensure that the an omap value satisfies a comparison, with the supplied value on the right hand side \(i.e. for OP\_LT, the comparison is actual\_value \< value.
> 
> Parameters
> * **write\_op** – operation to add this action to
> * **key** – which omap value to compare
> * **comparison\_operator** – one of LIBRADOS\_CMPXATTR\_OP\_EQ, LIBRADOS\_CMPXATTR\_OP\_LT, or LIBRADOS\_CMPXATTR\_OP\_GT
> * **val** – value to compare with
> * **key\_len** – length of key in bytes
> * **val\_len** – length of value in bytes
> * **prval** – where to store the return value from this action
> void `rados_write_op_setxattr`\([rados\_write\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_write_op_t") _write\_op_, _const_ char \*_name_, _const_ char \*_value_, size\_t _value\_len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Set an xattr
> 
> Parameters
> * **write\_op** – operation to add this action to
> * **name** – name of the xattr
> * **value** – buffer to set xattr to
> * **value\_len** – length of buffer to set xattr to
> void `rados_write_op_rmxattr`\([rados\_write\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_write_op_t") _write\_op_, _const_ char \*_name_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Remove an xattr
> 
> Parameters
> * **write\_op** – operation to add this action to
> * **name** – name of the xattr to remove
> void `rados_write_op_create`\([rados\_write\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_write_op_t") _write\_op_, int _exclusive_, _const_ char \*_category_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Create the object
> 
> Parameters
> * **write\_op** – operation to add this action to
> * **exclusive** – set to either LIBRADOS\_CREATE\_EXCLUSIVE or LIBRADOS\_CREATE\_IDEMPOTENT will error if the object already exists.
> * **category** – category string \(DEPRECATED, HAS NO EFFECT\)
> void `rados_write_op_write`\([rados\_write\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_write_op_t") _write\_op_, _const_ char \*_buffer_, size\_t _len_, uint64\_t _offset_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Write to offset
> 
> Parameters
> * **write\_op** – operation to add this action to
> * **offset** – offset to write to
> * **buffer** – bytes to write
> * **len** – length of buffer
> void `rados_write_op_write_full`\([rados\_write\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_write_op_t") _write\_op_, _const_ char \*_buffer_, size\_t _len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Write whole object, atomically replacing it.
> 
> Parameters
> * **write\_op** – operation to add this action to
> * **buffer** – bytes to write
> * **len** – length of buffer
> void `rados_write_op_writesame`\([rados\_write\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_write_op_t") _write\_op_, _const_ char \*_buffer_, size\_t _data\_len_, size\_t _write\_len_, uint64\_t _offset_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Write the same buffer multiple times
> 
> Parameters
> * **write\_op** – operation to add this action to
> * **buffer** – bytes to write
> * **data\_len** – length of buffer
> * **write\_len** – total number of bytes to write, as a multiple of `data_len`
> * **offset** – offset to write to
> void `rados_write_op_append`\([rados\_write\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_write_op_t") _write\_op_, _const_ char \*_buffer_, size\_t _len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Append to end of object.
> 
> Parameters
> * **write\_op** – operation to add this action to
> * **buffer** – bytes to write
> * **len** – length of buffer
> void `rados_write_op_remove`\([rados\_write\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_write_op_t") _write\_op_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Remove object
> 
> Parameters
> * **write\_op** – operation to add this action to
> void `rados_write_op_truncate`\([rados\_write\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_write_op_t") _write\_op_, uint64\_t _offset_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Truncate an object
> 
> Parameters
> * **write\_op** – operation to add this action to
> * **offset** – Offset to truncate to
> void `rados_write_op_zero`\([rados\_write\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_write_op_t") _write\_op_, uint64\_t _offset_, uint64\_t _len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Zero part of an object
> 
> Parameters
> * **write\_op** – operation to add this action to
> * **offset** – Offset to zero
> * **len** – length to zero
> void `rados_write_op_exec`\([rados\_write\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_write_op_t") _write\_op_, _const_ char \*_cls_, _const_ char \*_method_, _const_ char \*_in\_buf_, size\_t _in\_len_, int \*_prval_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Execute an OSD class method on an object See rados\_exec\(\) for general description.
> 
> Parameters
> * **write\_op** – operation to add this action to
> * **cls** – the name of the class
> * **method** – the name of the method
> * **in\_buf** – where to find input
> * **in\_len** – length of in\_buf in bytes
> * **prval** – where to store the return value from the method
> void `rados_write_op_omap_set`\([rados\_write\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_write_op_t") _write\_op_, char _const_ \*_const_ \*_keys_, char _const_ \*_const_ \*_vals_, _const_ size\_t \*_lens_, size\_t _num_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Set key/value pairs on an object
> 
> Parameters
> * **write\_op** – operation to add this action to
> * **keys** – array of null\-terminated char arrays representing keys to set
> * **vals** – array of pointers to values to set
> * **lens** – array of lengths corresponding to each value
> * **num** – number of key/value pairs to set
> void `rados_write_op_omap_set2`\([rados\_write\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_write_op_t") _write\_op_, char _const_ \*_const_ \*_keys_, char _const_ \*_const_ \*_vals_, _const_ size\_t \*_key\_lens_, _const_ size\_t \*_val\_lens_, size\_t _num_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Set key/value pairs on an object
> 
> Parameters
> * **write\_op** – operation to add this action to
> * **keys** – array of null\-terminated char arrays representing keys to set
> * **vals** – array of pointers to values to set
> * **key\_lens** – array of lengths corresponding to each key
> * **val\_lens** – array of lengths corresponding to each value
> * **num** – number of key/value pairs to set
> void `rados_write_op_omap_rm_keys`\([rados\_write\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_write_op_t") _write\_op_, char _const_ \*_const_ \*_keys_, size\_t _keys\_len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Remove key/value pairs from an object
> 
> Parameters
> * **write\_op** – operation to add this action to
> * **keys** – array of null\-terminated char arrays representing keys to remove
> * **keys\_len** – number of key/value pairs to remove
> void `rados_write_op_omap_rm_keys2`\([rados\_write\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_write_op_t") _write\_op_, char _const_ \*_const_ \*_keys_, _const_ size\_t \*_key\_lens_, size\_t _keys\_len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Remove key/value pairs from an object
> 
> Parameters
> * **write\_op** – operation to add this action to
> * **keys** – array of char arrays representing keys to remove
> * **key\_lens** – array of size\_t values representing length of each key
> * **keys\_len** – number of key/value pairs to remove
> void `rados_write_op_omap_rm_range2`\([rados\_write\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_write_op_t") _write\_op_, _const_ char \*_key\_begin_, size\_t _key\_begin\_len_, _const_ char \*_key\_end_, size\_t _key\_end\_len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Remove key/value pairs from an object whose keys are in the range \[key\_begin, key\_end\)
> 
> Parameters
> * **write\_op** – operation to add this action to
> * **key\_begin** – the lower bound of the key range to remove
> * **key\_begin\_len** – length of key\_begin
> * **key\_end** – the upper bound of the key range to remove
> * **key\_end\_len** – length of key\_end
> void `rados_write_op_omap_clear`\([rados\_write\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_write_op_t") _write\_op_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Remove all key/value pairs from an object
> 
> Parameters
> * **write\_op** – operation to add this action to
> void `rados_write_op_set_alloc_hint`\([rados\_write\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_write_op_t") _write\_op_, uint64\_t _expected\_object\_size_, uint64\_t _expected\_write\_size_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Set allocation hint for an object
> 
> Parameters
> * **write\_op** – operation to add this action to
> * **expected\_object\_size** – expected size of the object, in bytes
> * **expected\_write\_size** – expected size of writes to the object, in bytes
> void `rados_write_op_set_alloc_hint2`\([rados\_write\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_write_op_t") _write\_op_, uint64\_t _expected\_object\_size_, uint64\_t _expected\_write\_size_, uint32\_t _flags_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Set allocation hint for an object
> 
> Parameters
> * **write\_op** – operation to add this action to
> * **expected\_object\_size** – expected size of the object, in bytes
> * **expected\_write\_size** – expected size of writes to the object, in bytes
> * **flags** – hints about future IO patterns
> int `rados_write_op_operate`\([rados\_write\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_write_op_t") _write\_op_, [rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_oid_, time\_t \*_mtime_, int _flags_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Perform a write operation synchronously
> 
> Parameters
> * **write\_op** – operation to perform
> * **io** – the ioctx that the object is in
> * **oid** – the object id
> * **mtime** – the time to set the mtime to, NULL for the current time
> * **flags** – flags to apply to the entire operation \(LIBRADOS\_OPERATION\_\*\)
> int `rados_write_op_operate2`\([rados\_write\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_write_op_t") _write\_op_, [rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_oid_, _struct_ timespec \*_mtime_, int _flags_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Perform a write operation synchronously
> 
> Parameters
> * **write\_op** – operation to perform
> * **io** – the ioctx that the object is in
> * **oid** – the object id
> * **mtime** – the time to set the mtime to, NULL for the current time
> * **flags** – flags to apply to the entire operation \(LIBRADOS\_OPERATION\_\*\)
> int `rados_aio_write_op_operate`\([rados\_write\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_write_op_t") _write\_op_, [rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, [rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _completion_, _const_ char \*_oid_, time\_t \*_mtime_, int _flags_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Perform a write operation asynchronously
> 
> Parameters
> * **write\_op** – operation to perform
> * **io** – the ioctx that the object is in
> * **completion** – what to do when operation has been attempted
> * **oid** – the object id
> * **mtime** – the time to set the mtime to, NULL for the current time
> * **flags** – flags to apply to the entire operation \(LIBRADOS\_OPERATION\_\*\)
> [rados\_read\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_read_op_t") `rados_create_read_op`\(void\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Create a new rados\_read\_op\_t read operation. This will store all actions to be performed atomically. You must call rados\_release\_read\_op when you are finished with it \(after it completes, or you decide not to send it in the first place\).
> 
> Note:
> 
> the ownership of a read operartion is passed to the function performing the operation, so the same instance of `rados_read_op_t` cannot be used again after being performed.
> 
> 
> Returnsnon\-NULL on success, NULL on memory allocation error.
> 
> void `rados_release_read_op`\([rados\_read\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_read_op_t") _read\_op_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Free a rados\_read\_op\_t, must be called when you’re done with it.
> 
> Parameters
> * **read\_op** – operation to deallocate, created with rados\_create\_read\_op
> void `rados_read_op_set_flags`\([rados\_read\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_read_op_t") _read\_op_, int _flags_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Set flags for the last operation added to this read\_op. At least one op must have been added to the read\_op.
> 
> Parameters
> * **read\_op** – operation to add this action to
> * **flags** – see librados.h constants beginning with LIBRADOS\_OP\_FLAG
> void `rados_read_op_assert_exists`\([rados\_read\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_read_op_t") _read\_op_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Ensure that the object exists before reading
> 
> Parameters
> * **read\_op** – operation to add this action to
> void `rados_read_op_assert_version`\([rados\_read\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_read_op_t") _read\_op_, uint64\_t _ver_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Ensure that the object exists and that its internal version number is equal to “ver” before reading. “ver” should be a version number previously obtained with rados\_get\_last\_version\(\).
> 
> 
> 
> * If the object’s version is greater than the asserted version then rados\_read\_op\_operate will return \-ERANGE instead of executing the op.
> * If the object’s version is less than the asserted version then rados\_read\_op\_operate will return \-EOVERFLOW instead of executing the op.
> 
> 
> Parameters
> * **read\_op** – operation to add this action to
> * **ver** – object version number
> void `rados_read_op_cmpext`\([rados\_read\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_read_op_t") _read\_op_, _const_ char \*_cmp\_buf_, size\_t _cmp\_len_, uint64\_t _off_, int \*_prval_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Ensure that given object range \(extent\) satisfies comparison.
> 
> Parameters
> * **read\_op** – operation to add this action to
> * **cmp\_buf** – buffer containing bytes to be compared with object contents
> * **cmp\_len** – length to compare and size of `cmp_buf` in bytes
> * **off** – object byte offset at which to start the comparison
> * **prval** – returned result of comparison, 0 on success, negative error code on failure, \(\-MAX\_ERRNO \- mismatch\_off\) on mismatch
> void `rados_read_op_cmpxattr`\([rados\_read\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_read_op_t") _read\_op_, _const_ char \*_name_, uint8\_t _comparison\_operator_, _const_ char \*_value_, size\_t _value\_len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Ensure that the an xattr satisfies a comparison If the comparison is not satisfied, the return code of the operation will be \-ECANCELED
> 
> Parameters
> * **read\_op** – operation to add this action to
> * **name** – name of the xattr to look up
> * **comparison\_operator** – currently undocumented, look for LIBRADOS\_CMPXATTR\_OP\_EQ in librados.h
> * **value** – buffer to compare actual xattr value to
> * **value\_len** – length of buffer to compare actual xattr value to
> void `rados_read_op_getxattrs`\([rados\_read\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_read_op_t") _read\_op_, [rados\_xattrs\_iter\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_xattrs_iter_t") \*_iter_, int \*_prval_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Start iterating over xattrs on an object.
> 
> Parameters
> * **read\_op** – operation to add this action to
> * **iter** – where to store the iterator
> * **prval** – where to store the return value of this action
> void `rados_read_op_omap_cmp`\([rados\_read\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_read_op_t") _read\_op_, _const_ char \*_key_, uint8\_t _comparison\_operator_, _const_ char \*_val_, size\_t _val\_len_, int \*_prval_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Ensure that the an omap value satisfies a comparison, with the supplied value on the right hand side \(i.e. for OP\_LT, the comparison is actual\_value \< value.
> 
> Parameters
> * **read\_op** – operation to add this action to
> * **key** – which omap value to compare
> * **comparison\_operator** – one of LIBRADOS\_CMPXATTR\_OP\_EQ, LIBRADOS\_CMPXATTR\_OP\_LT, or LIBRADOS\_CMPXATTR\_OP\_GT
> * **val** – value to compare with
> * **val\_len** – length of value in bytes
> * **prval** – where to store the return value from this action
> void `rados_read_op_omap_cmp2`\([rados\_read\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_read_op_t") _read\_op_, _const_ char \*_key_, uint8\_t _comparison\_operator_, _const_ char \*_val_, size\_t _key\_len_, size\_t _val\_len_, int \*_prval_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Ensure that the an omap value satisfies a comparison, with the supplied value on the right hand side \(i.e. for OP\_LT, the comparison is actual\_value \< value.
> 
> Parameters
> * **read\_op** – operation to add this action to
> * **key** – which omap value to compare
> * **comparison\_operator** – one of LIBRADOS\_CMPXATTR\_OP\_EQ, LIBRADOS\_CMPXATTR\_OP\_LT, or LIBRADOS\_CMPXATTR\_OP\_GT
> * **val** – value to compare with
> * **key\_len** – length of key in bytes
> * **val\_len** – length of value in bytes
> * **prval** – where to store the return value from this action
> void `rados_read_op_stat`\([rados\_read\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_read_op_t") _read\_op_, uint64\_t \*_psize_, time\_t \*_pmtime_, int \*_prval_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Get object size and mtime
> 
> Parameters
> * **read\_op** – operation to add this action to
> * **psize** – where to store object size
> * **pmtime** – where to store modification time
> * **prval** – where to store the return value of this action
> void `rados_read_op_read`\([rados\_read\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_read_op_t") _read\_op_, uint64\_t _offset_, size\_t _len_, char \*_buffer_, size\_t \*_bytes\_read_, int \*_prval_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Read bytes from offset into buffer.
> 
> prlen will be filled with the number of bytes read if successful. A short read can only occur if the read reaches the end of the object.
> 
> Parameters
> * **read\_op** – operation to add this action to
> * **offset** – offset to read from
> * **len** – length of buffer
> * **buffer** – where to put the data
> * **bytes\_read** – where to store the number of bytes read by this action
> * **prval** – where to store the return value of this action
> void `rados_read_op_checksum`\([rados\_read\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_read_op_t") _read\_op_, [rados\_checksum\_type\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_checksum_type_t") _type_, _const_ char \*_init\_value_, size\_t _init\_value\_len_, uint64\_t _offset_, size\_t _len_, size\_t _chunk\_size_, char \*_pchecksum_, size\_t _checksum\_len_, int \*_prval_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Compute checksum from object data
> 
> Parameters
> * **read\_op** – operation to add this action to
> * **type** – the checksum algorithm to utilize
> * **init\_value** – the init value for the algorithm
> * **init\_value\_len** – the length of the init value
> * **offset** – the offset to start checksumming in the object
> * **len** – the number of bytes to checksum
> * **chunk\_size** – optional length\-aligned chunk size for checksums
> * **pchecksum** – where to store the checksum result for this action
> * **checksum\_len** – the number of bytes available for the result
> * **prval** – where to store the return value for this action
> void `rados_read_op_exec`\([rados\_read\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_read_op_t") _read\_op_, _const_ char \*_cls_, _const_ char \*_method_, _const_ char \*_in\_buf_, size\_t _in\_len_, char \*\*_out\_buf_, size\_t \*_out\_len_, int \*_prval_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Execute an OSD class method on an object See rados\_exec\(\) for general description.
> 
> The output buffer is allocated on the heap; the caller is expected to release that memory with rados\_buffer\_free\(\). The buffer and length pointers can all be NULL, in which case they are not filled in.
> 
> Parameters
> * **read\_op** – operation to add this action to
> * **cls** – the name of the class
> * **method** – the name of the method
> * **in\_buf** – where to find input
> * **in\_len** – length of in\_buf in bytes
> * **out\_buf** – where to put librados\-allocated output buffer
> * **out\_len** – length of out\_buf in bytes
> * **prval** – where to store the return value from the method
> void `rados_read_op_exec_user_buf`\([rados\_read\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_read_op_t") _read\_op_, _const_ char \*_cls_, _const_ char \*_method_, _const_ char \*_in\_buf_, size\_t _in\_len_, char \*_out\_buf_, size\_t _out\_len_, size\_t \*_used\_len_, int \*_prval_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Execute an OSD class method on an object See rados\_exec\(\) for general description.
> 
> If the output buffer is too small, prval will be set to \-ERANGE and used\_len will be 0.
> 
> Parameters
> * **read\_op** – operation to add this action to
> * **cls** – the name of the class
> * **method** – the name of the method
> * **in\_buf** – where to find input
> * **in\_len** – length of in\_buf in bytes
> * **out\_buf** – user\-provided buffer to read into
> * **out\_len** – length of out\_buf in bytes
> * **used\_len** – where to store the number of bytes read into out\_buf
> * **prval** – where to store the return value from the method
> void `rados_read_op_omap_get_vals`\([rados\_read\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_read_op_t") _read\_op_, _const_ char \*_start\_after_, _const_ char \*_filter\_prefix_, uint64\_t _max\_return_, [rados\_omap\_iter\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_omap_iter_t") \*_iter_, int \*_prval_\) \_\_attribute\_\_\(\(deprecated\)\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Start iterating over key/value pairs on an object.
> 
> They will be returned sorted by key.
> 
> Parameters
> * **read\_op** – operation to add this action to
> * **start\_after** – list keys starting after start\_after
> * **filter\_prefix** – list only keys beginning with filter\_prefix
> * **max\_return** – list no more than max\_return key/value pairs
> * **iter** – where to store the iterator
> * **prval** – where to store the return value from this action
> void `rados_read_op_omap_get_vals2`\([rados\_read\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_read_op_t") _read\_op_, _const_ char \*_start\_after_, _const_ char \*_filter\_prefix_, uint64\_t _max\_return_, [rados\_omap\_iter\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_omap_iter_t") \*_iter_, unsigned char \*_pmore_, int \*_prval_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Start iterating over key/value pairs on an object.
> 
> They will be returned sorted by key.
> 
> Parameters
> * **read\_op** – operation to add this action to
> * **start\_after** – list keys starting after start\_after
> * **filter\_prefix** – list only keys beginning with filter\_prefix
> * **max\_return** – list no more than max\_return key/value pairs
> * **iter** – where to store the iterator
> * **pmore** – flag indicating whether there are more keys to fetch
> * **prval** – where to store the return value from this action
> void `rados_read_op_omap_get_keys`\([rados\_read\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_read_op_t") _read\_op_, _const_ char \*_start\_after_, uint64\_t _max\_return_, [rados\_omap\_iter\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_omap_iter_t") \*_iter_, int \*_prval_\) \_\_attribute\_\_\(\(deprecated\)\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Start iterating over keys on an object.
> 
> They will be returned sorted by key, and the iterator will fill in NULL for all values if specified.
> 
> Parameters
> * **read\_op** – operation to add this action to
> * **start\_after** – list keys starting after start\_after
> * **max\_return** – list no more than max\_return keys
> * **iter** – where to store the iterator
> * **prval** – where to store the return value from this action
> void `rados_read_op_omap_get_keys2`\([rados\_read\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_read_op_t") _read\_op_, _const_ char \*_start\_after_, uint64\_t _max\_return_, [rados\_omap\_iter\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_omap_iter_t") \*_iter_, unsigned char \*_pmore_, int \*_prval_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Start iterating over keys on an object.
> 
> They will be returned sorted by key, and the iterator will fill in NULL for all values if specified.
> 
> Parameters
> * **read\_op** – operation to add this action to
> * **start\_after** – list keys starting after start\_after
> * **max\_return** – list no more than max\_return keys
> * **iter** – where to store the iterator
> * **pmore** – flag indicating whether there are more keys to fetch
> * **prval** – where to store the return value from this action
> void `rados_read_op_omap_get_vals_by_keys`\([rados\_read\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_read_op_t") _read\_op_, char _const_ \*_const_ \*_keys_, size\_t _keys\_len_, [rados\_omap\_iter\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_omap_iter_t") \*_iter_, int \*_prval_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Start iterating over specific key/value pairs
> 
> They will be returned sorted by key.
> 
> Parameters
> * **read\_op** – operation to add this action to
> * **keys** – array of pointers to null\-terminated keys to get
> * **keys\_len** – the number of strings in keys
> * **iter** – where to store the iterator
> * **prval** – where to store the return value from this action
> void `rados_read_op_omap_get_vals_by_keys2`\([rados\_read\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_read_op_t") _read\_op_, char _const_ \*_const_ \*_keys_, size\_t _num\_keys_, _const_ size\_t \*_key\_lens_, [rados\_omap\_iter\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_omap_iter_t") \*_iter_, int \*_prval_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Start iterating over specific key/value pairs
> 
> They will be returned sorted by key.
> 
> Parameters
> * **read\_op** – operation to add this action to
> * **keys** – array of pointers to keys to get
> * **num\_keys** – the number of strings in keys
> * **key\_lens** – array of size\_t’s describing each key len \(in bytes\)
> * **iter** – where to store the iterator
> * **prval** – where to store the return value from this action
> int `rados_read_op_operate`\([rados\_read\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_read_op_t") _read\_op_, [rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_oid_, int _flags_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Perform a read operation synchronously
> 
> Parameters
> * **read\_op** – operation to perform
> * **io** – the ioctx that the object is in
> * **oid** – the object id
> * **flags** – flags to apply to the entire operation \(LIBRADOS\_OPERATION\_\*\)
> int `rados_aio_read_op_operate`\([rados\_read\_op\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_read_op_t") _read\_op_, [rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, [rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _completion_, _const_ char \*_oid_, int _flags_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Perform a read operation asynchronously
> 
> Parameters
> * **read\_op** – operation to perform
> * **io** – the ioctx that the object is in
> * **completion** – what to do when operation has been attempted
> * **oid** – the object id
> * **flags** – flags to apply to the entire operation \(LIBRADOS\_OPERATION\_\*\)
> 
> Defines
> 
> `CEPH_OSD_TMAP_HDR`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> `CEPH_OSD_TMAP_SET`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> `CEPH_OSD_TMAP_CREATE`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> `CEPH_OSD_TMAP_RM`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> `LIBRADOS_VER_MAJOR`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> `LIBRADOS_VER_MINOR`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> `LIBRADOS_VER_EXTRA`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> `LIBRADOS_VERSION`\(_maj_, _min_, _extra_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> `LIBRADOS_VERSION_CODE`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> `LIBRADOS_SUPPORTS_WATCH`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> `LIBRADOS_SUPPORTS_SERVICES`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> `LIBRADOS_SUPPORTS_GETADDRS`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> `LIBRADOS_SUPPORTS_APP_METADATA`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> `LIBRADOS_LOCK_FLAG_RENEW`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> `LIBRADOS_LOCK_FLAG_MAY_RENEW`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> `LIBRADOS_LOCK_FLAG_MUST_RENEW`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> `LIBRADOS_CREATE_EXCLUSIVE`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> `LIBRADOS_CREATE_IDEMPOTENT`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> `CEPH_RADOS_API`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> `LIBRADOS_SNAP_HEAD`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> `LIBRADOS_SNAP_DIR`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> `VOIDPTR_RADOS_T`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> 
> Typedefs
> 
> _typedef_ void \*`rados_t`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> A handle for interacting with a RADOS cluster. It encapsulates all RADOS client configuration, including username, key for authentication, logging, and debugging. Talking to different clusters or to the same cluster with different users requires different cluster handles.
> 
> _typedef_ void \*`rados_config_t`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> A handle for the ceph configuration context for the rados\_t cluster instance. This can be used to share configuration context/state \(e.g., logging configuration\) between librados instance.
> 
> Warning:
> 
> The config context does not have independent reference counting. As such, a rados\_config\_t handle retrieved from a given rados\_t is only valid as long as that rados\_t.
> 
> 
> _typedef_ void \*`rados_ioctx_t`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> An io context encapsulates a few settings for all I/O operations done on it:
> 
> 
> 
> * pool \- set when the io context is created \(see rados\_ioctx\_create\(\)\)
> * snapshot context for writes \(see rados\_ioctx\_selfmanaged\_snap\_set\_write\_ctx\(\)\)
> * snapshot id to read from \(see rados\_ioctx\_snap\_set\_read\(\)\)
> * object locator for all single\-object operations \(see rados\_ioctx\_locator\_set\_key\(\)\)
> * namespace for all single\-object operations \(see rados\_ioctx\_set\_namespace\(\)\). Set to LIBRADOS\_ALL\_NSPACES before rados\_nobjects\_list\_open\(\) will list all objects in all namespaces.
> 
> 
> Warning:
> 
> Changing any of these settings is not thread\-safe \- librados users must synchronize any of these changes on their own, or use separate io contexts for each thread
> 
> 
> _typedef_ void \*`rados_list_ctx_t`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> An iterator for listing the objects in a pool. Used with rados\_nobjects\_list\_open\(\), rados\_nobjects\_list\_next\(\), rados\_nobjects\_list\_next2\(\), and rados\_nobjects\_list\_close\(\).
> 
> _typedef_ void \*`rados_object_list_cursor`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> The cursor used with rados\_enumerate\_objects and accompanying methods.
> 
> _typedef_ uint64\_t `rados_snap_t`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> The id of a snapshot.
> 
> _typedef_ void \*`rados_xattrs_iter_t`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> An iterator for listing extended attrbutes on an object. Used with rados\_getxattrs\(\), rados\_getxattrs\_next\(\), and rados\_getxattrs\_end\(\).
> 
> _typedef_ void \*`rados_omap_iter_t`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> An iterator for listing omap key/value pairs on an object. Used with rados\_read\_op\_omap\_get\_keys\(\), rados\_read\_op\_omap\_get\_vals\(\), rados\_read\_op\_omap\_get\_vals\_by\_keys\(\), rados\_omap\_get\_next\(\), and rados\_omap\_get\_end\(\).
> 
> _typedef_ void \*`rados_write_op_t`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> An object write operation stores a number of operations which can be executed atomically. For usage, see:
> 
> 
> 
> * Creation and deletion: rados\_create\_write\_op\(\) rados\_release\_write\_op\(\)
> * Extended attribute manipulation: rados\_write\_op\_cmpxattr\(\) rados\_write\_op\_cmpxattr\(\), rados\_write\_op\_setxattr\(\), rados\_write\_op\_rmxattr\(\)
> * Object map key/value pairs: rados\_write\_op\_omap\_set\(\), rados\_write\_op\_omap\_rm\_keys\(\), rados\_write\_op\_omap\_clear\(\), rados\_write\_op\_omap\_cmp\(\)
> * Object properties: rados\_write\_op\_assert\_exists\(\), rados\_write\_op\_assert\_version\(\)
> * Creating objects: rados\_write\_op\_create\(\)
> * IO on objects: rados\_write\_op\_append\(\), rados\_write\_op\_write\(\), rados\_write\_op\_zero rados\_write\_op\_write\_full\(\), rados\_write\_op\_writesame\(\), rados\_write\_op\_remove, rados\_write\_op\_truncate\(\), rados\_write\_op\_zero\(\), rados\_write\_op\_cmpext\(\)
> * Hints: rados\_write\_op\_set\_alloc\_hint\(\)
> * Performing the operation: rados\_write\_op\_operate\(\), rados\_aio\_write\_op\_operate\(\)
> 
> 
> _typedef_ void \*`rados_read_op_t`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> An object read operation stores a number of operations which can be executed atomically. For usage, see:
> 
> 
> 
> * Creation and deletion: rados\_create\_read\_op\(\) rados\_release\_read\_op\(\)
> * Extended attribute manipulation: rados\_read\_op\_cmpxattr\(\), rados\_read\_op\_getxattr\(\), rados\_read\_op\_getxattrs\(\)
> * Object map key/value pairs: rados\_read\_op\_omap\_get\_vals\(\), rados\_read\_op\_omap\_get\_keys\(\), rados\_read\_op\_omap\_get\_vals\_by\_keys\(\), rados\_read\_op\_omap\_cmp\(\)
> * Object properties: rados\_read\_op\_stat\(\), rados\_read\_op\_assert\_exists\(\), rados\_read\_op\_assert\_version\(\)
> * IO on objects: rados\_read\_op\_read\(\), rados\_read\_op\_checksum\(\), rados\_read\_op\_cmpext\(\)
> * Custom operations: rados\_read\_op\_exec\(\), rados\_read\_op\_exec\_user\_buf\(\)
> * Request properties: rados\_read\_op\_set\_flags\(\)
> * Performing the operation: rados\_read\_op\_operate\(\), rados\_aio\_read\_op\_operate\(\)
> 
> 
> _typedef_ void \*`rados_completion_t`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Represents the state of an asynchronous operation \- it contains the return value once the operation completes, and can be used to block until the operation is complete or safe.
> 
> 
> Enums
> 
> _enum_ **\[anonymous\]**[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _Values:_
> 
> _enumerator_ `LIBRADOS_OP_FLAG_EXCL`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_OP_FLAG_FAILOK`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_OP_FLAG_FADVISE_RANDOM`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_OP_FLAG_FADVISE_SEQUENTIAL`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_OP_FLAG_FADVISE_WILLNEED`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_OP_FLAG_FADVISE_DONTNEED`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_OP_FLAG_FADVISE_NOCACHE`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_OP_FLAG_FADVISE_FUA`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enum_ `rados_checksum_type_t`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _Values:_
> 
> _enumerator_ `LIBRADOS_CHECKSUM_TYPE_XXHASH32`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_CHECKSUM_TYPE_XXHASH64`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _enumerator_ `LIBRADOS_CHECKSUM_TYPE_CRC32C`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> 
> Functions
> 
> void `rados_version`\(int \*_major_, int \*_minor_, int \*_extra_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Get the version of librados.
> 
> The version number is major.minor.extra. Note that this is unrelated to the Ceph version number.
> 
> TODO: define version semantics, i.e.:
> 
> 
> 
> * incrementing major is for backwards\-incompatible changes
> * incrementing minor is for backwards\-compatible changes
> * incrementing extra is for bug fixes
> 
> 
> Parameters
> * **major** – where to store the major version number
> * **minor** – where to store the minor version number
> * **extra** – where to store the extra version number
> int `rados_cluster_stat`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, _struct_ [rados\_cluster\_stat\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_cluster_stat_t") \*_result_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Read usage info about the cluster
> 
> This tells you total space, space used, space available, and number of objects. These are not updated immediately when data is written, they are eventually consistent.
> 
> Parameters
> * **cluster** – cluster to query
> * **result** – where to store the results
> Returns0 on success, negative error code on failure
> 
> int `rados_cluster_fsid`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, char \*_buf_, size\_t _len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Get the fsid of the cluster as a hexadecimal string.
> 
> The fsid is a unique id of an entire Ceph cluster.
> 
> Parameters
> * **cluster** – where to get the fsid
> * **buf** – where to write the fsid
> * **len** – the size of buf in bytes \(should be 37\)
> Returns0 on success, negative error code on failure
> 
> Returns\-ERANGE if the buffer is too short to contain the fsid
> 
> int `rados_wait_for_latest_osdmap`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Get/wait for the most recent osdmap
> 
> Parameters
> * **cluster** – the cluster to shutdown
> Returns0 on success, negative error code on failure
> 
> int `rados_omap_get_next`\([rados\_omap\_iter\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_omap_iter_t") _iter_, char \*\*_key_, char \*\*_val_, size\_t \*_len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Get the next omap key/value pair on the object
> 
> Parameters
> * **iter** – iterator to advance
> * **key** – where to store the key of the next omap entry
> * **val** – where to store the value of the next omap entry
> * **len** – where to store the number of bytes in val
> Preiter is a valid iterator
> 
> Postkey and val are the next key/value pair. key is null\-terminated, and val has length len. If the end of the list has been reached, key and val are NULL, and len is 0. key and val will not be accessible after rados\_omap\_get\_end\(\) is called on iter, so if they are needed after that they should be copied.
> 
> Returns0 on success, negative error code on failure
> 
> int `rados_omap_get_next2`\([rados\_omap\_iter\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_omap_iter_t") _iter_, char \*\*_key_, char \*\*_val_, size\_t \*_key\_len_, size\_t \*_val\_len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Get the next omap key/value pair on the object. Note that it’s perfectly safe to mix calls to rados\_omap\_get\_next and rados\_omap\_get\_next2.
> 
> Parameters
> * **iter** – iterator to advance
> * **key** – where to store the key of the next omap entry
> * **val** – where to store the value of the next omap entry
> * **key\_len** – where to store the number of bytes in key
> * **val\_len** – where to store the number of bytes in val
> Preiter is a valid iterator
> 
> Postkey and val are the next key/value pair. key has length keylen and val has length vallen. If the end of the list has been reached, key and val are NULL, and keylen and vallen is 0. key and val will not be accessible after rados\_omap\_get\_end\(\) is called on iter, so if they are needed after that they should be copied.
> 
> Returns0 on success, negative error code on failure
> 
> unsigned int `rados_omap_iter_size`\([rados\_omap\_iter\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_omap_iter_t") _iter_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Return number of elements in the iterator
> 
> Parameters
> * **iter** – the iterator of which to return the size
> void `rados_omap_get_end`\([rados\_omap\_iter\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_omap_iter_t") _iter_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Close the omap iterator.
> 
> iter should not be used after this is called.
> 
> Parameters
> * **iter** – the iterator to close
> int `rados_stat`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_o_, uint64\_t \*_psize_, time\_t \*_pmtime_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Get object stats \(size/mtime\)
> 
> TODO: when are these set, and by whom? can they be out of date?
> 
> Parameters
> * **io** – ioctx
> * **o** – object name
> * **psize** – where to store object size
> * **pmtime** – where to store modification time
> Returns0 on success, negative error code on failure
> 
> int `rados_exec`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_oid_, _const_ char \*_cls_, _const_ char \*_method_, _const_ char \*_in\_buf_, size\_t _in\_len_, char \*_buf_, size\_t _out\_len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Execute an OSD class method on an object
> 
> The OSD has a plugin mechanism for performing complicated operations on an object atomically. These plugins are called classes. This function allows librados users to call the custom methods. The input and output formats are defined by the class. Classes in ceph.git can be found in src/cls subdirectories
> 
> Parameters
> * **io** – the context in which to call the method
> * **oid** – the object to call the method on
> * **cls** – the name of the class
> * **method** – the name of the method
> * **in\_buf** – where to find input
> * **in\_len** – length of in\_buf in bytes
> * **buf** – where to store output
> * **out\_len** – length of buf in bytes
> Returnsthe length of the output, or \-ERANGE if out\_buf does not have enough space to store it \(For methods that return data\). For methods that don’t return data, the return value is method\-specific.
> 
> int `rados_cache_pin`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_o_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Pin an object in the cache tier
> 
> When an object is pinned in the cache tier, it stays in the cache tier, and won’t be flushed out.
> 
> Parameters
> * **io** – the pool the object is in
> * **o** – the object id
> Returns0 on success, negative error code on failure
> 
> int `rados_cache_unpin`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_o_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Unpin an object in the cache tier
> 
> After an object is unpinned in the cache tier, it can be flushed out
> 
> Parameters
> * **io** – the pool the object is in
> * **o** – the object id
> Returns0 on success, negative error code on failure
> 
> int `rados_lock_exclusive`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_oid_, _const_ char \*_name_, _const_ char \*_cookie_, _const_ char \*_desc_, _struct_ timeval \*_duration_, uint8\_t _flags_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Take an exclusive lock on an object.
> 
> Parameters
> * **io** – the context to operate in
> * **oid** – the name of the object
> * **name** – the name of the lock
> * **cookie** – user\-defined identifier for this instance of the lock
> * **desc** – user\-defined lock description
> * **duration** – the duration of the lock. Set to NULL for infinite duration.
> * **flags** – lock flags
> Returns0 on success, negative error code on failure
> 
> Returns\-EBUSY if the lock is already held by another \(client, cookie\) pair
> 
> Returns\-EEXIST if the lock is already held by the same \(client, cookie\) pair
> 
> int `rados_lock_shared`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_o_, _const_ char \*_name_, _const_ char \*_cookie_, _const_ char \*_tag_, _const_ char \*_desc_, _struct_ timeval \*_duration_, uint8\_t _flags_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Take a shared lock on an object.
> 
> Parameters
> * **io** – the context to operate in
> * **o** – the name of the object
> * **name** – the name of the lock
> * **cookie** – user\-defined identifier for this instance of the lock
> * **tag** – The tag of the lock
> * **desc** – user\-defined lock description
> * **duration** – the duration of the lock. Set to NULL for infinite duration.
> * **flags** – lock flags
> Returns0 on success, negative error code on failure
> 
> Returns\-EBUSY if the lock is already held by another \(client, cookie\) pair
> 
> Returns\-EEXIST if the lock is already held by the same \(client, cookie\) pair
> 
> int `rados_unlock`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_o_, _const_ char \*_name_, _const_ char \*_cookie_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Release a shared or exclusive lock on an object.
> 
> Parameters
> * **io** – the context to operate in
> * **o** – the name of the object
> * **name** – the name of the lock
> * **cookie** – user\-defined identifier for the instance of the lock
> Returns0 on success, negative error code on failure
> 
> Returns\-ENOENT if the lock is not held by the specified \(client, cookie\) pair
> 
> int `rados_aio_unlock`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_o_, _const_ char \*_name_, _const_ char \*_cookie_, [rados\_completion\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_completion_t") _completion_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Asynchronous release a shared or exclusive lock on an object.
> 
> Parameters
> * **io** – the context to operate in
> * **o** – the name of the object
> * **name** – the name of the lock
> * **cookie** – user\-defined identifier for the instance of the lock
> * **completion** – what to do when operation has been attempted
> Returns0 on success, negative error code on failure
> 
> ssize\_t `rados_list_lockers`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_o_, _const_ char \*_name_, int \*_exclusive_, char \*_tag_, size\_t \*_tag\_len_, char \*_clients_, size\_t \*_clients\_len_, char \*_cookies_, size\_t \*_cookies\_len_, char \*_addrs_, size\_t \*_addrs\_len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> List clients that have locked the named object lock and information about the lock.
> 
> The number of bytes required in each buffer is put in the corresponding size out parameter. If any of the provided buffers are too short, \-ERANGE is returned after these sizes are filled in.
> 
> Parameters
> * **io** – the context to operate in
> * **o** – the name of the object
> * **name** – the name of the lock
> * **exclusive** – where to store whether the lock is exclusive \(1\) or shared \(0\)
> * **tag** – where to store the tag associated with the object lock
> * **tag\_len** – number of bytes in tag buffer
> * **clients** – buffer in which locker clients are stored, separated by ‘0’
> * **clients\_len** – number of bytes in the clients buffer
> * **cookies** – buffer in which locker cookies are stored, separated by ‘0’
> * **cookies\_len** – number of bytes in the cookies buffer
> * **addrs** – buffer in which locker addresses are stored, separated by ‘0’
> * **addrs\_len** – number of bytes in the clients buffer
> Returnsnumber of lockers on success, negative error code on failure
> 
> Returns\-ERANGE if any of the buffers are too short
> 
> int `rados_break_lock`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_o_, _const_ char \*_name_, _const_ char \*_client_, _const_ char \*_cookie_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Releases a shared or exclusive lock on an object, which was taken by the specified client.
> 
> Parameters
> * **io** – the context to operate in
> * **o** – the name of the object
> * **name** – the name of the lock
> * **client** – the client currently holding the lock
> * **cookie** – user\-defined identifier for the instance of the lock
> Returns0 on success, negative error code on failure
> 
> Returns\-ENOENT if the lock is not held by the specified \(client, cookie\) pair
> 
> Returns\-EINVAL if the client cannot be parsed
> 
> int `rados_blocklist_add`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, char \*_client\_address_, uint32\_t _expire\_seconds_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Blocklists the specified client from the OSDs
> 
> Parameters
> * **cluster** – cluster handle
> * **client\_address** – client address
> * **expire\_seconds** – number of seconds to blocklist \(0 for default\)
> Returns0 on success, negative error code on failure
> 
> int `rados_blacklist_add`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, char \*_client\_address_, uint32\_t _expire\_seconds_\) \_\_attribute\_\_\(\(deprecated\)\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> int `rados_getaddrs`\([rados\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_t") _cluster_, char \*\*_addrs_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Gets addresses of the RADOS session, suitable for blocklisting.
> 
> Parameters
> * **cluster** – cluster handle
> * **addrs** – the output string.
> Returns0 on success, negative error code on failure
> 
> void `rados_set_osdmap_full_try`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_\) \_\_attribute\_\_\(\(deprecated\)\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> void `rados_unset_osdmap_full_try`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_\) \_\_attribute\_\_\(\(deprecated\)\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> void `rados_set_pool_full_try`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> void `rados_unset_pool_full_try`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> int `rados_application_enable`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_app\_name_, int _force_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Enable an application on a pool
> 
> Parameters
> * **io** – pool ioctx
> * **app\_name** – application name
> * **force** – 0 if only single application per pool
> Returns0 on success, negative error code on failure
> 
> int `rados_application_list`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, char \*_values_, size\_t \*_values\_len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> List all enabled applications
> 
> If the provided buffer is too short, the required length is filled in and \-ERANGE is returned. Otherwise, the buffers are filled with the application names, with a ‘0’ after each.
> 
> Parameters
> * **io** – pool ioctx
> * **values** – buffer in which to store application names
> * **values\_len** – number of bytes in values buffer
> Returns0 on success, negative error code on failure
> 
> Returns\-ERANGE if either buffer is too short
> 
> int `rados_application_metadata_get`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_app\_name_, _const_ char \*_key_, char \*_value_, size\_t \*_value\_len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Get application metadata value from pool
> 
> Parameters
> * **io** – pool ioctx
> * **app\_name** – application name
> * **key** – metadata key
> * **value** – result buffer
> * **value\_len** – maximum len of value
> Returns0 on success, negative error code on failure
> 
> int `rados_application_metadata_set`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_app\_name_, _const_ char \*_key_, _const_ char \*_value_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Set application metadata on a pool
> 
> Parameters
> * **io** – pool ioctx
> * **app\_name** – application name
> * **key** – metadata key
> * **value** – metadata key
> Returns0 on success, negative error code on failure
> 
> int `rados_application_metadata_remove`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_app\_name_, _const_ char \*_key_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> Remove application metadata from a pool
> 
> Parameters
> * **io** – pool ioctx
> * **app\_name** – application name
> * **key** – metadata key
> Returns0 on success, negative error code on failure
> 
> int `rados_application_metadata_list`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, _const_ char \*_app\_name_, char \*_keys_, size\_t \*_key\_len_, char \*_values_, size\_t \*_vals\_len_\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> List all metadata key/value pairs associated with an application.
> 
> This iterates over all metadata, key\_len and val\_len are filled in with the number of bytes put into the keys and values buffers.
> 
> If the provided buffers are too short, the required lengths are filled in and \-ERANGE is returned. Otherwise, the buffers are filled with the keys and values of the metadata, with a ‘0’ after each.
> 
> Parameters
> * **io** – pool ioctx
> * **app\_name** – application name
> * **keys** – buffer in which to store key names
> * **key\_len** – number of bytes in keys buffer
> * **values** – buffer in which to store values
> * **vals\_len** – number of bytes in values buffer
> Returns0 on success, negative error code on failure
> 
> Returns\-ERANGE if either buffer is too short
> 
> int `rados_objects_list_open`\([rados\_ioctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_ioctx_t") _io_, [rados\_list\_ctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_list_ctx_t") \*_ctx_\) \_\_attribute\_\_\(\(deprecated\)\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> uint32\_t `rados_objects_list_get_pg_hash_position`\([rados\_list\_ctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_list_ctx_t") _ctx_\) \_\_attribute\_\_\(\(deprecated\)\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> uint32\_t `rados_objects_list_seek`\([rados\_list\_ctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_list_ctx_t") _ctx_, uint32\_t _pos_\) \_\_attribute\_\_\(\(deprecated\)\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> int `rados_objects_list_next`\([rados\_list\_ctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_list_ctx_t") _ctx_, _const_ char \*\*_entry_, _const_ char \*\*_key_\) \_\_attribute\_\_\(\(deprecated\)\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> void `rados_objects_list_close`\([rados\_list\_ctx\_t](https://docs.ceph.com/en/pacific/rados/api/librados/ "rados_list_ctx_t") _ctx_\) \_\_attribute\_\_\(\(deprecated\)\)[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> 
> _struct_ `rados_object_list_item`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _\#include \<librados.h\>_The item populated by rados\_object\_list in the results array.
> 
> Public Members
> 
> size\_t `oid_length`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> oid length
> 
> char \*`oid`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> name of the object
> 
> size\_t `nspace_length`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> namespace length
> 
> char \*`nspace`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> the object namespace
> 
> size\_t `locator_length`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> locator length
> 
> char \*`locator`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> object locator
> 
> 
> _struct_ `rados_pool_stat_t`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _\#include \<librados.h\>_Usage information for a pool.
> 
> Public Members
> 
> uint64\_t `num_bytes`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> space used in bytes
> 
> uint64\_t `num_kb`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> space used in KB
> 
> uint64\_t `num_objects`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> number of objects in the pool
> 
> uint64\_t `num_object_clones`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> number of clones of objects
> 
> uint64\_t `num_object_copies`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> num\_objects \* num\_replicas
> 
> uint64\_t `num_objects_missing_on_primary`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> number of objects missing on primary
> 
> uint64\_t `num_objects_unfound`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> number of objects found on no OSDs
> 
> uint64\_t `num_objects_degraded`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> number of objects replicated fewer times than they should be \(but found on at least one OSD\)
> 
> uint64\_t `num_rd`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> number of objects read
> 
> uint64\_t `num_rd_kb`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> objects read in KB
> 
> uint64\_t `num_wr`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> number of objects written
> 
> uint64\_t `num_wr_kb`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> objects written in KB
> 
> uint64\_t `num_user_bytes`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> bytes originally provided by user
> 
> uint64\_t `compressed_bytes_orig`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> bytes passed compression
> 
> uint64\_t `compressed_bytes`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> bytes resulted after compression
> 
> uint64\_t `compressed_bytes_alloc`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> bytes allocated at storage
> 
> 
> _struct_ `rados_cluster_stat_t`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> _\#include \<librados.h\>_Cluster\-wide usage information
> 
> Public Members
> 
> uint64\_t `kb`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> total device size
> 
> uint64\_t `kb_used`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> total used
> 
> uint64\_t `kb_avail`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> total available/free
> 
> uint64\_t `num_objects`[¶](https://docs.ceph.com/en/pacific/rados/api/librados/ "Permalink to this definition")
> 
> number of objects
