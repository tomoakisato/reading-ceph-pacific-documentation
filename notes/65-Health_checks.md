# 65: Health checks

**クリップソース:** [65: Health checks — Ceph Documentation](https://docs.ceph.com/en/pacific/rados/operations/health-checks/)

# Health checks[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

## Overview[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

Cephクラスタが提起できるヘルスメッセージのセットは有限であり、これらは一意の識別子を持つヘルスチェックとして定義されています。

識別子は、人間が読める\(つまり、変数名のような\)簡潔な文字列です。 これは、ツール\(UIなど\)がヘルスチェックの意味を理解し、その意味を反映した方法で表示できるようにすることを目的としています。

このページでは、モニターとマネージャーデーモンによって提起されるヘルスチェックの一覧を示します。 これらに加えて、MDSデーモンから発生するヘルスチェック\(CephFS health messagesを参照\)、およびceph\-mgr pythonモジュールで定義されるヘルスチェックも表示される場合があります。

## Definitions[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

### Monitor[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

#### DAEMON\_OLD\_VERSION[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

Cephの古いバージョンがいずれかのデーモンで実行されている場合に警告します。

複数のバージョンが検出された場合、ヘルスエラーが生成されます。ヘルスコンディションをトリガするには、この状態がmon\_warn\_older\_version\_delay \(デフォルトでは1週間に設定\)を超えて存在する必要があります。 これにより、ほとんどのアップグレードは警告を誤って見ることなく進めることができます。 アップグレードが長期間停止している場合、health mute を次のように使用できます。 "ceph health mute DAEMON\_OLD\_VERSION \-sticky".  この場合、アップグレードが終了したら、「ceph health unmute DAEMON\_OLD\_VERSION」を使用します。

#### MON\_DOWN[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

1つ以上のモニターデーモンが現在停止しています。 

クラスタが機能するためには、モニタの大部分（1/2以上）が必要です。 1つ以上のモニターが停止している場合、クライアントはクラスタへの最初の接続を形成するのに時間がかかり、動作中のモニターに到達するまでに多くのアドレスを試す必要がある可能性があるため、より困難になります。

ダウンしたモニターデーモンは、その後のモニター障害によるサービス停止のリスクを減らすために、通常、できるだけ早く再起動する必要があります。

#### MON\_CLOCK\_SKEW[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

ceph\-mon monitor daemonを実行しているホストのクロックが十分に同期されていません。 

このヘルスアラートは、クラスタがmon\_clock\_drift\_allowedを超えるクロックスキューを検出した場合に発生します。

これは、ntpdやchronyのようなツールを使用してクロックを同期させることで解決します。

クロックを厳密に同期させることが現実的でない場合、mon\_clock\_drift\_allowedの閾値を上げることもできますが、モニタークラスタが適切に機能するためには、この値をmon\_lease間隔よりかなり低く保つ必要があります。

#### MON\_MSGR2\_NOT\_ENABLED[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

ms\_bind\_msgr2オプションは有効ですが、1つまたは複数のモニターがクラスタのmonmapのv2ポートにバインドするように設定されていません。 

これは、msgr2 プロトコルに固有の機能 \(暗号化など\) が、一部またはすべての接続で利用できないことを意味します。

ほとんどの場合、これはコマンドを発行することによって修正することができます。

```
ceph mon enable-msgr2
```

このコマンドは、古いデフォルトポート6789で設定されているモニターを、6789でv1接続をリッスンし続け、新しいデフォルトポート3300でv2接続もリッスンするように変更します。

もし、モニターが6789以外のポートでv1接続をリッスンするように設定されている場合、monmapを手動で修正する必要があります。

#### MON\_DISK\_LOW[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

1つまたは複数のモニターのディスク容量が不足しています。 

この警告は、モニターデータベースを格納するファイルシステム（通常は/var/lib/ceph/mon）上の利用可能な領域が、パーセンテージとして mon\_data\_avail\_warn （デフォルト：30%）以下になった場合に発生します。

これは、システム上の他のプロセスまたはユーザーが、モニターが使用する同じファイルシステムをいっぱいにしていることを示すかもしれません。 また、モニターのデータベースが大きいことを示す場合もあります \(以下のMON\_DISK\_BIGを参照\)。

スペースを解放できない場合、モニターのデータディレクトリを別のストレージデバイスまたはファイルシステムに移動する必要があるかもしれません（もちろん、モニターデーモンが実行されていない間に）。

#### MON\_DISK\_CRIT[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

1つまたは複数のモニタのディスク容量が極端に少なくなっています。 

この警告は、モニターデータベースを格納するファイルシステム（通常は/var/lib/ceph/mon）上の利用可能なスペースが、パーセンテージとして mon\_data\_avail\_crit （デフォルト：5%）以下に低下した場合にトリガーされます。 上記の MON\_DISK\_LOW を参照してください。

#### MON\_DISK\_BIG[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

1つまたは複数のモニタのデータベースサイズが非常に大きい。 

モニターのデータベースのサイズが mon\_data\_size\_warn \(デフォルト: 15 GiB\) より大きい場合、この警告が表示されます。

データベースが大きいことは異常ですが、必ずしも問題を示しているわけではありません。 長い間、active\+cleanの状態に達していない配置グループがある場合、モニターのデータベースが大きくなることがあります。

これは、モニタのデータベースが適切にコンパクト化されていないことを示す場合もあります。これは、いくつかの古いバージョンの leveldb と rocksdb で観察されています。 ceph daemon mon.\<id\> compactで圧縮を強制すると、ディスク上のサイズが縮小されることがあります。

この警告は、モニタが保存するクラスタメタデータの刈り込みを妨げるバグがあることを示している可能性もあります。 この問題が解決しない場合は、バグを報告してください。

警告のしきい値を調整することができます。

```
ceph config set global mon_data_size_warn <size>
```

#### AUTH\_INSECURE\_GLOBAL\_ID\_RECLAIM[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

1つ以上のクライアントまたはデーモンが、モニターに再接続するときに global\_id \(クラスタの各エンティティを識別する一意の番号\) を安全に再要求しないクラスタに接続されています。 

auth\_allow\_insecure\_global\_id\_reclaimオプションがtrueに設定され\(すべてのcephクライアントがアップグレードされるまで必要な場合があります\)、auth\_expose\_insecure\_global\_id\_reclaimオプションがtrueに設定されているため、クライアントはとにかく接続が許可されています \(これによりモニターは、最初の認証直後に再接続させることで、セキュアではないリクレイムのクライアントを早期に検出することができます\)。

パッチが適用されていないcephクライアントコードを使用しているクライアントを特定するには、以下を使用します。

```
ceph health detail
```

クライアント global\_id reclaim rehavior は、個々のモニターに接続されているクライアントのダンプの global\_id\_status フィールドでも確認できます \(reclaim\_insecure は、そのクライアントがパッチされておらず、このヘルス警告に寄与していることを意味します\)。

```
ceph tell mon.* sessions
```

システム内のすべてのクライアントを、global\_id値を正しく再請求する新しいバージョンのCephにアップグレードすることを強くお勧めします。 すべてのクライアントが更新されたら、で安全でない再接続を許可しないようにすることができます。

```
ceph config set mon auth_allow_insecure_global_id_reclaim false
```

すべてのクライアントをすぐにアップグレードすることが現実的でない場合、この警告を一時的に停止することができます。

```
ceph health mute AUTH_INSECURE_GLOBAL_ID_RECLAIM 1w   # 1 week
```

また、この警告を表示しないようにすることもできます。

```
ceph config set mon mon_warn_on_insecure_global_id_reclaim false
```

#### AUTH\_INSECURE\_GLOBAL\_ID\_RECLAIM\_ALLOWED[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

Cephは現在、auth\_allow\_insecure\_global\_id\_reclaim設定がtrueに設定されているため、クライアントが安全でないプロセスを使用してモニターに再接続して以前のグローバルIDを再取得できるよう設定されています。 

既存のCephクライアントが、global\_idを正しく安全に再クレームするCephの新バージョンにアップグレードされる間、この設定を有効にしておく必要がある場合があります。

AUTH\_INSECURE\_GLOBAL\_ID\_RECLAIMヘルスアラートも発生せず、auth\_expose\_insecure\_global\_id\_reclaim設定も無効になっていない場合\(デフォルトでオン\)、現在アップグレードが必要なクライアントが接続しておらず、insecure global\_id reclaimを無効にしても安全であることが確認されます。

```
ceph config set mon auth_allow_insecure_global_id_reclaim false
```

アップグレードが必要なクライアントがまだある場合は、このアラートを一時的に停止することができます。

```
ceph health mute AUTH_INSECURE_GLOBAL_ID_RECLAIM_ALLOWED 1w   # 1 week
```

また、この警告を表示しないようにすることもできます。

```
ceph config set mon mon_warn_on_insecure_global_id_reclaim_allowed false
```

### Manager[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

#### MGR\_DOWN[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

現在、すべてのマネージャデーモンが停止しています。 

通常、クラスタには少なくとも1つのマネージャ\(ceph\-mgr\)デーモンが動作している必要があります。 マネージャデーモンが動作していない場合、クラスタ自身を監視する機能が低下し、管理APIの一部が使用できなくなります\(たとえば、ダッシュボードは動作せず、メトリクスやランタイムの状態を報告するほとんどのCLIコマンドはブロックされます\)。 ただし、クラスターはすべてのIOオペレーションを実行し、障害から回復することができます。

通常、ダウンマネージャデーモンはできるだけ早く再起動して、クラスタを監視できるようにする必要があります\(たとえば、ceph\-sの情報を最新にし、Prometheusでメトリクスをスクレイピングできるようにするためです\)。

#### MGR\_MODULE\_DEPENDENCY[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

有効化されたマネージャモジュールが依存性チェックに失敗しています。 

このヘルスチェックには、問題についてのモジュールからの説明メッセージが含まれるはずです。

例えば、モジュールは必要なパッケージがインストールされていないと報告するかもしれません。必要なパッケージをインストールし、マネージャデーモンを再起動してください。

このヘルスチェックは、有効なモジュールにのみ適用されます。 モジュールが有効になっていない場合、ceph module lsの出力で依存関係の問題が報告されているかどうかを確認できます。

#### MGR\_MODULE\_ERROR[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

マネージャモジュールに予期せぬエラーが発生しました。 

モジュールのサーブ関数から処理されない例外が発生したことを意味します。 例外がそれ自身について有用な説明を提供しなかった場合、エラーの説明は不明瞭な言葉になっているかもしれません。

このヘルスチェックは、バグを示す場合があります。バグに遭遇したと思われる場合は、Cephのバグレポートを開いてください。

エラーが一過性であると思われる場合は、マネージャデーモンを再起動するか、アクティブデーモンでceph mgr failを使用して別のデーモンへのフェイルオーバーを促すことができます。

### OSDs[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

#### OSD\_DOWN[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

1つまたは複数のOSDがマークdownしている。 

ceph\-osdデーモンが停止しているか、ピアOSDがネットワークを介してOSDに到達できない可能性があります。一般的な原因には、デーモンの停止またはクラッシュ、ホストのダウン、ネットワーク障害が含まれます。

ホストが正常であること、デーモンが起動していること、ネットワークが機能していることを確認します。 デーモンがクラッシュした場合、デーモンのログファイル（/var/log/ceph/ceph\-osd.\*）にデバッグ情報が含まれている可能性があります。

#### OSD\_\<crush type\>\_DOWN[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

\(例: OSD\_HOST\_DOWN, OSD\_ROOT\_DOWN\)

例えばホスト上のすべてのOSDのように、特定のCRUSHサブツリー内のすべてのOSDはマークdownされます。

#### OSD\_ORPHAN[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

CRUSHマップの階層でOSDが参照されているが、存在しない。

以下でCRUSH階層からOSDを削除することができる。

```
ceph osd crush rm osd.<id>
```

#### OSD\_OUT\_OF\_ORDER\_FULL[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

nearfull、backfillfull、full、failsafe\_fullの利用率の閾値が昇順ではありません。 

特に、nearfull \< backfillfull、backfillfull \< full、full \< failafe\_fullを想定しています。

以下で閾値は調整できる。

```
ceph osd set-nearfull-ratio <ratio>
ceph osd set-backfillfull-ratio <ratio>
ceph osd set-full-ratio <ratio>
```

#### OSD\_FULL[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

1つまたは複数のOSDがfullスレッショルドを超え、クラスタが書き込みを処理するのを妨げています。

プール別の使用率は、以下を使用して確認できます。

```
ceph df
```

現在定義されているfullレシオは、以下で見ることができます。

```
ceph osd dump | grep full_ratio
```

書き込み可用性を回復するための短期的な回避策は、完全な閾値を少量上げることです。

```
ceph osd set-full-ratio <ratio>
```

OSDを増設して新しいストレージをクラスターに追加するか、既存のデータを削除して空き容量を確保する必要があります。

#### OSD\_BACKFILLFULL[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

1つ以上のOSDがbackfillfullのしきい値を超えたため、このデバイスへのデータのリバランスが許可されなくなりました。 これは、リバランスが完了できない可能性があり、クラスタがfullに近づいていることを示す初期の警告です。

プール別の使用率は、以下を使用して確認できます。

```
ceph df
```

#### OSD\_NEARFULL[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

1つまたは複数のOSDがnearfullのしきい値を超えました。 これは、クラスタがfullに近づいていることを示す初期の警告です。

プール別の使用率は、以下を使用して確認できます。

```
ceph df
```

#### OSDMAP\_FLAGS[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

クラスタフラグが1つ以上設定されています。 フラグは以下の通りです。

* _full_
* _pauserd_
* _noup_
* _nodown_
* _noin_
* _noout_
* _nobackfill_
* _noscrub_
* _notieragent_

fullを除き、これらのフラグは、次の方法で設定または解除することができます。

```
ceph osd set <flag>
ceph osd unset <flag>
```

#### OSD\_FLAGS[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

1つ以上のOSDまたはCRUSH{nodes,device classes}にフラグが設定されている。フラグには：

* _noup_
* _nodown_
* _noin_
* _noout_

これらのフラグは、一括して設定・解除することができます。

```
ceph osd set-group <flags> <who>
ceph osd unset-group <flags> <who>
```

For example,

```
ceph osd set-group noup,noout osd.0 osd.1
ceph osd unset-group noup,noout osd.0 osd.1
ceph osd set-group noup,noout host-foo
ceph osd unset-group noup,noout host-foo
ceph osd set-group noup,noout class-hdd
ceph osd unset-group noup,noout class-hdd
```

#### OLD\_CRUSH\_TUNABLES[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

CRUSHマップは非常に古い設定を使用しているため、更新する必要があります。 

このヘルス警告をトリガーせずに使用できる最も古いチューナブル（つまり、クラスタに接続できる最も古いクライアントバージョン）は mon\_crush\_min\_required\_version 設定オプションで決定されます。詳細については、[Tunables](https://docs.ceph.com/en/pacific/rados/operations/health-checks/) を参照してください。

#### OLD\_CRUSH\_STRAW\_CALC\_VERSION[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

CRUSHマップは、straw バケットの中間ウエイト値の計算に、最適でない古い方法を使用しています。

CRUSHマップは新しい方法\(straw\_calc\_version=1\)を使用するように更新する必要があります。 詳しくは、[Tunables](https://docs.ceph.com/en/pacific/rados/operations/health-checks/) を参照してください。

#### CACHE\_POOL\_NO\_HIT\_SET[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

1つまたは複数のキャッシュプールに、使用率を追跡するためのヒットセットが設定されていないため、階層化エージェントがキャッシュからフラッシュして退避させるコールドオブジェクトを特定できません。

ヒットセットは、キャッシュプール上で次のように構成することができます。

```
ceph osd pool set <poolname> hit_set_type <type>
ceph osd pool set <poolname> hit_set_period <period-in-seconds>
ceph osd pool set <poolname> hit_set_count <number-of-hitsets>
ceph osd pool set <poolname> hit_set_fpp <target-false-positive-rate>
```

#### OSD\_NO\_SORTBITWISE[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

luminous v12.y.z以前のOSDは動作していないが、sortbitwiseフラグが設定されていない。

sortbitwise フラグは、luminous v12.y.z またはそれ以降の OSD が起動する前に設定されなければなりません。 

以下で安全にフラグを設定することができます。

```
ceph osd set sortbitwise
```

#### POOL\_FULL[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

1つまたは複数のプールがクォータに達し、書き込みを許可しなくなりました。

プールのクォータと使用率は、次のようにして確認できます。

```
ceph df detail
```

以下でプールクォータを上げるか、

```
ceph osd pool set-quota <poolname> max_objects <num-objects>
ceph osd pool set-quota <poolname> max_bytes <num-bytes>
```

既存のデータの一部を削除して使用率を下げることができます。

#### BLUEFS\_SPILLOVER[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

BlueStoreバックエンドを使用する1つ以上のOSDにdbパーティション（メタデータ用のストレージスペース、通常はより高速なデバイス）が割り当てられましたが、そのスペースがいっぱいになり、メタデータが通常の低速デバイスに「こぼれて」います。 これは必ずしもエラー状態ではなく、予期しないことでもありませんが、管理者がすべてのメタデータが高速なデバイスに収まることを期待していた場合、十分なスペースが提供されなかったことを示しています。

で全てのOSDでこの警告を無効にすることができます。

```
ceph config set osd bluestore_warn_on_bluefs_spillover false
```

また、特定のOSDで無効にすることもできます。

```
ceph config set osd.123 bluestore_warn_on_bluefs_spillover false
```

より多くのメタデータ領域を確保するために、当該OSDを破棄し、再プロビジョニングすることが考えられます。 この場合、データの移行と復旧が必要となります。

また、dbストレージのLVM論理ボリュームを拡張することも可能な場合があります。 LV を拡張した場合、OSD デーモンを停止して BlueFS にデバイスサイズの変更を通知する必要があります。

```
ceph-bluestore-tool bluefs-bdev-expand --path /var/lib/ceph/osd/ceph-$ID
```

#### BLUEFS\_AVAILABLE\_SPACE[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

BlueFSの空き容量を確認するには、次のようにします。

```
ceph daemon osd.123 bluestore bluefs available
```

これにより、最大で3つの値が出力されます。BDEV\_DB free, BDEV\_SLOW free, available\_from\_bluestore の3つの値が出力されます。BDEV\_DBとBDEV\_SLOWは、BlueFSが獲得した空き容量と考えられる量を報告します。available\_from\_bluestoreという値は、BlueStoreがBlueFSにさらに容量を譲り渡すことができるかを示しています。BlueFSのアロケーション単位はBlueStoreのアロケーション単位より大きいのが普通なので、この値はBlueStoreの空き容量と異なるのが普通です。これは、BlueStoreの空き領域の一部だけがBlueFSに受け入れられることを意味します。

#### BLUEFS\_LOW\_SPACE[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

BlueFS の空き容量が少なく、available\_from\_bluestore が少ない場合、BlueFS のアロケーションユニットのサイズを小さくすることを検討することができます。アロケーションユニットが異なる場合の空き容量をシミュレートするには、次のようにします。

```
ceph daemon osd.123 bluestore bluefs available <alloc-unit-size>
```

#### BLUESTORE\_FRAGMENTATION[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

BlueStoreの動作に伴い、基盤となるストレージの空き領域は断片化されます。これは正常であり、避けられないことですが、過度の断片化は速度低下の原因となります。BlueStoreのフラグメンテーションを検査するには、次のような方法があります。

```
ceph daemon osd.123 bluestore allocator score block
```

スコアは\[0\-1\]の範囲で表示されます。

\[0.0 ... 0.4\] 小さな断片化 

\[0.4 ... 0.7\] 小さな、許容できる断片化 

\[0.7 ... 0.9\] かなりの、安全な断片化

 \[0.9 ... 1.0\] 激しい断片化、BlueFS が BlueStore から領域を取得する能力に影響を与える可能性がある。

空きフラグメントの詳細なレポートが必要な場合は、以下を実行してください。

```
ceph daemon osd.123 bluestore allocator dump block
```

実行されていないOSDプロセスを扱う場合、ceph\-bluestore\-toolでフラグメンテーションを検査することができます。フラグメンテーションのスコアを取得します。

```
ceph-bluestore-tool --path /var/lib/ceph/osd/ceph-123 --allocator block free-score
```

そして、フリーチャンク詳細をダンプします。

```
ceph-bluestore-tool --path /var/lib/ceph/osd/ceph-123 --allocator block free-dump
```

#### BLUESTORE\_LEGACY\_STATFS[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

Nautilusリリースでは、BlueStoreはプール単位の粒度で内部使用統計を追跡し、1つ以上のOSDにはNautilus以前に作成されたBlueStoreボリュームがあります。 すべてのOSDがNautilusより古い場合、これは単にプールごとのメトリクスが利用できないことを意味します。 ただし、Nautilus以前のOSDとNautilus以降のOSDが混在している場合、ceph dfが報告するクラスタ使用統計は正確ではありません。

古いOSDは、各OSDを停止し、修復処理を実行し、再起動することで、新しい使用状況追跡方式を使用するように更新することができます。 例えば、osd.123の更新が必要な場合、:

```
systemctl stop ceph-osd@123
ceph-bluestore-tool repair --path /var/lib/ceph/osd/ceph-123
systemctl start ceph-osd@123
```

この警告を無効にするには：

```
ceph config set global bluestore_warn_on_legacy_statfs false
```

#### BLUESTORE\_NO\_PER\_POOL\_OMAP[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

Octopusリリースから、BlueStoreはプールごとのomapスペース使用率を追跡し、1つ以上のOSDはOctopusより前に作成されたボリュームを持っています。 

すべてのOSDが新しいトラッキングを有効にしてBlueStoreを実行していない場合、クラスタは最新のディープスクラブに基づくプールごとのomap使用量の概算値を報告します。

古いOSDは、各OSDを停止し、修復処理を実行し、再起動することによって、プールごとに追跡するように更新することができます。 

たとえば、osd.123 が更新される必要がある場合、:

```
systemctl stop ceph-osd@123
ceph-bluestore-tool repair --path /var/lib/ceph/osd/ceph-123
systemctl start ceph-osd@123
```

この警告を無効にするには：

```
ceph config set global bluestore_warn_on_no_per_pool_omap false
```

#### BLUESTORE\_NO\_PER\_PG\_OMAP[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

Pacificリリース以降、BlueStoreはPGごとのomapスペース使用量を追跡し、1つ以上のOSDがPacific以前に作成されたボリュームを持っています。

 PG単位のomapは、PGのマイグレーション時にPGの削除をより迅速に行うことができます。

古いOSDは、各OSDを停止し、修復処理を実行し、再起動することで、PGによる追跡に更新することができます。 

例えば、osd.123の更新が必要な場合、:

```
systemctl stop ceph-osd@123
ceph-bluestore-tool repair --path /var/lib/ceph/osd/ceph-123
systemctl start ceph-osd@123
```

この警告を無効にするには：

```
ceph config set global bluestore_warn_on_no_per_pg_omap false
```

#### BLUESTORE\_DISK\_SIZE\_MISMATCH[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

BlueStoreを使用する1つ以上のOSDで、物理デバイスのサイズとそのサイズを追跡するメタデータの間に内部不一致があります。 これは、将来的にOSDのクラッシュにつながる可能性があります。

問題のOSDは破棄し、再ビジョニングする必要があります。 この作業は、一度に 1 つの OSD で行い、データを危険にさらすことのないように注意する必要があります。 たとえば、osd $N にエラーが発生した場合、:

```
ceph osd out osd.$N
while ! ceph osd safe-to-destroy osd.$N ; do sleep 1m ; done
ceph osd destroy osd.$N
ceph-volume lvm zap /path/to/device
ceph-volume lvm create --osd-id $N --data /path/to/device
```

#### BLUESTORE\_NO\_COMPRESSION[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

1つまたは複数のOSDがBlueStore圧縮プラグインを読み込むことができません。これは、ceph\-osd のバイナリが圧縮プラグインと一致しない壊れたインストール、または ceph\-osd デーモンの再起動を含まない最近のアップグレードが原因である可能性があります。

問題のOSDを実行しているホスト上のパッケージが正しくインストールされていること、およびOSDデーモンが再起動されていることを確認します。 問題が解決しない場合は、OSDログをチェックして、問題の原因に関する何らかの手がかりを探します。

#### BLUESTORE\_SPURIOUS\_READ\_ERRORS[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

BlueStoreを使用している1つまたは複数のOSDが、メインデバイスでspuriousリードエラーを検出しました。BlueStoreはディスクの読み込みを再試行することでこれらのエラーから回復しています。しかし、これは基礎となるハードウェア、I/Oサブシステムなどに何らかの問題があることを示している可能性があります。理論的には、データ破壊を引き起こす可能性があります。根本的な原因に関するいくつかの見解は、https://tracker.ceph.com/issues/22464 に記載されています。

このアラートはすぐに対応する必要はありませんが、対応するホストは、OS/カーネルの最新バージョンへのアップグレードやH/Wのリソース使用状況の監視など、さらなる注意が必要な場合があります。

この警告は、すべてのOSDで無効にすることができます。

```
ceph config set osd bluestore_warn_on_spurious_read_errors false
```

また、特定のOSDで無効にすることもできます。

```
ceph config set osd.123 bluestore_warn_on_spurious_read_errors false
```

### Device health[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

#### DEVICE\_HEALTH[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

1台以上のデバイスがまもなく故障すると予想されます。

警告のしきい値は mgr/devicehealth/warn\_threshold 設定オプションで制御されます。

この警告は、現在 "in "とマークされているOSDにのみ適用されるので、この障害に対する予想される対応は、デバイスからデータを移行するためにデバイスを "out "とマークし、その後システムからハードウェアを取り外すことです。 

mgr/devicehealth/self\_heal が mgr/devicehealth/mark\_out\_threshold に基づいて有効になっている場合、マーク"out"は通常自動的に行われることに注意してください。

デバイスの健康状態を確認するには：

```
ceph device info <device-id>
```

デバイスの寿命は、mgrが実行する予測モデル、またはコマンドによる外部ツールによって設定されます。

```
ceph device set-life-expectancy <device-id> <from> <to>
```

保存された寿命を手動で変更することもできますが、通常、どのようなツールが最初に設定したかを問わず、再び設定される可能性があり、保存値を変更してもハードウェアデバイスの実際の健康状態には影響しないため、何の成果も得られません。

#### DEVICE\_HEALTH\_IN\_USE[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

1つ以上のデバイスがまもなく故障すると予想され、mgr/devicehealth/mark\_out\_thresholdに基づいてクラスタから"out"マークされましたが、それはまだ1つ以上のPGに参加しています。 これは、つい最近 "out" とマークされたばかりで、データがまだ移行中であるか、何らかの理由でデータが移行できないためです \(たとえば、クラスタがほぼ満杯であるか、CRUSH 階層のため、データを移行するのに適した別の OSD がない場合など\)。

このメッセージは、自己回復動作を無効にする \(mgr/devicehealth/self\_heal を false に設定する\)、 mgr/devicehealth/mark\_out\_threshold を調整する、または故障したデバイスからデータを移行するのを阻んでいるものに対処することで消すことができます。

#### DEVICE\_HEALTH\_TOOMANY[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

あまりにも多くのデバイスがまもなく故障すると予想され、mgr/devicehealth/self\_heal動作が有効になっているため、すべての故障したデバイスをマーク"out"すると、クラスタのmon\_osd\_min\_in\_ratio比を超えて、多すぎるOSDが自動的にマーク"out"されないようになっています。

これは一般に、クラスタ内の非常に多くのデバイスがまもなく故障すると予想されることを示し、非常に多くのデバイスが故障してデータが失われる前に、より新しい（健全な）デバイスを追加する措置をとる必要があることを示しています。

mon\_osd\_min\_in\_ratioやmgr/devicehealth/mark\_out\_thresholdなどのパラメータを調整することによってもヘルスメッセージを消すことができますが、これによってクラスタの回復不能なデータ損失の可能性が高くなることに注意してください。

### Data health \(pools & placement groups\)[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

#### PG\_AVAILABILITY[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

データの可用性が低下しています。

これは、クラスタ内の一部のデータに対する潜在的な読み取りまたは書き込み要求を処理できないことを意味します。

具体的には、1つまたは複数のPGがIOリクエストに対応できない状態になっています。 

問題のあるPGの状態には、peering、stale、incomplete、activeの欠如（これらの状態がすぐに解消されない場合）が含まれます。

どのPGが影響を受けるかについての詳細な情報を入手するには：

```
ceph health detail
```

ほとんどの場合、根本的な原因は、1つ以上の OSD が現在ダウンしていることです。

問題のある特定の PG の状態は、次のようにして問い合わせることができます。

```
ceph tell <pgid> query
```

#### PG\_DEGRADED[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

データの冗長性が一部のデータで低下しています。

つまり、クラスタはすべてのデータ（複製プールの場合）またはECフラグメント（ECプールの場合）に対して望ましい数の複製を持っていません。

具体的には、1つまたは複数のPGが：

* degradedまたはundersizedフラグが設定されており、クラスタ内にそのPGのインスタンスが十分でないことを意味します。
* しばらくクリーンフラグが立っていませんでした。

どのPGが影響を受けるかについての詳細な情報を入手するには：

```
ceph health detail
```

ほとんどの場合、根本的な原因は1つ以上のOSDが現在ダウンしていることです。

上記のOSD\_DOWNに関する議論を参照。

問題のある特定の PG の状態は、次のようにして問い合わせることができます：

```
ceph tell <pgid> query
```

#### PG\_RECOVERY\_FULL[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

クラスタの空き容量不足により、データの冗長性が低下したり、一部のデータでリスクが発生したりする可能性があります。 

具体的には、1つ以上のPGにrecovery\_toofullフラグが設定されており、1つ以上のOSDがフルスレッショルドを超えているため、クラスタはデータの移行や復旧ができないことを意味します。

この状態を解決するための手順については、上記のOSD\_FULLに関する説明を参照してください。

#### PG\_BACKFILL\_FULL[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

クラスタの空き容量不足により、データの冗長性が低下したり、一部のデータでリスクが発生したりする可能性があります。 

具体的には、1つ以上のPGにbackfill\_toofullフラグが設定されており、1つ以上のOSDがbackfillfullの閾値を超えているため、クラスタはデータの移行や回復ができないことを意味します。

この状態を解決する手順については、上記のOSD\_BACKFILLFULLに関する説明を参照してください。

#### PG\_DAMAGED[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

データ スクラビングにより、クラスタ内のデータの整合性にいくつかの問題があることが判明しました。 

具体的には、1 つ以上の PG に inconsistent または snaptrim\_error フラグが設定されており、以前のスクラブ操作で問題が見つかったことを示します。また、repair フラグが設定されており、現在不整合の修復が進行中であることを意味します。

詳しくは、「[Repairing PG inconsistencies](https://docs.ceph.com/en/pacific/rados/operations/health-checks/)」を参照してください。

#### OSD\_SCRUB\_ERRORS[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

最近のOSDスクラブで矛盾が発見されました。

このエラーは一般的に PG\_DAMAGED と対になっています（上記参照）。

詳しくは、「[Repairing PG inconsistencies](https://docs.ceph.com/en/pacific/rados/operations/health-checks/)」を参照してください。

#### OSD\_TOO\_MANY\_REPAIRS[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

読み込みエラーが発生し、別のレプリカが利用可能な場合、そのレプリカを使用して直ちにエラーを修復し、クライアントがオブジェクトデータを取得できるようにします。 

スクラブは静止しているデータに対するエラーを処理します。 スクラブエラーが発生していないディスクで障害が発生している可能性を特定するために、読み取り修復のカウントが維持されます。 このカウントが設定値のしきい値 mon\_osd\_warn\_num\_repaired \(デフォルト 10\) を超えると、このヘルス警告が生成されます。

#### LARGE\_OMAP\_OBJECTS[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

osd\_deep\_scrub\_large\_omap\_object\_key\_threshold \(大きなomapオブジェクトを決定するためのキー数の閾値\) または osd\_deep\_scrub\_large\_omap\_object\_value\_sum\_threshold \(大きなomapプロジェクトを決定するためのすべてのキー値の合計サイズ \(bytes\) の閾値\) またはその両方で決定した大きなomapオブジェクトが一つまたは複数のプールに含まれています。

オブジェクト名、キー数、バイト単位のサイズに関する詳細情報は、クラスタログで「Large omap object found」を検索することで確認することができます。

大きなomapオブジェクトは、自動リシャーディングが有効になっていないRGWバケット・インデックス・オブジェクトが原因で発生することがあります。

リシャーディングの詳細については、[RGW Dynamic Bucket Index Resharding](https://docs.ceph.com/en/pacific/rados/operations/health-checks/) を参照してください。

閾値を調整するには：

```
ceph config set osd osd_deep_scrub_large_omap_object_key_threshold <keys>
ceph config set osd osd_deep_scrub_large_omap_object_value_sum_threshold <bytes>
```

#### CACHE\_POOL\_NEAR\_FULL[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

キャッシュ・ティア・プールがほぼ満杯です。 

この文脈での満杯は、キャッシュプールの target\_max\_bytes と target\_max\_objects プロパティによって決定されます。 プールがターゲット閾値に達すると、プールへの書き込み要求は、データのフラッシュとキャッシュからの退避の間、ブロックされるかもしれません、この状態は通常、非常に高いレイテンシとパフォーマンスの低下につながります。

キャッシュプールのターゲットサイズを調整するには：

```
ceph osd pool set <cache-pool-name> target_max_bytes <bytes>
ceph osd pool set <cache-pool-name> target_max_objects <objects>
```

ベース・ティアの可用性やパフォーマンスの低下、またはクラスタ全体の負荷により、通常のキャッシュのフラッシュと退避\(evict\)の動作もスロットルされる可能性があります。

#### TOO\_FEW\_PGS[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

クラスタで使用されているPGの数が、OSDごとのmon\_pg\_warn\_min\_per\_osd PGsの設定可能な閾値を下回っています。 

これは、クラスタ内のOSD間のデータの分布とバランスが最適でなくなり、同様に全体的なパフォーマンスが低下する可能性があります。

データプールがまだ作成されていない場合、これは予期される状態である可能性があります。

既存のプールのPGカウントを増やすか、新しいプールを作成することができます。詳細については、「[Choosing the number of Placement Groups](https://docs.ceph.com/en/pacific/rados/operations/health-checks/) 」を参照してください。

#### POOL\_PG\_NUM\_NOT\_POWER\_OF\_TWO[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

1つ以上のプールの pg\_num 値が 2 の累乗ではありません。

これは厳密には間違っていませんが、いくつかのPGが他のPGのおよそ2倍のデータを持っているため、データの分布のバランスが悪くなります。

これは、影響を受けるプールのpg\_num値を2の近い累乗に設定することで簡単に修正できます。

```
ceph osd pool set <pool-name> pg_num <value>
```

このヘルス警告を無効にするには：

```
ceph config set global mon_warn_on_pool_pg_num_not_power_of_two false
```

#### POOL\_TOO\_FEW\_PGS[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

1つまたは複数のプールには、現在プールに格納されているデータ量に基づいて、より多くのPGを持つ必要があります。 

これは、クラスタ内の OSD 間でデータの分散とバランスが最適でなくなり、同様に全体のパフォーマンスを低下させる可能性があります。 この警告は、プールのpg\_autoscale\_modeプロパティがwarnに設定されている場合に生成されます。

警告を無効にするには、次の方法でプールのPGの自動スケーリングを完全に無効にします。

```
ceph osd pool set <pool-name> pg_autoscale_mode off
```

クラスタがPGの数を自動的に調整できるようにするには：

```
ceph osd pool set <pool-name> pg_autoscale_mode on
```

プールのPG数を推奨量に手動で設定するには：

```
ceph osd pool set <pool-name> pg_num <new-pg-num>
```

詳しくは、「[Choosing the number of Placement Groups](https://docs.ceph.com/en/pacific/rados/operations/health-checks/) 」と「 [Autoscaling placement groups](https://docs.ceph.com/en/pacific/rados/operations/health-checks/) 」を参照してください。

#### TOO\_MANY\_PGS[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

クラスタで使用されているPGの数が、設定可能な閾値であるmon\_max\_pg\_per\_osd PGs per OSDを上回っています。 

この閾値を超えると、クラスタは新しいプールの作成、プールのpg\_numの増加、プールのレプリケーションの増加（いずれもクラスタ内のPGの増加につながる）を許可しなくなります。 PGの数が多いと、OSDデーモンのメモリ使用率が高くなり、クラスタの状態変更（OSDの再起動、追加、削除など）後のピアリングが遅くなり、ManagerデーモンとMonitorデーモンの負荷が高くなる可能性があります。

この問題を軽減する最も簡単な方法は、ハードウェアを追加してクラスタ内のOSDの数を増やすことです。 このヘルスチェックの目的で使用されるOSDの数は、「in」OSDの数であることに注意してください。したがって、「out」OSDを（もしあれば）「in」マークすることも役に立ちます。

```
ceph osd in <osd id(s)>
```

詳しくは、「[Choosing the number of Placement Groups](https://docs.ceph.com/en/pacific/rados/operations/health-checks/)」をご覧ください。

#### POOL\_TOO\_MANY\_PGS[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

1つまたは複数のプールには、現在プールに格納されているデータ量に基づいて、より多くのPGを持つ必要があります。 

これにより、OSD デーモンのメモリ使用率が高くなり、クラスタの状態変更（OSD の再起動、追加、削除など）後のピアリングが遅くなり、マネージャおよびモニタ・デーモンの負荷が高くなることがあります。 この警告は、プールの pg\_autoscale\_mode プロパティが warn に設定されている場合に生成されます。

警告を無効にするには、次の方法でプールのPGの自動スケーリングを完全に無効にします。

```
ceph osd pool set <pool-name> pg_autoscale_mode off
```

クラスタがPGの数を自動的に調整できるようにするには：

```
ceph osd pool set <pool-name> pg_autoscale_mode on
```

プールのPG数を推奨量に手動で設定するには：

```
ceph osd pool set <pool-name> pg_num <new-pg-num>
```

詳しくは、「[Choosing the number of Placement Groups](https://docs.ceph.com/en/pacific/rados/operations/health-checks/) 」と「[Autoscaling placement groups](https://docs.ceph.com/en/pacific/rados/operations/health-checks/)」を参照してください。

#### POOL\_TARGET\_SIZE\_BYTES\_OVERCOMMITTED[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

1つ以上のプールにtarget\_size\_bytesプロパティが設定され、プールの予想サイズを推定していますが、その値は（それ自体または他のプールの実際の使用量と組み合わせて）利用可能なストレージの合計を上回っています。

これは通常、プールの target\_size\_bytes 値が大きすぎるため、値を減らすかゼロに設定する必要があることを示しています。

```
ceph osd pool set <pool-name> target_size_bytes 0
```

詳細については、「[Specifying expected pool size](https://docs.ceph.com/en/pacific/rados/operations/health-checks/)」を参照してください。

#### POOL\_HAS\_TARGET\_SIZE\_BYTES\_AND\_RATIO[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

1つ以上のプールにtarget\_size\_bytesとtarget\_size\_ratioの両方が設定され、プールの予想サイズを推定しています。

これらのプロパティのうち1つだけが非ゼロである必要があります。両方が設定されている場合、target\_size\_ratio が優先され、target\_size\_bytes は無視されます。

target\_size\_bytes を 0 にするには：

```
ceph osd pool set <pool-name> target_size_bytes 0
```

詳細については、「[Specifying expected pool size](https://docs.ceph.com/en/pacific/rados/operations/health-checks/)」を参照してください。

#### TOO\_FEW\_OSDS[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

クラスタ内のOSDの数が設定可能なosd\_pool\_default\_sizeの閾値を下回っています。

#### SMALLER\_PGP\_NUM[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

1つ以上のプールのpgp\_numの値がpg\_numより小さくなっています。 

これは通常、配置動作\(placement behavior\)を増加させずにPGカウントを増加させたことを示しています。

これは、PG数の調整時の分割ステップと、pgp\_numの変更時に必要なデータ移行を切り離すために意図的に行われることがあります。

これは通常、pgp\_numをpg\_numに一致するように設定し、データ移行をトリガすることで解決されます。

```
ceph osd pool set <pool> pgp_num <pg-num-value>
```

#### MANY\_OBJECTS\_PER\_PG[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

1つまたは複数のプールで、PGあたりのオブジェクトの平均数がクラスタ全体の平均よりも著しく多くなっています。 

具体的な閾値はmon\_pg\_warn\_max\_object\_skew設定値で制御されます。

これは通常、クラスタ内のほとんどのデータを含むプールのPGが少なすぎること、および/または、それほど多くのデータを含まない他のプールのPGが多すぎることを示すものです。 上記のTOO\_MANY\_PGSの議論を参照してください。

ヘルス警告を消すには、マネージャの mon\_pg\_warn\_max\_object\_skew 設定オプションを調整して閾値を高くします。

#### POOL\_APP\_NOT\_ENABLED[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

1つまたは複数のオブジェクトを含むプールが存在しますが、特定のアプリケーションで使用するためのタグ付けがされていません。

この警告を解決するには、プールをアプリケーションで使用するためのラベルを付けます。 たとえば、プールがRBDによって使用される場合、:

```
rbd pool init <poolname>
```

プールがカスタムアプリケーション 'foo' によって使用されている場合、低レベルコマンドでラベル付けすることもできます。

```
ceph osd pool application enable foo
```

詳しくは、「[Associate Pool to Application](https://docs.ceph.com/en/pacific/rados/operations/health-checks/)」を参照してください。

#### POOL\_FULL[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

1つまたは複数のプールがクォータに達しました（または非常に達しそうになりました）。 

このエラー状態を引き起こす閾値は、 mon\_pool\_quota\_crit\_threshold 設定オプションで制御されます。

プール枠を上下調整（削除）するには：

```
ceph osd pool set-quota <pool> max_bytes <bytes>
ceph osd pool set-quota <pool> max_objects <objects>
```

クォータ値を0にすると、クォータが無効になります。

#### POOL\_NEAR\_FULL[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

1つまたは複数のプールが、設定された満杯のしきい値に近づいています。

この警告条件を引き起こすことができる閾値は、mon\_pool\_quota\_warn\_thresholdコンフィギュレーションオプションです。

プール枠を上下調整（削除）するには：

```
ceph osd pool set-quota <pool> max_bytes <bytes>
ceph osd pool set-quota <pool> max_objects <objects>
```

クォータ値を0にすると、クォータが無効になります。

上記の2つの警告条件を引き起こす可能性のある他の閾値は、mon\_osd\_nearfull\_ratioとmon\_osd\_full\_ratioです。 詳細と解決策については、[Storage Capacity](https://docs.ceph.com/en/pacific/rados/operations/health-checks/)と[No Free Drive Space](https://docs.ceph.com/en/pacific/rados/operations/health-checks/)のドキュメントを参照してください。

#### OBJECT\_MISPLACED[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

クラスタ内の1つまたは複数のオブジェクトが、クラスタが格納することを望んでいるノードに格納されていません。 

これは、最近のクラスタ変更に伴うデータ移行がまだ完了していないことを示しています。

データの一貫性が損なわれることはなく、オブジェクトの古いコピーは、必要な数の新しいコピー（必要な場所にある）が存在するまで削除されることはありません。

#### OBJECT\_UNFOUND[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

クラスタ内の1つまたは複数のオブジェクトが見つかりません。 

具体的には、OSD はオブジェクトの新しいコピーまたは更新されたコピーが存在するはずだと知っていますが、そのバージョンのオブジェクトのコピーが現在オンラインになっている OSD で見つかっていません。

見つかっていないオブジェクトへの読み取りまたは書き込み要求はブロックされます。

ダウンしているOSDをオンラインに戻し、そのOSDが未発見のオブジェクトの最新のコピーを持っていることが理想的です。 候補となるOSDは、未発見のオブジェクトに責任のあるPG（複数可）のピアリング状態から特定することができます：

```
ceph tell <pgid> query
```

オブジェクトの最新コピーが利用できない場合、オブジェクトの以前のバージョンにロールバックするようにクラスタに指示することができます。詳細については、「[Unfound Objects](https://docs.ceph.com/en/pacific/rados/operations/health-checks/)」を参照してください。

#### SLOW\_OPS[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

1つまたは複数のOSDまたはモニター要求の処理に時間がかかっています。 

これは、極端な負荷、遅いストレージデバイス、またはソフトウェアのバグの兆候である可能性があります。

デーモンのホストから以下のコマンドを実行することで、当該デーモンのリクエストキューを照会することができます。

```
ceph daemon osd.<id> ops
```

最近の最も遅いリクエストの要約を見るには：

```
ceph daemon osd.<id> dump_historic_ops
```

OSDの位置を確認するには：

```
ceph osd find osd.<id>
```

#### PG\_NOT\_SCRUBBED[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

1つまたは複数のPGが最近スクラビングされていません。 

PG は通常、グローバルに osd\_scrub\_max\_interval で指定される設定された間隔ごとにスクラブを実行します。この間隔は scrub\_max\_interval でプール単位で上書きできます。警告は、mon\_warn\_pg\_not\_scrubbed\_ratio が指定された間隔でスクラブを行わずに経過した場合に発生します。

PG は、クリーンであるとのフラグが立っていない場合、スクラブを行いません。これは、PG が置き忘れたり劣化したりした場合に起こります \(上記の PG\_AVAILABILITY and PG\_DEGRADED を参照ください\)。

クリーンなPGのスクラブを手動で開始するには：

```
ceph pg scrub <pgid>
```

#### PG\_NOT\_DEEP\_SCRUBBED[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

1つまたは複数の PG が最近ディープスクラブされていません。 

PG は通常 osd\_deep\_scrub\_interval 秒ごとにスクラブを行いますが、 mon\_warn\_pg\_not\_deep\_scrubbed\_ratio の割合で、スクラブの期限が切れていない場合にこの警告を発生させます。

PG は、クリーンであるとのフラグが立っていない場合、\(深い\) スクラブ処理を行いません。これは、置き忘れたり劣化したりした場合に起こります \(上記の PG\_AVAILABILITY and PG\_DEGRADED を参照\)。

クリーンなPGのスクラブを手動で開始するには：

```
ceph pg deep-scrub <pgid>
```

#### PG\_SLOW\_SNAP\_TRIMMING[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

1つまたは複数のPGのスナップショットトリムキューが、設定された警告しきい値を超えました。 

これは、非常に多くのスナップショットが最近削除されたか、またはOSDが新しいスナップショット削除の速度に追いつくのに十分迅速にスナップショットをトリミングできないことを示します。

警告のしきい値は mon\_osd\_snap\_trim\_queue\_warn\_on オプションで制御します（デフォルト：32768）。

この警告は、OSDが過度の負荷を受け、バックグラウンドの作業に追いつけない場合、またはOSDの内部メタデータ・データベースが大きく断片化し、実行できない場合に発生することがあります。 また、OSDのその他のパフォーマンスの問題を示す場合もあります。

スナップショットトリムキューの正確なサイズは、_ceph pg ls \-f json\-detail_のsnaptrimq\_lenフィールドで報告されます。

### Miscellaneous[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

#### RECENT\_CRASH[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

1つまたは複数のCephデーモンが最近クラッシュし、そのクラッシュが管理者によってまだアーカイブされて\(ACKされて\)いません。 

これは、ソフトウェアのバグ、ハードウェアの問題 \(ディスクの故障など\)、その他の問題を示している可能性があります。

新しいクラッシュをリストアップするには：

```
ceph crash ls-new
```

特定のクラッシュに関する情報を調べるには：

```
ceph crash info <crash-id>
```

この警告は、クラッシュを「アーカイブ」して（管理者が調べた後）、この警告を発生させないようにすることで消すことができます。

```
ceph crash archive <crash-id>
```

同様に、すべての新しいクラッシュをアーカイブするには：

```
ceph crash archive-all
```

アーカイブされたクラッシュはceph crash lsで引き続き表示されますが、ceph crash ls\-newでは表示されません。

最近が意味する期間は、mgr/crash/warn\_recent\_intervalオプション\(デフォルト: 2週間\)で制御されます。

これらの警告を完全に無効にするには：

```
ceph config set mgr/crash/warn_recent_interval 0
```

#### TELEMETRY\_CHANGED[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

テレメトリーが有効になっていますが、テレメトリーレポートの内容がその時点から変更されているため、テレメトリーレポートが送信されません。

Cephの開発者は、定期的にテレメトリー機能を改訂し、新しい有用な情報を含めたり、無用または機密であることが判明した情報を削除しています。 新しい情報がレポートに含まれる場合、Cephは管理者にテレメトリーの再有効化を要求し、共有される情報の内容を\(再\)確認する機会を確保します。

テレメトリーレポートの内容を確認するには：

```
ceph telemetry show
```

テレメトリーレポートは、複数のオプションチャンネルで構成されており、それぞれ独立して有効または無効にすることができることに注意してください。 詳細については、[Telemetry Module](https://docs.ceph.com/en/pacific/rados/operations/health-checks/)を参照してください。

テレメトリーを再有効化する（この警告を消す）には：

```
ceph telemetry on
```

テレメトリーを無効にする（この警告を消す）には:

```
ceph telemetry off
```

#### AUTH\_BAD\_CAPS[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

1つ以上の認証ユーザーが、モニターで解析できない能力を持っています。 

これは一般に、そのユーザーが 1 つ以上のデーモンタイプで何らかのアクションを実行する権限がないことを示します。

このエラーは、アップグレード後に、構文を適切に検証しない古いバージョンのCephで能力が設定された場合、または能力の構文が変更された場合に発生する可能性がほとんどです。

当該ユーザーを削除するには：

```
ceph auth rm <entity-name>
```

\(これにより、ヘルスアラートは解決されますが、当然、クライアントはそのユーザーとして認証することができません\)。

あるいは、そのユーザーの能力を次のようにして更新することもできます。

```
ceph auth <entity-name> <daemon-type> <caps> [<daemon-type> <caps> ...]
```

認証機能の詳細については、「[User Management](https://docs.ceph.com/en/pacific/rados/operations/health-checks/)」を参照してください。

#### OSD\_NO\_DOWN\_OUT\_INTERVAL[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

mon\_osd\_down\_out\_intervalオプションがゼロに設定されており、これはOSDが故障した後にシステムが自動的に修復またはヒーリング操作を行わないことを意味します。 

その代わりに、管理者\(または他の外部エンティティ\)は、回復を起動するために、OSDを「out」として手動でマークする\(すなわち、ceph osd out\< osd\-id\>\)必要があります。

このオプションは通常5分または10分に設定され、ホストがパワーサイクルまたはリブートするのに十分な時間です。

この警告は、 mon\_warn\_on\_osd\_down\_out\_interval\_zero を false に設定することで無効にすることができます。

```
ceph config global mon mon_warn_on_osd_down_out_interval_zero false
```

#### DASHBOARD\_DEBUG[¶](https://docs.ceph.com/en/pacific/rados/operations/health-checks/ "Permalink to this headline")

Dashboardのデバッグモードが有効になっています。

これは、REST APIリクエストの処理中にエラーが発生した場合、HTTPエラーのレスポンスにPythonのトレースバックが含まれることを意味します。このようなトレースバックには良識ある情報が含まれ、公開される可能性があるため、本番環境ではこの動作を無効にする必要があります。

デバッグモードを無効にするには：

```
ceph dashboard debug disable
```
